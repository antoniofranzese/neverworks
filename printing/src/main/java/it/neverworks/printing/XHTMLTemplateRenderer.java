package it.neverworks.printing;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.Reader;

import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.description.Getter;
import it.neverworks.model.context.Inject;
import it.neverworks.template.TemplateEngine;
import it.neverworks.template.TemplateContext;

public class XHTMLTemplateRenderer extends XHTMLDocumentRenderer implements TemplateRenderer {
        
    @Property @Required @Inject( value = { "it.neverworks.printing.TemplateEngine", "it.neverworks.TemplateEngine" }, lazy = true )
    protected TemplateEngine templates;

    @Property @Required
    protected Object template;
    
    private TemplateContext context;

    protected Reader getReader( Getter<Reader> getter ) {
        if( getter.undefined() ) {
            getter.setRaw( new InputStreamReader( getTemplates().stream( getTemplate(), context != null ? context : getTemplates().createContext() ) ) );
        }
        return getter.get();
    }
    
    public TemplateRenderer with( Arguments arguments ) {
        if( context == null ) {
            context = getTemplates().createContext();
        }
        
        for( String key: arguments.keys() ) {
            context.put( key, arguments.get( key ) );
        }
        return this;
    }
    
    public Object getTemplate(){
        return this.template;
    }
    
    public void setTemplate( Object template ){
        this.template = template;
    }
    
    public TemplateEngine getTemplates(){
        return this.templates;
    }
    
    public void setTemplates( TemplateEngine templates ){
        this.templates = templates;
    }
}