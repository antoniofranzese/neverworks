package it.neverworks.printing;

import java.io.InputStream;
import it.neverworks.model.converters.Convert;

@Convert( PrintingResourcePluginConverter.class )
public interface PrintingResourcePlugin {
    InputStream resolvePrintingResource( String uri );

}