package it.neverworks.printing;

import java.awt.image.BufferedImage;

import it.neverworks.model.converters.IntConverter;

public class ImageColorDepthConverter extends IntConverter {
    
    public Object convert( Object value ) {
        if( value instanceof String ) {
            String s = ((String) value).toLowerCase();
            if( "rgb".equals( s ) ) {
                return BufferedImage.TYPE_INT_RGB;
            } else if( "gray".equals( s )  ) {
                return BufferedImage.TYPE_BYTE_GRAY;
            } else if( "binary".equals( s ) ) {
                return BufferedImage.TYPE_BYTE_BINARY;
            } else if( "indexed".equals( s ) ) {
                return BufferedImage.TYPE_BYTE_INDEXED;
            } else if( "argb".equals( s ) ) {
                return BufferedImage.TYPE_INT_ARGB;
            } else if( "bgr".equals( s ) ) {
                return BufferedImage.TYPE_INT_BGR;
            } else {
                return super.convert( value );
            }
        } else {
            return super.convert( value );
        }
    }
}