package it.neverworks.printing;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.List;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.File;
import java.net.URI;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.AlphaComposite;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xhtmlrenderer.pdf.ITextUserAgent;
import org.xhtmlrenderer.pdf.ITextOutputDevice;
import org.xhtmlrenderer.resource.XMLResource;
import org.xhtmlrenderer.resource.ImageResource;
import org.xhtmlrenderer.swing.Java2DRenderer;
import org.xhtmlrenderer.util.FSImageWriter;

import org.xhtmlrenderer.swing.SwingReplacedElementFactory;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.w3c.dom.Element;

import it.neverworks.io.Streams;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Application;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.description.Default;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.collections.Collection;
import it.neverworks.log.Logger;

public class XHTMLDocumentRenderer implements DocumentRenderer {
    
    private final static Logger logger = Logger.of( "it.neverworks.printing" );
    
    private final static String RESOURCE_PROTOCOL = "resource:";
    private final static List<String> FORMATS = list( "pdf", "png", "jpg" );
    
    @Property 
    protected Resource resources;
    
    @Property @Required
    protected Reader reader;
    
    @Property @AutoConvert @Required
    protected Integer dpi = 72;
    
    @Property @AutoConvert @Required
    protected Integer dpp = 1;
    
    @Property @AutoConvert
    protected Float scale;
    
    @Property @AutoConvert @Required
    protected Integer width = 1024;
    
    @Property @AutoConvert @Required
    protected Integer height = 768;
    
    @Property @Convert( ImageColorDepthConverter.class ) @Required
    protected Integer colors = BufferedImage.TYPE_INT_RGB;

    @Property @AutoConvert
    protected Boolean antialias = true;
    
    @Property @AutoConvert
    protected Boolean close = true;

    @Property @Collection
    protected Map<String, PrintingResourcePlugin> plugins;
    
    protected File temporaryFile;
    
    protected String format = "pdf";
    
    public XHTMLDocumentRenderer() {
        super();
    }
    
    public XHTMLDocumentRenderer( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public String getMimeType() {
        switch( this.format ) {
            case "png":
                return "image/png";
            case "jpg":
                return "image/jpeg";
            default:
                return "application/pdf";
        }
    }

    public DocumentRenderer as( String format ) {

        if( FORMATS.contains( str( format ).trim().toLowerCase() ) ) {
            this.format = format.trim().toLowerCase();
            return this;

        } else {
            throw new IllegalArgumentException( "Unsupported print format: " + format );
        }
    }
    
    public OutputStream to( OutputStream output ) {
        switch( this.format ) {
            case "png":
            case "jpg":
                writeImage( output );
                break;
            default:
                writePDF( output );
        }
        return output;
    }
    
    protected void writePDF( OutputStream output ) {
        try {
            ITextRenderer renderer = new ITextRenderer();
            String resourcedir = ( resources != null && resources.getFile() != null ) ? resources.getFile().getAbsolutePath() : null;
            
            logger.debug( "Using resource directory: {}", resourcedir );
                
            ResolvingITextUserAgent callback = new ResolvingITextUserAgent( renderer.getOutputDevice(), resourcedir, this.plugins );
            callback.setSharedContext( renderer.getSharedContext() );
            renderer.getSharedContext().setUserAgentCallback( callback );

            Document doc = XMLResource.load( new InputSource( getReader() ) ).getDocument();

            renderer.setDocument( doc, RESOURCE_PROTOCOL + "/" );
            renderer.layout();
            renderer.createPDF( output );
        
            Streams.flush( output );
            
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        } finally {
            if( this.close ) {
                Streams.close( output );
            }
        }
    }
    
    protected void writeImage( OutputStream output ) {
        try {

            Document doc = XMLResource.load( new InputSource( getReader() ) ).getDocument();

    		Java2DRenderer renderer = new Java2DRenderer( doc, get( "width" ), get( "height" ) );

            //renderer.getSharedContext().setReplacedElementFactory( new ImageReplacedElementFactory() );

    		renderer.getSharedContext().setDPI( Int( get( "dpi" ) ) * Int( get( "dpp" ) ) );
    		renderer.getSharedContext().setDotsPerPixel( get( "dpp" ) );

            renderer.setBufferedImageType( get( "colors" ) );

            BufferedImage image = renderer.getImage();
            
            if( scale == null ) {
                FSImageWriter imageWriter = new FSImageWriter();
                imageWriter.write( image, output );
                
            } else {
                int resizedWidth = Int( Float( get( "width" ) ) * scale );
                int resizedHeight = Int( Float( get( "height" ) ) * scale );
                BufferedImage resizedImage = new BufferedImage( resizedWidth, resizedHeight, image.getType() );
                Graphics2D graph = resizedImage.createGraphics();

                graph.setComposite( AlphaComposite.Src );
                graph.setRenderingHint( RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR );
                graph.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY );
                //graph.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );

                graph.drawImage( image, 0, 0, resizedWidth, resizedHeight, null );
                graph.dispose();
            
                ImageIO.write( resizedImage, format, output );
                
            }
            
            Streams.flush( output );
            
        } catch( Exception ex ) {
            throw Errors.wrap( ex );

        } finally {
            if( this.close ) {
                Streams.close( output );
            }
        }
        
    }
    
    public void cleanup() {
        if( temporaryFile != null && temporaryFile.exists() ) {
            temporaryFile.delete();
            temporaryFile = null;
        }
    }
    
    public Reader getReader(){
        return this.reader;
    }
    
    public void setReader( Reader reader ){
        this.reader = reader;
    }

    public Resource getResources(){
        return this.resources;
    }
    
    public void setResources( Resource resources ){
        this.resources = resources;
    }
    
    private static class ImageReplacedElementFactory extends SwingReplacedElementFactory {
        public ReplacedElement replaceImage( UserAgentCallback uac, LayoutContext context, Element elem, int cssWidth, int cssHeight ) {
            //TODO: external images
            return super.replaceImage( uac, context, elem, cssWidth, cssHeight );
        }
    }

    private static class ResolvingITextUserAgent extends ITextUserAgent {
    
        private String basedir;
        private Map<String, PrintingResourcePlugin> plugins;
    
        public ResolvingITextUserAgent( ITextOutputDevice outputDevice, String basedir, Map<String, PrintingResourcePlugin> plugins ) {
            super( outputDevice );
            this.basedir = basedir;
            this.plugins = plugins;
        }

        protected InputStream resolveAndOpenStream( String uri ) {
            try {
                if( ! empty( this.basedir ) && uri.startsWith( RESOURCE_PROTOCOL ) ) {
                    String filename = this.basedir + "/" + uri.substring( RESOURCE_PROTOCOL.length() );
                    logger.debug( "Resource path for {}: {}", uri, filename );
                    
                    InputStream is = new FileInputStream( filename );
                    return is;

                } else if( uri.startsWith( "classpath:" ) ) {
                    String filename = uri.substring( 10 );
                    logger.debug( "Class path for {}: {}", uri, filename );
                    InputStream is = Application.resource( filename );
                    if( is == null ) {
                        logger.error( "Error locating {0}: resource not found", uri );
                    }
                    return is;

                } else if( this.plugins != null ){
                    int colon = uri.indexOf( ":" );
                    if( colon > 0 ) {
                        String plugin = uri.substring( 0, colon );
                        if( this.plugins.containsKey( plugin ) ) {
                            String resource = uri.substring( colon + 1 );
                            try {
                                PrintingResourcePlugin resourcePlugin = this.plugins.get( plugin );
                                logger.debug( "Resolving {} with {}", uri, resourcePlugin );
                                return resourcePlugin.resolvePrintingResource( resource );
                            } catch( Exception ex ) {
                                logger.error( "Error using plugin ''{2}'' for {0}: {1, error}", uri, ex, plugin );
                                if( logger.isTraceEnabled() ) {
                                    logger.error( ex );
                                }
                                return null;
                            }
                            
                            
                        } else {
                            logger.error( "Error locating {0}: missing plugin ''{1}''", uri, plugin );
                            return null;
                        }
                        
                    } else {
                        logger.error( "Error locating {0}: missing resource protocol", uri );
                        return null;
                    }
                    
                } else {
                    logger.error( "Error locating {0}: cannot resolve", uri );
                    return null;
                    
                }
                
            } catch( Exception ex ) {
                ex.printStackTrace();
                logger.error( "Error locating {0}: {1,error}", uri, ex );
                return null;
            }
        }
    }
    
}