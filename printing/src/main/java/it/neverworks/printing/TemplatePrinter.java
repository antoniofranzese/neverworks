package it.neverworks.printing;

import java.io.File;
import java.io.InputStream;
import it.neverworks.io.Streams;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.InputStreamProvider;

public interface TemplatePrinter extends DocumentPrinter {

    TemplateRenderer render( String template );
    TemplateRenderer render( InputStream template );

    default TemplateRenderer render( InputStreamProvider streamProvider ) {
        return render( streamProvider.toStream() );
    }

    default TemplateRenderer render( FileInfo info ) {
        return render( info.getStream() );
    }

    default TemplateRenderer render( FileInfoProvider infoProvider ) {
        return render( infoProvider.retrieveFileInfo() );
    }
 
    default TemplateRenderer render( File file ) {
        return render( Streams.asStream( file ) );
    }

}