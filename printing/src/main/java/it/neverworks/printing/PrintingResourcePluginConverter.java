package it.neverworks.printing;

import it.neverworks.model.converters.Converter;
import it.neverworks.io.InputStreamResolver;
import it.neverworks.io.FileProvider;

public class PrintingResourcePluginConverter implements Converter {
    public Object convert( Object value ) {
        if( value instanceof PrintingResourcePlugin ) {
            return value;
        } else if( value instanceof FileProvider ) {
            return new FileProviderPlugin( (FileProvider) value );
        } else if( value instanceof InputStreamResolver ) {
            return new InputStreamResolverPlugin( (InputStreamResolver) value );
        } else {
            return value;
        }
    }
}