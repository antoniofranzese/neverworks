package it.neverworks.printing;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.ContentProvider;
import it.neverworks.io.TypedInputStreamProvider;
import it.neverworks.io.AutoDeleteFileInputStream;
import it.neverworks.model.Model;

public interface DocumentRenderer extends Model, ContentProvider, TypedInputStreamProvider {

    DocumentRenderer as( String format );
    OutputStream to( OutputStream stream );

    default DocumentRenderer set( String name, Object value ) {
        model().safeAssign( name, value );
        return this;
    }
    
    default InputStream toStream() {
        
        try {
            File temporaryFile = File.createTempFile( "nwk-render-" + this.getClass().getSimpleName().toLowerCase() + "-", ".tmp" );
            to( new FileOutputStream( temporaryFile ) );
            return new BufferedInputStream( new AutoDeleteFileInputStream( temporaryFile ) );

        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    default InputStream toPDF() {
        return as( "pdf" ).toStream();
    }

    default InputStream toPNG() {
        return as( "png" ).toStream();
    }

}