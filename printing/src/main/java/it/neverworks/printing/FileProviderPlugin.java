package it.neverworks.printing;

import java.io.InputStream;
import it.neverworks.io.FileProvider;
import it.neverworks.io.FileProviderStreamResolver;

public class FileProviderPlugin extends FileProviderStreamResolver implements PrintingResourcePlugin {
    
    public FileProviderPlugin() {
        super();
    }

    public FileProviderPlugin( FileProvider provider ) {
        super( provider );
    }
    
    public InputStream resolvePrintingResource( String uri ) {
        return this.resolveInputStream( uri );
    }
    
}