package it.neverworks.printing;

import java.io.InputStreamReader;
import java.io.InputStream;

import org.springframework.core.io.Resource;

import it.neverworks.lang.Arguments;
import it.neverworks.template.TemplateEngine;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;

public class XHTMLTemplatePrinter extends XHTMLDocumentPrinter implements TemplatePrinter {
    
    @Property @Required
    protected TemplateEngine templates;
    
    public XHTMLTemplatePrinter() {
        super();
    }

    public XHTMLTemplatePrinter( Arguments arguments ) {
        this();
        set( arguments );
    }

    @Override
    public TemplateRenderer render( String template ) {
        return createRenderer( template );
    }
    
    @Override
    public TemplateRenderer render( InputStream template ) {
        return createRenderer( template );
    }
    
    protected TemplateRenderer createRenderer( Object template ) {
        return (TemplateRenderer) new XHTMLTemplateRenderer()
            .set( "templates", get( "templates" ) )
            .set( "resources", get( "resources" ) )
            .set( "plugins", get( "plugins" ) )
            .set( "template", template );
    }

    public TemplateEngine getTemplates(){
        return this.templates;
    }
    
    public void setTemplates( TemplateEngine templates ){
        this.templates = templates;
    }

    public TemplateEngine getTemplateEngine(){
        return getTemplates();
    }
    
    public void setTemplateEngine( TemplateEngine templateEngine ){
        setTemplates( templateEngine );
    }
    
}