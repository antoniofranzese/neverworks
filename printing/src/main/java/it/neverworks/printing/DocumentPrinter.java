package it.neverworks.printing;

import java.io.File;
import java.io.InputStream;
import it.neverworks.lang.Errors;
import it.neverworks.io.Streams;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.InputStreamProvider;
import it.neverworks.model.Model;

public interface DocumentPrinter extends Model {

    DocumentRenderer render( InputStream stream );
    
    default DocumentRenderer render( InputStreamProvider streamProvider ) {
        return render( streamProvider.toStream() );
    }

    default DocumentRenderer render( FileInfo info ) {
        return render( info.getStream() );
    }

    default DocumentRenderer render( FileInfoProvider infoProvider ) {
        return render( infoProvider.retrieveFileInfo() );
    }
 
    default DocumentRenderer render( File file ) {
        return render( Streams.asStream( file ) );
    }
    
    default DocumentPrinter set( String name, Object value ) {
        model().safeAssign( name, value );
        return this;
    }
    
}