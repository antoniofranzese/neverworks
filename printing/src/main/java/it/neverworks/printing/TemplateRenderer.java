package it.neverworks.printing;

import it.neverworks.lang.Arguments;

public interface TemplateRenderer extends DocumentRenderer {

    TemplateRenderer with( Arguments arguments );
    
    default TemplateRenderer with( String name, Object value ) {
        return this.with( new Arguments().arg( name, value ) );
    }

}