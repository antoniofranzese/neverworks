package it.neverworks.printing;

import java.io.InputStream;
import it.neverworks.io.InputStreamResolver;

public class InputStreamResolverPlugin implements PrintingResourcePlugin {
    
    private InputStreamResolver streamResolver;
    
    public InputStreamResolverPlugin( InputStreamResolver streamResolver ) {
        this.streamResolver = streamResolver;
    }
    
    public InputStream resolvePrintingResource( String uri ) {
        return this.streamResolver.resolveInputStream( uri );
    }

}