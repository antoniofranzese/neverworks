package it.neverworks.printing;

import java.util.Map;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.File;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ClassPathResource;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.template.TemplateEngine;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;

public class XHTMLDocumentPrinter implements DocumentPrinter {
    
    @Property @Required @AutoConvert
    protected Resource resources;
    
    @Property @Collection
    private Map<String,PrintingResourcePlugin> plugins;
    
    public XHTMLDocumentPrinter() {
        super();
        if( resources == null ) {
            set( "resources", new ClassPathResource( "resources" ) /* "classpath:resources" */ );
        }
    }
    
    public XHTMLDocumentPrinter( Arguments arguments ) {
        this();
        set( arguments );
    }

    public DocumentRenderer render( InputStream stream ) {
        return (DocumentRenderer) new XHTMLDocumentRenderer()
            .set( "reader", new InputStreamReader( stream ) )
            .set( "resources", get( "resources" ) )
            .set( "plugins", get( "plugins" ) );
    }

    public Resource getResources(){
        return this.resources;
    }
    
    public void setResources( Resource resources ){
        this.resources = resources;
    }
    
    public Map<String,PrintingResourcePlugin> getPlugins(){
        return this.plugins;
    }
    
    public void setPlugins( Map<String,PrintingResourcePlugin> plugins ){
        this.plugins = plugins;
    }

}