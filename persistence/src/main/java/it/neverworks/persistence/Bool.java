package it.neverworks.persistence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Inherited;

@Inherited
@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Bool {
    String yes() default "";
    String no() default "";
    int on() default 1;
    int off() default 0;
    boolean lenient() default false;
}
