package it.neverworks.persistence;

import it.neverworks.persistence.criteria.Finder;

public class FindEvent extends PersistenceEvent {
    
    protected Finder finder;
    protected Class target;
    
    public Class getTarget(){
        return this.target;
    }
    
    public void setTarget( Class target ){
        this.target = target;
    }
    
    public <F> Finder<F> getFinder(){
        return (Finder<F>) this.finder;
    }
    
    public void setFinder( Finder finder ){
        this.finder = finder;
    }
}