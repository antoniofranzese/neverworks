package it.neverworks.persistence;

import it.neverworks.lang.Objects;
import it.neverworks.model.events.Event;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.persistence.entities.EntityManager;

public class PersistenceEvent extends Event {

    protected Object entity;
    
    protected EntityManager manager;
    
    public EntityManager getManager(){
        return this.manager;
    }
    
    public void setManager( EntityManager manager ){
        this.manager = manager;
    }
    
    public Object getEntity(){
        return this.entity;
    }
    
    public void setEntity( Object entity ){
        this.entity = entity;
    }
    
    public Class getTarget() {
        return this.entity != null ? this.entity.getClass() : null;
    }
    
    private boolean vetoed;
    
    public boolean vetoed(){
        return this.vetoed;
    }

    public void veto() {
        this.vetoed = true;
    }
    
    public void unveto() {
        this.vetoed = false;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "entity" )
            .add( "manager" )
            .toString();
    }
}