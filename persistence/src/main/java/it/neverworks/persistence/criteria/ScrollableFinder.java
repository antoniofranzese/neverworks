package it.neverworks.persistence.criteria;

public interface ScrollableFinder {
    Scroller scroll();
}