package it.neverworks.persistence.criteria;

import java.util.List;
import it.neverworks.lang.Functional;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Work;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.Value;

public interface Finder<T> extends Functional<T> {

	default CriteriaBuilder<T> by( String property ) {
        return and( property );
    }

	public CriteriaBuilder<T> and( String property );
	
	public CriteriaBuilder<T> or( String property );

    public Finder<T> begin();

	public Finder<T> include( String... properties );
	
	public Finder<T> select( String... properties );
	
	public Finder<T> pack();

	public Finder<T> expr( String expression, Object... params );
	
	public Finder<T> collect( String property );

	public Finder<T> first( Integer index );

	public Finder<T> max( Integer elements );
	
	public Finder<T> order( String... properties );
	public Finder<T> order( String property );
	public Finder<T> order( Iterable<String> properties );
	
	public Finder<T> example( T example );
    public Finder<T> transform( Transformer transformer );
	
	public Finder<T> copy();

	public int count();
	
	public FluentList<T> list();
	
	public T result();
    public Value value();
    public Value value( String expression );
    public <V> V get( String expression );
    public <V> V get( String expression, V defaultValue );
    
    public ObjectQuery<T> query();
    
    public Finder<T> with( String name );
    public Finder<T> with( String name, Object value );
    public Finder<T> without( String name );
    public boolean has( String name );
    public Value attribute( String name );
	public Finder<T> use( Iterable<String> features );
	public Finder<T> use( String... features );
    
    public Finder<T> comment( String comment );
    
    default Finder<T> within( Work<Finder> work ) {
        try {
            if( work != null ) {
                work.doWithinWork( this );
            }
            return this;
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
}