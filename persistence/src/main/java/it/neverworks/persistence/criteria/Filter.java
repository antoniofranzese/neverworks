package it.neverworks.persistence.criteria;

import java.util.List;

public interface Filter<T> {
	List<T> filterList( List<T> result );
}
