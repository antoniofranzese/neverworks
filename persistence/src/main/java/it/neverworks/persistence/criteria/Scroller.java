package it.neverworks.persistence.criteria;

public interface Scroller {
    public boolean next();
    public <T> T get( int index );
}