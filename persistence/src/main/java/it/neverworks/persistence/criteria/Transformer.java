package it.neverworks.persistence.criteria;

import java.util.List;
import java.util.Arrays;
import it.neverworks.model.features.Retrieve;

public interface Transformer<T> {
    
    default T transformRow( Object[] values, String[] names ) {
        return transformItem( new Item( values, names ) );
    }

	T transformItem( Item item );

	default List<T> transformList( List list ) {
	    return (List<T>)list;
	}
    
    public class Item implements Retrieve {
        private Object[] row;
        private String[] names;
        
        public Item( Object[] row, String[] names ) {
            this.row = row;
            this.names = names;
        }
        
        public Object retrieveItem( Object key ) {
            if( key instanceof String ) {
                return get( (String) key );
                
            } else if( key instanceof Number ) {
                return get( ((Number) key).intValue() );
                
            } else {
                throw new IllegalArgumentException( "Invalid key: " + key );
            }
        }
        
        public int indexOf( String name ) {
            int index = -1;
            int i = 0;
            while( index < 0 && i < this.row.length ) {
                if( this.names[ i ].equals( name ) ) {
                    index = i;
                } else {
                    i += 1;
                }
            }
            return index;
        }

        public int size() {
            return this.row.length;
        }

        public boolean contains( String name ) {
            return this.indexOf( name ) >= 0;
        }

        public boolean has( String name ) {
            return contains( name );
        }

        public boolean contains( int index ) {
            return index >= 0 && index < this.row.length;
        }

        public boolean has( int index ) {
            return has( index );
        }

        public Object[] getValues(){
            return this.row;
        }
        
        public String[] getNames(){
            return this.names;
        }
        
        public Object[] values(){
            return this.row;
        }
        
        public String[] names(){
            return this.names;
        }
        
        public <T> T get( String name ) {
            int index = this.indexOf( name );
            if( index >= 0 ) {
                return (T) this.row[ index ];
            } else {
                throw new IllegalArgumentException( "Missing property in result: " + name );
            }
        }

        public <T> T get( int index ) {
            if( index >= 0 && index < this.row.length ) {
                return (T) this.row[ index ];
            } else {
                throw new IllegalArgumentException( "Invalid item field index: " + index );
            }
        }
        
        public String toString() {
            return "<names=" + Arrays.asList( this.names ) + ", row=" + Arrays.asList( this.row ) + ">";
        }
    }
    
}
