package it.neverworks.persistence.criteria;

import java.util.List;
import java.util.Map;
import it.neverworks.lang.Functional;
import it.neverworks.lang.FluentList;
import it.neverworks.model.Value;

public interface Query<T> extends Functional<T> {
	Query<T> set( String name, Object value );
	Query<T> setAll( List<Object> params );
	Query<T> setAll( Object... params );
	Query<T> setAll( Map<String, Object> params );
	Query<T> max( int elements );
	
	Query<T> use( String... features );
	Query<T> transform( Transformer transformer );
    Query<T> comment( String comment );
    
	T result();
	Value value();
	FluentList<T> list();
	void execute();
}
