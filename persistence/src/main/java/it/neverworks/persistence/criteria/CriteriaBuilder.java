package it.neverworks.persistence.criteria;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Wrapper;

public interface CriteriaBuilder<T> {

	public Finder<T> eq(Object value);
	public Finder<T> like( Object value );
	public Finder<T> startsWith( Object value );
	public Finder<T> endsWith( Object value );
	public Finder<T> contains( Object value );
	public Finder<T> gt( Object value );
	public Finder<T> ge( Object value );
	public Finder<T> lt( Object value );
	public Finder<T> le( Object value );
	public Finder<T> between( Object lowValue, Object highValue);
	public Finder<T> isNull();
	public Finder<T> in( Object... values );
	public Finder<T> in( Iterable values );
	public Finder<T> in( Wrapper values );
	public Finder<T> in( Finder finder );
	public Finder<T> using( Object... values );
	public Finder<T> using( Map arguments );
    public CriteriaBuilder<T> not();
    
    public Finder<T> within( Object object );
    public Finder<T> intersects( Object object );
    public Finder<T> overlaps( Object object );
    public Finder<T> crosses( Object object );
    public Finder<T> touches( Object object );
    public Finder<T> disjoint( Object object );
    
    // legacy nots
    
	default Finder<T> ne( Object value ) {
        return not().eq( value );
	}

	default Finder<T> notLike( Object value ) {
        return not().like( value );
	}

	default Finder<T> notIn( Wrapper values ) {
        return not().in( values );
	}

	default Finder<T> notIn( Finder finder ) {
        return not().in( finder );
	}

	default Finder<T> notIn( Iterable values ) {
        return not().in( values );
	}

	default Finder<T> notIn( Object... values ) {
        return not().in( values );
	}

	default Finder<T> notNull() {
        return not().isNull();
	}

    // ultra-legacy                   
                           
	default Finder<T> notlike( String value ) {
	    return notLike( value );
	}

	default Finder<T> isNotNull() {
	    return notNull();
	}
}