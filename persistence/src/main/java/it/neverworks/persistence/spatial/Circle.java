package it.neverworks.persistence.spatial;

import static it.neverworks.language.*;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.util.GeometricShapeFactory;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.utils.Identification;

@Identification({ "x", "y", "radius", "resolution", "srid" })
public class Circle extends Polygon {
    
    @Property @Convert( CoordinateConverter.class )
    private Number x = Double.NaN;
    
    @Property @Convert( CoordinateConverter.class )
    private Number y = Double.NaN;

    @Property @AutoConvert
    private Double radius = new Double( 0 );
    
    @Property @AutoConvert
    private int resolution = 32;
    
    public Circle() {
        super();
    }
    
    public Circle( Object x, Object y, Object radius ) {
        this();
        set( "x", x );
        set( "y", y );
        set( "radius", radius );
    }
    
    public Circle( Point center, Object radius ) {
        this();
        set( "center", center );
        set( "radius", radius );
    }
    
    public Circle( Arguments arguments ) {
        this();
        set( arguments );
    }
    
     public Geometry toJTSGeometry() {
        GeometricShapeFactory shapeFactory = new GeometricShapeFactory();
        shapeFactory.setNumPoints( this.resolution );
        shapeFactory.setCentre( new Coordinate( x.doubleValue(), y.doubleValue() ) ); 
        shapeFactory.setSize( this.radius * 2 );
        
        Geometry result = shapeFactory.createCircle();        
        if( this.srid != null ) {
            result.setSRID( this.srid );
        }
        return result;
    }
    
    public int getResolution(){
        return this.resolution;
    }
    
    public void setResolution( int resolution ){
        this.resolution = resolution;
    }
        
    public Double getRadius(){
        return this.radius;
    }
    
    public void setRadius( Double radius ){
        this.radius = radius;
    }

    public Number getY(){
        return this.y;
    }
    
    public void setY( Number y ){
        this.y = y;
    }
    
    public Number getX(){
        return this.x;
    }
    
    public void setX( Number x ){
        this.x = x;
    }
    
    @Virtual
    public Point getCenter(){
        return new Point( this.x, this.y ).set( "srid", getSrid() );
    }
    
    @AutoConvert
    public void setCenter( Point center ){
        this.x = center != null ? Double( center.getX() ) : null;
        this.y = center != null ? Double( center.getY() ) : null;
    }
    
}