package it.neverworks.persistence.spatial;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.PrecisionModel;
import org.opengis.referencing.operation.MathTransform;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Required;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.utils.Identification;

@Convert( PointConverter.class )
@Identification({ "x", "y", "z", "srid" })
public class Point extends GeometryModel {

    @Property @Convert( CoordinateConverter.class )
    private Number x = Double.NaN;
    
    @Property @Convert( CoordinateConverter.class )
    private Number y = Double.NaN;

    @Property @Convert( CoordinateConverter.class )
    private Number z = Double.NaN;
    
    @Property
    private Integer decimals = null;
    
    @Override
    public Point transform( MathTransform transform ) {
        return new Point( (com.vividsolutions.jts.geom.Point) transformJTS( transform ) );
    }
    
    public Integer getDecimals(){
        return this.decimals;
    }
    
    public void setDecimals( Integer decimals ){
        this.decimals = decimals;
    }

    public Point() {
        super();
    }
    
    public Point( Object x, Object y ) {
        this();
        set( "x", x );
        set( "y", y );
    }

    public Point( Object x, Object y, Object z ) {
        this( x, y );
        set( "z", z );
    }

    public Point( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Point( com.vividsolutions.jts.geom.Point point ) {
        this();
        if( point != null && point.getCoordinate() != null ) {
            
            this.x = point.getCoordinate().getOrdinate( Coordinate.X );
            this.y = point.getCoordinate().getOrdinate( Coordinate.Y );
            this.z = point.getCoordinate().getOrdinate( Coordinate.Z );
            if( point.getSRID() != 0 ) {
                set( "srid", point.getSRID() );
            }
        }
    }
    
    public Point( Coordinate coord ) {
        this();
        set( "x", coord.getOrdinate( Coordinate.X ) );
        set( "y", coord.getOrdinate( Coordinate.Y ) );
        set( "z", coord.getOrdinate( Coordinate.Z ) );
    }
    
    public static Point of( com.vividsolutions.jts.geom.Geometry geometry ) {
        if( geometry instanceof com.vividsolutions.jts.geom.Point ) {
            return new Point( (com.vividsolutions.jts.geom.Point) geometry );
        } else if( geometry != null ){
            return new Point( geometry.getCentroid() );
        } else {
            return null;
        }
    }

    protected double scale( Number value ) {
        if( decimals == null ) {
            return value.doubleValue();
        } else if( value != null && ! Numbers.isNaN( value ) ) {
            return Math.round( value.doubleValue() * Math.pow( 10, decimals ) ) / Math.pow( 10, decimals );
        } else {
            return value.doubleValue();
        }
    }
    
    public Geometry toJTSGeometry() {
        CoordinateArraySequence coords = new CoordinateArraySequence( 1 );
        coords.setOrdinate( 0, CoordinateArraySequence.X, scale( getX() ) );
        coords.setOrdinate( 0, CoordinateArraySequence.Y, scale( getY() ) );
        coords.setOrdinate( 0, CoordinateArraySequence.Z, scale( getZ() ) );
        
        PrecisionModel precision = new PrecisionModel( PrecisionModel.FLOATING );
        GeometryFactory factory = getSrid() != null ? new GeometryFactory( precision, getSrid() ) : new GeometryFactory( precision );
        com.vividsolutions.jts.geom.Point point = new com.vividsolutions.jts.geom.Point( coords, factory );
        
        return point;
    }
    
    public Number getZ(){
        return this.z;
    }
    
    public void setZ( Number z ){
        this.z = z;
    }
    
    public Number getY(){
        return this.y;
    }
    
    public void setY( Number y ){
        this.y = y;
    }
    
    public Number getX(){
        return this.x;
    }
    
    public void setX( Number x ){
        this.x = x;
    }

    public String toString() {
        ToStringBuilder builder = new ToStringBuilder( this ) 
            .add( "x" )
            .add( "y" );

        if( z != null && ! Numbers.isNaN( z ) ) {
            builder.add( "z" );
        }
        
        return builder
            .add( "!srid" )
            .toString();
    }
}