package it.neverworks.persistence.spatial;

import static it.neverworks.language.*;

import java.util.List;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import org.opengis.referencing.operation.MathTransform;

import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;

public class Scene extends GeometryModel {
    
    @Property @Collection
    private List<GeometryModel> geometries;
    
    public Scene() {
        super();
    }

    public Scene( Iterable geometries ) {
        this();
        if( geometries != null ) {
            for( Object geometry: geometries ) {
                this.add( geometry );
            }
        }
    }
    
    public Scene( GeometryCollection collection ) {
        this();
        if( collection != null ) {
            for( int i: range( collection.getLength() ) ) {
                this.add( collection.getGeometryN( i ) );
            }
        }
    }
    
    public Scene add( Object geometry ) {
        if( geometry != null ) {
            value( "geometries" ).add( geometry );
        }
        return this;
    }
    
    public Geometry toJTSGeometry() {
        PrecisionModel precision = new PrecisionModel( PrecisionModel.FLOATING );
        GeometryFactory factory = srid != null ? new GeometryFactory( precision, srid ) : new GeometryFactory( precision );
        Geometry[] geoms = each( this.getGeometries() )
            .map( g -> g.toJTSGeometry() )
            .array( Geometry.class );

        return new GeometryCollection( geoms, factory );
    }

    public Scene transform( MathTransform transform ) {
        return new Scene( (GeometryCollection) transformJTS( transform ) );
    }
    
    public List<GeometryModel> getGeometries(){
        return this.geometries;
    }
    
    public void setGeometries( List<GeometryModel> geometries ){
        this.geometries = geometries;
    }
}