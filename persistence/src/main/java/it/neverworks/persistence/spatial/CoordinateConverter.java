package it.neverworks.persistence.spatial;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;

public class CoordinateConverter implements Converter {
    
    private final static TypeDefinition<Double> DoubleType = TypeFactory.shared( Double.class );
    
    public Object convert( Object value ) {
        if( value instanceof Number ) {
            return value;
        } else {
            return DoubleType.convert( value );
        }
    }

}