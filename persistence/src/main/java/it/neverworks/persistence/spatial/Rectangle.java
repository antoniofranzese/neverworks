package it.neverworks.persistence.spatial;

import static it.neverworks.language.*;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import org.opengis.referencing.operation.MathTransform;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.utils.Identification;

@Identification({ "x1", "y1", "x2", "y2", "srid" })
public class Rectangle extends Polygon {
    
    @Property @Convert( CoordinateConverter.class )
    private Number x1;
    
    @Property @Convert( CoordinateConverter.class )
    private Number x2;
    
    @Property @Convert( CoordinateConverter.class )
    private Number y1;
    
    @Property @Convert( CoordinateConverter.class )
    private Number y2;
    
    public Rectangle() {
        super();
    }
    
    public Rectangle( Object x1, Object y1, Object x2, Object y2 ) {
        this();
        set( "x1", x1 );
        set( "y1", y1 );
        set( "x2", x2 );
        set( "y2", y2 );
    }

    protected CoordinateArraySequence getCoordinateSequence() {
        getShell().clear();
        if( x1 != null && y1 != null && x2 != null && y2 != null ) {
            add( x1, y1 );
            add( x2, y1 );
            add( x2, y2 );
            add( x1, y2 );
            add( x1, y1 );
            return super.getCoordinateSequence();
        } else {
            return null;
        }
    }
    
    @Override
    public Rectangle transform( MathTransform transform ) {
        return new Rectangle( getTop().transform( transform ), getBottom().transform( transform ) );
    }
    
    public Rectangle( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Rectangle( Point top, Point bottom ) {
        this();
        set( "top", top );
        set( "bottom", bottom );
        set( "srid", top.get( "srid" ) );
    }
    
    public Rectangle( Envelope envelope ) {
        this();
        set( "x1", envelope.getMinX() );
        set( "x2", envelope.getMaxX() );
        set( "y1", envelope.getMinY() );
        set( "y2", envelope.getMaxY() );
    }
    
    @Virtual
    public Point getTop(){
        return new Point( this.x1, this.y1 ).set( "srid", getSrid() );
    }
    
    @AutoConvert
    public void setTop( Point point ){
        this.x1 = point != null ? Double( point.getX() ) : null;
        this.y1 = point != null ? Double( point.getY() ) : null;
    }

    @Virtual
    public Point getBottom(){
        return new Point( this.x2, this.y2 ).set( "srid", getSrid() );
    }
    
    @AutoConvert
    public void setBottom( Point point ){
        this.x2 = point != null ? Double( point.getX() ) : null;
        this.y2 = point != null ? Double( point.getY() ) : null;
    }

    @Virtual @Override
    public Double getWidth(){
        return Math.abs( Double( this.x2 ) - Double( this.x1 ) );
    }

    @Virtual @Override
    public Double getHeight(){
        return Math.abs( Double( this.y2 ) - Double( this.y1 ) );
    }

    public Number getY2(){
        return this.y2;
    }
    
    public void setY2( Number y2 ){
        this.y2 = y2;
    }
    
    public Number getY1(){
        return this.y1;
    }
    
    public void setY1( Number y1 ){
        this.y1 = y1;
    }
    
    public Number getX2(){
        return this.x2;
    }
    
    public void setX2( Number x2 ){
        this.x2 = x2;
    }
    
    public Number getX1(){
        return this.x1;
    }
    
    public void setX1( Number x1 ){
        this.x1 = x1;
    }
}