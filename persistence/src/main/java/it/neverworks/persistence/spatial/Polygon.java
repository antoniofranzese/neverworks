package it.neverworks.persistence.spatial;

import static it.neverworks.language.*;

import java.util.List;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.LinearRing;
import org.opengis.referencing.operation.MathTransform;

import it.neverworks.lang.Range;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Functional;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.utils.Identification;

@Convert( PolygonConverter.class )
@Identification({ "srid", "*shell", "*holes" })
public class Polygon extends GeometryModel {

    @Property @Collection
    protected List<Point> shell;
    
    @Property @Collection
    protected List<Polygon> holes;
    
    @Property @AutoConvert
    protected Integer decimals;
    
    public Polygon() {
        super();
    }
    
    public Polygon( Object shell ) {
        this();
        set( "shell", Collections.list( shell ) );
    }

    public Polygon( Arguments arguments ) {
        this();
        set( arguments );
    }

    public Polygon( com.vividsolutions.jts.geom.Polygon polygon ) {
        this();
        if( polygon != null ) {
            for( Coordinate coord: polygon.getCoordinates() ) {
                add( 
                    coord.getOrdinate( Coordinate.X ) 
                    ,coord.getOrdinate( Coordinate.Y ) 
                    ,coord.getOrdinate( Coordinate.Z ) 
                );
            }
            this.set( "srid", polygon.getSRID() );
        }
    }
    
    public static Polygon of( com.vividsolutions.jts.geom.Geometry geometry ) {
        if( geometry instanceof com.vividsolutions.jts.geom.Polygon ) {
            return new Polygon( (com.vividsolutions.jts.geom.Polygon) geometry );
        } else {
            throw new IllegalArgumentException( "Invalid Polygon Geometry: " + geometry );
        }
    }

    @Override
    public Polygon transform( MathTransform transform ) {
        return new Polygon( (com.vividsolutions.jts.geom.Polygon) transformJTS( transform ) );
    }

    public Polygon add( Point point ) {
        if( point != null ) {
            point.set( "srid", this.get( "srid" ) );
            getShell().add( point );
            if( point.getDecimals() == null ) {
                point.setDecimals( decimals );
            }
        }
        return this;
    }

    public Polygon add( Object x, Object y ) {
        return add( new Point( x, y ) );
    }

    public Polygon add( Object x, Object y, Object z ) {
        return add( new Point( x, y, z ) );
    }

    public Polygon add( com.vividsolutions.jts.geom.Point point ) {
        return add( new Point( point ) );
    }

    public Polygon subtract( Polygon polygon ) {
        getHoles().add( polygon );
        if( polygon.getDecimals() == null ) {
            polygon.setDecimals( decimals );
        }
        return this;
    }

    public Geometry toJTSGeometry() {
        CoordinateArraySequence coords = getCoordinateSequence();
        if( coords != null ) {
            PrecisionModel precision = new PrecisionModel( PrecisionModel.FLOATING );
            GeometryFactory factory = srid != null ? new GeometryFactory( precision, srid ) : new GeometryFactory( precision );
            LinearRing ring = new LinearRing( coords, factory );
            
            FluentList<LinearRing> holeRings = new FluentArrayList<LinearRing>();
            
            for( int h: Range.of( holes ) ) {
                CoordinateArraySequence holeCoords = holes.get( h ).getCoordinateSequence();
                if( holeCoords != null ) {
                    holeRings.add( new LinearRing( holeCoords, factory ) );
                }
            }
            
            com.vividsolutions.jts.geom.Polygon polygon = new com.vividsolutions.jts.geom.Polygon( ring, holeRings.array( LinearRing.class ), factory );
            return polygon;

        } else {
            throw new IllegalStateException( "Cannot build Polygon, empty shell" );
        }
    }

    protected CoordinateArraySequence getCoordinateSequence() {
        List<Point> shell = each( getShell() ).filter( p -> p != null ).list();
        if( shell.size() > 0 ) {
            
            if( ! shell.get( 0 ).equals( shell.get( shell.size() - 1 ) ) ) {
                shell.add( shell.get( 0 ) );
            }

            CoordinateArraySequence coords = new CoordinateArraySequence( shell.size() );
            for( int i: Range.of( shell ) ) {
                coords.setOrdinate( i, CoordinateArraySequence.X, shell.get( i ).getX().doubleValue() );
                coords.setOrdinate( i, CoordinateArraySequence.Y, shell.get( i ).getY().doubleValue() );
                coords.setOrdinate( i, CoordinateArraySequence.Z, shell.get( i ).getZ().doubleValue() );
            }
            return coords;
        } else {
            return null;
        }
    }
    
    protected void setDecimals( Setter<Integer> setter ) {
        setter.set();
        getAllPoints().inspect( p -> p.setDecimals( setter.raw() ) );
    }
    
    protected void setSrid( Setter<Integer> setter ) {
        setter.set();
        getAllPoints().inspect( p -> p.setSrid( setter.raw() ) );
    }
    
    protected Functional<Point> getAllPoints() {
        FluentList<Point> result = new FluentArrayList<Point>();
        if( shell != null ) {
            result.addAll( shell );
        }
        if( holes != null ) {
            for( Polygon hole: holes ) {
                result.addAll( hole.getPoints() );
            }
        }
        return result;
    }
    
    @Virtual
    public List<Point> getPoints(){
        return this.shell;
    }

    public void setPoints( List<Point> points ){
        this.shell = points;
    }

    public List<Point> getShell(){
        return this.shell;
    }
    
    public void setShell( List<Point> shell ){
        this.shell = shell;
    }
    
    public List<Polygon> getHoles(){
        return this.holes;
    }
    
    public void setHoles( List<Polygon> holes ){
        this.holes = holes;
    }
    
    public Integer getDecimals(){
        return this.decimals;
    }
    
    public void setDecimals( Integer decimals ){
        this.decimals = decimals;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "shell" )
            .optional( "holes", this.holes )
            .toString();
    }
}