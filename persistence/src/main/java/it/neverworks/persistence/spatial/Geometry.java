package it.neverworks.persistence.spatial;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Inherited;

@Inherited
@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
public @interface Geometry {
}
