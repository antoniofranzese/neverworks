package it.neverworks.persistence.spatial;

import com.vividsolutions.jts.geom.Geometry;
import it.neverworks.model.converters.Converter;

public class JTSGeometryConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Geometry ) {
            return value;

        } else if( value instanceof GeometryModel ) {
            return ((GeometryModel) value).toJTSGeometry();

        } else if( value instanceof String ) {
            try {
                return new WKT( (String) value ).toJTSGeometry();
            } catch( Exception ex ) {
                return value;
            }
        
        } else {
            return value;
     
        }
    }
    
}