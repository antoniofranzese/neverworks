package it.neverworks.persistence.spatial;

import java.util.List;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.PrecisionModel;
import com.vividsolutions.jts.geom.LinearRing;
import org.opengis.referencing.operation.MathTransform;

import it.neverworks.lang.Range;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.utils.EqualsBuilder;

public class LineString extends GeometryModel {

    @Property @Collection
    protected List<Point> points;
    
    @Property @AutoConvert
    protected Integer decimals;
    
    public LineString() {
        super();
    }
    
    public LineString( Object points ) {
        this();
        set( "points", Collections.list( points ) );
    }

    public LineString( Arguments arguments ) {
        this();
        set( arguments );
    }

    public LineString( com.vividsolutions.jts.geom.LineString lineString ) {
        this();
        if( lineString != null ) {
            CoordinateSequence sequence = lineString.getCoordinateSequence();
            for( int i = 0; i < sequence.size(); i++ ) {
                Coordinate coord = sequence.getCoordinate( i );
                add( 
                    coord.getOrdinate( Coordinate.X ) 
                    ,coord.getOrdinate( Coordinate.Y ) 
                    ,coord.getOrdinate( Coordinate.Z ) 
                );
            }
        }
    }

    public static LineString of( com.vividsolutions.jts.geom.Geometry geometry ) {
        if( geometry instanceof com.vividsolutions.jts.geom.LineString ) {
            return new LineString( (com.vividsolutions.jts.geom.LineString) geometry );
        } else {
            throw new IllegalArgumentException( "Invalid Point Geometry: " + geometry );
        }
    }
    
    @Override
    public LineString transform( MathTransform transform ) {
        return new LineString( (com.vividsolutions.jts.geom.LineString) transformJTS( transform ) );
    }
    
    public LineString add( Point point ) {
        if( point != null ) {
            getPoints().add( point );
            if( point.getDecimals() == null ) {
                point.setDecimals( decimals );
            }
        }
        return this;
    }

    public LineString add( Object x, Object y ) {
        getPoints().add( new Point( x, y ).<Point>set( "decimals", decimals ) );
        return this;
    }

    public LineString add( Object x, Object y, Object z ) {
        getPoints().add( new Point( x, y, z ).<Point>set( "decimals", decimals ) );
        return this;
    }

    public LineString add( com.vividsolutions.jts.geom.Point point ) {
        if( point != null ) {
            getPoints().add( new Point( point ).<Point>set( "decimals", decimals ) );
        }
        return this;
    }

    public Geometry toJTSGeometry() {
        CoordinateArraySequence coords = getCoordinateSequence();
        if( coords != null ) {
            PrecisionModel precision = new PrecisionModel( PrecisionModel.FLOATING );
            GeometryFactory factory = srid != null ? new GeometryFactory( precision, srid ) : new GeometryFactory( precision );

            com.vividsolutions.jts.geom.LineString polygon = new com.vividsolutions.jts.geom.LineString( coords, factory );
            return polygon;

        } else {
            throw new IllegalStateException( "Cannot build Polygon, empty points" );
        }
    }

    protected CoordinateArraySequence getCoordinateSequence() {
        List<Point> points = getPoints();
        if( points.size() > 0 ) {
            
            CoordinateArraySequence coords = new CoordinateArraySequence( points.size() );
            for( int i: Range.of( points ) ) {
                coords.setOrdinate( i, CoordinateArraySequence.X, points.get( i ).getX().doubleValue() );
                coords.setOrdinate( i, CoordinateArraySequence.Y, points.get( i ).getY().doubleValue() );
                coords.setOrdinate( i, CoordinateArraySequence.Z, points.get( i ).getZ().doubleValue() );
            }
            return coords;
        } else {
            return null;
        }
    }
    
    protected void setDecimals( Setter<Integer> setter ) {
        setter.set();
        if( points != null ) {
            for( Point point: points ) {
                if( point.getDecimals() == null ) {
                    point.setDecimals( decimals );
                }
            }
            
        }
    }

    public List<Point> getPoints(){
        return this.points;
    }
    
    public void setPoints( List<Point> points ){
        this.points = points;
    }
    
    public Integer getDecimals(){
        return this.decimals;
    }
    
    public void setDecimals( Integer decimals ){
        this.decimals = decimals;
    }

    public boolean equals( Object other ) {
        return new EqualsBuilder( this )
            .add( "srid" )
            .add( "*points" )
            .equals( other );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "points" )
            .add( "srid" )
            .toString();
    }
    
}