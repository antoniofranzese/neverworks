package it.neverworks.persistence.spatial;

import static it.neverworks.language.*;

import it.neverworks.model.converters.Converter;

public class GeometryModelConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof GeometryModel ) {
            return value;

        } else if( value instanceof com.vividsolutions.jts.geom.Geometry ) {
            if( value instanceof com.vividsolutions.jts.geom.Point ) {
                return new Point( (com.vividsolutions.jts.geom.Point) value );
            } else if( value instanceof com.vividsolutions.jts.geom.Polygon ) {
                return new Polygon( (com.vividsolutions.jts.geom.Polygon) value );
            } else if( value instanceof com.vividsolutions.jts.geom.LineString ) {
                return new LineString( (com.vividsolutions.jts.geom.LineString) value );
            } else {
                return new WKT( ((com.vividsolutions.jts.geom.Polygon) value).toText() );
            }

        } else if( value instanceof String ) {
            try {
                return convert( new WKT( (String) value ).toJTSGeometry() );
            } catch( Exception ex ) {
                return value;
            }

        } else {
            return value;
        }
    }
    
}