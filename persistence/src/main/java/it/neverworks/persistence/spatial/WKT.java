package it.neverworks.persistence.spatial;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.ParseException;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.Identification;

@Identification({ "text", "srid" })
public class WKT extends GeometryModel {
    
    @Property @AutoConvert
    private String text;
    
    public WKT(){
        super();
    }
    
    public WKT( Object text ) {
        this();
        set( "text", text );
    }

    public WKT( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Geometry toJTSGeometry() {
        try {
            Geometry geometry = new WKTReader().read( text );
            if( geometry != null && srid != null ) {
                geometry.setSRID( srid );
            }
            return geometry;
        } catch( ParseException ex ) {
            throw Errors.wrap( ex );
        }
    }

    public String getText(){
        return this.text;
    }
    
    public void setText( String text ){
        this.text = text;
    }
}