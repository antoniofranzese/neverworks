package it.neverworks.persistence.spatial;

import java.util.List;
import it.neverworks.lang.Collections;

public class PointConverter extends GeometryModelConverter {
    
    public Object convert( Object value ) {
        if( value instanceof Point ) {
            return value;

        } else if( Collections.isListable( value ) ) {
            List coords = Collections.list( value );
            if( coords.size() >= 2 ) {
                Point point = new Point()
                    .set( "x", coords.get( 0 ) )
                    .set( "y", coords.get( 1 ) );

                if( coords.size() > 2 ) {
                    point.set( "z", coords.get( 2 ) );
                }
                return point;
            } else {
                return pass( value );
            }

        } else {
            return pass( value );
        }
    }
    
    protected Object pass( Object value ) {
        Object converted = super.convert( value );
        if( converted instanceof Point ) {
            return converted;
        } else {
            return value;
        }
    }
}