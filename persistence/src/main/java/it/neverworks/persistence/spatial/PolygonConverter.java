package it.neverworks.persistence.spatial;

import it.neverworks.lang.Collections;

public class PolygonConverter extends GeometryModelConverter {
    
    public Object convert( Object value ) {
        if( value instanceof Polygon ) {
            return value;

        } if( Collections.isListable( value ) ) {
            return new Polygon( value );

        } else {
            Object converted = super.convert( value );
            if( converted instanceof Polygon ) {
                return converted;
            } else {
                return value;
            }
        }
    }
    
}