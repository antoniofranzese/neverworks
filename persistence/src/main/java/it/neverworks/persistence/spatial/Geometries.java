package it.neverworks.persistence.spatial;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.types.TypeDefinition;

public class Geometries {
    
    private final static TypeDefinition<GeometryModel> MODEL_TYPE = TypeFactory.build( GeometryModel.class, new GeometryModelConverter() );
    private final static TypeDefinition<com.vividsolutions.jts.geom.Geometry> JTS_TYPE = TypeFactory.build( com.vividsolutions.jts.geom.Geometry.class, new JTSGeometryConverter() );
    
    private static Integer srid;
    
    public static WKT WKT( Object text ) {
        return new WKT( text )
            .set( "srid", srid );
    }
    
    public static Point Point() {
        return new Point()
            .set( "srid", srid );
    }

    public static Point Point( Object x, Object y ) {
        return new Point( x, y )
            .set( "srid", srid );
    }

    public static Point Point( Object x, Object y, Object z ) {
        return new Point( x, y, z )
            .set( "srid", srid );
    }

    public static Point Point( com.vividsolutions.jts.geom.Point point ) {
        return new Point( point );
    }

    public static Polygon Polygon() {
        return new Polygon()
            .set( "srid", srid );
    }

    public static Polygon Polygon( Object shell ) {
        return new Polygon( shell )
            .set( "srid", srid );
    }
    
    public static <T extends GeometryModel> T Model( Object value ) {
        return (T) MODEL_TYPE.process( value );
    }
    
    public static <T extends com.vividsolutions.jts.geom.Geometry> T JTS( Object value ) {
        return (T) JTS_TYPE.process( value );
    }
    
    public static Integer getDefaultSrid() {
        return srid;
    }

    public static Integer SRID() {
        return srid;
    }
    
    public void setDefaultSrid( Integer defaultSrid ) {
        srid = defaultSrid;
    }
    
}