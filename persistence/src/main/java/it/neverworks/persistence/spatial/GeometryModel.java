package it.neverworks.persistence.spatial;

import com.vividsolutions.jts.geom.Geometry;
import org.geotools.geometry.jts.JTS;
import org.opengis.referencing.operation.MathTransform;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.model.CoreModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.Convert;

@Convert( GeometryModelConverter.class )
public abstract class GeometryModel extends CoreModel {
    
    @Property
    protected Integer srid;
    
    public abstract Geometry toJTSGeometry();
    
    public GeometryModel transform( MathTransform transform ) {
        throw new UnsupportedFeatureException( "Cannot transform " + this.getClass().getSimpleName() + ", feature is not supported" );
    }
    
    public Geometry transformJTS( MathTransform transform ) {
        try {
            return JTS.transform( this.toJTSGeometry(), transform );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public String toWKT() {
        return toJTSGeometry().toText();
    }
    
    public String getWkt() {
        return toWKT();
    }

    public String getEwkt() {
        return (this.srid != null ? "SRID=" + this.srid +"; " : "" ) + toWKT();
    }
    
    protected GeometryModel() {
        super();
        set( "srid", Geometries.getDefaultSrid() );
    }
    
    public <T extends GeometryModel> T set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }

    public <T extends GeometryModel> T set( Arguments arguments ) {
        for( String name: arguments.keys() ) {
            this.set( name, arguments.get( name ) );
        }
        return (T)this;
    }

    @Virtual
    public Point getCentroid() {
        Point centroid = new Point( this.toJTSGeometry().getCentroid() );
        centroid.setSrid( this.getSrid() );
        return centroid;
    }
    

    @Virtual
    public Double getWidth(){
        return this.toJTSGeometry().getEnvelopeInternal().getWidth();
    }

    @Virtual
    public Double getHeight(){
        return this.toJTSGeometry().getEnvelopeInternal().getHeight();
    }

    @Virtual
    public Integer getSrid(){
        return this.srid;
    }
    
    public void setSrid( Integer srid ){
        this.srid = srid;
    }

}