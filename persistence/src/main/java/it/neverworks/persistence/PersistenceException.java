package it.neverworks.persistence;

public class PersistenceException extends RuntimeException {
    
    public PersistenceException( String message ) {
        super( message );
    }

    public PersistenceException( String message, Throwable cause ) {
        super( message, cause );
    }

}