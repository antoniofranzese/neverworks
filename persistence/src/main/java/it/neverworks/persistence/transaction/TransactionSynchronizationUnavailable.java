package it.neverworks.persistence.transaction;

public class TransactionSynchronizationUnavailable extends RuntimeException {
    public TransactionSynchronizationUnavailable( String message ) {
        super( message );
    }
}