package it.neverworks.persistence.transaction;

import java.util.Map;
import java.io.Closeable;
import java.io.IOException;

import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import it.neverworks.lang.Guard;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Dispatcher;

public class Transaction implements Closeable, Guard<Transaction> {

    private PlatformTransactionManager platform;
    private TransactionStatus actual;
    private Arguments arguments;

    public Transaction( Arguments arguments ) {
        this.arguments = arguments;
    }
    
    public Transaction() {
        this( new Arguments() );
    }
    
    /* package */ Transaction( PlatformTransactionManager platform, TransactionStatus actual ) {
        this.platform = platform;
        this.actual = actual;
    }
    
    public Transaction begin() {
        if( this.actual == null ) {
            Transaction tx = TransactionManager.begin( this.arguments );
            this.actual = tx.actual;
            this.platform = tx.platform;
        }
        return this;
    }

    public void commit() {
        check();
        platform.commit( actual );
    }
    
    public void rollback() {
        check();
        if( ! actual.isCompleted() ) {
            platform.rollback( actual );
        }
    }
    
    public void close() throws IOException {
        check();
        if( ! actual.isCompleted() ) {
            commit();
        }
    }

    public boolean isCompleted() {
        check();
        return actual.isCompleted();
    }
    
    public RuntimeException rollback( Throwable error ) {
        check();
        if( !actual.isCompleted() ) {
            Exception ex = Errors.unwrap( error );
            //TODO: ignored exceptions
            if( TransactionSynchronizationManager.hasResource( TransactionException.class ) ) {
                TransactionSynchronizationManager.unbindResource( TransactionException.class );
            } else {
                TransactionSynchronizationManager.registerSynchronization( new TransactionErrorCleaner() );
            }

            TransactionSynchronizationManager.bindResource( TransactionException.class, ex );
            
            platform.rollback( actual );
        }
		return Errors.wrap( error );
    }

    public Transaction enter() {
        return this.begin();
    }
    
    public Throwable exit( Throwable error ) {
        if( error != null ) {
            return this.rollback( error );
        } else {
            this.commit();
            return null;
        }
    }
    
    protected void check() {
        if( this.actual == null ) {
            throw new IllegalStateException( "Transaction is not begun" );
        }
        if( this.platform == null ) {
            throw new IllegalStateException( "Transaction is detached" );
        }
    }
    
    public Transaction on( Arguments arguments ) {
        Transactions.on( arguments );
        return this;
    }

    public Transaction on( Arguments arguments, Map<String, Object> attributes ) {
        Transactions.on( arguments, attributes );
        return this;
    }
    
    public Transaction on( String event, Dispatcher dispatcher ) {
        Transactions.on( event, dispatcher );
        return this;
    }

    public Transaction on( String event, Dispatcher dispatcher, Map<String, Object> attributes ) {
        Transactions.on( event, dispatcher, attributes );
        return this;
    }
    
}
