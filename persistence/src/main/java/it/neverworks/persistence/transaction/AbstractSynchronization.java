package it.neverworks.persistence.transaction;

import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.core.Ordered;

public abstract class AbstractSynchronization implements TransactionSynchronization, Ordered {

    public void afterCommit() {

    }
    
    public void afterCompletion( int status ) {

    }
    
    public void beforeCommit( boolean readOnly ) {

    }
    
    public void beforeCompletion() {

    }
    
    public void resume() {

    }
    
    public void suspend() {

    }
    
    public int getOrder() {
        return 1;
    }
}