package it.neverworks.persistence.transaction;

public enum TransactionStatus {
    WORKING,
    COMMIT,
    HIT,
    FAIL,
    UNKNOWN
}