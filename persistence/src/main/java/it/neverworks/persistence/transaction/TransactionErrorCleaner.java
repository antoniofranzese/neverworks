package it.neverworks.persistence.transaction;

import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class TransactionErrorCleaner extends TransactionSynchronizationAdapter {
    
    public void afterCompletion( int status ) {
        if( TransactionSynchronizationManager.hasResource( TransactionException.class ) ) {
            TransactionSynchronizationManager.unbindResource( TransactionException.class );
        }        
    }
    
}