package it.neverworks.persistence.transaction;

import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;

import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import it.neverworks.lang.Arguments;
import it.neverworks.context.Context;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.types.TypeDefinition;

public class TransactionManager {
    
    private static PlatformTransactionManager manager;
    private final static TypeDefinition<String> StringType = TypeFactory.build( String.class );
    private final static TypeDefinition<Propagation> PropagationType = TypeFactory.build( Propagation.class );
    private final static TypeDefinition<Isolation> IsolationType = TypeFactory.build( Isolation.class );
    private final static TypeDefinition<TransactionEvents> SynchronizerType = TypeFactory.build( TransactionEvents.class );
    
    public static Transaction begin() {
        return begin( new Arguments() );
    }

    public static Transaction begin( Arguments arguments ) {
        arguments.restrict( "name", "propagation", "isolation" );
        
        DefaultTransactionDefinition def = new DefaultTransactionDefinition();
        
        def.setName( arguments.<String>get( StringType, "name", null ) );
        
        switch( arguments.get( PropagationType, "propagation", Propagation.REQUIRED ) ) {
            case REQUIRED:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_REQUIRED );
                break;
            case REQUIRES_NEW:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_REQUIRES_NEW );
                break;
            case MANDATORY:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_MANDATORY );
                break;
            case NESTED:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_NESTED );
                break;
            case SUPPORTED:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_SUPPORTS );
                break;
            case NOT_SUPPORTED:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_NOT_SUPPORTED );
                break;
            case NEVER:
                def.setPropagationBehavior( TransactionDefinition.PROPAGATION_NEVER );
                break;

        }
        
        switch( arguments.get( IsolationType, "isolation", Isolation.DEFAULT ) ) {
            case DEFAULT:
                def.setIsolationLevel( TransactionDefinition.ISOLATION_DEFAULT );
                break;
            case READ_COMMITTED:
                def.setIsolationLevel( TransactionDefinition.ISOLATION_READ_COMMITTED );
                break;
            case READ_UNCOMMITTED:
                def.setIsolationLevel( TransactionDefinition.ISOLATION_READ_UNCOMMITTED );
                break;
            case REPEATABLE_READ:
                def.setIsolationLevel( TransactionDefinition.ISOLATION_REPEATABLE_READ );
                break;
            case SERIALIZABLE:
                def.setIsolationLevel( TransactionDefinition.ISOLATION_SERIALIZABLE );
                break;
            
        }
        
        org.springframework.transaction.TransactionStatus tx = manager().getTransaction( def );
        
        if( ! TransactionSynchronizationManager.hasResource( SynchronizationStatus.class ) ) {
            SynchronizationStatus sync = new SynchronizationStatus();
            TransactionSynchronizationManager.bindResource( SynchronizationStatus.class, sync );
            TransactionSynchronizationManager.registerSynchronization( sync );
            TransactionSynchronizationManager.registerSynchronization( new SynchronizationCleanup() );
        }
        
        return new Transaction( manager(), tx );
    }
    
    public static void commit( Transaction transaction ) {
        transaction.commit();
    }
    
    public static void rollback( Transaction transaction ) {
        transaction.rollback();
    }

    public static void rollback( Transaction transaction, Throwable error ) {
        throw transaction.rollback( error );
    }

    public static void sync( Object input ) {
        sync( SynchronizerType.process( input ) );
    }
    
    public static void sync( TransactionSynchronizer sync ) {
		if( TransactionSynchronizationManager.isSynchronizationActive() ){
			TransactionSynchronizationManager.registerSynchronization( new TransactionSynchronizationAdapter( sync ) );
		} else {
			throw new TransactionSynchronizationUnavailable( "Transaction synchronization is not active" );
		}
    }
    
    private static PlatformTransactionManager manager() {
        if( TransactionManager.manager == null ) {
            try {
                TransactionManager.manager = Context.get( PlatformTransactionManager.class );
            } catch( Exception ex ){}
        }
        
        if( TransactionManager.manager != null ) {
            return TransactionManager.manager;
        } else {
            throw new RuntimeException( "Application-wide TransactionManager is not initialized" );
        }
    }
    
    public void setManager( PlatformTransactionManager aManager ) {
        TransactionManager.manager = aManager;
    }
    

    public static TransactionStatus status() {
        return TransactionSynchronizationManager.hasResource( SynchronizationStatus.class )
            ? ((SynchronizationStatus) TransactionSynchronizationManager.getResource( SynchronizationStatus.class )).status
            : TransactionStatus.UNKNOWN;
    }
    
    public static Throwable error() {
        return TransactionSynchronizationManager.hasResource( TransactionException.class ) ? (Throwable) TransactionSynchronizationManager.getResource( TransactionException.class ) : null;
    }
    
    private static class SynchronizationStatus extends AbstractSynchronization {

        private TransactionStatus status = TransactionStatus.WORKING;

        public void afterCompletion( int status ) {
            if( this.status == TransactionStatus.COMMIT ) {
                this.status = TransactionStatus.HIT;
            } else {
                this.status = TransactionStatus.FAIL;
            }
        }
    
        public void beforeCommit( boolean readOnly ) {
            this.status = TransactionStatus.COMMIT;
        }
    
        public int getOrder() {
            return 0;
        }
    
    }

    private static class SynchronizationCleanup extends AbstractSynchronization {

        public void afterCompletion( int status ) {
            TransactionSynchronizationManager.unbindResource( SynchronizationStatus.class );
        }
    
        public int getOrder() {
            return Integer.MAX_VALUE;
        }
    
    }
    
}