package it.neverworks.persistence.transaction;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.EnumConverter;

@Convert( EnumConverter.class )
public enum Isolation {
    DEFAULT 
    ,READ_COMMITTED 
    ,READ_UNCOMMITTED 
    ,REPEATABLE_READ 
    ,SERIALIZABLE 
}