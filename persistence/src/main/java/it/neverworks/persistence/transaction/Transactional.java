package it.neverworks.persistence.transaction;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.MethodAnnotation;

@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@MethodAnnotation( processor = TransactionalInterceptor.class )
public @interface Transactional {
    Propagation propagation() default Propagation.REQUIRED;
    Isolation isolation() default Isolation.DEFAULT;
    String name() default "";
}
