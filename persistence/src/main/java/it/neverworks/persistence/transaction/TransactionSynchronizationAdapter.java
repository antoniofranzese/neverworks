package it.neverworks.persistence.transaction;

import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class TransactionSynchronizationAdapter extends AbstractSynchronization {

    protected TransactionSynchronizer synchronizer;
    protected int order;
    
    public TransactionSynchronizationAdapter( TransactionSynchronizer synchronizer ) {
        this.synchronizer = synchronizer;
        int o = synchronizer.order();
        this.order = o >= 1 ? o : 1;
    }
    
    public void afterCommit() {
        synchronizer.commit();
    }
    
    public void afterCompletion( int status ) {
        if( TransactionManager.status() == TransactionStatus.HIT ) {
            synchronizer.hit();
        } else {
            synchronizer.fail( TransactionManager.error() );
        }
    }
    
    public void beforeCommit( boolean readOnly ) {
        synchronizer.committing();  
    }
    
    public void beforeCompletion() {
        if( TransactionManager.status() == TransactionStatus.COMMIT ) {
            synchronizer.hitting();
        } else {
            synchronizer.failing( TransactionManager.error() );
        }
    }
    
    public int getOrder() {
        return this.order;
    }
    
    
}