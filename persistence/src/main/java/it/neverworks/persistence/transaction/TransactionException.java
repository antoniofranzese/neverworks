package it.neverworks.persistence.transaction;

public class TransactionException extends RuntimeException {
    
    public TransactionException() {
        super();
    }
    
    public TransactionException( String message ) {
        super( message );
    }

    public TransactionException( String message, Throwable cause ) {
        super( message, cause );
    }

}