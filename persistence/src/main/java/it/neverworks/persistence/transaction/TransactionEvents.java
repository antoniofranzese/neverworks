package it.neverworks.persistence.transaction;

import java.util.Map;

import static it.neverworks.language.*;
import it.neverworks.persistence.transaction.TransactionSynchronizer;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.events.Signal;

@Convert( ModelConverter.class )
public class TransactionEvents extends BaseModel implements TransactionSynchronizer {

    @Property
    private Signal<TransactionEvent> committing;

    @Property
    private Signal<TransactionEvent> commit;

    @Property
    private Signal<TransactionEvent> hitting;

    @Property
    private Signal<TransactionEvent> hit;

    @Property
    private Signal<TransactionEvent> failing;

    @Property
    private Signal<TransactionEvent> fail;
    
    @Property
    private Map<String, Object> attributes;
    
    public void commit() {
        if( commit != null ) commit.fire( arg( "status", TransactionStatus.COMMIT ).arg( "attributes", attributes ) );
    }

    public void committing() {
        if( committing != null ) committing.fire( arg( "status", TransactionStatus.COMMIT ).arg( "attributes", attributes ) );
    }
    
    public void hit() {
        if( hit != null ) hit.fire( arg( "status", TransactionStatus.HIT ).arg( "attributes", attributes ) );
    } 
    
    public void hitting() {
        if( hitting != null ) hitting.fire( arg( "status", TransactionStatus.HIT ).arg( "attributes", attributes ) );
    } 
    
    public void fail( Throwable error ) {
        if( fail != null ) fail.fire( arg( "status", TransactionStatus.FAIL ).arg( "error", error ).arg( "attributes", attributes ) );
    }
    
    public void failing( Throwable error ) {
        if( failing != null ) failing.fire( arg( "status", TransactionStatus.FAIL ).arg( "error", error ).arg( "attributes", attributes ) );
    }
    
    public int order() {
        return 1;
    }
    
    public Signal<TransactionEvent> getFail(){
        return this.fail;
    }
    
    public void setFail( Signal<TransactionEvent> fail ){
        this.fail = fail;
    }
    
    public Signal<TransactionEvent> getFailing(){
        return this.failing;
    }
    
    public void setFailing( Signal<TransactionEvent> failing ){
        this.failing = failing;
    }
    
    public Signal<TransactionEvent> getHit(){
        return this.hit;
    }
    
    public void setHit( Signal<TransactionEvent> hit ){
        this.hit = hit;
    }
    
    public Signal<TransactionEvent> getHitting(){
        return this.hitting;
    }
    
    public void setHitting( Signal<TransactionEvent> hitting ){
        this.hitting = hitting;
    }
    
    public Signal<TransactionEvent> getCommit(){
        return this.commit;
    }
    
    public void setCommit( Signal<TransactionEvent> commit ){
        this.commit = commit;
    }
    
    public Signal<TransactionEvent> getCommitting(){
        return this.committing;
    }
    
    public void setCommitting( Signal<TransactionEvent> committing ){
        this.committing = committing;
    }

    
    
}