package it.neverworks.persistence.transaction;

import java.lang.reflect.Method;
import java.util.Set;

import it.neverworks.log.Logger;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

class TransactionalInterceptor implements Interceptor, MethodProcessor {
    
    private final static Logger logger = Logger.of( TransactionalInterceptor.class );
    
    private String name;
    private Propagation propagation;
    private Isolation isolation;
    private Set<Class<? extends Throwable>> ignores;
    private Method frontMethod;
    
    public TransactionalInterceptor() {
        this.propagation = Propagation.REQUIRED;
        this.isolation = Isolation.DEFAULT;
        this.name = "";
    }

    public TransactionalInterceptor( AnnotationInfo<Transactional> info ) {
        this.propagation = info.annotation().propagation();
        this.isolation = info.annotation().isolation();
        this.name = Strings.hasText( info.annotation().name() ) ? info.annotation().name().trim() : null;
    }
    
    public void processMethod( MethodDescriptor method ) {
        method.place( this ).first();
        this.frontMethod = method.front();
    }

    public Object invoke( Invocation invocation ) {
        if( logger.isDebugEnabled() ) {
            if( frontMethod != null ) {
                logger.debug( "Requesting transaction begin [{}] for {}.{}", name, frontMethod.getDeclaringClass().getName(), frontMethod.getName() );
            } else {
                logger.debug( "Requesting transaction begin [{}]", name );
            }
        }

        Transaction tx = null;
        try {
            tx = TransactionManager.begin( 
                arg( "name", name )
                .arg( "propagation", propagation )
                .arg( "isolation", isolation ) 
            );
            
            Object result = invocation.invoke();

            if( logger.isDebugEnabled() ) {
                if( frontMethod != null ) {
                    logger.debug( "Requesting transaction commit [{}] for {}.{}", name, frontMethod.getDeclaringClass().getName(), frontMethod.getName() );
                } else {
                    logger.debug( "Requesting transaction commit [{}]", name );
                }
            }

            tx.commit();
            return result;

        } catch( Exception ex ) {
            //TODO: eccezioni da ignorare
            
            if( logger.isDebugEnabled() ) {
                if( frontMethod != null ) {
                    logger.debug( "Requesting transaction rollback [{}] for {}.{}", name, frontMethod.getDeclaringClass().getName(), frontMethod.getName() );
                } else {
                    logger.debug( "Requesting transaction rollback [{}]", name );
                }
            }

            if( tx != null ) {
                throw tx.rollback( ex );
            } else {
                throw Errors.wrap( ex );
            }
        }

    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public void setPropagation( Propagation propagation ){
        this.propagation = propagation;
    }
    
    public void setIsolation( Isolation isolation ){
        this.isolation = isolation;
    }
    
}