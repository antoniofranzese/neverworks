package it.neverworks.persistence.transaction;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.EnumConverter;

@Convert( EnumConverter.class )
public enum Propagation {
    REQUIRED
    ,REQUIRES_NEW
    ,MANDATORY
    ,NESTED
    ,SUPPORTED
    ,NOT_SUPPORTED
    ,NEVER
}