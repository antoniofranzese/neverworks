package it.neverworks.persistence.transaction;

import java.util.Map;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Dispatcher;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class Transactions {
    
    public static void on( Arguments arguments ) {
        on( arguments, null );
    }

    public static void on( Arguments arguments, Map<String, Object> attributes ) {
        if( attributes == null && arguments.has( Map.class, "attributes" ) ) {
            attributes = arguments.<Map<String, Object>>get( "attributes" );
            arguments.remove( "attributes" );
        }
        
        TransactionManager.sync( new TransactionEvents()
            .set( arguments )
            .set( "attributes", attributes )
        );
    }
    
    public static void on( String event, Dispatcher dispatcher ) {
        on( event, dispatcher, null );
    }

    public static void on( String event, Dispatcher dispatcher, Map<String, Object> attributes ) {
        TransactionManager.sync( new TransactionEvents()
            .set( event, dispatcher )
            .set( "attributes", attributes ) 
        );
    }
    
    public static Transaction begin() {
        return TransactionManager.begin();
    }

    public static Transaction begin( Arguments arguments ) {
        return TransactionManager.begin( arguments );
    }

    public static boolean active() {
		return TransactionSynchronizationManager.isSynchronizationActive();
    }

    public static TransactionStatus status() {
		return TransactionManager.status();
    }

    public static Throwable error() {
		return TransactionManager.error();
    }

}