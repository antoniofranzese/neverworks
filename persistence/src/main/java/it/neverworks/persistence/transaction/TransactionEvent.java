package it.neverworks.persistence.transaction;

import java.util.Map;
import it.neverworks.model.events.Event;

public class TransactionEvent<T> extends Event<T> {

    private Throwable error;
    private TransactionStatus status = TransactionStatus.WORKING;
   
    public TransactionStatus getStatus(){
        return this.status;
    }
    
    public void setStatus( TransactionStatus status ){
        this.status = status;
    }

    public Throwable getError(){
        return this.error;
    }
    
    public void setError( Throwable error ){
        this.error = error;
    }
    
    
}