package it.neverworks.persistence.transaction;

public interface TransactionSynchronizer {

    public void committing();
    public void commit();
    
    public void hitting();
    public void hit();

    public void failing( Throwable error );
    public void fail( Throwable error );
    
    public int order();
}