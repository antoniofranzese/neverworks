package it.neverworks.persistence.sql;

public class InsertBuilder extends QueryBuilder<InsertBuilder>{

	public InsertBuilder() {
		super();
	}
	
	@Override
	public String getQuery() {
		if( fields.size() > 0 ) {
			StringBuilder query = new StringBuilder(1000);
			
			query.append( "INSERT INTO " )
				.append( tableName )
				.append( " (" );
			
			boolean first = true;
			for( String field: fieldNames ) {
				if( first ) {
					first = false;
				} else {
					query.append( ", " );
				}
				query.append( field );
			}
			query.append( ") VALUES (" );
	
			first = true;
			for( String param: fieldNames ) {
				if( first ) {
					first = false;
				} else {
					query.append( ", " );
				}
				query
					.append( ":" )
					.append( prefix )
					.append( param )
					.append( suffix );
			}
			query.append( ")" );
			
			return query.toString();
		} else {
			return "";
		}
	}
	
	@Override
	public String getFullQuery() {
		if( fields.size() > 0 ) {
			StringBuilder query = new StringBuilder(3000);
			StringBuilder values = new StringBuilder(2000);
			query.append( "INSERT INTO " )
				.append( tableName )
				.append( " (" );
			values.append( " VALUES (" );
			boolean first = true;
			for( String field: fieldNames ) {
				Object value = fields.get( field );  
				if ( value == null && !exportNulls ) {
					continue;
				} 
				
				if( first ) {
					first = false;
				} else {
					query.append( ", " );
					values.append( ", ");
				}			
				values.append( getValue( value ) );
				query.append( field );
			}
			values.append( ")" );
			query.append( ")" ).append( values );
			return query.toString();
		} else {
			return "";
		}
	}
	
}
