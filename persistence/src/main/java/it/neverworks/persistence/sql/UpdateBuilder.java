package it.neverworks.persistence.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UpdateBuilder extends QueryBuilder<UpdateBuilder> {

	private List<String> whereOnlyClause =  new ArrayList<String>();
	
	@Override
	public String getQuery() {
		if( fields.size() > 0 ) {
			StringBuilder query = new StringBuilder(1000);
			
			query.append( "UPDATE " )
				.append( tableName )
				.append( " SET " );
			
			boolean first = true;
			for( String field: fields.keySet() ) {
				if ( whereOnlyClause.contains( field ) ) {
					continue;
				}
				if( first ) {
					first = false;
				} else {
					query.append( ", " );
				}
				query.append( field )
					.append( " = " )
					.append( prefix )
					.append( field )
					.append( suffix );
			}
			first = true;
			for ( String whereField: whereOnlyClause ) {
				if( first ) {
					query.append( " WHERE " );
					first = false;
				} else {
					query.append( " AND " );
				}
				query.append( whereField )
					.append( " = " )
					.append( prefix )
					.append( whereField )
					.append( suffix );
			}
			return query.toString();
		} else {
			return "";
		}
	}

	@Override
	public String getFullQuery() {
		if( fields.size() > 0 ) {
			StringBuilder query = new StringBuilder(3000);
			query.append( "UPDATE " )
			.append( tableName )
			.append( " SET " );
			boolean first = true;
			for( String field: fields.keySet() ) {
				if ( whereOnlyClause.contains( field ) ) {
					continue;
				}
				if( first ) {
					first = false;
				} else {
					query.append( ", " );
				}
				query.append( field )
					.append( " = " )
					.append( getValue( fields.get( field ) ) );
			}
			first = true;
			for ( String whereField: whereOnlyClause ) {
				if( first ) {
					query.append( " WHERE " );
					first = false;
				} else {
					query.append( " AND " );
				}
				query.append( whereField )
					.append( " = " )
					.append( getValue( fields.get( whereField ) ) );
			}
			return query.toString();
		} else {
			return "";
		}
	}

	public UpdateBuilder withWhereOnly( String... param ) {
		if ( param != null && param.length > 0 ) {
			whereOnlyClause.addAll( Arrays.asList( param ) );
		}
		return this;
	}
}
