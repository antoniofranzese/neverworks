package it.neverworks.persistence.sql;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;

public abstract class QueryBuilder<T> implements Map<String, Object> {
	protected Map<String, Object> fields = new HashMap<String, Object>();
	protected List<String> fieldNames = new ArrayList<String>();
	protected String tableName;
	protected String prefix = "";
	protected String suffix = "";
	
	protected String booleanTrue="'Y'";

	protected String booleanFalse="'N'";
    
    protected boolean exportNulls = false;

	protected static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat( "dd/MM/yyyy HH:mm:ss" );

	public abstract String getQuery();
	public abstract String getFullQuery();
	
	public T withTable( String name ) {
		this.tableName = name;
		return (T)this;
	}
	
	public T withPrefix( String prefix ) {
		this.prefix = prefix;
		return (T)this;
	}
	
	public T withSuffix( String suffix ) {
		this.suffix = suffix;
		return (T)this;
	}
	
	public T withBooleanTrue( String booleanTrue ) {
		this.booleanTrue = booleanTrue;
		return (T)this;
	}

	public T withBooleanFalse( String booleanFalse ) {
		this.booleanFalse = booleanFalse;
		return (T)this;
	}
    
    public T withNulls() {
        this.exportNulls = true;
        return (T)this;
    }

	public T set( String name, Object value ) {
		if( value != null || exportNulls ) {
			fields.put( name, value );
            fieldNames.add( name );
		}
		return (T)this;
	}

	public Map<String, Object> getParameters() {
		return fields;
	}
	
	@Override
	public void clear() {
		fields.clear();
	}

	@Override
	public boolean containsKey(Object key) {
		return fields.containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		return fields.containsValue(value);
	}

	@Override
	public Set<java.util.Map.Entry<String, Object>> entrySet() {
		throw unimplemented();
	}

	@Override
	public Object get(Object key) {
		return fields.get( key );
	}

	@Override
	public boolean isEmpty() {
		return fields.isEmpty();
	}

	@Override
	public Set<String> keySet() {
		return fields.keySet();
	}

	@Override
	public Object put(String key, Object value) {
		this.set(key, value);
		return value;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		for( String key: m.keySet() ) {
			this.set( key, m.get( key ) );
		}
	}

	@Override
	public Object remove(Object key) {
		return fields.remove(key);
	}

	@Override
	public int size() {
		return fields.size();
	}

	@Override
	public Collection<Object> values() {
		throw unimplemented();
	}

	private RuntimeException unimplemented() {
		return new RuntimeException("Unimplemented Map Method");
	}

	protected Object getValue( Object value ) {
		if ( value == null) {
			return "NULL"; 
		} else if (value instanceof Collection) {
			throw new RuntimeException("Collection is not implemented");
		} else if (value instanceof Boolean) {
			return ((Boolean)value)? booleanTrue: booleanFalse;
		} else if (value instanceof Calendar) {
			return"TO_DATE '" + DATE_FORMATTER.format( ((Calendar)value).getTime() ) + "', 'DD/MM/YYYY HH24:mi:ss')";
		} else if (value instanceof Date) {
			return"TO_DATE( '" + DATE_FORMATTER.format( (Date)value) + "', 'DD/MM/YYYY HH24:mi:ss')";
		} else if (value instanceof BigDecimal) {
			return (BigDecimal) value ;
		} else if (value instanceof BigInteger) {
			return new BigDecimal( (BigInteger) value );
		} else if (value instanceof Double) {
			return new BigDecimal( (Double) value);
		} else if (value instanceof Float) {
			return new BigDecimal( (Float) value);
		} else if (value instanceof Long) {
			return new BigDecimal( (Long) value);
		} else if (value instanceof Integer) {
			return new BigDecimal( (Integer) value);
		} else if (value instanceof Short) {
			return new BigDecimal( (Short) value);
		} else if (value instanceof Byte) {
			return new BigDecimal( (Byte) value) ;
		} else {
			return "'" + value.toString().replace( "'", "''" ) + "'" ;
		}
	} 
	
}
