package it.neverworks.persistence;

import it.neverworks.lang.Arguments;
import it.neverworks.persistence.criteria.Finder;

public class CriteriaEvent extends PersistenceEvent {
    
    protected Finder finder;
    protected Class target;
    protected Arguments arguments;
    
    public Arguments getArguments(){
        return this.arguments;
    }
    
    public void setArguments( Arguments arguments ){
        this.arguments = arguments;
    }
    
    public Class getTarget(){
        return this.target;
    }
    
    public void setTarget( Class target ){
        this.target = target;
    }
    
    public <F> Finder<F> getFinder(){
        return (Finder<F>) this.finder;
    }
    
    public void setFinder( Finder finder ){
        this.finder = finder;
    }
}