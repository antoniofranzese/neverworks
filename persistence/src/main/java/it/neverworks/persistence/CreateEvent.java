package it.neverworks.persistence;

import it.neverworks.lang.Arguments;
import it.neverworks.persistence.criteria.Finder;

public class CreateEvent extends PersistenceEvent {
    
    protected Class target;
    protected Arguments arguments;

    public Arguments getArguments(){
        return this.arguments;
    }
    
    public void setArguments( Arguments arguments ){
        this.arguments = arguments;
    }
    
    public Class getTarget(){
        return this.target;
    }
    
    public void setTarget( Class target ){
        this.target = target;
    }
    
}