package it.neverworks.persistence;

public interface PersistenceEventFactory {
    
    public PersistenceEvent build();
}