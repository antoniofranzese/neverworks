package it.neverworks.persistence;

public interface EntityTypeAware {
    public void setEntityType( Class type );
}