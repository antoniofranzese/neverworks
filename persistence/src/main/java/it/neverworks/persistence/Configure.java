package it.neverworks.persistence;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Inherited;

@Inherited
@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@ClassHandler
public @interface Configure {
}
