package it.neverworks.persistence.entities;

import java.lang.annotation.Annotation;
import it.neverworks.model.features.Get;
import it.neverworks.model.features.Describe;

public interface EntityPropertyDescriptor extends Get, Describe {
    public enum Association { ONE_TO_MANY, MANY_TO_ONE, ONE_TO_ONE, MANY_TO_MANY }

    public Object convert( Object value );    
    public Class getType();
    public String getName();
    public EntityPropertyDescriptor set( String name, Object value );
    public <T extends Annotation> T annotation( Class<T> annotationType );
}