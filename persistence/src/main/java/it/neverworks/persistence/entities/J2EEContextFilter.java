package it.neverworks.persistence.entities;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import it.neverworks.httpd.InjectableFilter;
import it.neverworks.log.Logger;

public class J2EEContextFilter extends InjectableFilter {

    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws ServletException, IOException {
        EntityContext context = entities.open();
        try {
            chain.doFilter( request, response );
        } finally {
            context.close();
        }
    }
    
    private EntityManager entities;
    
    public EntityManager getEntities(){
        return this.entities;
    }
    
    public void setEntities( EntityManager entities ){
        this.entities = entities;
    }

}