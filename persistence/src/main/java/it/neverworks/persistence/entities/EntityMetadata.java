package it.neverworks.persistence.entities;

import it.neverworks.model.expressions.ObjectQuery;

public interface EntityMetadata<E> {
    public EntityPropertyDescriptor property( String path );
    public EntityPropertyDescriptor identifier();
    public ObjectQuery<EntityPropertyDescriptor> query();
    public <T> T get( String name );
    public EntityMetadata<E> set( String name, Object value );
    public Class type();
    public EntityPersister<E> persister();
    public EntityManager manager();

    default EntityPropertyDescriptor getIdentifier() {
        return this.identifier();
    }
    
    default Class getType() {
        return this.type();
    }
    
    default EntityPersister<E> getPersister() {
        return this.persister();
    }
    
    default EntityManager getManager() {
        return this.manager();
    }
    
}