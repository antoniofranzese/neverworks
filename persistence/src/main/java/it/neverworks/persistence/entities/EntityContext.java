package it.neverworks.persistence.entities;

import java.io.Closeable;
import it.neverworks.lang.Guard;

public interface EntityContext extends Closeable, Guard<EntityManager> {

    public void shutdown();
    public EntityManager manager();
    
    default EntityManager enter() {
        return manager();
    }
    
    default Throwable exit( Throwable error ) {
        shutdown();
        return error;
    }
    
}