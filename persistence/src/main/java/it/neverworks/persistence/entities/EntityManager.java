package it.neverworks.persistence.entities;

import java.util.Map;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.criteria.Finder;

public interface EntityManager {

    public <T> EntityPersister<T> on( Class<T> type );
    
	public <T> T create( Class<T> type, Object... params );
	public <T> T create( Class<T> type, Map params );

    public <T> Query<T> query( String body, Map params );
    public <T> Query<T> query( String body, Object... params );
    public <T> Query<T> query( String body );

    public <T> Query<T> query( Class<T> type, String body, Map params );
    public <T> Query<T> query( Class<T> type, String body, Object... params );
    public <T> Query<T> query( Class<T> type, String body );

    public <T> Finder<T> find( Class<T> type );
	public <T> T load( Class<T> type, Object key );
	public <T> T load( T example );
	public <T> T save( T entity );
	public <T> T insert( T entity );
	public <T> T update( T entity );
	public <T> T delete( T entity );
    public <T> EntityMetadata<T> metadata( Class<T> type );
    public <T> T forget( T entity );
    public <T> T refresh( T entity );
    public <T> T merge( T entity );
    public EntityContext open();
    public void reset();
    
    public EntityEngine engine();
    
}