package it.neverworks.persistence.entities;

import it.neverworks.lang.Errors;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;

public class EntityContextInterceptor extends BaseModel implements Interceptor {

    public Object invoke( Invocation invocation ) {
        EntityContext ctx = getEntities().open();
        
        try {
            return invocation.invoke();
        } finally {
            ctx.shutdown();
        }
    }
    
    @Property @Required @Inject( lazy = true )
    private EntityManager entities;
    
    public EntityManager getEntities(){
        return this.entities;
    }
    
    public void setEntities( EntityManager entities ){
        this.entities = entities;
    }
    
    public void setManager( EntityManager manager ){
        this.entities = manager;
    }
    
}