package it.neverworks.persistence.entities;

import java.util.Map;
import it.neverworks.persistence.entities.EntityMetadata;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.criteria.Finder;

public interface EntityPersister<T> {

	public T create( Object... params );
	public T create( Map params );

    public Query<T> query( String body, Map params );
    public Query<T> query( String body, Object... params );
    public Query<T> query( String body );

    public Finder<T> find();
	public T load( Object key );
	public void save( T entity );
	public Object insert( T entity );
	public void update( T entity );
	public void delete( T entity );
    public EntityMetadata metadata();
    public void forget( T entity );
    public T refresh( T entity );
    public T merge( T entity );
    
}