package it.neverworks.persistence.entities;

import java.util.Map;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Inspector;
import it.neverworks.context.Context;
import it.neverworks.model.events.Promise;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.PersistenceException;

public class Entities {
    
    private static EntityManager entityManager;
    private static Promise readyDeferred = new Promise( EntityManagerReadyEvent.class );
    
    public static Promise ready() {
        return readyDeferred;
    }

    public static <T> EntityPersister<T> on( Class<T> type ) {
        return manager().on( type );
    }

	public static <T> T create( Class<T> type, Object... params ) {
        return (T)manager().create( type, params );
    }
    
    public static <T> Finder<T> find( Class<T> type ) {
        return (Finder<T>)manager().find( type );
    }
    
    public static <T> Query<T> query( String name, Map params ) {
        return (Query<T>)manager().query( name, params );
    }
    
    public static <T> Query<T> query( String name, Object... params ) {
        return (Query<T>)manager().query( name, params );
    }
    
    public static <T> Query<T> query( String name) {
        return (Query<T>)manager().query( name );
    }
    
    public static <T> Query<T> query( Class<T> type, String body, Map params ) {
        return (Query<T>)manager().query( type, body, params );
    }
    
    public static <T> Query<T> query( Class<T> type, String body, Object... params ) {
        return (Query<T>)manager().query( type, body, params );
    }
    
    public static <T> Query<T> query( Class<T> type, String body ) {
        return (Query<T>)manager().query( type, body );
    }
    
	public static <T> T load( Class<T> type, Object key ) {
        return (T)manager().load( type, key );
    }
    
	public static <T> T load( T example ) {
        return (T)manager().load( example );
    }

	public static <T> T reload( T example ) {
        return (T)manager().load( example );
    }
    
	public static <T> T forget( T entity ) {
        return manager().forget( entity );
    }
 
 	public static <T> T save( T entity ) {
        return manager().save( entity );
    }
    
	public static <T> T insert( T entity ) {
        return manager().insert( entity );
    }
    
	public static <T> T update( T entity ) {
        return manager().update( entity );
    }
    
	public static <T> T delete( T entity ) {
        return manager().delete( entity );
    }

	public static <T> T merge( T entity ) {
        return manager().merge( entity );
    }

	public static <T> T refresh( T entity ) {
        return manager().refresh( entity );
    }

	public static void reset() {
        manager().reset();
    }
    
    public static EntityContext open() {
        return manager().open();
    }
    
    public static <T> EntityMetadata<T> metadata( Class<T> type ) {
        return manager().metadata( type );
    }
    
    public void setManager( EntityManager manager ){
        updateManager( manager );
    }

    private static void updateManager( EntityManager manager ){
        if( manager != entityManager ) {
            entityManager = manager;
            readyDeferred.resolve( new EntityManagerReadyEvent().set( "manager", manager ) );
        }
    }
    
    public static <T extends EntityManager> T manager() {
        if( entityManager == null ) {
            try {
                updateManager( Context.get( EntityManager.class ) );
            } catch( Exception ex ){}
        }
        
        if( entityManager != null ) {
            return (T) entityManager;
        } else {
            throw new PersistenceException( "Application-wide EntityManager is not configured" );
        }
    }
    
    public static EntityEngine engine() {
        return manager().engine();
    }
    /* Functionals */

    public static class LoadByExampleFunction<I,O> implements Mapper<I,O> {
        public O map( I source ) {
            return (O) Entities.load( source );
        }
    }

    public static <T> LoadByExampleFunction<T,T> load() {
        return new LoadByExampleFunction<T, T>();
    }

    public static class AutoDetectLoadFunction<I,O> implements Mapper<I,O> {
        private Class<O> target;
        
        public AutoDetectLoadFunction( Class<O> target ) {
            this.target = target;
        }
        
        public O map( I source ) {
            if( target.isInstance( source ) ) {
                return (O) Entities.load( source );
            } else {
                return (O) Entities.load( target, source );
            }
        }
    }

    public static <T,K> AutoDetectLoadFunction<K,T> load( Class<T> entity ) {
        return new AutoDetectLoadFunction<K, T>( entity );
    }

    public static class MergeFunction<I,O> implements Mapper<I,O> {
        public O map( I source ) {
            return (O) Entities.merge( source );
        }
    }
    
    public static <T> MergeFunction<T,T> merge() {
        return new MergeFunction<T, T>();
    }

    public static class RefreshFunction<I,O> implements Mapper<I,O>, Inspector<I> {
        public O map( I source ) {
            return (O) Entities.refresh( source );
        }
        
        public void inspect( I source ) {
            Entities.refresh( source );
        }
    }
    
    public static <T> RefreshFunction<T,T> refresh() {
        return new RefreshFunction<T, T>();
    }

    public static class SaveFunction<I,O> implements Mapper<I,O>, Inspector<I> {
        public O map( I source ) {
            Entities.save( source );
            return (O) source;
        }
        
        public void inspect( I source ) {
            Entities.save( source );
        }
    }
    
    public static <T> SaveFunction<T,T> save() {
        return new SaveFunction<T, T>();
    }

    public static class UpdateFunction<I,O> implements Mapper<I,O>, Inspector<I> {
        public O map( I source ) {
            Entities.update( source );
            return (O) source;
        }
        
        public void inspect( I source ) {
            Entities.update( source );
        }
    }
    
    public static <T> UpdateFunction<T,T> update() {
        return new UpdateFunction<T, T>();
    }

    public static class InsertFunction<I,O> implements Mapper<I,O>, Inspector<I> {
        public O map( I source ) {
            Entities.insert( source );
            return (O) source;
        }
        
        public void inspect( I source ) {
            Entities.insert( source );
        }
    }
    
    public static <T> InsertFunction<T,T> insert() {
        return new InsertFunction<T, T>();
    }

    public static class DeleteFunction<I,O> implements Mapper<I,O>, Inspector<I> {
        public O map( I source ) {
            Entities.delete( source );
            return (O) source;
        }
        
        public void inspect( I source ) {
            Entities.delete( source );
        }
    }
    
    public static <T> DeleteFunction<T,T> delete() {
        return new DeleteFunction<T, T>();
    }

    public static class ForgetFunction<I,O> implements Mapper<I,O>, Inspector<I> {
        public O map( I source ) {
            Entities.forget( source );
            return (O) source;
        }
        
        public void inspect( I source ) {
            Entities.forget( source );
        }
    }
    
    public static <T> ForgetFunction<T,T> forget() {
        return new ForgetFunction<T, T>();
    }

}