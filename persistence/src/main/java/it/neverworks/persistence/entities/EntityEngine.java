package it.neverworks.persistence.entities;

public enum EntityEngine {
    ORACLE
    ,PGSQL
    ,MYSQL
    ,UNKNOWN
}