package it.neverworks.persistence.entities;

import it.neverworks.model.Property;
import it.neverworks.model.events.Event;

public class EntityManagerReadyEvent extends Event {
    
    @Property
    private EntityManager manager;
    
    public EntityManager getManager(){
        return this.manager;
    }
    
    public void setManager( EntityManager manager ){
        this.manager = manager;
    }

}