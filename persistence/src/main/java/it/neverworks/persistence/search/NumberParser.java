package it.neverworks.persistence.search;

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;

public class NumberParser implements Parser {

    //TODO: separare le espressioni, e creare il caso n1,n2,...
    private final static Pattern REGEX = Pattern.compile( "([<>]?=?)([0-9\\.,]+)(-([0-9\\.,]*))?");
    
    public Token parse( String text ) {
        Matcher m = REGEX.matcher( text );
        if( m.matches() ) {
            Number number1 = Numbers.number( m.group( 2 ).replaceAll( ",", "." ) );
            Number number2 = null;

            if( Strings.hasText( m.group( 4 ) ) ) {
                number2 = Numbers.number( m.group( 4 ).replaceAll( ",", "." ) );
            }

            String op = m.group( 1 );
            return new NumberToken()
                .set( "text", text )
                .set( "operator", Strings.hasText( op ) && ! "=".equals( op ) ? op : null )
                .set( "number", number1 )
                .set( "ending", number2 );

        } else {
            return null;
        }
    }

}