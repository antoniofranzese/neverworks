package it.neverworks.persistence.search;

import java.util.List;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.Identification;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;

@Identification( string = { "path", "!alias", "inquirer" })
public class SearchPropertyDescriptor implements Model {
    
    @Property
    protected String path;
    
    @Property @Collection
    protected List<String> alias;

    @Property
    protected EntityPropertyDescriptor descriptor;

    @Property
    protected Inquirer inquirer;

    public Class getType() {
        return get( "!descriptor.type" );
    }
    
    public Inquirer inquirer(){
        return this.inquirer;
    }
    
    public EntityPropertyDescriptor descriptor(){
        return this.descriptor;
    }
    
    public List<String> alias(){
        return this.alias;
    }
    
    public String path(){
        return this.path;
    }

    
}