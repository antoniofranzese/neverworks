package it.neverworks.persistence.search;

import java.util.Date;
import it.neverworks.persistence.criteria.Finder;

public class CriteriaDialect implements Dialect {
    
    public void queryText( SearchCase search, String text ) {
        search.finder().or( propertyPath( search.property().path() ) ).contains( text );
    }

    public void queryNumber( SearchCase search, String operator, Number number ) {
        queryWithOperator( search.finder(), propertyPath( search.property().path() ), operator, number );
    }

    public void queryNumbers( SearchCase search, Number from, Number to ) {
        search.finder().or( propertyPath( search.property().path() ) ).between( from, to );
    }

    public void queryDate( SearchCase search, String operator, Date date ) {
        queryWithOperator( search.finder(), propertyPath( search.property().path() ), operator, date );
    }

    public void queryDates( SearchCase search, Date from, Date to ) {
        search.finder().or( propertyPath( search.property().path() ) ).between( from, to );
    }

    protected String propertyPath( String path ) {
        return path.indexOf( "." ) > 0 ? "+" + path : path;
    }

    protected void queryWithOperator( Finder finder, String path, String operator, Object value ) {
         if( operator == null ) {
            finder.or( path ).eq( value );

        } else if( ">=".equals( operator ) ) {
            finder.or( path ).ge( value );

        } else if( ">".equals( operator ) ) {
            finder.or( path ).gt( value );
            
        } else if( "<=".equals( operator ) ) {
            finder.or( path ).le( value );
            
        } else if( "<".equals( operator ) ) {
            finder.or( path ).lt( value );
        } 
    }

}