package it.neverworks.persistence.search;

import it.neverworks.lang.Numbers;

public class NumberInquirer implements Inquirer {

    public boolean claims( Token token ) {
        return token instanceof NumberToken;
    }
    
    public void process( SearchCase search ) {
        Class type = search.property().descriptor().get( "type" );
        Number number = type != null ? Numbers.convert( search.token().get( "number" ), type ) : search.token().get( "number" );

        if( search.token().get( "ending" ) != null ) {
            Number ending = type != null ? Numbers.convert( search.token().get( "ending" ), type ) : search.token().get( "ending" );

            if( Numbers.compare( number, ending ) > 0 ) {
                Number n = number;
                number = ending;
                ending = n;
            }

            search.dialect().queryNumbers( search, number, ending );

        } else {
            search.dialect().queryNumber( search, search.token().get( "operator" ), number );
        }

    }

}