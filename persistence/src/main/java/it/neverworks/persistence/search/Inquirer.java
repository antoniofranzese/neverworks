package it.neverworks.persistence.search;

import it.neverworks.model.Model;
import it.neverworks.persistence.criteria.Finder;

public interface Inquirer extends Model {
    boolean claims( Token token );
    void process( SearchCase search );
}