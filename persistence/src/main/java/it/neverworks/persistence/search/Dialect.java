package it.neverworks.persistence.search;

import java.util.Date;

public interface Dialect {
    
    void queryText( SearchCase search, String text );
    void queryNumber( SearchCase search, String operator, Number number );
    void queryNumbers( SearchCase search, Number from, Number to );
    void queryDate( SearchCase search, String operator, Date date );
    void queryDates( SearchCase search, Date from, Date to );

}