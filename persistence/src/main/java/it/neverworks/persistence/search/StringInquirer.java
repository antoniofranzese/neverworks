package it.neverworks.persistence.search;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.persistence.criteria.Finder;

public class StringInquirer implements Inquirer {

    @Property @AutoConvert
    protected boolean dates = false;

    @Property @AutoConvert
    protected boolean numbers = false;

    @Property @AutoConvert
    protected boolean hints = false;
    
    public boolean claims( Token token ) {
        return token instanceof StringToken 
            || ( dates && token instanceof DateToken ) 
            || ( numbers && token instanceof NumberToken && token.get( "operator" ) == null );
    }
    
    public void process( SearchCase search ) {
        String text = null;

        text = search.token().<String>get( "text" );

        if( hints ) {
            text = text.replaceAll( "\\+", " " );
        }
        
        search.dialect().queryText( search, text );
    }

    public String toString() {
        return new ToStringBuilder( this )
            .add( "hints" )
            .add( "dates" )
            .add( "numbers" )
            .toString();
    }

}