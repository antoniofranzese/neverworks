package it.neverworks.persistence.search;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Stack;
import java.util.Date;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.Identification;
import it.neverworks.persistence.PersistenceException;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.entities.Entities;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;

@Identification( string = { "type", "tokenizer" })
public class SearchDescriptor implements Model {

    //TODO: prendere dal contesto
    private final static Controller controller = new CriteriaController();
    
    public static SearchDescriptor of( Class type ) {
        return describe( type );
    }

    public static SearchDescriptor of( Object object ) {
        return describe( object.getClass() );
    }

    private final static Map<Class,SearchDescriptor> descriptions = new HashMap<>();

    @Property
    protected Class type;
    
    @Property @Collection
    protected List<SearchPropertyDescriptor> properties;

    @Property @Collection
    protected List<SearchEntityReference> references;

    @Property
    protected Tokenizer tokenizer;

    public SearchDescriptor add( SearchPropertyDescriptor property ) {
        this.<List>get( "properties" ).add( property );
        return this;
    }

    public SearchDescriptor add( SearchEntityReference reference ) {
        this.<List>get( "references" ).add( reference );
        return this;
    }

    public SearchPlan search( String text ) {
        return controller.plan( this, text );
    }
    
    public Tokenizer tokenizer(){
        return this.tokenizer;
    }

    public Controller controller(){
        return controller;
    }
    
    public Dialect dialect(){
        return controller.dialect();
    }

    public List<SearchPropertyDescriptor> properties(){
        if( this.references == null || this.references.size() == 0 ) {
            return this.properties;
        } else {
            return this.collectProperties( new Stack<Class>(), null, null );
        }
    }

    /* package */ List<SearchPropertyDescriptor> collectProperties( Stack<Class> visit, List<String> include, List<String> exclude ) {
        visit.push( type );
        List<SearchPropertyDescriptor> result = new ArrayList<>();

        if( this.properties != null ) {
            for( SearchPropertyDescriptor prop: this.properties ) {
                String path = prop.get( "path" );
               
                if( ( exclude == null || ! exclude.contains( path ) ) && ( include == null || include.contains( path ) ) ) {
                    result.add( prop );
                }
            }

        }

        if( this.references != null ) {
            for( SearchEntityReference ref: this.references ) {

                if( ! visit.contains( ref.target() )  ) {
                    String path = ref.get( "path" );

                    if( ( exclude == null || ! exclude.contains( path ) ) && ( include == null || include.contains( path ) ) ) {
                        for( SearchPropertyDescriptor prop: describe( ref.target() ).collectProperties( visit, ref.include(), ref.exclude() ) ) {
                            SearchPropertyDescriptor refMeta = new SearchPropertyDescriptor()
                                .set( "descriptor", prop.get( "descriptor" ) )
                                .set( "alias", prop.get( "alias" ) )
                                .set( "inquirer", prop.get( "inquirer" ) )
                                .set( "path", path + "." + prop.get( "path" ) );
 
                            if( ref.alias() != null && ref.alias().containsKey( prop.get( "path" ) ) ) {
                                refMeta.<List>get( "alias" ).add( ref.alias().get( prop.get( "path" ) ) );
                            }
 
                            result.add( refMeta );
                        }
                    }
                }
            }
        }
        visit.pop();
        return result;
    }
    
    public Class getType(){
        return this.type;
    }
    
    private static SearchDescriptor describe( Class type ) {
        return describe( type, new Stack<Class>() );
    }

    private static SearchDescriptor describe( Class type, Stack<Class> visit ) {
        if( visit.contains( type ) ) {
            List<String> names = Collections.each( visit ).map( cls -> cls.getSimpleName() ).list();
            names.add( type.getSimpleName() );
            throw new PersistenceException( "Searchable circular reference: " + Collections.join( names, "->" ) );
        
        } else {
            visit.push( type );

            SearchDescriptor result = descriptions.get( type );
            if( result == null ) {
                result = new SearchDescriptor().set( "type", type );

                Searchable searchable = (Searchable) type.getAnnotation( Searchable.class );
                if( searchable == null ) {
                    throw new PersistenceException( type.getName() + " is not searchable" );
                }

                Tokenizer tokenizer = null;

                if( Tokenizer.class.equals( searchable.tokenizer() ) ) {
                    tokenizer = controller.createTokenizer( type );
                } else {
                    tokenizer = Reflection.newInstance( searchable.tokenizer() );
                }

                if( Strings.hasText( searchable.setup() ) && Arguments.isParsable( searchable.setup() ) ) {
                    tokenizer.set( Arguments.parse( searchable.setup() ) );
                }

                result.set( "tokenizer", tokenizer );

                for( EntityPropertyDescriptor entityProperty:  (Iterable<EntityPropertyDescriptor>) Entities.metadata( type ).query() ) {
                    Search search = entityProperty.annotation( Search.class );
                    if( search != null ) {
                        if( entityProperty.get( "association" ) == null || ! Inquirer.class.equals( search.inquirer() ) ) {

                            SearchPropertyDescriptor propMeta = new SearchPropertyDescriptor()
                                .set( "descriptor", entityProperty );

                            List<String> alias = new ArrayList<>();

                            if( searchable.autoAlias() ) {
                                alias.add( entityProperty.<String>get( "name" ).toLowerCase() );
                            }

                            if( search.alias() != null && search.alias().length > 0 ) {
                                Collections.each( search.alias() )
                                    .filter( s -> Strings.hasText( s ) )
                                    .inspect( s -> alias.add( s.trim().toLowerCase() ) );
                            }

                            propMeta.set( "path", entityProperty.get( "name" ) );
                            propMeta.set( "alias", alias );
                            propMeta.set( "inquirer", controller.createInquirer( entityProperty ) );
                            result.add( propMeta );

                        } else {

                            Map<String,String> remaps = null;
                            if( search.alias() != null && search.alias().length > 0 ) {
                                remaps = Collections.each( search.alias() )
                                    .filter( s -> Strings.hasText( s ) && s.indexOf( "=" ) > 0 )
                                    .dict( s -> {
                                        int sep = s.indexOf( "=" );
                                        return pair( s.substring( sep + 1 ), s.substring( 0, sep ).toLowerCase() );
                                    });
                            }

                            SearchEntityReference ref = new SearchEntityReference()
                                .set( "path", entityProperty.get( "name" ) )
                                .set( "target", entityProperty.get( "target" ) )
                                .set( "alias", remaps )
                                .set( "include", search.include().length > 0 ? Collections.list( search.include() ) : null )
                                .set( "exclude", search.exclude().length > 0 ? Collections.list( search.exclude() ) : null );

                            result.add( ref );
                        }

                    }
                }

                result = controller.setup( result );
            }

            visit.pop();
            return result;
        }
    }

}