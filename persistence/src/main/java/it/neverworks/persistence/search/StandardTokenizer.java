package it.neverworks.persistence.search;

import java.util.List;

import it.neverworks.model.Model;
import it.neverworks.model.Property;

public class StandardTokenizer extends BaseTokenizer {

    public StandardTokenizer() {
        super();
        register( new NumberParser() );
        register( new DateParser() );
    }

 }