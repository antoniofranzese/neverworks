package it.neverworks.persistence.search;

import java.util.Date;
import it.neverworks.lang.Dates;

public class DateParser implements Parser {

    public Token parse( String text ) {
        try {
            Date date = Dates.date( text );
            return new DateToken()
                .set( "text", text )
                .set( "date", date );

        } catch( Exception ex ) {
            return null;
        }
    }

}