package it.neverworks.persistence.search;

public interface Parser {

    Token parse( String text );

}