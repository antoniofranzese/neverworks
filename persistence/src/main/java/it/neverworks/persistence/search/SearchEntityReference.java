package it.neverworks.persistence.search;

import java.util.Map;
import java.util.List;

import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.utils.Identification;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;

@Identification( string = { "path", "target" } )
public class SearchEntityReference implements Model {
    
    @Property
    protected String path;
    
    @Property
    protected Class target;

    @Property
    protected List<String> include;

    @Property
    protected List<String> exclude;

    @Property
    protected Map<String,String> alias;
    
    public List<String> exclude(){
        return this.exclude;
    }
    
    public List<String> include(){
        return this.include;
    }
    
    public Class target(){
        return this.target;
    }
    
    public String path(){
        return this.path;
    }

    public Map<String,String> alias() {
        return this.alias;
    }
    
}