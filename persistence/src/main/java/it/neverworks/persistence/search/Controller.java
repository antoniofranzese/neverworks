package it.neverworks.persistence.search;

import it.neverworks.model.Model;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;

public interface Controller extends Model {

    public Dialect dialect();
    public SearchPlan plan( SearchDescriptor descriptor, String text );
    public Tokenizer createTokenizer( Class type );
    public Inquirer createInquirer( EntityPropertyDescriptor property );
    
    default SearchDescriptor setup( SearchDescriptor descriptor ) { 
        return descriptor;
    }
}