package it.neverworks.persistence.search;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.model.features.Get;
import it.neverworks.persistence.criteria.Finder;

public class CriteriaSearchPlan implements SearchPlan {
    
    protected SearchDescriptor descriptor;
    protected List<SearchPropertyDescriptor> properties;
    protected List<Token> tokens;
    protected Map<String, Object> context;
    
    public CriteriaSearchPlan( SearchDescriptor descriptor, List<SearchPropertyDescriptor> properties, List<Token> tokens ) {
        this.descriptor = descriptor;
        this.properties = properties;
        this.tokens = tokens;
    }

    @Override
    public void using( Finder finder ) {
        for( Token token: this.tokens ) {
            finder.begin();
            for( SearchPropertyDescriptor property: this.properties ) {
                Inquirer inquirer = property.inquirer();
                if( inquirer.claims( token ) 
                    && ( token.get( "alias" ) == null || property.value( "alias" ).contains( token.get( "alias" ) ) ) ) {
                    inquirer.process( new SearchCase( this, property, token, finder ) );
                }
            }
        }
    }

    @Override
    public SearchDescriptor descriptor() {
        return this.descriptor;
    }

    @Override
    public <T> T get( String name ) {
        if( context != null ) {
            return (T) context.get( name );
        } else {
            return null;
        }
    }

    @Override
    public SearchPlan set( String name, Object value ) {
        if( context == null ) {
            context = new HashMap<>();
        }
        context.put( name, value );
        return this;
    }

}