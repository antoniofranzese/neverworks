package it.neverworks.persistence.search;

import it.neverworks.model.Property;

public class NumberToken extends Token {
    
    @Property
    protected Number number;
    
    @Property
    protected String operator;

    @Property
    protected Number ending;
}