package it.neverworks.persistence.search;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Strings;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class BaseTokenizer implements Tokenizer {

    @Property @AutoConvert
    protected boolean aliases = true;

    protected List<Parser> parsers = new ArrayList<Parser>();

    public List<Token> tokenize( String text ) {
        List<Token> result = new ArrayList<>();
        if( Strings.hasText( text ) ) {
            for( String word: text.replaceAll( "\\s+", " " ).split( " " ) ) {
                if( Strings.hasText( word ) ) {
                    String alias = null;

                    if( aliases ) {
                        int colon = word.indexOf( ":" );
                        if( colon > 0 ) {
                            alias = word.substring( 0, colon ).trim().toLowerCase();
                            word = word.substring( colon + 1 );
                        }

                    }

                    Token token = null;
                    int i = 0;
                    if( parsers != null ) {
                        while( token == null && i < parsers.size() ) {
                            token = parsers.get( i++ ).parse( word );
                        }
                    }
                    if( token == null ) {
                        token = new StringToken().set( "text", word );
                    }
                    token.set( "alias", alias );
                    result.add( token );                    
                }
            }

        }
        return result;
    }

    public BaseTokenizer register( Parser parser ) {
        this.parsers.add( parser );
        return this;
    }

}