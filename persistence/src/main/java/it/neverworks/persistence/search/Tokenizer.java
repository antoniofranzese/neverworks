package it.neverworks.persistence.search;

import java.util.List;
import it.neverworks.model.Model;

public interface Tokenizer extends Model {
    List<Token> tokenize( String text );
}