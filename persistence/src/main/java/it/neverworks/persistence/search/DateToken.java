package it.neverworks.persistence.search;

import java.util.Date;
import it.neverworks.model.Property;

public class DateToken extends Token {
    
    @Property
    protected Date date;
    
    @Property
    protected Date ending;

    @Property
    protected String operator;

}