package it.neverworks.persistence.search;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Wrapper;
import it.neverworks.model.features.Get;
import it.neverworks.persistence.criteria.Finder;

public interface SearchPlan extends Get {
    
    public void using( Finder finder );
    public SearchDescriptor descriptor();
    public <T> T get( String name );
    public SearchPlan set( String name, Object value );

    default <T> T probe( String name, Wrapper initializer ) {
        T value = get( name );
        if( value == null ) {
            value = (T) initializer.asObject();
            set( name, value );
        }
        return value;
    }

}