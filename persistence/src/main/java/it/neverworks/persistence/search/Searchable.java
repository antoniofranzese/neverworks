package it.neverworks.persistence.search;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Inherited;

@Inherited
@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
public @interface Searchable {
    Class<? extends Tokenizer> tokenizer() default Tokenizer.class;
    String setup() default "";
    boolean autoAlias() default true;
}
