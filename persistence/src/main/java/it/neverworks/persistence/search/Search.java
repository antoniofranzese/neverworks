package it.neverworks.persistence.search;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Inherited;

@Inherited
@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention( RetentionPolicy.RUNTIME )
public @interface Search {
    String[] alias() default {};
    Class<? extends Inquirer> inquirer() default Inquirer.class;
    String setup() default "";
    String[] include() default {};
    String[] exclude() default {};
}
