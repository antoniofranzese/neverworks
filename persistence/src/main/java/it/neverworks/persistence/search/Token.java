package it.neverworks.persistence.search;

import it.neverworks.model.Property;
import it.neverworks.model.Model;
import it.neverworks.model.utils.Identification;

@Identification( string = "text" )
public class Token implements Model {
    
    @Property
    protected String text;
    
    public String text(){
        return this.text;
    }

    @Property
    protected String alias;
    
    public String alias(){
        return this.alias;
    }    

}