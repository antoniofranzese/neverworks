package it.neverworks.persistence.search;

import java.util.Date;
import it.neverworks.lang.Dates;
import it.neverworks.model.Property;
import it.neverworks.persistence.criteria.Finder;

public class DateInquirer implements Inquirer {

    @Property
    protected boolean day = true;

    public boolean claims( Token token ) {
        return token instanceof DateToken;
    }
    
    public void process( SearchCase search ) {
        Date date = search.token().get( "date" );
        if( day ) {
            search.dialect().queryDates( search, Dates.beginning( date ), Dates.ending( date ) );

        } else {
            search.dialect().queryDate( search, null, date );
        }
    }

}