package it.neverworks.persistence.search;

import java.util.Date;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;

import it.neverworks.model.Property;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;

public class CriteriaController implements Controller {
    
    @Property
    private Dialect dialect = new CriteriaDialect();

    public SearchPlan plan( SearchDescriptor descriptor, String text ) {
        return new CriteriaSearchPlan( descriptor, descriptor.properties(), descriptor.tokenizer().tokenize( text ) );
    }

    public Tokenizer createTokenizer( Class type ) {
        return new BaseTokenizer()
            .register( new NumberParser() )
            .register( new DateParser() );
    }

    public Inquirer createInquirer( EntityPropertyDescriptor property ) {
        Class<? extends Inquirer> inquirerClass = null;
        Search search = property.annotation( Search.class );
        if( search != null && ! search.inquirer().equals( Inquirer.class ) ) {
            inquirerClass = search.inquirer();
        }
        
        if( inquirerClass == null ) {
            Class type = property.get( "type" );

            if( Date.class.isAssignableFrom( type ) ) {
                inquirerClass = DateInquirer.class;
            } else if( Number.class.isAssignableFrom( type ) ) {
                inquirerClass = NumberInquirer.class;
            } else {
                inquirerClass = StringInquirer.class;
            }
        }

        Inquirer inquirer = Reflection.newInstance( inquirerClass );
        if( Strings.hasText( search.setup() ) && Arguments.isParsable( search.setup() ) ) {
            inquirer.set( Arguments.parse( search.setup() ) );
        }

        return inquirer;
    }

    public Dialect dialect() {
        return dialect;
    }
}