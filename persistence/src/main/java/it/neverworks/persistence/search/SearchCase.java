package it.neverworks.persistence.search;

import it.neverworks.model.features.Get;
import it.neverworks.persistence.criteria.Finder;

public class SearchCase implements Get {
    
    protected SearchPlan plan;
    
    protected SearchPropertyDescriptor property;

    protected Token token;

    protected Finder finder;
    
    public SearchCase( SearchPlan plan, SearchPropertyDescriptor property, Token token, Finder finder ) {
        this.plan = plan;
        this.property = property;
        this.token = token;
        this.finder = finder;
    }

    public Finder finder(){
        return this.finder;
    }
    
    public Token token(){
        return this.token;
    }
    
    public SearchPropertyDescriptor property(){
        return this.property;
    }
    
    public SearchPlan plan(){
        return this.plan;
    }

    public SearchDescriptor descriptor() {
        return this.plan.descriptor();
    }

    public Controller controller() {
        return this.plan.descriptor().controller();
    }

    public Dialect dialect() {
        return this.plan.descriptor().dialect();
    }
}