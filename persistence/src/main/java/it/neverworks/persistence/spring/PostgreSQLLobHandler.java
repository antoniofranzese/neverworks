package it.neverworks.persistence.spring;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.DefaultLobHandler;

import it.neverworks.lang.Errors;

public class PostgreSQLLobHandler extends DefaultLobHandler {
    
    protected class AutoDeleteFileInputStream extends FileInputStream {
        private boolean _closed = false;
        private int _position = 0;
        private File _file;
        private int _length;

        public AutoDeleteFileInputStream( File file, int length ) throws FileNotFoundException {
            super( file );
            this._file = file;
            this._length = length;
        }

        private void checkShouldClose( int readResult ) throws IOException {
            if( readResult == -1 ) {
                close();
            } else {
                this._position += readResult;
                if( this._position >= this._length ) {
                    close();
                }
            }
        }

        public int read( byte[] b ) throws IOException {
            if( this._closed ) {
                return -1;
            }
            int result = super.read( b );
            checkShouldClose( result );
            return result;
        }

        public int read( byte[] b, int off, int len ) throws IOException {
            if( this._closed ) {
                return -1;
            }
            
            int result = super.read( b, off, len );
            checkShouldClose( result );
            return result;
        }

        public void close() throws IOException {
            if( !this._closed ) {
                super.close();
                this._file.delete();
                this._closed = true;
            }
        }

        protected void finalize() throws IOException {
            close();
            super.finalize();
        }

    }
    
    protected class PostgreSQLLobCreator extends DefaultLobCreator {
        
        public void setBlobAsBinaryStream( PreparedStatement ps, int paramIndex, InputStream contentStream, int contentLength ) throws SQLException {
            try {
                
                int length = contentLength;
                InputStream content = contentStream;
            
                if( length <= 0 ) {
                    if( contentStream instanceof ByteArrayInputStream ) {
                        length = contentStream.available();
                    } else if( contentStream != null ){
                        ByteArrayOutputStream memory = new ByteArrayOutputStream();
                        int copied = copyAndMeasure( contentStream, memory, 51200 );
                    
                        if( copied >= 0 ) {
                            content = new ByteArrayInputStream( memory.toByteArray() );
                            length = copied;

                        } else {
                            byte[] data = memory.toByteArray();
                            File tempFile = File.createTempFile( "NWK-PostgreSQL-BLOB-", null );
                            FileOutputStream disk = new FileOutputStream( tempFile );
                            disk.write( data );
                        
                            try {
                                int copied2 = copyAndMeasure( contentStream, disk, 2147483647 - data.length );
                                if( copied2 == - 1 ) {
                                    throw new RuntimeException( "BLOB is too large to send over the protocol" );
                                }
                                
                                disk.flush();
                                length = data.length + copied2;
                                content = new AutoDeleteFileInputStream( tempFile, length );
                            
                            } finally {
                                disk.close();
                            }
                        }
                    
                    } else {
                        length = 0;
                    }
                }
            
                super.setBlobAsBinaryStream( ps, paramIndex, content, length );

            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        } 

    	protected int copyAndMeasure( InputStream input, OutputStream output, int limit ) {
            int total = 0;
    		try {
    			byte buffer[] = new byte[ 8192 ];
    			int readCount;
    			while ( ( readCount = input.read( buffer ) ) != -1 ) {
    				output.write( buffer, 0, readCount );
                    total += readCount;
                    if( total >= limit ) {
                        return -1;
                    }
    			}

                return total;
    		} catch( IOException ex ) {
    			throw new RuntimeException( ex );
    		}
    	}

    }
    
    public LobCreator getLobCreator() {
        return new PostgreSQLLobCreator();
    }

}