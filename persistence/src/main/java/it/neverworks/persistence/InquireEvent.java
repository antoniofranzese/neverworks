package it.neverworks.persistence;

import it.neverworks.persistence.criteria.Query;

public class InquireEvent extends PersistenceEvent {
    
    protected String body;
    protected Class target;
    protected Query query;
    
    public Class getTarget(){
        return this.target;
    }
    
    public void setTarget( Class target ){
        this.target = target;
    }
    
    public String getBody(){
        return this.body;
    }
    
    public void setBody( String body ){
        this.body = body;
    }
    
    public <Q> Query<Q> getQuery(){
        return (Query<Q>) this.query;
    }
    
    public void setQuery( Query query ){
        this.query = query;
    }
}