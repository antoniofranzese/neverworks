package it.neverworks.persistence;

public class ConfigureEvent extends PersistenceEvent {
    
    protected Class target;

    public Class getTarget(){
        return this.target;
    }
    
    public void setTarget( Class target ){
        this.target = target;
    }
    
}