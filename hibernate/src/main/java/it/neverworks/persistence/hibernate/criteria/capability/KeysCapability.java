package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;

import it.neverworks.persistence.hibernate.criteria.Capability;
import it.neverworks.persistence.hibernate.criteria.Phase;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.statement.AbstractStatement;

import org.hibernate.criterion.Projections;

public class KeysCapability extends AbstractCapability {

	public KeysCapability( String name ) {
		super( name );
	}

	@Override
	public void doWithFinder( StatementFinder finder, Arguments arguments ) {
		finder.addPhase( new Phase( "keys" ) ).inPlaceOf( "projection" );

		finder.getPhase( "keys" ).append( new AbstractStatement() {
			
			@Override public void doWithFinder(StatementFinder finder) {
				logger.debug( "Disabling projection for keys" );
				finder.set( "__useProjection", false );

				if( finder.attribute( "__uniques" ).isTrue() ) {
                    finder.getCriteria().setProjection( Projections.distinct( Projections.id() ) );
				} else {
    				finder.getCriteria().setProjection( Projections.id() );
				}
			}

			@Override public String toString() {
				return "keys";
			}
		
		});
	}
	

}
