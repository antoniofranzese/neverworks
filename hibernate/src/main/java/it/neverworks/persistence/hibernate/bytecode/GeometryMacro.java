package it.neverworks.persistence.hibernate.bytecode;

import javassist.CtClass;
import javassist.CtField;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.persistence.spatial.Geometry;
import it.neverworks.persistence.spatial.Spatial;
import it.neverworks.persistence.spatial.GeometryModel;
import it.neverworks.persistence.spatial.JTSGeometryConverter;
import it.neverworks.model.bytecode.ByteCodeUtils;

public class GeometryMacro implements FieldProcessor {
    
    public void process( CtClass cls, CtField field ) throws Exception {

        if( ! ByteCodeUtils.isMarked( field, "entities.spatial" ) 
            && ( field.hasAnnotation( Spatial.class ) || field.hasAnnotation( Geometry.class ) )
            && ! field.hasAnnotation( Type.class ) ) {

            boolean useModel = ByteCodeUtils.hasSuperclass( field.getType(), ByteCodeUtils.getType( "it.neverworks.persistence.spatial.GeometryModel" ) );

            ByteCodeUtils.createAnnotation( cls, Type.class )
                // .set( "type", "it.neverworks.persistence.hibernate.spatial.GeometryUserType" )
                .set( "type", useModel
                    ? "it.neverworks.persistence.hibernate.spatial.GeometryModelUserType"
                    : "it.neverworks.persistence.hibernate.spatial.GeometryUserType"
                    // ? "GeometryModel"
                    // : "Geometry"
                )
                // .set( "parameters", list(
                //     ByteCodeUtils.createAnnotation( cls, Parameter.class )
                //         .set( "name", "model" )
                //         .set( "value", useModel ? "true" : "false" )
                //         .annotation()
                // ))
                .annotate( field );
                
            if( field.hasAnnotation( Property.class )
                && ! field.hasAnnotation( Convert.class )
                && ! field.hasAnnotation( AutoConvert.class ) ) {
                
                ByteCodeUtils.createAnnotation( cls, AutoConvert.class )
                    .annotate( field );
            }
            
            ByteCodeUtils.mark( field, "entities.spatial" );

        }
        
    }
    
}