package it.neverworks.persistence.hibernate;

import java.util.Properties;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.usertype.ParameterizedType;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.EnumConverter;
import it.neverworks.model.utils.ToStringBuilder;

public class StringEnumType extends AbstractUserType implements ParameterizedType, Converter {

    private String propertyName = "id";
    private Class enumClass = null;
    private Converter converter;

    @Override
    public Object convert( Object value ) {
        if( this.converter == null && this.enumClass != null ) {
            this.converter = new EnumConverter( this.enumClass );
        }
        if( this.converter != null ) {
            return this.converter.convert( value );
        } else {
            return value;
        }
    }

	@Override
	public Class returnedClass() {
		return Enum.class;
	}
    
	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.VARCHAR };
	}
    
    public void setParameterValues( Properties parameters ) {
        if( parameters != null ) {
            if( parameters.containsKey( "enum" ) ) {
                this.enumClass = Reflection.findClass( parameters.getProperty( "enum" ) );
            }
            if( parameters.containsKey( "property" ) ) {
                this.propertyName = parameters.getProperty( "property" );
            }
        }
    } 

    public Object nullSafeGet( ResultSet rs, String[] names, Object owner ) throws HibernateException, SQLException {
        String value = rs.getString( names[ 0 ] );

        if( Strings.isEmpty( value ) ) {
            return null;

        } else {
            Object result = new ObjectQuery( enumClass ).by( propertyName ).eq( value ).result();
            if( result != null ) {
                return result;
            } else {
                throw new HibernateException( "Cannot map string to enum: " + value );
            }
        }
        
    }
    
    public void nullSafeSet( PreparedStatement st, Object value, int index ) throws HibernateException, SQLException {
        st.setString( index, value != null ? Strings.valueOf( ExpressionEvaluator.evaluate( value, propertyName ) ) : null );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "enum", this.enumClass != null ? this.enumClass.getName() : "null" )
            .add( "property", this.propertyName )
            .toString();
    }
    
}