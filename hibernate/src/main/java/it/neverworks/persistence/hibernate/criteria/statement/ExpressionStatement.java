package it.neverworks.persistence.hibernate.criteria.statement;

import java.util.Arrays;

import it.neverworks.model.Property;
import it.neverworks.model.description.Required;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.hibernate.type.TypeFactory;

public class ExpressionStatement extends AbstractStatement {

    @Property
	private String expression = null;

    @Property //TODO: Autoconvert per array
    private Object[] parameters = null;
	
	@Override
	public void doWithFinder( StatementFinder finder ) {
		if( expression != null ) {
			if( parameters != null ) {
				Type[] types = new Type[ parameters.length ];
				for( int i = 0; i < parameters.length; i++ ) {
					types[ i ] = TypeFactory.heuristicType( parameters[ i ].getClass().getName() );
				}
				finder.getCriteria().add(
					Restrictions.sqlRestriction( expression, parameters, types )
				);
			} else {
				finder.getCriteria().add(
					Restrictions.sqlRestriction( expression )
				);
			}
		}
	}

	@Override
	public String toString() {
		return "expression: "
			+ ( expression != null ? expression : "null expression" ) + ", "
			+ ( parameters != null && parameters.length > 0  ? "parameters: " + Arrays.asList( parameters ) : "no parameters" );
	}
	

}
