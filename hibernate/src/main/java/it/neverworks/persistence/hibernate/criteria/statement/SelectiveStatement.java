package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Criterion;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public interface SelectiveStatement {
    Criterion createCriterion( StatementFinder finder );
}