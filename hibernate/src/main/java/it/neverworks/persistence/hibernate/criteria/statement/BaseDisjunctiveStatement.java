package it.neverworks.persistence.hibernate.criteria.statement;

import java.util.List;
import java.util.ArrayList;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Junction;

import it.neverworks.model.Property;
import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public abstract class BaseDisjunctiveStatement extends AbstractStatement implements DisjunctiveStatement, SelectiveStatement {
	
    protected List<SelectiveStatement> disjunctions;
    
    public void addDisjunction( SelectiveStatement statement ) {
        if( this.disjunctions == null ) {
            this.disjunctions = new ArrayList<SelectiveStatement>();
        }
        this.disjunctions.add( statement );
    }
    
    public Criterion createCriterion( StatementFinder finder ) {
        if( disjunctions == null || disjunctions.size() == 0 ) {
            return createOwnCriterion( finder );

        } else {
            List<Criterion> criteria = new ArrayList<Criterion>();
            Criterion own = createOwnCriterion( finder );
            if( own != null ) {
                criteria.add( own );
            }

            for( SelectiveStatement statement: disjunctions ) {
                Criterion or = statement.createCriterion( finder );
                if( or != null ) {
                    criteria.add( or );
                }
            }

            if( criteria.size() > 1 ) {
                Junction junction = Restrictions.disjunction();
                for( Criterion criterion: criteria ) {
                    junction.add( criterion );
                }
                return junction;
            } else if( criteria.size() == 1 ) {
                return criteria.get( 0 );
            } else {
                return null;
            }
            
        }
    }

    protected abstract Criterion createOwnCriterion( StatementFinder finder );

}
