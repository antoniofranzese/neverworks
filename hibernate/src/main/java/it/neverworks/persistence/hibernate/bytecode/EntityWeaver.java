package it.neverworks.persistence.hibernate.bytecode;

import static it.neverworks.language.*;

import javassist.Modifier;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtField;
import javassist.CtConstructor;
import javassist.NotFoundException;
import javassist.bytecode.ConstPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.StringMemberValue;

import java.io.InputStream;
import java.io.Reader;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Transient;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToMany;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Embeddable;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.Proxy;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import it.neverworks.persistence.Lazy;
import it.neverworks.persistence.Avid;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Framework;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.model.bytecode.Weaver;
import it.neverworks.model.bytecode.ByteCodeUtils;

public class EntityWeaver implements Weaver {

    private FluentList<String> features = new FluentArrayList<String>();
    private FluentList<FieldProcessor> fieldProcessors = list(
        new LobMacros()
        ,new SequenceMacro()
        ,new BoolMacro()
        ,new EnumMacro()
        ,new GeometryMacro()
    );
    
    public EntityWeaver() {
        if( Framework.feature( "code.entities.transient" ) ) features.add( "transient" );
        if( Framework.feature( "code.entities.lazy.class" ) ) features.add( "lazy-class" );
        if( Framework.feature( "code.entities.lazy.many2one" ) ) features.add( "lazy-m2o" );
        if( Framework.feature( "code.entities.lazy.one2many" ) ) features.add( "lazy-o2m" );
        if( Framework.feature( "code.entities.constraints" ) ) features.add( "constraints" );
    }
    
    
    protected boolean active( String feature ) {
        return features.contains( feature );
    }
    
    public boolean instrument( String className ) throws Exception {

        CtClass cls = ByteCodeUtils.getType( className );
        boolean instrumented = false;

        // Instrumenta solo le classi, se non sono state gia' trattate
        //if( ( !cls.isInterface() && ! cls.hasAnnotation( EntityWoven.class ) ) 
        if( ( !cls.isInterface() && ! ByteCodeUtils.isMarked( cls, "entities" ) ) 
            && ( 
                ByteCodeUtils.hasAnnotation( cls, Entity.class ) 
                || ByteCodeUtils.hasAnnotation( cls, Table.class ) 
                || ByteCodeUtils.hasAnnotation( cls, MappedSuperclass.class ) 
                || ByteCodeUtils.hasAnnotation( cls, Embeddable.class ) 
            ) ) {
            instrumentClass( cls );
        }
        return instrumented;
        
    }
    
    private void instrumentClass( CtClass cls ) throws Exception {
        println( "Instrumenting entity " + cls.getName() + " [" + features.join( ", " ) + "]" );
        boolean performed = false;
        int counter = 0;

        // Scongela la classe in esame
        if( cls.isFrozen() ) {
            cls.defrost();
        }

        ClassFile clsFile = cls.getClassFile();

        if( ! cls.hasAnnotation( MappedSuperclass.class )
            && ! cls.hasAnnotation( Embeddable.class ) ) {
                 
            if( ! cls.hasAnnotation( Entity.class ) 
                && cls.hasAnnotation( Table.class ) ) {

                ByteCodeUtils.createAnnotation( cls, Entity.class )
                    .annotate( cls );
            }

            if( active( "lazy-class" ) 
                && ! cls.hasAnnotation( Lazy.class ) 
                && ! cls.hasAnnotation( Proxy.class ) ) {
            
                ByteCodeUtils.createAnnotation( cls, Proxy.class )
                    .set( "lazy", false )
                    .annotate( cls );
            }
        }


        for( CtField field: cls.getDeclaredFields() ) {
            
            if( active( "constraints" ) 
                && field.hasAnnotation( Column.class ) ) {
                
                ByteCodeUtils.AnnotationBuilder column = ByteCodeUtils.extractAnnotation( field, Column.class ); 

                if( field.hasAnnotation( NotNull.class ) ) {
                    column.set( "nullable", false );
                }

                if( field.hasAnnotation( Size.class ) ) {
                    Size size = (Size) field.getAnnotation( Size.class );
                    
                    // Max is not default
                    if( size.max() != 2147483647 ) {
                        column.set( "length", size.max() );
                    }
                }

                column.annotate( field );
            }
            
            // Aggiunge @Transient se non e' presente nessuna annotazione di mapping
            if( active( "transient" ) 
                && ! Modifier.isStatic( field.getModifiers() )
                && ! field.hasAnnotation( Id.class ) 
                && ! field.hasAnnotation( Column.class ) 
                && ! field.hasAnnotation( ManyToOne.class )
                && ! field.hasAnnotation( OneToMany.class )
                && ! field.hasAnnotation( OneToOne.class )
                && ! field.hasAnnotation( ManyToMany.class )
                && ! field.hasAnnotation( Embedded.class )
                && ! field.hasAnnotation( EmbeddedId.class )
                && ! field.hasAnnotation( Transient.class ) ) {

                ByteCodeUtils.createAnnotation( cls, Transient.class )
                    .annotate( field );
                
            }
            
            // Disattiva lazy su ManyToOne se non richiesto
            if( active( "lazy-m2o" )
                && field.hasAnnotation( ManyToOne.class ) ) {

                ByteCodeUtils.extractAnnotation( field, ManyToOne.class )
                    .set( "fetch", FetchType.class, field.hasAnnotation( Lazy.class ) ? "LAZY" : "EAGER" )
                    .annotate( field );
            }

            // Attiva avid su ManyToOne se richiesto, imposta LazyCollection se non presente
            if( active( "lazy-o2m" )
                && field.hasAnnotation( OneToMany.class )) {

                // Avid
                if( field.hasAnnotation( Avid.class ) ) {
                    ByteCodeUtils.extractAnnotation( field, OneToMany.class )
                        .set( "fetch", FetchType.class, "EAGER" )
                        .annotate( field );

                    if( ! field.hasAnnotation( LazyCollection.class ) ) {
                        ByteCodeUtils.createAnnotation( cls, LazyCollection.class )
                            .set( "value", LazyCollectionOption.class, "FALSE" )
                            .annotate( field );
                    }
                    
                // Lazy
                } else {
                    ByteCodeUtils.extractAnnotation( field, OneToMany.class )
                        .set( "fetch", FetchType.class, "LAZY" )
                        .annotate( field );

                    if( ! field.hasAnnotation( LazyCollection.class ) ) {
                        ByteCodeUtils.createAnnotation( cls, LazyCollection.class )
                            .set( "value", LazyCollectionOption.class, "TRUE" )
                            .annotate( field );
                    }
                    
                }

            }
            
            // Espansioni macro e altre operazioni sui field
            for( FieldProcessor fieldProcessor: fieldProcessors ) {
                fieldProcessor.process( cls, field );
            }

        }


        // Aggiunge un'annotazione per tracciare il lavoro del weaver
        //ByteCodeUtils.createAnnotation( cls, EntityWoven.class ).annotate( cls );
        ByteCodeUtils.mark( cls, "entities" );
        
        // Congela la classe per catturare eventuali errori di weaving
        ByteCodeUtils.write( cls );
        
    }
    
}