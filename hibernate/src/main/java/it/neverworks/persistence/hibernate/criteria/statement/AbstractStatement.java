package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.log.Logger;
import it.neverworks.model.Property;
import it.neverworks.model.AbstractModel;
import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public abstract class AbstractStatement extends AbstractModel implements Statement {

    protected static Logger logger = Logger.of( StatementFinder.class );

    @Property
    protected boolean negation = false;

    public <T> T get( String name ) {
        return (T)this.model().eval( name );
    }

    public <T extends AbstractStatement> T set( String name, Object value ) {
        this.model().safeAssign( name, value );
        return (T)this;
    }
    
}
