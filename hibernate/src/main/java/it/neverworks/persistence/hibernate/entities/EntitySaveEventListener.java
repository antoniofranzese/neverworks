package it.neverworks.persistence.hibernate.entities;

import it.neverworks.persistence.Save;
import it.neverworks.persistence.SaveEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.SaveOrUpdateEvent;
import org.hibernate.event.SaveOrUpdateEventListener;

/**
 * @author antonio
 *
 * Gestisce i listener @Save
 */
public class EntitySaveEventListener extends AbstractListener implements SaveOrUpdateEventListener {

	@Override
	public void onSaveOrUpdate(SaveOrUpdateEvent event) {
        manager.notify( event.getEntity(), Save.class, FACTORY );
	}
	
    private final static PersistenceEventFactory FACTORY = new PersistenceEventFactory() {
        public PersistenceEvent build() {
            return new SaveEvent();
        }
    };
}
