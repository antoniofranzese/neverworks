package it.neverworks.persistence.hibernate;

import org.hibernate.Session;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

public class AutoCloseSessionHolder extends SessionHolder {
	
    public AutoCloseSessionHolder( Session session ) {
        super( session );
    }
    
    public void released() {
        super.released();
        if( ! this.isOpen() ) {
            this.shutdown();
        }
    }
    
    public void shutdown() {
        SessionFactoryUtils.closeSession( this.getSession() );
    }
    
    
}