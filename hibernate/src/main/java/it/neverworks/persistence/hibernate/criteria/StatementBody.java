package it.neverworks.persistence.hibernate.criteria;

import it.neverworks.lang.Arguments;

public interface StatementBody {
	public void doWithFinder( StatementFinder finder, Arguments arguments ) ;
}
