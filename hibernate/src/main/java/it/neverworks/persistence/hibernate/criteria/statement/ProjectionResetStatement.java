package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class ProjectionResetStatement extends AbstractStatement {

	@Override public void doWithFinder( StatementFinder finder ) {
		finder.clearProjection();
	}

	@Override public String toString() {
		return "reset projection";
	}

}
