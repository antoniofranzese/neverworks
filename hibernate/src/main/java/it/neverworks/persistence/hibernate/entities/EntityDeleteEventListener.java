package it.neverworks.persistence.hibernate.entities;

import it.neverworks.persistence.Veto;
import it.neverworks.persistence.Delete;
import it.neverworks.persistence.DeleteEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.PreDeleteEvent;
import org.hibernate.event.PreDeleteEventListener;

/**
 * @author antonio
 *
 * Gestisce i listener @Delete
 */
public class EntityDeleteEventListener extends AbstractListener implements PreDeleteEventListener {

	@Override
	public boolean onPreDelete(PreDeleteEvent event) {
        try {
            manager.notify( event.getEntity(), Delete.class, FACTORY );
            return false;
        } catch( Veto v ) {
            return true;
        }
	}
	
    public final static PersistenceEventFactory FACTORY = new PersistenceEventFactory() {
        public PersistenceEvent build() {
            return new DeleteEvent();
        }
    };
}
