package it.neverworks.persistence.hibernate.entities;

public interface PostDetachedDeleteEventListener {
    boolean onPostDetachedDelete( PostDetachedDeleteEvent event );
}