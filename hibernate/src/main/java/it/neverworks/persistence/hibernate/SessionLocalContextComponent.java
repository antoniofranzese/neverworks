package it.neverworks.persistence.hibernate;

import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import it.neverworks.context.LocalContextComponent;
import it.neverworks.context.LocalContext;

public class SessionLocalContextComponent implements LocalContextComponent {
    
    private static final String SESSION_ATTRIBUTE = SessionHolder.class.getName();
    private SessionFactory sessionFactory;
    
    public SessionLocalContextComponent( SessionFactory sessionFactory ) {
        this.sessionFactory = sessionFactory;
    }

    public void backup( LocalContext context ) {
        if( TransactionSynchronizationManager.hasResource( sessionFactory ) ) {
            SessionHolder holder = (SessionHolder) TransactionSynchronizationManager.getResource( sessionFactory );
            holder.requested();
            context.put( SESSION_ATTRIBUTE, holder );
        }
    }                                 
    
    public void restore( LocalContext context ) {
        if( context.has( SESSION_ATTRIBUTE ) ) {
            SessionHolder holder = context.<SessionHolder>get( SESSION_ATTRIBUTE );
    		TransactionSynchronizationManager.bindResource( sessionFactory, holder );
        }
    }
    
    public void cleanup( LocalContext context ) {
        if( context.has( SESSION_ATTRIBUTE ) ) {
            if( TransactionSynchronizationManager.hasResource( sessionFactory ) ) {
                SessionHolder holder = (SessionHolder) TransactionSynchronizationManager.unbindResource( sessionFactory );
                holder.released();
            }
        }
    }

}

