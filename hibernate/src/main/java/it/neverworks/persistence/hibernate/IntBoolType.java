package it.neverworks.persistence.hibernate;

import java.util.Properties;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.usertype.ParameterizedType;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.BooleanConverter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.utils.ToStringBuilder;

public class IntBoolType extends AbstractUserType implements ParameterizedType, Converter {

    private final static TypeDefinition<Boolean> BooleanType = TypeFactory.shared( Boolean.class );

    private int trueValue = 1;
    private int falseValue = 0;
    private boolean allowNulls = true;
    private boolean lenient = false;

    @Override
    public Object convert( Object value ) {
        if( value instanceof Boolean ) {
            return value;

        } else if( value instanceof Number ) {

            if( value.equals( trueValue ) ) {
                return Boolean.TRUE;
            } else if( value.equals( falseValue ) ) {
                return Boolean.FALSE;
            } else {
                return BooleanType.convert( value );
            }
            
        } else {
            return BooleanType.convert( value );
        }
        
    }

	@Override
	public Class returnedClass() {
		return Boolean.class;
	}
    
	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.NUMERIC };
	}
    
    public void setParameterValues( Properties parameters ) {
        if( parameters != null ) {
            if( parameters.containsKey( "true" ) ) {
                this.trueValue = Integer.parseInt( parameters.getProperty( "true" ) );
            }
            if( parameters.containsKey( "false" ) ) {
                this.falseValue = Integer.parseInt( parameters.getProperty( "false" ) );
            }
            if( parameters.containsKey( "nulls" ) ) {
                this.allowNulls = "true".equalsIgnoreCase( parameters.getProperty( "nulls" ) );
            }
            if( parameters.containsKey( "lenient" ) ) {
                this.lenient = "true".equalsIgnoreCase( parameters.getProperty( "lenient" ) );
            }
        }
    } 

    public Object nullSafeGet( ResultSet rs, String[] names, Object owner ) throws HibernateException, SQLException {
        Integer value = rs.getInt( names[ 0 ] );
        if( rs.wasNull() ) {
            value = null;
        }
        
        if( value == null ) {
            return allowNulls ? null : Boolean.FALSE;
        } else if( value.equals( trueValue ) ) {
            return Boolean.TRUE;
        } else if( value.equals( falseValue ) ) {
            return Boolean.FALSE;
        } else if( this.lenient ){
            return Boolean.FALSE;
        } else {
            throw new HibernateException( "Cannot map number to Boolean: " + value );
        }
        
    }
    
    public void nullSafeSet( PreparedStatement st, Object value, int index ) throws HibernateException, SQLException {
        if( value == null && allowNulls ) {
            st.setNull( index, Types.INTEGER );
        } else {
            st.setInt( index, Boolean.TRUE.equals( value ) ? trueValue : falseValue );
        }
    }
    
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "true", this.trueValue )
            .add( "false", this.falseValue )
            .toString();
    }
    
}