package it.neverworks.persistence.hibernate.criteria;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Collections;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;

import it.neverworks.persistence.criteria.CriteriaBuilder;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.hibernate.criteria.statement.BetweenStatement;
import it.neverworks.persistence.hibernate.criteria.statement.EqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.GreaterEqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.GreaterThanStatement;
import it.neverworks.persistence.hibernate.criteria.statement.InStatement;
import it.neverworks.persistence.hibernate.criteria.statement.LessEqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.LessThanStatement;
import it.neverworks.persistence.hibernate.criteria.statement.LikeStatement;
import it.neverworks.persistence.hibernate.criteria.statement.ContainsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.StartsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.EndsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.NullStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialWithinStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialDisjointStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialIntersectsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialOverlapsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialContainsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialCrossesStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialTouchesStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SpatialEqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.AutoEqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.AutoContainsStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SubInStatement;
import it.neverworks.persistence.spatial.JTSGeometryConverter;
import com.vividsolutions.jts.geom.Geometry;

import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import it.neverworks.log.Logger;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class StatementCriteriaBuilder<T> extends BaseModel implements CriteriaBuilder<T> {

	private final static Logger logger = Logger.of( StatementFinder.class );

    @Property @Required
	protected StatementFinder<T> finder;
    
    @Property @Required
	protected String property;
	
    protected boolean negation = false;
    
	public StatementCriteriaBuilder() {
	}

	public StatementCriteriaBuilder( StatementFinder finder, String property) {
		this.finder = finder;
		this.property = property;
	}

    protected void appendStatement( Object statement ) {
        finder.addCriteriaStatement( (Statement) statement );
    }

	@Override
	public Finder<T> between( Object lowValue, Object highValue ) {
		appendStatement( new BetweenStatement()
            .set( "property", property )
            .set( "&lowValue", lowValue )
            .set( "&highValue", highValue )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> eq( Object value ) {
        if( value instanceof Wrapper ) {
    		appendStatement( new AutoEqualStatement()
                .set( "property", property )
                .set( "&value", value )
                .set( "negation", this.negation )
            );
        } else {
    		appendStatement( AutoEqualStatement.detectStatement( value )
                 .set( "property", property )
                 .set( "negation", this.negation )
            );
        }
		return this.finder;
	}

	@Override
	public Finder<T> ge( Object value ) {
		appendStatement( new GreaterEqualStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> gt( Object value ) {
		appendStatement( new GreaterThanStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> le( Object value ) {
		appendStatement( new LessEqualStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> like( Object value ) {
		appendStatement( new LikeStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        ); 
		return this.finder;
	}

	@Override
	public Finder<T> startsWith( Object value ) {
		appendStatement( new StartsStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        ); 
		return this.finder;
	}

	@Override
	public Finder<T> endsWith( Object value ) {
		appendStatement( new EndsStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        ); 
		return this.finder;
	}

	@Override
	public Finder<T> contains( Object value ) {
        if( value instanceof Wrapper ) {
    		appendStatement( new AutoContainsStatement()
                .set( "property", property )
                .set( "&value", value )
                .set( "negation", this.negation )
            );
        } else {
    		appendStatement( AutoContainsStatement.detectStatement( value )
                 .set( "property", property )
                 .set( "negation", this.negation )
            );
        }
		return this.finder;
	}

	@Override
	public Finder<T> lt( Object value ) {
		appendStatement( new LessThanStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> in( Wrapper values ) {
		appendStatement( new InStatement()
            .set( "property", property )
            .set( "&value", values )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> in( Iterable values ) {
		appendStatement( new InStatement()
            .set( "property", property )
            .set( "value", Collections.list( values ) )
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> in( Object... values ) {
		appendStatement( new InStatement() 
            .set( "property", property )
            .set( "value", values != null ? Arrays.asList( values  ) : null )
            .set( "negation", this.negation )
        );
		return this.finder;
	}
    
    @Override
	public Finder<T> in( Finder subFinder ) {
	    appendStatement( new SubInStatement()
            .set( "property", property )
            .set( "value", subFinder )
            .set( "negation", this.negation )
        );
        return this.finder;
	}
    
	@Override
	public Finder<T> isNull() {
		appendStatement( new NullStatement()
            .set( "property", property ) 
            .set( "negation", this.negation )
        );
		return this.finder;
	}

	@Override
	public Finder<T> using( Object... values ) {
        return using( new Arguments( values ).arg( "negation", negation ) );
	}

	@Override
	public Finder<T> using( Map arguments ) {
		Capability capability = finder.getCapability( property );
        Arguments processed = Arguments.process( arguments );
        if( ! processed.has( "negation" ) ) {
            processed.arg( "negation", this.negation );
        }
		capability.doWithFinder( finder, processed );

		return this.finder;
	}
	
    @Override
    public CriteriaBuilder<T> not() {
        this.negation = ! this.negation;
        return this;
    }
    
    @Override
    public Finder<T> within( Object value ) {
		appendStatement( new SpatialWithinStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
        return this.finder;
    }

    @Override
    public Finder<T> intersects( Object value ) {
		appendStatement( new SpatialIntersectsStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
        return this.finder;
    }

    @Override
    public Finder<T> overlaps( Object value ) {
		appendStatement( new SpatialOverlapsStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
        return this.finder;
    }
	
    @Override
    public Finder<T> crosses( Object value ) {
		appendStatement( new SpatialCrossesStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
        return this.finder;
    }
	
    @Override
    public Finder<T> touches( Object value ) {
		appendStatement( new SpatialTouchesStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
        return this.finder;
    }

    @Override
    public Finder<T> disjoint( Object value ) {
		appendStatement( new SpatialDisjointStatement()
            .set( "property", property )
            .set( "&value", value )
            .set( "negation", this.negation )
        );
        return this.finder;
    }
	
}
