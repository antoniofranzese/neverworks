package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Criterion;
import com.vividsolutions.jts.geom.Geometry;
import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

public class AutoContainsStatement extends DispatchingStatement {
    
    public void doWithFinder( StatementFinder finder ) {
        if( this.property != null ) {
            detectStatement( Value.from( value ) )
                .set( "property", this.property )
                .set( "negation", this.negation )
                .doWithFinder( finder );
        }
    }

    public static AbstractStatement detectStatement( Object value ) {
        Object converted = Spatials.convert( value );
        if( converted instanceof Geometry ) {
    		return new SpatialContainsStatement()
                .set( "value", converted );
        } else {
    		return new ContainsStatement()
                 .set( "value", value );
        }
     }
    
	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " auto contains " + value;
	}
    
}