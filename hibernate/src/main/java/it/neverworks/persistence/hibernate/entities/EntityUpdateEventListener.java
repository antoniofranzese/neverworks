package it.neverworks.persistence.hibernate.entities;

import static it.neverworks.language.*;
import it.neverworks.persistence.Veto;
import it.neverworks.persistence.Update;
import it.neverworks.persistence.UpdateEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.PreUpdateEvent;
import org.hibernate.event.PreUpdateEventListener;

/**
 * @author antonio
 *
 * Gestisce i listener @Update
 */
public class EntityUpdateEventListener extends AbstractListener implements PreUpdateEventListener {

	@Override
	public boolean onPreUpdate(PreUpdateEvent event) {

        try {
            if( manager.notify( event.getEntity(), Update.class, FACTORY ) ) {
                updateState( event, event.getEntity(), event.getPersister().getPropertyNames(), event.getState() );
            }
            return false;
        } catch( Veto v ) {
            return true;
        }
	}
	
    private final static PersistenceEventFactory FACTORY = new PersistenceEventFactory() {
        public PersistenceEvent build() {
            return new UpdateEvent();
        }
    };
    
}
