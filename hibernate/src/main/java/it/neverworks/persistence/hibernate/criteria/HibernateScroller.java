package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.ScrollableResults;
import it.neverworks.persistence.criteria.Scroller;

public class HibernateScroller implements Scroller {
    
    private ScrollableResults results;
    
    public HibernateScroller( ScrollableResults results ) {
        this.results = results;
    }
    
    public boolean next(){
        return results.next();
    }
    
    public <T> T get( int index ) {
        return (T)results.get( index );
    }
    
}