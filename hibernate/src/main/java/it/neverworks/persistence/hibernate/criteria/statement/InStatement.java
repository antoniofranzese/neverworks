package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;

public class InStatement extends BinaryCriteriaStatement {

	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
        Object val = Value.from( value );
		if( Collections.isListable( val ) ) {
			List values = Collections.list( val );
			if( values.size() > 0 ) {
				if( values.size() > 1 ) {
					return checkNot( createSafeCriterion( finder.translateProperty( property ), values ) );
				} else {
					return checkNot( Restrictions.eq( finder.translateProperty( property ), values.get( 0 ) ) );
				}
			} else {
			    return null;
			}
		} else if( val != null ){
			return checkNot( Restrictions.eq( finder.translateProperty( property ), val ) );
		} else {
		    return null;
		}
		
	}
    
    protected final static int SAFETY_LIMIT = 1000;
    
    protected Criterion createSafeCriterion( String property, List values ) {
        if( values.size() <= SAFETY_LIMIT ) {
            return Restrictions.in( property, values );

        } else {
            Disjunction or = Restrictions.disjunction();
        
            int start = 0;
            int end = SAFETY_LIMIT;

            while( start < values.size() ) {
                or.add( Restrictions.in( property, values.subList( start, end ) ) );

                start += SAFETY_LIMIT;
                end = Math.min( end + SAFETY_LIMIT, values.size() );
            }

            return or;
        }
    }
    
	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " in " + Strings.limit( Strings.valueOf( value ), 100, "..." );
	}

}
