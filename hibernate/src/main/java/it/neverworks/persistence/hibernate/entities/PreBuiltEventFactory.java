package it.neverworks.persistence.hibernate.entities;

import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;
import it.neverworks.persistence.criteria.Finder;

public class PreBuiltEventFactory implements PersistenceEventFactory {
    private PersistenceEvent event;
    
    public PreBuiltEventFactory( PersistenceEvent event ) {
        this.event = event ;
    }
    
    public PersistenceEvent build() {
        return this.event;
    }
    
}

