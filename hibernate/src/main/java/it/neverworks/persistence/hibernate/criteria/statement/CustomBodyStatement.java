package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;

import it.neverworks.persistence.hibernate.criteria.StatementBody;
import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

import java.util.Arrays;

public class CustomBodyStatement extends AbstractStatement {

    @Property
	StatementBody body;
    
    @Property @Default( Arguments.class )
	Arguments arguments;
	
	@Override
	public void doWithFinder( StatementFinder finder ) {
		if( this.body != null ) {
			this.body.doWithFinder( finder, arguments );
		}
	}
	
	@Override
	public String toString() {
		return "custom: " 
			+ ( body != null ? body.toString() : "null body" ) + ", "
			+ ( arguments != null  ? "arguments: " + arguments : "no arguments" );
	}
	

}
