package it.neverworks.persistence.hibernate.entities;

import it.neverworks.persistence.InquireEvent;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class LazyInquireEvent extends InquireEvent {
    
    protected ListeningEntityManager manager;
    
    public LazyInquireEvent( ListeningEntityManager manager, Class target, String body ) {
        this.manager = manager;
        this.target = target;
        this.body = body;
    }
    
    @Override
    public <Q> Query<Q> getQuery(){
        if( this.query == null ) {
            this.query = manager.buildQuery( this.target, this.body );
        }
        return (Query<Q>)this.query;
    }
    
    public ListeningEntityManager getManager(){
        return this.manager;
    }
    
}