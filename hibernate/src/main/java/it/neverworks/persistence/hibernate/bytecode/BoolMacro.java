package it.neverworks.persistence.hibernate.bytecode;

import javassist.CtClass;
import javassist.CtField;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.persistence.Bool;
import it.neverworks.model.bytecode.ByteCodeUtils;

public class BoolMacro implements FieldProcessor {
    
    public void process( CtClass cls, CtField field ) throws Exception {

        if( ! ByteCodeUtils.isMarked( field, "entities.bool" ) 
            && field.hasAnnotation( Bool.class ) 
            && ! field.hasAnnotation( Type.class ) ) {

            Bool bool = (Bool) field.getAnnotation( Bool.class );
            String allowNulls = "true";
            boolean isValid = true;
            boolean useString = Strings.hasText( bool.yes() );
            
            if( field.getType().getName().equals( "boolean" ) ) {
                allowNulls = "false";
            } else if( ! field.getType().getName().equals( "java.lang.Boolean" ) ) {
                isValid = false;
            }
            
            if( isValid ) {
                ByteCodeUtils.createAnnotation( cls, Type.class )
                    .set( "type", useString ? "it.neverworks.persistence.hibernate.StringBoolType" : "it.neverworks.persistence.hibernate.IntBoolType" )
                    .set( "parameters", list(
                        ByteCodeUtils.createAnnotation( cls, Parameter.class )
                            .set( "name", "true" )
                            .set( "value", Strings.valueOf( useString ? bool.yes() : bool.on() ) )
                            .annotation()
                        ,ByteCodeUtils.createAnnotation( cls, Parameter.class )
                            .set( "name", "false" )
                            .set( "value", Strings.valueOf( useString ? bool.no() : bool.off() ) )
                            .annotation()
                        ,ByteCodeUtils.createAnnotation( cls, Parameter.class )
                            .set( "name", "nulls" )
                            .set( "value", allowNulls )
                            .annotation()
                        ,ByteCodeUtils.createAnnotation( cls, Parameter.class )
                            .set( "name", "lenient" )
                            .set( "value", bool.lenient() ? "true" : "false" )
                            .annotation()
                    ))
                    .annotate( field );
                
                ByteCodeUtils.mark( field, "entities.bool" );
            }

        }
        
    }
    
}