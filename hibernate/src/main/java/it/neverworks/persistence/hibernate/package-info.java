@TypeDefs({
    //  @TypeDef( name="Geometry",            typeClass = GeometryUserType.class )
    // ,@TypeDef( name="GeometryModel",       typeClass = GeometryModelUserType.class )
    @TypeDef( name="BlobBinaryStream",    typeClass = BlobBinaryStreamType.class )
    ,@TypeDef( name="BlobByteArray",       typeClass = BlobByteArrayType.class )
    ,@TypeDef( name="ClobAsciiStream",     typeClass = ClobAsciiStreamType.class )
    ,@TypeDef( name="ClobCharacterReader", typeClass = ClobCharacterReaderType.class )
    ,@TypeDef( name="ClobString",          typeClass = ClobStringType.class )
    ,@TypeDef( name="IntBool",             typeClass = IntBoolType.class )
    ,@TypeDef( name="StringBool",          typeClass = StringBoolType.class )
    ,@TypeDef( name="StringEnum",          typeClass = StringEnumType.class )
})
package it.neverworks.persistence.hibernate;

import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
// import it.neverworks.persistence.hibernate.spatial.GeometryUserType;
// import it.neverworks.persistence.hibernate.spatial.GeometryModelUserType;
import it.neverworks.persistence.hibernate.BlobBinaryStreamType;
import it.neverworks.persistence.hibernate.BlobByteArrayType;
import it.neverworks.persistence.hibernate.ClobAsciiStreamType;
import it.neverworks.persistence.hibernate.ClobCharacterReaderType;
import it.neverworks.persistence.hibernate.ClobStringType;
import it.neverworks.persistence.hibernate.IntBoolType;
import it.neverworks.persistence.hibernate.StringBoolType;
import it.neverworks.persistence.hibernate.StringEnumType;

