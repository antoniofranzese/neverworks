package it.neverworks.persistence.hibernate.bytecode;

import javassist.CtClass;
import javassist.CtField;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;

import static it.neverworks.language.*;
import it.neverworks.model.bytecode.ByteCodeUtils;
import it.neverworks.persistence.Enum;

public class EnumMacro implements FieldProcessor {
    
    public void process( CtClass cls, CtField field ) throws Exception {

        if( ! ByteCodeUtils.isMarked( field, "entities.enum" )
            && field.hasAnnotation( Enum.class ) 
            && ! field.hasAnnotation( Type.class ) ) {

            Enum enumAnnotation = (Enum) field.getAnnotation( Enum.class );

            ByteCodeUtils.createAnnotation( cls, Type.class )
                .set( "type", "it.neverworks.persistence.hibernate.StringEnumType" )
                .set( "parameters", list(
                    ByteCodeUtils.createAnnotation( cls, Parameter.class )
                        .set( "name", "enum" )
                        .set( "value", field.getType().getName() )
                        .annotation()
                    ,ByteCodeUtils.createAnnotation( cls, Parameter.class )
                        .set( "name", "property" )
                        .set( "value", enumAnnotation.property() )
                        .annotation()
                ))
                .annotate( field );
                
            ByteCodeUtils.mark( field, "entities.enum" );

        }
        
    }
    
}