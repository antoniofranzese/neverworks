package it.neverworks.persistence.hibernate;

import java.util.Properties;
import org.hibernate.usertype.ParameterizedType;
import it.neverworks.lang.Booleans;

public abstract class AbstractLobType extends org.springframework.orm.hibernate3.support.AbstractLobType implements ParameterizedType {

    protected boolean blankable = true;
    
    public void setParameterValues( Properties parameters ) {
        if( parameters != null ) {
            if( parameters.containsKey( "blankable" ) ) {
                this.blankable = Booleans.isTrue( parameters.getProperty( "blankable" ) );
            }
        }
    } 
    
    public boolean getBlankable(){
        return this.blankable;
    }
    
    public void setBlankable( boolean blankable ){
        this.blankable = blankable;
    }

}
