package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import it.neverworks.model.Property;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import org.hibernate.criterion.Criterion;

public abstract class UnaryCriteriaStatement extends BaseDisjunctiveStatement {
	
    @Property
    protected String property = null;
	
	@Override
	public void doWithFinder(StatementFinder finder) {
		if( property != null ) {
            Criterion criterion = createCriterion( finder );
            if( criterion != null ) {
        		finder.getCriteria().add( criterion );
            }
		}
	}

    protected Criterion checkNot( Criterion criterion ) {
        return this.negation ? Restrictions.not( criterion ) : criterion;
    }

}
