package it.neverworks.persistence.hibernate.criteria.statement;

public interface DisjunctiveStatement {
    void addDisjunction( SelectiveStatement statement );
}