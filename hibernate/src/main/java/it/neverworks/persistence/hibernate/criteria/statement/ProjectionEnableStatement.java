package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class ProjectionEnableStatement extends AbstractStatement {

	@Override public void doWithFinder( StatementFinder finder ) {
		logger.debug( "Enabling projection" );
		finder.set( "__useProjection", true );
	}

	@Override public String toString() {
		return "enable projection";
	}

}
