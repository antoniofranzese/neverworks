package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.Statement;

public class GlobalOuterCapability extends AbstractCapability {

	public GlobalOuterCapability(String name) {
		super(name);
	}

	@Override
	public void doWithFinder(StatementFinder finder, Arguments arguments ) {
		finder.getPhase( "startup" ).append( new Statement() {
			@Override public void doWithFinder(StatementFinder finder) {
				logger.debug( "Enabling outer join as default" );
				finder.set( "__useOuterJoinAsDefault", true );
			}
		});
	}

}
