package it.neverworks.persistence.hibernate;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.collection.PersistentCollection;
import org.hibernate.engine.SessionImplementor;
import org.hibernate.loader.CollectionAliases;
import org.hibernate.persister.collection.CollectionPersister;
import org.hibernate.type.Type;

public class ModelPersistentCollection implements PersistentCollection {

	@Override
	public boolean afterInitialize() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void afterRowInsert(CollectionPersister arg0, Object arg1, int arg2)
			throws HibernateException {
		// TODO Auto-generated method stub

	}

	@Override
	public void beforeInitialize(CollectionPersister arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void beginRead() {
		// TODO Auto-generated method stub

	}

	@Override
	public void clearDirty() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dirty() {
		// TODO Auto-generated method stub

	}

	@Override
	public Serializable disassemble(CollectionPersister arg0)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean empty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean endRead() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator entries(CollectionPersister arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean entryExists(Object arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean equalsSnapshot(CollectionPersister arg0)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void forceInitialization() throws HibernateException {
		// TODO Auto-generated method stub

	}

	@Override
	public Iterator getDeletes(CollectionPersister arg0, boolean arg1)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getElement(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getIdentifier(Object arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getIndex(Object arg0, int arg1, CollectionPersister arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Serializable getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getOrphans(Serializable arg0, String arg1)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getOwner() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection getQueuedOrphans(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getRole() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Serializable getSnapshot(CollectionPersister arg0)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getSnapshotElement(Object arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Serializable getStoredSnapshot() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasQueuedOperations() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void initializeFromCache(CollectionPersister arg0,
			Serializable arg1, Object arg2) throws HibernateException {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isDirectlyAccessible() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDirty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isRowUpdatePossible() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSnapshotEmpty(Serializable arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isUnreferenced() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isWrapper(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean needsInserting(Object arg0, int arg1, Type arg2)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean needsRecreate(CollectionPersister arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean needsUpdating(Object arg0, int arg1, Type arg2)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void postAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public void preInsert(CollectionPersister arg0) throws HibernateException {
		// TODO Auto-generated method stub

	}

	@Override
	public Iterator queuedAdditionIterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object readFrom(ResultSet arg0, CollectionPersister arg1,
			CollectionAliases arg2, Object arg3) throws HibernateException,
			SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean setCurrentSession(SessionImplementor arg0)
			throws HibernateException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setOwner(Object arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setSnapshot(Serializable arg0, String arg1, Serializable arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean unsetSession(SessionImplementor arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean wasInitialized() {
		// TODO Auto-generated method stub
		return false;
	}

}
