package it.neverworks.persistence.hibernate;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import it.neverworks.lang.Strings;

public class ClobStringType extends AbstractLobType {

	@Override
	public Class returnedClass() {
		return String.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.CLOB };
	}

	@Override
	protected Object nullSafeGetInternal( ResultSet rs, String[] names, Object owner, LobHandler lobHandler ) throws SQLException, IOException, HibernateException {
		return process( lobHandler.getClobAsString( rs, names[ 0 ] ) );
	}

	@Override
	protected void nullSafeSetInternal( PreparedStatement ps, int index, Object value, LobCreator lobCreator ) throws SQLException, IOException, HibernateException {
		lobCreator.setClobAsString( ps, index, process( (String) value ) );
	}

    protected String process( String string ) {
        if( ! blankable && string != null) {
            return Strings.plane( string );
        } else {
            return string;
        }
    }
}
