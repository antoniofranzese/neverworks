package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.model.Value;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;

public class GreaterEqualStatement extends BinaryCriteriaStatement {

	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
        Object val = Value.from( value );
		return val != null ? checkNot( Restrictions.ge( finder.translateProperty( property ), val ) ) : null;
	}

	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " greater or equal to " + value;
	}
	
}
