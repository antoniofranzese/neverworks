package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.lang.Strings;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;

public class LikeStatement extends BinaryCriteriaStatement {

	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
        Object val = Value.from( value );
		if( Strings.hasText( val ) ) {
            String like = processLike( Strings.safe( val ) );
    		if( !like.matches("^%*$") ) {
				return checkNot( Restrictions.ilike( finder.translateProperty( property ), like ) );
    		} else {
    		    return null;
    		}
		} else {
		    return null;
		}
	}

    protected String processLike( String value ) {
        return value.replaceAll( "\\*", "%" );
    }
    
	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " like " + value;
	}
	
}
