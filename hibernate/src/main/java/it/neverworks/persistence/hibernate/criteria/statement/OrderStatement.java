package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Order;

public class OrderStatement extends AbstractStatement {

    @Property @Collection
	List<String> properties;
	
	@Override
	public void doWithFinder(StatementFinder finder) {
		if( properties != null ) {
			for( String property: properties ) {
                if( Strings.hasText( property ) ) {
    				boolean descending = false;
    				if( property.startsWith( "-" ) ) {
    					property = property.substring( 1 );
    					descending = true;
    				}
    				String propertyName = finder.translateProperty( property );
    				finder.getCriteria().addOrder( descending ? Order.desc( propertyName ) : Order.asc( propertyName ) );
                }
			}
		}
	}

	@Override
	public String toString() {
		return " order " + properties;
	}

}
