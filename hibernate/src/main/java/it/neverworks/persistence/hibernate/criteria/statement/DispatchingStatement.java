package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.model.Property;

public abstract class DispatchingStatement extends AbstractStatement {
    
    @Property
    protected String property = null;
	
    @Property
    protected Object value;
    
}