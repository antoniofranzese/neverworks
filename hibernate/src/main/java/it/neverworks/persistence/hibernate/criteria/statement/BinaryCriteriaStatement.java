package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Criterion;

import it.neverworks.model.Property;
import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public abstract class BinaryCriteriaStatement extends UnaryCriteriaStatement {
	
    @Property
    protected Object value;
	
}
