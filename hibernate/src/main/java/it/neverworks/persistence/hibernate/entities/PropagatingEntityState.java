package it.neverworks.persistence.hibernate.entities;

import it.neverworks.lang.Properties;

public class PropagatingEntityState extends EntityState {
    
    public PropagatingEntityState( Object entity, String[] names, Object[] values ) {
        super( entity, names, values );
    }
    
    public void set( String name, Object value ) {
        super.set( name, value );
        Properties.set( this.entity, name, value );
    }
    
}