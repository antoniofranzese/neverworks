package it.neverworks.persistence.hibernate;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import it.neverworks.io.Streams;

public class ClobAsciiStreamType extends AbstractLobType {

	@Override
	public Class returnedClass() {
		return InputStream.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.BLOB };
	}

	@Override
	protected Object nullSafeGetInternal(ResultSet rs, String[] names, Object owner, LobHandler lobHandler) throws SQLException, IOException, HibernateException {
		return process( lobHandler.getClobAsAsciiStream( rs, names[ 0 ] ) );
	}

	@Override
	protected void nullSafeSetInternal(PreparedStatement ps, int index,	Object value, LobCreator lobCreator) throws SQLException, IOException, HibernateException {
		lobCreator.setClobAsAsciiStream( ps, index, process( (InputStream) value ), 0 );
	}

    protected InputStream process( InputStream stream ) throws IOException {
        if( !this.blankable && stream != null ) {
            return Streams.plane( stream );
        } else {
            return stream;
        }
    }
    
}
