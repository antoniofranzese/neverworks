package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class FirstStatement extends SingleValueStatement {

	@Override
	protected void doWithValue( StatementFinder finder, Object value ) {
		if( value instanceof Number ) {
			finder.getCriteria().setFirstResult( ((Number)value).intValue() );
		}
	}
	
	@Override
	public String toString() {
		return "first " + value;
	}
	
}
