package it.neverworks.persistence.hibernate.entities;

import static it.neverworks.language.*;
import it.neverworks.persistence.Insert;
import it.neverworks.persistence.InsertEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.FlushEvent;
import org.hibernate.event.FlushEventListener;

/**
 * @author antonio
 *
 */
public class SessionFlushEventListener extends AbstractListener implements FlushEventListener {

	@Override
	public void onFlush( FlushEvent event ) {
        //System.out.println( "FLUSHING SESSION " );
	}
    
}
