package it.neverworks.persistence.hibernate.criteria;

import static it.neverworks.language.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.Functional;
import it.neverworks.lang.MapLike;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.log.Logger;
import it.neverworks.persistence.criteria.Filter;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.criteria.Transformer;
import it.neverworks.model.Value;

public class HibernateQuery<T> implements Query<T>, MapLike<String, Object>, Functional<T>, Wrapper {

	private final static Logger logger = Logger.of( HibernateQuery.class );

	private QueryFactory factory;
    private Arguments arguments = new Arguments();

	private Transformer transformer;
    private Integer max;
    private boolean cache = false;
    private String comment;

    protected HibernateQuery() {
        
    }
    
	public HibernateQuery( QueryFactory factory ) {
		this.factory = factory;
	}

	public HibernateQuery( QueryFactory factory, Object... params ) {
		this( factory );
		this.setAll( params );
	}

	public HibernateQuery( QueryFactory factory, Map params ) {
		this( factory );
		this.setAll( params );
	}

	@Override
	public FluentList<T> list() {
        return new FluentArrayList<T>( results() );
	}
    
    protected org.hibernate.Query buildQuery() {
        org.hibernate.Query query = this.factory.createQuery();
        
        if( arguments.hasKeywords() ) {
            List<String> parameters = Collections.list( query.getNamedParameters() );
        
            for( String name: arguments.keys() ) {
                Object value = arguments.get( name );
            
                boolean optional = false;
                boolean nullable = false;
 
                while( name.startsWith( "?" ) || name.startsWith( "!" ) ) {
                    if( name.startsWith( "?" ) ) {
                        optional = true;
                        name = name.substring( 1 );
                    } else if( name.startsWith( "!" ) ) {
                        nullable = true;
                        name = name.substring( 1 );
                    }
                }

                if( parameters.contains( name ) ) {

        			if( value instanceof Collection ) {
        				query.setParameterList( name, (Collection) value );

        			} else if( value instanceof BigDecimal ) {
        				query.setBigDecimal( name, (BigDecimal) value );

        			} else if( value instanceof BigInteger ) {
        				query.setBigInteger( name, (BigInteger) value );

        			} else if( value instanceof Boolean ) {
        				query.setBoolean( name, (Boolean) value );

        			} else if( value instanceof Calendar ) {
        				query.setCalendar( name, (Calendar) value );

        			} else if( value instanceof Timestamp ) {
        				query.setTimestamp( name, (Timestamp) value );

        			} else if( value instanceof Date ) {
        				query.setDate( name, (Date) value );

        			} else if( value instanceof DateTime ) {
        				query.setDate( name, ((DateTime) value).toDate() );

        			} else if( value instanceof Double ) {
        				query.setDouble( name, (Double) value );

        			} else if( value instanceof Float ) {
        				query.setFloat( name, (Float) value );

        			} else if( value instanceof Long ) {
        				query.setLong( name, (Long) value );

        			} else if( value instanceof Integer ) {
        				query.setInteger( name, (Integer) value );

        			} else if( value instanceof Short ) {
        				query.setShort( name, (Short) value );

        			} else if( value instanceof Byte ) {
        				query.setByte( name, (Byte) value );

        			} else if( value != null || nullable ) {
        				query.setParameter( name, value );
        			}

                } else if( ! optional ) {
                    throw new IllegalArgumentException( "Invalid query named parameter: " + name );
                }
            }
        }
        
        if( arguments.hasPositionals() ) {
            for( Integer index: arguments.indexes() ) {
                Object value = arguments.get( index );
            
        		if( value != null ) {
        			if( value instanceof Collection ) {
                        throw new UnsupportedFeatureException( "Positional Collection argument is not supported" );
 
        			} else if( value instanceof BigDecimal ) {
        				query.setBigDecimal( index, (BigDecimal) value );

        			} else if( value instanceof BigInteger ) {
        				query.setBigInteger( index, (BigInteger) value );

        			} else if( value instanceof Boolean ) {
        				query.setBoolean( index, (Boolean) value );

        			} else if( value instanceof Calendar ) {
        				query.setCalendar( index, (Calendar) value );

        			} else if( value instanceof Timestamp ) {
        				query.setTimestamp( index, (Timestamp) value );

        			} else if( value instanceof Date ) {
        				query.setDate( index, (Date) value );

        			} else if( value instanceof DateTime ) {
        				query.setDate( index, ((DateTime) value).toDate() );

        			} else if( value instanceof Double ) {
        				query.setDouble( index, (Double) value );

        			} else if( value instanceof Float ) {
        				query.setFloat( index, (Float) value );

        			} else if( value instanceof Long ) {
        				query.setLong( index, (Long) value );

        			} else if( value instanceof Integer ) {
        				query.setInteger( index, (Integer) value );

        			} else if( value instanceof Short ) {
        				query.setShort( index, (Short) value );

        			} else if( value instanceof Byte ) {
        				query.setByte( index, (Byte) value );

        			} else {
        				query.setParameter(index, value);
        			}
                }
            }
        }
        
        if( this.max != null ) {
    		query.setMaxResults( this.max );
        }

		if( this.transformer != null ) {
			query.setResultTransformer( new TransformerAdapter( transformer ) );
		}
        
        if( this.cache ) {
			query.setCacheable( true );
        }

        if( Strings.hasText( this.comment ) ) {
            query.setComment( this.comment );
        }
        
        return query;
    }
	@SuppressWarnings( "unchecked" )
    protected List<T> results() {
		return buildQuery().list();
	}

	@Override
	@SuppressWarnings( "unchecked" )
	public T result() {
		return (T) buildQuery().uniqueResult();
	}
    
    public Value value() {
        return Value.of( result() );
    }

    public Object asObject() {
        return list();
    }
    
	@Override
	public Query<T> set( String name, Object value ) {
        arguments.put( name, value );
		return this;
	}

	@Override
	public Query<T> setAll( Map<String, Object> params ) {
		for( String name : params.keySet() ) {
			set( name, params.get( name ) );
		}
		return this;
	}

	@Override
	public Query<T> setAll( List<Object> params ) {
		return setAll( params.toArray() );
	}

	@Override
	public Query<T> setAll( Object... params ) {
		if( params != null && params.length > 0 ) {
			for( int i = 0; i < params.length; i++ ) {
				arguments.arg( params[ i ] );
			}
		}
		return this;
	}

	@Override
	public Query<T> use( String... features ) {
		for( String feature : features ) {
			if( "cache".equalsIgnoreCase( feature ) ) {
                this.cache = true;
			}
		}
		return this;
	}

	@Override
	public Query<T> transform( Transformer transformer ) {
		this.transformer = transformer;
		return this;
	}

	@Override
	public Query<T> max( int elements ) {
        this.max = elements;
		return this;
	}

	@Override
	public void execute() {
		buildQuery().executeUpdate();
	}

	@Override
	public Object get( Object key ) {
		return arguments.get( str( key ) );
	}

	@Override
	public Object put( String key, Object value ) {
        Object previous = arguments.get( key, null );
		this.set( key, value );
		return previous;
	}

    public Query<T> comment( String comment ) {
        if( Strings.hasText( comment ) ) {
            this.comment = this.comment != null ? ( this.comment + " " + comment ) : comment;
        }
        return this;
    }

    public Iterator iterator() {
        return factory.iterate( buildQuery() );
    }
    
}
