package it.neverworks.persistence.hibernate.criteria;

public interface Statement {
	@SuppressWarnings( "rawtypes" )
	public void doWithFinder( StatementFinder finder );
    
}
