package it.neverworks.persistence.hibernate.criteria;

import java.util.List;
import java.util.Arrays;
import org.hibernate.transform.ResultTransformer;
import it.neverworks.model.features.Retrieve;
import it.neverworks.persistence.criteria.Transformer;

@SuppressWarnings("serial")
public class TransformerAdapter implements ResultTransformer {

	private Transformer transformer;
	
	public TransformerAdapter( Transformer transformer ) {
		this.transformer = transformer;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List transformList( List list ) {
		return transformer.transformList( list );
	}

	@Override
	public Object transformTuple( Object[] row, String[] names ) {
		return transformer.transformRow( row, names );
	}

}
