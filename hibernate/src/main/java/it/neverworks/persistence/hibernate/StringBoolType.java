package it.neverworks.persistence.hibernate;

import java.util.Properties;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.usertype.ParameterizedType;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.BooleanConverter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.utils.ToStringBuilder;

public class StringBoolType extends AbstractUserType implements ParameterizedType, Converter {

    private final static TypeDefinition<Boolean> BooleanType = TypeFactory.shared( Boolean.class );

    private String trueString = "T";
    private String falseString = "F";
    private boolean allowNulls = true;
    private boolean lenient = false;
    
    @Override    
    public Object convert( Object value ) {
        if( value instanceof Boolean ) {
            return value;

        } else if( value instanceof String ) {

            if( trueString.equalsIgnoreCase( (String) value ) ) {
                return Boolean.TRUE;
            } else if( falseString.equalsIgnoreCase( (String) value ) ) {
                return Boolean.FALSE;
            } else if( Strings.isEmpty( value ) ) {
                return null;
            } else {
                return BooleanType.convert( value );
            }
            
        } else {
            return BooleanType.convert( value );
        }
        
    }

	@Override
	public Class returnedClass() {
		return Boolean.class;
	}
    
	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.VARCHAR };
	}
    
    public void setParameterValues( Properties parameters ) {
        if( parameters != null ) {
            if( parameters.containsKey( "true" ) ) {
                this.trueString = parameters.getProperty( "true" );
            }
            if( parameters.containsKey( "false" ) ) {
                this.falseString = parameters.getProperty( "false" );
            }
            if( parameters.containsKey( "nulls" ) ) {
                this.allowNulls = "true".equalsIgnoreCase( parameters.getProperty( "nulls" ) );
            }
            if( parameters.containsKey( "lenient" ) ) {
                this.lenient = "true".equalsIgnoreCase( parameters.getProperty( "lenient" ) );
            }
        }
    } 

    public Object nullSafeGet( ResultSet rs, String[] names, Object owner ) throws HibernateException, SQLException {
        String value = rs.getString( names[ 0 ] );

        if( trueString.equalsIgnoreCase( value ) ) {
            return Boolean.TRUE;
        } else if( falseString.equalsIgnoreCase( value ) ) {
            return Boolean.FALSE;
        } else if( Strings.isEmpty( value ) ) {
            return allowNulls ? null : Boolean.FALSE;
        } else if( this.lenient ){
            return Boolean.FALSE;
        } else {
            throw new HibernateException( "Cannot map string to Boolean: " + value );
        }
        
    }
    
    public void nullSafeSet( PreparedStatement st, Object value, int index ) throws HibernateException, SQLException {
        if( value == null && allowNulls ) {
            st.setString( index, null );
        } else {
            st.setString( index, Boolean.TRUE.equals( value ) ? trueString : falseString );
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "true", this.trueString )
            .add( "false", this.falseString )
            .toString();
    }
}