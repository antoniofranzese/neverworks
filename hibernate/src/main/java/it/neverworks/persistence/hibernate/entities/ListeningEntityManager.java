package it.neverworks.persistence.hibernate.entities;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;
import it.neverworks.log.Logger;

import org.hibernate.Interceptor;
import org.hibernate.SessionFactory;
import org.hibernate.impl.SessionFactoryImpl;
import org.hibernate.event.EventListeners;
import org.hibernate.event.PostLoadEventListener;
import org.hibernate.event.PreInsertEventListener;
import org.hibernate.event.PreUpdateEventListener;
import org.hibernate.event.PreDeleteEventListener;
import org.hibernate.event.SaveOrUpdateEventListener;
import org.hibernate.event.FlushEntityEventListener;
import org.hibernate.event.FlushEventListener;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.FluentList;
import it.neverworks.model.events.Event;
import it.neverworks.model.description.Setter;
import it.neverworks.persistence.hibernate.criteria.capability.CustomCriterion;
import it.neverworks.persistence.hibernate.criteria.DelegatingStatementBody;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.Listeners;
import it.neverworks.persistence.PostListener;
import it.neverworks.persistence.OverrideListeners;
import it.neverworks.persistence.PersistenceException;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.entities.EntityManager;
import it.neverworks.persistence.PersistenceEventFactory;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.ConfigureEvent;
import it.neverworks.persistence.Configure;
import it.neverworks.persistence.EntityTypeAware;
import it.neverworks.persistence.ClassHandler;
import it.neverworks.persistence.Find;
import it.neverworks.persistence.Inquire;
import it.neverworks.persistence.Create;
import it.neverworks.persistence.CriteriaDef;
import it.neverworks.persistence.QueryDef;
import it.neverworks.persistence.Veto;
import it.neverworks.persistence.Delete;

public class ListeningEntityManager extends SimpleEntityManager {

    private static final Logger logger = Logger.of( HibernateEntityManager.class );
    
    private List listeners = list( 
        new EntityLoadEventListener()            
        ,new EntityInsertEventListener()            
        ,new EntityUpdateEventListener()            
        ,new EntityDeleteEventListener()
        ,new EntitySaveEventListener()
        // ,new SessionFlushEventListener()
        // ,new EntityFlushEventListener()
    ); 
    
    @Override
    protected Interceptor createSessionInterceptor() {
        return new ListeningSessionInterceptor();
    }

    @Override
    protected void processMetadata( HibernateMetadata meta ) {
        // Nothing to do
    }

    @Override
    protected <T> T newInstance( Class<T> type, Arguments arguments ) {
        if( listening( type, Create.class ) ) {
            LazyCreateEvent event = new LazyCreateEvent( this, type, arguments );
            notify( type, describe( type, Create.class ), new PreBuiltEventFactory( event ) );
            return (T) event.getEntity();
        } else {
            return super.newInstance( type, arguments );
        }
    }

    /* package */ <T> T buildInstance( Class<T> type, Arguments arguments ) {
        return super.newInstance( type, arguments );
    }
    
    @Override
	public <T> T delete( T entity ){
        T unpacked = (T) unpack( entity );
        if( getSession().contains( unpacked ) ) {
            return super.delete( unpacked );
        } else {
            try {
                this.notify( unpacked, Delete.class, EntityDeleteEventListener.FACTORY );
                return super.delete( unpacked );
            } catch( Veto v ) {
                // Nothing to do 
                return unpacked;
            }
        }
    }

    @Override
    public <T> Finder<T> find( Class<T> type ) {
        Finder<T> finder = null;
        if( listening( type, Find.class ) ) {
            LazyFindEvent event = new LazyFindEvent( this, type );
            notify( type, describe( type, Find.class ), new PreBuiltEventFactory( event ), ( entity, meta, factory ) -> {
                if( meta.signature == MethodMetadata.Signature.SINGLE && Finder.class.isAssignableFrom( meta.parameter ) ) {
                    Object owner = meta.owner != null ? meta.owner : entity;
                    Reflection.invokeMethod( owner, meta.method, event.getFinder() );
                    return true;
                } else {
                    throw new PersistenceException( "Unusable @Find signature on method " + meta.method );
                }
            });

            return event.<T>getFinder();
        } else {
            return buildFinder( type );
        }
    }
    
    /* package */ <T> Finder<T> buildFinder( Class<T> type ) {
        return ((StatementFinder<T>) super.find( type ) )
            .addCapabilities( describe( type ).customCriteria );
    }

    @Override
    public <T> Query<T> query( Class<T> type, String body ) {
        if( listening( type, Inquire.class ) ) {
            LazyInquireEvent event = new LazyInquireEvent( this, type, body );
            notify( type, describe( type, Inquire.class ), new PreBuiltEventFactory( event ) );
            return event.<T>getQuery();
        } else {
            return buildQuery( type, body );
        }
    }
    
    /* package */ <T> Query<T> buildQuery( Class<T> type, String body ) {
        ClassMetadata classMeta = describe( type );
        if( classMeta.queries.containsKey( body ) ) {
            MethodMetadata methodMeta = ((QueryMetadata) classMeta.queries.get( body )).method;
            Object owner = methodMeta.owner != null ? methodMeta.owner : type;
            Event event = null;

            Object result = null;
            switch( methodMeta.signature ) {
                //TODO: Event?
                case MANAGER:
                    result = Reflection.invokeMethod( owner, methodMeta.method, this );
                    break;
                case CLASS:
                    result = Reflection.invokeMethod( owner, methodMeta.method, type );
                    break;
                case EVENT:
                    if( event == null ) {
                        event = new LazyInquireEvent( this, type, body );
                    }
                    result = Reflection.invokeMethod( owner, methodMeta.method, event );
                    break;
                default:
                    result = Reflection.invokeMethod( owner, methodMeta.method );
            }
            
            if( result instanceof Query ) {
                return (Query<T>) result;
            } else if( result instanceof String ) {
                return super.query( type, (String) result );
            } else {
                throw new PersistenceException( "Invalid @QueryDef '" + body + "' result: " + repr( result ) );
            }
            
        } else {
            return super.query( type, body );
        }
        
    }
    
    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();

        for( Object listener: this.listeners ) {
            register( listener );
        }

        for( Class type: getAllMappedClasses() ) {
            if( listening( type, Configure.class ) ) {
                ConfigureEvent event = new ConfigureEvent()
                    .set( "target", type )
                    .set( "manager", this );
                notify( type, describe( type, Configure.class ), new PreBuiltEventFactory( event ) );
            }
        }
    
    }
    
    public void register( Object listener ) {
        if( listener instanceof EntityListener ) {
            ((EntityListener) listener).setManager( this );
        }
        super.register( listener );
    }
    
    public boolean listening( Class entityClass, Class<? extends Annotation> annotationClass ) {
        return describe( entityClass, annotationClass ).methods.size() > 0;
    }

    public boolean notify( Object entity, Class<? extends Annotation> annotationClass, PersistenceEventFactory eventFactory ) {
        return notify( entity, describe( entity.getClass(), annotationClass ), eventFactory );
    }

    protected boolean notify( Object entity, EventMetadata eventMeta, PersistenceEventFactory eventFactory ) {
        return notify( entity, eventMeta, eventFactory, null );
    }
    
    protected boolean notify( Object entity, EventMetadata eventMeta, PersistenceEventFactory eventFactory, EventNotifier eventNotifier ) {
        PersistenceEvent event = null;
        boolean notified = false;
        
        for( MethodMetadata methodMeta: eventMeta.methods ) {
            notified = true;
            Object owner = methodMeta.owner != null ? methodMeta.owner : entity;

            switch( methodMeta.signature ) {
                case EMPTY:
                    Reflection.invokeMethod( owner, methodMeta.method );
                    break;
                case ENTITY:
                    Reflection.invokeMethod( owner, methodMeta.method, entity );
                    break;
                case MANAGER:
                    Reflection.invokeMethod( owner, methodMeta.method, this );
                    break;
                case EVENT:
                    if( event == null ) {
                        event = eventFactory.build();
                        event.set( "entity", entity );
                        event.set( "manager", this );
                    }
                    Reflection.invokeMethod( owner, methodMeta.method, event );
                    break;
                case CLASS:
                    Reflection.invokeMethod( owner, methodMeta.method, entity instanceof Class ? entity : entity.getClass() );
                    break;
                default:
                    if( eventNotifier != null ) {
                        return eventNotifier.notify( entity, methodMeta, eventFactory );
                    } else {
                        throw new PersistenceException( "Unusable @" + eventMeta.target.getSimpleName() + " signature on method " + methodMeta.method );
                    }
                    
            }
        }
        
        return notified;
    }
    
    private static interface EventNotifier {
        boolean notify( Object entity, MethodMetadata methodMeta, PersistenceEventFactory eventFactory );
    }
    
    protected EventMetadata describe( Class<?> entityClass, Class<? extends Annotation> annotationClass ) {
        ClassMetadata classMeta = describe( entityClass );

        if( ! classMeta.events.containsKey( annotationClass ) ) {
            classMeta.events.put( annotationClass, parseEvent( classMeta, annotationClass) );
        }
        
        return (EventMetadata) classMeta.events.get( annotationClass );
    }

    protected EventMetadata parseEvent( ClassMetadata classMeta, Class<? extends Annotation> annotationClass ) {
        return parseEvent( classMeta, annotationClass, /*allowEmpty*/ false );
    }
    
    protected EventMetadata parseEvent( ClassMetadata classMeta, Class<? extends Annotation> annotationClass, boolean allowEmpty ) {
        Class<?> entityClass = classMeta.target;
        EventMetadata eventMeta = new EventMetadata();
        eventMeta.target = annotationClass;
        
        // TODO: istanziare pre e post listeners solo se necessario
        
        // PreListeners
        for( Object preListener: classMeta.preListeners ) {
            for( Method method: Reflection.findMethods( preListener.getClass() ) ) {
                if( method.isAnnotationPresent( annotationClass ) ) {
                    MethodMetadata methodMeta = parseMethod( entityClass, annotationClass, method, preListener, allowEmpty );
                    eventMeta.methods.add( methodMeta );
                }
            }
        }
        
        // Entity callbacks
        for( Method method: Reflection.findMethods( entityClass ) ) {
            if( method.isAnnotationPresent( annotationClass ) ) {
                MethodMetadata methodMeta = parseMethod( entityClass, annotationClass, method, null, allowEmpty );
                eventMeta.methods.add( methodMeta );
            }
        }
        
        // PostListeners
        for( Object postListener: classMeta.postListeners ) {
            for( Method method: Reflection.findMethods( postListener.getClass() ) ) {
                if( method.isAnnotationPresent( annotationClass ) ) {
                    MethodMetadata methodMeta = parseMethod( entityClass, annotationClass, method, postListener, allowEmpty );
                    eventMeta.methods.add( methodMeta );
                }
            }
        }
        
        return eventMeta;
        
    }

     protected MethodMetadata parseMethod( Class<?> entityClass,  Class<? extends Annotation> annotationClass, Method method, Object owner, boolean allowEmpty ) {
        // System.out.println( "PARSE " + method + " for " + owner + ", entity " + entityClass );
        
        MethodMetadata methodMeta = new MethodMetadata();
        methodMeta.method = method;
        
        boolean classHandler = annotationClass.isAnnotationPresent( ClassHandler.class );
        
        if( Modifier.isStatic( method.getModifiers() ) ) {
            methodMeta.owner = method.getDeclaringClass();
        } else { 
            if( owner == null && classHandler ){
                throw new PersistenceException( "Method " + method + " must be static on entity class" );
            }
            methodMeta.owner = owner;
        }
        
        if( method.getParameterTypes().length == 0 ) {
            if( ! classHandler || allowEmpty ) {
                methodMeta.signature = MethodMetadata.Signature.EMPTY;
            } else {
                throw new PersistenceException( "Method " + method + " cannot accept nothing for @" + annotationClass.getSimpleName() );
            }
        
        } else if( method.getParameterTypes().length == 1 ) {
            Class paramType = method.getParameterTypes()[0];
            
            if( paramType.isAssignableFrom( entityClass ) ) {
                if( ! classHandler ) {
                    methodMeta.signature = MethodMetadata.Signature.ENTITY;
                } else {
                    throw new PersistenceException( "Method " + method + " cannot accept entity for @" + annotationClass.getSimpleName() );
                }

            } else if( EntityManager.class.isAssignableFrom( paramType ) ) {
                methodMeta.signature = MethodMetadata.Signature.MANAGER;
                
            } else if( Event.class.isAssignableFrom( paramType ) ) {
                methodMeta.signature = MethodMetadata.Signature.EVENT;

            } else if( Class.class.isAssignableFrom( paramType ) ) {
                methodMeta.signature = MethodMetadata.Signature.CLASS;
            
            } else {
                methodMeta.signature = MethodMetadata.Signature.SINGLE;
                methodMeta.parameter = paramType;
            }
            
        } 

        if( methodMeta.signature == null ) {
            methodMeta.signature = MethodMetadata.Signature.MULTIPLE;
            methodMeta.parameters = method.getParameterTypes();
        }

        Reflection.makeAccessible( method );
        return methodMeta;
        
    }
    
    protected ClassMetadata describe( Class<?> entityClass ) {
        if( ! classes.containsKey( entityClass ) ) {
            ClassMetadata meta = new ClassMetadata();
            meta.target = entityClass;
            
            // Annotation listeners
            List<Class<?>> classListeners = new ArrayList<Class<?>>();
            Class<?> current = entityClass;
            do {

                List<Class<?>> currentListeners = new ArrayList<Class<?>>();
                Listeners annotation = current.getAnnotation( Listeners.class );
                if( annotation != null ) {

                    for( Class<?> listenerClass: annotation.value() ) {
                        if( ! classListeners.contains( listenerClass ) ) {
                            currentListeners.add( 0, listenerClass );
                        }
                    }
                    
                    for( Class<?> listenerClass: currentListeners ) {
                        classListeners.add( 0, listenerClass );
                    }
                }

                current = current.getSuperclass();

            } while( current != Object.class && ! current.isAnnotationPresent( OverrideListeners.class ) );
            
            
            //System.out.println( "Listeners for " + entityClass.getSimpleName() + ": " + classListeners );
            
            // Pre/Post Listeners
            for( Class<?> classListener: classListeners ) {
                Object listenerInstance = Reflection.newInstance( classListener );
                if( listenerInstance instanceof EntityTypeAware ) {
                    ((EntityTypeAware) listenerInstance).setEntityType( entityClass );
                }
                if( classListener.isAnnotationPresent( PostListener.class ) ) {
                    meta.postListeners.add( listenerInstance );
                } else {
                    meta.preListeners.add( listenerInstance );
                }
            }

            parseCriteria( meta );    
            parseQueries( meta );    
            classes.put( entityClass, meta );
            
        }
        
        return (ClassMetadata) classes.get( entityClass );
    }
    
    protected void parseCriteria( ClassMetadata classMeta ) {
        EventMetadata criteriaDefs = parseEvent( classMeta, CriteriaDef.class );
        FluentList<CustomCriterion> result = list();
        
        for( MethodMetadata methodMeta: criteriaDefs.methods ) {
            Method method = methodMeta.method;
            Class<?>[] types = method.getParameterTypes();
            if( ( types.length == 2 
                    && Finder.class.isAssignableFrom( types[ 0 ] )
                    && Arguments.class.isAssignableFrom( types[ 1 ] ) )
                || ( types.length == 3 
                    && Class.class.isAssignableFrom( types[ 0 ] )
                    && Finder.class.isAssignableFrom( types[ 1 ] )
                    && Arguments.class.isAssignableFrom( types[ 2 ] ) )
                || ( types.length == 1 
                    && Event.class.isAssignableFrom( types[ 0 ] ) )
                ) {
                
                CriteriaDef def = method.getAnnotation( CriteriaDef.class );
                String name = Strings.hasText( def.name() ) ? def.name() : method.getName();
                CustomCriterion criterion = new CustomCriterion( name );
                criterion.set( "body", new DelegatingStatementBody( 
                    methodMeta.owner != null ? methodMeta.owner : classMeta.target
                    ,method 
                    ,types.length == 2 ? null : classMeta.target
                    ,types.length == 1
                ));
                result.add( criterion );
                
            } else {
                throw new PersistenceException( "Unusable @CriteriaDef signature on method " + methodMeta.method );
                
            }
        }
        
        classMeta.customCriteria = result.array( CustomCriterion.class );
    }

    protected void parseQueries( ClassMetadata classMeta ) {
        EventMetadata criteriaDefs = parseEvent( classMeta, QueryDef.class, /*allowEmpty*/ true );

        for( MethodMetadata methodMeta: criteriaDefs.methods ) {
            Method method = methodMeta.method;
            
            if( Query.class.isAssignableFrom( method.getReturnType() ) 
                || method.getReturnType().equals( String.class ) ) {

                if( methodMeta.signature == MethodMetadata.Signature.EMPTY 
                    || methodMeta.signature == MethodMetadata.Signature.CLASS
                    || methodMeta.signature == MethodMetadata.Signature.EVENT
                    || methodMeta.signature == MethodMetadata.Signature.MANAGER ) {
                    
                    QueryDef def = method.getAnnotation( QueryDef.class );
                    String name = Strings.hasText( def.name() ) ? def.name() : method.getName();
                    
                    if( ! classMeta.queries.containsKey( name ) ) {
                        QueryMetadata queryMeta = new QueryMetadata();
                        queryMeta.method = methodMeta;
                        classMeta.queries.put( name, queryMeta);

                    } else {
                        Method existing = ((MethodMetadata) classMeta.queries.get( name )).method;
                        throw new PersistenceException( 
                            "Duplicate @QueryDef '" + name + "' " 
                            + "on " + method.getDeclaringClass().getName() + "." + method.getName() 
                            + "and " + existing.getDeclaringClass().getName() + "." + existing.getName() 
                        );
                    }

                } else {
                    throw new PersistenceException( "Unusable @QueryDef signature on method " + methodMeta.method );
                }

            } else {
                throw new PersistenceException( "@CriteriaDef method " + methodMeta.method + " must return String, Query or subclass" );
            } 

        }
    }    
    
    protected Map classes = new HashMap();
    
    private static class ClassMetadata {
        public Class<?> target;
        public List preListeners = new ArrayList();
        public List postListeners = new ArrayList();
        public CustomCriterion[] customCriteria;
        public Map events = new HashMap();
        public Map queries = new HashMap();
    }
    
    private static class EventMetadata {
        public Class<? extends Annotation> target;
        public List<MethodMetadata> methods = new ArrayList<MethodMetadata>();
    }
    
    private static class MethodMetadata {
        public enum Signature {
            EMPTY, ENTITY, MANAGER, EVENT, CLASS, SINGLE, MULTIPLE
        }
        
        public Method method;
        public Object owner = null;
        public Signature signature;
        public Class parameter;
        public Class[] parameters;
    }
    
    private static class QueryMetadata {
        public MethodMetadata method;
    }
    
    public List getListeners(){
        return this.listeners;
    }
    
    public void setListeners( List listeners ){
        this.listeners = listeners;
    }
    
}