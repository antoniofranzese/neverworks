package it.neverworks.persistence.hibernate.criteria;

import it.neverworks.persistence.criteria.Finder;
import org.hibernate.criterion.Criterion;

public interface HibernateFinder<T> extends Finder<T> {
    public HibernateCriteria getCriteria();
    public HibernateFinder<T> addCriterion( Criterion criterion );
    public String translateProperty( String property );
    public Phase getPhase( String name );
    public HibernateFinder<T> on( String name, Operation operation );
    
    /* Covariant return type overrides */
    public HibernateFinder<T> with( String name );
    public HibernateFinder<T> with( String name, Object value );
    public HibernateFinder<T> without( String name );
    
	public HibernateFinder<T> use( Iterable<String> features );
	public HibernateFinder<T> use( String... features );
    
}