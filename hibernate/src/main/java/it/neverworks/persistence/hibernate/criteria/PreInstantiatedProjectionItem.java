package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;

import it.neverworks.log.Logger;
import it.neverworks.model.utils.Identification;

@Identification({ "projection" })
public class PreInstantiatedProjectionItem implements ProjectionItem {

    private static Logger logger = Logger.of( StatementFinder.class );

    private Projection projection;
    
    public PreInstantiatedProjectionItem( Projection projection ) {
        this.projection = projection;
    }

    public ProjectionList populate( ProjectionList list ) {
		logger.trace( "Adding projection {}", this.projection );
        list.add( this.projection );
        return list;
    }
    
    public Projection getProjection(){
        return this.projection;
    }
}