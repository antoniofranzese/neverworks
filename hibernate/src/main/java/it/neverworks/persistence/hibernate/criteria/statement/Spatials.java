package it.neverworks.persistence.hibernate.criteria.statement;

import com.vividsolutions.jts.geom.Geometry;
import it.neverworks.persistence.spatial.JTSGeometryConverter;

import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;

public abstract class Spatials extends AbstractStatement {
    
    protected final static TypeDefinition<Geometry> GEOMETRY_TYPE = TypeFactory.build( Geometry.class, new JTSGeometryConverter() );
    protected final static String TYPE_MESSAGE = "{0} criteria";

    public static Object convert( Object value ) {
        return GEOMETRY_TYPE.convert( value );
    }

    public static Geometry process( String property, Object value ) {
        return GEOMETRY_TYPE.process( value, TYPE_MESSAGE, property );
    }

}

