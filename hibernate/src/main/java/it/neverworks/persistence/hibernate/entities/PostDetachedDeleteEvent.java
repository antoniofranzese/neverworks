package it.neverworks.persistence.hibernate.entities;

import java.io.Serializable;
import org.hibernate.event.AbstractEvent;
import org.hibernate.event.EventSource;

public class PostDetachedDeleteEvent extends PreDetachedDeleteEvent {
    
    public PostDetachedDeleteEvent( Class<?> type, Serializable id, EventSource session ) {
        super( type, id, session );
    }
    
}