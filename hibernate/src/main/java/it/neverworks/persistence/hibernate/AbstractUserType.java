package it.neverworks.persistence.hibernate;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;
import org.hibernate.util.EqualsHelper;

public abstract class AbstractUserType implements UserType {

    public boolean isMutable() {
        return false;
    }

    public boolean equals( Object x, Object y ) throws HibernateException {
        return EqualsHelper.equals(x, y);
    }

    public int hashCode( Object x ) throws HibernateException {
        return x.hashCode();
    }

    public Object deepCopy( Object value ) throws HibernateException {
        return value;
    }

    public Serializable disassemble( Object value ) throws HibernateException {
        return (Serializable)value;
    }

    public Object assemble( Serializable cached, Object owner ) throws HibernateException {
        return cached;
    }

    public Object replace( Object original, Object target, Object owner ) throws HibernateException {
      return original;
    }
    
    public abstract Object nullSafeGet( ResultSet rs, String[] names, Object owner ) throws HibernateException, SQLException;
    public abstract void nullSafeSet( PreparedStatement st, Object value, int index ) throws HibernateException, SQLException;
    
}