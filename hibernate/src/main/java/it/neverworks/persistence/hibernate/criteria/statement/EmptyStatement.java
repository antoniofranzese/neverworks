package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import it.neverworks.model.Property;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import org.hibernate.criterion.Criterion;

public class EmptyStatement extends BaseDisjunctiveStatement {
	
	@Override
	public void doWithFinder(StatementFinder finder) {
        Criterion criterion = createCriterion( finder );
        if( criterion != null ) {
    		finder.getCriteria().add( criterion );
        }
	}

    protected Criterion createOwnCriterion( StatementFinder finder ) {
        return null;
    }

}
