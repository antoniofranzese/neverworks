package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Projections;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.LongType;
import org.hibernate.type.AnyType;
import org.hibernate.type.Type;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.PropertyProjectionItem;
import it.neverworks.persistence.hibernate.criteria.PreInstantiatedProjectionItem;

public class ProjectionPropertyStatement extends AbstractStatement  {
	
    @Property
    private String property;
    
    @Override 
    public void doWithFinder( StatementFinder finder ) {
        
        int colon = property.indexOf( ":" );
        if( colon > 0 ) {
            String spec = property.substring( 0, colon ).toLowerCase();
            String sql = property.substring( colon + 1 );
            String alias = "expr" + Strings.generateHexID( 8 ) + "_";
            Type type = null;
            
            if( "sql".equals( spec ) ) {
                type = new AnyType();
            
            // } else if( "integer".equals( spec ) ) {
            //     type = new IntegerType();
            //
            // } else if( "long".equals( spec ) ) {
            //     type = new LongType();
            //
            // } else if( "string".equals( spec ) ) {
            //     type = new StringType();

            } else {
                throw new IllegalArgumentException( "Unknown projection specification: " + spec );
            }

            finder.addProjection( new PreInstantiatedProjectionItem(
                Projections.sqlProjection( sql + " as " + alias, new String[]{ alias }, new Type[]{ type } )
            ));
        
        } else {
            StatementFinder.PropertyInfo info = finder.parseProperty( property, finder.attribute( "__useOuterJoinAsDefault" ).isTrue() || finder.attribute( "__useOuterJoinInSelect" ).isTrue() );
    		String propertyName = info.name;
    		String alias = finder.cleanProperty( property );
    		finder.addProjection( new PropertyProjectionItem( propertyName, alias ) );
        }
        
	}
	
    @Override 
    public String toString() {
		return "projection property: " + property;
	}
    
}