package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import org.hibernate.criterion.Criterion;
import org.hibernatespatial.criterion.SpatialRestrictions;
import com.vividsolutions.jts.geom.Geometry;

public class SpatialDisjointStatement extends BinaryCriteriaStatement {

    protected Criterion createOwnCriterion( StatementFinder finder ) {
        Geometry geometry = Spatials.process( this.property, Value.from( value ) );
        return geometry != null ? checkNot( SpatialRestrictions.disjoint( finder.translateProperty( property ), geometry ) ): null;
    }

	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " intersects " + value;
	}
}
