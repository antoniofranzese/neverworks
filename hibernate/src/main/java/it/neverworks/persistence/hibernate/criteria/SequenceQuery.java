package it.neverworks.persistence.hibernate.criteria;

import java.util.List;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Connection;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.resolver.DialectResolver;
import org.hibernate.dialect.resolver.StandardDialectResolver;
import org.hibernate.jdbc.Work;

import static it.neverworks.language.*;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.Errors;
import it.neverworks.persistence.hibernate.entities.HibernateEntityManager;

public class SequenceQuery<T> extends HibernateQuery implements Work {
    
    private HibernateEntityManager manager;
    private String name;
    private Long nextValue;
    
    public SequenceQuery( HibernateEntityManager manager, String name ) {
        this.manager = manager;
        this.name = name;
    }
    
    public void execute( Connection connection ) throws SQLException {
        DialectResolver dialectResolver = new StandardDialectResolver();
        Dialect dialect =  dialectResolver.resolveDialect( connection.getMetaData() );
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            ps = connection.prepareStatement( dialect.getSequenceNextValString( this.name ) );
            rs = ps.executeQuery();
            rs.next();
            this.nextValue = rs.getLong( 1 );

        } finally {
            if( rs != null ) {
                rs.close();
            }
            if( ps != null ) {
                ps.close();
            }
        }
    }

    @Override
    public Object asObject() {
        return result();
    }
    
	@Override
	public FluentList<T> list() {
        return new FluentArrayList<T>().append( result() );
	}
    
	@Override
	@SuppressWarnings("unchecked")
    protected List<T> results() {
        return (List<T>) list();
	}

	@Override
	@SuppressWarnings("unchecked")
	public T result() {
        this.manager.getSession().doWork( this );
		return (T) this.nextValue;
	}
    
}