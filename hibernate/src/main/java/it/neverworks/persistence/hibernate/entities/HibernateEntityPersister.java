package it.neverworks.persistence.hibernate.entities;

import java.util.Map;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.persistence.entities.EntityPersister;
import it.neverworks.persistence.entities.EntityMetadata;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.criteria.Finder;

public class HibernateEntityPersister<T> implements EntityPersister<T> {
    
    private HibernateEntityManager manager;
    private Class<T> type;
    
    public HibernateEntityPersister( HibernateEntityManager manager, Class<T> type ) {
        this.manager = manager;
        this.type = type;
    }
    
	public T create( Object... params ) {
	    return (T) this.manager.create( this.type, params );
	}
    
	public T create( Map params ) {
	    return (T) this.manager.create( this.type, params );
	}

    public Query<T> query( String body, Map params ) {
        return this.manager.query( this.type, body, params );
    }
    
    public Query<T> query( String body, Object... params ) {
        return this.manager.query( this.type, body, params );
    }
    
    public Query<T> query( String body ) {
        return this.manager.query( this.type, body );
    }
    
    public Finder<T> find() {
        return this.manager.find( this.type );
    }
    
	public T load( Object key ) {
	    return this.type.isInstance( key ) ? this.manager.load( (T) key ) : this.manager.load( this.type, key );
	}

	public void save( T entity ) {
	    this.manager.save( entity );
	}
    
	public Object insert( T entity ) {
	    return this.manager.insert( entity );
	}
    
	public void update( T entity ) {
	    this.manager.update( entity );
	}
    
	public void delete( T entity ){
	    this.manager.delete( entity );
	}
    
    public EntityMetadata metadata() {
        return this.manager.metadata( this.type );
    }
    
    public void forget( T entity ) {
        this.manager.forget( entity );
    }

    public T refresh( T entity ) {
        return this.manager.refresh( entity );
    }
    
    public T merge( T entity ) {
        return this.manager.merge( entity );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "on", this.type.getName() )
            .toString();
    }
    
}