package it.neverworks.persistence.hibernate.criteria;

public class OperationStatement implements Statement {
    
    private Operation operation;
    
    public OperationStatement( Operation operation ) {
        this.operation = operation;
    }
    
	public void doWithFinder( StatementFinder finder ) {
	    this.operation.doWithFinder( finder );
	}
    
}