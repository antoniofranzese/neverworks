package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;

import it.neverworks.persistence.hibernate.criteria.Capability;
import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementBody;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.statement.CustomBodyStatement;

public class CustomCriterion extends AbstractCapability {

	public CustomCriterion() {
		super();
	}

	public CustomCriterion( String name ) {
		super( name );
	}

	public CustomCriterion( String name, StatementBody body ) {
		super( name );
        this.body = body;
	}

    @Property 
	protected StatementBody body;
	
	@Override
	public void doWithFinder( StatementFinder finder, Arguments arguments ) {
		finder.getPhase( "criteria" ).append( (Statement) new CustomBodyStatement()
            .set( "body", body )
            .set( "arguments", arguments )
        );
	}

}
