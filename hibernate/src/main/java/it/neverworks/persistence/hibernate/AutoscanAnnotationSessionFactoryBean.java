package it.neverworks.persistence.hibernate;

import it.neverworks.lang.Errors;
import it.neverworks.lang.ClassScanner;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.Reflection;

import java.lang.annotation.Annotation;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernatespatial.SpatialDialect;
import it.neverworks.log.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.classreading.SimpleMetadataReaderFactory;
import org.springframework.orm.hibernate3.annotation.AnnotationSessionFactoryBean;
import org.springframework.util.ClassUtils;

public class AutoscanAnnotationSessionFactoryBean extends AnnotationSessionFactoryBean {

	private static Logger logger = Logger.of( AutoscanAnnotationSessionFactoryBean.class );

	protected ResourcePatternResolver resourcePatternResolver; 
	protected MetadataReaderFactory metadataReaderFactory = new SimpleMetadataReaderFactory( resourcePatternResolver );
	protected Set<String> includes;
	protected Set<String> excludes;
	
	String basePackage = null;
	String resourcePattern = "**/*.class";
    protected ClassLoader _beanClassLoader;

	public AutoscanAnnotationSessionFactoryBean() {
		super();
		includes = new HashSet<String>();
		includes.add( "javax.persistence.Entity" );
		includes.add( "javax.persistence.Embeddable" );
		excludes = new HashSet<String>();
	}

    public void setBeanClassLoader( ClassLoader loader ) {
        _beanClassLoader = loader;
        super.setBeanClassLoader( loader );
    }
    
	@Override
	protected void postProcessAnnotationConfiguration( AnnotationConfiguration config) throws HibernateException {

        config.addPackage( "it.neverworks.persistence.hibernate" );
        
        if( System.getProperty( "hibernate.spatial.dialect" ) == null ) {
            String dialect = getHibernateProperties().getProperty( "hibernate.dialect" );
            if( dialect != null ) {
                Class dialectClass = Reflection.findClass( dialect );
                if( dialectClass != null && SpatialDialect.class.isAssignableFrom( dialectClass ) ) {
                    logger.info( "Using dialect as spatial dialect: {}", dialect );
                    System.setProperty( "hibernate.spatial.dialect", dialect );
                }
            }
        }

		try {
			String packageSearchPath = 
				ClassUtils.convertClassNameToResourcePath( basePackage != null ? basePackage : "" )
				+ "/" 
				+ resourcePattern;

			ClassScanner scanner = new ClassScanner( _beanClassLoader );
            for( Class entityClass: scanner.scanClasses( packageSearchPath ) ) {
                
                if( isCompatible( entityClass ) ) {
                    config.addAnnotatedClass( entityClass );
                    logger.debug( "Added class: {}", entityClass.getName() );
                }

			}
            
		} catch (Exception e) {
			Errors.lower( e );
		
		}
			
		super.postProcessAnnotationConfiguration(config);
	}
    
	protected boolean isCompatible( Class cls ) {
        for( Annotation annotation: Annotations.all( cls ) ) {
            String name = annotation.annotationType().getName();

			if( excludes.contains( name ) ) {
				return false;
			} else if( includes.contains( name ) ) {
				return true;
			}
        }

        return false;
	}
	
	public void setBasePackage(String basePackage) {
		this.basePackage = basePackage;
	}
	
	public void setResourcePattern(String resourcePattern) {
		this.resourcePattern = resourcePattern;
	}

	public void setIncludes(Set<String> includes) {
		this.includes = includes;
	}

	public void setExcludes(Set<String> excludes) {
		this.excludes = excludes;
	}

	
}
