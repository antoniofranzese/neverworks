package it.neverworks.persistence.hibernate.entities;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class ColumnDescriptor extends BaseModel {
    
    @Property
    private String name;
    
    @Property
    private Boolean plain = true;
    
}