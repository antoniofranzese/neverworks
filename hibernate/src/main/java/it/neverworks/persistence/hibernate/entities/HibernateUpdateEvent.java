package it.neverworks.persistence.hibernate.entities;

import it.neverworks.model.Property;
import it.neverworks.persistence.UpdateEvent;

public class HibernateUpdateEvent extends UpdateEvent {
    
    @Property
    private EntityState state;
    
    public EntityState getState(){
        return this.state;
    }
    
    public void setState( EntityState state ){
        this.state = state;
    }
    
}