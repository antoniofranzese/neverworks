package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class EqualStatement extends BinaryCriteriaStatement {

    protected Criterion createOwnCriterion( StatementFinder finder ) {
        Object val = Value.from( value );
        if( val != null ) {
            if( this.negation ) {
                return Restrictions.ne( finder.translateProperty( property ), val );
            } else {
                return Restrictions.eq( finder.translateProperty( property ), val );
            }
        } else {
            return null;
        }
    }

	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " equals " + value;
	}
}
