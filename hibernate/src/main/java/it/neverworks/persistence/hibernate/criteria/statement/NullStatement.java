package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;

public class NullStatement extends UnaryCriteriaStatement {

	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
		return this.negation 
            ? Restrictions.isNotNull( finder.translateProperty( property ) )
            : Restrictions.isNull( finder.translateProperty( property ) );
	}

	@Override
	public String toString() {
		return property + " is" + ( this.negation ? " not" : "" ) + "null";
	}
	
}
