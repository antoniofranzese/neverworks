package it.neverworks.persistence.hibernate.entities;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Field;
import java.lang.annotation.Annotation;

import org.hibernate.type.Type;
import org.hibernate.usertype.UserType;
import org.hibernate.metadata.CollectionMetadata;

import it.neverworks.lang.Strings;
import it.neverworks.model.utils.CoreExpandoModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.utils.Identification;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;

@Identification( common = { "name", "owner" }, string = { "index", "type", "!converter", "!userType", "!association" } )
public class HibernatePropertyDescriptor extends CoreExpandoModel implements EntityPropertyDescriptor {
    
    @Property
    private Class type;
    
    @Property
    private String name;
    
    @Property
    private Class owner;

    @Property
    private Class target;
    
    @Property
    private ColumnDescriptor column;
    
    @Property
    private HibernateMetadata metadata;
    
    @Property
    private Converter converter;

    @Property
    private Type propertyType;
    
    @Property
    private UserType userType;
    
    @Property
    private int index;

    @Property
    protected Field field;

    @Property
    protected PropertyDescriptor property;

    @Property
    protected EntityPropertyDescriptor.Association association;
    
    public EntityPropertyDescriptor set( String name, Object value ) {
        model().assign( name, value );
        return this;
    }

    public <T extends Annotation> T annotation( Class<T> annotationType ) {
        if( property != null && property.retains( annotationType ) ) {
            return property.annotation( annotationType );
        } else if( field != null ) {
            return field.getAnnotation( annotationType );
        } else {
            return null;
        }
    }

    public CollectionMetadata getCollection() {
        return this.metadata.collectionMetadata( this.name );
    }
    
    public EntityPropertyDescriptor.Association getAssociation(){
        return this.association;
    }
    
    public void setAssociation( EntityPropertyDescriptor.Association association ){
        this.association = association;
    }

    public Field getField(){
        return this.field;
    }
    
    public void setField( Field field ){
        this.field = field;
    }
    
    public int getIndex(){
        return this.index;
    }
    
    public void setIndex( int index ){
        this.index = index;
    }
    
    public UserType getUserType(){
        return this.userType;
    }
    
    public void setUserType( UserType userType ){
        this.userType = userType;
    }
    
    public Type getPropertyType(){
        return this.propertyType;
    }
    
    public void setPropertyType( Type propertyType ){
        this.propertyType = propertyType;
    }
    
    public Object convert( Object value ) {
        return getConverter().convert( value );
    }
    
    protected Converter getConverter( Getter<Converter> getter ) {
        if( getter.undefined() ) {
            getter.setRaw( AutoConvertProcessor.inferConverter( this.type ) );
        }
        return getter.get();
    }

    public ColumnDescriptor getColumn(){
        return this.column;
    }
    
    public void setColumn( ColumnDescriptor column ){
        this.column = column;
    }

    public Class getType(){
        return this.type;
    }
    
    public void setType( Class type ){
        this.type = type;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

    public HibernateMetadata getMetadata(){
        return this.metadata;
    }
    
    public void setMetadata( HibernateMetadata metadata ){
        this.metadata = metadata;
    }
    
    public Class getOwner(){
        return this.owner;
    }
    
    public void setOwner( Class owner ){
        this.owner = owner;
    }
    
    public Converter getConverter(){
        return this.converter;
    }
    
    public void setConverter( Converter converter ){
        this.converter = converter;
    }
    
}