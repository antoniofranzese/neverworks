package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.Query;
import it.neverworks.persistence.hibernate.entities.HibernateEntityManager;

public class NamedQueryFactory implements QueryFactory {
    
    protected HibernateEntityManager manager;
    protected String query;
    
    public NamedQueryFactory( HibernateEntityManager manager, String query ) {
        this.manager = manager;
        this.query = query;
    }
    
    public Query createQuery() {
        return this.manager.getSession().getNamedQuery( query );
    }
}