package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.FetchMode;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Order;
import org.hibernate.transform.ResultTransformer;

public interface HibernateCriteria extends CriteriaSpecification {
     HibernateCriteria add( Criterion criterion ); 
     HibernateCriteria addOrder( Order order ); 

     String getAlias();
     HibernateCriteria createAlias( String associationPath, String alias );
     HibernateCriteria createAlias( String associationPath, String alias, int joinType );

     HibernateCriteria setFetchMode( String associationPath, FetchMode mode ); 
     HibernateCriteria setProjection( Projection projection );
     HibernateCriteria setResultTransformer( ResultTransformer resultTransformer );
     
     HibernateCriteria setFirstResult( int firstResult );
     HibernateCriteria setMaxResults( int maxResults );
     HibernateCriteria setCacheable( boolean cacheable );
     HibernateCriteria setComment( String comment );
      
}