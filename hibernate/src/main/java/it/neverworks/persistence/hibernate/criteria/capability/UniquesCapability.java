package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;

import it.neverworks.persistence.hibernate.criteria.Capability;
import it.neverworks.persistence.hibernate.criteria.Phase;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.statement.AbstractStatement;

import org.hibernate.criterion.Projections;

public class UniquesCapability extends AbstractCapability {

	public UniquesCapability( String name ) {
		super( name );
	}

	@Override
	public void doWithFinder( StatementFinder finder, Arguments arguments ) {
        finder.set( "__uniques", true );
	}
	

}
