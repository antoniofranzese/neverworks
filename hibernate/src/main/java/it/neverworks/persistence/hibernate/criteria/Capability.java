package it.neverworks.persistence.hibernate.criteria;

import it.neverworks.lang.Arguments;

public interface Capability {

	String getName();
	@SuppressWarnings("rawtypes")
	void doWithFinder( StatementFinder finder, Arguments arguments );

}
