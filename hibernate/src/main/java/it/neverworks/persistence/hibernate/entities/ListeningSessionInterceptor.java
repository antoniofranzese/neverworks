package it.neverworks.persistence.hibernate.entities;

import java.util.Iterator;
import it.neverworks.lang.Threads;
import it.neverworks.log.Logger;

import org.hibernate.Transaction;
import org.hibernate.EmptyInterceptor;
import org.hibernate.event.PostLoadEvent;
import org.hibernate.engine.EntityEntry;
import org.hibernate.engine.CollectionEntry;
import org.hibernate.engine.PersistenceContext;
import org.hibernate.collection.PersistentCollection;

public class ListeningSessionInterceptor extends SessionInterceptor {

    protected static final ThreadLocal<Boolean> flushing = new ThreadLocal<Boolean> ();
    
    static {
        Threads.registerCleaner( th -> { 
            flushing.remove(); 
        });
    }

    public void preFlush( Iterator entities ) {
        logger.debug( "Flush begin" );
        flushing.set( true );
    }
    
    public void postFlush( Iterator entities ) {
        logger.debug( "Flush complete" );
        flushing.set( false );
    }
    
    public void afterTransactionBegin( Transaction tx ) {
        logger.debug( "Transaction begin" );
        // Safety set
        flushing.set( false );
    }     
    
    public void afterTransactionCompletion( Transaction tx ) {
        logger.debug( "Transaction complete" );
        // Safety set
        flushing.set( false );
    } 

    public static void notifyPostLoad( PostLoadEvent event ) {
        if( Boolean.TRUE.equals( flushing.get() ) ) {

            Object entity = event.getEntity();
            logger.debug( "PostLoad notification within flush (will be processed): {}", entity );
                
            PersistenceContext persistenceContext = event.getSession().getPersistenceContext();
            
            // Entities loaded during flush must be fixed and made read-only
            EntityEntry entry = persistenceContext.getEntry( entity );
            for( Object obj: entry.getLoadedState() ) {
                if( obj instanceof PersistentCollection ) {
                    CollectionEntry collentry = persistenceContext.getCollectionEntry( (PersistentCollection) obj );

                    logger.trace( "Collection flush fix: {}", collentry );
                    collentry.setProcessed( true );
                }
            }
            event.getSession().setReadOnly( entity, true );
            
        } else {
            if( logger.isDebugEnabled() ) {
                logger.debug( "PostLoad notification outside flush: {}", event.getEntity() );
            }
        }
    }
}