package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;

public class LessEqualStatement extends BinaryCriteriaStatement {

	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
        Object val = Value.from( value );
		return val != null ? checkNot( Restrictions.le( finder.translateProperty( property ), val ) ) : null;
	}

	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " less or equal to " + value;
	}
	

}
