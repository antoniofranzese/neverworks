package it.neverworks.persistence.hibernate.entities;

import it.neverworks.persistence.Load;
import it.neverworks.persistence.LoadEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.PostLoadEvent;
import org.hibernate.event.PostLoadEventListener;
/**
 * @author antonio
 *
 * Gestisce i listener @Load
 */
public class EntityLoadEventListener extends AbstractListener implements PostLoadEventListener {

	@Override
	public void onPostLoad( PostLoadEvent event ) {
        if( logger.isDebugEnabled() && manager.listening( event.getEntity().getClass(), Load.class ) ) {
            logger.debug( "Handling @Load for: {}", event.getEntity() );
        }
        manager.notify( event.getEntity(), Load.class, FACTORY );
        
        ListeningSessionInterceptor.notifyPostLoad( event );
	}
	
    private final static PersistenceEventFactory FACTORY = new PersistenceEventFactory() {
        public PersistenceEvent build() {
            return new LoadEvent();
        }
    };
}
