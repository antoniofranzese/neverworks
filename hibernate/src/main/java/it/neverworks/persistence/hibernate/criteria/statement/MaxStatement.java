package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class MaxStatement extends SingleValueStatement {

	@Override
	protected void doWithValue( StatementFinder finder, Object value ) {
		finder.getCriteria().setMaxResults( ((Number)value).intValue() );
	}

	@Override
	public String toString() {
		return "max " + value;
	}
	
}
