package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Order;
import org.hibernate.transform.ResultTransformer;

public class DetachedHibernateCriteria implements HibernateCriteria {
    
    private DetachedCriteria criteria;
    
    public DetachedHibernateCriteria( DetachedCriteria criteria ) {
        this.criteria = criteria;
    }
    
    public HibernateCriteria add( Criterion criterion ) {
        this.criteria.add( criterion );
        return this;
    }

    public HibernateCriteria setProjection( Projection projection ) {
        this.criteria.setProjection( projection );
        return this;
    }
    
    public HibernateCriteria setResultTransformer( ResultTransformer resultTransformer ) {
        this.criteria.setResultTransformer( resultTransformer );
        return this;
    } 

    public String getAlias() {
        return this.criteria.getAlias();
    }
    
    public HibernateCriteria createAlias( String associationPath, String alias ) {
        this.criteria.createAlias( associationPath, alias );
        return this;
    }
    
    public HibernateCriteria createAlias( String associationPath, String alias, int joinType ) {
        this.criteria.createAlias( associationPath, alias, joinType );
        return this;
    }

    public HibernateCriteria addOrder( Order order ) {
        this.criteria.addOrder( order );
        return this;
    }

    public HibernateCriteria setFetchMode( String associationPath, FetchMode mode ) {
        this.criteria.setFetchMode( associationPath, mode );
        return this;
    }

    public HibernateCriteria setFirstResult( int firstResult ) {
        // Ignore
        return this;
    }
    
    public HibernateCriteria setMaxResults( int maxResults ) {
        // Ignore
        return this;
    }
    
    public HibernateCriteria setCacheable( boolean cacheable ) {
        // Ignore
        return this;
    }
    
    public HibernateCriteria setComment( String comment ) {
        this.criteria.setComment( comment );
        return this;
    }

}