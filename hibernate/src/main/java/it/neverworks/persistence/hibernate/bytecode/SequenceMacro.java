package it.neverworks.persistence.hibernate.bytecode;

import javassist.CtClass;
import javassist.CtField;

import java.util.Map;
import java.util.HashMap;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.persistence.Sequence;
import it.neverworks.model.bytecode.ByteCodeUtils;

public class SequenceMacro implements FieldProcessor {
    
    private Map<String, Integer> sequenceCounters = new HashMap<String, Integer>();

    public void process( CtClass cls, CtField field ) throws Exception {

        String classPrefix = cls.getName().replaceAll( "\\.", "_" );
        if( ! sequenceCounters.containsKey( cls.getName() ) ) {
            sequenceCounters.put( cls.getName(), 0 );
        }

        // Espande la macro @Sequence
        if( ! ByteCodeUtils.isMarked( field, "entities.sequence" )
            && field.hasAnnotation( Sequence.class ) 
            && ! field.hasAnnotation( SequenceGenerator.class )
            && ! field.hasAnnotation( GeneratedValue.class ) ) {
            
            String generatorName = classPrefix + "_seq" + sequenceCounters.get( cls.getName() );
            sequenceCounters.put( cls.getName(), sequenceCounters.get( cls.getName() ) + 1 );

            ByteCodeUtils.createAnnotation( cls, GeneratedValue.class )
                .set( "strategy", GenerationType.class, "AUTO" )
                .set( "generator", generatorName )
                .annotate( field );

            ByteCodeUtils.createAnnotation( cls, SequenceGenerator.class )
                .set( "name", generatorName )
                .set( "sequenceName", ((Sequence) field.getAnnotation( Sequence.class )).name() )
                .set( "allocationSize", 1 )
                .annotate( field );
                
            ByteCodeUtils.mark( field, "entities.sequence" );
                
        }
        
        
    }
    
}