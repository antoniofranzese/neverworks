package it.neverworks.persistence.hibernate;

import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;

public class BlobByteArrayType extends AbstractLobType {

	@Override
	public Class returnedClass() {
		return Object.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.VARCHAR };
	}

	@Override
	protected Object nullSafeGetInternal(ResultSet rs, String[] names, Object owner, LobHandler lobHandler) throws SQLException, IOException, HibernateException {
		return process( lobHandler.getBlobAsBytes( rs, names[ 0 ] ) );
	}

	@Override
	protected void nullSafeSetInternal(PreparedStatement ps, int index,	Object value, LobCreator lobCreator) throws SQLException, IOException, HibernateException {
		lobCreator.setBlobAsBytes( ps, index, process( (byte[]) value ) );
	}


    protected byte[] process( byte[] array ) throws IOException {
        if( !this.blankable && array != null && array.length == 0 ) {
            return null;
        } else {
            return array;
        }
    }

}
