package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.statement.AbstractStatement;

public class CacheCapability extends AbstractCapability {

	public CacheCapability( String name ) {
		super( name );
	}

	@Override public void doWithFinder(StatementFinder finder, Arguments arguments ) {
		finder.getPhase( "ending" ).append( new AbstractStatement() {
			@Override public void doWithFinder(StatementFinder finder) {
				logger.debug( "Enabling cache" );
				finder.getCriteria().setCacheable( true );
			}
		});
	}

}
