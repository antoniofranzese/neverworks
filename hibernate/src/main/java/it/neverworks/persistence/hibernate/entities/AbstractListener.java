package it.neverworks.persistence.hibernate.entities;

import it.neverworks.log.Logger;

import static it.neverworks.language.*;
import it.neverworks.lang.Range;
import it.neverworks.lang.Properties;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.model.BaseModel;

import org.hibernate.event.AbstractEvent;
import org.hibernate.impl.SessionImpl;

public class AbstractListener extends BaseModel implements EntityListener {

    protected static final Logger logger = Logger.of( HibernateEntityManager.class );
    
    protected ListeningEntityManager manager;

    public AbstractListener() {
        super();
    }

    public AbstractListener( ListeningEntityManager manager ) {
        super();
        this.manager = manager;
    }

    public ListeningEntityManager getManager(){
        return this.manager;
    }

    public void setManager( ListeningEntityManager manager ){
        this.manager = manager;
    }
    
    protected void updateState( AbstractEvent event, Object entity, String[] names, Object[] values ) {
        for( int i: Range.of( names ) ) {
            try {
                Object value = Properties.get( entity, names[ i ] );
                values[ i ] = value;

            } catch( MissingPropertyException ex ) {
                //TODO: gestire i casi particolari
            }
        }
    }
}

