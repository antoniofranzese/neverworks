package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.Query;
import it.neverworks.persistence.hibernate.entities.HibernateEntityManager;

public class SQLQueryFactory implements QueryFactory {
    
    protected HibernateEntityManager manager;
    protected String query;
    
    public SQLQueryFactory( HibernateEntityManager manager, String query ) {
        this.manager = manager;
        this.query = query;
    }
    
    public Query createQuery() {
        return this.manager.getSession().createSQLQuery( query );
    }
}