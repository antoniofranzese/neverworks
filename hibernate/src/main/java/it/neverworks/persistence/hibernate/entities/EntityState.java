package it.neverworks.persistence.hibernate.entities;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Store;

public class EntityState implements Retrieve, Store {
    protected Object[] values;
    protected String[] names;
    protected Object entity;
    
    public EntityState( Object entity, String[] names, Object[] values ) {
        this.names = names;
        this.values = values;
        this.entity = entity;
    }
    
    public Object retrieveItem( Object key ) {
        return get( Strings.valueOf( key ) );
    }
    
    public void storeItem( Object key, Object value ) {
        set( Strings.valueOf( key ), value );
    }
    
    public Object get( String name ) {
        return values[ locate( name ) ];
    }
    
    public void set( String name, Object value ) {
        values[ locate( name ) ] = value;
    }
    
    protected int locate( String name ) {
        for( int i = 0; i < names.length; i++ ) {
            if( name.equals( names[ i ] ) ) {
                return i;
            }
        }
        throw new IllegalArgumentException( "Missing '" + name + "' property in " + Objects.className( entity ) + " entity values" );
    }
}
