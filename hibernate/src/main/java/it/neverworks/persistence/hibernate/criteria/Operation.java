package it.neverworks.persistence.hibernate.criteria;

public interface Operation {
	@SuppressWarnings( "rawtypes" )
	public void doWithFinder( HibernateFinder finder );
    
}
