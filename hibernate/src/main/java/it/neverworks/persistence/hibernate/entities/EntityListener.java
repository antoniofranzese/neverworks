package it.neverworks.persistence.hibernate.entities;

public interface EntityListener {
    void setManager( ListeningEntityManager manager );
}