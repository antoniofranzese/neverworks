package it.neverworks.persistence.hibernate.entities;

import java.sql.Connection;

public interface ReturningWork<T> {
    T execute( Connection connection );
}