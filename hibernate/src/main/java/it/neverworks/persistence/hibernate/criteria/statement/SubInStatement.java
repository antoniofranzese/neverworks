package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Subqueries;
import org.hibernate.criterion.DetachedCriteria;

public class SubInStatement extends BinaryCriteriaStatement {

	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
		if( this.value != null ) {
            StatementFinder subFinder = (StatementFinder) this.value;

            if( subFinder.getPhase( "projection" ).size() == 0 ) {
                subFinder.use( "keys" );
            }
            DetachedCriteria criteria = subFinder.buildDetachedCriteria();

            if( this.negation ) {
                return Subqueries.propertyNotIn( finder.translateProperty( this.property ), criteria );
            } else {
                return Subqueries.propertyIn( finder.translateProperty( this.property ), criteria );
            }
		} else {
		    return null;
		}
		
	}
    
	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " in subfinder " + this.value ;
	}

}
