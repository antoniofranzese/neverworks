package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;

import it.neverworks.model.Value;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class ExampleStatement extends SingleValueStatement {

	@Override
	protected void doWithValue( StatementFinder finder, Object value ) {
 		Example exampleCriterion =  Example.create( Value.from( value ) );
		exampleCriterion.enableLike( MatchMode.ANYWHERE );
		exampleCriterion.ignoreCase();
	    
	    finder.getCriteria().add( exampleCriterion );
	}

	@Override
	public String toString() {
		return "example: " + value;
	}
	

}
