package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;

import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.Statement;

public class PassThroughCapability extends AbstractCapability {

	public PassThroughCapability(String name) {
		super(name);
	}

	@Override
	public void doWithFinder(StatementFinder finder, Arguments arguments ) {
		finder.getPhase( "startup" ).append( new Statement() {
			@Override public void doWithFinder(StatementFinder finder) {
				logger.debug( "Disabling result transformers" );
				finder.set( "__skipTransformers", true );
			}
		});
	}

}
