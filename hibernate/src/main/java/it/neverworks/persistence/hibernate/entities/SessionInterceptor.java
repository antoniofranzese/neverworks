package it.neverworks.persistence.hibernate.entities;

import java.util.Iterator;
import it.neverworks.lang.Threads;
import it.neverworks.log.Logger;

import org.hibernate.Transaction;
import org.hibernate.EmptyInterceptor;
import org.hibernate.event.PostLoadEvent;
import org.hibernate.engine.EntityEntry;
import org.hibernate.engine.CollectionEntry;
import org.hibernate.engine.PersistenceContext;
import org.hibernate.collection.PersistentCollection;

public class SessionInterceptor extends EmptyInterceptor {

    protected static final Logger logger = Logger.of( HibernateEntityManager.class );

    @Override
    public String onPrepareStatement( String sql ) {
        return super.onPrepareStatement( sql );
    }
    
}