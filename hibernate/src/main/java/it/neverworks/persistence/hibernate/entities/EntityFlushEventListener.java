package it.neverworks.persistence.hibernate.entities;

import static it.neverworks.language.*;
import it.neverworks.persistence.Insert;
import it.neverworks.persistence.InsertEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.FlushEntityEvent;
import org.hibernate.event.FlushEntityEventListener;

/**
 * @author antonio
 *
 */
public class EntityFlushEventListener extends AbstractListener implements FlushEntityEventListener {

	@Override
	public void onFlushEntity( FlushEntityEvent event ) {
        //System.out.println( "FLUSHING: " + event.getEntity() );
	}
    
}
