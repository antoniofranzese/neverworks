package it.neverworks.persistence.hibernate.criteria;

import java.util.ArrayList;
import java.util.List;
import it.neverworks.model.expressions.ObjectQuery;

public class Phase {

    public static enum Status {
        COLLECT, PLAY
    }
    
    private List<Statement> persistentStatements;
	private List<Statement> statements;
	private String name;
	private boolean enabled;
    private Status status; 
    
	public Phase( String name ) {
		super();
		this.name = name;
		this.statements = new ArrayList<Statement>();
    	this.enabled = true;
        this.status = Status.COLLECT; 
	}

    public Phase restart() {
        this.status = Status.COLLECT;
        if( this.persistentStatements == null ) {
            this.persistentStatements = new ArrayList<Statement>( this.statements );
        } else {
            this.statements = new ArrayList<Statement>( this.persistentStatements );
        }
        return this;
    }
    
    public Phase playWith( StatementFinder finder ) {
        this.status = Status.PLAY;
    	for( Statement statement: this.statements ) {
    		statement.doWithFinder( finder );
    	}
        return this;
    }

	public String getName() {
		return name;
	}

	public Phase append( Statement statement ) {
        if( this.status != Status.PLAY ) {
    		this.statements.add( statement );
    		return this;
        } else {
            throw new IllegalStateException( "Cannot add statement to playing phase '" + this.name + "'" );
        }
	}
	
	public Phase prepend( Statement statement ) {
        if( this.status != Status.PLAY ) {
    		this.statements.add( 0, statement );
    		return this;
        } else {
            throw new IllegalStateException( "Cannot add statement to playing phase '" + this.name + "'" );
        }
	}
	
    public boolean contains( Class<? extends Statement> statementClass ) {
        for( Statement statement: statements ) {
            if( statementClass.isInstance( statement ) ) {
                return true;
            }
        }
        return false;
    }
    
	public List<Statement> getStatements() {
		return this.statements;
	}
	
    public ObjectQuery<Statement> query() {
        return new ObjectQuery( this.statements );
    }

    public <T> ObjectQuery<T> query( Class<T> cls ) {
        return (ObjectQuery<T>) new ObjectQuery( this.statements ).instanceOf( cls );
    }
    
    public Statement peek() {
        if( this.statements.size() > 0 ) {
            return this.statements.get( this.statements.size() - 1 );
        } else {
            return null;
        }
    }
    
	public Phase copy() {
		Phase copy = new Phase( this.name );
		for( Statement statement: this.statements ) {
			copy.append(statement);
		}
		return copy;
	}
    
    public String toString() {
        return "Phase(" + this.name + ")";
    }

	public boolean getEnabled(){
	    return this.enabled;
	}
	
	public void setEnabled( boolean enabled ){
	    this.enabled = enabled;
	}

    public boolean playing(){
        return ( this.status == Status.PLAY );
    }

    public boolean collecting(){
        return ( this.status == Status.COLLECT );
    }
	
    public int size() {
        return this.statements.size();
    }
}
