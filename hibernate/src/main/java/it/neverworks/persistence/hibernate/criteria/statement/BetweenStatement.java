package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.model.Value;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;

public class BetweenStatement extends UnaryCriteriaStatement {

    @Property
	protected Object lowValue;
    
    @Property
	protected Object highValue;
	
	@Override
	protected Criterion createOwnCriterion( StatementFinder finder ) {
        Object low = Value.from( lowValue );
        Object high = Value.from( highValue );
		if( low != null && high != null ) {
			return checkNot( Restrictions.between( finder.translateProperty( property ), low, high ) );

		} else if( low != null ) {
			return checkNot( Restrictions.ge( finder.translateProperty( property ), low ) );
		
		} else if( high != null ) {
			return checkNot( Restrictions.le( finder.translateProperty( property ), high ) );

		} else {
		    return null;
		}
	}

	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " between " + lowValue + " and " + highValue;
	}
	
	
	
}
