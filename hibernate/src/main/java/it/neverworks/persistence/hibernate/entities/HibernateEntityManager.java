package it.neverworks.persistence.hibernate.entities;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.Dialect;
import org.hibernate.jdbc.Work;

import it.neverworks.persistence.entities.EntityManager;
import it.neverworks.persistence.hibernate.criteria.HibernateQuery;

public interface HibernateEntityManager extends EntityManager {
    
    Session getSession();
    SessionFactory getSessionFactory();
    Dialect getDialect();
    
    Criteria criteria( Class<?> type );
    void register( Object listener );
    
    boolean hasNamedQuery( String name );
    public <T> HibernateQuery<T> getNamedQuery( String name );
    public <T> HibernateQuery<T> createQuery( String body );
    
    void doWork( Work work );
    <T> T fromWork( ReturningWork<T> work );
    
    //Hibernate naming
    default <T> T doReturningWork( ReturningWork<T> work ) {
        return fromWork( work );
    }

}