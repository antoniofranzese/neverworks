package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.lang.Strings;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.model.Value;

import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Criterion;

public class StartsStatement extends LikeStatement {

    protected String processLike( String value ) {
        return super.processLike( Strings.hasText( value ) ? ( value + "%" ) : null ); 
    }

	@Override
	public String toString() {
		return property + ( this.negation ? " not" : "" ) + " starts " + value;
	}
	
}
