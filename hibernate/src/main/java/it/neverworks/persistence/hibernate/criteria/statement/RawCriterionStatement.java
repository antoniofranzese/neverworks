package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.model.Property;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import org.hibernate.criterion.Criterion;

public class RawCriterionStatement extends AbstractStatement {
	
    @Property
    protected Criterion criterion = null;
	
	@Override
	public void doWithFinder(StatementFinder finder) {
        if( criterion != null ) {
    		finder.getCriteria().add( criterion );
        }
	}

}
