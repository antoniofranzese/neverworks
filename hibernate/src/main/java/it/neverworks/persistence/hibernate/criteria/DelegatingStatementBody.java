package it.neverworks.persistence.hibernate.criteria;

import java.lang.reflect.Method;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Arguments;
import it.neverworks.persistence.CriteriaEvent;

public class DelegatingStatementBody implements StatementBody {
    private Object owner;
    private Method method;
    private Class target;
    private boolean event = false;
    
    public DelegatingStatementBody( Object owner, Method method ) {
        this( owner, method, null, false );
    }

    public DelegatingStatementBody( Object owner, Method method, Class target ) {
        this( owner, method, target, false );
    }
    
    public DelegatingStatementBody( Object owner, Method method, Class target, boolean event ) {
        this.owner = owner;
        this.method = method;
        this.target = target;
        this.event = event;
    }

    
	public void doWithFinder( StatementFinder finder, Arguments arguments ) {
        if( this.event ) {
            Reflection.invokeMethod( this.owner, this.method, new CriteriaEvent()
                .set( "target", target )
                .set( "finder", finder )
                .set( "arguments", arguments )
            );
        } else if( this.target != null ) {
            Reflection.invokeMethod( this.owner, this.method, this.target, finder, arguments );
        } else {
            Reflection.invokeMethod( this.owner, this.method, finder, arguments );
        }
	}
}
