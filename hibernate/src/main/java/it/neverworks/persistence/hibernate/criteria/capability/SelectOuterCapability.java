package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.Statement;

public class SelectOuterCapability extends AbstractCapability {

	public SelectOuterCapability(String name) {
		super(name);
	}

	@Override
	public void doWithFinder(StatementFinder finder, Arguments arguments ) {
		finder.getPhase( "startup" ).append( new Statement() {
			@Override public void doWithFinder( StatementFinder finder ) {
				logger.debug( "Enabling outer join in projection" );
				finder.set( "__useOuterJoinInSelect", true );
			}
		});
	}

}
