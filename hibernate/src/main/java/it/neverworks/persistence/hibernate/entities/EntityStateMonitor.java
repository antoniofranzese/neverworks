package it.neverworks.persistence.hibernate.entities;

import it.neverworks.lang.Objects;
import it.neverworks.model.description.ModelInstance;

public class EntityStateMonitor {
    private Object[] values;
    private String[] names;
    private Object entity;
    private ModelInstance model; 
    private boolean watch = false;
    
    public EntityStateMonitor( Object entity, String[] names, Object[] values ) {
        this.names = names;
        this.values = values;
        this.entity = entity;
        if( ModelInstance.supports( entity ) ) {
            this.model = ModelInstance.of( entity );
            this.watch = this.model.watchable();
        }
    }
    
    public void begin() {

        if( watch ) {
            if( model.watching() ) {
                throw new IllegalStateException( "Entity already in watching state, ignore before persistence" );
            } else {
                model.watch();
            }
        }
        
    }
    
    public void end() {
        if( watch ) {
            if( model.changes().size() > 0 ) {
                EntityState state = new EntityState( entity, names, values );
                for( String name: model.changes() ) {
                    state.set( name, model.get( name ) );
                }    
            }
            model.ignore();
        }
    }
    
}
