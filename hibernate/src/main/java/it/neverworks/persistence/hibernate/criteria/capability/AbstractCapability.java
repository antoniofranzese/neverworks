package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.model.AbstractModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.description.Defended;

import it.neverworks.persistence.hibernate.criteria.Capability;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.log.Logger;

public abstract class AbstractCapability extends AbstractModel implements Capability {

    @Logger.ForMe
	protected static Logger logger;

    @Property @Required @Defended
	protected String name;
	
	public String getName() {
		return name;
	}

	public AbstractCapability() {
		super();
	}

	public AbstractCapability( String name ) {
		super();
		this.name = name;
	}
    
    public <T> T get( String name ) {
        return (T)this.model().get( name );
    }

    public <T extends AbstractCapability> T set( String name, Object value ) {
        this.model().set( name, value );
        return (T)this;
    }
    

}
