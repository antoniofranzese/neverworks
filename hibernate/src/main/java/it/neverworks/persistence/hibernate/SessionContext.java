package it.neverworks.persistence.hibernate;

import it.neverworks.log.Logger;

import org.hibernate.Session;
import org.hibernate.FlushMode;
import org.hibernate.SessionFactory;

import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import it.neverworks.lang.Errors;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.persistence.entities.EntityContext;
import it.neverworks.persistence.entities.EntityManager;
import it.neverworks.persistence.hibernate.entities.HibernateEntityManager;

public class SessionContext extends BaseModel implements EntityContext {
    
    private static final Logger logger = Logger.of( SessionContext.class );
    private static final String SESSION_ATTRIBUTE = SessionHolder.class.getName();

    @Property @Inject @Required
    private SessionFactory sessionFactory;
    
    private HibernateEntityManager manager;
    private boolean needsRelease = true;
    
    public SessionContext() {}
        
    public SessionContext( HibernateEntityManager manager ) {
        this.manager = manager;
        this.sessionFactory = manager.getSessionFactory();;
    }
    
    public SessionContext open() {
        if( TransactionSynchronizationManager.hasResource( getSessionFactory() ) ) {
            SessionHolder current = (SessionHolder) TransactionSynchronizationManager.unbindResource( getSessionFactory() );
            current.released();
        }
        
        Session session = SessionFactoryUtils.getSession( getSessionFactory(), true );
		session.setFlushMode( FlushMode.COMMIT );
        
		SessionHolder created = new AutoCloseSessionHolder( session );
        created.requested();
        
		TransactionSynchronizationManager.bindResource( getSessionFactory(), created );
        
        return this;
    }

    public void close() {
        if( TransactionSynchronizationManager.hasResource( getSessionFactory() ) ) {
            SessionHolder current = (SessionHolder) TransactionSynchronizationManager.unbindResource( getSessionFactory() );
            current.released();
        }
    }
    
    public void shutdown() {
        try {
            this.close();
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public EntityManager manager() {
        return this.manager;
    }
    
    public Session getSession(){
        SessionHolder current = (SessionHolder) TransactionSynchronizationManager.getResource( getSessionFactory() );
        if( current != null ) {
            return current.getSession();
        } else {
            return null;
        }
    }
    
    public SessionFactory getSessionFactory(){
        return this.sessionFactory;
    }
    
    public void setSessionFactory( SessionFactory sessionFactory ){
        this.sessionFactory = sessionFactory;
    }
}