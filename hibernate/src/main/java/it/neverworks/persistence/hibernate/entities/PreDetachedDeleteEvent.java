package it.neverworks.persistence.hibernate.entities;

import java.io.Serializable;
import org.hibernate.event.AbstractEvent;
import org.hibernate.event.EventSource;

public class PreDetachedDeleteEvent extends AbstractEvent {
    
    protected Serializable id;
    protected Class<?> type;
    
    public PreDetachedDeleteEvent( Class<?> type, Serializable id, EventSource session ) {
        super( session );
        this.type = type;
        this.id = id;
    }
    
    public Class<?> getType(){
        return this.type;
    }
    
    public Serializable getId(){
        return this.id;
    }
    

}