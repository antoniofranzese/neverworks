package it.neverworks.persistence.hibernate.criteria.statement;

import org.hibernate.FetchMode;

import it.neverworks.model.Property;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class CollectStatement extends AbstractStatement {

    @Property
    protected String property;
	
	@Override
	public void doWithFinder( StatementFinder finder ) {
		finder.getCriteria().setFetchMode( property, FetchMode.JOIN );
	}

	@Override
	public String toString() {
		return "collect " + property;
	}
	

}
