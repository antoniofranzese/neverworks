package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.criterion.ProjectionList;

public interface ProjectionItem {
    ProjectionList populate( ProjectionList list );
}