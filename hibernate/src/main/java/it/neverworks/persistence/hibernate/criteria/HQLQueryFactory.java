package it.neverworks.persistence.hibernate.criteria;

import java.util.Iterator;
import org.hibernate.Query;
import it.neverworks.persistence.hibernate.entities.HibernateEntityManager;

public class HQLQueryFactory implements QueryFactory {
    
    protected HibernateEntityManager manager;
    protected String query;
    
    public HQLQueryFactory( HibernateEntityManager manager, String query ) {
        this.manager = manager;
        this.query = query;
    }
    
    public Query createQuery() {
        return this.manager.getSession().createQuery( query );
    }
    
    public Iterator iterate( Query query ) {
        return query.iterate();
    }
    
}