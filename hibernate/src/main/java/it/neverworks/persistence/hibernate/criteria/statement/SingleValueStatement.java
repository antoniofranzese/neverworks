package it.neverworks.persistence.hibernate.criteria.statement;

import it.neverworks.model.Property;

import it.neverworks.persistence.hibernate.criteria.Statement;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public abstract class SingleValueStatement extends AbstractStatement {
    
    @Property
	protected Object value = null;
	
	@Override
	public void doWithFinder(StatementFinder finder) {
		if( value != null ) {
			doWithValue( finder, value );
		}
	}
	
	protected abstract void doWithValue( StatementFinder finder, Object value );

}
