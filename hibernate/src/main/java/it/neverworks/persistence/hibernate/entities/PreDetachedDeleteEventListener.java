package it.neverworks.persistence.hibernate.entities;

public interface PreDetachedDeleteEventListener {
    boolean onPreDetachedDelete( PreDetachedDeleteEvent event );
}