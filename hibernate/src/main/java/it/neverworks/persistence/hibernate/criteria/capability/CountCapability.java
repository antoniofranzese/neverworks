package it.neverworks.persistence.hibernate.criteria.capability;

import it.neverworks.lang.Arguments;

import it.neverworks.persistence.hibernate.criteria.Capability;
import it.neverworks.persistence.hibernate.criteria.Phase;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.statement.AbstractStatement;

import org.hibernate.criterion.Projections;

public class CountCapability extends AbstractCapability {

	public CountCapability( String name ) {
		super( name );
	}

	@Override
	public void doWithFinder( StatementFinder finder, Arguments arguments ) {
		finder.addPhase( new Phase( "count" ) ).inPlaceOf( "projection" );

		finder.getPhase( "count" ).append( new AbstractStatement() {
			
			@Override public void doWithFinder(StatementFinder finder) {
				logger.debug( "Disabling projection for count" );
				finder.set( "__useProjection", false );

                if( finder.attribute( "__uniques" ).isTrue() ) {
                    finder.getCriteria().setProjection( Projections.countDistinct( finder.getIdProperty() ) );
                } else {
                    finder.getCriteria().setProjection( Projections.rowCount() );
                }
                
                if( finder.hasPhase( "order" ) ) {
                    finder.getPhase( "order" ).setEnabled( false );
                }
			}

			@Override public String toString() {
				return "count";
			}
		
		});
	}
	

}
