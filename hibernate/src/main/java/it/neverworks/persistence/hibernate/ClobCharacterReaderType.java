package it.neverworks.persistence.hibernate;

import java.io.IOException;
import java.io.Reader;
import java.io.PushbackReader;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import it.neverworks.io.Streams;

public class ClobCharacterReaderType extends AbstractLobType {

	@Override
	public Class returnedClass() {
		return Reader.class;
	}

	@Override
	public int[] sqlTypes() {
		return new int[]{ Types.CLOB };
	}

	@Override
	protected Object nullSafeGetInternal( ResultSet rs, String[] names, Object owner, LobHandler lobHandler ) throws SQLException, IOException, HibernateException {
		return lobHandler.getClobAsCharacterStream( rs, names[ 0 ] );
	}

	@Override
	protected void nullSafeSetInternal( PreparedStatement ps, int index, Object value, LobCreator lobCreator ) throws SQLException, IOException, HibernateException {
		lobCreator.setClobAsCharacterStream( ps, index, (Reader) value, 0 );
	}

    protected Reader process( Reader reader ) throws IOException {
        if( !this.blankable && reader != null ) {
            return Streams.plane( reader );
        } else {
            return reader;
        }
    }

}
