package it.neverworks.persistence.hibernate.bytecode;

import javassist.CtClass;
import javassist.CtField;

public interface FieldProcessor {
    void process( CtClass cls, CtField field ) throws Exception;
}