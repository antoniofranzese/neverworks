package it.neverworks.persistence.hibernate.criteria;

import java.util.Iterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.neverworks.log.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.ScrollMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Criterion;
import org.hibernate.transform.ResultTransformer;

import it.neverworks.language;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Functional;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.Value;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.Modifiers;

import it.neverworks.persistence.criteria.CriteriaBuilder;
import it.neverworks.persistence.criteria.Filter;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.criteria.Scroller;
import it.neverworks.persistence.criteria.ScrollableFinder;
import it.neverworks.persistence.criteria.Transformer;
import it.neverworks.persistence.hibernate.entities.HibernateEntityManager;
import it.neverworks.persistence.hibernate.criteria.TransformerAdapter;
import it.neverworks.persistence.hibernate.criteria.capability.KeysCapability;
import it.neverworks.persistence.hibernate.criteria.capability.CacheCapability;
import it.neverworks.persistence.hibernate.criteria.capability.CountCapability;
import it.neverworks.persistence.hibernate.criteria.capability.UniquesCapability;
import it.neverworks.persistence.hibernate.criteria.capability.GlobalOuterCapability;
import it.neverworks.persistence.hibernate.criteria.capability.SelectOuterCapability;
import it.neverworks.persistence.hibernate.criteria.capability.PassThroughCapability;
import it.neverworks.persistence.hibernate.criteria.statement.EmptyStatement;
import it.neverworks.persistence.hibernate.criteria.statement.CollectStatement;
import it.neverworks.persistence.hibernate.criteria.statement.ExampleStatement;
import it.neverworks.persistence.hibernate.criteria.statement.ExpressionStatement;
import it.neverworks.persistence.hibernate.criteria.statement.FirstStatement;
import it.neverworks.persistence.hibernate.criteria.statement.MaxStatement;
import it.neverworks.persistence.hibernate.criteria.statement.OrderStatement;
import it.neverworks.persistence.hibernate.criteria.statement.DisjunctiveStatement;
import it.neverworks.persistence.hibernate.criteria.statement.RawCriterionStatement;
import it.neverworks.persistence.hibernate.criteria.statement.ProjectionResetStatement;
import it.neverworks.persistence.hibernate.criteria.statement.ProjectionEnableStatement;
import it.neverworks.persistence.hibernate.criteria.statement.ProjectionPropertyStatement;

@SuppressWarnings({ "serial", "unchecked", "rawtypes" })
public class StatementFinder<T> implements HibernateFinder<T>, ScrollableFinder, ResultTransformer, Functional<T> {
	
	private final static Logger logger = Logger.of( StatementFinder.class );

    public final static Modifiers WITH_NULLS = new Modifiers().withNulls();

	private final static List<Capability> standardCapabilities = language.list(
		new CacheCapability( "cache" )
		,new GlobalOuterCapability( "outer" )
		,new SelectOuterCapability( "select-outer" )
		,new PassThroughCapability( "raw" )
		,new CountCapability( "count" )
		,new KeysCapability( "keys" )
		,new UniquesCapability( "uniques" )
		,new UniquesCapability( "distinct" )
	);
	
    private HibernateEntityManager manager;
	private HibernateCriteria criteria;
	private AliasNode rootNode;

	private Class type;

	private Set<ProjectionItem>projectionItems = new HashSet<ProjectionItem>();
	
	private Transformer transformer;
    private String comment;
    private boolean building = false;
	
	public class Initializer {
		private List<Phase> initPhases;
		private Collection<Capability> initCapabilities = new ArrayList<Capability>(standardCapabilities);
		
		public Initializer andCapabilities( Collection<Capability> capabilities ) {
			this.initCapabilities = new ArrayList<Capability>( capabilities );
			return this;
		}
		
		public Initializer andCapabilities( Capability... capabilities ) {
			this.initCapabilities = Arrays.asList( capabilities );
			return this;
		}
		
		public Initializer andAdditionalCapabilities( Capability... capabilities ) {
			this.initCapabilities.addAll( Arrays.asList( capabilities ) );
			return this;
		}
		
		public Initializer andPhases( Phase... phases ) {
			this.initPhases = Arrays.asList( phases );
			return this;
		}
		
		public Initializer andPhases( List<Phase> phases ) {
			this.initPhases = new ArrayList<Phase>(phases);
			return this;
		}
		
		public StatementFinder<T> andType( Class entityType ) {
            if( entityType != null ) {
    			type = entityType;
			
    			if( initPhases == null ) {
    				initPhases = new ArrayList<Phase>();
    			}
    			if( initPhases.size() == 0 ) {
    				initPhases.add(	new Phase( "startup" ) );
    				initPhases.add(	new Phase( "criteria" ) );
    				initPhases.add(	new Phase( "additional-criteria" ) );
    				initPhases.add(	new Phase( "order" ) );
    				initPhases.add(	new Phase( "projection" ) );
    				initPhases.add(	new Phase( "ending" ) );
    			}
    			initPhases( initPhases );
    			initCapabilities( initCapabilities );
			
    			return StatementFinder.this;
            } else {
                throw new IllegalArgumentException( "Null type initializing Finder" );
            }
		}
		
 	}
        
    //TODO: manager constructor
    public Initializer initWithManager( HibernateEntityManager manager ) {
        this.manager = manager;
        return new Initializer();
    }

    //TODO: copy constructor
    public StatementFinder<T> initWithFinder( StatementFinder<T> other ) {
        if( other.manager != null ) {

            List<Phase> phaseCopies = new ArrayList<Phase>();

            for( Phase phase: other.phases ) {
                phaseCopies.add( phase.copy() );
            }

            this
                .initWithManager( other.manager )
                .andPhases( phaseCopies )
                .andCapabilities( other.capabilities.values() )
                .andType( other.type )
                    .use( other.features );

                return this;

        } else {
            throw new RuntimeException( "Cannot copy StatementFinder: no manager" );
        }
    }
	
	
	@Override
	public Finder<T> copy() {
		return new StatementFinder<T>().initWithFinder( this );
	}
	
	private List<Phase> phases;

	private Map<String,Phase> phaseMap = new HashMap<String, Phase>();;
	
	public StatementFinder<T> initPhases( List<Phase> phases ) {
		this.phases = phases;
		this.phaseMap.clear();
		for( Phase phase: phases ) {
			this.phaseMap.put( phase.getName(), phase );
		}
		return this;
	}

	public Phase getPhase( String name ) {
		if( this.phaseMap.containsKey( name ) ) {
			return this.phaseMap.get( name );
		} else {
			if( attribute( "__strict" ).isFalse() ) {
				Phase phase = new Phase( name );
                addPhase( phase ).asLast();
                return phase;
			} else {
				throw new RuntimeException( "Unknown phase: " + name );
			}
		}
	}

    public boolean hasPhase( String name ) {
        return this.phaseMap.containsKey( name );
    }
    
	private Map<String, Capability> capabilities = new HashMap<String, Capability>();

	public StatementFinder<T> initCapabilities( Collection<Capability> capabilities ) {
		this.capabilities.clear();
		addCapabilities( capabilities.toArray( new Capability[ capabilities.size() ] ) );
		return this;
	}

	public StatementFinder<T> addCapabilities( Capability... capabilities ) {
		for( Capability capability: capabilities ) {
			this.capabilities.put( capability.getName().toLowerCase(), capability );
		}
		return this;
	}

	public Capability getCapability( String name ) {
		if( Strings.hasText( name ) ) {
			String key = name.toLowerCase();
			if( capabilities.containsKey( key ) ) {
				return capabilities.get( key );
			} else {
				throw new RuntimeException( "Unknown capability: " + name );
			}
		} else {
			throw new RuntimeException( "Null or empty capability" );
		}
	}
	
	private ExpandoModel attributes = new ExpandoModel()
        .set( "__strict", true )
        .set( "__uniques", false )
        .set( "__useProjection", false )
        .set( "__useOuterJoinAsDefault", false )
        .set( "__useOuterJoinInSelect", false )
        .set( "__skipTransformers", false );

    public Value attribute( String name ) {
        return Value.of( attributes.containsKey( name ) ? attributes.get( name ) : null );
    }
    
    public Map<String,Object> getAttributes() {
        return this.attributes;
    }

    public StatementFinder<T> set( String name, Object value ) {
        attributes.set( name, value );
        return this;
    }
    
    public HibernateFinder<T> with( String name ) {
        return set( name, true );
    }

    public HibernateFinder<T> with( String name, Object value ) {
        return set( name, value );
    }
    
    public HibernateFinder<T> without( String name ) {
        return set( name, false );
    }
    
    public boolean has( String name ) {
        return attribute( name ).isTrue();
    }
    
    private Set<String> features = new HashSet<String>();

	public HibernateFinder<T> use( Iterable<String> features ) {
        for( String feature: features ) {
            use( feature );
        }
        return this;
    }

	@Override
	public HibernateFinder<T> use(String... features) {
		if( features != null ) {
			for( String feature: features ) {
				if( feature != null && ! this.features.contains( feature ) ) {
					Capability capability = capabilities.get( feature.toLowerCase() );
					if( capability != null ) {
						capability.doWithFinder( this, new Arguments() );
					}
                    this.features.add( feature );
				}
			}
		}
		return this;
	}

    private String disjunctionDenial = null;
	
	@Override
	public CriteriaBuilder<T> and( String property ) {
        disjunctionDenial = null;
        if( Strings.hasText( property ) ) {
    		return (CriteriaBuilder<T>) new StatementCriteriaBuilder<T>().set( "finder", this ).set( "property", property );
        } else {
            throw new RuntimeException( "Null or empty criteria property" );
        }
	}
	
	@Override
    public CriteriaBuilder<T> or( String property ) {
        if( disjunctionDenial == null ) {
            Statement statement = currentCriteriaPhase().peek();
            if( statement != null ) {
                if( statement instanceof DisjunctiveStatement ) {
                    return new DisjunctiveCriteriaBuilder( this, property, (DisjunctiveStatement) statement );
                } else {
                    throw new RuntimeException( statement.getClass().getSimpleName() + " is not disjunctive" );
                }
            } else {
                throw new RuntimeException( "No previous disjunctive statement" );
            }
        } else {
            throw new RuntimeException( Strings.message( "Disjunction is not available after \"{0}\"", disjunctionDenial ) );
        }
    }

    @Override
    public Finder<T> begin() {
        disjunctionDenial = null;
        this.currentCriteriaPhase().append( new EmptyStatement() );
        return this;
    }

	public String translateProperty( String property ) {
        PropertyInfo info = parseProperty( property );
		String propertyName = info.name;
		String alias = cleanProperty( property );
		if( attribute( "__useProjection" ).isTrue() ) {
			addProjection( new PropertyProjectionItem( propertyName, alias ) );
		}
		return propertyName;
	}

    public HibernateFinder<T> addCriterion( Criterion criterion ) {
		this.currentCriteriaPhase().append( (Statement) new RawCriterionStatement()
            .set( "criterion", criterion )
        );
        return this;
    }
    
    public StatementFinder<T> addProjection( ProjectionItem item ) {
		this.projectionItems.add( item );
        return this;
    }

    public StatementFinder<T> clearProjection() {
		this.projectionItems.clear();
        return this;
    }

    public void addCriteriaStatement( Statement statement ) {
        this.currentCriteriaPhase().append( statement );
    }
    
    public Phase currentCriteriaPhase() {
        return getPhase( this.building ? "additional-criteria" : "criteria" );
    }
    
	@Override
	public Finder<T> include( String... properties ) {
        disjunctionDenial = "include";

        if( ! this.getPhase( "startup" ).contains( ProjectionEnableStatement.class ) ) {
    		this.getPhase( "startup" ).append( new ProjectionEnableStatement() );
        }

		for( final String property: properties ) {
			this.getPhase( "projection" ).append( new ProjectionPropertyStatement().set( "property", property ) );
		}
		
		return this;
	}

	@Override
	public Finder<T> select( String... properties ) {
        disjunctionDenial = "select";
        if( ! this.getPhase( "projection" ).contains( ProjectionResetStatement.class ) ) {
    		this.getPhase( "projection" ).prepend( new ProjectionResetStatement() );
        }
		return include( properties );
	}
	
	@Override
	public Finder<T> pack() {
        disjunctionDenial = "pack";
		set( "__useProjection", true );
		return this;
	}

	@Override
	public Finder<T> expr( String expression, Object... params ) {
        disjunctionDenial = "expr";
		this.currentCriteriaPhase().append( (Statement) new ExpressionStatement()
             .set( "expression", expression )
             .set( "parameters", params )
        ); //.init( expression, params ) );
		return this;
	}

	@Override
	public Finder<T> order( String... properties ) {
        disjunctionDenial = "order";
        if( hasPhase( "order") ) {
    		this.getPhase( "order" ).append( (Statement) new OrderStatement()
                 .set( "properties", (Object[]) properties )
            ); //.init( (Object[]) properties ) );
        }
		return this;
	}

	@Override
	public Finder<T> order( String property ) {
		return this.order( new String[]{ property });
	}

	@Override
	public Finder<T> order( Iterable<String> properties ) {
		return this.order( Collections.toArray( String.class, properties ) );
	}

	@Override
	public Finder<T> collect( String property ) {
        disjunctionDenial = "collect";
		this.currentCriteriaPhase().append( (Statement) new CollectStatement()
            .set( "property", property )
        ); 
		return this;
	}

	@Override
	public Finder<T> max( Integer elements ) {
        disjunctionDenial = "max";
		this.currentCriteriaPhase().append( (Statement) new MaxStatement()
            .set( "value", elements )
        ); 
		return this;
	}

	@Override
	public Finder<T> first( final Integer index ) {
        disjunctionDenial = "first";
		this.currentCriteriaPhase().append( (Statement) new FirstStatement()
            .set( "value", index )
        );
		return this;
	}

	
	@Override
	public Finder<T> example( final T example ) {
        disjunctionDenial = "example";
		this.currentCriteriaPhase().append( (Statement) new ExampleStatement()
            .set( "example", example )
        ); 
	    return this;
	}

    public Finder<T> comment( String comment ) {
        if( Strings.hasText( comment ) ) {
            this.comment = this.comment != null ? ( this.comment + " " + comment ) : comment;
        }
        return this;
    }
    
	@Override
	public T result() {
		return (T)buildCriteria().uniqueResult();
	}
    
    public Value value() {
        return Value.of( result() );
    }

    public Value value( String expression ) {
        return Value.of( get( expression ) );
    }
    
    public <V> V get( String expression ) {
        Object result = result();
        return result != null ? (V) ExpressionEvaluator.evaluate( result, expression ) : null;
    }

    public <V> V get( String expression, V defaultValue ) {
        Object value = get( expression );
        return value != null ? (V) value : defaultValue;
    }

	@Override
	public FluentList<T> list() {
        return new FluentArrayList<T>( results() );
	}
    
    protected List<T> results() {
		return buildCriteria().list();
    }

    public Iterator<T> iterator() {
        return results().iterator();
    }

    public Scroller scroll() {
        return new HibernateScroller( buildCriteria().scroll( ScrollMode.FORWARD_ONLY ) );
    }
    
    protected Criteria buildCriteria() {
		if( this.manager != null ) {
            if( type != null ) {
    			Criteria connectedCriteria = this.manager.getSession().createCriteria( this.type );
                this.criteria = new ConnectedHibernateCriteria( connectedCriteria );
                playFinder();
                return connectedCriteria;
            } else {
                throw new RuntimeException( "Missing StatementFinder type" );
            }
            
		} else {
		    throw new IllegalStateException( "Cannot create connected Criteria, null manager" );
		}
    }

    public DetachedCriteria buildDetachedCriteria() {
        if( type != null ) {
			DetachedCriteria detachedCriteria = DetachedCriteria.forClass( this.type );
            this.criteria = new DetachedHibernateCriteria( detachedCriteria );
            playFinder();
			return detachedCriteria;
        } else {
            throw new RuntimeException( "Missing StatementFinder type" );
        }
    }
    
	private void playFinder() {
        this.building = true;
		
        for( Phase phase: this.phases ) {
            phase.restart();
        }

		for( Phase phase: this.phases ) {
            if( phase.getEnabled() ) {
    			logger.debug( "Playing {name}", phase );
                phase.playWith( this );
            } else {
    			logger.debug( "Skipping {name}", phase );
            }
		}
		
		if( attribute( "__useProjection" ).equals( true ) ) {
			ProjectionList projectionList = Projections.projectionList();
			for( ProjectionItem item: projectionItems ) {
                projectionList = item.populate( projectionList );
			}
            
			criteria.setProjection( attribute( "__uniques" ).isTrue() ? Projections.distinct( projectionList ) : projectionList );
			setResultTransformer( this );

		} else {
			setResultTransformer( Criteria.DISTINCT_ROOT_ENTITY );
		}
        
        if( Strings.hasText( this.comment ) ) {
            criteria.setComment( this.comment );
        }
        
        this.building = false;
		
	}
	
	private void setResultTransformer( ResultTransformer resultTransformer ) {
        if( attribute( "__skipTransformers" ).isFalse() ) {
    		if( this.transformer != null ) {
    			getCriteria().setResultTransformer( new TransformerAdapter( this.transformer ) );
    		} else {
    			getCriteria().setResultTransformer( resultTransformer );
    		}
        }
	}
	
	@Override
	public List transformList(List list) {
		return list;
	}

    @Override
    public Finder<T> transform(Transformer transformer) {
        this.transformer = transformer;
        return this;
    }

	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		if( logger.trace() && tuple != null ) {
			Class[] classes = new Class[ tuple.length ];
			for( int i = 0; i < tuple.length; i++ ){
				if( tuple[i] != null ) {
					classes[i] = tuple[i].getClass();
				}	
			}
			logger.trace( "Transform Tuple {} = {}, {}", new Object[]{ Arrays.toString( aliases ), Arrays.toString( tuple ), Arrays.toString( classes ) });
		}
        
        Object item = Reflection.newInstance( this.type );
		for( int i = 0; i < tuple.length; i++ ) {
			ExpressionEvaluator.valorize( item, aliases[i], tuple[i], WITH_NULLS );
		}
        return item;
        
	}
	
	public String cleanProperty( String property ) {
		return property.replaceAll( "!", "" ).replaceAll( "\\+", "" );
	}
	
    public static class PropertyInfo {
        public String name;
    }
    
	public PropertyInfo parseProperty( String property ) {
		return parseProperty( property, attribute( "__useOuterJoinAsDefault" ).toBoolean() );
	}

	private int aliasCount = 0;

	public PropertyInfo parseProperty( String property, boolean outerJoinDefault ) {
		logger.trace( "Checking property {}", property );

		if( property.indexOf( "." ) < 0 ) {
            PropertyInfo info = new PropertyInfo();
            info.name = criteria.getAlias() + "." + property;
			return info;
	
		} else {
			String[] steps = property.split( "\\." );
			
            if( this.rootNode == null ) {
                this.rootNode = new AliasNode( criteria.getAlias() );
            }
            
            boolean useOuterJoin = outerJoinDefault;
			AliasNode currentNode = rootNode;
			for( int i = 0; i < steps.length - 1; i++ ) {
				String rawStep = steps[i];
				logger.trace( "Verifying {}", rawStep );
				
                String cleanStep = cleanProperty( rawStep );

				if( !currentNode.hasAlias( cleanStep ) ) {
					logger.trace( "Creating node alias for {0} in {1.name}", cleanStep, currentNode );
					
					// TODO Rilevare automaticamente se la proprieta' e' un embedded
					if( rawStep.startsWith( "!" ) || rawStep.startsWith( "$" ) ) {
						String name = rawStep.replaceFirst( "\\" + rawStep.substring( 0, 1 ), criteria.getAlias() + "." );
						AliasNode node = new AliasNode( name );
						currentNode.addAlias( cleanStep, node );
						logger.trace( "Replacing {0.name}.{1} with {2.name}", currentNode, cleanStep, node );
					} else {

						//TODO: verificare una tecnica per determinare automaticamente il tipo di join
						if( rawStep.startsWith( "+" ) ) {
							useOuterJoin = true;
						} else if( rawStep.startsWith( "-" ) ) {
							useOuterJoin = false;
						}

						AliasNode node = new AliasNode( "_" + cleanStep + aliasCount++ );
						currentNode.addAlias( cleanStep, node );
					    logger.trace( "Aliasing {0.name}.{1} with {2.name}, using {3,bool,true=outer|false=inner} join", currentNode, cleanStep, node, useOuterJoin );
						
						criteria.createAlias( currentNode.getName() + "." + cleanStep, node.getName(), useOuterJoin ? CriteriaSpecification.LEFT_JOIN : CriteriaSpecification.INNER_JOIN );
						
					}

					
				} else {
					logger.trace( "Found node alias for {}", cleanStep );
				}

				currentNode = currentNode.getAlias( cleanStep );
			}
			
            PropertyInfo info = new PropertyInfo();
            info.name = currentNode.getName() + "." + steps[ steps.length - 1 ];
			return info;
		}
	}

    public String getIdProperty() {
        if( this.manager != null ) {
            if( this.type != null ) {
                return manager.metadata( this.type ).identifier().getName();
            } else {
                throw new RuntimeException( "Missing StatementFinder type" );
            }
        } else {
            throw new RuntimeException( "Missing StatementFinder manager" );
        }
    }
	
	public HibernateCriteria getCriteria() {
		if( this.criteria != null ) {
            return this.criteria;
		} else {
		    throw new IllegalStateException( "No current criteria" );
		}
	}
	
	@Override
	public int count() {
		return (Integer)use( "count" ).result();
	}

    public ObjectQuery<T> query() {
        return new ObjectQuery<T>( results() );
    }

    public HibernateFinder<T> on( String name, Operation operation ) {
        getPhase( name ).append( new OperationStatement( operation ) );
        return this;
    }
    
	public void explain() {
		if( logger.isDebugEnabled() ) {
			for( Phase phase: this.phases ) {
				logger.debug( "Explaining {name}:", phase );
				for( Statement statement: phase.getStatements() ) {
					logger.debug( "> {}", statement );
				}
			}
		}
	}
	
	private class AliasNode {
		private String name;
		private Map<String, AliasNode> aliases;
		@SuppressWarnings("unused")
		private AliasNode parent;

		public AliasNode(String name) {
			logger.trace( "Creating Alias Node {}", name );
			this.name = name;
			this.aliases = new HashMap<String, AliasNode>();
		}

		public String getName() {
			return name;
		}
		
		public void addAlias( String alias, AliasNode node ) {
			node.parent = this;
			aliases.put( alias , node );
		}
		
		public AliasNode getAlias( String alias ) {
			return aliases.get( alias );
		}
		
		public boolean hasAlias( String alias ) {
			return aliases.containsKey( alias );
		}
        
        public String toString() {
            return new ToStringBuilder( this )
                .add( "name" )
                .add( "!parent" )
                .toString();
        }
	}

	public PhaseAdder addPhase( Phase phase ) {
		return new PhaseAdder( phase );
	}

	public class PhaseAdder {
		private Phase phase;

		public PhaseAdder(Phase phase) {
			super();
			this.phase = phase;
		}
		
		public StatementFinder<T> asFirst() {
			return add( phase, 0);
		}

		public StatementFinder<T> asLast() {
			return add( phase, phases.size() );
		}

		public StatementFinder<T> before( String name ) {
			return add( phase, phases.indexOf( getPhase( name ) ) );
		}
		
		public StatementFinder<T> after( String name ) {
			return add( phase, phases.indexOf( getPhase( name ) ) + 1 );
		}
		
		public StatementFinder<T> inPlaceOf( String name ) {
			int idx = phases.indexOf( getPhase( name ) );
			phases.remove( idx );
			return add( phase, idx );
		}
		
		private StatementFinder<T> add( Phase phase, int index ){
			phases.add( index, phase );
			initPhases( phases );
			return StatementFinder.this;
		}
	}
	
	public Class getEntityType() {
		return type;
	}
	
    public String toString() {
        return this.getClass().getSimpleName() + "(entity=" + ( type != null ? type.getName() : "null" ) + ")";
    }
    
}
