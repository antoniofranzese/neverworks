package it.neverworks.persistence.hibernate.entities;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.lang.reflect.Field;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.OneToMany;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;

import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.usertype.UserType;
import org.hibernate.type.CustomType;
import org.hibernate.type.Type;

import it.neverworks.lang.Reflection;
import it.neverworks.model.Property;
import it.neverworks.model.utils.CoreExpandoModel;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.utils.Identification;
import it.neverworks.persistence.entities.EntityManager;
import it.neverworks.persistence.entities.EntityMetadata;
import it.neverworks.persistence.entities.EntityPersister;
import it.neverworks.persistence.entities.EntityPropertyDescriptor;
import it.neverworks.persistence.PersistenceException;

@Identification( "type" )
public class HibernateMetadata<E> extends CoreExpandoModel implements EntityMetadata<E> {
    
    private SessionFactory sessionFactory;

    private ClassMetadata metadata;
    private Map<String, EntityPropertyDescriptor> properties;
    private List<String> identifierNames;
    
    @Property
    private HibernateEntityManager manager;
    
    @Property
    private HibernateEntityPersister<E> persister;
    
    @Property
    private TableDescriptor table;
    
    @Property
    private Class<E> type;

    @Property
    private ModelDescriptor model;
    
    public HibernateMetadata( HibernateEntityManager manager, Class<E> type ) {
        super();
        this.manager = manager;
        this.type = type;
        this.persister = new HibernateEntityPersister<E>( this.manager, this.type );
        this.sessionFactory = this.manager.getSessionFactory();
        this.model = ModelDescriptor.of( type );
        this.parse();
    }

    public EntityMetadata<E> set( String name, Object value ) {
        model().assign( name, value );
        return this;
    }

    public EntityManager manager() {
        return this.manager;
    }
    
    public EntityPersister<E> persister() {
        return this.persister;
    }

    public EntityPropertyDescriptor property( String name ) {
        if( this.properties.containsKey( name ) ) {
            return this.properties.get( name );
        } else {
            throw new IllegalArgumentException( "Missing persistent property '" + name + "' in " + type.getName() );
        }
    }
    
    public ObjectQuery<EntityPropertyDescriptor> query() {
        return new ObjectQuery<EntityPropertyDescriptor>( this.properties.values() );
    }
    
    public EntityPropertyDescriptor identifier() {
        if( this.identifierNames.size() == 1 ) {
            return property( this.identifierNames.get( 0 ) );
        } else {
            throw new RuntimeException( this.type.getName() + " has composite identifier" );
        }
    }
    
    public boolean hasCompositeIdentifier() {
        return this.identifierNames.size() > 1;
    }
    
    public Map<String,EntityPropertyDescriptor> getCompositeIdentifier() {
        Map result = new HashMap<String,EntityPropertyDescriptor>();
        for( String identifierName: this.identifierNames ) {
            result.put( identifierName, property( identifierName ) );
        }
        
        return result;
    }
    
    public Class type(){
        return this.type;
    }
    
    protected void parse() {
        if( this.metadata == null ) {
            this.metadata = this.sessionFactory.getClassMetadata( this.type );
            if( this.metadata == null ) {
                throw new PersistenceException( type.getName() + " is not a persistent class" );
            }
        }
        
        if( this.table == null ) {
            Table tableAnnotation = (Table) this.type.getAnnotation( Table.class );
            this.table = new TableDescriptor();
            if( tableAnnotation != null ) {
                this.table.set( "name", tableAnnotation.name() );
            } else {
                this.table.set( "name", this.type.getSimpleName() );
            }
        }
        
        if( this.identifierNames == null ) {
            this.identifierNames = new ArrayList<String>();
            
            if( this.metadata.getIdentifierPropertyName() != null ) {
                this.identifierNames.add( this.metadata.getIdentifierPropertyName() );
            } else {
                for( Field field: this.type.getDeclaredFields() ) {
                    if( field.getAnnotation( Id.class ) != null ) {
                        this.identifierNames.add( field.getName() );
                    }
                }
            }
        }

        if( this.properties == null ) {
            this.properties = new HashMap<String, EntityPropertyDescriptor>();

            String[] propertyNames = this.metadata.getPropertyNames();
            for( String identifierName: this.identifierNames ) {
                this.properties.put( identifierName, parseProperty( identifierName, -1 ) );
            }

            for( int index = 0; index < propertyNames.length; index++  ) {
                String propertyName = propertyNames[ index ];
                if( ! "_identifierMapper".equals( propertyName ) ) {
                    this.properties.put( propertyName, parseProperty( propertyName, index ) );
                }
            }
        }
        
    }
    
    protected HibernatePropertyDescriptor parseProperty( String propertyName, int index ) {
        HibernatePropertyDescriptor descriptor = new HibernatePropertyDescriptor();

        Type propertyType = this.metadata.getPropertyType( propertyName );

        descriptor
            .set( "name", propertyName )
            .set( "index", index )
            .set( "type", propertyType.getReturnedClass() )
            .set( "property", this.model.has( propertyName ) ? this.model.property( propertyName ) : null )
            .set( "owner", type )
            .set( "propertyType", propertyType )
            .set( "metadata", this );
            

        if( propertyType instanceof CustomType ) {
            UserType userType = (UserType) Reflection.getField( propertyType, "userType" );
            descriptor.set( "userType", userType );
            if( userType instanceof Converter ) {
                descriptor.set( "converter", userType );
            }
        }
        
        Field field = Reflection.getField( type, propertyName );
        if( field != null ) {
            descriptor.set( "field", field );

            Column column = field.getAnnotation( Column.class );
            ColumnDescriptor colDesc = new ColumnDescriptor();
            descriptor.set( "column", colDesc );

            if( column != null ) {
                colDesc.set( "name", column.name() );
            } else {
                
                JoinColumn joinColumn = field.getAnnotation( JoinColumn.class );
                if( joinColumn != null ) {
                    colDesc.set( "plain", false );
                    colDesc.set( "name", joinColumn.name() );
                } else {
                    colDesc.set( "name", propertyName );
                }
                
            }

            if( field.getAnnotation( OneToMany.class ) != null ) {
                descriptor
                    .set( "association", EntityPropertyDescriptor.Association.ONE_TO_MANY )
                    .set( "target", collectionMetadata( propertyName ).getElementType().getReturnedClass() );
            
            } else if( field.getAnnotation( OneToOne.class ) != null ) {
                descriptor
                    .set( "association", EntityPropertyDescriptor.Association.ONE_TO_ONE )
                    .set( "target", propertyType.getReturnedClass() );
            
            } else if( field.getAnnotation( ManyToOne.class ) != null ) {
                descriptor
                    .set( "association", EntityPropertyDescriptor.Association.MANY_TO_ONE )
                    .set( "target", propertyType.getReturnedClass() );
            
            } else if( field.getAnnotation( ManyToMany.class ) != null ) {
                descriptor
                    .set( "association", EntityPropertyDescriptor.Association.MANY_TO_MANY )
                    .set( "target", collectionMetadata( propertyName ).getElementType().getReturnedClass() );
            }            
 
        }
        
        return descriptor;
        
    }

    public CollectionMetadata collectionMetadata( String name ) {
        Map<String, CollectionMetadata> allCollectionMetadata = sessionFactory.getAllCollectionMetadata();
        return allCollectionMetadata.get( type.getName() + "." + name );
    }
        
}