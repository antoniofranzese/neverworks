package it.neverworks.persistence.hibernate.entities;

import java.io.Serializable;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.EntityMode;
import org.hibernate.SessionFactory;
import org.hibernate.MappingException;
import org.hibernate.metadata.ClassMetadata;

import org.hibernate.jdbc.Work;
import org.hibernate.impl.SessionFactoryImpl;
import org.hibernate.dialect.Dialect;
import org.hibernate.event.EventSource;
import org.hibernate.event.EventListeners;
import org.hibernate.event.PostLoadEventListener;
import org.hibernate.event.PreInsertEventListener;
import org.hibernate.event.PostInsertEventListener;
import org.hibernate.event.PreUpdateEventListener;
import org.hibernate.event.PostUpdateEventListener;
import org.hibernate.event.PreDeleteEventListener;
import org.hibernate.event.PostDeleteEventListener;
import org.hibernate.event.SaveOrUpdateEventListener;
import org.hibernate.event.FlushEntityEventListener;
import org.hibernate.event.FlushEventListener;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.Interceptor;
import org.springframework.beans.factory.InitializingBean;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.model.Value;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Required;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Evaluate;
import it.neverworks.model.expressions.Expression;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.persistence.Listeners;
import it.neverworks.persistence.PersistenceException;
import it.neverworks.persistence.entities.EntityManager;
import it.neverworks.persistence.entities.EntityMetadata;
import it.neverworks.persistence.entities.EntityContext;
import it.neverworks.persistence.entities.EntityPersister;
import it.neverworks.persistence.entities.EntityEngine;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.criteria.Query;
import it.neverworks.persistence.hibernate.SessionContext;
import it.neverworks.persistence.hibernate.SessionLocalContextComponent;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;
import it.neverworks.persistence.hibernate.criteria.HibernateQuery;
import it.neverworks.persistence.hibernate.criteria.SequenceQuery;
import it.neverworks.persistence.hibernate.criteria.NamedQueryFactory;
import it.neverworks.persistence.hibernate.criteria.HQLQueryFactory;
import it.neverworks.persistence.hibernate.criteria.SQLQueryFactory;
import it.neverworks.context.ContextManagerAware;
import it.neverworks.context.ContextManager;

public class SimpleEntityManager extends AbstractModel implements EntityManager, HibernateEntityManager, ContextManagerAware, InitializingBean, Inspect, Retrieve, Evaluate {
	
    @Property
    protected SessionFactory sessionFactory;
    
    protected Map<Class, EntityMetadata> classMetadata = new HashMap<Class, EntityMetadata>();
    protected ContextManager contextManager;
    protected List<PreDetachedDeleteEventListener> preDetachedDeleteEventListeners = new ArrayList<>();
    protected List<PostDetachedDeleteEventListener> postDetachedDeleteEventListeners = new ArrayList<>();
    protected EntityEngine engine;

    protected Map persisters = new HashMap();
    
    public <T> EntityPersister<T> on( Class<T> type ) {
        if( ! this.persisters.containsKey( type ) ) {
            synchronized( persisters ) {
                if( ! this.persisters.containsKey( type ) ) {
                    this.persisters.put( type, new HibernateEntityPersister<T>( this, type ) );
                }
            }
        }
        return (EntityPersister<T>) this.persisters.get( type );
    }
    
    public EntityEngine engine() {
        return this.engine;
    }
    
    protected Interceptor createSessionInterceptor() {
        return new SessionInterceptor();
    }
    
    protected void setSessionFactory( Setter<SessionFactory> setter ) {
        if( setter.value() instanceof SessionFactoryImpl ) {
            Reflection.setField( setter.value(), "interceptor", this.createSessionInterceptor() );
        }
        
        setter.set();
        Dialect dialect = this.getDialect();
        
        if( dialect instanceof org.hibernate.dialect.OracleDialect
            || dialect instanceof org.hibernate.dialect.Oracle8iDialect
            || dialect instanceof org.hibernate.dialect.Oracle9Dialect
            || dialect instanceof org.hibernate.dialect.Oracle9iDialect
            || dialect instanceof org.hibernate.dialect.Oracle10gDialect
            || dialect instanceof org.hibernate.dialect.DataDirectOracle9Dialect ) {
            
            this.engine = EntityEngine.ORACLE;
        
        } else if( dialect instanceof org.hibernate.dialect.PostgreSQLDialect ) {
            this.engine = EntityEngine.PGSQL;

        } else if( dialect instanceof org.hibernate.dialect.MySQLDialect ) {
            this.engine = EntityEngine.MYSQL;

        } else {
            this.engine = EntityEngine.UNKNOWN;
        }

    }
    
    public void setContextManager( ContextManager manager ) {
        this.contextManager = manager;
    }
    
    public void afterPropertiesSet() {
        // if( this.contextManager != null ) {
        //     this.contextManager.registerLocalContextComponent( new SessionLocalContextComponent( getSessionFactory() ) );
        // }
    }
    
    public void doWork( Work work ) {
        getSession().doWork( work );
    }
    
    public <T> T fromWork( ReturningWork<T> work ) {
        final Value result = Value.of( null );
        getSession().doWork( connection -> result.set( work.execute( connection ) ) );
        return (T) result.asObject(); 
    }

	protected Object unpack( Object entity ) {
        Object value = Value.from( entity );
		if( value instanceof HibernateProxy ) {
			return ((HibernateProxy) value).getHibernateLazyInitializer().getImplementation();
		} else {
			return value;
		}
	}

    protected FluentList<Class> getAllMappedClasses() {
        FluentList<Class> result = new FluentArrayList<>();
        Map<String,ClassMetadata> allMetadata = this.sessionFactory.getAllClassMetadata();
        for( ClassMetadata classMeta: allMetadata.values() ) {
            result.add( classMeta.getMappedClass( EntityMode.POJO ) );
        }
        return result;
    }
    
    protected ClassMetadata getClassMetadata( Object entity ) {
        if( entity != null ) {
            return getClassMetadata( unpack( entity ).getClass() );
        } else {
            throw new IllegalArgumentException( "Cannot retrieve ClassMetadata on null entity" ); 
        }
    }

    protected ClassMetadata getClassMetadata( Class entityClass ) {
        ClassMetadata meta = getSessionFactory().getClassMetadata( entityClass ); 
        if( meta != null ) {
            return meta;
        } else {
            throw new IllegalArgumentException( "Cannot retrieve ClassMetadata on " + entityClass ); 
        }
    }

    protected boolean hasClassMetadata( Class entityClass ) {
        return getSessionFactory().getClassMetadata( entityClass ) != null; 
    }
    
    public boolean containsItem( Object key ) {
        if( key instanceof Class ) {
            return hasClassMetadata( (Class) key );
        } else if( key instanceof String ) {
            if( Reflection.isClass( (String) key ) ) {
                return hasClassMetadata( Reflection.findClass( (String) key ) );
            } else {
                return this.modelInstance.has( (String) key );
            }
        } else {
            return false;
        }
    }

    public Object retrieveItem( Object key ) {
        if( key instanceof Class ) {
            return getClassMetadata( (Class) key );
        } else if( key instanceof String ) {
            if( Reflection.isClass( (String) key ) ) {
                return metadata( Reflection.findClass( (String) key ) ).persister();
            } else {
                return this.modelInstance.get( (String) key );
            }
        } else {
            throw new IllegalArgumentException( "Invalid key: " + repr( key ) );
        }
    }
    
    public Object evaluateExpression( Expression expression ) {
        if( containsItem( expression.text() ) || expression.size() == 1 ) {
            return retrieveItem( expression.text() );
        } else {
            return expression.sub( 1 ).evaluate( retrieveItem( expression.sub( 0, 1 ).text() ) );
        }
    }
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( name );
    }

    public <T extends EntityManager> T set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }
    
    public EntityContext open() {
        return new SessionContext( this ).open();
    }

    public <T> Finder<T> find( Class<T> type ) {
        return new StatementFinder<T>()
            .initWithManager( this )
            .andType( metadata( type ).type() );
    }
    
    public <T> T create( Class<T> type, Object... params ){
        return newInstance( type, new Arguments( params ) );
    }

    public <T> T create( Class<T> type, Map params ){
        return newInstance( type, Arguments.process( params ) );
    }

    protected <T> T newInstance( Class<T> type, Arguments arguments ){
        T instance = (T)Reflection.newInstance( type );
        if( arguments != null ) {
            arguments.populate( instance );
        }
        return instance;
    }
    
	public <T> T load( Class<T> type, Object key ) {
        EntityMetadata meta = metadata( type );
        return (T)getSession().load( meta.type(), (Serializable) meta.identifier().convert( Value.from( key ) ) );
    }
    
    public <T> T load( T example ) {
        if( example != null ) {
            EntityMetadata meta = metadata( unpack( example ).getClass() );
            return load( (Class<T>) meta.type(), (Serializable) eval( example, meta.identifier().getName() ) );
        } else {
            throw new PersistenceException( "Null example in load" );
        }
    }

	public <T> T save( T entity ){
        T value = (T) Value.from( entity );
        getSession().saveOrUpdate( value );
        return value;
    }
    
	public <T> T insert( T  entity ){
        T value = (T) Value.from( entity );
        getSession().save( value );
        return value;
    }
    
	public <T> T update( T entity ){
        T value = (T) Value.from( entity );
        getSession().update( value );
        return value;
    }
    
	public <T> T delete( T entity ){
        T value = (T) unpack( entity );
        if( getSession().contains( value ) ) {
            getSession().delete( value );
        } else {
            detachedDelete( value );
        }
        return value;
    }
    
    protected void detachedDelete( Object entity ) {
        Class<?> type = entity.getClass();
        String idProperty = getSessionFactory().getClassMetadata( type ).getIdentifierPropertyName();
        Serializable idValue = (Serializable) eval( entity, idProperty );
        boolean vetoed = false;

        if( this.preDetachedDeleteEventListeners.size() > 0 ) {
            PreDetachedDeleteEvent event = new PreDetachedDeleteEvent( type, idValue, (EventSource) getSession() );
            int i = 0;
            while( ! vetoed && i < this.preDetachedDeleteEventListeners.size() ) {
                if( this.preDetachedDeleteEventListeners.get( i++ ).onPreDetachedDelete( event ) ) {
                    vetoed = true;
                }
            }
        }

        if( ! vetoed ) {
            int affected = getSession()
                .createQuery( "delete from " + type.getName() + " where " + idProperty + " = :id" )
                .setParameter( "id", idValue )
                .executeUpdate();
        
            if( affected != 1 ) {
                throw new PersistenceException( "Unexpected row count deleting " + type.getName() + " instance with id " + idValue + ": " + affected );
            }
            
            if( this.postDetachedDeleteEventListeners.size() > 0 ) {
                PostDetachedDeleteEvent event = new PostDetachedDeleteEvent( type, idValue, (EventSource) getSession() );
                int i = 0;
                for( PostDetachedDeleteEventListener listener: this.postDetachedDeleteEventListeners ) {
                    listener.onPostDetachedDelete( event );
                }
            }
            
        }
    
    }

    public Criteria criteria( Class<?> type ) {
        return getSession().createCriteria( type );
    }
    
    public Session getSession() {
        return getSessionFactory().getCurrentSession();
    }
    
    public SessionFactory getSessionFactory(){
        return this.sessionFactory;
    }
    
    public void setSessionFactory( SessionFactory sessionFactory ){
        this.sessionFactory = sessionFactory;
    }
    
    public EntityMetadata metadata( Class type ) {
        if( !( classMetadata.containsKey( type ) ) ) {
            synchronized( classMetadata ) {
                if( !( classMetadata.containsKey( type ) ) ) {
                    HibernateMetadata meta = new HibernateMetadata( this, type );
                    this.processMetadata( meta );
                    classMetadata.put( type, meta );
                }
            }
        }
        return classMetadata.get( type );
    }
    
    protected void processMetadata( HibernateMetadata meta ) {
        Class cls = meta.get( "type" );
        if( Annotations.has( cls, Listeners.class ) ) {
            throw new PersistenceException( cls.getName() + " needs Listeners, but this is not a ListeningEntityManager" );
        }
    }
    
    public <T> Query<T> query( String body, Map params ) {
        Query<T> q = query( body );
        q.setAll( Value.from( params ) );
        return q;
    }
    
    public <T> Query<T> query( String body, Object... params ) {
        Query<T> q = query( body );
        q.setAll( Value.from( params ) );
        return q;
    }
    
    public boolean hasNamedQuery( String name ) {
        if( sessionFactory instanceof SessionFactoryImpl ) {
            return ((SessionFactoryImpl) sessionFactory).getNamedQuery( name ) != null || ((SessionFactoryImpl) sessionFactory).getNamedSQLQuery( name ) != null;
        } else {
            throw new PersistenceException( "Cannot access named query metadata for this session factory: " + repr( sessionFactory ) );
        }
    }

    public <T> HibernateQuery<T> getNamedQuery( String name ) {
        return new HibernateQuery<T>( new NamedQueryFactory( this, name ) );
    }

    public <T> HibernateQuery<T> createQuery( String body ) {
		if( body.toUpperCase().startsWith( "SQL:") ) {
            return new HibernateQuery<T>( new SQLQueryFactory( this, body.substring( 4 ) ) );

		} else if( body.toUpperCase().startsWith( "NEXT:") ) {
            return new SequenceQuery<T>( this, body.substring( 5 ).trim() );

		} else if( hasNamedQuery( body ) ) {
            return new HibernateQuery<T>( new NamedQueryFactory( this, body ) );

        } else {
            return new HibernateQuery<T>( new HQLQueryFactory( this, body ) );
		}	
    }
    
    public <T> Query<T> query( String body ) {
		if( Strings.hasText( body )) {
    		
            if( hasNamedQuery( body ) ) {
                return getNamedQuery( body );
            } else {
                return createQuery( body );
            }

		} else {
			throw new PersistenceException( "Empty query body" );
		}
        
    }

    public <T> Query<T> query( Class<T> type, String body, Map params ) {
        Query<T> q = query( type, body );
        q.setAll( Value.from( params ) );
        return q;
    }
    
    public <T> Query<T> query( Class<T> type, String body, Object... params ) {
        Query<T> q = query( type, body );
        q.setAll( Value.from( params ) );
        return q;
    }
    
    public <T> Query<T> query( Class<T> type, String body ) {
		if( Strings.hasText( body ) ) {
			if( body.indexOf( "#this") >= 0 ) {
				return createQuery( body.replaceAll( "#this", type.getName() ) );
			} else {
			    return query( body );
			}
		} else {
			throw new PersistenceException( "Empty query body" );
		}
        
    }
    
    public <T> T forget( T entity ) {
        T value = (T) Value.from( entity );
        if( getSession().contains( value ) ) {
            getSession().evict( value );
        }
        return value;
    }

    public <T> T merge( T entity ) {
        return (T) getSession().merge( Value.from( entity ) );
    }

    public <T> T refresh( T entity ) {
        T e = (T) Value.from( entity );
        getSession().refresh( e );
        return e;
    }
    
    public void reset() {
        getSession().clear();
    }
    
    public String toString() {
        return new ToStringBuilder( this ).toString();
    }

    public Dialect getDialect() {
        if( sessionFactory instanceof SessionFactoryImpl ) {
            return ((SessionFactoryImpl) sessionFactory).getDialect();
        } else {
            throw new PersistenceException( "Cannot access dialect for this session factory: " + repr( sessionFactory ) );
        }
    }
    
    protected EventListeners getEventListeners() {
        if( sessionFactory instanceof SessionFactoryImpl ) {
            return ((SessionFactoryImpl) sessionFactory).getEventListeners();
        } else {
            throw new PersistenceException( "Cannot access listeners for this session factory: " + repr( sessionFactory ) );
        }
    }

    public void register( Object listener ) {
        EventListeners el = getEventListeners();

        if( listener instanceof PostLoadEventListener ) {
            el.setPostLoadEventListeners( list()
                .cat( el.getPostLoadEventListeners() )
                .append( listener )
                .array( PostLoadEventListener.class )
            );
        }
            
        if( listener instanceof PreInsertEventListener ) {
            el.setPreInsertEventListeners( list()
                .cat( el.getPreInsertEventListeners() )
                .append( listener )
                .array( PreInsertEventListener.class )
            );
        }
            
        if( listener instanceof PostInsertEventListener ) {
            el.setPostInsertEventListeners( list()
                .cat( el.getPostInsertEventListeners() )
                .append( listener )
                .array( PostInsertEventListener.class )
            );
        }
            
        if( listener instanceof PreUpdateEventListener ) {
            el.setPreUpdateEventListeners( list()
                .cat( el.getPreUpdateEventListeners() )
                .append( listener )
                .array( PreUpdateEventListener.class )
            );
        }
            
        if( listener instanceof PostUpdateEventListener ) {
            el.setPostUpdateEventListeners( list()
                .cat( el.getPostUpdateEventListeners() )
                .append( listener )
                .array( PostUpdateEventListener.class )
            );
        }
            
        if( listener instanceof PreDeleteEventListener ) {
            el.setPreDeleteEventListeners( list()
                .cat( el.getPreDeleteEventListeners() )
                .append( listener )
                .array( PreDeleteEventListener.class )
            );
        }
    
        if( listener instanceof PostDeleteEventListener ) {
            el.setPostDeleteEventListeners( list()
                .cat( el.getPostDeleteEventListeners() )
                .append( listener )
                .array( PostDeleteEventListener.class )
            );
        }
    
        if( listener instanceof SaveOrUpdateEventListener ) {
            el.setSaveOrUpdateEventListeners( list()
                .cat( el.getSaveOrUpdateEventListeners() )
                .append( listener )
                .array( SaveOrUpdateEventListener.class )
            );
        }

        if( listener instanceof FlushEntityEventListener ) {
            el.setFlushEntityEventListeners( list()
                .cat( el.getFlushEntityEventListeners() )
                .append( listener )
                .array( FlushEntityEventListener.class )
            );
        }

        if( listener instanceof FlushEventListener ) {
            el.setFlushEventListeners( list()
                .cat( el.getFlushEventListeners() )
                .append( listener )
                .array( FlushEventListener.class )
            );
        }
        
        if( listener instanceof PreDetachedDeleteEventListener ) {
            this.preDetachedDeleteEventListeners.add( (PreDetachedDeleteEventListener) listener );
        }

        if( listener instanceof PostDetachedDeleteEventListener ) {
            this.postDetachedDeleteEventListeners.add( (PostDetachedDeleteEventListener) listener );
        }
                
    }
    
}