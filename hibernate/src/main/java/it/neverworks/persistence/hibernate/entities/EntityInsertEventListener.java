package it.neverworks.persistence.hibernate.entities;

import static it.neverworks.language.*;
import it.neverworks.persistence.Veto;
import it.neverworks.persistence.Insert;
import it.neverworks.persistence.InsertEvent;
import it.neverworks.persistence.PersistenceEvent;
import it.neverworks.persistence.PersistenceEventFactory;

import org.hibernate.event.PreInsertEvent;
import org.hibernate.event.PreInsertEventListener;

/**
 * @author antonio
 *
 * Gestisce i listener @Insert
 */
public class EntityInsertEventListener extends AbstractListener implements PreInsertEventListener {

	@Override
	public boolean onPreInsert( PreInsertEvent event ) {
        
        try {
            if( manager.notify( event.getEntity(), Insert.class, new InsertEventFactory( event ) ) ){
                updateState( event, event.getEntity(), event.getPersister().getPropertyNames(), event.getState() );
            }
            return false;
        } catch( Veto v ) {
            return true;
        }
	}
    
    private static class InsertEventFactory implements PersistenceEventFactory {
        private PreInsertEvent event;
        
        public InsertEventFactory( PreInsertEvent event ) {
            this.event = event;
        }
        
        public PersistenceEvent build() {
            return new HibernateInsertEvent().set( "state", new PropagatingEntityState( event.getEntity(), event.getPersister().getPropertyNames(), event.getState() ) );
        }
    };
}
