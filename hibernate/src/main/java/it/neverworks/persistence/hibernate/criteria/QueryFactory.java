package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.Query;
import java.util.Iterator;

public interface QueryFactory {
    Query createQuery();
    
    default Iterator iterate( Query query ) {
        return query.list().iterator();
    }
}