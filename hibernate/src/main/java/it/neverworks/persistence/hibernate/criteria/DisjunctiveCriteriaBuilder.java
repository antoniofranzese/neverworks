package it.neverworks.persistence.hibernate.criteria;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;

import it.neverworks.persistence.criteria.CriteriaBuilder;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.hibernate.criteria.statement.BetweenStatement;
import it.neverworks.persistence.hibernate.criteria.statement.EqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.GreaterEqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.GreaterThanStatement;
import it.neverworks.persistence.hibernate.criteria.statement.InStatement;
import it.neverworks.persistence.hibernate.criteria.statement.LessEqualStatement;
import it.neverworks.persistence.hibernate.criteria.statement.LessThanStatement;
import it.neverworks.persistence.hibernate.criteria.statement.LikeStatement;
import it.neverworks.persistence.hibernate.criteria.statement.NullStatement;
import it.neverworks.persistence.hibernate.criteria.statement.DisjunctiveStatement;
import it.neverworks.persistence.hibernate.criteria.statement.SelectiveStatement;

import java.util.Map;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import it.neverworks.log.Logger;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class DisjunctiveCriteriaBuilder<T> extends StatementCriteriaBuilder {

	private final static Logger logger = Logger.of( StatementFinder.class );

    //     @Property @Required
    // private StatementFinder<T> finder;
    //
    //     @Property @Required
    // private String property;
    
    @Property @Required
    protected DisjunctiveStatement statement;
    
    // private boolean negation = false;
	
	public DisjunctiveCriteriaBuilder( StatementFinder finder, String property, DisjunctiveStatement statement ) {
        super( finder, property );
        this.statement = statement;
	}
	
    @Override 
    public void appendStatement( Object statement ) {
        this.statement.addDisjunction( (SelectiveStatement) statement );
    }
	
	@Override
	public Finder<T> using( Object... values ) {
        throw unimplemented( "using" );
	}

	@Override
	public Finder<T> using( Map arguments ) {
        throw unimplemented( "using" );
	}
	
    private RuntimeException unimplemented( String method ) {
        return new RuntimeException( Strings.message( "\"{0}\" disjunction is not supported", method ) );
    }
	
}
