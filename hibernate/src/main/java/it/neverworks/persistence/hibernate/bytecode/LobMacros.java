package it.neverworks.persistence.hibernate.bytecode;

import javassist.CtClass;
import javassist.CtField;

import javax.persistence.Lob;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Parameter;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.persistence.Blob;
import it.neverworks.persistence.Clob;
import it.neverworks.model.bytecode.ByteCodeUtils;

public class LobMacros implements FieldProcessor {
    
    public void process( CtClass cls, CtField field ) throws Exception {

        if( ! ByteCodeUtils.isMarked( field, "entities.lob" ) ) {
            // Espande la macro @Blob
            if( field.hasAnnotation( Blob.class ) 
                && ! field.hasAnnotation( Lob.class )
                && ! field.hasAnnotation( Type.class ) ) {
            
                String type = null;
                if( field.getType().getName().equals( "java.io.InputStream" ) ) {
                    type = "it.neverworks.persistence.hibernate.BlobBinaryStreamType";
                } else if( field.getType().getName().equals( "byte[]" ) ) {
                    type = "it.neverworks.persistence.hibernate.BlobByteArrayType";
                }
            
                if( type != null ) {
                    ByteCodeUtils.createAnnotation( cls, Lob.class )
                        .annotate( field );
                
                    ByteCodeUtils.createAnnotation( cls, Type.class )
                        .set( "type", type )
                        .set( "parameters", list(
                            ByteCodeUtils.createAnnotation( cls, Parameter.class )
                                .set( "name", "blankable" )
                                .set( "value", ((Blob) field.getAnnotation( Blob.class )).blankable() ? "true" : "false" )
                                .annotation()
                        ))
                        .annotate( field );
                        
                    ByteCodeUtils.mark( field, "entities.lob" );

                } else {
                    throw new Exception( "Cannot map @Blob on " + field + ", invalid field type: " + field.getType().getName() );
                }
        
            // Espande la macro @Clob                  
            } else if( field.hasAnnotation( Clob.class ) 
                && ! field.hasAnnotation( Lob.class )
                && ! field.hasAnnotation( Type.class ) ) {
            
                String type = null;

                if( field.getType().getName().equals( "java.io.InputStream" ) ) {
                    type = "it.neverworks.persistence.hibernate.ClobAsciiStreamType";
                } else if( field.getType().getName().equals( "java.io.Reader" ) ) {
                    type = "it.neverworks.persistence.hibernate.ClobCharacterReaderType";
                } else if( field.getType().getName().equals( "java.lang.String" ) ) {
                    type = "it.neverworks.persistence.hibernate.ClobStringType";
                }
            
                if( type != null ) {
                    ByteCodeUtils.createAnnotation( cls, Lob.class )
                        .annotate( field );
                
                    ByteCodeUtils.createAnnotation( cls, Type.class )
                        .set( "type", type )
                        .set( "parameters", list(
                            ByteCodeUtils.createAnnotation( cls, Parameter.class )
                                .set( "name", "blankable" )
                                .set( "value", ((Clob) field.getAnnotation( Clob.class )).blankable() ? "true" : "false" )
                                .annotation()
                        ))
                        .annotate( field );

                    ByteCodeUtils.mark( field, "entities.lob" );

                } else {
                    throw new Exception( "Cannot map @Clob on " + field + ", unknown type: " + field.getType().getName() );
                }
            }
        }
        
    }
    
}