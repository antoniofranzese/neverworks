package it.neverworks.persistence.hibernate.entities;

import it.neverworks.persistence.FindEvent;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class LazyFindEvent extends FindEvent {
    
    protected ListeningEntityManager manager;
    
    public LazyFindEvent( ListeningEntityManager manager, Class target ) {
        this.manager = manager;
        this.target = target;
    }
    
    @Override
    public <F> Finder<F> getFinder() {
        if( this.finder == null ) {
            this.finder = manager.buildFinder( this.target );
        }
        return (Finder<F>) this.finder;
    }
    
    public ListeningEntityManager getManager(){
        return this.manager;
    }

}