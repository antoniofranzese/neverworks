package it.neverworks.persistence.hibernate.criteria;

import org.hibernate.criterion.Projections;
import org.hibernate.criterion.ProjectionList;

import it.neverworks.log.Logger;
import it.neverworks.model.utils.Identification;

@Identification({ "property", "alias" })
public class PropertyProjectionItem implements ProjectionItem {

    private static Logger logger = Logger.of( StatementFinder.class );
    
    private String property;
    private String alias;
    
    public PropertyProjectionItem( String property, String alias ) {
        this.property = property;
        this.alias = alias;
    }

    public ProjectionList populate( ProjectionList list ) {
		logger.trace( "Adding projection property {}, with alias {}", this.property, this.alias );
		list.add( Projections.property( this.property ), this.alias );
        return list;
    }

    public String getProperty(){
        return this.property;
    }
    
    public String getAlias(){
        return this.alias;
    }
}