package it.neverworks.persistence.hibernate.entities;

import it.neverworks.lang.Arguments;
import it.neverworks.persistence.CreateEvent;
import it.neverworks.persistence.criteria.Finder;
import it.neverworks.persistence.hibernate.criteria.StatementFinder;

public class LazyCreateEvent extends CreateEvent {
    
    protected ListeningEntityManager manager;
    
    public LazyCreateEvent( ListeningEntityManager manager, Class target, Arguments arguments ) {
        this.manager = manager;
        this.target = target;
        this.arguments = arguments;
    }
    
    @Override
    public Object getEntity() {
        if( this.entity == null ) {
            this.entity = manager.buildInstance( this.target, this.arguments );
        }
        return this.entity;
    }
    
    public ListeningEntityManager getManager(){
        return this.manager;
    }

}