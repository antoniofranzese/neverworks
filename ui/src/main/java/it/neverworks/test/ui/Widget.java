package it.neverworks.test.ui;

import java.util.List;
import java.util.Map;

import it.neverworks.lang.Arguments;

public interface Widget {
    
    public Widget widget( String name );
    
    public <T> T get( String name );
    public <T extends Widget> T set( String name, Object value );
    
    public boolean exists();
    public Widget fire( String name );
    public Widget fire( String name, Arguments arguments );
    
    public String getId();
    public String getType();
}