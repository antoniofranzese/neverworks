package it.neverworks.test.ui.web;

public interface URLMapper {
    public String map( Class cls );
}