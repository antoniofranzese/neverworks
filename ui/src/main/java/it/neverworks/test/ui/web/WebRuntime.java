package it.neverworks.test.ui.web;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.awt.Rectangle;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.AlphaComposite;
import java.awt.RenderingHints;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.OutputType;

import static it.neverworks.language.*;
import it.neverworks.io.Files;
import it.neverworks.lang.Errors;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Required;

import it.neverworks.test.ui.Runtime;
import it.neverworks.test.ui.Form;

public class WebRuntime extends BaseWebProxy implements Runtime {

    public static List VALID_FORM_WIDGETS = list( "works.form.Manager", "works.form.manager.FormContentPane", "works.form.PopupProxy" );

    @Property
    private String root;
    
    @Property
    private int timeout = 60;
    
    @Property @Required
    private URLMapper mapper;
    
    private Integer width;
    private Integer height;
    private boolean resized = false;
    
    public void resize() {
        if( !resized && width != null && height != null ) {
            driver.manage().window().setSize( new org.openqa.selenium.Dimension( width, height ) );
            Map clientSize = (Map)execute( "return test.size()" );

            Integer clientWidth = ((Long)clientSize.get( "width" )).intValue();
            Integer clientHeight = ((Long)clientSize.get( "height" )).intValue();
            Integer widthCorrection = 0; 
            Integer heightCorrection = 0; 

            if( clientWidth != width ) {
                widthCorrection = width - clientWidth; 
            }

            if( clientHeight != height ) {
                heightCorrection = height - clientHeight; 
            }

            if( widthCorrection != 0 || heightCorrection != 0 ) {
                driver.manage().window().setSize( new org.openqa.selenium.Dimension( width + widthCorrection, height + heightCorrection ) );
            }
            resized = true;
            
        }
        //driver.manage().window().setPosition(new Point(0, 0));
    }
    
    public void quit() {
        driver.quit();
    }
    
    public Runtime load( Class cls ) {
        return load( getMapper().map( cls ) );
    }
    
    public Runtime load( String url ) {
        navigate( ( this.root != null ? this.root : "" ) + url );
        wait( "window.require != undefined" );
        executeAsync( "require( [ 'works/dev/test' ], function( test ){ window.test = test; test.app.then( callback ) });" );
        resize();
        return this;
    }
    
    public Form form() {
        return form( null );
    }
    
    public Form form( String name ) {
        // System.out.println( "Asking form " + name );
        Map result = process( execute( "console.log( 'argf', arguments ); return test.form( arguments[ 0 ] );", name ) ); 
        sync();
        if( VALID_FORM_WIDGETS.contains( result.get( "type" ) ) ) {
            return new WebForm()
                .set( "driver", driver )
                .set( "id", result.get( "id" ) )
                .set( "type", result.get( "type" ) )
                .set( "runtime", this );
        } else {
            throw new RuntimeException( "Not a valid form widget: " + name + " (" + result.get( "type" ) + ")" );
        }
    }

    public Runtime sync() {
        Map result = process( execute( "return test.sync();" ) );
        if( result.containsKey( "messages" ) ) {
            for( Object message: (List) result.get( "messages" ) ) {
                System.out.println( "console> " + message );
            }
        }
        return this;
    }

    public File shoot( String file ) {
        return shoot( file, null, null );
    }

    public File shoot( File file ) {
        return shoot( file, null, null );
    }

    public File shoot( String file, Rectangle crop ) {
        return shoot( file, crop, null );
    }

    public File shoot( File file, Rectangle crop ) {
        return shoot( file, crop, null );
    }
    
    public File shoot( String file, Dimension size ){
        return shoot( file, null, size );
    }
    
    public File shoot( File file, Dimension size ){
        return shoot( file, null, size );
    }
    
    public File shoot( String file, Rectangle crop, Dimension size ){
        return shoot( new File( file ), crop, size );
    }
    
    public File shoot( File file, Rectangle crop, Dimension size ){
        File source = ((TakesScreenshot)driver ).getScreenshotAs( OutputType.FILE );
        executeAsync( "test.shoot().then( callback )" );

        if( crop != null ) {
            try {
                BufferedImage sourceImage = ImageIO.read( source );
                BufferedImage croppedImage = sourceImage.getSubimage( crop.x, crop.y, crop.width, crop.height );
                //TODO: verificare estensione per cambiare formato
                ImageIO.write( croppedImage, "png", file );
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            Files.copy( source, file );

        }

        if( size != null ) {
            try {
                BufferedImage image = ImageIO.read( file );
            
                BufferedImage resizedImage = new BufferedImage( (int)size.getWidth(), (int)size.getHeight(), image.getType() );
                Graphics2D graph = resizedImage.createGraphics();

                graph.setComposite( AlphaComposite.Src );
                graph.setRenderingHint( RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR );
                graph.setRenderingHint( RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY );
                //graph.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );

                graph.drawImage( image, 0, 0, (int)size.getWidth(), (int)size.getHeight(), null );
                graph.dispose();
            
                ImageIO.write( resizedImage, "png", file );
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        }
        
        return file;
    }
    

    public <T> T get( String name ) {
        return (T)model().get( name );
    }

    public <T extends Runtime> T set( String name, Object value ) {
        model().set( name, value );
        return (T)this;
    }

    public String getRoot(){
        return this.root;
    }
    
    public void setRoot( String root ){
        this.root = root;
    }

    public int getTimeout(){
        return this.timeout;
    }
    
    public void setTimeout( int timeout ){
        this.timeout = timeout;
    }
    
    public URLMapper getMapper(){
        return this.mapper;
    }
    
    public void setMapper( URLMapper mapper ){
        this.mapper = mapper;
    }

    public Integer getHeight(){
        return this.height;
    }
    
    public void setHeight( Integer height ){
        this.height = height;
    }
    
    public Integer getWidth(){
        return this.width;
    }
    
    public void setWidth( Integer width ){
        this.width = width;
    }

}