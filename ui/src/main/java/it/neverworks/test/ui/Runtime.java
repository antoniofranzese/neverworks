package it.neverworks.test.ui;

import java.io.File;
import java.awt.Rectangle;
import java.awt.Dimension;

public interface Runtime {

    public void quit();
    public Runtime load( Class cls );
    
    public Form form();
    public Form form( String name );
    public Runtime sync();
    
    public File shoot( String file );
    public File shoot( File file );
    public File shoot( String file, Rectangle crop );
    public File shoot( File file, Rectangle crop );
    public File shoot( String file, Dimension size );
    public File shoot( File file, Dimension size );
    public File shoot( String file, Rectangle crop, Dimension size );
    public File shoot( File file, Rectangle crop, Dimension size );
    
}