package it.neverworks.test.ui.web;

import java.util.Map;
import java.util.ArrayList;
import java.util.Collection;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import it.neverworks.lang.Collections;
import it.neverworks.model.Property;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.utils.ToStringBuilder;

public abstract class BaseWebProxy extends AbstractModel {
    
    public static class JavascriptCondition implements ExpectedCondition<Boolean> {
        private String condition;
        
        public JavascriptCondition( String condition ) {
            super();
            this.condition = condition;
        }
        
        public Boolean apply( WebDriver driver ) {
            return (Boolean) ((JavascriptExecutor) driver).executeScript( "return " + this.condition );
        }
    }
    
    @Property    
    protected WebDriver driver;
    
    @Property
    protected String id;
    
    @Property
    private int timeout = 60 ;
    
    public int getTimeout(){
        return this.timeout;
    }
    
    public void setTimeout( int timeout ){
        this.timeout = timeout;
    }
    
    public String getId(){
        return this.id;
    }
    
    public void setId( String id ){
        this.id = id;
    }
    
    public WebDriver getDriver(){
        return this.driver;
    }
    
    public void setDriver( WebDriver driver ){
        this.driver = driver;
    }
    
    protected void navigate( String url ) {
        driver.get( url );
        execute( "window.ASYNC_SCRIPTS = { " + 
            "running: false " + 
            ",startup: function(){ this.running = true; delete this.result; } " +
            ",resolve: function( result ){ this.result = result; this.running = false } " + 
            "}"        
        );
    }
    
    protected Map process( Object result ) {
        if( result instanceof Map ) {
            if( ((Map) result).containsKey( "error" ) ) {
                throw new RuntimeException( (String) ((Map) result).get( "error" ) );
            } else {
                return (Map) result;
            }
        } else {
            throw new RuntimeException( "Unknown result type: " + ( result != null ? result.getClass().getName() : "null" ) );
        }
    }
    
    public void wait( String condition ) {
        wait( new JavascriptCondition( condition ), this.timeout );
    }

    public void wait( String condition, int timeout ) {
        wait( new JavascriptCondition( condition ), timeout );
    }

    public void wait( ExpectedCondition condition ) {
        wait( condition, this.timeout );
    }

    public void wait( ExpectedCondition condition, int timeout ) {
        WebDriverWait wait = new WebDriverWait( driver, timeout );
        wait.until( condition );
    }
    
    public Object execute( String script, Object... arguments ) {
        // Firefox bug: gli argomenti non vengono passati
        if( driver instanceof FirefoxDriver ) {
            StringBuilder fullScript = new StringBuilder( "arguments=[];");
            for( int i = 0; i < arguments.length; i++ ) {
                fullScript
                    .append( "arguments[" )
                    .append( i )
                    .append( "]=" )
                    .append( js( arguments[ i ] ) )
                    .append( ";");
            }
            fullScript.append( script );
            return ((JavascriptExecutor) driver).executeScript( fullScript.toString() );
        } else {
            return ((JavascriptExecutor) driver).executeScript( script, arguments );
        }
    }
    
    public Object executeAsync( String script, Object... arguments ) {
        execute( "window.ASYNC_SCRIPTS.startup(); var callback = function( r ){ window.ASYNC_SCRIPTS.resolve( r ) }; " + script, arguments );
        wait( "window.ASYNC_SCRIPTS.running == false;" );
        return execute( "return window.ASYNC_SCRIPTS.result;" );
    }

    public String js( Object value ) {
        if( value == null ) {
            return "null";
        } else if( value instanceof Boolean ) {
            return Boolean.TRUE.equals( value ) ? "true" : "false";
        } else if( value instanceof Number ) {
            return String.valueOf( value );
        } else if( value instanceof Collection ) {
            ArrayList ls = new ArrayList();
            for( Object obj: ((Collection) value) ) {
                ls.add( js( obj ) );
            }
            return "[" + Collections.join( ls, "," ) + "]";
        } else {
            return "\"" + value.toString() + "\"";
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "id" ).toString();
    }
    
}