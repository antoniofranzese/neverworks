package it.neverworks.test.ui;

public interface Form extends Widget {
    
    public Widget widget( String name );
    public Form form( String name );
    public boolean contains( Widget widget );
    public boolean contains( String id );
}