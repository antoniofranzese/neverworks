package it.neverworks.test.ui.web;

import java.util.Map;
import it.neverworks.test.ui.Form;
import it.neverworks.test.ui.Widget;

public class WebForm extends WebWidget implements Form {
    
    public Widget widget( String name ) {
        // System.out.println( "Asking for " + id + ", " + name );
        Object exec = execute( "return test.widget( arguments[ 0 ], arguments[ 1 ] );", id, name );
        getRuntime().sync();
        Map result = process( exec );
        return new WebWidget()
            .set( "driver", driver )
            .set( "form", this )
            .set( "id", result.get( "id" ) )
            .set( "type", result.get( "type" ) )
            .set( "runtime", this.getRuntime() );
    }
    
    public Form form( String name ) {
        Map result = process( execute( "return test.widget( arguments[ 0 ], arguments[ 1 ] );", id, name ) );
        if( WebRuntime.VALID_FORM_WIDGETS.contains( result.get( "type" ) ) ) {
            return new WebForm()
                .set( "driver", driver )
                .set( "id", result.get( "id" ) )
                .set( "type", result.get( "type" ) )
                .set( "runtime", this.getRuntime() );
        } else {
            throw new RuntimeException( "Not a valid form widget: " + name + " (" + result.get( "type" ) + ")" );
        }
    }
    
    public boolean contains( Widget widget ) {
        return contains( widget.getId() );
    }
    
    public boolean contains( String id ) {
        Map result = process( execute( "return test.contains( arguments[ 0 ], arguments[ 1 ] );", this.id, id ) );
        return result.get( "contained" ).equals( Boolean.TRUE );
    }
}