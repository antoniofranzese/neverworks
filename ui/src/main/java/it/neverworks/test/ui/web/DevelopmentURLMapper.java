package it.neverworks.test.ui.web;

import it.neverworks.model.BaseModel;

public class DevelopmentURLMapper extends BaseModel implements URLMapper {
    
    private String prefix = "/form/"; 
    private String suffix = "?NORELOAD";
    
    public String map( Class cls ) {
        return prefix + cls.getName().replace( ".", "/" ) + suffix;
    }

    public void setPrefix( String prefix ){
        this.prefix = prefix;
    }
    
    public void setSuffix( String suffix ){
        this.suffix = suffix;
    }
    
    
}