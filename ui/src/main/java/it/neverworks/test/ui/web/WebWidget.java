package it.neverworks.test.ui.web;

import java.util.List;
import java.util.Map;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.Property;

import it.neverworks.test.ui.Widget;
import it.neverworks.test.ui.Form;

public class WebWidget extends BaseWebProxy implements Widget {
    
    protected final static List internals = list( "id", "driver", "type", "form", "runtime" );

    @Property
    protected String type;
    
    @Property
    protected Form form;
    
    @Property
    protected WebRuntime runtime;
    
    public <T> T get( String name ) {
        if( internals.contains( name ) ) {
            return (T)model().get( name );
        } else {
            Object path = ExpressionEvaluator.isExpression( name ) ? ExpressionEvaluator.parse( name ) : name;
            Map result = process( execute( "return test.get( arguments[ 0 ], arguments[ 1 ] );", this.id, path ) );
            Object value = result.get( "value" );
            if( result.containsKey( "type" ) ) {
                Class type = Reflection.findClass( (String) result.get( "type" ) );
                if( type != null ) {
                    Converter converter = AutoConvertProcessor.inferConverter( type );
                    value = converter.convert( value );
                }
            }
            return (T)value;
        }
    }
    
    public <T extends Widget> T set( String name, Object value ) {
        if( internals.contains( name ) ) {
            model().set( name, value );
        } else {
            executeAsync( "test.set( arguments[ 0 ], arguments[ 1 ], arguments[ 2 ] ).then( callback );", this.id, name, value );
        }
        return (T)this;
    }
    
    public boolean exists() {
        Map result = process( execute( "return test.exists( arguments[ 0 ] );", this.id, id ) );
        return result.get( "exists" ).equals( Boolean.TRUE );
        
    }
    
    public Widget fire( String name ){
        return fire( name, null );
    }
    
    public Widget fire( String name, Arguments arguments ) {
        executeAsync( "test.fire( arguments[ 0 ], arguments[ 1 ], arguments[ 2 ] ).then( callback );", this.id, name, arguments );
        return this;
    }
    
    public Widget widget( String name ) {
        return form.widget( name );
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "id" ).add( "type" ).toString();
    }

    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }
    
    public Form getForm(){
        return this.form;
    }
    
    public void setForm( Form form ){
        this.form = form;
    }
    
    public WebRuntime getRuntime(){
        return this.runtime;
    }
    
    public void setRuntime( WebRuntime runtime ){
        this.runtime = runtime;
    }
    
}