package it.neverworks.test.web;

import org.springframework.beans.factory.FactoryBean;
import org.openqa.selenium.WebDriver;

public abstract class BaseDriverFactoryBean implements FactoryBean {
    
    protected int scriptTimeout = 60; 
    
    public Class getObjectType() {
        return WebDriver.class;
    } 
    
    public boolean isSingleton() {
        return true;
    } 

    public int getScriptTimeout(){
        return this.scriptTimeout;
    }
    
    public void setScriptTimeout( int scriptTimeout ){
        this.scriptTimeout = scriptTimeout;
    }
    
}