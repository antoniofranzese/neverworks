package it.neverworks.test.web;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.FactoryBean;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.WebDriver;

public class FirefoxDriverFactoryBean extends BaseDriverFactoryBean {
    
    public Object getObject(){
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().setScriptTimeout( scriptTimeout, TimeUnit.SECONDS );
        
        return driver;
    }
    
}