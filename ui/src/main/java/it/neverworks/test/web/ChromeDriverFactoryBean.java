package it.neverworks.test.web;

import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.FactoryBean;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;

public class ChromeDriverFactoryBean extends BaseDriverFactoryBean {
    
    public Object getObject(){
        WebDriver driver = new ChromeDriver();
       // driver.manage().timeouts().setScriptTimeout( scriptTimeout, TimeUnit.SECONDS );
        return driver;
    }

}