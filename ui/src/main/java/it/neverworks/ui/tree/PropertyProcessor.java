package it.neverworks.ui.tree;

import it.neverworks.lang.Strings;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.TemplateFormatterConverter;

@Child
@Convert( PropertyProcessorConverter.class )
public class PropertyProcessor extends BaseModel implements ChildModel {
    
    private String path;
    
    @Property @Convert( TemplateFormatterConverter.class )
    private Formatter format;
    
    private Tree tree;
    
    public Object process( Object item ) {
        if( tree != null ) {
            if( format != null ) {
                if( format instanceof ItemFormatter ) {
                    return format.format( item );
                } else if( Strings.hasText( path ) ) {
                    return format.format( tree.getItems().get( item, path ) );
                } else {
                    throw new IllegalArgumentException( "Cannot format caption, missing property path" );
                }
            } else if( Strings.hasText( path ) ) {
                return tree.getItems().get( item, path );
            } else {
                throw new IllegalArgumentException( "Cannot get caption, missing property path" );
            }
        } else {
            throw new IllegalStateException( "Orphan PropertyProcessor" );
        }
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        tree = (Tree) model.actual();
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        tree = null;
    }
    
    
    public Formatter getFormat(){
        return this.format;
    }
    
    public void setFormat( Formatter format ){
        this.format = format;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
    
}