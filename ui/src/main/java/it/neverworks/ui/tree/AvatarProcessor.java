package it.neverworks.ui.tree;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.TemplateFormatterConverter;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.dnd.Avatar;

@Child
@Convert( AvatarProcessorConverter.class )
public class AvatarProcessor extends BaseModel implements ChildModel {
    
    private Object defaultAvatar;
    private String path;
    
    @Property @Convert( TemplateFormatterConverter.class )
    private Formatter format;
    
    private Tree tree;
    
    public Avatar process( Object item ) {
        Object result = null;
        if( tree != null ) {
            if( format != null ) {
                if( format instanceof ItemFormatter ) {
                    result = format.format( item );
                } else if( Strings.hasText( path ) ) {
                    result = format.format( tree.getItems().get( item, path ) );
                } else {
                    throw new IllegalArgumentException( "Cannot format avatar, missing property path" );
                }
            } else if( Strings.hasText( path ) ) {
                try {
                    result = tree.getItems().get( item, path );
                } catch( Exception ex ) {
                    result = null;
                }
            }
        } else {
            throw new IllegalStateException( "Orphan AvatarProcessor" );
        }
        
        if( result != null ) {
            Object candidate = null;
            if( result instanceof String ) {
                String name = (String) result;
                for( Form ancestor: tree.ancestors( Form.class ) ) {
                    if( ancestor.contains( name ) && ancestor.get( name ) instanceof Avatar ) {
                        candidate = ancestor.get( name );
                        break;
                    } 
                }
            } else {
                candidate = result;
            }
        
            if( candidate instanceof Avatar ) {
                return (Avatar) candidate;
            } else {
                throw new IllegalArgumentException( "Object is not an Avatar: " + Objects.repr( candidate ) );
            }
        } else {
            return null;
        }
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        tree = (Tree) model.actual();
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        tree = null;
    }
    
    public Object getDefault(){
        return this.defaultAvatar;
    }
    
    public void setDefault( Object avatar ){
        this.defaultAvatar = avatar;
    }
    
    public Formatter getFormat(){
        return this.format;
    }
    
    public void setFormat( Formatter format ){
        this.format = format;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
    
}