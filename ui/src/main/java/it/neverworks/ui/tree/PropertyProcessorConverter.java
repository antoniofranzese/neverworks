package it.neverworks.ui.tree;

import java.util.Map;
import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.ui.data.TemplateFormatter;
import it.neverworks.ui.data.Formatter;

public class PropertyProcessorConverter implements Converter {

    public Object convert( Object value ) {
        if( value instanceof PropertyProcessor ) {
            return value;

        } else if( value instanceof Formatter ) {
            return new PropertyProcessor().set( "format", value );

        } else if( value instanceof String ) {
            String str = (String) value;
            
            if( Strings.hasText( str ) && str.indexOf( "{" ) >= 0 && str.indexOf( "}" ) > 0 ) {
                return new PropertyProcessor().set( "format", new TemplateFormatter( str ) );
                
            } else {
                return new PropertyProcessor().set( "path", str );
            }

        } else if( value instanceof Map ) {
            Map map = (Map) value;
            PropertyProcessor processor = new PropertyProcessor();
            
            if( map.containsKey( "property" ) ) {
                processor.set( "path", map.get( "property" ) );
            }
            
            if( map.containsKey( "format" ) ) {
                processor.set( "format", map.get( "format" ) );
            }
            
            return processor;
        
        } else {
            return value;
        }
    }
    
}
