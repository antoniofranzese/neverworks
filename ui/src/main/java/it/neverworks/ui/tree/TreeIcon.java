package it.neverworks.ui.tree;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Getter;
import it.neverworks.model.converters.Convert;
import it.neverworks.ui.data.StyleArtifact;

@Convert( TreeIconConverter.class )
public class TreeIcon extends BaseModel {
    
    @Property
    private StyleArtifact opened;
    
    @Property
    private StyleArtifact closed;
    
    protected StyleArtifact getOpened( Getter<StyleArtifact> getter ) {
        if( getter.undefined() ) {
            return this.closed;
        } else {
            return getter.get();
        }
    }

    protected StyleArtifact getClosed( Getter<StyleArtifact> getter ) {
        if( getter.undefined() ) {
            return this.opened;
        } else {
            return getter.get();
        }
    }
    
    public StyleArtifact getClosed(){
        return this.closed;
    }
    
    public void setClosed( StyleArtifact closed ){
        this.closed = closed;
    }
    
    public StyleArtifact getOpened(){
        return this.opened;
    }
    
    public void setOpened( StyleArtifact opened ){
        this.opened = opened;
    }
    
}