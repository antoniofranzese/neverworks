package it.neverworks.ui.tree;

import it.neverworks.model.converters.Converter;
import it.neverworks.ui.types.Decorator;

public class RowProcessorConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof RowProcessor ) {
            return value;
        } else if( value instanceof Decorator ) {
            return new RowProcessor( (Decorator) value );
        } else {
            return value;
        }
    }
}