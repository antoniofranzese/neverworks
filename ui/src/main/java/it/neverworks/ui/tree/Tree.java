package it.neverworks.ui.tree;

import java.util.Map;
import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;
import it.neverworks.ui.BaseWidget;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.DataSourceKeyProvider;
import it.neverworks.ui.data.MissingItemException;
import it.neverworks.ui.data.TemplateFormatterConverter;
import it.neverworks.ui.dnd.ItemDropSource;
import it.neverworks.ui.dnd.Avatar;

public class Tree extends BaseWidget implements BorderLayoutCapable, DataSourceKeyProvider, ItemDropSource {

    public enum Encoding {
        TEXT, HTML
    }

    @Property @Child
    private DataSource items;
    
    @Property
    private String key = "id";
    
    @Property
    private PropertyProcessor caption = new PropertyProcessor().set( "path", "name" );
    
    @Property
    private String link = "parent";
    
    @Property
    private String icon;
    
    @Property
    private String proposal;
    
    @Property
    private Signal<ItemClickEvent> click;
    
    @Property
    private Signal<ItemClickEvent> inspect;
    
    @Property
    private Signal<ItemSelectEvent> select;
    
    @Property
    private Size width;
    
    @Property
    private Size height;
    
    @Property
    private Object stem;
    
    @Property @AutoConvert
    private Boolean showStem;
    
    @Property
    private LayoutInfo layout;
    
    @Property @AutoConvert
    private Boolean expanded;
    
    @Property @AutoConvert
    private Boolean disabled;
    
    @Property @Collection
    private Map<Object, TreeIcon> icons;
    
    @Property
    private Object selectedKey;
    
    @Property
    private Object selected;
    
    @Property
    private AvatarProcessor avatar;
    
    @Property
    private RowProcessor rows;
    
    @Property @AutoConvert
    private Encoding encoding = Encoding.TEXT; 
    
    @Property @Convert( TemplateFormatterConverter.class )
    private Formatter format;
    
    private boolean selecting = false;
    private boolean selectionChanged = false;
    
    public Tree() {
        super();
    }
    
    public Tree( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected void handleClick( Handler<ItemClickEvent> handler ) {
        set( "selectedKey", handler.event().getKey() );
        handler.handle();
    }
    
    protected void handleSelect( Handler<ItemSelectEvent> handler ) throws Exception {
        Object deselected = handler.event().get( "deselected" );
        selecting = true;
        selectionChanged = false;
        try {
            handler.handle();
        } catch( Exception ex ) {
            //TODO: recupero della selezione precedente, ma andrebbe evitata la selezione al livello javascript
            if( ! selectionChanged ) {
                raw( "selectedKey", getItems().get( deselected, getKey() ) );
                model().touch( "selectedKey" );
            }
            throw ex;
        } finally {
            selecting = false;
            selectionChanged = false;
        }
    }
    
    protected void setSelectedKey( Setter<Object> setter ) {
        Object previous = setter.raw();
        if( setter.value() == null ) {
            raw( "selected", getStem() );
            setter.set();
        } else {
            try {
                raw( "selected", getItems().load( getKey(), setter.value() ) );
                setter.set();
            } catch( MissingItemException ex ) {
                raw( "selected", null );
                setter.set( null );
                throw ex;
            }
        }
        
        if( selecting ) {
            selectionChanged = true;
        } else if( previous != null ? ! previous.equals( setter.raw() ) : setter.raw() != null ) {
            getSelect().fire( arg( "old", previous ).arg( "new", setter.raw() ) );
        }
    }
    
    protected void setSelected( Setter<Object> setter ) {
        set( "selectedKey", setter.value() != null ? getItems().get( setter.value(), getKey() ) : null );
        setter.decline();
    }
    
    public void select( Object key ) {
        set( "selectedKey", key );
    }

    protected void setKey( Setter<String> setter ) {
        setter.set();
        if( items != null && items.getDefaultKey() == null ) {
            items.setDefaultKey( setter.value() );
        }
    }
    
    protected void setItems( Setter<DataSource> setter ) {
        setter.set();
        try {
            raw( "selected", getItems().load( getKey(), get( "selectedKey" ) ) );
        } catch( MissingItemException ex ) {
            raw( "selectedKey", null );
            raw( "selected", null );
        }
    }
    
    public String getDataSourceKey() {
        return this.key;
    }
    
    public <T> T item( Object key ) {
        if( key == null ) {
            return (T) getStem();
        } else {
            return (T) getItems().load( getKey(), key );
        }
    }
    
    public Object getDropProposal( Object key ) {
        if( getItems() != null && Strings.hasText( getProposal() ) ) {
            return getItems().get( getDropItem( key ), getProposal() );
        } else {
            return null;
        }
    }

    public Object getDropItem( Object key ) {
        if( getItems() != null ) {
            return getItems().load( key );
        } else {
            return null;
        }
    }

    /* Virtual Properties */
    
    @Virtual
    public Boolean getEnabled(){
        return Boolean.FALSE.equals( getDisabled() );
    }
    
    public void setEnabled( Boolean enabled ){
        setDisabled( Boolean.FALSE.equals( enabled ) );
    }

    /* Bean Accessors */
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public DataSource getItems(){
        return this.items;
    }
    
    public void setItems( DataSource items ){
        this.items = items;
    }

    public String getLink(){
        return this.link;
    }
    
    public void setLink( String link ){
        this.link = link;
    }
    
    public PropertyProcessor getCaption(){
        return this.caption;
    }
    
    public void setCaption( PropertyProcessor caption ){
        this.caption = caption;
    }
    
    public String getKey(){
        return this.key;
    }
    
    public void setKey( String key ){
        this.key = key;
    }
    
    public Signal<ItemClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<ItemClickEvent> click ){
        this.click = click;
    }

    public Signal<ItemClickEvent> getInspect(){
        return this.inspect;
    }
    
    public void setInspect( Signal<ItemClickEvent> inspect ){
        this.inspect = inspect;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Object getStem(){
        return this.stem;
    }
    
    public void setStem( Object stem ){
        this.stem = stem;
    }
    
    public Boolean getShowStem(){
        return this.showStem;
    }
    
    public void setShowStem( Boolean showStem ){
        this.showStem = showStem;
    }
    
    public Boolean getExpanded(){
        return this.expanded;
    }
    
    public void setExpanded( Boolean expanded ){
        this.expanded = expanded;
    }
    
    public String getIcon(){
        return this.icon;
    }
    
    public void setIcon( String icon ){
        this.icon = icon;
    }
    
    public Object getSelected(){
        return this.selected;
    }
    
    public void setSelected( Object selected ){
        this.selected = selected;
    }
    
    public Object getSelectedKey(){
        return this.selectedKey;
    }
    
    public void setSelectedKey( Object selectedKey ){
        this.selectedKey = selectedKey;
    }
    
    public Signal<ItemSelectEvent> getSelect(){
        return this.select;
    }
    
    public void setSelect( Signal<ItemSelectEvent> select ){
        this.select = select;
    }

    public Map<Object, TreeIcon> getIcons(){
        return this.icons;
    }
    
    public void setIcons( Map<Object, TreeIcon> icons ){
        this.icons = icons;
    }
    
    public String getProposal(){
        return this.proposal;
    }
    
    public void setProposal( String proposal ){
        this.proposal = proposal;
    }
    
    public AvatarProcessor getAvatar(){
        return this.avatar;
    }
    
    public void setAvatar( AvatarProcessor avatar ){
        this.avatar = avatar;
    }
    
    public Boolean getDisabled(){
        return this.disabled;
    }
    
    public void setDisabled( Boolean disabled ){
        this.disabled = disabled;
    }
    
    public RowProcessor getRows(){
        return this.rows;
    }
    
    public void setRows( RowProcessor rows ){
        this.rows = rows;
    }
    
    public Formatter getFormat(){
        return this.format;
    }
    
    public void setFormat( Formatter format ){
        this.format = format;
    }
    
    public Encoding getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( Encoding encoding ){
        this.encoding = encoding;
    }
    
}