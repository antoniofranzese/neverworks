package it.neverworks.ui.tree;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.types.Point;

public class ItemClickEvent<T extends Tree> extends UIEvent<T> {

    public enum Button {
        LEFT, RIGHT
    }
    
    @Property
    private Object key;
    
    @Property @AutoConvert
    private Button button;

    @Property @AutoConvert
    private int x;
    
    @Property @AutoConvert
    private int y;
    
    public <X> X getItem() {
        return getSource().item( key );
    }

    public Point getOrigin() {
        return new Point( this.x, this.y );
    }
    
    /* Bean Accessors */
    
    public Object getKey(){
        return this.key;
    }
    
    public void setKey( Object key ){
        this.key = key;
    }

    public Button getButton(){
        return this.button;
    }
    
    public void setButton( Button button ){
        this.button = button;
    }
    
    public int getY(){
        return this.y;
    }
    
    public void setY( int y ){
        this.y = y;
    }
    
    public int getX(){
        return this.x;
    }
    
    public void setX( int x ){
        this.x = x;
    }
        
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "x" ).add( "y" ).toString();
    }
}