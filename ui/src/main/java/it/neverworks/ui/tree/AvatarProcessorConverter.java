package it.neverworks.ui.tree;

import java.util.Map;
import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.ui.data.TemplateFormatter;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.dnd.Avatar;

public class AvatarProcessorConverter implements Converter {

    public Object convert( Object value ) {
        if( value instanceof AvatarProcessor ) {
            return value;

        } else if( value instanceof Avatar ) {
            return new AvatarProcessor().set( "default", value );

        } else if( value instanceof String ) {
            String str = (String) value;
            
            if( Strings.hasText( str ) && str.indexOf( "{" ) >= 0 && str.indexOf( "}" ) > 0 ) {
                return new AvatarProcessor().set( "format", new TemplateFormatter( str ) );
                
            } else {
                return new AvatarProcessor().set( "path", str );
            }

        } else if( value instanceof Map ) {
            Map map = (Map) value;
            AvatarProcessor processor = new AvatarProcessor();
        
            if( map.containsKey( "property" ) ) {
                processor.set( "path", map.get( "property" ) );
            }

            if( map.containsKey( "format" ) ) {
                processor.set( "format", map.get( "format" ) );
            }
        
            if( map.containsKey( "default" ) ) {
                processor.set( "default", map.get( "default" ) );
            }
        
            return processor;
        } else {
            return value;
        }
    }
    
}
