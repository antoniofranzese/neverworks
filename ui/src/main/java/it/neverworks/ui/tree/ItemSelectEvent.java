package it.neverworks.ui.tree;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.lifecycle.UIEvent;

public class ItemSelectEvent<T> extends UIEvent<T> {
    
    protected Object old;
    protected Object new_;
    
    protected Object oldItem;
    protected Object newItem;

    @Property @AutoConvert
    private Integer index;
    
    public Integer getIndex(){
        return this.index;
    }
    
    public void setIndex( Integer index ){
        this.index = index;
    }
    
    public Object getNew(){
        return this.new_;
    }
    
    public void setNew( Object new_ ){
        this.new_ = new_;
    }
    
    public Object getOld(){
        return this.old;
    }
    
    public void setOld( Object old ){
        this.old = old;
    }
    
    public Object getOldItem() {
        if( oldItem == null ) {
            oldItem = this.old != null ? ((Tree) this.source).item( this.old ) : null;
        }
        return oldItem;
    }
    
    public Object getNewItem() {
        if( newItem == null ) {
            newItem = this.new_ != null ? ((Tree) this.source).item( this.new_ ) : null;
        }
        return newItem;
    }
    
    public Object selected() {
        return getNewItem();
    }
    
    public Object getSelected() {
        return getNewItem();
    }
    
    public Object deselected() {
        return getOldItem();
    }
    
    public Object getDeselected() {
        return getOldItem();
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "old" ).add( "new" ).toString();
    }
    
}