package it.neverworks.ui.tree;

import java.util.Map;
import java.util.List;
import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;
import it.neverworks.ui.data.StyleArtifact;

public class TreeIconConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof TreeIcon ) {
            return value;

        } else if( value instanceof StyleArtifact ) {
            return new TreeIcon().set( "opened", value ).set( "closed", value );

        } else if( value instanceof Map ) {
            Map map = ((Map) value);
            if( map.containsKey( "opened" ) || map.containsKey( "closed" ) ) {
                TreeIcon icon = new TreeIcon();
                if( map.containsKey( "opened" ) ) {
                    icon.set( "opened", map.get( "opened" ) );
                }
                if( map.containsKey( "closed" ) ) {
                    icon.set( "closed", map.get( "closed" ) );
                }
                return icon;
            } else {
                return value;
            }

        } else if( Collections.isListable( value ) ) {

            List list = Collections.list( value );

            if( list.size() > 0 ) {
                TreeIcon icon = new TreeIcon();
                icon.set( "opened", list.get( 0 ) );
                if( list.size() > 1 ) {
                    icon.set( "closed", list.get( 1 ) );
                }
                return icon;
            } else {
                return value;
            }
            
        } else {
            return value;
        }
    }
}