package it.neverworks.ui.tree;

import it.neverworks.model.converters.Convert;
import it.neverworks.ui.types.Decorator;
import it.neverworks.ui.types.Decoration;

@Convert( RowProcessorConverter.class )
public class RowProcessor {
    
    private Decorator decorator = null;
    
    public RowProcessor( Decorator decorator ) {
        this.decorator = decorator;
    }
    
    public Decoration decorate( Object item ) {
        return decorator != null ? decorator.decorate( item ) : null;
    }
    
}