package it.neverworks.ui.data;

import it.neverworks.io.FileInfo;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.model.utils.ToStringBuilder;

public class FileInfoArtifact implements FileArtifact, FileInfoProvider {
    
    private FileInfo info;

    public FileInfoArtifact( FileInfo info ) {
        this.info = info;
    }

    public FileInfo getInfo(){
        return this.info;
    }
    
    public String getName() {
        return this.info.getBadge().getToken();
    }
    
    public FileInfo retrieveFileInfo() {
        return this.info;
    }

    public String toString() {
        return new ToStringBuilder( this ).add( "info" ).toString();
    }
}