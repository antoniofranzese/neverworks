package it.neverworks.ui.data;

import java.util.Locale;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Objects;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormat;

public class TextFormatter implements ValueFormatter {
    
    private TextFormat format;
    private boolean lenient;
    private Locale locale;
    
    public TextFormatter( String pattern ) {
        this( Translation.current(), pattern );
    }

    public TextFormatter( Locale locale, String pattern ) {
        this( new TextFormat( pattern ) );
        this.locale = locale;
    }
    
    public TextFormatter( Arguments args ) {
        this( args.<Locale>get( "locale", Translation.current() ), args.<String>get( "format", null ) );
        this.lenient = args.<Boolean>get( "lenient", false );
    }
    
    public TextFormatter( TextFormat format ) {
        if( format != null ) {
            this.format = format;
        } else {
            throw new IllegalArgumentException( "Null format" );
        }
    }
    
    public Object format( Object value ) {
        if( value != null ) {
            try {
                return this.format.apply( this.locale, value );
            } catch( Exception ex ) {
                if( lenient ) {
                    return Strings.valueOf( value );
                } else {
                    throw new FormatterException( "TextFormatter: Cannot format " + Objects.className( value ), ex );
                }
            }
        } else {
            return null;
        }
    }
    
}