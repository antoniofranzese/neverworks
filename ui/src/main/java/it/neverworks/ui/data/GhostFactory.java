package it.neverworks.ui.data;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;

import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Reflection;

public class GhostFactory {

    private static Map<Class, Class> enhancers = new HashMap<Class, Class>();
    
    public static Object create( Class target ) {
        if( ! enhancers.containsKey( target ) ) {
            synchronized( enhancers ) {
                if( ! enhancers.containsKey( target ) ) {
                    Enhancer enhancer = new Enhancer();
                    enhancer.setSuperclass( target );
                    enhancer.setInterfaces( new Class[]{ Ghost.class } );
                    enhancer.setCallbackType( NoOp.class );
                    enhancers.put( target, enhancer.createClass() );
                }
            }
        }

        Class ghostClass = enhancers.get( target );
        return Reflection.newInstance( ghostClass );
    }

}