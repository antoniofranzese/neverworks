package it.neverworks.ui.data;

public class MissingItemException extends DataSourceException {
    
    public MissingItemException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public MissingItemException( String message ){
        super( message );
    }
}