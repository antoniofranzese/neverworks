package it.neverworks.ui.data;

public interface Artifact {
    String getName();
}