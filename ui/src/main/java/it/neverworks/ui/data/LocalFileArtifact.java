package it.neverworks.ui.data;

import java.io.File;

public class LocalFileArtifact implements FileArtifact {
    
    private File file;

    public LocalFileArtifact( File file ) {
        this.file = file;
    }

    public File getFile(){
        return this.file;
    }
    
    public String getName() {
        return this.file != null ? this.file.getName() : null;
    }
    
}