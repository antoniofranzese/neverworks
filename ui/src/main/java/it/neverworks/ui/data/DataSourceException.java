package it.neverworks.ui.data;

public class DataSourceException extends RuntimeException {
    
    public DataSourceException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public DataSourceException( String message ){
        super( message );
    }
}