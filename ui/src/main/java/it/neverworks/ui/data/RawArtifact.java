package it.neverworks.ui.data;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class RawArtifact extends BaseModel implements Artifact {

    @Property
    protected Object content;

    public RawArtifact() {

    }

    public RawArtifact( Object content ) {
        this();
        set( "content", content );
    }
    
    public Object getContent(){
        return this.content;
    }
    
    public void setContent( Object content ){
        this.content = content;
    }
    
    public String getName() {
        return this.getClass().getSimpleName() + ": " + Objects.repr( this.content );
    }
    
    public String toString() {
        return Strings.valueOf( this.content );
    }

}