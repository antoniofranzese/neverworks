package it.neverworks.ui.data;

public class FormatterException extends RuntimeException {
    
    public FormatterException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public FormatterException( String message ){
        super( message );
    }
}