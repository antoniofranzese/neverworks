package it.neverworks.ui.data.export;

import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.util.Date;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.io.Streams;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.io.StringWriter;
import it.neverworks.io.CloseableGuard;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.features.Text;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.data.Formatter;

public class CSVWriter extends AbstractWriter implements Text, CloseableGuard<CSVWriter> {
    
    private Object nullReplacement = "";
    private String newLineReplacement = " ";
    private String quoteReplacement = "\"\"";

    private String lineEndValue = "\n";
    private String separatorValue = ","; 
    private String quoteValue = "\""; 

    private boolean newLine = true;
    
    private Writer writer;
    
    public CSVWriter() {
        this( new StringWriter() );
    }

    public CSVWriter( File file ) {
        try {
            this.writer = new FileWriter( file );
            this.file = file;
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public CSVWriter( Writer writer ) {
        this.writer = writer;
    }
    
    public CSVWriter( OutputStream stream ) {
        this.writer = new OutputStreamWriter( stream );
    }
    
    public CSVWriter( FileInfo info ) {
        this( info.openWriter() );
        this.setFileInfo( info );
    }
    
    public String getMimeType() {
        return "text/csv";
    }

    public CSVWriter set( String name, Object value ) {
        return this.<CSVWriter>_set( name, value );
    }

    public CSVWriter set( Arguments arguments ) {
        return this.<CSVWriter>_set( arguments );
    }

    public CSVWriter format( Class cls, Formatter formatter ) {
        formatters.put( cls, formatter );
        return this;
    }
    
    public CSVWriter write( Object value ) {
        return write( value, new Arguments() );
    }

    public CSVWriter write( Object value, Formatter formatter ) {
        return write( value, arg( "format", formatter ) );
    }
    
    public CSVWriter write( Object value, Arguments arguments ) {
        arguments.restrict( "format" );
        Formatter format = arguments.get( FormatterType, "format" );

        if( value == null ) {
            return write( this.nullReplacement, format );

        } else {
    
            String field = Strings.valueOf( formatValue( value, format ) );
            
            boolean quote = false;
            if( Strings.hasText( field ) ) {
                if( field.indexOf( " " ) >= 0 
                    || field.indexOf( "\n" ) >= 0 
                    || field.indexOf( separatorValue ) >= 0 ) {

                    quote = true;
                }
            
                if( quoteValue != null ) {
                    if( field.indexOf( quoteValue ) >= 0 ) {
                        quote = true;
                    } 
                    field = field.replaceAll( quoteValue, quoteReplacement );
                }

                if( newLineReplacement != null ) {
                    if( field.indexOf( "\n" ) >= 0
                        || field.indexOf( "\r" ) >= 0 ) {
                        quote = true;
                    } 

                    field = field
                        .replaceAll( "\n\r", newLineReplacement )
                        .replaceAll( "\n", newLineReplacement )
                        .replaceAll( "\r", newLineReplacement );
                }
            } else {
                quote = true;
            }

            try {

                if( ! newLine ) {
                    writer.write( separatorValue );
                } else {
                    newLine = false;
                }

                writer.write( ( quote ? quoteValue : "" ) + field + ( quote ? quoteValue : "" ) );
                
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
            
        }
        
        return this;
        
    }
    
    public CSVWriter end() {
        if( ! newLine ) {
            try {
                writer.write( lineEndValue );
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
            newLine = true;
        }
        return this;
    }

    public CSVWriter flush() {
        end();
        try {
            writer.flush();
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
        return this;
    }
    
    public CSVWriter complete() {
        try {
            close();
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
        return this;
    }
    
    protected void doClose() throws IOException {
        flush();
        writer.close();
    }
    
    public String toString() {
        if( writer instanceof StringWriter ) {
            flush();
            return ((StringWriter) writer).toString();
        } else {
            return this.getClass().getSimpleName() + "(writer=" + ( writer != null ? writer.getClass().getName() : "null" ) + ")";
        }
    }
    
    public InputStream asStream() {
        if( writer instanceof StringWriter ) {
            return Streams.asStream( toString() );
        } else {
            return super.asStream();
        }
    }
    
    public String asText() {
        return toString();
    }
    
    public void setLineEnd( String lineEnd ){
        this.lineEndValue = lineEnd;
    }
    
    public void setQuote( String quote ){
        this.quoteValue = quote;
    }
    
    public void setSeparator( String separator ){
        this.separatorValue = separator;
    }
    
    public void setNulls( Object nullReplacement ){
        this.nullReplacement = nullReplacement;
    }

    public void setNewLines( String newLineReplacement ) {
        this.newLineReplacement = newLineReplacement;
    }
    
    public void setQuotes( String quoteReplacement ){
        this.quoteReplacement = quoteReplacement;
    }
    
}