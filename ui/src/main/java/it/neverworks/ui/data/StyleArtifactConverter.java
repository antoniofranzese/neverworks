package it.neverworks.ui.data;

import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;

public class StyleArtifactConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof StyleArtifact ) {
            return value;
        } else if( Collections.isListable( value ) ) {
            CompositeStyleArtifact composite = new CompositeStyleArtifact();
            for( Object item: Collections.list( value ) ) {
                Object converted = convert( item );
                if( converted instanceof StyleArtifact ) {
                    composite.add( (StyleArtifact) converted );
                } else {
                    return value;
                }
            }
            return composite;
        } else if( value != null && !( value instanceof Artifact ) ) {
            return new RawStyleArtifact().set( "content", value );
        } else {
            return value;
        }
    }
}