package it.neverworks.ui.data.export;

import static it.neverworks.language.*;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.io.File;

import java.util.Stack;
import java.util.Map;
import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;

import it.neverworks.lang.Arguments;
import it.neverworks.io.Streams;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.io.StringWriter;
import it.neverworks.encoding.XML;
import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.model.features.Text;
import it.neverworks.ui.data.Formatter;

public abstract class AbstractXMLWriter extends AbstractWriter implements Text, JSONEncodable {
    
    protected Writer writer;
    protected Stack<Element> stack = new Stack<Element>();
    protected String indent = "\t";
    protected boolean initialized = false;
    
    protected class Element {
        protected String name;
        protected Arguments attributes = new Arguments();
        protected boolean started = false;
        protected boolean streaming = false;
        protected int level = 0;
        protected Element parent;
        protected boolean indent = false;
        
        public Element( String name ){
            this.name = name;
        }

        public void start() {
            start( false, true );
        }

        public void start( boolean collapsed ) {
            start( collapsed, true );
        }
        
        public void start( boolean collapsed, boolean complete ) {
            if( ! this.streaming ) {
                if( level > 0 ) {
                    AbstractXMLWriter.this.write( "\n" );
                    if( parent != null ) {
                        parent.indent = true;
                    }

                    for( int i = 0; i < level; i++ ) {
                        AbstractXMLWriter.this.write( AbstractXMLWriter.this.indent );
                    }
                }


                AbstractXMLWriter.this.write( "<" + name );
 
            }
 
            if( complete ) {
                started = true;
                if( attributes.size() > 0 ) {
                    for( String key: attributes.keys() ) {
                        AbstractXMLWriter.this.write( " " + key + "=\"" + attributes.get( key ) + "\"" );
                    }
                }

                AbstractXMLWriter.this.write( ( collapsed ? "/" : "" ) + ">" );
            }
        }
        
        public void stream() {
            if( ! this.streaming ) {
                start( false, false );
                this.streaming = true;
            }
        }
        
        public void end() {
            if( ! started ) {
                start( true, true );
            } else {
                if( indent ) {
                    AbstractXMLWriter.this.write( "\n" );
                    if( level > 0 ) {
                        for( int i = 0; i < level; i++ ) {
                            AbstractXMLWriter.this.write( AbstractXMLWriter.this.indent );
                        }
                    }
                }
                AbstractXMLWriter.this.write( "</" + name + ">" );
            }
        }
    }
    

    protected void startElement( String name ) {
        if( ! initialized ) {
            initialized = true;
        }
        if( stack.size() > 0 && ! stack.peek().started ) {
            stack.peek().start();
        }
        Element element = new Element( name );
        element.parent = stack.size() > 0 ? stack.peek() : null;
        stack.push( element );
    }
    
    protected void indentCurrentElement() {
        if( stack.size() > 0 ) {
            if( ! stack.peek().started ) {
                stack.peek().level = stack.size() - 1;
            } else {
                throw new IllegalStateException( "Cannot indent, element '" + stack.peek().name + " has been written" );
            }
        }
    }
    
    protected void addAttribute( String name, Object value, Formatter formatter ) {
        if( stack.size() > 0 ) {
            if( ! stack.peek().started ) {
                Object formatted = formatValue( value, formatter );
                if( Strings.hasText( formatted ) ) {
                    stack.peek().attributes.arg( name, XML.encode( formatted ) );
                }
            } else {
                throw new IllegalStateException( "Cannot set attribute, element '" + stack.peek().name + " has been written" );
            }
        } else {
            throw new IllegalStateException( "No current element" );
        }
    }
    
    
    protected void startStreaming() {
        if( stack.size() > 0 ) {
            if( ! stack.peek().started ) {
                stack.peek().stream();
            } else {
                throw new IllegalStateException( "Cannot start streaming, element '" + stack.peek().name + " has been written" );
            }
        } else {
            throw new IllegalStateException( "No current element" );
        }
    }
    
    protected void startCurrentElement() {
        if( stack.size() > 0 ) {
            if( ! stack.peek().started ) {
                stack.peek().start();
            }
        }
    }

    protected void write( Object value ) {
        try {
            this.writer.write( Strings.valueOf( value ) );
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    protected void endElement( String name ) {
        if( stack.size() > 0 ) {
            Element last = stack.pop();
            if( last.name.equals( name ) ) {
                last.end();
            } else {
                throw new IllegalStateException( "Unexpected element end, '" + name + "' requested, '" + last.name + "' current" );
            }
        } else {
            throw new IllegalStateException( "No current element" );
        }
    }

    protected void endElement() {
        if( stack.size() > 0 ) {
            stack.pop().end();
        } else {
            throw new IllegalStateException( "No current element" );
        }
    }
    
    public void flush() {
        try {
            this.writer.flush();
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    protected void endAll() {
        while( stack.size() > 0 ) {
            endElement();
        }
    }
    
    protected void doClose() throws IOException {
        endAll();
        flush();
        this.writer.close();
    }
    
    public String toString() {
        if( writer instanceof StringWriter ) {
            endAll();
            flush();
            return ((StringWriter) this.writer).toString();
        } else {
            return this.getClass().getSimpleName() + "(writer=" + ( writer != null ? writer.getClass().getName() : "null" ) + ")";
        }
    }

    public InputStream asStream() {
        if( writer instanceof StringWriter ) {
            return Streams.asStream( toString() );
        } else {
            return super.asStream();
        }
    }

    public String asText() {
        return this.toString();
    }

    public JsonNode toJSON() {
        return new TextNode( this.toString() );
    }

}
