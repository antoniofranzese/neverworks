package it.neverworks.ui.data;

import it.neverworks.io.FileBadgeProvider;
import it.neverworks.io.FileBadge;
import it.neverworks.model.utils.ToStringBuilder;

public class FileBadgeArtifact implements FileArtifact, FileBadgeProvider {
    
    private FileBadge badge;

    public FileBadgeArtifact( FileBadge badge ) {
        this.badge = badge;
    }

    public FileBadge getBadge(){
        return this.badge;
    }
    
    public String getName() {
        return this.badge.getToken();
    }
    
    public FileBadge retrieveFileBadge() {
        return getBadge();
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "badge" ).toString();
    }
}