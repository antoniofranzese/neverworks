package it.neverworks.ui.data;

public interface IndexedDataSource {
    
    public int indexOf( String property, Object key );
    public int indexOf( Object key );
    public <T> T pick( int index );
    
}