package it.neverworks.ui.data;

import java.io.InputStream;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.InputStreamProvider;

public class StreamArtifact implements FileArtifact, FileInfoProvider, InputStreamProvider {
    
    private InputStream stream;
    private String name;
    private FileInfo file;

    public StreamArtifact( InputStream stream ) {
        this( stream, "stream" );
    }

    public StreamArtifact( InputStream stream, String name ) {
        this.stream = stream;
        this.name = name;
    }
    
    public void replace( FileInfo file ){
        this.file = file;
        this.stream = null;
    }
    
    public FileInfo retrieveFileInfo(){
        return this.file;
    }
    
    public InputStream toStream() {
        if( this.stream != null ) {
            return this.stream;
        } else if( this.file != null ) {
            return this.file.getStream();
        } else {
            return null;
        }
    }
    
    public InputStream getStream(){
        return this.stream;
    }
    
    public String getName() {
        return this.name;
    }

}