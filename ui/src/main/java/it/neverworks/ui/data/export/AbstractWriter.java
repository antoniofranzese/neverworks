package it.neverworks.ui.data.export;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.File;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.data.TemplateFormatterConverter;
import it.neverworks.ui.data.Formatter;

public abstract class AbstractWriter extends AbstractModel implements FileInfoProvider, Closeable {

    protected final static TypeDefinition<Formatter> FormatterType = TypeFactory.build( Formatter.class, new TemplateFormatterConverter() );
    protected final static TypeDefinition<Formatter> BooleanType = TypeFactory.auto( Boolean.class );
    
    protected Map<Class, Formatter> formatters = new HashMap<Class, Formatter>();
    protected FileInfo fileInfo;
    protected boolean closed;
    protected File file;
    
    protected Object formatValue( Object value, Formatter formatter ) {
        if( value != null ) {
            if( formatter != null ) {
                return formatter.format( value );
        
            } else if( formatters.containsKey( value.getClass() ) ) {
                return formatters.get( value.getClass() ).format( value );

            } else {
                return value;
            }
        } else {
            return null;
        }
    }

    public InputStream asStream() {
        if( this.fileInfo != null ) {
            return this.fileInfo.get( "stream" );
        } else if( this.file != null ) {
            try {
                return new FileInputStream( this.file );
            } catch( FileNotFoundException ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            throw new IllegalStateException( "Cannot return stream" );
        }
    }
    
    public FileInfo retrieveFileInfo() {
        if( this.fileInfo != null ) {
            try {
                close();
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
            return this.fileInfo;
        } else {
            return new FileInfo(
                arg( "name", this.getClass().getSimpleName() )
                .arg( "type", this.getMimeType() )
                .arg( "stream", this.asStream() )
            );
        }
    }
    
    protected void setFileInfo( FileInfo info ) {
        this.fileInfo = info;
        if( info != null ) {
            if( info.getType() == null ) {
                info.setType( this.getMimeType() );
            }
        }
    }

    public abstract String getMimeType();
    
    public void close() throws IOException {
        if( ! this.closed ) {
            this.closed = true;
            this.doClose();
        }
    }

    protected abstract void doClose() throws IOException;
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( name );
    }

    protected <T extends AbstractWriter> T _set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }

    public <T extends AbstractWriter> T _set( Arguments arguments ) {
        for( String name: arguments.keys() ) {
            this._set( name, arguments.get( name ) );
        }
        return (T)this;
    }
    
    @Virtual
    public void setDates( Formatter formatter ){
        formatters.put( Date.class, formatter );
    }

    @Virtual
    public void setIntegers( Formatter formatter ){
        formatters.put( Integer.class, formatter );
    }

    @Virtual
    public void setLongs( Formatter formatter ){
        formatters.put( Long.class, formatter );
    }

    @Virtual
    public void setFloats( Formatter formatter ){
        formatters.put( Float.class, formatter );
    }

    @Virtual
    public void setDoubles( Formatter formatter ){
        formatters.put( Double.class, formatter );
    }
    
    @Virtual
    public void setBooleans( Formatter formatter ){
        formatters.put( Boolean.class, formatter );
    }


}