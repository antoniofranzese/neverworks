package it.neverworks.ui.data.export;

import static it.neverworks.language.*;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.io.File;

import java.util.Stack;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.codec.binary.Base64OutputStream;

import static it.neverworks.language.*;
import it.neverworks.io.Streams;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.io.StringWriter;
import it.neverworks.io.CloseableGuard;
import it.neverworks.io.WriterOutputStream;
import it.neverworks.encoding.MIME;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.XML;
import it.neverworks.model.Value;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.InputStreamProvider;
import it.neverworks.io.TypedInputStreamProvider;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Color;

public class XHTMLWriter extends AbstractXMLWriter implements CloseableGuard<XHTMLWriter> {
    
    private enum DocumentMode {
        HTML, CSS
    }

    protected Rule currentRule = null;
    protected DocumentMode mode;
    
    protected class Rule {
        private String rule;
        private Arguments styles = new Arguments();
        
        public Rule( String rule ) {
            this.rule = rule;
        }
        
        public void flush() {
            XHTMLWriter.this.startCurrentElement();
            int level = XHTMLWriter.this.stack.size() > 0 ? XHTMLWriter.this.stack.peek().level : 0;
            String indent = "";
            if( level > 0 ) {
                level += 1;
                XHTMLWriter.this.stack.peek().indent = true;
                
                for( int i = 0; i < level; i ++ ) {
                    indent += XHTMLWriter.this.indent;
                }
                
                XHTMLWriter.this.write( "\n" );
            }
            
            XHTMLWriter.this.write( indent + rule + " {" );
            
            if( level > 0 ) {
                XHTMLWriter.this.write( "\n" );
            }    
                
            for( String name: styles.names() ) {
                XHTMLWriter.this.write( indent );

                if( level > 0 ) {
                    XHTMLWriter.this.write( XHTMLWriter.this.indent );
                }    
                
                XHTMLWriter.this.write( name + ": " + styles.get( name ) + "; " );

                if( level > 0 ) {
                    XHTMLWriter.this.write( "\n" );
                }    

            }
            
            XHTMLWriter.this.write( indent + "} " );
            
            if( level > 0 ) {
                XHTMLWriter.this.write( "\n" );
            }    
        }
    }
    
    public XHTMLWriter() {
        this( new StringWriter() );
    }
    
    public XHTMLWriter( File file ) {
        try {
            this.writer = new FileWriter( file );
            this.file = file;
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public XHTMLWriter( OutputStream stream ) {
        this.writer = new OutputStreamWriter( stream );
    }
    
    public XHTMLWriter( Writer writer ) {
        super();
        this.writer = writer;
    }
    
    public XHTMLWriter( FileInfo info ) {
        this( info.openWriter() );
        this.setFileInfo( info );
    }
    
    public String getMimeType() {
        return "text/html";
    }

    public XHTMLWriter set( String name, Object value ) {
        return this.<XHTMLWriter>_set( name, value );
    }

    public XHTMLWriter set( Arguments arguments ) {
        return this.<XHTMLWriter>_set( arguments );
    }
 
    public XHTMLWriter format( Class cls, Formatter formatter ) {
        formatters.put( cls, formatter );
        return this;
    }

    public XHTMLWriter init() {
        return init( "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" );
    }
    
    public XHTMLWriter init( String declaration ) {
        if( ! initialized ) {
            write( declaration );
            initialized = true;
        }
        return this;
    }

    public XHTMLWriter start( String name ) {
        if( this.mode == null ) {
            this.mode = DocumentMode.HTML;
        }

        if( this.mode == DocumentMode.HTML ) {
            closeStyles();
            startElement( name );
            return this;
        } else {
            throw new IllegalStateException( "Cannot start <" + name + ">, document mode is " + this.mode );
        }
    }
    
    public XHTMLWriter in() {
        indentCurrentElement();
        return this;
    }
    
    public XHTMLWriter attr( String name, Object value ) {
        return attr( name, value, null );
    }

    public XHTMLWriter attr( String name, Object value, Formatter formatter ) {
        if( value != null ) {
            addAttribute( name, value, formatter );
        }
        return this;
    }
    
    public XHTMLWriter attr( String name, InputStream input ) {
        return input != null ? attr( name, input, MIME.detect( input ) ) : this;
    }
    
    public XHTMLWriter attr( String name, InputStream input, String mimeType ) {
        if( input != null ) {
            startStreaming();
            write( " " );
            write( name );
            write( "=\"data:" );
            write( mimeType );
            write( ";base64," );
        
            Streams.copy( input, new Base64OutputStream( new WriterOutputStream( this.writer ), /* doEncode */ true, /* lineLength */ -1, /* lineSeparator */ null ) );

            write( "\"" );
        }
        return this;
    }

    public XHTMLWriter attr( String name, File input ) {
        return input != null ? attr( name, input, (String) null ) : this;
    }
    
    public XHTMLWriter attr( String name, File input, String mimeType ) {
        return input != null ? attr( name, Streams.asStream( input ), mimeType != null ? mimeType : MIME.detect( input ) ) : this;
    }

    public XHTMLWriter attr( String name, FileInfo input ) {
        return input != null ? attr( name, input, (String) null ) : this;
    }

    public XHTMLWriter attr( String name, FileInfo input, String mimeType ) {
        return input != null ? attr( name, input.getStream(), mimeType != null ? mimeType : input.get( "type", MIME.detect( input.<String>get( "name" ) ) ) ) : this;
    }

    public XHTMLWriter attr( String name, FileInfoProvider input ) {
        return input != null ? attr( name, input.retrieveFileInfo(), (String) null ) : this;
    }

    public XHTMLWriter attr( String name, FileInfoProvider input, String mimeType ) {
        return input != null ? attr( name, input.retrieveFileInfo(), mimeType ) : this;
    }

    public XHTMLWriter attr( String name, TypedInputStreamProvider input ) {
        return input != null ? attr( name, input.toStream(), input.getMimeType() ) : this;
    }
    
    public XHTMLWriter attr( String name, InputStreamProvider input, String mimeType ) {
        return input != null ? attr( name, input.toStream(), mimeType ) : this;
    }

    public XHTMLWriter attr( String name, Value input ) {
        return input != null ? attr( name, input, (String) null ) : this;
    }
    
    public XHTMLWriter attr( String name, Value input, String mimeType ) {
        if( input != null ) {
            if( input.is( File.class ) ) {
                return attr( name, input.as( File.class ), mimeType ); 
            } else if( input.is( FileInfoProvider.class ) ) {
                return attr( name, input.as( FileInfoProvider.class ), mimeType );
            } else if( input.is( FileInfo.class ) ) {
                return attr( name, input.as( FileInfo.class ), mimeType );
            } else if( input.is( InputStreamProvider.class ) ) {
                return attr( name, input.as( InputStreamProvider.class ), mimeType ); 
            } else {
                return attr( name, input.as( Object.class ) ); 
            } 
        } else {
            return this;
        }
    }

    public XHTMLWriter text( Object value ) {
        return text( value, null );
    } 

    public XHTMLWriter text( Object value, Formatter formatter ) {
        startCurrentElement();
        write( Strings.safe( XML.encode( formatValue( value, formatter ) ) ).replace( "\n", "<br/>" ) );
        return this;
    } 

    public XHTMLWriter raw( Object value ) {
        return raw( value, null );
    } 

    public XHTMLWriter raw( Object value, Formatter formatter ) {
        flushStyles();
        startCurrentElement();
        write( formatValue( value, formatter ) );
        return this;
    } 

    public XHTMLWriter end( String name ) {
        flushStyles();
        endElement( name );
        return this;
    }

    public XHTMLWriter end() {
        flushStyles();
        endElement();
        return this;
    }

    protected void endAll() {
        closeStyles();
        super.endAll();
    }

    public XHTMLWriter rule( String rule ) {
        if( this.mode == null ) {
            this.mode = DocumentMode.CSS;
        }

        if( this.mode == DocumentMode.CSS ) {
            if( currentRule != null ) {
                currentRule.flush();
            }
            currentRule = new Rule( rule );

        } else if( stack.size() > 0 ) {
            if( ! "style".equalsIgnoreCase( stack.peek().name ) ) {
                start( "style" ).attr( "type", "text/css" );
            }

            if( currentRule != null ) {
                currentRule.flush();
            }

            currentRule = new Rule( rule );

        } else {
            throw new IllegalStateException( "No current element" );
        }
        return this;
    }
    
    public XHTMLWriter style( Arguments arguments ) {
        if( arguments != null ) {
            for( String key: arguments.keys() ) {
                style( key, str( arguments.get( key ) ) );
            }
        }
        return this;
    }

    public XHTMLWriter style( String name, String value ) {
        if( Strings.hasText( name ) && Strings.hasText( value ) ) {
            if( currentRule != null ) {
                currentRule.styles.arg( name, value );
        
            } else if( stack.size() > 0 ) {
                Element currentElement = stack.peek();

                if( ! currentElement.started ) {
                    if( currentElement.attributes.containsKey( "style" ) ) { 
                        String currentStyle = currentElement.attributes.<String>get( "style" ).trim();
                        currentElement.attributes.put( "style", 
                            currentStyle 
                            + ( currentStyle.endsWith( ";" ) ? "" : ";" )
                            + " " + Strings.camel2hyphen( name ) + ": " + value + ";" 
                        );
                
                    } else {
                        currentElement.attributes.put( "style", Strings.camel2hyphen( name ) + ": " + value + ";" );
                    }
                } else {
                    throw new IllegalStateException( "Cannot add style, element '" + currentElement.name + " is already started" );
                }
        
            } else {
                throw new IllegalStateException( "No current rule or element" );
            }
        }
        return this;
    }
    
    public XHTMLWriter style( String name ) {
        if( Strings.hasText( name ) ) {
            if( currentRule != null ) {
                throw new IllegalStateException( "Cannot add style class to rule" );
        
            } else if( stack.size() > 0 ) {
                Element currentElement = stack.peek();

                if( ! currentElement.started ) {
                    if( currentElement.attributes.containsKey( "class" ) ) { 
                        currentElement.attributes.put( "class", currentElement.attributes.<String>get( "class" ).trim() + " " + name );
                    } else {
                        currentElement.attributes.put( "class", name );
                    }
                } else {
                    throw new IllegalStateException( "Cannot add style class, element '" + currentElement.name + " is already started" );
                }
        
            } else {
                throw new IllegalStateException( "No current element" );
            }
        }
        return this;
    }
    
    public XHTMLWriter style( Font font ) {
        if( font != null ) {
            if( font.getStyle() != null ) {
                if( font.getStyle() == Font.Style.ITALIC ) {
                    style( "font-style", "italic" );
                }
            }

            if( font.getWeight() != null ) {
                if( font.getWeight() == Font.Weight.BOLD ) {
                    style( "font-weight", "bold" );
                } else if( font.getWeight() == Font.Weight.LIGHT ) {
                    style( "font-weight", "lighter" );
                }
            }
    
            if( font.getSize() != null ) {
                style( "font-size", font.getSize() );
            }
    
            if( font.getFamily() != null ) {
                style( "font-family", font.getFamily() );
            } 

            if( font.getLine() != null ) {
                if( font.getLine() == Font.Line.UNDER ) {
                    style( "text-decoration", "underline" );
                } else if( font.getLine() == Font.Line.OVER ) {
                    style( "text-decoration", "overline" );
                } else if( font.getLine() == Font.Line.STRIKE ) {
                    style( "text-decoration", "line-through" );
                }
            }
            
        }
        
        return this;
    }

    public XHTMLWriter style( String name, Box box ) {
        if( box != null ) {
            style( name, box.get( "top", 0 ) + "px " + box.get( "right", 0 ) + "px " + box.get( "bottom", 0 ) + "px " + box.get( "left", 0 ) + "px" );
        }
        
        return this;
    }

    public XHTMLWriter style( String name, Color color ) {
        if( color != null ) {
            if( color.equals( Color.TRANSPARENT ) ) {
                style( name, "transparent" );
            } else {
                style( name, new java.util.Formatter().format( "#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue() ).toString() );
            }
        }
        
        return this;
    }

    public XHTMLWriter style( String name, Size size ) {
        if( size != null ) {
            if( Size.Kind.PIXELS.equals( size.getKind() ) ) {
                style( name, size.getValue() + "px" );
            } else if( Size.Kind.POINTS.equals( size.getKind() ) ) {
                style( name, size.getValue() + "pt" );
            } else if( Size.Kind.EMS.equals( size.getKind() ) ) {
                style( name, size.getValue() + "em" );
            } else if( Size.Kind.PERCENTAGE.equals( size.getKind() ) ) {
                style( name, size.getValue() + "%" );
            } else {
                style( name, "auto" );
            }
        }
        
        return this;
    }

    protected void flushStyles() {
        if( currentRule != null ) {
            currentRule.flush();
            currentRule = null;
        }
    }

    protected void closeStyles() {
        flushStyles();
        if( stack.size() > 0 && "style".equalsIgnoreCase( stack.peek().name ) ) {
            stack.pop().end();
        }
    }
    
    public XHTMLWriter table() {
        return start( "table" );
    }

    public XHTMLWriter row() {
        return start( "tr" );
    }

    public XHTMLWriter cell() {
        return start( "td" );
    }
    
    public XHTMLWriter div() {
        return start( "div" );
    }
    
    public XHTMLWriter span() {
        return start( "span" );
    }
    
    public XHTMLWriter image() {
        return start( "img" );
    }
    
    public XHTMLWriter space() {
        return space( 1 );
    }
    
    public XHTMLWriter space( int count ) {
        for( int i: range( count ) ) {
            write( "&nbsp;" );
        }
        return this;
    }
    
    public XHTMLWriter br() {
        write( "<br/>" );
        return this;
    }

    public XHTMLWriter bold() {
        return start( "b" );
    }
    
    public XHTMLWriter italic() {
        return start( "i" );
    }

    public XHTMLWriter underline() {
        return start( "u" );
    }

}
