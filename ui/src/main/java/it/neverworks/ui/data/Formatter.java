package it.neverworks.ui.data;

public interface Formatter<T> {
    
    Object format( T value );
}