package it.neverworks.ui.data;

public interface Inventory {
    public <T> T select( Object selectable );
    public <T> T deselect();
    public <T> T item( Object key );

    public DataSource getItems();
    public void setItems( DataSource items );
    public Object getSelected();
    public void setSelected( Object selected );
    public Object getItem();
    public void setItem( Object item );
    public Object getValue();
    public void setValue( Object value );

}