package it.neverworks.ui.data;

import java.text.NumberFormat;
import java.text.DecimalFormat;

import java.util.Locale;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Objects;
import it.neverworks.i18n.Translation;

public class NumberFormatter implements ValueFormatter {
    
    private NumberFormat format;
    private boolean lenient;
    
    public NumberFormatter() {
        this( NumberFormat.getInstance( Translation.current() ) );
    }
    
    public NumberFormatter( String pattern ) {
        this( Translation.current(), pattern );

    }

    public NumberFormatter( Locale locale, String pattern ) {
        this( NumberFormat.getNumberInstance( Translation.current() ) );
        DecimalFormat df = (DecimalFormat)this.format;
        df.applyPattern( pattern );

    }
    
    public NumberFormatter( Arguments args ) {
        this( args.<Locale>get( "locale", Translation.current() ), args.<String>get( "format", null ) );
        this.lenient = args.<Boolean>get( "lenient", false );
    }
    
    public NumberFormatter( NumberFormat format ) {
        if( format != null ) {
            this.format = format;
        } else {
            throw new IllegalArgumentException( "Null format" );
        }
    }
    
    public Object format( Object value ) {
        if( value != null ) {
            if( Numbers.isNumber( value ) ) {
                Number number = Numbers.number( value );
                if( number instanceof Double || number instanceof Float ) {
                    return this.format.format( number.doubleValue() );
                } else {
                    return this.format.format( number.longValue() );
                }
            } else {
                if( lenient ) {
                    return Strings.valueOf( value );
                } else {
                    throw new FormatterException( "NumberFormatter: Cannot format " + Objects.className( value ) );
                }
            }
        } else {
            return null;
        }
    }
    
}