package it.neverworks.ui.data;

import it.neverworks.model.converters.Convert;

@Convert( StyleArtifactConverter.class )
public interface StyleArtifact extends Artifact {
    
}