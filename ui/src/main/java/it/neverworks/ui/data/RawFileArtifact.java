package it.neverworks.ui.data;

public class RawFileArtifact extends RawArtifact implements FileArtifact {
    
    public RawFileArtifact() {
        super();
    }

    public RawFileArtifact( Object content ) {
        this();
        set( "content", content );
    }

}