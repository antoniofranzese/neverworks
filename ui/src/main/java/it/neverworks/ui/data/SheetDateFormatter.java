package it.neverworks.ui.data;

import java.util.Date;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;

public class SheetDateFormatter implements ValueFormatter {
    
    private String format;
    private boolean lenient;
    
    public SheetDateFormatter() {
        this( arg( "format", null ) );
    }
    
    public SheetDateFormatter( String pattern ) {
        this( arg( "format", pattern ) );
    }
    
    public SheetDateFormatter( Arguments arguments ) {
        this.format = arguments.get( "format", "dd/mm/yyyy" );
        this.lenient = arguments.get( "lenient", false );
    }
    
    public Object format( Object value ) {
        if( value != null ) {
            if( value instanceof Date || lenient ) {
                return arg( "value", value ).arg( "format", format );
            } else {
                throw new FormatterException( "SheetDateFormatter: Cannot format " + repr( value ) );
            }
        } else {
            return null;
        }
    }
    
}