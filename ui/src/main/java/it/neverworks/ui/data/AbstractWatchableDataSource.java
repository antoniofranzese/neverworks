package it.neverworks.ui.data;

import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

public abstract class AbstractWatchableDataSource extends AbstractDataSource implements WatchableDataSource {
    protected Set<Object> changes;
    protected boolean watching = false;
    
    public void watch() {
        this.watching = true;
        this.changes = new HashSet<Object>();
    }
    
    public void ignore() {
        this.watching = false;
        this.changes = null;
    }
    
    public DataSource changes() {
        return new ListDataSource( this.changes != null ? new ArrayList( this.changes ) : new ArrayList() );
    }
    
    public boolean watching() {
        return this.watching;
    }
    
    public void touch( Object item ) {
        touch( keyProperty(), item );
    }
    
    public void touch( String property, Object item ) {
        Object key = get( item, property );
        Object existing = load( property, key );
        if( existing != null ) {
            touch( property, key, existing );
        } else {
            throw new MissingItemException( "Missing item for '" + property + "' key: " + key );
        }    
    }
    
    protected void touch( String property, Object key, Object item ) {
        if( this.watching ) {
            this.changes.add( item );
        } else {
            this.touch();
        }
        
    }
    
}