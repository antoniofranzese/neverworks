package it.neverworks.ui.data;

import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Modifiers;

public class ExpressionFormatter<T> implements ItemFormatter<T> {
    
    public final static Modifiers WITH_NULLS = new Modifiers().withNulls();
    
    private String expression;
    
    public ExpressionFormatter( String expression ) {
        this.expression = expression;
    }
    
    public Object format( T source ) {
        return ExpressionEvaluator.evaluate( source, expression, WITH_NULLS );
    }
}