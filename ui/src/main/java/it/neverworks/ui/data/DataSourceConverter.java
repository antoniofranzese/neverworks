package it.neverworks.ui.data;

import java.util.List;
import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.expressions.ObjectQuery;

public class DataSourceConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof DataSource ) {
            return value;

        } else if( value instanceof List ) {
            return new ListDataSource( (List) value );

        } else if( Collections.isListable( value ) ) {
            return new ListDataSource( Collections.list( value ) );

        } else {
            return value;
        }
    }
}