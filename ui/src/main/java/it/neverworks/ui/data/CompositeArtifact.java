package it.neverworks.ui.data;

public interface CompositeArtifact {
    Artifact[] getArtifacts();
}