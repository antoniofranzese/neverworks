package it.neverworks.ui.data;

import java.util.List;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Default;

@Convert( DataSourceConverter.class )
@Default( DataSourceDefaultFactory.class )
public interface DataSource extends Iterable {
    
    <T> T load( Object key );
    <T> T load( String property, Object key );

    <T> List<T> gather( Object key );
    <T> List<T> gather( String property, Object key );

    int size();
    Iterable slice( int start, int count );
    DataSource sort( String name );
    
    <T> T get( Object item, String property );
    void set( Object item, String property, Object value );
    
    <T> T get( String name );
    <T extends AbstractDataSource> T set( String name, Object value );
    
    String getDefaultKey();
    public void setDefaultKey( String property );
    
    public void touch();

    default boolean contains( Object key ) {
        return gather( key ).size() > 0;
    }
    
    default boolean contains( String property, Object key ) {
        return gather( property, key ).size() > 0;
    }

}