package it.neverworks.ui.data;

public interface Catalog {
    Artifact get( String expression );    
}