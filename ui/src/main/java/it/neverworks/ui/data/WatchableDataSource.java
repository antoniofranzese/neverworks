package it.neverworks.ui.data;

import java.util.List;

public interface WatchableDataSource {
    
    public void watch();
    public void ignore();
    public DataSource changes();
    public boolean watching();
    public void touch( Object key );
    public void touch( String property, Object key );


}