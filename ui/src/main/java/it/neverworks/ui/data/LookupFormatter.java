package it.neverworks.ui.data;

import java.util.Map;
import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class LookupFormatter<T> extends BaseModel implements ValueFormatter<T> {
    
    @Property
    private DataSource items;
    
    @Property
    private String key = "id";
    
    @Property
    private String caption = "name";
    
    @Property
    private String empty;
    
    public LookupFormatter() {
        this( new ListDataSource() );
    }

    public LookupFormatter( Object items ) {
        super();
        set( "items", items );
        this.build();
    }
    
    public void build() {}
    
    public LookupFormatter add( Object key, Object value ) {
        if( items instanceof MutableDataSource ) {
            ((MutableDataSource) items).insert( getKey(), new Item( arg( getKey(), key ).arg( getCaption(), value ) ) );
            return this;
        } else {
            throw new IllegalArgumentException( "Provided DataSource is not Mutable: " + Objects.className( items ) );
        }
    }
    
    public Object format( T value ) {
        try {
            if( value == null && empty != null ) {
                return empty;
            } else {
                return items.get( items.load( getKey(), value ), getCaption() );
            }

        } catch( MissingItemException ex ) {
            return value;
        }
    }

    public String getCaption(){
        return this.caption;
    }
    
    public void setCaption( String caption ){
        this.caption = caption;
    }
    
    public String getKey(){
        return this.key;
    }
    
    public void setKey( String key ){
        this.key = key;
    }

    public DataSource getItems(){
        return this.items;
    }
    
    public void setItems( DataSource items ){
        this.items = items;
    }
    
    public String getEmpty(){
        return this.empty;
    }
    
    public void setEmpty( String empty ){
        this.empty = empty;
    }
    

}