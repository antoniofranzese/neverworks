package it.neverworks.ui.data;

public interface MutableDataSource extends DataSource {
    
    public void insert( String property, Object item );
    public void update( String property, Object item );
    public void delete( String property, Object item );
    
    public void insert( Object item );
    public void update( Object item );
    public void delete( Object item );
    
}