package it.neverworks.ui.data;

import static it.neverworks.language.*;

import java.util.Map;

import it.neverworks.lang.Properties;
import it.neverworks.lang.Errors;

import it.neverworks.model.Property;
import it.neverworks.model.Model;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Length;

public abstract class AbstractDataSource extends AbstractModel implements ChildModel, Retrieve, Length {
    
    @Property
    protected String defaultKey;
    
    @Property
    protected ModelInstance model;
    
    @Property
    protected PropertyDescriptor property;
    
    @Property @Collection
    protected Map<String, ItemFormatter> virtuals;
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        this.model = model;
        this.property = property;
        if( this.defaultKey == null && model.actual() instanceof DataSourceKeyProvider ) {
            this.defaultKey = ((DataSourceKeyProvider) model.actual()).getDataSourceKey();
        }
    }

    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        this.model = null;
        this.property = null;
    }
    
    public Object retrieveItem( Object key ) {
        if( "size".equals( key ) ) {
            return size();
        } else if( "empty".equals( key ) ) {
            return getEmpty();
        } else if( model().has( str( key ) ) ) {
            return model().get( str( key ) );
        } else {
            return load( key );
        }
    }
    
    public int itemsCount() {
        return size();
    }
    
    public <T> T load( Object key ) {
        return load( keyProperty(), key );
    }
    
    public abstract <T> T load( String property, Object key );
    public abstract int size();

    public void touch() {
        //Touch property
        if( this.model != null ) {
            this.model.touch( this.property );
        }
    }
    
    @Virtual
    public void setTouched( boolean touched ) {
        if( touched ) {
            touch();
        }
    }
    
    @Virtual
    public boolean getEmpty() {
        return size() <= 0;
    }
    
    public <T> T get( Object item, String property ) {
        if( virtuals != null && virtuals.containsKey( property ) ) {
            return (T)virtuals.get( property ).format( item );
        } else {
            return (T)ExpressionEvaluator.evaluate( item, property );
        }
    }
    
    public void set( Object item, String property, Object value ) {
        if( virtuals != null && virtuals.containsKey( property ) ) {
            throw new DataSourceException( property + " virtual property is not writable" );
        } else {
            ExpressionEvaluator.valorize( item, property, value );
        }
    }
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( name );
    }
    
    public <T extends AbstractDataSource> T set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }

    protected String keyProperty() {
        if( this.getDefaultKey() != null ) {
            return this.getDefaultKey();
        } else {
            throw new MissingKeyException( "Default key is not configured" );
        }
    }

    /* Bean Accessors */
    
    public Map<String, ItemFormatter> getVirtuals(){
        return this.virtuals;
    }
    
    public String getDefaultKey(){
        return this.defaultKey;
    }
    
    public void setDefaultKey( String key ){
        this.defaultKey = defaultKey;
    }

    public ModelInstance getModel(){
        return this.model;
    }
    
    public PropertyDescriptor getProperty(){
        return this.property;
    }
    
}