package it.neverworks.ui.data;

import java.util.ArrayList;

import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.DefaultFactory;

public class DataSourceDefaultFactory implements DefaultFactory {

    public Object getDefaultValue( ModelInstance instance ) {
        return new ListDataSource( new ArrayList() );
    }

}

