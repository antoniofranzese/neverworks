package it.neverworks.ui.data;

import java.io.File;
import java.io.InputStream;
import java.io.ByteArrayInputStream;
import it.neverworks.lang.Reflection;
import it.neverworks.io.Streams;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.FileBadgeProvider;
import it.neverworks.io.InputStreamProvider;
import it.neverworks.model.converters.Converter;

public class FileArtifactConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof FileArtifact ) {
            return value;

        } else if( value instanceof FileInfo ) {
            return new FileInfoArtifact( (FileInfo) value );

        } else if( value instanceof FileBadge ) {
            return new FileBadgeArtifact( (FileBadge) value );

        } else if( value instanceof FileInfoProvider ) {
            return convert( ((FileInfoProvider) value).retrieveFileInfo() );

        } else if( value instanceof FileBadgeProvider ) {
            return convert( ((FileBadgeProvider) value).retrieveFileBadge() );

        } else if( value instanceof InputStream ) {
            return new StreamArtifact( ((InputStream) value) );

        } else if( value instanceof File ) {
            return new LocalFileArtifact( ((File) value) );

        } else if( Reflection.isArray( value ) ) {
            
            if( Reflection.isArrayOf( byte.class, value ) ) {
                return new StreamArtifact( Streams.asStream( (byte[]) value ) );

            } else if( Reflection.isArrayOf( Byte.class, value ) ) {
                return new StreamArtifact( Streams.asStream( (Byte[]) value ) );

            } else {
                return value;
            }

        } else if( value instanceof InputStreamProvider ) {
            return new StreamArtifact( ((InputStreamProvider) value).toStream() );

        } else if( value != null && !( value instanceof Artifact ) ) {
            return new RawFileArtifact().set( "content", value );

        } else {
            return value;
        }
    }
}