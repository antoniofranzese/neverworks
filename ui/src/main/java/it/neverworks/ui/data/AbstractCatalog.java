package it.neverworks.ui.data;

import it.neverworks.model.features.Retrieve;

public abstract class AbstractCatalog implements Catalog, Retrieve {
    
    public Object retrieveItem( Object key ) {
        return this.get( (String) key );
    }
    
    public abstract Artifact get( String key );
}