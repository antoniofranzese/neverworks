package it.neverworks.ui.data;

import java.util.Map;

public class BasicCatalog implements Catalog {
    
    protected Map<String,Artifact> contents;
    
    public Map<String,Artifact> getContents(){
        return this.contents;
    }
    
    public void setContents( Map<String,Artifact> contents ){
        this.contents = contents;
    }

    public Artifact get( String expression ) {
        if( this.contents != null && this.contents.containsKey( expression ) ) {
            return this.contents.get( expression );
        } else {
            return null;
        }
    }

}