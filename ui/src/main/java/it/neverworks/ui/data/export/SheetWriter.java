package it.neverworks.ui.data.export;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.File;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Calendar;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;

import static it.neverworks.language.*;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.model.Property;
import it.neverworks.model.SafeModel;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.EqualsBuilder;
import it.neverworks.model.utils.HashCodeBuilder;
import it.neverworks.io.Files;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileRepository;
import it.neverworks.io.CloseableGuard;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Size;

public class SheetWriter extends AbstractWriter implements CloseableGuard<SheetWriter> {
    
    private static enum Standard {
        LEGACY, XML
    }
    
    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT( CellStyle.ALIGN_LEFT ),
        CENTER( CellStyle.ALIGN_CENTER ),
        RIGHT( CellStyle.ALIGN_RIGHT ),
        JUSTIFY( CellStyle.ALIGN_JUSTIFY );
        
        private short value;
        
        private HorizontalAlignment( short value ) {
            this.value = value;
        }
        
        public short getValue(){
            return this.value;
        }
    }
    
    @AutoConvert
    public static enum VerticalAlignment {
        TOP( CellStyle.VERTICAL_TOP ),
        MIDDLE( CellStyle.VERTICAL_CENTER ),
        BOTTOM( CellStyle.VERTICAL_BOTTOM ),
        JUSTIFY( CellStyle.VERTICAL_JUSTIFY );
        
        private short value;
        
        private VerticalAlignment( short value ) {
            this.value = value;
        }
        
        public short getValue(){
            return this.value;
        }
    }

    private final static String DATE_PATTERN = "dd/mm/yyyy";
    
    private OutputStream output;
    private Workbook workbook;
    private Row lastRow;
    private List<FillingRow> fillingRows = new ArrayList<FillingRow>();
    private Standard standard;
    private boolean templated = false;
    private boolean mergeCells = false;
    private boolean streaming;
    private FileInfo fileInfo;
    private Current current = new Current();
    //private org.apache.poi.ss.usermodel.Font standardFont;

    private static class Current {
        Sheet sheet;
        boolean resizeColumns;
        Row row;
        Cell cell;
        Cell selectedCell;
        List<Size> columnSizes;
        FillingRow fillingRow;
        Decoration rowDecoration;
        Arguments cellArguments;
        Arguments rowArguments;
    }
    
    
    public SheetWriter() {
        this( new ByteArrayOutputStream() );
    }

    public SheetWriter( File output ) {
        this( 
            arg( "output", output ) 
            .arg( "standard", output.getName().endsWith( "xls" ) ? "xls" : "xlsx" )
        );
    }

    public SheetWriter( OutputStream output ) {
        this( arg( "output", output ) );
    }

    public SheetWriter( FileInfo info ) {
        this( arg( "output", info ) );
    }
    
    public SheetWriter( FileRepository repository ) {
        this( arg( "output", repository.create(
            arg( "extension", ".xlsx" )
        )));
    }

    public SheetWriter( File input, File output ) {
        this( 
            arg( "input", input )
            .arg( "output", output ) 
            .arg( "standard", input.getName().endsWith( "xls" ) ? "xls" : "xlsx" )
        );
    }

    public SheetWriter( InputStream input, OutputStream output ) {
        this( arg( "input", input ).arg( "output", output ) );
    }

    public SheetWriter( Arguments arguments ) {
        super();
        this.output = arguments.require( type( OutputStream.class ), "output" );
        
        if( this.output == null ) {
            throw new IllegalArgumentException( "Missing output stream" );
        }

        this.mergeCells = arguments.get( type( Boolean.class ), "mergeCells", true );
        this.streaming = arguments.get( type( Boolean.class ), "streaming", true );
        int streamingBuffer = arguments.get( type( Integer.class), "buffer", 100 );
        InputStream input = arguments.get( type( InputStream.class ), "input" );
 
        if( "xls".equalsIgnoreCase( arguments.get( "standard", "xlsx" ) ) ) {
            this.standard = Standard.LEGACY;
            if( input != null ) {
                this.templated = true;
                try {
                    this.workbook = new HSSFWorkbook( input );
                } catch( IOException ex ) {
                    throw Errors.wrap( ex );
                }
            } else {
                this.workbook = new HSSFWorkbook();
            }

        } else {
            this.standard = Standard.XML;
            if( input != null ) {
                this.templated = true;
                try {
                    if( this.streaming ) {
                        this.workbook = new SXSSFWorkbook( new XSSFWorkbook( input ), streamingBuffer );
                    } else {
                        this.workbook = new XSSFWorkbook( input );
                    }
                
                } catch( IOException ex ) {
                    throw Errors.wrap( ex );
                }
            } else {
                if( this.streaming ) {
                    this.workbook = new SXSSFWorkbook( streamingBuffer );
                } else {
                    this.workbook = new XSSFWorkbook();
                }
            }
        }
        
        if( arguments.get( "output" ) instanceof FileInfo ) {
            this.setFileInfo( arguments.get( "output" ) );
        } else if( arguments.has( File.class, "output" ) ) {
            this.set( "file", arguments.get( "output" ) );
        }

        if( workbook.getNumberOfSheets() > 0 ) {
            select( 0 );
        }

        CellStyle normalStyle = arguments.get( type( CellStyle.class ), "cell" );

    }

    public String getMimeType() {
        return this.standard == Standard.LEGACY 
            ? "application/vnd.ms-excel"
            : "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }

    public InputStream asStream() {
        if( output instanceof ByteArrayOutputStream ) {
            try {
                close();
                return new ByteArrayInputStream( ((ByteArrayOutputStream) output).toByteArray() );
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            return super.asStream();
        }
    }

    
    public SheetWriter set( String name, Object value ) {
        return this.<SheetWriter>_set( name, value );
    }

    public SheetWriter set( Arguments arguments ) {
        return this.<SheetWriter>_set( arguments );
    }
    
    public SheetWriter format( Class cls, Formatter formatter ) {
        formatters.put( cls, formatter );
        return this;
    }
    
    public SheetWriter sheet() {
        return sheet( new Arguments() );
    }
    
    public SheetWriter sheet( String name ) {
        return sheet( arg( "name", name ) );
    }

    private static class CellDefaults extends SafeModel {
        @Property
        Size width;

        @Property
        Size height;

        @Property
        HorizontalAlignment halign;

        @Property
        VerticalAlignment valign;

        @Property
        Font font;

        @Property
        Color color;

        @Property
        Color background;

        @Property
        Border border;
    
    }

    private static class SheetDefinition extends SafeModel {
        
        @Property
        private String name;
        
        @Property @Collection
        private List<Size> columns;

        @Property @AutoConvert
        private Arguments cell;

        @Property @AutoConvert
        private Arguments row;

        @Property @AutoConvert
        private Integer zoom;

    }
    
    public SheetWriter sheet( Arguments arguments ) {
        SheetDefinition def = new SheetDefinition().set( arguments );
        
        if( empty( def.name ) ) {
            int count = workbook.getNumberOfSheets();
            do {
                def.name = "Sheet " + count++;
            } while( workbook.getSheet( def.name ) != null );
        }
        
        if( workbook.getSheet( def.name ) == null ) {
            if( current.sheet != null ) {
                seal();
            }
            current.columnSizes = def.columns != null ? def.columns : new ArrayList<Size>();
            current.sheet = workbook.createSheet( def.name );
            current.cellArguments = null;

            if( def.cell != null ) {
                current.cellArguments = def.cell.copy( k -> def.cell.get( k ) != null );
                Arguments defaults = current.cellArguments.split( list( "height", "width" ) );

                Size defaultHeight = defaults.get( type( Size.class ), "height" );
                if( defaultHeight != null && ! defaultHeight.isAuto() ) {
                    current.sheet.setDefaultRowHeightInPoints( sizeToPoints( defaultHeight ) );
                }

                Size defaultWidth = defaults.get( type( Size.class ), "height" );
                if( defaultWidth != null && ! defaultWidth.isAuto() ) {
                    current.sheet.setDefaultColumnWidth( sizeToCharacters( defaultWidth ) );
                }
            }

            if( current.cellArguments != null && current.cellArguments.size() < 1 ) {
                current.cellArguments = null;
            }

            if( def.row != null ) {
                current.rowArguments = def.row.copy( k -> def.cell.get( k ) != null );
            }
            
            if( current.rowArguments != null && current.rowArguments.size() < 1 ) {
                current.rowArguments = null;
            }

            if( def.zoom != null ) {
                current.sheet.setZoom( def.zoom, 100 );
            }

            return this;

        } else {
            throw new IllegalArgumentException( "Duplicate sheet name: " + def.name );
        }
    }
    
    public SheetWriter select( int index ) {
        if( current.sheet != null ) {
            seal();
        }
        
        if( index < workbook.getNumberOfSheets() ) {
            current.sheet = workbook.getSheetAt( index );
            return this;
        } else {
            throw new IllegalArgumentException( "Missing sheet for index: " + index );
        }
    }

    public SheetWriter select( String name ) {
        Sheet sheet = workbook.getSheet( name );
        if( sheet != null ) {
            if( current.sheet != null ) {
                seal();
            }
            current.sheet = sheet;
            return this;
        } else {
            throw new IllegalArgumentException( "Missing sheet for name: " + sheet );
        }
    }
    
    public SheetWriter skip( int rows ) {
        if( current.sheet == null ) {
            sheet();
        }

        if( current.row != null ) {
            end();
            rows += 1;
        } else if( lastRow != null ) {
            rows += 1;
        }
        
        int last = lastRow != null ? lastRow.getRowNum() : 0;
        while( rows > 0 ) {
            if( current.sheet.getRow( last ) == null ) {
                row();
                end();
            }
            last++;
            rows--;
        }
        return this;
    }
    
    public SheetWriter jump( int row ) {
        if( current.sheet == null ) {
            sheet();
        }

        if( current.row != null ) {
            end();
        }
        
        current.row = current.sheet.getRow( row );
        if( current.row == null ) {
            current.row = current.sheet.createRow( row );
        }
        return this;
    }
    
    public SheetWriter shift( int column ) {
        if( current.sheet == null ) {
            sheet();
        }

        if( current.row == null ) {
            row();
        }
        
        if( current.row.getCell( column - 1 ) == null ) {
            current.row.createCell( column - 1 );
        }
        
        return this;
    }
    
    public SheetWriter move( String reference ) {
        CellReference ref = new CellReference( reference );
        if( ref.getSheetName() != null ) {
            select( ref.getSheetName() );
        }
        return move( ref.getRow(), ref.getCol() );
    }
    
    
    public SheetWriter move( int row, int column ) {
        jump( row );

        current.selectedCell = current.row.getCell( column );
        if( current.selectedCell == null ) {
            current.selectedCell = current.row.createCell( column );
        }
        current.cell = current.selectedCell;
        return this;
    }
    
    public SheetWriter row() {
        return row( new Arguments() );
    }

    private static class RowDefinition extends SafeModel {

        @Property
        Decoration style;

        @Property
        Size height;

    }

    public SheetWriter row( Arguments arguments ) {
        if( current.sheet == null ) {
            sheet();
        }

        if( current.row != null ) {
            end();
        }

        RowDefinition def = new RowDefinition()
            .set( current.rowArguments )
            .set( arguments );

        current.fillingRow = lastRow != null ? query( fillingRows ).by( "row.rowNum" ).eq( lastRow.getRowNum() + 1 ).result() : null;

        if( current.fillingRow != null ) {
            current.row = current.fillingRow.getRow();
            //System.out.println( msg( "filling row {}", current.fillingRow ) );
            fillingRows.remove( current.fillingRow );
        } else {
            int last = current.sheet.getLastRowNum();
            //TODO: rivedere
            if( last != 0 || current.sheet.getRow( last ) != null ) {
                last += 1;
            }
            current.row = current.sheet.createRow( last );
            //System.out.println( msg( "new row {}", current.row.getRowNum() ) );
        }
        
        if( def.height != null ) {
            current.row.setHeightInPoints( sizeToPoints( def.height ) );
        }
        
        current.rowDecoration = def.style;
        return this;
    }
    
    public SheetWriter cell() {
        return cell( null, null );
    }

    public SheetWriter cell( Object value ) {
        return cell( value, null );
    }

    private static class CellDefinition extends SafeModel {

        @Property
        Font font;

        @Property
        Color color;

        @Property
        Color background;

        @Property
        Border border;

        @Property
        Decoration style;

        @Property
        Formatter format;

        @Property @AutoConvert
        Integer hspan;

        @Property @AutoConvert
        Integer vspan;

        @Property @AutoConvert
        String pattern;

        @Property
        HorizontalAlignment halign;

        @Property
        VerticalAlignment valign;
        
        @Property @AutoConvert
        Short indent;

        @Property @AutoConvert
        Short rotation;
        
        @Property
        Object value;

        @Property
        Size width;

        @Property
        Size height;

    }

    public SheetWriter cell( Object value, Arguments arguments ) {

        if( current.row == null ) {
            row();
        }

        CellDefinition def = new CellDefinition()
            .set( current.cellArguments )
            .set( arguments );

        if( value instanceof Map ) {
            def.set( Arguments.process( value ) );
        } else {
            def.set( "value", value );
        }

        if( def.hspan == null || def.hspan < 1 ) {
            def.hspan = 1;
        }

        if( def.vspan == null || def.vspan < 1 ) {
            def.vspan = 1;
        }

        if( def.vspan > 1 && def.valign == null ) {
            def.valign = VerticalAlignment.TOP;
        }
        
        Cell cell = current.selectedCell;
        if( cell == null ) {
            if( current.fillingRow != null && current.fillingRow.hasFillings() ) {
                cell = current.row.getCell( current.fillingRow.pickFilling().getCol() );
            } else {
                cell = appendCell( current.row );  
            }
        }
        current.cell = cell;

        if( def.style != null ) {
            if( def.font == null ) {
                def.font = def.style.getFont();
            }
            if( def.border == null ) {
                def.border = def.style.getBorder();
            }
            if( def.color == null ) {
                def.color = def.style.getColor();
            }
            if( def.background == null ) {
                def.background = def.get( "!style.background.color" );
            }
        } 
        
        if( current.rowDecoration != null ) {
            if( def.font == null ) {
                def.font = current.rowDecoration.getFont();
            }
            if( def.border == null ) {
                def.border = current.rowDecoration.getBorder();
            }
            if( def.color == null ) {
                def.color = current.rowDecoration.getColor();
            }
            if( def.background == null ) {
                def.background = current.rowDecoration.get( "!background.color" );
            }
        }
        
        CellStyle cellStyle = buildCellStyle( def );
        if( cellStyle != null ) {
            cell.setCellStyle( cellStyle );
        }    

        if( def.height != null ) {
            float height = sizeToPoints( def.height );
            if( cell.getRow().getHeightInPoints() < height ) {
                cell.getRow().setHeightInPoints( height );
            }
        }

        if( current.fillingRow != null ) {
            current.fillingRow.remove( cell );
        }

		if( def.hspan > 1 || def.vspan > 1 ) {
    		int firstCol = cell.getColumnIndex();		
    		int lastCol = firstCol + def.hspan - 1;
            int firstRow = current.row.getRowNum();
            int lastRow = firstRow + def.vspan - 1;
            //System.out.println( msg( "Cell fr {}, lr {}, fc {}, lc {}: {}", firstRow, lastRow, firstCol, lastCol, def.value ) );
            if( mergeCells ) {
                current.sheet.addMergedRegion( new CellRangeAddress( firstRow, lastRow, firstCol, lastCol ) );
            }
            
            for( int r = firstRow; r <= lastRow; r++ ) {
                FillingRow fillingRow = null;

                Row row = current.sheet.getRow( r );
                if( row == null ) {
                    row = current.sheet.createRow( r );
                    fillingRow = new FillingRow( row );
                    fillingRows.add( fillingRow );
                    //System.out.println( msg( "Created row {}", row.getRowNum() ) );
                } else if( r > firstRow ) {
                    fillingRow = query( fillingRows ).by( "row" ).eq( row ).result();
                }
                
                //System.out.println( msg( "Row {} Filling row {}, count {}", row.getRowNum(), fillingRow, cellCount( row ) ) );
                
                if( fillingRow != null && firstCol > 0 ) {
                    while( cellCount( row ) < firstCol ) {
                        Cell fillCell = appendCell( row );
                        //System.out.println( "Create filling cell " + ref( fillCell ) );
                        fillingRow.addFilling( fillCell );
                    }
                }

                for( int c = firstCol; c <= lastCol; c++ ) {
                    if( r > firstRow || c > firstCol ) {
                        Cell spanCell = appendCell( row );
                        if( fillingRow != null ) {
                            fillingRow.addSpanning( spanCell );
                        }
                        if( cellStyle != null ) {
                            spanCell.setCellStyle( cellStyle );
                        }    

                        //System.out.println( "Create span cell" + ref( spanCell ) );
                    }
                }
            }
		} else {
            //System.out.println( msg( "plain cell: {}", def.value ) );
        }
        
        Object formatted = formatValue( def.value, def.format );
        
		if( formatted != null ) {

			if( formatted instanceof Number ) {
				cell.setCellValue( ((Number)formatted).doubleValue() );

			} else if( formatted instanceof Date ) {
				cell.setCellValue( (Date)formatted );
                if( def.pattern == null ) {
                    def.pattern = DATE_PATTERN;
                }

			} else if( formatted instanceof Calendar ) {
				cell.setCellValue( (Calendar)formatted );				
                if( def.pattern == null ) {
                    def.pattern = DATE_PATTERN;
                }

			} else if( formatted instanceof Boolean ) {
				cell.setCellValue( ((Boolean)formatted).booleanValue() );				

			} else {
				cell.setCellValue( Strings.valueOf( formatted ) );

			}

		} else {
		    cell.setCellValue( "" );
		}

        if( current.columnSizes != null ) {
            int columnCount = current.row.getLastCellNum();
            while( current.columnSizes.size() < columnCount ) {
                current.columnSizes.add( Size.auto() );
            }

            if( def.width != null ) {
                int idx = columnCount - 1;
                if( current.columnSizes.get( idx ).isAuto() || sizeToPixels( def.width ) > sizeToPixels( current.columnSizes.get( idx ) ) ) {
                    current.columnSizes.set( idx, def.width );
                }            
            }
        }

        return this;
    }
    
    public SheetWriter end() {
        if( current.row != null ) {
            lastRow = current.row;
            current.row = null;
            current.rowDecoration = null;
        }
        return this;
    }
    
    public SheetWriter seal() {
        if( current.sheet != null ) {
            if( current.row != null ) {
                end();
            }

            if( current.columnSizes != null ) {
                for( int i: range( current.columnSizes ) ) {
                    if( current.columnSizes.get( i ).getKind() != Size.Kind.AUTO ) {
                        current.sheet.setColumnWidth( i, Math.min( sizeToWidthUnits( current.columnSizes.get( i ) ), EXCEL_MAX_COLUMN_WIDTH ) );

                    } else {
                        current.sheet.autoSizeColumn( i, /* useMergedCells */ true );
                    }
                }
            }
            
            //TODO: registro definizioni foglio
            current = new Current();
            lastRow = null;
        }
        return this;
    }
    
    public SheetWriter flush() {
        try {
            if( current.sheet != null ) {
                seal();
            }
            
            workbook.write( output );
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
        return this;
    }
    
    public void doClose() throws IOException {
        flush();
        output.close();
    }
    
    protected static Cell appendCell( Row row ) {
        return row.createCell( row.getLastCellNum() > 0 ? row.getLastCellNum() : 0 );
    }

    protected static int cellCount( Row row ) {
        short last = row.getLastCellNum();
        return last < 0 ? 0 : last;
    }

    protected static CellReference ref( Cell cell ) {
        return new CellReference( cell.getRowIndex(), cell.getColumnIndex() );
    }

    protected Map<CellStyleDescription,CellStyle> cellStyleCache = new HashMap<CellStyleDescription,CellStyle>();

    protected void populateNormalStyle( CellDefinition cell ) {

    }
    
    protected CellStyle buildCellStyle( CellDefinition cell ) {
        CellStyleDescription desc = new CellStyleDescription( cell );
        
        if( ! cellStyleCache.containsKey( desc ) ) {

            CellStyle style = null;

            if( cell.font != null || cell.color != null ) {
                if( style == null ) style = workbook.createCellStyle();
                populateFont( style, cell.font, cell.color );
            }
        
            if( cell.background != null ) {
                if( style == null ) style = workbook.createCellStyle();
                populateBackground( style, cell.background );
            }

            if( cell.border != null ) {
                if( style == null ) style = workbook.createCellStyle();
                populateBorder( style, cell.border );
            }

            if( ! empty( cell.pattern ) ) {
                if( style == null ) style = workbook.createCellStyle();
                style.setDataFormat( this.workbook.createDataFormat().getFormat( cell.pattern ) );
            }
        
            if( cell.halign != null ) {
                if( style == null ) style = workbook.createCellStyle();
                style.setAlignment( cell.halign.getValue() );
            }

            if( cell.valign != null ) {
                if( style == null ) style = workbook.createCellStyle();
                style.setVerticalAlignment( cell.valign.getValue() );
            }
            
            if( cell.indent != null ) {
                if( style == null ) style = workbook.createCellStyle();
                style.setIndention( cell.indent.shortValue() );
            }
            
            if( cell.rotation != null ) {
                if( style == null ) style = workbook.createCellStyle();
                style.setRotation( cell.rotation.shortValue() );
            }
            
            cellStyleCache.put( desc, style );
        }

        return cellStyleCache.get( desc );
    }
    
    protected Map<FontDefinition,org.apache.poi.ss.usermodel.Font> fontCache = new HashMap<FontDefinition,org.apache.poi.ss.usermodel.Font>();
    
    protected org.apache.poi.ss.usermodel.Font retrieveFont( Font font, Color color ) {
        FontDefinition fontDef = new FontDefinition();

        if( font != null && font.getWeight() == Font.Weight.BOLD ) {
            fontDef.weight = org.apache.poi.ss.usermodel.Font.BOLDWEIGHT_BOLD;
        }

        if( font != null && font.getStyle() == Font.Style.ITALIC ) {
            fontDef.italic = true;
        }
        
        if( font != null && font.getSize() != null && list( Size.Kind.PIXELS, Size.Kind.POINTS ).contains( font.getSize().getKind() ) ) {
            int points = font.getSize().getKind() == Size.Kind.POINTS ? font.getSize().getValue() : font.getSize().getValue() * 72 / 96;
            fontDef.height = (short) points;
        }
        
        if( font != null && font.getFamily() != null ) {
            fontDef.name = font.getFamily();
        }
        
        if( color != null ) {
            fontDef.color = color;
        }
        
        if( ! fontCache.containsKey( fontDef ) ) {
            org.apache.poi.ss.usermodel.Font xlsFont = workbook.createFont();
            
            if( fontDef.weight != null ) {
                xlsFont.setBoldweight( fontDef.weight );
            }

            if( fontDef.italic != null ) {
                xlsFont.setItalic( fontDef.italic );
            }
        
            if( fontDef.height != null ) {
                xlsFont.setFontHeightInPoints( fontDef.height );
            }
        
            if( fontDef.name != null ) {
                xlsFont.setFontName( fontDef.name );
            }
        
            if( fontDef.color != null ) {
                if( standard == Standard.XML ) {
                    ((XSSFFont) xlsFont).setColor( retrieveXSSFColor( fontDef.color ) );
                } else {
                    xlsFont.setColor( retrieveHSSFColor( fontDef.color ).getIndex() );
                }
            }
            fontCache.put( fontDef, xlsFont );

        }

        return fontCache.get( fontDef );
    }
    
    protected void populateFont( CellStyle style, Font font, Color color ) {
        style.setFont( retrieveFont( font, color ) );
    }
    
    protected void populateBackground( CellStyle style, Color background ) {
        if( standard == Standard.XML ) {
            ((XSSFCellStyle) style).setFillForegroundColor( retrieveXSSFColor( background ) );
        } else {
            style.setFillForegroundColor( retrieveHSSFColor( background ).getIndex() );
        }
        style.setFillPattern( CellStyle.SOLID_FOREGROUND );
        
    }
    
    protected Map<Color,XSSFColor> colorCache = new HashMap<Color,XSSFColor>();
    
    protected XSSFColor retrieveXSSFColor( Color color ) {
        if( ! colorCache.containsKey( color ) ) {
            XSSFColor xc = new XSSFColor( new java.awt.Color( color.getRed(), color.getGreen(), color.getBlue() ) );
            colorCache.put( color, xc );
        }
        return colorCache.get( color );
    }
    
    protected HSSFColor retrieveHSSFColor( Color color ) {
        HSSFPalette palette = ((HSSFWorkbook) workbook).getCustomPalette();
        
        HSSFColor result = palette.findColor( (byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue() );
        if( result == null ) {
            result = palette.findSimilarColor( (byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue() );
        }

        return result;

    }
    
    protected void populateBorder( CellStyle style, Border border ) {
        if( border.getLeft() != null ) {
            style.setBorderLeft( translateBorder( border.getLeft() ) );
            if( border.getLeft().getColor() != null ) {
                if( standard == Standard.XML ) {
                    ((XSSFCellStyle) style).setLeftBorderColor( retrieveXSSFColor( border.getLeft().getColor() ) );
                } else {
                    style.setLeftBorderColor( retrieveHSSFColor( border.getLeft().getColor() ).getIndex() );
                }
            }
        }

        if( border.getRight() != null ) {
            style.setBorderRight( translateBorder( border.getRight() ) );
            if( border.getRight().getColor() != null ) {
                if( standard == Standard.XML ) {
                    ((XSSFCellStyle) style).setRightBorderColor( retrieveXSSFColor( border.getRight().getColor() ) );
                } else {
                    style.setRightBorderColor( retrieveHSSFColor( border.getRight().getColor() ).getIndex() );
                }
            }
        }
        
        if( border.getTop() != null ) {
            style.setBorderTop( translateBorder( border.getTop() ) );
            if( border.getTop().getColor() != null ) {
                if( standard == Standard.XML ) {
                    ((XSSFCellStyle) style).setTopBorderColor( retrieveXSSFColor( border.getTop().getColor() ) );
                } else {
                    style.setTopBorderColor( retrieveHSSFColor( border.getTop().getColor() ).getIndex() );
                }
            }
        }
        
        if( border.getBottom() != null ) {
            style.setBorderBottom( translateBorder( border.getBottom() ) );
            if( border.getBottom().getColor() != null ) {
                if( standard == Standard.XML ) {
                    ((XSSFCellStyle) style).setBottomBorderColor( retrieveXSSFColor( border.getBottom().getColor() ) );
                } else {
                    style.setBottomBorderColor( retrieveHSSFColor( border.getBottom().getColor() ).getIndex() );
                }
            }
        }

    }

    protected short translateBorder( Border.Side side ) {
        if( side.getStyle() == Border.Style.DOUBLE ) {
            return CellStyle.BORDER_DOUBLE;
        
        } if( side.getSize() == null || side.getSize().intValue() == 1 ) {
            if( side.getStyle() == Border.Style.DOTTED ) {
                return CellStyle.BORDER_DOTTED;
            } else if( side.getStyle() == Border.Style.DASHED ) {
                return CellStyle.BORDER_DASHED;
            } else if( side.getStyle() == Border.Style.SOLID || side.getSize() != null ){
                return CellStyle.BORDER_THIN;
            } else {
                return CellStyle.BORDER_NONE;
            }
        
        } else if( side.getSize().intValue() == 2 ) {
            if( side.getStyle() == Border.Style.DOTTED ) {
                return CellStyle.BORDER_MEDIUM_DASH_DOT_DOT;
            } else if( side.getStyle() == Border.Style.DASHED ) {
                return CellStyle.BORDER_MEDIUM_DASHED;
            } else {
                return CellStyle.BORDER_MEDIUM;
            }

        } else if( side.getSize().intValue() > 2 ) {
            return CellStyle.BORDER_THICK;
            
        } else if( side.getSize().intValue() < 1 ) {
            return CellStyle.BORDER_HAIR;

        } else {
            return CellStyle.BORDER_NONE;
        }
    
    }
    
    protected float sizeToPoints( Size size ) {
        if( size.getKind() == Size.Kind.PIXELS ) {
            return 0.75f * size.getValue();
        } else if( size.getKind() == Size.Kind.POINTS ) {
            return new Float( size.getValue() );
        } else {
            throw new IllegalArgumentException( "Cannot convert to points: " + size );
        }
    }

    protected Integer sizeToPixels( Size size ) {
        if( size.getKind() == Size.Kind.PIXELS ) {
            return size.getValue();
        } else if( size.getKind() == Size.Kind.POINTS ) {
            return Math.round( 1.33333334f * size.getValue() );
        } else {
            throw new IllegalArgumentException( "Cannot convert to pixels: " + size );
        }
    }
    
    protected Short sizeToWidthUnits( Size size ) {
        int pixels = sizeToPixels( size );
        return pixel2WidthUnits( pixels );
    }

    protected Integer sizeToCharacters( Size size ) {
        return Numbers.Int( Math.ceil( sizeToWidthUnits( size ) / 256.0f ) );
    }

    protected final static int EXCEL_MAX_COLUMN_WIDTH = 255 * 256;
    protected static final short EXCEL_COLUMN_WIDTH_FACTOR = 256;
    protected static final int UNIT_OFFSET_LENGTH = 7;
    protected static final int[] UNIT_OFFSET_MAP = new int[] { 0, 36, 73, 109, 146, 182, 219 };

    protected static short pixel2WidthUnits( int pixels ) {
        short widthUnits = (short) (EXCEL_COLUMN_WIDTH_FACTOR * ( pixels / UNIT_OFFSET_LENGTH ) );

        widthUnits += UNIT_OFFSET_MAP[( pixels % UNIT_OFFSET_LENGTH )];

        return widthUnits;
    }

    protected static int widthUnits2Pixel( short widthUnits ) {
        int pixels = (widthUnits / EXCEL_COLUMN_WIDTH_FACTOR) * UNIT_OFFSET_LENGTH;

        int offsetWidthUnits = widthUnits % EXCEL_COLUMN_WIDTH_FACTOR;
        pixels += Math.round((float) offsetWidthUnits / ((float) EXCEL_COLUMN_WIDTH_FACTOR / UNIT_OFFSET_LENGTH));

        return pixels;
    }

    private static class BorderDefinition {
        public Short border;
        public Color color;
        
        public boolean equals( Object other ) {
            if( other instanceof BorderDefinition ) {
                BorderDefinition o = (BorderDefinition)other;
                if( border != null ? ! border.equals( o.border ) : o.border != null ) return false;
                if( color != null  ? ! color.equals( o.color )   : o.color != null )  return false;
                return true;
            } else {
                return false;
            }
        }
    }

    private static class FontDefinition {
        public Short weight;
        public Boolean italic;
        public Color color;
        public Short height;
        public String name;
        
        public boolean equals( Object other ) {
            if( other instanceof FontDefinition ) {
                FontDefinition o = (FontDefinition)other;
                if( weight != null ? ! weight.equals( o.weight ) : o.weight != null ) return false;
                if( italic != null ? ! italic.equals( o.italic ) : o.italic != null ) return false;
                if( color != null  ? ! color.equals( o.color )   : o.color != null )  return false;
                if( height != null ? ! height.equals( o.height ) : o.height != null ) return false;
                if( name != null   ? ! name.equals( o.name )     : o.name != null )   return false;
                return true;
            } else {
                return false;
            }
        }
        
        public int hashCode() {
            return new HashCodeBuilder( this )
                .append( name )
                .append( height )
                .append( color )
                .append( weight )
                .append( italic )
                .toHashCode();
        }

        public String toString() {
            return msg( "Font: name={0}, height={1}, weight={2}, italic={3}, color={4}", name, height, weight, italic, color );
        }
    }
    
    private static class CellStyleDescription {
        String dataFormat;
        Font font;
        Color color;
        Color background;
        Border border;
        HorizontalAlignment halign;
        VerticalAlignment valign;
        Short indent;
        Short rotation;
        
        public CellStyleDescription( CellDefinition cell ) {
            super();
            this.dataFormat = cell.pattern;
            this.font = cell.font;
            this.color = cell.color;
            this.background = cell.background;
            this.border = cell.border;
            this.halign = cell.halign;
            this.valign = cell.valign;
            this.indent = cell.indent;
            this.rotation = cell.rotation;
        }
        
        public boolean equals( Object other ) {
            if( other instanceof CellStyleDescription ) {
                CellStyleDescription o = (CellStyleDescription)other;
                if( dataFormat != null ? ! dataFormat.equals( o.dataFormat ) : o.dataFormat != null ) return false;
                if( font != null       ? ! font.equals( o.font )             : o.font != null ) return false;
                if( color != null      ? ! color.equals( o.color )           : o.color != null )  return false;
                if( background != null ? ! background.equals( o.background ) : o.background != null ) return false;
                if( border != null     ? ! border.equals( o.border )         : o.border != null )   return false;
                if( halign != null     ? ! halign.equals( o.halign )         : o.halign != null )   return false;
                if( valign != null     ? ! valign.equals( o.valign )         : o.valign != null )   return false;
                if( indent != null     ? ! indent.equals( o.indent )         : o.indent != null )   return false;
                if( rotation != null   ? ! rotation.equals( o.rotation )     : o.rotation != null )   return false;
                return true;
            } else {
                return false;
            }
        }
        
        public int hashCode() {
            return new HashCodeBuilder( this )
                .append( dataFormat )
                .append( font )
                .append( color )
                .append( background )
                .append( border )
                .append( halign )
                .append( valign )
                .append( indent )
                .append( rotation )
                .toHashCode();
        }

    }

    private static class FillingRow {
        private List<CellReference> fillings = new ArrayList<CellReference>();
        private Set<CellReference> spannings = new HashSet<CellReference>();
        private Row row;

        public FillingRow( Row row ) {
            this.row = row;
        }

        public Row getRow(){
            return this.row;
        }
        
        public void addFilling( Cell cell ) {
            this.fillings.add( ref( cell ) );
        }

        public void addSpanning( Cell cell ) {
            this.spannings.add( ref( cell ) );
        }

        public boolean hasFillings() {
            return this.fillings.size() > 0;
        }

        public CellReference pickFilling() {
            CellReference r = this.fillings.get( 0 );
            this.fillings.remove( 0 );
            return r;
        }

        public boolean hasSpanning( Cell cell ) {
            return this.spannings.contains( ref( cell ) );
        }

        public void remove( Cell cell ) {
            CellReference cref = ref( cell );
            this.fillings.remove( cref );
            this.spannings.remove( cref );
        }

        public String toString() {
            return "FillingRow(" + this.row.getRowNum() + ")";
        }

    }
    
    public static interface WorkbookWork {
        void doWithWorkbook( Workbook workbook );
    }

    public SheetWriter withWorkbook( WorkbookWork work ) {
        work.doWithWorkbook( workbook );
        return this;
    }

    public static interface SheetWork {
        void doWithSheet( Sheet sheet );
    }

    public SheetWriter withSheet( SheetWork work ) {
        if( current.sheet != null ) {
            work.doWithSheet( current.sheet );
            return this;
        } else {
            throw new IllegalStateException( "No current sheet" );
        }
    }

    public static interface CellWork {
        void doWithCell( Cell cell );
    }

    public SheetWriter withCell( CellWork work ) {
        if( current.cell != null ) {
            work.doWithCell( current.cell );
            return this;
        } else {
            throw new IllegalStateException( "No current cell" );
        }
    }

}