package it.neverworks.ui.data;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Comparator;

import it.neverworks.lang.SparseList;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Properties;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.cache.Cache;
import it.neverworks.cache.SimpleLRUCache;
import it.neverworks.model.Property;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.SingleValueCriteria;
import it.neverworks.model.features.Append;
import it.neverworks.model.features.Clear;
import it.neverworks.model.features.Length;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;

public class ListDataSource extends AbstractWatchableDataSource implements MutableDataSource, IndexedDataSource, Append, Clear, Length {
    
    private final static TypeDefinition<Integer> IntType = TypeFactory.shared( Integer.class );

    public class StringMatch extends SingleValueCriteria {
        
        public StringMatch( String expression, Object value ) {
            super( expression, value != null ? value.toString() : value );
        }
        
        public boolean matches( Object target, Object value ) {
            return target != null ? target.toString().equals( value ) : value == null; 
        }
    }
    
    protected class CacheModule {
        private Cache keys;
        private Cache<Integer> indexes;
        
        public CacheModule( int size ) {
            this.keys = new SimpleLRUCache( size );
            this.indexes = new SimpleLRUCache<Integer>( size );
        }

        public void clear() {
            this.keys.clear();
            this.indexes.clear();
        }
    }
    
    protected List list;
    protected FluentMap<String,CacheModule> caches = new FluentHashMap<String, CacheModule>();
    protected int cacheSize = 50;
    
    public ListDataSource( List list ) {
        this.list = list != null ? list : new ArrayList();
    }

    public ListDataSource( Iterable iterable ) {
        this( Collections.list( iterable ) );
    }

    public ListDataSource() {
        this( new ArrayList() );
    }
    
    protected CacheModule cache( String property ) {
        return this.caches.probe( property, () -> new CacheModule( this.cacheSize ) );
    }
    
    public void appendItem( Object item ) {
        this.list.add( item );
    }
    
    public void clearItems() {
        this.clear();
    }
    
    public void clear() {
        this.list.clear();
        this.touch();
    }
    
    public int indexOf( Object key ){
        return indexOf( keyProperty(), key );
    }

    public int indexOf( String property, Object key ){
        return cache( property ).indexes.probe( str( key ), k -> {
            List<Integer> indexes = new ObjectQuery( list ).add( new StringMatch( property, k ) ).indexes();
        
            if( indexes.size() == 1 ) {
                return indexes.get( 0 );

            } else if( indexes.size() == 0 ) {
                throw new MissingItemException( "Missing item for '" + property + "' key: " + k );

            } else {
                throw new NonUniqueItemException( "Non unique item for '" + property + "' key: " + k );
            }
        });
    }
    
    public <T> T pick( int index ) {
        if( index >= 0 && index < this.list.size() ) {
            return (T)this.list.get( index );
        } else {
            throw new MissingItemException( "Item index out of range: " + index );
        }
    }
    
    public <T> T get( Object item, String property ) {
        if( "#".equals( property ) ) {
            Integer index = this.list.indexOf( item );
            if( index >= 0 ) {
                return (T)index;
            } else {
                throw new MissingItemException( "Cannot retrieve index for: " + item );
            }
        } else {
            return super.get( item, property );
        }
    }
    
    public <T> T load( Object key ) {
        return load( keyProperty(), key );
    }
    
    public <T> T load( String property, Object key ) {
        //TODO: differenziare le cache per property ed usare un oggetto apposito che mantenga una dimensione massima
        if( "#".equals( property ) ) {
            int index = IntType.process( key );
            if( index < this.list.size() ) {
                return (T)this.list.get( index );
            } else {
                throw new MissingItemException( "Item index out of range: " + index );
            }

        } else {
            return (T) cache( property ).keys.probe( str( key ), k -> {
                List results = new ObjectQuery( list ).add( new StringMatch( property, k ) ).list();
                if( results.size() == 1 ) {
                    return (T)results.get( 0 );

                } else if( results.size() == 0 ) {
                    throw new MissingItemException( "Missing item for '" + property + "' key: " + k );

                } else {
                    throw new NonUniqueItemException( "Non unique item for '" + property + "' key: " + k );
                }
            });
        }

    }
    
    public <T> List<T> gather( Object key ) {
        return gather( keyProperty(), key );
    }
    
    public <T> List<T> gather( String property, Object key ){
        return new ObjectQuery<T>( list ).add( new StringMatch( property, key ) ).list();
    }
    
    public Iterator iterator() {
        return list.iterator();
    }
    
    public int size() {
        return list.size();
    }
    
    public int itemsCount() {
        return this.size();
    }
    
    public Iterable slice( int start, int count ) {
        if( start < list.size() ) {
            return list.subList( start, ( start + count ) < list.size() ? ( start + count ) : list.size() );
        } else {
            return new ArrayList();
        }
    }
    
    public <T> ObjectQuery<T> query( Class<T> cls ) {
        return new ObjectQuery<T>( list ).byItem().instanceOf( cls );
    }

    public <T> ObjectQuery<T> query() {
        return new ObjectQuery<T>( list );
    }
    
    private class ItemComparator implements Comparator {
        private String expression;
        private int direction;
        private DataSource source;
        
        public ItemComparator( ListDataSource source, String expression ) {
            this.source = source;
            if( expression.startsWith( "-" ) ) {
                this.expression = expression.substring( 1 );
                this.direction = -1;
            } else {
                this.expression = expression;
                this.direction = 1;
            }
        }
        
        public int compare( Object item1, Object item2 ){
            Object obj1 = source.get( item1, expression );
            Object obj2 = source.get( item2, expression );
            
            if( obj1 instanceof Comparable ) {
                if( obj2 == null ) {
                    return 1 * direction;
                } else {
                    return ((Comparable) obj1).compareTo( obj2 ) * direction;
                }
            } else if( obj1 == null ) {
                if( obj2 == null ) {
                    return 0;
                } else {
                    return -1 * direction;
                }
            } else if( obj2 == null ) {
                return 1 * direction;
            } else {
                throw new IllegalArgumentException( "Cannot compare " + Objects.className( obj1 ) + " and " + Objects.className( obj2 ) );
            }
        }
    }
    
    public DataSource sort( String expression ) {
        return wrap( Collections.sort( this.list, new ItemComparator( this, expression ) ) );
    }
    
    public ListDataSource wrap( List list ) {
        ListDataSource source = new ListDataSource( list );
        if( this.virtuals != null ) {
            source.virtuals = new HashMap<String, ItemFormatter>( this.virtuals );
        }
        source.defaultKey = this.defaultKey;
        return source;
    }
    
    public ListDataSource add( String name, ItemFormatter formatter ) {
        this.getVirtuals().put( name, formatter );
        return this;
    }
    
    public void insert( Object item ) {
        insert( keyProperty(), item );
    }

    public void insert( String property, Object item ) {
        Object key = get( item, property );
        try {
            load( property, key );
            throw new DuplicateItemException( "Duplicate item for '" + property + "' key: " + key );
        } catch( MissingItemException ex ) {
            this.list.add( item );
            touch();
        }
    }
    
    public void update( Object item ) {
        update( keyProperty(), item );
    }
    
    public void update( String property, Object item ) {
        Object key = get( item, property );
        Object existing = load( property, key );
        if( existing != null ) {
            int index = list.indexOf( existing );
            list.set( index, item );
            touch( property, key, item );
        } else {
            throw new MissingItemException( "Missing item for '" + property + "' key: " + key );
        }    
    }
    
    public void delete( Object item ) {
        delete( keyProperty(), item );
    }

    public void delete( String property, Object item ) {
        Object key = get( item, property );
        Object existing = load( property, key );
        if( existing != null ) {
            int index = list.indexOf( existing );
            list.remove( index );
            touch();
        } else {
            throw new MissingItemException( "Missing item for '" + property + "' key: " + key );
        }    
    }
    
    protected String keyProperty() {
        if( this.getDefaultKey() != null ) {
            return this.getDefaultKey();
        } else {
            throw new MissingKeyException( "Default key is not configured" );
        }
    }
    
    public void touch() {
        //Invalidate caches
        caches.clear();
        if( this.watching() ) {
            this.touched = true;
        }
        super.touch();
    }
    
    /* Watchable */
    
    private boolean touched = false;

    public void watch() {
        this.touched = false;
        super.watch();
    }
    
    @Override
    public DataSource changes() {
        if( !this.touched && this.changes != null ) {
            if( "#".equals( keyProperty() ) ) {
                SparseList sparse = new SparseList();
                for( Object item: this.changes ) {
                    sparse.set( this.list.indexOf( item ), item );
                }
                return wrap( sparse );
            } else {
                return wrap( new ArrayList( this.changes ) );
            }
            
        } else {
            return wrap( new ArrayList() );
        }
    }
    
}