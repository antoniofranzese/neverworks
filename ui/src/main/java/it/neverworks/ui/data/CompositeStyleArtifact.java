package it.neverworks.ui.data;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import it.neverworks.lang.Collections;

public class CompositeStyleArtifact implements StyleArtifact, CompositeArtifact {
    
    private List<StyleArtifact> artifacts;

    public CompositeStyleArtifact() {
        super();
        this.artifacts = new ArrayList<StyleArtifact>();
    }
    
    public CompositeStyleArtifact( StyleArtifact... artifacts ) {
        super();
        this.artifacts = Collections.list( artifacts );
    }

    public CompositeStyleArtifact( Collection<StyleArtifact> artifacts ) {
        super();
        this.artifacts = Collections.list( artifacts );
    }

    public CompositeStyleArtifact add( StyleArtifact artifact ) {
        if( artifact != null ) {
            this.artifacts.add( artifact );
        }
        return this;
    }
    
    public Artifact[] getArtifacts() {
        return Collections.toArray( Artifact.class, artifacts );
    }
    
    public String getName() {
        return "Composite: " + Collections.join( query( artifacts ).get( "name" ), ", " );
    }
}