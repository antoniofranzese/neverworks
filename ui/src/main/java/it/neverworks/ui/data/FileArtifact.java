package it.neverworks.ui.data;

import it.neverworks.model.converters.Convert;

@Convert( FileArtifactConverter.class )
public interface FileArtifact extends Artifact {
    
}