package it.neverworks.ui.data.export;

import java.io.Closeable;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.OutputStream;
import java.io.FileWriter;
import java.io.Writer;
import java.io.File;

import java.util.Stack;
import java.util.Map;
import java.util.HashMap;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.io.StringWriter;
import it.neverworks.io.CloseableGuard;
import it.neverworks.encoding.XML;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.data.Formatter;

public class XMLWriter extends AbstractXMLWriter implements CloseableGuard<XMLWriter> {
    
    public XMLWriter() {
        this( new StringWriter() );
    }
    
    public XMLWriter( File file ) {
        try {
            this.writer = new FileWriter( file );
            this.file = file;
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public XMLWriter( OutputStream stream ) {
        this.writer = new OutputStreamWriter( stream );
    }
    
    public XMLWriter( Writer writer ) {
        super();
        this.writer = writer;
    }
    
    public XMLWriter( FileInfo info ) {
        this( info.openWriter() );
        this.setFileInfo( info );
    }
    
    public String getMimeType() {
        return "text/xml";
    }

    public XMLWriter set( String name, Object value ) {
        return this.<XMLWriter>_set( name, value );
    }

    public XMLWriter set( Arguments arguments ) {
        return this.<XMLWriter>_set( arguments );
    }
 
    public XMLWriter format( Class cls, Formatter formatter ) {
        formatters.put( cls, formatter );
        return this;
    }

    public XMLWriter init() {
        return init( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" );
    }

    public XMLWriter init( String declaration ) {
        if( ! initialized ) {
            write( declaration );
            initialized = true;
        }
        return this;
    }

    public XMLWriter start( String name ) {
        startElement( name );
        return this;
    }
    
    public XMLWriter in() {
        indentCurrentElement();
        return this;
    }
    
    public XMLWriter attr( String name, Object value ) {
        return attr( name, value, null );
    }

    public XMLWriter attr( String name, Object value, Formatter formatter ) {
        if( value != null ) {
            addAttribute( name, value, formatter );
        }
        return this;
    }
    
    public XMLWriter text( Object value ) {
        return text( value, null );
    } 

    public XMLWriter text( Object value, Formatter formatter ) {
        startCurrentElement();
        write( XML.encode( formatValue( value, formatter ) ) );
        return this;
    } 

    public XMLWriter raw( Object value ) {
        return raw( value, null );
    } 

    public XMLWriter raw( Object value, Formatter formatter ) {
        startCurrentElement();
        write( formatValue( value, formatter ) );
        return this;
    } 

    public XMLWriter cdata( Object value ) {
        return cdata( value, null );
    } 

    public XMLWriter cdata( Object value, Formatter formatter ) {
        startCurrentElement();
        write( "<![CDATA[" + formatValue( value, formatter ) + "]]>" );
        return this;
    } 
    
    public XMLWriter end( String name ) {
        endElement( name );
        return this;
    }

    public XMLWriter end() {
        endElement();
        return this;
    }

}
