package it.neverworks.ui.data;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Locale;

import it.neverworks.lang.Dates;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Objects;
import it.neverworks.i18n.Translation;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.DateConverter;

public class DateFormatter implements ValueFormatter {
    
    private final static String DEFAULT_FORMAT = "dd/MM/yyyy";
    private DateFormat format;
    private boolean lenient;
    private Converter converter = new DateConverter();
    
    public DateFormatter() {
        this( Translation.current(), DEFAULT_FORMAT );
    }
    
    public DateFormatter( String pattern ) {
        this( new SimpleDateFormat( pattern, Translation.current() ) );
    }

    public DateFormatter( Locale locale, String pattern ) {
        this( new SimpleDateFormat( pattern, locale ) );
    }
    
    public DateFormatter( Arguments args ) {
        this( args.<Locale>get( "locale", Translation.current() ), args.<String>get( "format", DEFAULT_FORMAT ) );
        this.lenient = args.<Boolean>get( "lenient", false );
        this.converter = args.get( "converter", new DateConverter() );
    }
    
    public DateFormatter( DateFormat format ) {
        if( format != null ) {
            this.format = format;
        } else {
            throw new IllegalArgumentException( "Null format" );
        }
    }
    
    public Object format( Object value ) {
        if( value != null ) {
            Object converted = converter != null ? converter.convert( value ) : value;
            if( converted instanceof Date ) {
                return this.format.format( converted );
            } else {
                if( lenient ) {
                    return Strings.valueOf( value );
                } else {
                    throw new FormatterException( "DateFormatter: Cannot format " + Objects.className( value ) );
                }
            }
        } else {
            return null;
        }
    }
    
}