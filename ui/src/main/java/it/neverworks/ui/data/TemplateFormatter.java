package it.neverworks.ui.data;

import it.neverworks.lang.Strings;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.utils.ToStringBuilder;

public class TemplateFormatter implements ItemFormatter {
    
    private Template template = null;
    private String constant = null;

    public TemplateFormatter( Object text ) {
        this( Strings.valueOf( text ) );
    }
    
    public TemplateFormatter( String text ) {
        super();
        if( Template.isTemplate( text ) ) {
            this.template = new Template( text ).lenient();
        } else {
            this.constant = text;
        }
    }

    public Object format( Object item ) {
        return template != null ? template.apply( item ) : constant;
    }

    public String toString() {
        ToStringBuilder builder = new ToStringBuilder( this );
        if( template != null ) {
            builder.add( "template", template );
        } else {
            builder.add( "constant", constant );
        }
        return builder.toString();
    }
}