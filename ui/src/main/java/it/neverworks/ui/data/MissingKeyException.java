package it.neverworks.ui.data;

public class MissingKeyException extends DataSourceException {
    
    public MissingKeyException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public MissingKeyException( String message ){
        super( message );
    }
}