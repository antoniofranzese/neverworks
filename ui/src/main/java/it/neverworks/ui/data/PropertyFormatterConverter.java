package it.neverworks.ui.data;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.features.Text;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.TemplateFormatter;
import it.neverworks.ui.data.ExpressionFormatter;

public class PropertyFormatterConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof ItemFormatter ) {
            return value;

        } else if( value instanceof String ) {
            if( Strings.hasText( value ) ) {
                if( Template.isTemplate( value ) ) {
                    return new TemplateFormatter( (String) value );
                } else {
                    return new ExpressionFormatter( (String) value );
                }
            } else {
                return null;
            }

        } else if( value instanceof Text ) {
            return convert( ((Text) value).asText() );

        } else {
            return value;
        }
    }

}