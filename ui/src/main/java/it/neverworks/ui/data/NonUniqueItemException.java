package it.neverworks.ui.data;

public class NonUniqueItemException extends DataSourceException {
    
    public NonUniqueItemException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public NonUniqueItemException( String message ){
        super( message );
    }
}