package it.neverworks.ui.data;

public class DuplicateItemException extends DataSourceException {
    
    public DuplicateItemException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public DuplicateItemException( String message ){
        super( message );
    }
}