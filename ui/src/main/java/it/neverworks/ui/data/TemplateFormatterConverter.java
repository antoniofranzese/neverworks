package it.neverworks.ui.data;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.features.Text;

public class TemplateFormatterConverter implements Converter {

    public Object convert( Object value ) {
        if( value instanceof Formatter ) {
            return value;

        } else if( value instanceof String ) {
            if( Strings.hasText( value ) ) {
                String template = (String) value;
                return new TemplateFormatter( template );
                
            } else {
                return null;
            }

        } else if( value instanceof Text ) {
            return convert( ((Text) value).asText() );

        } else {
            return value;
        }
    }
    
}
