package it.neverworks.ui.data;

import java.util.List;

public class ListSource extends ListDataSource {
    
    public ListSource( Iterable iterable ) {
        super( iterable );
    }

    public ListSource( List list ) {
        super( list );
    }

    public ListSource() {
        super();
    }
    
}