package it.neverworks.ui.data;

public interface DataSourceKeyProvider {
    String getDataSourceKey();
}