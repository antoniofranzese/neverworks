package it.neverworks.ui.data;

import java.util.Map;
import it.neverworks.model.utils.ExpandoModel;

public class Item extends ExpandoModel {
    
    public Item() {
        super();
    }
    
    public Item( Map<String, Object> properties ) {
        super( properties );
    }
    
}