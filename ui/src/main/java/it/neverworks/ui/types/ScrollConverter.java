package it.neverworks.ui.types;

import java.util.Map;

import it.neverworks.model.converters.Converter;

public class ScrollConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Scroll ) {
            return value;

        } else if( value instanceof Map ) {
            Scroll scroll = new Scroll();
            Map map = (Map) value;
            if( map.containsKey( "vertical" ) ) {
                scroll.set( "vertical", map.get( "vertical" ) );
            }
            if( map.containsKey( "horizontal" ) ) {
                scroll.set( "horizontal", map.get( "horizontal" ) );
            }
            return scroll;

        } else if( value instanceof String || value instanceof Number ) {
            return new Scroll().set( "vertical", value );

        } else {
            return value;
        }
    }
}

