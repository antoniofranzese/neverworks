package it.neverworks.ui.types;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.description.PropertyManager;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.ModelMetadata;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.ModelCopier;
import it.neverworks.model.description.Getter;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.WidgetException;

public class DecorationStencil implements PropertyProcessor, ModelMetadata {
    
    private Interceptor getter;
    
    public void processProperty( MetaPropertyManager property ) {
        this.getter = new CascadingGetter( property );
        ModelDescriptor owner = property.getOwner();

        if( ! owner.metadata().has( this.getClass() ) ) {
            owner.metadata().add( this );
        }
    }
    
    public void meetModel( ModelDescriptor model ) {
        this.wireProperty( model, "font", Font.class );
        this.wireProperty( model, "color", Color.class );
        this.wireProperty( model, "background", Background.class );
        this.wireProperty( model, "background", Color.class );
        this.wireProperty( model, "border", Border.class );
        this.wireProperty( model, "flavor", StyleArtifact.class );
    }
    
    protected void wireProperty( ModelDescriptor model, String name, Class type ) {
        if( model.has( name, type ) ) {
            PropertyManager manager = model.manager( name );
            if( manager instanceof MetaPropertyManager ) {
                ((MetaPropertyManager) manager).placeGetter( this.getter ).after();
            }
        }
    }
    
    public void inheritModel( ModelDescriptor model ) {
/*        if( ! model.metadata().has( this.getClass() ) ) {
            model.metadata().add( this );
        }
*/    }

    public static class CascadingGetter implements Interceptor {
    
        private PropertyManager source;
        
        public CascadingGetter( PropertyManager source ) {
            this.source = source;
        }
         
        public Object invoke( Invocation invocation ) {
            Getter getter = invocation.argument( 0 );
            
            if( getter.raw() == null ) {
                Decoration decoration = (Decoration) source.getCheckedValue( getter.instance() );
                if( decoration != null ) {
                    Object value = decoration.get( getter.name() );
                    getter.setRaw( value != null ? getter.value( ModelCopier.duplicate( value ) ) : null ); 
                }
            }
            return getter.get();
        }
    
    }
}