package it.neverworks.ui.types;

import java.util.List;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.TypeAware;

public class ColorConverter implements Converter {
    
    protected Class<?> targetType;
    
    public Object convert( Object value ) {
        if( this.targetType == null ? value instanceof Color : this.targetType.isInstance( value ) ) {
            return value;
            
        } else if( value instanceof String ) {
            return toTargetType( new Color( (String) value ) );
            
        } else if( value instanceof List && ((List) value).size() == 3 ) {
            return toTargetType( new Color( 
                ((Number) ((List) value).get( 0 ) ).shortValue(), 
                ((Number) ((List) value).get( 1 ) ).shortValue(), 
                ((Number) ((List) value).get( 2 ) ).shortValue() 
            ));
            
        } else if( value instanceof java.awt.Color ) {
            return toTargetType( new Color( (java.awt.Color) value ) );
        
        } else if( value instanceof Background ) {
            return toTargetType( ((Background) value).getColor() );
        
        } else {
            return value;
        }
    }
    
    protected Object toTargetType( Color color ) {
        if( targetType == null || color == null ) {
            return color;
        } else if( targetType.equals( java.awt.Color.class ) ) {
            return color.getAWTColor();
        } else {
            return color;
        }
    }
    
    public void setRelatedClass( Class<?> type ) {
        this.targetType = type;
    }
    
}