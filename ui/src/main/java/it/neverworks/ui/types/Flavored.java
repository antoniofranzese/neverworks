package it.neverworks.ui.types;

import it.neverworks.ui.data.StyleArtifact;

public interface Flavored {
    public StyleArtifact getFlavor();
}