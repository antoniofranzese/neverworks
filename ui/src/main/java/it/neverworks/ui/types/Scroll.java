package it.neverworks.ui.types;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Watchable;

@Watchable
@Convert( ScrollConverter.class )
public class Scroll extends BaseModel {
    
    @Property @Access( setter = "setDirection" )
    private Size vertical;

    @Property @Access( setter = "setDirection" )
    private Size horizontal;
    
    protected void setDirection( Setter<Size> setter ) {
        setter.set();
        setter.touch();
    }
    
    public Size getHorizontal(){
        return this.horizontal;
    }
    
    public void setHorizontal( Size horizontal ){
        this.horizontal = horizontal;
    }
    
    public Size getVertical(){
        return this.vertical;
    }
    
    public void setVertical( Size vertical ){
        this.vertical = vertical;
    }
}