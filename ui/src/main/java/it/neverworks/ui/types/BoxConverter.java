package it.neverworks.ui.types;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.List;

import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;

public class BoxConverter implements Converter {
    
    private final static TypeDefinition<Integer> IntType = TypeFactory.shared( Integer.class );

    public Object convert( Object value ) {
        if( value instanceof Box ) {
            return value;
        } else if( value instanceof Number ) {
            return new Box( ((Number) value).intValue() );
        } else if( value instanceof Map ) {
            return new Box( (Map) value );
        } else if( value instanceof List ) {
            List list = (List) value;
            try {
                if( list.size() >= 4 ) {
                    return new Box(
                        IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 1 ) )
                        ,IntType.process( list.get( 2 ) )
                        ,IntType.process( list.get( 3 ) )
                    );
                } else if( list.size() == 3 ) {
                    return new Box(
                        IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 1 ) )
                        ,IntType.process( list.get( 2 ) )
                        ,IntType.process( list.get( 1 ) )
                    );
                } else if( list.size() == 2 ) {
                    return new Box(
                        IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 1 ) )
                        ,IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 1 ) )
                    );
                } else if( list.size() == 1 ) {
                    return new Box(
                        IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 0 ) )
                        ,IntType.process( list.get( 0 ) )
                    );
                } else {
                    return value;
                }
            } catch( Exception ex ) {
                return value;
            }

        } else if( value instanceof String ) {
            if( Arguments.isParsable( value ) ) {
                return new Box().set( Arguments.parse( value ) );
            } else {
                return convert( each( ((String) value).replaceAll( "\\s+", "," ).split( "," ) ).filter( s -> ! empty( s ) ).list());
            }
        
        } else {
            return value;
        }
    }
}