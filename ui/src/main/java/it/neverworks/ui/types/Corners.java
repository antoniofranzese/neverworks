package it.neverworks.ui.types;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;

@Watchable
@Convert( CornersConverter.class )
public class Corners extends BaseModel {
    
    @Property
    protected Size topLeft;

    @Property
    protected Size topRight;

    @Property
    protected Size bottomLeft;

    @Property
    protected Size bottomRight;
    
    public Corners() {}
        
    public Corners( Object value ) {
        set( "all", value );
    }

    public Corners( Object topLeft, Object topRight, Object bottomRight, Object bottomLeft ) {
        set( "topLeft", topLeft );
        set( "topRight", topRight );
        set( "bottomLeft", bottomLeft );
        set( "bottomRight", bottomRight );
    }
    
    public Corners( Arguments arguments ) {
        set( arguments );
    }

    /* Virtual Properties */

    @Virtual
    public void setAll( Object value ) {
        set( "topLeft", value );
        set( "topRight", value );
        set( "bottomLeft", value );
        set( "bottomRight", value );
    }
    
    @Virtual
    public Size getBr(){
        return get( "bottomRight" );
    }
    
    public void setBr( Object br ){
        set( "bottomRight", br );
    }
    
    @Virtual
    public Size getBl(){
        return get( "bottomLeft" );
    }
    
    public void setBl( Object bl ){
        set( "bottomLeft", bl );
    }
    
    @Virtual
    public Size getTr(){
        return get( "topRight" );
    }
    
    public void setTr( Object tr ){
        set( "topRight", tr );
    }
    
    @Virtual
    public Size getTl(){
        return get( "topLeft" );
    }
    
    public void setTl( Object tl ){
        set( "topLeft", tl );
    }

    /* Bean Accessors */

    public Size getBottomRight(){
        return this.bottomRight;
    }
    
    public void setBottomRight( Size bottomRight ){
        this.bottomRight = bottomRight;
    }
    
    public Size getBottomLeft(){
        return this.bottomLeft;
    }
    
    public void setBottomLeft( Size bottomLeft ){
        this.bottomLeft = bottomLeft;
    }
    
    public Size getTopRight(){
        return this.topRight;
    }
    
    public void setTopRight( Size topRight ){
        this.topRight = topRight;
    }
    
    public Size getTopLeft(){
        return this.topLeft;
    }
    
    public void setTopLeft( Size topLeft ){
        this.topLeft = topLeft;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "topLeft" )
            .add( "topRight" )
            .add( "bottomLeft" )
            .add( "bottomRight" )
            .toString();
    }
    
}