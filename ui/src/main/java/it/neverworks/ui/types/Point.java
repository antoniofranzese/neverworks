package it.neverworks.ui.types;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.utils.ToStringBuilder;

@Watchable
@Convert( PointConverter.class )
public class Point extends BaseModel {
    
    @Property @AutoConvert
    private Integer x;
    
    @Property @AutoConvert
    private Integer y;
    
    public Point() {
        super();
    }
    
    public Point( int x, int y ) {
        super();
        this.x = x;
        this.y = y;
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY( Integer y ){
        this.y = y;
    }
    
    public Integer getX(){
        return this.x;
    }
    
    public void setX( Integer x ){
        this.x = x;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "x" )
            .add( "y" )
            .toString();
    }
}