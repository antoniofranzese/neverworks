package it.neverworks.ui.types;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.SafeModel;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Watch;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.utils.ToStringBuilder;

@Watchable
@Convert( BorderConverter.class )
public class Border extends SafeModel {
    
    @AutoConvert
    public enum Style {
        SOLID, DOTTED, DASHED, DOUBLE;
    }
    
    private final static TypeDefinition<Side> SideType = TypeFactory.build( Side.class );
    private final static TypeDefinition<Color> ColorType = TypeFactory.build( Color.class );
    private final static TypeDefinition<Style> StyleType = TypeFactory.build( Style.class );
    private final static TypeDefinition<Size> SizeType = TypeFactory.build( Size.class );
    
    public static class SideConverter implements Converter {
    
        public Object convert( Object value ) {
            if( value instanceof Side ) {
                return value;
            } else if( value instanceof Number ) {
                return new Side().set( "size", ((Number) value).intValue() );
            } else if( value instanceof String ) {
                Side side = new Side();
                String[] parts = ((String) value).trim().replaceAll( "\\s+", " " ).split( " " );
                
                for( String part: parts ) {
                    boolean processed = false;
                    
                    if( side.get( "size" ) == null ) {
                        Object s = SizeType.convert( part );
                        if( s instanceof Size && ((Size) s).getKind() == Size.Kind.PIXELS ) {
                            side.set( "size", ((Size) s).getValue() );
                            processed = true;
                        }
                    }
                    
                    if( !processed && side.get( "style" ) == null ) {
                        Object t = StyleType.convert( part );
                        if( t instanceof Style ) {
                            side.set( "style", t );
                            processed = true;
                        }
                    }

                    if( !processed && side.get( "color" ) == null ) {
                        Object c = ColorType.convert( part );
                        if( c instanceof Color ) {
                            side.set( "color", c );
                        }
                    }
                    
                }

                return side;

            } else if( value instanceof Map ) {
                Arguments arguments = Arguments.process( value );
                return new Side()
                    .set( "size", arguments.get( "size", null ) )
                    .set( "style", arguments.get( "style", null ) )
                    .set( "color", arguments.get( "color", null ) );
            } else {
                return value;
            }
        }
    }
    
    @Watchable
    @Convert( SideConverter.class )
    public static class Side extends SafeModel {

        @Property
        private Integer size;
        
        @Property
        private Color color;

        @Property
        private Style style;
        
        public Style getStyle(){
            return this.style;
        }
        
        public void setStyle( Style style ){
            this.style = style;
        }

        public Color getColor(){
            return this.color;
        }
        
        public void setColor( Color color ){
            this.color = color;
        }
        
        public Integer getSize(){
            return this.size;
        }
        
        public void setSize( Integer size ){
            this.size = size;
        }

        public String toString() {
            return new ToStringBuilder( this )
                .add( "size" )
                .add( "color" )
                .add( "style" )
                .toString();
        }

    }

    public Border(){}
        
    public Border( int size ) {
        setSize( size );
    }

    public Border( String spec ) {
        Side side = SideType.process( spec );
        if( side.getSize() != null ) {
            setSize( side.getSize() );
        }
        if( side.getColor() != null ) {
            setColor( side.getColor() );
        }
        if( side.getStyle() != null ) {
            setStyle( side.getStyle() );
        }
    }

    public Border( Color color ) {
        setColor( color );
    }
    
    public Border( Arguments arguments ) {
        this();
        set( arguments );
    }

    @Property @Watch
    protected Side top;
    
    @Property @Watch
    protected Side bottom;
    
    @Property @Watch
    protected Side left;
    
    @Property @Watch
    protected Side right;
    
    @Virtual
    public void setSize( int size ){
        set( "!top.size",    size );
        set( "!bottom.size", size );
        set( "!left.size",   size );
        set( "!right.size",  size );
    }

    @Virtual
    public void setColor( Object color ){
        set( "!top.color",    color );
        set( "!bottom.color", color );
        set( "!left.color",   color );
        set( "!right.color",  color );
    }

    @Virtual
    public void setStyle( Object style ){
        set( "!top.style",    style );
        set( "!bottom.style", style );
        set( "!left.style",   style );
        set( "!right.style",  style );
    }

    public Side getRight(){
        return this.right;
    }
    
    public void setRight( Side right ){
        this.right = right;
    }
    
    public Side getLeft(){
        return this.left;
    }
    
    public void setLeft( Side left ){
        this.left = left;
    }
    
    public Side getBottom(){
        return this.bottom;
    }
    
    public void setBottom( Side bottom ){
        this.bottom = bottom;
    }
    
    public Side getTop(){
        return this.top;
    }
    
    public void setTop( Side top ){
        this.top = top;
    }

    public String toString() {
        return new ToStringBuilder( this )
            .add( "top" )
            .add( "bottom" )
            .add( "left" )
            .add( "right" )
            .toString();
    }

}