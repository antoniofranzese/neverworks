package it.neverworks.ui.types;

import it.neverworks.model.converters.Convert;

@Convert( ScrollabilityConverter.class )
public enum Scrollability {
    NONE, 
    HORIZONTAL, 
    VERTICAL, 
    BOTH
}

