package it.neverworks.ui.types;

import java.util.List;
import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;

public class PointConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Point ) {
            return value;
            
        } else if( value instanceof String ) {
            String[] numbers = ((String) value).split( "," );
            if( numbers.length == 2 ) {
                try {
                    int x = Integer.parseInt( numbers[ 0 ].trim() );
                    int y = Integer.parseInt( numbers[ 1 ].trim() );
                    return new Point( x, y );
                } catch( NumberFormatException ex ) {
                    return value;
                }
                
            } else {
                return value;
            }

        } else if( value instanceof Map ) {
            Map args = (Map) value;

            if( args.containsKey( "x" ) && args.containsKey( "y" ) ) {
                try {
                    int x = args.get( "x" ) instanceof Number ? ((Number) args.get( "x" )).intValue() : Integer.parseInt( String.valueOf( args.get( "x" ) ).trim() );
                    int y = args.get( "y" ) instanceof Number ? ((Number) args.get( "y" )).intValue() : Integer.parseInt( String.valueOf( args.get( "y" ) ).trim() );
                    return new Point( x, y );
                } catch( NumberFormatException ex ) {
                    return value;
                }
                
            } else {
                return value;
            }

        } else if( Collections.isListable( value ) ) {
            List list = Collections.list( value );
         
            if( list.size() == 2 ) {
                try {
                    int x = list.get( 0 ) instanceof Number ? ((Number) list.get( 0 )).intValue() : Integer.parseInt( String.valueOf( list.get( 0 ) ).trim() );
                    int y = list.get( 1 ) instanceof Number ? ((Number) list.get( 1 )).intValue() : Integer.parseInt( String.valueOf( list.get( 1 ) ).trim() );
                    return new Point( x, y );
                } catch( NumberFormatException ex ) {
                    return value;
                }
            } else {
                return value;
            }
            
        } else {
            return value;
        }
    }
}
    