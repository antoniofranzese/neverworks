package it.neverworks.ui.types;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.EnumConverter;
import it.neverworks.model.converters.BooleanConverter;

public class ScrollabilityConverter implements Converter {
    private final static Converter ENUM = new EnumConverter( Scrollability.class );
    private final static Converter BOOL = new BooleanConverter();
    
    public Object convert( Object value ) {
        if( value instanceof Scrollability ) {
            return value;
        } else if( value instanceof Boolean ) {
            if( Boolean.TRUE.equals( value ) ) {
                return Scrollability.BOTH;
            } else {
                return Scrollability.NONE;
            }
        } else if( value instanceof String ) {
            Object bool = BOOL.convert( value );
            if( Boolean.TRUE.equals( bool) ) {
                return Scrollability.BOTH;
            } else if( Boolean.FALSE.equals( value ) ) {
                return Scrollability.NONE;
            } else {
                return ENUM.convert( value );
            }
        } else {
            return ENUM.convert( value );
        }
    }
}

