package it.neverworks.ui.types;

import static it.neverworks.language.*;
import it.neverworks.model.converters.ModelConverter;

public class ViewportInfoConverter extends ModelConverter {
    
    public ViewportInfoConverter() {
        super( 
            arg( "type", ViewportInfo.class )
            .arg( "lenient", true )
        );
    }
    
    public Object convert( Object value ) {
        if( value instanceof Boolean || value instanceof String ) {
            try {
                return new ViewportInfo().set( "visible", value );
            } catch( IllegalArgumentException ex ) {
                return value;
            }
        } else {
            return super.convert( value );
        }
    }

}