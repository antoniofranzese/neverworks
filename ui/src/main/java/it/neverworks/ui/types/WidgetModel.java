package it.neverworks.ui.types;

import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.SafeModel;
import it.neverworks.model.Property;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.Widget;

@Watchable
public class WidgetModel extends SafeModel implements ChildModel {
 
    @Property @Ignore
    private Attributes attributes;

    @Property @Ignore
    private Attributes production;
    
    @Property
    private Widget parent;
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        if( model.is( Widget.class ) ) {
            set( "parent", model.as( Widget.class ) );
        } else {
            set( "parent", null );
        }
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        set( "parent", null );
    }

    public Attributes getAttributes(){
        return this.attributes;
    }
    
    public void setAttributes( Attributes attributes ){
        this.attributes = attributes;
    }

    public Object getParent(){
        return this.parent;
    }
    
}