package it.neverworks.ui.types;

public interface Decorator<T> {
    Decoration decorate( T item );
}