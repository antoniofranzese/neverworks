package it.neverworks.ui.types;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.features.Retrieve;

@Convert( ZoomConverter.class )
public class Zoom  {
    
    public static enum Kind {
        VALUE
        ,FIT
    }
    
    public static Zoom value( int value ) {
        return new Zoom( value, Kind.VALUE );
    }
    
    public static Zoom fit() {
        return new Zoom( 0, Kind.FIT );
    }
    
    protected int value;
    protected Kind kind;
    
    protected Zoom( int value, Kind kind ) {
        this.value = value;
        this.kind = kind;
    }
    
    public int getValue(){
        return this.value;
    }
    
    public void setValue( int value ){
        this.value = value;
    }
    
    public Kind getKind(){
        return this.kind;
    }
    
    public void setKind( Kind kind ){
        this.kind = kind;
    }
    
    public String toString() {
        return "Zoom(" + ( Kind.FIT.equals( kind ) ? "fit" : String.valueOf( value ) ) + ")";
    }
}