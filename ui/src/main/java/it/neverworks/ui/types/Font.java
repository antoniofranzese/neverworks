package it.neverworks.ui.types;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Application;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.utils.ToStringBuilder;

@Watchable
@Convert( FontConverter.class )
public class Font extends BaseModel {
    
    @AutoConvert
    public enum Weight {
        NORMAL, MEDIUM, DEMI, BOLD, BOLDER, HEAVY, LIGHT, THIN, ULTRALIGHT
    }
    
    @AutoConvert
    public enum Stretch {
        ULTRACONDENSED, EXTRACONDENSED, CONDENSED, SEMICONDENSED, NORMAL, SEMIEXPANDED, EXPANDED, EXTRAEXPANDED, ULTRAEXPANDED
    }

    @AutoConvert
    public enum Style {
        NORMAL, ITALIC
    }
    
    @AutoConvert
    public enum Line {
        NONE, UNDER, OVER, STRIKE
    }
    
    @AutoConvert
    public enum Letter {
        NORMAL, UPPER, LOWER, CAPITAL
    }
    
    @Property
    private String family;
    
    @Property 
    private Size size;
    
    @Property 
    private Weight weight;
    
    @Property 
    private Style style;
    
    @Property 
    private Line line;
    
    @Property 
    private Letter letter;

    @Property
    protected Stretch stretch;

    @Property
    protected Size height;
    
    public Font() {
        super();
    }
    
    public Font( String template ) {
        this();
        set( "template", template );
    }
    
    public Font( Arguments arguments ) {
        this();
        set( arguments );
    }

    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Line getLine(){
        return this.line;
    }
    
    public void setLine( Line line ){
        this.line = line;
    }
    
    public Style getStyle(){
        return this.style;
    }
    
    public void setStyle( Style style ){
        this.style = style;
    }
    
    public Weight getWeight(){
        return this.weight;
    }
    
    public void setWeight( Weight weight ){
        this.weight = weight;
    }
    
    public Size getSize(){
        return this.size;
    }
    
    public void setSize( Size size ){
        this.size = size;
    }
    
    public String getFamily(){
        return this.family;
    }
    
    public void setFamily( String family ){
        this.family = family;
    }
    
    @Virtual
    public String getFace(){
        return this.family;
    }
    
    public void setFace( String face ){
        this.family = face;
    }
    
    public Letter getLetter(){
        return this.letter;
    }
    
    public void setLetter( Letter letter ){
        this.letter = letter;
    }
    
    public Stretch getStretch(){
        return this.stretch;
    }
    
    public void setStretch( Stretch stretch ){
        this.stretch = stretch;
    }
     
   @Virtual 
    public void setTemplate( String name ) {
        if( templates.containsKey( name ) ) {
            Font template = (Font) templates.get( name );
            if( template.family != null ) {
                this.family = template.family;
            }
            if( template.size != null ) {
                this.size = template.size;
            }
            if( template.weight != null ) {
                this.weight = template.weight;
            }
            if( template.style != null ) {
                this.style = template.style;
            }
            if( template.line != null ) {
                this.line = template.line;
            }
            if( template.letter != null ) {
                this.letter = template.letter;
            }
        } else {
            throw new IllegalArgumentException( "Unknown template: " + name );
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "!family" )
            .add( "!size" )
            .add( "!weight" )
            .add( "!style" )
            .add( "!line" )
            .add( "!letter" )
            .toString();
    }

    private final static Map templates = new HashMap();
    
    static {
        for( String source: list( "META-INF/neverworks/fonts.properties", "META-INF/application/fonts.properties" ) ) {
            Arguments args = Arguments.load( Application.resource( source ) );
            for( String name: args.keys() ) {
                try {
                    Object value = args.get( name );
                    Font font = new Font( Arguments.process( args.get( name ) ) );
                    templates.put( name, font );
                    
                } catch( IllegalArgumentException ex ) {
                    throw new IllegalArgumentException( msg( "Invalid font specification ({0}): {1}={2}", source, name, args.get( name ) ) );
                }
            }
        }
    }
    
    public static boolean hasTemplate( String name ) {
        return templates.containsKey( name );
    }

    
}