package it.neverworks.ui.types;

import it.neverworks.model.converters.Converter;

public class ZoomConverter implements Converter {
    public Object convert( Object value ) {
        if( value instanceof Zoom ) {
            return value;
        } else if( value instanceof Integer ) {
            return Zoom.value( ((Integer) value).intValue() );
        } else if( value instanceof Long ) {
            return Zoom.value( ((Long) value).intValue() );
        } else if( value instanceof Float ) {
            return Zoom.value( new Integer( (int) ( ((Float) value).floatValue() * 100 ) ) );
        } else if( value instanceof Double ) {
            return Zoom.value( new Integer( (int) ( ((Double) value).doubleValue() * 100 ) ) );
        } else if( value instanceof String ){
            if( "fit".equalsIgnoreCase( (String) value ) ) {
                return Zoom.fit();
            } else {
                try {
                    return Zoom.value( Integer.parseInt( (String) value ) );
                } catch( NumberFormatException ex ) {
                    try {
                        Float n = Float.parseFloat( (String) value );
                        return Zoom.value( new Integer( (int) ( n.floatValue() * 100 ) ) );
                    } catch( NumberFormatException ex2 ) {
                        return value;
                    }
                }
                
            }
        } else {
            return value;
        }
    }
}

