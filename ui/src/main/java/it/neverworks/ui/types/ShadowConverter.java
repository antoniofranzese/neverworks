package it.neverworks.ui.types;

import static it.neverworks.language.*;

import java.util.Iterator;
import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.ModelConverter;

public class ShadowConverter extends ModelConverter {

    public Object convert( Object value ) {
        if( value instanceof Iterable ) {
            Iterator it = ((Iterable) value).iterator();
            Shadow shadow = new Shadow();
            if( it.hasNext() ) {
                shadow.set( "color", it.next() );
            }
            if( it.hasNext() ) {
                shadow.set( "blur", it.next() );
                if( ! it.hasNext() ) {
                    shadow.set( "x", shadow.get( "blur" ) );
                    shadow.set( "y", shadow.get( "blur" ) );
                }
            }
            if( it.hasNext() ) {
                shadow.set( "x", it.next() );
                if( ! it.hasNext() ) {
                    shadow.set( "y", shadow.get( "x" ) );
                }
            }
            if( it.hasNext() ) {
                shadow.set( "y", it.next() );
            }
            if( it.hasNext() ) {
                shadow.set( "spread", it.next() );
            }
            return shadow;

        } else if( value instanceof String ) {
            if( Arguments.isParsable( value ) ) {
                return new Box( Arguments.parse( value ) );
            } else {
                String str = ((String) value).trim();
                if( str.indexOf( " " ) > 0 || str.indexOf( "," ) > 0 ) {
                    return convert( each( str.replaceAll( "\\s+", "," ).split( "," ) ).filter( s -> ! empty( s ) ).list());
                } else {
                    return new Shadow().set( "color", value );
                }
            }

        } else {
            return super.convert( value );
        }
    }
}