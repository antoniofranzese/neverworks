package it.neverworks.ui.types;

import it.neverworks.lang.Booleans;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.types.Size;

@Convert( ViewportInfoConverter.class )
@Identification( string = { "visible", "width", "height" } )
public class ViewportInfo extends BaseModel {
    
    @Property
    private Size width;
    
    @Property
    private Size height;
    
    @Property @AutoConvert
    private Boolean visible;
    
    @Property
    private String region;

    public void setHidden( Object hidden ) {
        set( "visible", Booleans.isFalse( hidden ) );
    }

}