package it.neverworks.ui.types;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.ui.Widget;

public class AnchorConverter implements Converter, InstanceConverter {
    
    public Object convert( Object value ) {
        if( value instanceof Point || value instanceof Widget ) {
            return value;
        } else {
            return new PointConverter().convert( value );
        }
    }
    
    public Object convert( ModelInstance instance, Object value ) {
        if( instance.actual() instanceof Widget && value instanceof String && ((String) value).indexOf( "," ) < 0 ) {
            Widget root = ((Widget) instance.actual()).root();
            if( root != null ) {
                try {
                    return root.get( (String) value );
                } catch( Exception ex ) {
                    return convert( value );
                }
            } else {
                return convert( value );
            }
        } else {
            return convert( value );
        }
    }
}