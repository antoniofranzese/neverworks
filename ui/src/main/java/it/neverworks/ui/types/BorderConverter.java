package it.neverworks.ui.types;

import java.util.Map;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;

public class BorderConverter implements Converter {
    
    protected Class<? extends Border> type = Border.class;
    
    public BorderConverter() {}
    
    public BorderConverter( Class<? extends Border> type ) {
        this.type = type;
    }
    
    public Object convert( Object value ) {
        if( value instanceof Border ) {
            return value;
        } else if( value instanceof Number ) {
            return Reflection.withConstructor( this.type, int.class ).newInstance( ((Number) value).intValue() );
        } else if( value instanceof String ) {
            if( Arguments.isParsable( value ) ) {
                return Reflection.withConstructor( this.type ).newInstance().set( Arguments.parse( value ) );
            } else {
                return Reflection.withConstructor( this.type, String.class ).newInstance( ((String) value) ); 
            }
        } else if( value instanceof Map ) {
            return Reflection.withConstructor( this.type, Arguments.class ).newInstance( Arguments.process( value ) ); 
        } else {
            return value;
        }
    }
}