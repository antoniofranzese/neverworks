package it.neverworks.ui.types;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.types.Color;

@Convert( ShadowConverter.class )
public class Shadow implements Model {
    
    @Property
    protected Color color;

    @Property
    protected Size blur;

    @Property
    protected Size spread;
    
    @Property
    protected Size x;

    @Property
    protected Size y;
    
    public Size getSpread(){
        return this.spread;
    }
    
    public void setSpread( Size spread ){
        this.spread = spread;
    }

    public Size getY(){
        return this.y;
    }
    
    public void setY( Size y ){
        this.y = y;
    }
    
    public Size getX(){
        return this.x;
    }
    
    public void setX( Size x ){
        this.x = x;
    }
    
    public Size getBlur(){
        return this.blur;
    }
    
    public void setBlur( Size blur ){
        this.blur = blur;
    }

    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }


}