package it.neverworks.ui.types;

import java.util.List;
import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;

public class PositionConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Position ) {
            return value;
            
        } else if( value instanceof String ) {
            String[] parts = ((String) value).split( "," );
            
            if( parts.length == 2 ) {
                try {
                    return new Position()
                        .set( "x", parts[ 0 ] )
                        .set( "y", parts[ 1 ] );
                } catch( Exception ex ) {
                    return value;
                }
                
            } else if( parts.length == 1 ) {
                try {
                    return new Position()
                        .set( "x", parts[ 0 ] )
                        .set( "y", parts[ 0 ] );
                } catch( Exception ex ) {
                    return value;
                }
                
            } else {
                return value;
            }

        } else if( value instanceof Map ) {
            return new Position().set( Arguments.process( value ) );

        } else if( Collections.isListable( value ) ) {
            List list = Collections.list( value );
         
            if( list.size() == 2 ) {
                try {
                    return new Position()
                        .set( "x", list.get( 0 ) )
                        .set( "y", list.get( 1 ) );
                } catch( Exception ex ) {
                    return value;
                }

            } else if( list.size() == 1 ) {
                try {
                    return new Position()
                        .set( "x", list.get( 0 ) )
                        .set( "y", list.get( 0 ) );
                } catch( Exception ex ) {
                    return value;
                }

            } else {
                return value;
            }
            
        } else {
            return value;
        }
    }
}
    