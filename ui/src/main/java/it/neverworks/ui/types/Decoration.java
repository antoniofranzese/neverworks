package it.neverworks.ui.types;

import java.util.Map;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.SafeModel;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Process;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.model.utils.ToStringBuilder;

@Convert( DecorationConverter.class )
public class Decoration extends SafeModel implements Flavored {
    
    @Property
    private Background background;
    
    @Property
    private Border border;

    @Property
    private Color color;
    
    @Property
    private Font font;
    
    @Property
    private StyleArtifact flavor;
    
    public Decoration() {
        super();
    }
        
    public Decoration( Arguments arguments ) {
        this();
        this.set( arguments );
    }
    
    public Decoration( Map<String, Object> arguments ) {
        this( Arguments.process( arguments ) );
    }
    
    public boolean isEmpty() {
        return background == null 
            && border == null
            && color == null                  
            && font == null 
            && flavor == null;
    }
    
    protected void setFlavor( Setter<Object> setter ) {
        if( setter.value() instanceof String || setter.value() instanceof StyleArtifact ) {
            setter.set();
        } else {
            throw new IllegalArgumentException( "Wrong value for " + setter.fullName() + "<String|StyleArtifact>: " + Objects.repr( setter.value() ) );
        }
    }
    
    /* Virtual Properties */
    
    @Virtual
    public Object getStyle(){
        return get( "flavor" );
    }
    
    public void setStyle( Object style ){
        set( "flavor", style );
    }

    /* Bean Accessors */
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "!color" )
            .add( "!background" )
            .add( "!font" )
            .add( "!border" )
            .add( "!style" )
            .toString();
    }
    
}