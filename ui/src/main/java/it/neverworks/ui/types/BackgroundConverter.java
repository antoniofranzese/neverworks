package it.neverworks.ui.types;

import java.util.List;
import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;
import it.neverworks.ui.data.FileArtifact;

public class BackgroundConverter implements Converter {
    
    private ColorConverter colorConverter = new ColorConverter();
    
    public Object convert( Object value ) {
        if( value instanceof Background ) {
            return value;
            
        } else if( Arguments.isParsable( value ) ) {
            return new Background().set( Arguments.parse( value ) );
        
        } else if( value instanceof String || value instanceof List || value instanceof Color ) {
            Object color = colorConverter.convert( value );
            if( color instanceof Color ) {
                return new Background().set( "color", color );
            } else {
                return value;
            }

        } else if( value instanceof FileArtifact ) {
            return new Background().set( "image", value );

        } else if( value instanceof Map ) {
            return new Background().set( Arguments.process( value ) );

        } else {
            return value;
        }
    }
}
    