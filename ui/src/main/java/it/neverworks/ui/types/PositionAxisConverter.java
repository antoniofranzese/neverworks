package it.neverworks.ui.types;

import it.neverworks.model.converters.Converter;

public class PositionAxisConverter implements Converter {
    
    private SizeConverter sizeConverter = new SizeConverter();
    
    public Object convert( Object value ) {
        if( value instanceof Size ) {
            return value;

        } else if( value instanceof String ) {
            String s = ((String) value).trim();
            if( "left".equalsIgnoreCase( s ) ) {
                return Size.percentage( 0 );           
            } else if( "center".equalsIgnoreCase( s ) ) {
                return Size.percentage( 50 );           
            } else if( "right".equalsIgnoreCase( s ) ) {
                return Size.percentage( 100 );           
            } else if( "top".equalsIgnoreCase( s ) ) {
                return Size.percentage( 0 );           
            } else if( "middle".equalsIgnoreCase( s ) ) {
                return Size.percentage( 50 );           
            } else if( "bottom".equalsIgnoreCase( s ) ) {
                return Size.percentage( 100 );           
            } else {
                return sizeConverter.convert( value );
            }

        } else {
            return sizeConverter.convert( value );
        }
    }
}