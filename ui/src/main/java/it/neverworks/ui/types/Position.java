package it.neverworks.ui.types;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;

@Convert( PositionConverter.class )
public class Position extends BaseModel {
    
    @Property @Convert( PositionAxisConverter.class )
    private Size x;
    
    @Property @Convert( PositionAxisConverter.class )
    private Size y;
    
    public Size getY(){
        return this.y;
    }
    
    public void setY( Size y ){
        this.y = y;
    }
    
    public Size getX(){
        return this.x;
    }
    
    public void setX( Size x ){
        this.x = x;
    }
    
}