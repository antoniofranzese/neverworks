package it.neverworks.ui.types;

import java.util.Map;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;

@Watchable
@Convert( BoxConverter.class )
public class Box extends BaseModel {
    
    @Property @AutoConvert
    private Integer top;
    
    @Property @AutoConvert
    private Integer bottom;
    
    @Property @AutoConvert
    private Integer left;
    
    @Property @AutoConvert
    private Integer right;
    
    public Box() {}
        
    public Box( int value ) {
        this.top = value;
        this.bottom = value;
        this.left = value;
        this.right = value;
    }

    public Box( int top, int right, int bottom, int left ) {
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
    }
    
    public Box( Map<String, Object> arguments ) {
        for( String name: arguments.keySet() ) {
            this.set( name, arguments.get( name ) );
        }
    }
    
    @Virtual
    public void setSize( Object value ) {
        set( "top", value );
        set( "bottom", value );
        set( "left", value );
        set( "right", value );
    }
    
    public Integer getRight(){
        return this.right;
    }
    
    public void setRight( Integer right ){
        this.right = right;
    }
    
    public Integer getLeft(){
        return this.left;
    }
    
    public void setLeft( Integer left ){
        this.left = left;
    }
    
    public Integer getBottom(){
        return this.bottom;
    }
    
    public void setBottom( Integer bottom ){
        this.bottom = bottom;
    }
    
    public Integer getTop(){
        return this.top;
    }
    
    public void setTop( Integer top ){
        this.top = top;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "top" )
            .add( "bottom" )
            .add( "left" )
            .add( "right" )
            .toString();
    }
    
    public static Box sum( Box... boxes ) {
        Box result = new Box();
        boolean used = false;
        for( Box box: boxes ) {
            if( box != null ) {
                used = true;
                if( box.left != null ) {
                    if( result.left == null ) {
                        result.left = 0;
                    }
                    result.left += box.left;
                }
                
                if( box.right != null ) {
                    if( result.right == null ) {
                        result.right = 0;
                    }
                    result.right += box.right;
                }
                
                if( box.top != null ) {
                    if( result.top == null ) {
                        result.top = 0;
                    }
                    result.top += box.top;
                }
                
                if( box.bottom != null ) {
                    if( result.bottom == null ) {
                        result.bottom = 0;
                    }
                    result.bottom += box.bottom;
                }
                
            }
        }
        
        return used ? result : null;
    }
}