package it.neverworks.ui.types;

import java.util.Map;
import it.neverworks.model.converters.Converter;

public class DecorationConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Decoration ) {
            return value;
        } else if( value instanceof Map ) {
            return new Decoration( (Map) value );
        } else if( value instanceof String ) {
            return new Decoration().set( "flavor", value );
        } else {
            return value;
        }
    }
}