package it.neverworks.ui.types;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.description.Watchable;

@Watchable
@Convert( SizeConverter.class )
public class Size /* implements Retrieve */ {
    
    public static enum Kind {
         PIXELS( "px", "pixels" )
        ,POINTS( "pt", "points" )
        ,EMS( "em", "ems" )
        ,PERCENTAGE( "%", "percent" )
        ,WIDTH( "vw", "view width" )
        ,HEIGHT( "vh", "view height" )
        ,AUTO( "", "auto" );
        
        private String description;
        private String unit;
        
        Kind( String unit, String description ) {
            this.unit = unit;
            this.description = description;
        }
        
        public String getDescription(){
            return this.description;
        }
        
        public String getUnit(){
            return this.unit;
        }
    }
    
    public static Size pixels( Number value ) {
        return value != null ? new Size( value.intValue(), Kind.PIXELS ) : null;
    }

    public static Size points( Number value ) {
        return value != null ? new Size( value.intValue(), Kind.POINTS ) : null;
    }

    public static Size ems( Number value ) {
        return value != null ? new Size( value.intValue(), Kind.EMS ) : null;
    }

    public static Size percentage( Number value ) {
        return value != null ? new Size( value.intValue(), Kind.PERCENTAGE ) : null;
    }

    public static Size width( Number value ) {
        return value != null ? new Size( value.intValue(), Kind.WIDTH ) : null;
    }
    
    public static Size height( Number value ) {
        return value != null ? new Size( value.intValue(), Kind.HEIGHT ) : null;
    }
    
    public static Size auto() {
        return new Size( 0, Kind.AUTO );
    }
    
    protected int value = 0;
    protected Kind kind = Kind.PIXELS;
    
    protected Size( int value, Kind kind ) {
        setKind( kind );
        setValue( value );
    }

    public boolean isAuto() {
        return this.kind == Kind.AUTO;
    }
    
    public boolean isPhysical() {
        return this.kind != Kind.PERCENTAGE && this.kind != Kind.AUTO;
    }
    
    public int getValue(){
        return this.value;
    }
    
    public void setValue( int value ){
        this.value = value;
    }
    
    public Kind getKind(){
        return this.kind;
    }
    
    public void setKind( Kind kind ){
        if( kind != null ) {
            this.kind = kind;
        } else {
            throw new IllegalArgumentException( "Missing size kind" );
        }
    }
    
    public String toString() {
        return "Size(" + ( Kind.AUTO.equals( kind ) ? "auto" : ( Strings.valueOf( value ) + " "  + kind.getDescription() ) ) + ")";
    }
}