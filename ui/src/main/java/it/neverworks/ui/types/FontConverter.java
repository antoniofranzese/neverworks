package it.neverworks.ui.types;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;

public class FontConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Font ) {
            return value;
        } else if( value instanceof Map ) {
            return new Font( Arguments.process( value ) );
        } else if( value instanceof Number ) {
            return new Font().set( "size", value );
        } else if( value instanceof String  ) {
            if( Font.hasTemplate( (String) value ) ) {
                return new Font( (String) value );
            } else {
                if( Arguments.isParsable( value ) ) {
                    return new Font( Arguments.parse( value ) );
                } else {
                    try {
                        return new Font().set( "size", value );
                    } catch( IllegalArgumentException ex ) {
                        return value;
                    }
                }
            }
        } else {
            return value;
        }
    }
}