package it.neverworks.ui.types;

import it.neverworks.model.types.BaseType;
import it.neverworks.ui.Widget;

public class AnchorType extends BaseType {
    
    public AnchorType(){
        super();
        this.converter = new AnchorConverter();
    }
    
    public boolean isInstance( Object object ) {
        return object instanceof Point || object instanceof Widget;
    }
    
    public String getName() {
        return "Point or Widget";
    }
    
}