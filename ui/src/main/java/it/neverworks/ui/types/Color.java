package it.neverworks.ui.types;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.Formatter;
import java.util.regex.Pattern;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Application;
import it.neverworks.lang.Representable;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.HashCodeBuilder;
import it.neverworks.model.utils.ToStringBuilder;

@Convert( ColorConverter.class )
public class Color implements Representable {
    
    private final static Pattern HEX_COLOR = Pattern.compile( "[0-9A-Fa-f]{6,8}" );
    
    public final static Color TRANSPARENT = new Color( -1, -1, -1, 0 );
    public final static int SOLID = 255;

    private final static float FLOAT_SOLID = 255.0F;

    private final static int R = 0;
    private final static int G = 1;
    private final static int B = 2;

    private final static int H = 0;
    private final static int S = 1;
    private final static int L = 2;
    
    private int red;
    private int green;
    private int blue;
    private int alpha;
    
    public static Color HSL( Number hue, Number saturation, Number lightness ) {
        return HSL( hue, saturation, lightness, 255 );
    }

    public static Color HSL( Number hue, Number saturation, Number lightness, Number alpha ) {
        int[] rgb = HSLtoRGB( 
            Numbers.Float( hue ), 
            Numbers.Float( saturation ), 
            Numbers.Float( lightness )
        );
        return new Color( rgb[ R ], rgb[ G ], rgb[ B ], Numbers.Int( alpha ) );
    }

    public static Color RGB( Number red, Number green, Number blue, Number alpha ) {
        return new Color( 
            Numbers.Int( red ), 
            Numbers.Int( green ), 
            Numbers.Int( blue ), 
            Numbers.Int( alpha ) 
        );
    }

    public static Color RGB( Number red, Number green, Number blue ) {
        return new Color( 
            Numbers.Int( red ), 
            Numbers.Int( green ), 
            Numbers.Int( blue ) 
        );
    }
    
    public Color( int red, int green, int blue ) {
        this( red, green, blue, SOLID );
    }

    public Color( int red, int green, int blue, int alpha ) {
        this.red = Numbers.limit( red, 0, SOLID );
        this.green = Numbers.limit( green, 0, SOLID );
        this.blue = Numbers.limit( blue, 0, SOLID );
        this.alpha = Numbers.limit( alpha, 0, SOLID );
    }
    
    public Color( String color ) {
        if( Strings.hasText( color ) ) {
            color = color.toLowerCase();
            
            if( "transparent".equals( color ) ) {
                this.red = TRANSPARENT.red;
                this.green = TRANSPARENT.green;
                this.blue = TRANSPARENT.blue;
                this.alpha = TRANSPARENT.alpha;

            } else {
                
                if( names != null && names.containsKey( color ) ) {
                    color = (String)names.get( color );
                } else if( color.startsWith( "#" ) ) {
                    color = color.substring( 1 );
                }
            
                if( HEX_COLOR.matcher( color ).matches() ) {
                    this.red   = Integer.decode( "0x" + color.substring( 0, 2 ) );
                    this.green = Integer.decode( "0x" + color.substring( 2, 4 ) );
                    this.blue  = Integer.decode( "0x" + color.substring( 4, 6 ) );
                    this.alpha = color.length() == 8 ? Integer.decode( "0x" + color.substring( 6, 8 ) ) : SOLID;
                } else {
                    throw new IllegalArgumentException( "Invalid color specification: " + color );
                }
            }
            
        } else {
            throw new IllegalArgumentException( "Null or empty color specification" );
        }
    }
    
    public Color( java.awt.Color color ) {
        this( color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha() );
    }
    
    public int getBlue(){
        return this.blue;
    }
    
    public int getGreen(){
        return this.green;
    }
    
    public int getRed(){
        return this.red;
    }
    
    public int getAlpha(){
        return this.alpha;
    }
    
    public float getHue(){
        return RGBtoHSL( red, green, blue )[ H ];
    }
    
    public float getSaturation(){
        return RGBtoHSL( red, green, blue )[ S ];
    }
    
    public float getLightness(){
        return RGBtoHSL( red, green, blue )[ L ];
    }

    public java.awt.Color getAwt() {
        return new java.awt.Color( this.red, this.green, this.blue, this.alpha );
    }

    public java.awt.Color getAWTColor() {
        return getAwt();
    }

    public String getHex() {
        if( this.alpha == Color.SOLID ) {
            return new Formatter().format( "%02x%02x%02x", this.red, this.green, this.blue ).toString();
        } else {
            return new Formatter().format( "%02x%02x%02x%02x", this.red, this.green, this.blue, this.alpha ).toString();
        }
    }

    public Color withRed( int value ) {
        return RGB( value, this.green, this.blue, this.alpha );
    }

    public Color withGreen( int value ) {
        return RGB( this.red, value, this.blue, this.alpha );
    }

    public Color withBlue( int value ) {
        return RGB( this.red, this.green, value, this.alpha );
    }

    public Color withAlpha( int value ) {
        return RGB( this.red, this.green, this.blue, value );
    }

    public Color withHue( float value ){
        float[] hsl = RGBtoHSL( this.red, this.green, this.blue );
        return HSL( value, hsl[ S ], hsl[ L ], this.alpha );
    }
    
    public Color withSaturation( float value ){
        float[] hsl = RGBtoHSL( this.red, this.green, this.blue );
        return HSL( hsl[ H ], value, hsl[ L ], this.alpha );
    }
    
    public Color withLightness( float value ){
        float[] hsl = RGBtoHSL( this.red, this.green, this.blue );
        return HSL( hsl[ H ], hsl[ S ], value, this.alpha );
    }
    
    public Color darken( Number amount ) {
        return withLightness( Math.max( 0, getLightness() - Numbers.Float( amount ) ) );
    }

    public Color darkenBy( Number percent ) {
        return withLightness( Math.round( Math.max( 0, getLightness() * ( 1.0F - Numbers.Float( percent ) ) ) ) );
    }
    
    public Color lighten( Number amount ) {
        return withLightness( Math.min( 100.0f, getLightness() + Numbers.Float( amount ) ) );
    }
    
    public Color lightenBy( Number percent ) {
        return withLightness( Math.round( Math.min( 100.0f, getLightness() * ( 1.0F + Numbers.Float( percent ) ) ) ) );
    }

    public Color desaturate( Number amount ) {
        return withSaturation( Math.max( 0, getSaturation() - Numbers.Float( amount ) ) );
    }

    public Color desaturateBy( Number percent ) {
        return withSaturation( Math.round( Math.max( 0, getSaturation() * ( 1.0F - Numbers.Float( percent ) ) ) ) );
    }
    
    public Color saturate( Number amount ) {
        return withSaturation( Math.min( 100.0f, getSaturation() + Numbers.Float( amount ) ) );
    }

    public Color saturateBy( Number percent ) {
        return withSaturation( Math.round( Math.min( 100.0f, getSaturation() * ( 1.0F + Numbers.Float( percent ) ) ) ) );
    }
    
    public Color shift( Number amount ) {
        return withHue( getHue() + Numbers.Float( amount ) );
    }
    
    public float getLuminance() {
        return new Double( 
            0.2126 * new Integer( red ).doubleValue() 
            + 0.7152 * new Integer( green ).doubleValue() 
            + 0.0722 * new Integer( blue ).doubleValue() 
        ).floatValue();
    }

    public boolean equals( Object other ) {
        if( other instanceof Color ) {
            return this.red == ((Color) other).red
                && this.green == ((Color) other).green
                && this.blue == ((Color) other).blue
                && this.alpha == ((Color) other).alpha;
                
        } else {
            return false;
        }
    }
    

    public int hashCode() {
        return new HashCodeBuilder( this )
            .append( red )
            .append( green )
            .append( blue )
            .toHashCode();
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "hex", getHex() )
            .toString();
    }

    public String toRepresentation() {
        float[] hsl = RGBtoHSL( this.red, this.green, this.blue );
        
        return new ToStringBuilder( this )
            .add( "r", this.red )
            .add( "g", this.green )
            .add( "b", this.blue )
            .add( "h", hsl[ H ] )
            .add( "s", hsl[ S ] )
            .add( "l", hsl[ L ] )
            .add( "hex", getHex() )
            .toString();
    }

    public static int loop( int number, int min, int max ) {
        int distance = max - min + 1;

        if( number > max ) {
            return min + ( number - min ) % distance;
        } else if( number < min ) {
            return max + ( ( number - distance - 1 ) % distance );
        } else {
            return number;
        }
    }

	public static float[] RGBtoHSL( int red, int green, int blue ) {
		float[] rgb = new java.awt.Color( red, green, blue ).getRGBColorComponents( null );
		float r = rgb[0];
		float g = rgb[1];
		float b = rgb[2];

		float min = Math.min(r, Math.min(g, b));
		float max = Math.max(r, Math.max(g, b));
		float h = 0;

		if (max == min) {
			h = 0;
		} else if (max == r) {
			h = ((60 * (g - b) / (max - min)) + 360) % 360;
		} else if (max == g) {
			h = (60 * (b - r) / (max - min)) + 120;
		} else if (max == b) {    
			h = (60 * (r - g) / (max - min)) + 240;
        }                      
		
		float l = (max + min) / 2;
		float s = 0;

		if (max == min) {
			s = 0;       
		} else if (l <= .5f) {
			s = (max - min) / (max + min);
		} else {                  
			s = (max - min) / (2 - max - min);
        }
                
		return new float[] { h, s * 100, l * 100 };
	}
    
	public static int[] HSLtoRGB( float h, float s, float l ) {
        s = Numbers.limit( s, 0f, 100f );
        l = Numbers.limit( l, 0f, 100f );

		h = h % 360.0f;
		h /= 360f;
		s /= 100f;
		l /= 100f;

		float q = 0;

		if (l < 0.5) {
			q = l * (1 + s);
		} else {          
			q = (l + s) - (s * l);
        }        
		
        float p = 2 * l - q;

		float r = Math.max(0, HueToRGB(p, q, h + (1.0f / 3.0f)));
		float g = Math.max(0, HueToRGB(p, q, h));
		float b = Math.max(0, HueToRGB(p, q, h - (1.0f / 3.0f)));

		r = Math.min(r, 1.0f);
		g = Math.min(g, 1.0f);
		b = Math.min(b, 1.0f);

        return new int[]{ floatToRange( r ), floatToRange( g ), floatToRange( b ) };
	}

	private static float HueToRGB( float p, float q, float h ) {
		if (h < 0) h += 1;

		if (h > 1 ) h -= 1;

		if (6 * h < 1) {
			return p + ((q - p) * 6 * h);
		}

		if (2 * h < 1 ) {
			return  q;
		}

		if (3 * h < 2) {
			return p + ( (q - p) * 6 * ((2.0f / 3.0f) - h) );
		}

   		return p;
	}
    
    private static float rangeToFloat( int n ) {
        return Numbers.Float( n / 255.0f );
    }
    
    private static int floatToRange( float n ) {
        return Numbers.Int( 255.0f * n );
    }
    
    private final static Map names = new HashMap();
    
    static {
        for( String source: list( "META-INF/neverworks/colors.properties", "META-INF/application/colors.properties" ) ) {
            Arguments args = Arguments.load( Application.resource( source ) );
            for( String name: args.keys() ) {
                try {
                    Color color = new Color( args.<String>get( name ) );
                    names.put( name, color.getHex() );
                    
                } catch( IllegalArgumentException ex ) {
                    throw new IllegalArgumentException( msg( "Invalid color specification ({0}): {1}={2}", source, name, args.get( name ) ) );
                }
            }
        }
    }
    
    private static Color standard( String name ) {
        try {
            return new Color( name );
        } catch( IllegalArgumentException ex ) {
            throw new IllegalArgumentException( msg( "Invalid standard color specification: {0}", name ) );
        }
    }

    public final static Color ALICEBLUE = standard( "aliceblue" );
    public final static Color ANTIQUEWHITE = standard( "antiquewhite" );
    public final static Color AQUA = standard( "aqua" );
    public final static Color AQUAMARINE = standard( "aquamarine" );
    public final static Color AZURE = standard( "azure" );
    public final static Color BEIGE = standard( "beige" );
    public final static Color BISQUE = standard( "bisque" );
    public final static Color BLACK = standard( "black" );
    public final static Color BLANCHEDALMOND = standard( "blanchedalmond" );
    public final static Color BLUE = standard( "blue" );
    public final static Color BLUEVIOLET = standard( "blueviolet" );
    public final static Color BROWN = standard( "brown" );
    public final static Color BURLYWOOD = standard( "burlywood" );
    public final static Color CADETBLUE = standard( "cadetblue" );
    public final static Color CHARTREUSE = standard( "chartreuse" );
    public final static Color CHOCOLATE = standard( "chocolate" );
    public final static Color CORAL = standard( "coral" );
    public final static Color CORNFLOWERBLUE = standard( "cornflowerblue" );
    public final static Color CORNSILK = standard( "cornsilk" );
    public final static Color CRIMSON = standard( "crimson" );
    public final static Color CYAN = standard( "cyan" );
    public final static Color DARKBLUE = standard( "darkblue" );
    public final static Color DARKCYAN = standard( "darkcyan" );
    public final static Color DARKGOLDENROD = standard( "darkgoldenrod" );
    public final static Color DARKGRAY = standard( "darkgray" );
    public final static Color DARKGREEN = standard( "darkgreen" );
    public final static Color DARKKHAKI = standard( "darkkhaki" );
    public final static Color DARKMAGENTA = standard( "darkmagenta" );
    public final static Color DARKOLIVEGREEN = standard( "darkolivegreen" );
    public final static Color DARKORANGE = standard( "darkorange" );
    public final static Color DARKORCHID = standard( "darkorchid" );
    public final static Color DARKRED = standard( "darkred" );
    public final static Color DARKSALMON = standard( "darksalmon" );
    public final static Color DARKSEAGREEN = standard( "darkseagreen" );
    public final static Color DARKSLATEBLUE = standard( "darkslateblue" );
    public final static Color DARKSLATEGRAY = standard( "darkslategray" );
    public final static Color DARKTURQUOISE = standard( "darkturquoise" );
    public final static Color DARKVIOLET = standard( "darkviolet" );
    public final static Color DEEPPINK = standard( "deeppink" );
    public final static Color DEEPSKYBLUE = standard( "deepskyblue" );
    public final static Color DIMGRAY = standard( "dimgray" );
    public final static Color DODGERBLUE = standard( "dodgerblue" );
    public final static Color FIREBRICK = standard( "firebrick" );
    public final static Color FLORALWHITE = standard( "floralwhite" );
    public final static Color FORESTGREEN = standard( "forestgreen" );
    public final static Color FUCHSIA = standard( "fuchsia" );
    public final static Color GAINSBORO = standard( "gainsboro" );
    public final static Color GHOSTWHITE = standard( "ghostwhite" );
    public final static Color GOLD = standard( "gold" );
    public final static Color GOLDENROD = standard( "goldenrod" );
    public final static Color GRAY = standard( "gray" );
    public final static Color GREEN = standard( "green" );
    public final static Color GREENYELLOW = standard( "greenyellow" );
    public final static Color HONEYDEW = standard( "honeydew" );
    public final static Color HOTPINK = standard( "hotpink" );
    public final static Color INDIANRED = standard( "indianred" );
    public final static Color INDIGO = standard( "indigo" );
    public final static Color IVORY = standard( "ivory" );
    public final static Color KHAKI = standard( "khaki" );
    public final static Color LAVENDER = standard( "lavender" );
    public final static Color LAVENDERBLUSH = standard( "lavenderblush" );
    public final static Color LAWNGREEN = standard( "lawngreen" );
    public final static Color LEMONCHIFFON = standard( "lemonchiffon" );
    public final static Color LIGHTBLUE = standard( "lightblue" );
    public final static Color LIGHTCORAL = standard( "lightcoral" );
    public final static Color LIGHTCYAN = standard( "lightcyan" );
    public final static Color LIGHTGOLDENRODYELLOW = standard( "lightgoldenrodyellow" );
    public final static Color LIGHTGRAY = standard( "lightgray" );
    public final static Color LIGHTGREEN = standard( "lightgreen" );
    public final static Color LIGHTPINK = standard( "lightpink" );
    public final static Color LIGHTSALMON = standard( "lightsalmon" );
    public final static Color LIGHTSEAGREEN = standard( "lightseagreen" );
    public final static Color LIGHTSKYBLUE = standard( "lightskyblue" );
    public final static Color LIGHTSLATEGRAY = standard( "lightslategray" );
    public final static Color LIGHTSTEELBLUE = standard( "lightsteelblue" );
    public final static Color LIGHTYELLOW = standard( "lightyellow" );
    public final static Color LIME = standard( "lime" );
    public final static Color LIMEGREEN = standard( "limegreen" );
    public final static Color LINEN = standard( "linen" );
    public final static Color MAGENTA = standard( "magenta" );
    public final static Color MAROON = standard( "maroon" );
    public final static Color MEDIUMAQUAMARINE = standard( "mediumaquamarine" );
    public final static Color MEDIUMBLUE = standard( "mediumblue" );
    public final static Color MEDIUMORCHID = standard( "mediumorchid" );
    public final static Color MEDIUMPURPLE = standard( "mediumpurple" );
    public final static Color MEDIUMSEAGREEN = standard( "mediumseagreen" );
    public final static Color MEDIUMSLATEBLUE = standard( "mediumslateblue" );
    public final static Color MEDIUMSPRINGGREEN = standard( "mediumspringgreen" );
    public final static Color MEDIUMTURQUOISE = standard( "mediumturquoise" );
    public final static Color MEDIUMVIOLETRED = standard( "mediumvioletred" );
    public final static Color MIDNIGHTBLUE = standard( "midnightblue" );
    public final static Color MINTCREAM = standard( "mintcream" );
    public final static Color MISTYROSE = standard( "mistyrose" );
    public final static Color MOCCASIN = standard( "moccasin" );
    public final static Color NAVAJOWHITE = standard( "navajowhite" );
    public final static Color NAVY = standard( "navy" );
    public final static Color OLDLACE = standard( "oldlace" );
    public final static Color OLIVE = standard( "olive" );
    public final static Color OLIVEDRAB = standard( "olivedrab" );
    public final static Color ORANGE = standard( "orange" );
    public final static Color ORANGERED = standard( "orangered" );
    public final static Color ORCHID = standard( "orchid" );
    public final static Color PALEGOLDENROD = standard( "palegoldenrod" );
    public final static Color PALEGREEN = standard( "palegreen" );
    public final static Color PALETURQUOISE = standard( "paleturquoise" );
    public final static Color PALEVIOLETRED = standard( "palevioletred" );
    public final static Color PAPAYAWHIP = standard( "papayawhip" );
    public final static Color PEACHPUFF = standard( "peachpuff" );
    public final static Color PERU = standard( "peru" );
    public final static Color PINK = standard( "pink" );
    public final static Color PLUM = standard( "plum" );
    public final static Color POWDERBLUE = standard( "powderblue" );
    public final static Color PURPLE = standard( "purple" );
    public final static Color RED = standard( "red" );
    public final static Color ROSYBROWN = standard( "rosybrown" );
    public final static Color ROYALBLUE = standard( "royalblue" );
    public final static Color SADDLEBROWN = standard( "saddlebrown" );
    public final static Color SALMON = standard( "salmon" );
    public final static Color SANDYBROWN = standard( "sandybrown" );
    public final static Color SEAGREEN = standard( "seagreen" );
    public final static Color SEASHELL = standard( "seashell" );
    public final static Color SIENNA = standard( "sienna" );
    public final static Color SILVER = standard( "silver" );
    public final static Color SKYBLUE = standard( "skyblue" );
    public final static Color SLATEBLUE = standard( "slateblue" );
    public final static Color SLATEGRAY = standard( "slategray" );
    public final static Color SNOW = standard( "snow" );
    public final static Color SPRINGGREEN = standard( "springgreen" );
    public final static Color STEELBLUE = standard( "steelblue" );
    public final static Color TAN = standard( "tan" );
    public final static Color TEAL = standard( "teal" );
    public final static Color THISTLE = standard( "thistle" );
    public final static Color TOMATO = standard( "tomato" );
    public final static Color TURQUOISE = standard( "turquoise" );
    public final static Color VIOLET = standard( "violet" );
    public final static Color WHEAT = standard( "wheat" );
    public final static Color WHITE = standard( "white" );
    public final static Color WHITESMOKE = standard( "whitesmoke" );
    public final static Color YELLOW = standard( "yellow" );
    public final static Color YELLOWGREEN = standard( "yellowgreen" );
    
}