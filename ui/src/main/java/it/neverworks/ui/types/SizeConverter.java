package it.neverworks.ui.types;

import it.neverworks.model.converters.Converter;

public class SizeConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Number ) {
            return Size.pixels( ((Number) value).intValue() );

        } else if( value instanceof String ) {
            String str = ((String) value).trim();

            if( str.endsWith( "%" ) ) {
                try {
                    return Size.percentage( Integer.parseInt( str.substring( 0, str.length() - 1 ) ) );
                } catch( NumberFormatException ex ) {
                    return value;
                }

            } else if( "auto".equalsIgnoreCase( str ) ) {
                return Size.auto();

            } else if( "full".equalsIgnoreCase( str ) ) {
                return Size.percentage( 100 );

            } else if( "half".equalsIgnoreCase( str ) ) {
                return Size.percentage( 50 );

            } else if( "quarter".equalsIgnoreCase( str ) ) {
                return Size.percentage( 25 );

            } else {
                try {

                    if( str.endsWith( "px" ) ) {
                        str = str.substring( 0, str.length() - 2 );
                        return Size.pixels( Integer.parseInt( str ) );

                    } else if( str.endsWith( "pt" ) ) {
                        str = str.substring( 0, str.length() - 2 );
                        return Size.points( Integer.parseInt( str ) );

                    } else if( str.endsWith( "em" ) ) {
                        str = str.substring( 0, str.length() - 2 );
                        return Size.ems( Integer.parseInt( str ) );

                    } else if( str.endsWith( "vw" ) ) {
                        str = str.substring( 0, str.length() - 2 );
                        return Size.width( Integer.parseInt( str ) );

                    } else if( str.endsWith( "vh" ) ) {
                        str = str.substring( 0, str.length() - 2 );
                        return Size.height( Integer.parseInt( str ) );

                    } else {
                        return Size.pixels( Integer.parseInt( str ) );

                    }

                } catch( NumberFormatException ex ) {
                    return value;
                }
            }

        } else {
            return value;
        }
    }
}