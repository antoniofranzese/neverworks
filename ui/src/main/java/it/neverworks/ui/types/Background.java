package it.neverworks.ui.types;

import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.data.FileArtifact;

@Watchable
@Convert( BackgroundConverter.class )
public class Background extends BaseModel {
    
    @AutoConvert
    public static enum Repeat {
        NONE, HORIZONTAL, VERTICAL, BOTH
    }
    
    @Property
    private Color color;
    
    @Property
    private FileArtifact image;
    
    @Property 
    private Repeat repeat;
    
    @Property @Watch
    private Position position;
    
    @Property @AutoConvert
    private Boolean scrollable;
    
    public Background() {
        super();
    }
    
    public Background( Arguments arguments ) {
        this();
        this.set( arguments );
    }
    
    /* Virtual Properties */
    
    @Virtual
    public Size getY(){
        return get( "!position.y" );
    }
    
    public void setY( Size y ){
        set( "!position.y", y );
    }
    
    @Virtual
    public Size getX(){
        return get( "!position.x" );
    }
    
    public void setX( Size x ){
        set( "!position.x", x );
    }
    
    /* Bean Accessors */
    
    public Boolean getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( Boolean scrollable ){
        this.scrollable = scrollable;
    }
    
    public Position getPosition(){
        return this.position;
    }
    
    public void setPosition( Position position ){
        this.position = position;
    }

    public Repeat getRepeat(){
        return this.repeat;
    }
    
    public void setRepeat( Repeat repeat ){
        this.repeat = repeat;
    }
    
    public FileArtifact getImage(){
        return this.image;
    }
    
    public void setImage( FileArtifact image ){
        this.image = image;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "!color" )
            .add( "!image" )
            .add( "!repeat" )
            .add( "!position" )
            .add( "!scrollable" )
            .toString();
    }
}