package it.neverworks.ui;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.description.Defended;
import it.neverworks.ui.Widget;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.Contained;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.BuildContentEvent;
import it.neverworks.ui.lifecycle.WidgetAddedEvent;
import it.neverworks.ui.lifecycle.WidgetRemovedEvent;
import it.neverworks.ui.lifecycle.WidgetAbandonedEvent;
import it.neverworks.ui.lifecycle.WidgetGainEvent;
import it.neverworks.ui.lifecycle.WidgetLoseEvent;
import it.neverworks.ui.lifecycle.ContainerActivationEvent;
import it.neverworks.ui.lifecycle.CollectorJoinEvent;
import it.neverworks.ui.lifecycle.CollectorLeaveEvent;
import it.neverworks.ui.lifecycle.NotifyEvent;
import it.neverworks.ui.lifecycle.EventChannel;

public abstract class BaseParentWidget extends BaseWidget {
    
    @Property @Defended
    protected boolean joined = false;

    @Property @Defended
    private Signal<WidgetGainEvent> gain;
    
    @Property @Defended
    private Signal<WidgetLoseEvent> lose;
    
    protected void joinWidget( Widget widget, String canonicalName ) {
        // Assegna un nome pseudo-casuale se il widget e' anonimo
        if( ! Strings.hasText( widget.<String>get( "name" ) ) ) {
            widget.set( "name", generateUniqueName( widget ) );
        }

        if( widget instanceof Contained ) {
            ((Contained) widget).joinParent( this );
        } else {
            widget.set( "parent", this );
        }
        widget.set( "canonicalName", canonicalName );

        // Propaga lo stato di costruzione
        if( this.built ) {
            ((WidgetLifeCycle) widget).spreadIn( new BuildContentEvent( this, "Widget added" ) );
        }
        
        // Propaga lo stato di attivazione
        if( this.active ) {
            ((WidgetLifeCycle) widget).spreadIn( new ContainerActivationEvent( this, "Widget added" ) );
        }

        if( this.model().watching() ) {
            widget.model().watch();
        }
        
        // Notifica ai propri contenitori l'aggiunta di un widget in un attributo
        this.bubble( new WidgetAddedEvent( this, widget ) );

        WidgetGainEvent gain = new WidgetGainEvent( this, widget );
        if( this.started ) {
            signal( "gain" ).fire( gain );
        } else {
            on( "startup", slot( evt -> signal( "gain" ).fire( gain ) ).once() );
        }
        
    }

    protected void leaveWidget( Widget widget ) {
        if( widget instanceof Contained ) {
            ((Contained) widget).leaveParent( this );
        } else {
            widget.set( "parent", null );
        }
        
        // Notifica ai propri contenitori la rimozione di un widget da un attributo
        this.bubble( new WidgetRemovedEvent( this, widget ) );
        
        // Se il contenitore e' persistente, notifica al widget che non ha piu' una rappresentazione client
        if( this.persistent ) {
            ((WidgetLifeCycle) widget).notify( new WidgetAbandonedEvent( this, widget, "Removed from container" ) );
        }
        
        signal( "lose" ).fire( new WidgetLoseEvent( this, widget ) );
    }
    
    @Override
    protected void handleCollectorJoinEvent( CollectorJoinEvent event ) {
        this.joined = true;
        if( event.getWidget() == this ) {
            joinCollector();
        }
        super.handleCollectorJoinEvent( event );
    }
    
    @Override
    protected void handleCollectorLeaveEvent( CollectorLeaveEvent event ) {
        if( event.getWidget() == this ) {
            leaveCollector();
        }
        this.joined = false;
        super.handleCollectorLeaveEvent( event );
    }
    
    protected void joinCollector(){}
    protected void leaveCollector(){}
    
    public void build() {
        
    }
    
    public Signal<WidgetLoseEvent> getLose(){
        return this.lose;
    }
    
    public void setLose( Signal<WidgetLoseEvent> lose ){
        this.lose = lose;
    }
    
    public Signal<WidgetGainEvent> getGain(){
        return this.gain;
    }
    
    public void setGain( Signal<WidgetGainEvent> gain ){
        this.gain = gain;
    }
    
    
}