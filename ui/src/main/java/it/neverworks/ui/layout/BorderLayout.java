package it.neverworks.ui.layout;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.collections.Adder;
import it.neverworks.model.collections.Remover;
import it.neverworks.model.Property;

import it.neverworks.ui.Widget;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.model.events.Event;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.util.WidgetArrayList;
import it.neverworks.ui.GenericContainer;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.WidgetSelection;
import it.neverworks.ui.SwapCapable;

public class BorderLayout extends BaseLayoutWidget implements GenericContainer, BorderLayoutCapable, TitleCapable, SwapCapable {
    
    public static class Frame {
        private Widget widget;
        private String region;
        private String property;

        private Frame( String region, String property, Widget widget ) {
            this.widget = widget;
            this.region = region;
            this.property = property;
        }
        
        public String getRegion(){
            return this.region;
        }

        public String getProperty(){
            return this.property;
        }
        
        public Widget getWidget(){
            return this.widget;
        }
        
        public String toString() {
            return new ToStringBuilder( this )
                .add( "region" )
                .add( "property" )
                .add( "widget" )
                .toString();
        }
    }
    
    public final static List<String> REGIONS = list(  "top", "left", "center", "right", "bottom" );
    
    @AutoConvert
    public enum Design {
        HEADLINE, SIDEBAR
    }
    
    public BorderLayout() {
        super();
    }
    
    public BorderLayout( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    @Property @Collection( adder = "addRegionChild", remover = "removeRegionChild", wrap = true )
    private List<Widget> tops;
    
    @Property @Collection( adder = "addRegionChild", remover = "removeRegionChild", wrap = true )
    private List<Widget> bottoms;
    
    @Property @Collection( adder = "addRegionChild", remover = "removeRegionChild", wrap = true )
    private List<Widget> lefts;
    
    @Property @Collection( adder = "addRegionChild", remover = "removeRegionChild", wrap = true )
    private List<Widget> rights;
    
    @Property
    private Widget center;
    
    @Property
    private boolean gutters = false;

    @Property 
    private Design design = Design.HEADLINE;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Background background;
    
    @Property
    private Color color;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Font font;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Border border;

    @Property
    private String title;
    
    @Property
    private LayoutInfo layout;
    
    public List<Widget> getChildren() {
        return list()
             .cat( tops )
             .cat( lefts )
             .append( center )
             .cat( rights )
             .cat( bottoms )
             .as( Widget.class );
    }
    
    public void addWidget( Widget widget ) {
        String region = detectRegion( widget );
        if( "center".equals( region ) ) {
            set( "center", widget );
        } else {
            value( region + "s" ).add( widget );
        }
    }
    
    public void removeWidget( Widget widget ) {
        String region = detectRegion( widget );
        if( "center".equals( region ) ) {
            if( get( "center" ) == widget ) {
                set( "center", null );
            } else {
                throw new WidgetException( "Widget is not in center region: " + widget.getCommonName() );
            }
        } else {
            List<Widget> collection = get( region + "s" );
            if( collection != null && collection.contains( widget ) ) {
                collection.remove( widget );
            } else {
                throw new WidgetException( "Widget is not in " + region + " region: " + widget.getCommonName() );
             }
        }
    }

    protected String detectRegion( Widget widget ) {
        String region = null;
        if( widget instanceof LayoutCapable ) {
            region = Strings.safe( widget.get( "!layout.region", "center" ) ).trim().toLowerCase();
        }
        if( REGIONS.contains( region ) ) {
            return region;
        } else if( empty( region ) ) {
            return "center";
        } else {
            throw new WidgetException( "Invalid region '" + region + "' for " + widget.getCommonName() + ": " + region );
        }
    }

    public WidgetSelection<Widget> children() {
        return new WidgetArrayList<Widget>( getChildren() );
    }

    protected void setCenter( Setter<Widget> setter ) {
        leaveChild( setter.raw() );
        if( setter.value() != null ) {
            joinChild( setter.value(), setter.name() );
        }
        setter.set();
    }
    
    protected void addRegionChild( Adder<Widget> adder ) {
        if( adder.item() != null ) {
            adder.add();
            joinChild( adder.item(), adder.name().substring( 0, adder.name().length() - 1 ) );
            model().touch( adder.name() );
        } else {
            adder.decline();
        }
    }
    
    protected void removeRegionChild( Remover<Widget> remover ) {
        leaveChild( remover.item() );
        remover.remove();
        model().touch( remover.name() );
    }

    protected void joinChild( Widget child, String name ) {
        if( child != null ) {
            joinWidget( child, "" );

            if( child instanceof BorderLayoutCapable ) {
                child.set( "!layout.region", name );
            }
        }
    }
    
    protected void leaveChild( Widget child ) {
        if( child != null ) {
            if( child instanceof BorderLayoutCapable ) {
                child.set( "!layout.region", null );
            }

            leaveWidget( child );
        }
    }
    
    public void swapChild( Widget source, Widget destination ) {
        if( source.equals( this.center ) ) {
            set( "center", destination );
        } else {
            List<Widget> region = null;
            for( String name: list(  "tops", "lefts", "rights", "bottoms" ) ) {
                if( this.<List>get( name ).contains( source ) ) {
                    region = get( name );
                    break;
                }
            }
            if( region != null ) {
                int index = region.indexOf( source );
                region.remove( index );
                region.add( index, destination );
            } else {
                throw new WidgetException( "Widget is not contained in this BorderLayout: " + source.getCommonName() );
            }
        }
    }
    
    protected void spreadOut( Event event ) {
        for( Widget child: children() ) {
            ((WidgetLifeCycle) child).spreadIn( event );
        }
        super.spreadOut( event );
    }
    
    protected void leaveCollector() {
        for( Widget child: children() ) {
            leaveWidget( child );
        }
        
    }

    
    /* Virtual Properties */

    @Virtual
    public List<Frame> getInventory() {
        List<Frame> result = new ArrayList<Frame>();

        if( this.tops != null ) {
            for( Widget widget: this.tops ) {
                result.add( new Frame( "top", "tops", widget ) );
            }
        }

        if( this.lefts != null ) {
            for( Widget widget: this.lefts ) {
                result.add( new Frame( "left", "lefts", widget ) );
            }
        }

        if( this.center != null ) {
            result.add( new Frame( "center", "center", this.center ) );
        }

        if( this.rights != null ) {
            for( Widget widget: this.rights ) {
                result.add( new Frame( "right", "rights", widget ) );
            }
        }

        if( this.bottoms != null ) {
            for( Widget widget: this.bottoms ) {
                result.add( new Frame( "bottom", "bottoms", widget ) );
            }
        }

        return result;
    }
    
    @Virtual
    public Widget getRight(){
        return this.rights != null && this.rights.size() > 0 ? this.rights.get( 0 ) : null;
    }
    
    public void setRight( Widget right ){
        set( "rights", list( right ) );
    }
    
    @Virtual
    public Widget getLeft(){
        return this.lefts != null && this.lefts.size() > 0 ? this.lefts.get( 0 ) : null;
    }
    
    public void setLeft( Widget left ){
        set( "lefts", list( left ) );
    }
    
    @Virtual
    public Widget getBottom(){
        return this.bottoms != null && this.bottoms.size() > 0 ? this.bottoms.get( 0 ) : null;
    }
    
    public void setBottom( Widget bottom ){
        set( "bottoms", list( bottom ) );
    }
    
    @Virtual
    public Widget getTop(){
        return this.tops != null && this.tops.size() > 0 ? this.tops.get( 0 ) : null;
    }
    
    public void setTop( Widget top ){
        set( "tops", list( top ) );
    }
    

    /* Bean accessors */

    public List<Widget> getRights(){
        return this.rights;
    }
    
    public void setRights( List<Widget> rights ){
        this.rights = rights;
    }
    
    public List<Widget> getLefts(){
        return this.lefts;
    }
    
    public void setLefts( List<Widget> lefts ){
        this.lefts = lefts;
    }
    
    public List<Widget> getBottoms(){
        return this.bottoms;
    }
    
    public void setBottoms( List<Widget> bottoms ){
        this.bottoms = bottoms;
    }
    
    public List<Widget> getTops(){
        return this.tops;
    }
    
    public void setTops( List<Widget> tops ){
        this.tops = tops;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public Widget getCenter(){
        return this.center;
    }
    
    public void setCenter( Widget center ){
        this.center = center;
    }
    
    public boolean getGutters(){
        return this.gutters;
    }
    
    public void setGutters( boolean gutters ){
        this.gutters = gutters;
    }
    
    public Design getDesign(){
        return this.design;
    }
    
    public void setDesign( Design design ){
        this.design = design;
    }
    
    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
}