package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Watch;
import it.neverworks.ui.Widget;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Box;

public class TabLayout extends AccordionLayout {
    
    public enum Position {
        TOP, BOTTOM;
    }
    
    @Property @AutoConvert
    private Position position;
    
    @Property
    private Border border;
    
    @Property @AutoConvert
    private Boolean nested;
    
    @Property @Watch
    protected TabContent content;
    
    @Property @Watch
    private TabStrip strip;
    
    public TabLayout() {
        super();
    }

    public TabLayout( Arguments arguments ) {
        super();
        set( arguments );
    }

    public TabLayout( Widget... widgets ) {
        super( widgets );
    }

    public TabLayout( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    public Position getPosition(){
        return this.position;
    }
    
    public void setPosition( Position position ){
        this.position = position;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public Boolean getNested(){
        return this.nested;
    }
    
    public void setNested( Boolean nested ){
        this.nested = nested;
    }
    
    public TabStrip getStrip(){
        return this.strip;
    }
    
    public void setStrip( TabStrip strip ){
        this.strip = strip;
    }
    
    public TabContent getContent(){
        return this.content;
    }
    
    public void setContent( TabContent content ){
        this.content = content;
    }

}