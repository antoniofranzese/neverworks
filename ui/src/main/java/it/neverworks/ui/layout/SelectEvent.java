package it.neverworks.ui.layout;

import it.neverworks.ui.Widget;
import it.neverworks.ui.Container;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;

public class SelectEvent<T extends StackLayout> extends UIEvent<T> {
    
    @Property @AutoConvert
    private int old;
    
    @Property @AutoConvert
    private int new_;
    
    public int getNew(){
        return this.new_;
    }
    
    public void setNew( int new_ ){
        this.new_ = new_;
    }
    
    public int getOld(){
        return this.old;
    }
    
    public void setOld( int old ){
        this.old = old;
    }
    
    public Widget getSelected() {
        return this.new_ >= 0 ? this.getSource().widget( this.new_ ) : null;
    }

    public Widget getDeselected() {
        return this.old >= 0 ? this.getSource().widget( this.old ) : null;
    }
 
    public void select() {
        this.getSource().select( this.new_ );
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "old" ).add( "new" ).toString();
    }
}