package it.neverworks.ui.layout;

import it.neverworks.model.Property;
import it.neverworks.lang.Arguments;
import it.neverworks.ui.Widget;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Color;

public class FloatingLayout extends StackLayout {
    
    @Property
    private Border border;
    
    @Property
    private Color background;
    
    public FloatingLayout() {
        super();
    }

    public FloatingLayout( Arguments arguments ) {
        super();
        set( arguments );
    }

    public FloatingLayout( Widget... widgets ) {
        super( widgets );
    }

    public FloatingLayout( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }

    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
}