package it.neverworks.ui.layout;

public interface PanelCapable {
    String getTitle();
    boolean isClosable();
}