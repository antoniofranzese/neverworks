package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class LabelPanel extends Panel {
    
    public enum Position {
        TOP, BOTTOM, LEFT, RIGHT
    }
    
    public enum Alignment {
        TOP, BOTTOM, LEFT, RIGHT, CENTER, MIDDLE
    }
    
    @Property @AutoConvert
    private Position position;
    
    @Property @AutoConvert
    private Alignment alignment;
    
    public LabelPanel() {
        super();
    }
    
    public LabelPanel( Arguments arguments ) {
        super();
        set( arguments );
    }

    public boolean isStructural() {
        return false;
    }

    public Alignment getAlignment(){
        return this.alignment;
    }
    
    public void setAlignment( Alignment alignment ){
        this.alignment = alignment;
    }
    
    public Position getPosition(){
        return this.position;
    }
    
    public void setPosition( Position position ){
        this.position = position;
    }
}