package it.neverworks.ui.layout;

import it.neverworks.ui.lifecycle.UIEvent;

public class ExpandoToggleEvent extends UIEvent {
    
    private boolean expanding;
    
    public boolean getExpanding(){
        return this.expanding;
    }
    
    public void setExpanding( boolean expanding ){
        this.expanding = expanding;
    }
    
    public boolean getCollapsing(){
        return ! this.expanding;
    }
    
}