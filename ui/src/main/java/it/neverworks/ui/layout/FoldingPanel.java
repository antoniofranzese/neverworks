package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.types.Size;

public class FoldingPanel extends ExpandoPanel {

    @Property
    private Size fold;
    
    public FoldingPanel() {
        super();
    }
    
    public FoldingPanel( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public Size getFold(){
        return this.fold;
    }
    
    public void setFold( Size fold ){
        this.fold = fold;
    }
}