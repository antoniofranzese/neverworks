package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Subscription;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.Property;
import it.neverworks.ui.Widget;

public class LazyPanel extends Panel {

    @Property @Defended
    private Widget lazyContent;
    
    @Property @Defended
    protected Subscription parentSelection;

    @Property @Defended
    protected boolean drawn = false;
    
    public LazyPanel() {
        super();
        on( "startup", slot( "startup" ).unsigned() );
    }
    
    public LazyPanel( Arguments arguments ) {
        this();
        set( arguments );
    }

    public LazyPanel( Arguments arguments, Widget content ) {
        this();
        set( arguments );
        set( "lazyContent", content );
    }
    
    protected Widget getContent( Getter<Widget> getter ) {
        if( ! this.drawn ) {
            this.draw();
        }
        return getter.get();
    }
    
    protected void setContent( Setter<Widget> setter ) throws Exception {
        if( this.drawn ) {
            super.setContent( setter );
        } else {
            set( "lazyContent", setter.value() );
            setter.decline();
        }
    }
    
    protected void setParent( Setter<Widget> setter ) {
        if( this.parentSelection != null ) {
            this.parentSelection.remove();
        }

        super.setParent( setter );

        if( setter.value() instanceof Selector ) {
            this.parentSelection = on( "parent.select", slot( "select" ) );

        } else {
            this.draw();

        }

    }
    
    protected void startup() {
        if( this.equals( get( "!parent.selected" ) ) ) {
            this.draw();
        }
    }
    
    protected void select( SelectEvent event ) {
        if( this.equals( event.get( "selected" ) ) ) {
            draw();
        } 
    }
    
    protected void draw() {
        set( "drawn", true );
        if( this.lazyContent != null ) {
            set( "content", get( "lazyContent" ) );
            set( "lazyContent", null );
        }
    }
}