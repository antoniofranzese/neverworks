package it.neverworks.ui.layout;

public interface TitleCapable {
    String getTitle();
    void setTitle( String title );
}