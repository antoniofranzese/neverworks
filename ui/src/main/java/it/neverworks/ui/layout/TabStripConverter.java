package it.neverworks.ui.layout;

import it.neverworks.lang.Booleans;
import it.neverworks.model.converters.ModelConverter;

public class TabStripConverter extends ModelConverter {

    public Object convert( Object value ) {
        if( Booleans.isBoolean( value ) ) {
            return new TabStrip().set( "visible", Booleans.bool( value ) );
        } else {
            return super.convert( value );
        }
    }

}