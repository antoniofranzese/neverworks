package it.neverworks.ui.layout;

public interface VisibilityCapable {
    
    boolean getVisible();
    void setVisible( boolean visible );
    
}