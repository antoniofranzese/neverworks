package it.neverworks.ui.layout;

import static it.neverworks.language.*;

import java.util.List;
import java.util.Collection;
import java.io.InputStream;
import java.io.Reader;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.LocalConvert;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Scroll;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.types.Scrollability;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.SwapCapable;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.Widget;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.layout.StructureCapable;
import it.neverworks.ui.dnd.DropAcceptance;
import it.neverworks.ui.dnd.DropTarget;
import it.neverworks.ui.dnd.DropEvent;
import it.neverworks.ui.util.WidgetArrayList;
import it.neverworks.ui.WidgetSelection;
import it.neverworks.ui.GenericContainer;

public class Panel extends BaseLayoutWidget implements GenericContainer, BorderLayoutCapable, StructureCapable, PanelCapable, TitleCapable, CloseCapable, SwapCapable, OriginCapable, DropTarget {
    
    @Property @LocalConvert
    protected Widget content;
    
    @Property
    protected LayoutInfo layout;
    
    @Property @AutoConvert
    protected boolean closable = false;

    @Property
    protected String title;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    protected Background background;
    
    @Property
    protected Point origin;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    protected Border border;
    
    @Property
    protected DropAcceptance acceptance;
    
    @Property
    protected Signal<DropEvent> drop;
    
    @Property
    private Scrollability scrollable;
    
    @Property @Watch
    private Scroll scroll;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Font font;
    
    @Property
    private Color color;
    
    @Property
    protected Corners rounding;

    @Property
    protected Shadow shadow;

    @Property
    protected Boolean displaced;
    
    public Panel() {
        super();
    }
    
    public Panel( Arguments arguments ) {
        super();
        set( arguments );
    }

    public Panel( Arguments arguments, Widget content ) {
        super();
        set( arguments );
        set( "content", content );
    }
    
    public List<Widget> getChildren() {
        return ( content == null ? list() : list( content ) ).as( Widget.class );
    }
    
    public void addWidget( Widget widget ) {
        set( "content", widget );
    }
    
    public void removeWidget( Widget widget ) {
        if( widget == content ) {
            set( "content", null );
        } else {
            throw new WidgetException( "Widget is not " + this.getClass().getSimpleName() + " content: " + widget.getCommonName() );
        }
    }

    public WidgetSelection<Widget> children() {
        return new WidgetArrayList<Widget>( content );
    }

    public <T> WidgetSelection<T> children( Class<T> type ) {
        return type.isInstance( content ) ? new WidgetArrayList<T>( (T) content ) : new WidgetArrayList<T>();
    }

    protected Object convertContent( Object value ) {
        if( value instanceof Widget ) {
            return value;
        } else if( value instanceof String || value instanceof InputStream || value instanceof Reader ) {
            return new Label()
                .set( "value", value )
                .set( "wrap", true )
                .set( "encoding", "html" );
        } else {
            return value;
        }
    }

    protected void setContent( Setter<Widget> setter ) throws Exception {
        // System.out.println( "Setting panel content " + setter.value().getCommonName() );
        if( setter.raw() != null ) {
            leaveWidget( setter.raw() );
        }

        setter.set();
        
        //In caso di errori di build o attivazione, annulla l'impostazione
        if( setter.value() != null ) {
            try {
                joinWidget( setter.value(), "content" );
            } catch( Exception ex ) {
                setter.set( null );
                throw ex;
            }
        }

    }
    
    protected void spreadOut( Event event ) {
        if( content != null ) {
            ((WidgetLifeCycle) content).spreadIn( event );
        }
        super.spreadOut( event );
    }

    public void swapChild( Widget source, Widget destination ) {
        if( getContent() == source ) {
            setContent( destination ); 
        } else {
            throw new WidgetException( "Cannot swap widget, " + ( source != null ? source.getCommonName() : "null" ) + " is not the current content, current is " + ( getContent() != null ? getContent().getCommonName() : "null" ) );
        }
    }
    
    protected void leaveCollector(){
        if( this.content != null ) {
            leaveWidget( this.content );
        }
    }

    // protected LayoutInfo getLayout( Getter<LayoutInfo> getter ) {
    //     if( getter.undefined() ) {
    //         getter.setRaw( new LayoutInfo() );
    //     }
    //
    //     return getter.get();
    // }
    //
    
    protected void setLayout( Setter<LayoutInfo> setter ) {
        if( setter.defined() ) {
            setter.value().mixin( setter.raw() );
        }
        setter.set();
    }
    
    protected void setTitle( Setter<String> setter ) {
        setter.set();
        set( "!layout.title", setter.value() );
    }

    protected void setClosable( Setter<Boolean> setter ) {
        setter.set();
        set( "!layout.closable", setter.value() );
    }

    protected void setOrigin( Setter<Point> setter ) {
        setter.set();
        set( "!layout.x", setter.value() != null ? setter.value().getX() : null );
        set( "!layout.y", setter.value() != null ? setter.value().getY() : null );
    }
    
    public void setLayoutProperty( String name, Object value ) {
        if( name.equals( "title" ) ) {
            model().direct( "title", value );

        } else if( name.equals( "closable" ) ) {
            model().direct( "closable", value );

        } else if( name.equals( "x" ) ) {
            if( model().undefined( "origin" ) ) {
                model().direct( "origin", new Point() );
            }
            set( "origin.x", value );

        } else if( name.equals( "y" ) ) {
            if( model().undefined( "origin" ) ) {
                model().direct( "origin", new Point() );
            }
            set( "origin.y", value );
        }
    }


    public boolean isStructural() {
        return true;
    }

    /* Virtual Properties */
    
    @Virtual
    public boolean getEmpty(){
        return get( "content" ) == null;
    }
    
    /* Bean Accessors */
    
    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }
    
    public Corners getRounding(){
        return this.rounding;
    }
    
    public void setRounding( Corners rounding ){
        this.rounding = rounding;
    }

    public Widget getContent(){
        return this.content;
    }
    
    public void setContent( Widget content ){
        this.content = content;
    }
    
    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }
    
    public boolean getClosable(){
        return this.closable;
    }
    
    public void setClosable( boolean closable ){
        this.closable = closable;
    }
    
    public boolean isClosable() {
        return this.closable;
    }
    
    public Point getOrigin(){
        return this.origin;
    }
    
    public void setOrigin( Point origin ){
        this.origin = origin;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public DropAcceptance getAcceptance(){
        return this.acceptance;
    }
    
    public void setAcceptance( DropAcceptance acceptance ){
        this.acceptance = acceptance;
    }
    
    public Signal<DropEvent> getDrop(){
        return this.drop;
    }
    
    public void setDrop( Signal<DropEvent> drop ){
        this.drop = drop;
    }
    
    public Scrollability getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( Scrollability scrollable ){
        this.scrollable = scrollable;
    }
    
    public Scroll getScroll(){
        return this.scroll;
    }
    
    public void setScroll( Scroll scroll ){
        this.scroll = scroll;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Boolean getDisplaced(){
        return this.displaced;
    }
    
    public void setDisplaced( Boolean displaced ){
        this.displaced = displaced;
    }


}