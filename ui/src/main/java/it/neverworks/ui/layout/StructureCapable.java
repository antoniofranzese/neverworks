package it.neverworks.ui.layout;
    
public interface StructureCapable {
    public boolean isStructural();
}