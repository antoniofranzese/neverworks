package it.neverworks.ui.layout;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Event;
import it.neverworks.ui.WidgetException;

public class ExpandoPanel extends Panel {
    
    @Property
    private boolean expanded = true;
    
    @Property
    private Signal<ExpandoToggleEvent> toggle;
    
    @Property
    private Signal<Event> expand;
    
    @Property
    private Signal<Event> collapse;
    
    // @Property
    // private Signal<ExpandoToggleEvent> expand;
    //
    // @Property
    // private Signal<ExpandoToggleEvent> collapse;
    
    public ExpandoPanel() {
        super();
    }
    
    public ExpandoPanel( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    // protected void handleToggle( Handler<ExpandoToggleEvent> handler ) {
    //     handler.handle();
    //     if( handler.event().getExpanding() ) {
    //         getExpand().fire( handler.event() );
    //     } else {
    //         getCollapse().fire( handler.event() );
    //     }
    // }
    
    protected void setParent( Setter setter ) {
        if( setter.value() == null || setter.value() instanceof BorderLayout ) {
            super.setParent( setter );
        } else {
            throw new WidgetException( "ExpandoPanel must be placed in a BorderLayout" );
        }
    }
    
    protected boolean changing = false;
    
    protected void setExpanded( Setter setter ) {
        if( setter.value() != null ) {

            if( ! changing && !( setter.value().equals( setter.raw() ) ) ) {
                getToggle().fire( arg( "expanding", setter.value() ) );
            }
            
            setter.set();

        } else {
            throw new IllegalArgumentException( setter.fullName() + " cannot be null" );
        }
    }
    
    public ExpandoPanel expand(){
        signal( "expand" ).fire();
        return this;
    }

    public ExpandoPanel collapse(){
        signal( "collapse" ).fire();
        return this;
    }

    protected void handleExpand( Handler<Event> handler ) {
        handler.handle();
        if( ! handler.event().isStopped() ) {
            raw( "expanded", true );
            model().touch( "expanded" );
        }            
    }

    protected void handleCollapse( Handler<Event> handler ) {
        handler.handle();
        if( ! handler.event().isStopped() ) {
            raw( "expanded", false );
            model().touch( "expanded" );
        }            
    }
    
    public ExpandoPanel toggle(){
        signal( "toggle" ).fire();
        return this;
    }
    
    protected void handleToggle( Handler<Event> handler ) {
        handler.handle();
        if( ! handler.event().isStopped() ) {
            if( getExpanded() ) {
                signal( "collapse" ).fire();
            } else {
                signal( "expand" ).fire();
            }
        }            
    }
    
    /* Virtual Properties */

    @Virtual
    public boolean getCollapsed(){
        return ! this.getExpanded();
    }
    
    public void setCollapsed( boolean collapsed ){
        setExpanded( ! collapsed );
    }

    /* Bean Accessors */

    public boolean getExpanded(){
        return this.expanded;
    }
    
    public void setExpanded( boolean expanded ){
        this.expanded = expanded;
    }
    
    public Signal<ExpandoToggleEvent> getToggle(){
        return this.toggle;
    }
    
    public void setToggle( Signal<ExpandoToggleEvent> toggle ){
        this.toggle = toggle;
    }
    
}