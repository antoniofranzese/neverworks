package it.neverworks.ui.layout;

import java.util.Map;
import java.util.List;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.EnumConverter;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.form.LabelCapable;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.BorderConverter;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Scrollability;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.StyleArtifact;


public class PinboardLayout extends BaseContainerLayoutWidget implements BorderLayoutCapable, StructureCapable, LabelCapable {
    
    public static enum CellVerticalAlignment {
        TOP, MIDDLE, BOTTOM, CENTER
    }
    
    @Convert( ColumnConverter.class )
    public static class Column implements Model {

        @Property
        private Size width;

    }

    public static class ColumnConverter extends ModelConverter {
        
        public ColumnConverter() {
            super( Column.class );
        }

        public Object convert( Object value ) {
            if( value instanceof Column ) {
                return value;
            } else if( value instanceof Number || value instanceof String ) {
                return new Column().set( "width", value );
            } else {
                return super.convert( value );
            }
        }
    
    }

    @Property @Defended
    protected Color background;
    
    @Property @Defended
    protected LayoutInfo layout;

    @Property @Defended @Watch
    protected Border border;
    
    @Property @Defended 
    protected Scrollability scrollable;

    @Property @Defended
    private Label label;

    @Property @Collection @Defended
    protected List<Column> columns;

    @Property @Defended
    protected Font font;

    @Property @Defended
    protected Color color;

    @Property @Defended
    protected Size spacing;
    
    public PinboardLayout() {
        super();
    }

    public PinboardLayout( Arguments arguments ) {
        super();
        set( arguments );
    }

    public PinboardLayout( Widget... widgets ) {
        super( widgets );
    }

    public PinboardLayout( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    public boolean isStructural() {
        return true;
    }

    /* Bean Accessors */

    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public Scrollability getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( Scrollability scrollable ){
        this.scrollable = scrollable;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }

    public Label getLabel(){
        return this.label;
    }
    
    public void setLabel( Label label ){
        this.label = label;
    }

    public List<Column> getColumns(){
        return this.columns;
    }
    
    public void setColumns( List<Column> columns ){
        this.columns = columns;
    }
     
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
   
    public Size getSpacing(){
        return this.spacing;
    }
    
    public void setSpacing( Size spacing ){
        this.spacing = spacing;
    }


}