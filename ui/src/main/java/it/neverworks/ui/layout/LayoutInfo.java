package it.neverworks.ui.layout;

import java.util.Map;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.ui.types.ViewportInfo;

@Child    
@Ignore
public class LayoutInfo extends ExpandoModel implements ChildModel {

    @Property @Collection
    protected Map<String, ViewportInfo> viewport;
    
    public LayoutInfo() {
        super();
    }
    
    public LayoutInfo( Map<String, Object> properties ) {
        super();
        for( String name: properties.keySet() ) {
            set( name, properties.get( name ) );
        }
    }
    
    public Object retrieveItem( Object key ) {
        return containsItem( key ) ? super.retrieveItem( key ) : null;
    }

    protected LayoutCapable parent = null;
    
    public <T extends ExpandoModel> T set( String name, Object value ) {
        String normalizedName = Strings.hyphen2camel( name );
        T result = super.set( normalizedName, value );
        if( this.parent != null ) {
            this.parent.setLayoutProperty( normalizedName, value );
        }
        return result;
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ){
        if( this.parent == null || parent == model.actual() ) {
            if( model.actual() instanceof LayoutCapable ) {
                this.parent = model.<LayoutCapable>actual();
                if( this.size() > 0 ) {
                    for( String key: this.keySet() ) {
                        this.parent.setLayoutProperty( key, this.get( key ) );
                    }
                }
            }
        } else {
            throw new RuntimeException( "LayoutInfo instance cannot be shared (previous: " + parent + " ,current: " + model.actual() + " )" );
        }
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ){
        this.parent = null;
    }
    
    public void mixin( Map other ) {
        if( other != null ) {
            for( Object key: other.keySet() ) {
                String prop = key.toString();
                if( !( this.containsKey( prop ) ) ) {
                    this.set( prop, other.get( key ) );
                }
            }
            
        }
    }
    
    @Virtual
    public ViewportInfo getLandscape() {
        return get( "!viewport.landscape" );
    }

    @AutoConvert
    public void setLandscape( ViewportInfo value ) {
        set( "!viewport.landscape", value );
    }

    @Virtual
    public ViewportInfo getPortrait() {
        return get( "!viewport.portrait" );
    }
    
    @AutoConvert
    public void setPortrait( ViewportInfo value ) {
        set( "!viewport.portrait", value );
    }

}