package it.neverworks.ui.layout;

import it.neverworks.model.events.Signal;
import it.neverworks.ui.Widget;

public interface Selector {
    
    public Signal<SelectEvent> getSelect();
    public Widget getSelected();

}