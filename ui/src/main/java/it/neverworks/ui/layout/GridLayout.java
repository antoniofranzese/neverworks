package it.neverworks.ui.layout;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.EnumConverter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.form.LabelCapable;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.types.BorderConverter;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Scrollability;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.StyleArtifact;

public class GridLayout extends BaseContainerLayoutWidget implements BorderLayoutCapable, StructureCapable, PanelCapable, TitleCapable, LabelCapable {
    
    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }
    
    @AutoConvert
    public static enum CellHorizontalAlignment {
        LEFT, CENTER, RIGHT, MIDDLE
    }

    @AutoConvert
    public static enum CellVerticalAlignment {
        TOP, MIDDLE, BOTTOM, CENTER
    }

    @Convert( InnerBorderConverter.class )
    public static class InnerBorder extends Border {
        public InnerBorder(){}
        
        public InnerBorder( int size ) {
            super( size );
        }

        public InnerBorder( String color ) {
            super( color );
        }

        public InnerBorder( Color color ) {
            super( color );
        }
    
        public InnerBorder( Arguments arguments ) {
            super( arguments );
        }
        
        protected void setRight( Setter<Side> setter ) {
            setter.decline();
        }

        protected void setBottom( Setter<Side> setter ) {
            setter.decline();
        }
        
        @Virtual
        public Side getVertical(){
            return getLeft();
        }
        
        public void setVertical( Side vertical ){
            setLeft( vertical );
        }
        
        @Virtual
        public Side getHorizontal(){
            return getTop();
        }
        
        public void setHorizontal( Side horizontal ){
            setTop( horizontal );
        }
        
        public String toString() {
            return new ToStringBuilder( this )
                .add( "horizontal" )
                .add( "vertical" )
                .toString();
        }
    }
    
    public static class InnerBorderConverter extends BorderConverter {
        public InnerBorderConverter() {
            super( InnerBorder.class );
        }
    }
    
    @Watchable
    @Default( GridLayoutBorder.class )
    @Convert( GridLayoutBorderConverter.class )
    public static class GridLayoutBorder extends BaseModel {
        
        @Property @Watch( changes = WatchChanges.WHOLE )
        private InnerBorder inner;
        
        @Property @Watch( changes = WatchChanges.WHOLE )
        private Border outer;
        
        public GridLayoutBorder() {}
        
        public GridLayoutBorder( Arguments arguments ) {
            for( String name: arguments.names() ) {
                set( name.startsWith( "!" ) ? name : "!" + name, arguments.get( name ) );
            }
        }
        
        public Border getOuter(){
            return this.outer;
        }
        
        public void setOuter( Border outer ){
            this.outer = outer;
        }
        
        public InnerBorder getInner(){
            return this.inner;
        }
        
        public void setInner( InnerBorder inner ){
            this.inner = inner;
        }
        
        public Object getRight(){
            return get( "!outer.right" );
        }
        
        public void setRight( Object right ){
            set( "!outer.right", right );
        }
        
        public Object getLeft(){
            return get( "!outer.left" );
        }
        
        public void setLeft( Object left ){
            set( "!outer.left", left );
        }
        
        public Object getBottom(){
            return get( "!outer.bottom" );
        }
        
        public void setBottom( Object bottom ){
            set( "!outer.bottom", bottom );
        }
        
        public Object getTop(){
            return get( "!outer.top" );
        }
        
        public void setTop( Object top ){
            set( "!outer.top", top );
        }
        
        public void setSize( Object size ) {
            set( "!outer.size", size );
        }
        
        public void setColor( Object color ) {
            set( "!outer.color", color );
        }

        public void setStyle( Object style ) {
            set( "!outer.style", style );
        }
        
        public String toString() {
            return new ToStringBuilder( this )
                .add( "outer" )
                .add( "inner" )
                .toString();
        }
    }
    
    public static class GridLayoutBorderConverter implements Converter {
        public Object convert( Object value ) {
            if( value instanceof GridLayoutBorder ) {
                return value;
            } else if( value instanceof Arguments ) {
                Arguments arguments = (Arguments) value;
                return new GridLayoutBorder( (Arguments) value );
            } else {
                // BorderConverter pass-through
                return new GridLayoutBorder()
                    .set( "inner", value )
                    .set( "outer", value );
            }
        }
    }
    
    public static class ColumnsConverter implements Converter {
        private final static TypeDefinition<Integer> IntType = TypeFactory.shared( Integer.class );
        
        public Object convert( Object value ) {
            if( value instanceof Integer ) {
                return value;

            } else if( value instanceof String ) {
                if( "unbound".equalsIgnoreCase( (String) value ) ) {
                    return Integer.MAX_VALUE;
                } else if( "single".equalsIgnoreCase( (String) value ) ) {
                    return 1;
                } else {
                    return IntType.convert( value );
                }

            } else {
                return IntType.convert( value );
            }
        }
    }
    
    @Property @Convert( ColumnsConverter.class ) @Defended
    protected int columns = 2;
    
    @Property @Defended
    protected Background background;

    @Property @Defended
    protected Color color;
    
    @Property @Defended
    protected LayoutInfo layout;

    @Property @Defended @Watch
    protected GridLayoutBorder border;

    @Property @Defended
    protected Corners rounding;
    
    @Property
    protected Shadow shadow;

    @Property @Defended 
    protected Scrollability scrollable;

    @Property @Defended @AutoConvert
    protected boolean collapsible = false;
    
    @Property @Defended
    private HorizontalAlignment halign;
    
    @Property @Defended
    private Font font;
    
    @Property @Defended
    private Size spacing;
    
    @Property @Defended
    private String title;
    
    @Property @Defended
    private boolean closable;
    
    @Property @Defended
    private Label label;

    public GridLayout() {
        super();
    }

    public GridLayout( Arguments arguments ) {
        super();
        set( arguments );
    }

    public GridLayout( Widget... widgets ) {
        super( widgets );
    }

    public GridLayout( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    public void setLayoutProperty( String name, Object value ) {
        if( name.equals( "title" ) ) {
            model().direct( "title", value );

        } else if( name.equals( "closable" ) ) {
            model().direct( "closable", value );
        }
    }

    public boolean isStructural() {
        return true;
    }

    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }

    public Corners getRounding(){
        return this.rounding;
    }
    
    public void setRounding( Corners rounding ){
        this.rounding = rounding;
    }

    public int getColumns(){
        return this.columns;
    }
    
    public void setColumns( int columns ){
        this.columns = columns;
    }

    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public Scrollability getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( Scrollability scrollable ){
        this.scrollable = scrollable;
    }
    
    public GridLayoutBorder getBorder(){
        return this.border;
    }
    
    public void setBorder( GridLayoutBorder border ){
        this.border = border;
    }

    public boolean getCollapsible(){
        return this.collapsible;
    }
    
    public void setCollapsible( boolean collapsible ){
        this.collapsible = collapsible;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Size getSpacing(){
        return this.spacing;
    }
    
    public void setSpacing( Size spacing ){
        this.spacing = spacing;
    }

    public boolean isClosable(){
        return this.closable;
    }

    public boolean getClosable(){
        return this.closable;
    }
    
    public void setClosable( boolean closable ){
        this.closable = closable;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }
    
    public Label getLabel(){
        return this.label;
    }
    
    public void setLabel( Label label ){
        this.label = label;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
}