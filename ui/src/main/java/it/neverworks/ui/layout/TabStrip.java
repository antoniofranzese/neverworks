package it.neverworks.ui.layout;

import it.neverworks.model.Property;
import it.neverworks.model.description.Synthesize;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Box;

@Convert( TabStripConverter.class )
public class TabStrip extends TabContent {
    
    @Property @Synthesize @AutoConvert
    protected boolean visible = true;
    
}