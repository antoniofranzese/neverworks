package it.neverworks.ui.layout;

import it.neverworks.ui.Widget;
import it.neverworks.ui.Container;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.utils.ToStringBuilder;

public class CloseEvent<T extends StackLayout> extends UIEvent<T> {
    
    private int index;
    private Widget closed;
    
    public int getIndex(){
        return this.index;
    }
    
    public void setIndex( int index ){
        this.index = index;
    }
    
    public Widget getClosed() {
        return closed != null ? closed : this.getSource().widget( this.index );
    }
    
    /*package*/ void setClosed( Widget page ) {
        this.closed = page;
    }
    
    public void close() {
        this.getSource().close( this.index );
    }

    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "index" ).toString();
    }
}