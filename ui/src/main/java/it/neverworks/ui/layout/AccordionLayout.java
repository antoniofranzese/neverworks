package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.ui.Widget;

public class AccordionLayout extends StackLayout {

    public AccordionLayout() {
        super();
    }

    public AccordionLayout( Arguments arguments ) {
        super();
        set( arguments );
    }

    public AccordionLayout( Widget... widgets ) {
        super( widgets );
    }

    public AccordionLayout( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
}