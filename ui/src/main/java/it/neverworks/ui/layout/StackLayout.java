package it.neverworks.ui.layout;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watcher;
import it.neverworks.model.collections.ListAdder;
import it.neverworks.model.collections.ListRemover;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.io.State;
import it.neverworks.ui.Widget;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Size;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Event;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.types.Flavored;

public class StackLayout extends BaseContainerLayoutWidget implements BorderLayoutCapable, Selector {
    
    @Property @Convert( ChildWidgetConverter.class )
    @Access( setter = "updateSelected" )
    private Widget selected;
    
    @Property
    protected Signal<SelectEvent> selecting;
    
    @Property
    protected Signal<SelectEvent> select;
    
    @Property
    protected Signal<CloseEvent> closing;

    @Property
    protected Signal<CloseEvent> close;
    
    @Property
    private Signal<SelectEvent> click;
    
    @Property
    protected LayoutInfo layout;
    
    protected List<Modification> modifications;
    
    protected boolean changing = false;
    
    public StackLayout() {
        super();
    }

    public StackLayout( Arguments arguments ) {
        this();
        set( arguments );
    }

    public StackLayout( Widget... widgets ) {
        this();
        add( widgets );
    }

    public StackLayout( Arguments arguments, Widget... widgets ) {
        super();
        set( arguments );
        add( widgets );
    }
    
    public void select( Object child ) {
        if( child instanceof Number ) {
            int index = ((Number) child).intValue();
            if( checkIndex( index ) ) {
                int old = getSelectedIndex();
                setSelectedIndex( index );
                event( "select" ).fire( arg( "old", old ).arg( "new", getSelectedIndex() ) );
            }
        } else if( child instanceof Widget ) {
            Widget widget = (Widget) child;
            if( checkChild( widget ) ) {
                setSelected( widget );
            }
        } else if( child != null ){
            select( get( str( child ) ) );
        }
    }
    
    protected void handleSelect( Handler<SelectEvent> handler ) {
        this.changing = true;
        try {
            if( signal( "click" ).fire( handler.event() ).isContinued() ) {
                handler.handle();
            } else {
                handler.decline();
            }
        } finally {
            this.changing = false;
        }
    }
    
    protected void updateSelected( Setter<Widget> setter ) {
        int old = -1;
        if( ! changing && active ) {
            old = getSelectedIndex();
            if( setter.raw() != null && setter.raw() instanceof LayoutCapable ) {
                setter.raw().set( "!layout.selected", false );
            }
        }
        setter.set();
        
        if( ! changing && active ) {
            int new_ = getSelectedIndex();
            if( old != new_ ) {
                getSelect().fire( arg( "old", old ).arg( "new", new_ ) );
            }
        }
        
        if( setter.raw() != null && setter.raw() instanceof LayoutCapable ) {
            setter.raw().set( "!layout.selected", true );
        }
        
    }

    protected void addChildrenItem( ListAdder<Widget> adder ) {
        super.addChildrenItem( adder );
        Widget added = adder.item();

        if( selected == null || ( added instanceof LayoutCapable && added.value( "!layout.selected" ).isTrue() ) ) {
            if( started ) {
                set( "selected", added );
            } else {
                selected = added;
                event( "startup" ).add( slot( "recoverFirstSelect" ).once() );
            }
        }

        if( persistent ) {
            enlistModification( new Insertion( added, adder.index() - 1 ) );
        }

        if( !( added instanceof VisibilityCapable ) || added.value( "visible" ).isTrue() ) {
            model().touch( "selected" );
        }
        
    }
    
    protected void recoverFirstSelect( Event event ) {
        Widget first = selected;
        selected = null;
        set( "selected", first );
    }
    
    protected void removeChildrenItem( ListRemover<Widget> remover ) {
        if( remover.item() == selected ) {
            if( getChildren().size() > 1 ) {
                if( remover.index() > 0 ) {
                    select( remover.index() - 1 );
                } else {
                    select( 1 );
                }
            } else {
                selected = null;
            }
        }
        if( persistent ) {
            enlistModification( new Removal( remover.item(), remover.index() ) );
        }
        super.removeChildrenItem( remover );

        model().touch( "selected" );
    }
    
    protected void handleClose( Handler<CloseEvent> handler ) {
        Widget widget = widget( handler.event().getIndex() );
        this.remove( widget );
        handler.event().setClosed( widget );
        handler.handle();
    }
    
    public void close( Object child ) {
        if( child instanceof Number ) {
            int index = ((Number) child).intValue();
            if( checkIndex( index ) ) {
                event( "close" ).fire( arg( "index", index ) );
            }
        } else if( child instanceof Widget ) {
            Widget widget = (Widget) child;
            if( checkChild( widget )  ) {
                close( this.getChildren().indexOf( widget ) );
            }
        } else if( child != null ) {
            close( get( str( child ) ) );
        }
    }

    public boolean checkChild( Widget widget ) {
        if( widget != null ) {
            if( this.getChildren().contains( widget ) ) {            
                return true;
            } else {
                throw new IllegalArgumentException( widget.getCommonName() + " is not a child" );
            }
        } else {
            throw new IllegalArgumentException( "Cannot operate on null child" );
        }
    }

    protected boolean checkIndex( int index ) {
        if( index >= 0 && index < this.getChildren().size() ) {
            return true;
        } else {
            throw new IndexOutOfBoundsException( "Invalid child index: " + index );
        }
    }
    
    /* Virtual Properties */
    
    @Virtual
    public int getSelectedIndex() {
        if( selected != null ) {
            return this.getChildren().indexOf( selected );
        } else {
            return -1;
        }
    }
    
    public void setSelectedIndex( int index ) {
        if( index >= 0 ) {
            if( checkIndex( index ) ) {
                setSelected( this.getChildren().get( index ) );
            }
        } else {
            setSelected( null );
        }
    }
    
    /* Modifications */
    protected void enlistModification( Modification modification ) {
        if( modifications == null ) {
            modifications = new ArrayList<Modification>();
        }
        modifications.add( modification );
    }
    
    public static class Modification {
        protected Widget child;
        protected int index;
        
        public Modification( Widget child, int index ) {
            this.child = child;
            this.index = index;
        }
        
        public Widget getChild() {
            return this.child;
        }

        public int getIndex() {
            return this.index;
        }

    }
    
    public static class Insertion extends Modification {
        public Insertion( Widget child, int index ) {
            super( child, index );
        }
    }
    
    public static class Removal extends Modification {
        public Removal( Widget child, int index ) {
            super( child, index );
        }
    }
    
    protected void watch( Watcher watch ) {
        this.modifications = null;
    }    

    protected void ignore( Watcher ignore ) {
        this.modifications = null;
    }    
    
    /* Beans Accessors */

    public Widget getSelected(){
        return this.selected;
    }
    
    public void setSelected( Widget selected ){
        this.selected = selected;
    }
    
    public List<Modification> getModifications(){
        return this.modifications;
    }
    
    public Signal<SelectEvent> getSelect(){
        return this.select;
    }
    
    public void setSelect( Signal<SelectEvent> select ){
        this.select = select;
    }
    public Signal<CloseEvent> getClose(){
        return this.close;
    }
    
    public void setClose( Signal<CloseEvent> close ){
        this.close = close;
    }

    public Signal<CloseEvent> getClosing(){
        return this.closing;
    }
    
    public void setClosing( Signal<CloseEvent> closing ){
        this.closing = closing;
    }
    
    public Signal<SelectEvent> getSelecting(){
        return this.selecting;
    }
    
    public void setSelecting( Signal<SelectEvent> selecting ){
        this.selecting = selecting;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public Signal<SelectEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<SelectEvent> click ){
        this.click = click;
    }
    
}