package it.neverworks.ui.layout;

import it.neverworks.model.Property;
import it.neverworks.model.description.Synthesize;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.ui.types.WidgetModel;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Box;

@Convert( ModelConverter.class )
public class TabContent extends WidgetModel {
    
    @Property @Synthesize
    protected Color color;
    
    @Property @Synthesize
    protected Color background;
    
    @Property @Synthesize
    protected Box padding;

    @Property @Synthesize
    protected Font font;
    
}