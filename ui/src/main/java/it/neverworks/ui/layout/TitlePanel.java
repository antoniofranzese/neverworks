package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class TitlePanel extends Panel {
    
    public enum Alignment {
        TOP, BOTTOM, LEFT, RIGHT, CENTER, MIDDLE
    }

    @Property
    private Boolean collapsible;
    
    @Property @AutoConvert
    private Alignment alignment;
    
    @Property @AutoConvert
    private boolean opened = true;
    
    public TitlePanel() {
        super();
    }
    
    public TitlePanel( Arguments arguments ) {
        super();
        set( arguments );
    }

    public boolean isStructural() {
        return false;
    }

    public Boolean getCollapsible(){
        return this.collapsible;
    }
    
    public void setCollapsible( Boolean collapsible ){
        this.collapsible = collapsible;
    }
    
    public Alignment getAlignment(){
        return this.alignment;
    }
    
    public void setAlignment( Alignment alignment ){
        this.alignment = alignment;
    }
    
    public boolean getOpened(){
        return this.opened;
    }
    
    public void setOpened( boolean opened ){
        this.opened = opened;
    }

}