package it.neverworks.ui.layout;

import it.neverworks.model.Property;
import it.neverworks.model.description.Defended;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.Widget;
import it.neverworks.ui.BaseParentWidget;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.types.ViewportInfo;
import it.neverworks.ui.types.Flavored;

public abstract class BaseLayoutWidget extends BaseParentWidget implements LayoutWidget, Flavored, VisibilityCapable {
    
    @Property @Defended
    protected Size width;
    
    @Property @Defended
    protected Size height;
    
    @Property @Defended
    protected Box margin;
    
    @Property @Defended
    protected Box padding;
    
    @Property @Defended
    protected StyleArtifact flavor;
    
    @Property @Defended @AutoConvert
    private boolean visible = true;
    
    /* Bean Accessors */
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public Box getMargin(){
        return this.margin;
    }
    
    public void setMargin( Box margin ){
        this.margin = margin;
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
}