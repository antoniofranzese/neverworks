package it.neverworks.ui.layout;

import java.util.Map;
import it.neverworks.model.description.Virtual;
import it.neverworks.ui.types.ViewportInfo;
import it.neverworks.ui.Widget;

public interface LayoutCapable extends Widget {
    LayoutInfo getLayout();
    
    default void setLayoutProperty( String name, Object value ) {
        
    }

    @Virtual
    default Map<String, ViewportInfo> getViewport() {
         return get( "!layout.viewport" );
    }

    default void setViewport( Object value ) {
        set( "!layout.viewport", value );
    }
}