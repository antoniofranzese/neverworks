package it.neverworks.ui.layout;

import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;

public class ChildWidgetConverter implements InstanceConverter {
    
    public Object convert( Object value ) {
        return value;
    }
    
    public Object convert( ModelInstance instance, Object value ) {
        if( value instanceof Widget ) {
            return value;
        } else {
            Widget widget = (Widget) instance.actual();
            if( value instanceof Number && widget instanceof Container ) {
                try {
                    return ((Container) widget).get( ((Number) value).intValue() );
                } catch( Exception ex ) {
                    return value;
                }
            } else if( value instanceof String ) {
                Widget child = null;
                try {
                    child = widget.children().query().by( "name" ).eq( value ).result();
                } catch( Exception ex ) {
                    return value;
                }
                
                return child != null ? child : value;
            
            } else {
                return value;
            }
            
        }
    }
}