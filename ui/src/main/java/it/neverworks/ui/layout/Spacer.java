package it.neverworks.ui.layout;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.BaseWidget;

public class Spacer extends BaseWidget implements LayoutCapable, Flavored, VisibilityCapable {
    
    @Property
    private Size width;
    
    @Property
    private Size height;
    
    @Property @AutoConvert
    private boolean visible = true;
    
    @Property
    private LayoutInfo layout;
    
    @Property
    private StyleArtifact flavor;
    
    @Property
    private Background background;
    
    @Property
    private Border border;
    
    public Spacer() {
        super();
    }
    
    public Spacer( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }

    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
}