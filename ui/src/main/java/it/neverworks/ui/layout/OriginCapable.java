package it.neverworks.ui.layout;

import it.neverworks.ui.types.Point;

public interface OriginCapable {
    public Point getOrigin();
}