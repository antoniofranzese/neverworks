package it.neverworks.ui.layout;

public interface CloseCapable {
    boolean isClosable();
    void setClosable( boolean closable );
}