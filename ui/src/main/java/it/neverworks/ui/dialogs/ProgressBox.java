package it.neverworks.ui.dialogs;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.events.Signal;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ProgressBar;
import it.neverworks.ui.form.ProgressConverter;
import it.neverworks.ui.Container;

public class ProgressBox extends WaitBox {
    
    @Property
    private Signal<DialogEvent> cancel;
    
    @Property @Convert( ProgressConverter.class )
    private Integer progress;

    public ProgressBox() {
        super();
    }
    
    public ProgressBox( Arguments arguments ) {
        super();
        set( arguments );
    }
    

    public void build() {
        super.build();
        
        container( "main" ).add(
            new ProgressBar()
                .set( "name", "progressBar" )
                .set( "layout", arg( "halign", "center" ) )
                .set( "value", progress )
        );
        
        if( getCancel().size() > 0 ) {
            container( "main" ).add(
                new Button()
                    .set( "name", "cancelButton" )
                    .set( "caption", T( "Cancel" ) )
                    .set( "layout", arg( "halign", "center" ) )
            );
        }
    }

    protected void setProgress( Setter<Integer> setter ) {
        set( "?progressBar.value", setter.set() );
    }

    @Virtual @AutoConvert
    public void setIndeterminate( boolean indeterminate ) {
        set( "progress", indeterminate ? null : 0 );
    }
    
    /* Bean Accessors */
    
    public Signal<DialogEvent> getCancel(){
        return this.cancel;
    }
    
    public void setCancel( Signal<DialogEvent> cancel ){
        this.cancel = cancel;
    }
    
    public int getProgress(){
        return this.progress;
    }
    
    public void setProgress( int progress ){
        this.progress = progress;
    }
    
    

}