package it.neverworks.ui.dialogs;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;
import it.neverworks.ui.lifecycle.ErrorEvent;

public class WaitBox extends Popup {

    @Property
    protected String message;
    
    public WaitBox() {
        this( T("No message") );
    }

    public WaitBox( String message ) {
        this( message, T("Wait") );
    }
    
    public WaitBox( String message, String title ) {
        super();
        set( "opened", true );
        set( "closable", false );
        set( "message", message );
        set( "title", title );
        set( "autoDestroy", true );
        //set( "maximumWidth", 600 );
        set( "minimumWidth", 250 );
    }
    
    public WaitBox( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 1 )
            .arg( "width", "100%")
            .arg( "messageBox", new Label(
                arg( "value", this.message )
                .arg( "layout",
                    arg( "halign", "center" ) 
                )
            ))
        ));

    }
    
    protected void setMessage( Setter<String> setter ) {
        set( "?messageBox.value", setter.set() );
    }

    /* Bean Accessors */
    
    public String getMessage(){
        return this.message;
    }
    
    public void setMessage( String message ){
        this.message = message;
    }

}