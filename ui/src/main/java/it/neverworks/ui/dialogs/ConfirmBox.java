package it.neverworks.ui.dialogs;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.Container;

public class ConfirmBox extends MessageBox {

    @Property
    protected Signal<DialogEvent> cancel;
    
    public ConfirmBox() {
        this( T( "No message") );
    }

    public ConfirmBox( String message ) {
        this( message, T("Confirm") );
    }
    
    public ConfirmBox( String message, String title ) {
        super( message, title );
    }
    
    public ConfirmBox( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void build() {
        super.build();
        container( "buttons" ).place(
            new Button()
                .set( "name", "cancelButton" )
                .set( "caption", T("Cancel") )
                .set( "click", signal( "cancel" ) )
        ).after( "confirmButton" );
        
    }
    
    protected void handleCancel( Handler<DialogEvent> handler ) {
        if( handler.handle().isContinued() ) {
            close();
        }
    }
    
    /* Bean Accessors */
    
    public Signal<DialogEvent> getCancel(){
        return this.cancel;
    }
    
    public void setCancel( Signal<DialogEvent> cancel ){
        this.cancel = cancel;
    }
    
}