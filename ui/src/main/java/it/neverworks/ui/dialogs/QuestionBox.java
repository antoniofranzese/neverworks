package it.neverworks.ui.dialogs;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.Container;

public class QuestionBox extends ConfirmBox {

    @Property
    protected Signal<DialogEvent> reject;
    
    public QuestionBox() {
        this( T("No message") );
    }

    public QuestionBox( String message ) {
        this( message, T("Question") );
    }
    
    public QuestionBox( String message, String title ) {
        super( message, title );
    }
    
    public QuestionBox( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void build() {
        super.build();
        container( "buttons" ).place(
            new Button()
                .set( "name", "rejectButton" )
                .set( "caption", T("No") )
                .set( "click", signal( "reject" ) )
        ).after( "confirmButton" );
        
        set( "confirmButton.caption", T("Yes") );    
    }
    
    protected void handleReject( Handler<DialogEvent> handler ) {
        if( handler.handle().isContinued() ) {
            close();
        }
    }
    
    /* Bean Accessors */
    
    public Signal<DialogEvent> getReject(){
        return this.reject;
    }
    
    public void setReject( Signal<DialogEvent> reject ){
        this.reject = reject;
    }
    
}