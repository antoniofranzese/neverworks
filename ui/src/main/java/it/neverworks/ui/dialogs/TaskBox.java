package it.neverworks.ui.dialogs;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.context.Task;
import it.neverworks.context.TaskEvent;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Wire;
import it.neverworks.model.events.Signal;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Setter;

public class TaskBox extends ProgressBox {
    @Property
    protected Task task;

    @Property
    protected Signal<TaskEvent> complete;

    @Property
    protected Signal<TaskEvent> reject;

    @Property
    protected Signal<TaskEvent> poll;

    @Property
    protected Signal<TaskEvent> stop;

    @Property @AutoConvert
    protected int interval = 5000;

    @Property @AutoConvert
    protected int delay = 0;

    public TaskBox() {
        super();
    }

    public TaskBox( Arguments arguments ) {
        super();
        set( arguments );
    }

    @Wire( "startup" )
    protected void onStartup( Event event ) {
        this.start();
        
        if( this.delay > 0 ) {
            try {
                this.task.hold( this.delay );
                if( this.task.isAlive() ) {
                    next( slot( "checkTask" ) );
                } else {
                    this.complete();
                }
            } catch( RuntimeException ex ) {
                this.handle( ex );
            }
        } else {
            next( slot( "checkTask" ) );
        }
    }

    protected void setInterval( Setter<Integer> setter ) {
        setter.set( Math.max( setter.value(), 1000 ) );
    }

    public Task start() {
        if( this.task != null ) {
            return this.task.start();
        } else {
            throw new IllegalStateException( "No task to start in " + this.getClass().getSimpleName() );
        }
    }

    public void checkTask( Event event ) {
        try {
            if( this.task.isAlive() ) {
                try {
                    signal( "poll" ).fire( new TaskEvent( this.task ) );
                    Thread.sleep( this.interval );
                    next( slot( "checkTask" ) );
                } catch( InterruptedException ex ) {
                    // Nothing to do
                }
            } else {
                this.complete();
            }
        } catch( RuntimeException ex ) {
            this.handle( ex );
        }
    }

    protected void handle( RuntimeException ex ) {
        call( "close" );
        if( signal( "reject" ).size() > 0 ) {
            signal( "reject" ).fire( new TaskEvent( this.task ) );
        } else {
            throw ex;
        }
    }

    protected void complete() {
        call( "close" );
        signal( "complete" ).fire( new TaskEvent( this.task ) );
    }

    protected void stop() {
        call( "close" );
        signal( "stop" ).fire( new TaskEvent( this.task ) );
    }

}
