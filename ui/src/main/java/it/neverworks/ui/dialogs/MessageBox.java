package it.neverworks.ui.dialogs;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;
import it.neverworks.ui.lifecycle.ErrorEvent;

public class MessageBox extends Popup {

    @Property
    protected String message;
    
    @Property
    protected Signal<DialogEvent> confirm;
    
    public MessageBox() {
        this( T("No message") );
    }

    public MessageBox( String message ) {
        this( message, T( "Message" ) );
    }

    public MessageBox( String message, String title ) {
        super();
        set( "opened", true );
        set( "closable", false );
        set( "autoDestroy", true );
        set( "message", message );
        set( "title", title );
        //set( "maximumWidth", 600 );
        set( "minimumWidth", 250 );
   }
    
    public MessageBox( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 1 )
            .arg( "width", "100%" )

            .arg( "messageBox", new Label(
                arg( "value", this.message )
                .arg( "layout",
                    arg( "halign", "center" ) 
                )
                .arg( "wrap", true )
            ))

            .arg( "buttons", new ToolBar(
                arg( "layout",
                    arg( "halign", "center" ) 
                )
                .arg( "padding", 
                    arg( "top", 10 ) 
                )
                ,new Button(
                    arg( "name", "confirmButton" )
                    .arg( "caption", T("OK") )
                    .arg( "click", signal( "confirm" ) )
                )
            ))
        ));
        set( "error", slot( "onError" ) );
    }
    
    protected void handleConfirm( Handler<DialogEvent> handler ) {
        if( handler.handle().isContinued() ) {
            close();
        }
    }

    protected void onError( ErrorEvent event ) {
        try {
            bubble( event );
        } finally {
            close();
        }
    }
    
    protected void setMessage( Setter<String> setter ) {
        setter.set();
        if( this.contains( "messageBox" ) ) {
            set( "messageBox.value", setter.value() );
        }
    }
    
    /* Bean Accessors */
    
    public Signal getConfirm(){
        return this.confirm;
    }
    
    public void setConfirm( Signal confirm ){
        this.confirm = confirm;
    }
    
    public String getMessage(){
        return this.message;
    }
    
    public void setMessage( String message ){
        this.message = message;
    }

}