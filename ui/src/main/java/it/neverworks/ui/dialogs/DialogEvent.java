package it.neverworks.ui.dialogs;

import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.utils.ToStringBuilder;

public class DialogEvent<T> extends UIEvent<T> {
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).toString();
    }
}