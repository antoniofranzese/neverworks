package it.neverworks.ui.menu;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.model.Property;
import it.neverworks.model.events.Signal;
import it.neverworks.model.description.Setter;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.Widget;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;

public class MenuItem extends BaseWidget {

    @Property
    private String caption;
    
    @Property
    private boolean disabled;
    
    @Property
    private Signal<MenuClickEvent> click;
    
    @Property
    private boolean visible = true; 
    
    @Property
    private Color color;
    
    @Property
    private Color background;
    
    @Property
    private Font font;
    
    public MenuItem() {
        super();
    }

    public MenuItem( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public String getPath() {
        if( getParent() == null || !( getParent() instanceof MenuItem ) ) {
            return super.getPath();
        } else {
            //Local item path
            List<String> names = new ArrayList<String>();
            Widget current = this;
            while( current.getParent() != null && current.getParent() instanceof MenuItem ) {
                names.add( 0, current.getName() );
                current = current.getParent();
            }
        
            return Collections.join( names, "." );
        }
    }
    
    protected void setParent( Setter<Widget> setter ) {
        if( setter.value() == null || validateParent( setter.value() ) ) {
            super.setParent( setter );
        }
    }
    
    protected boolean validateParent( Widget parent ) {
        if( parent instanceof Menu ) {
            return true;
        } else {
            throw new WidgetException( this.getClass().getSimpleName() + " must be placed within a Menu" );
        }
    }

    /* Virtual Properties */
    
    public boolean getEnabled(){
        return ! getDisabled();
    }
    
    public void setEnabled( boolean enabled ){
        setDisabled( ! enabled );
    }
    
    /* Bean Accessors */
    
    public Signal<MenuClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<MenuClickEvent> click ){
        this.click = click;
    }
    
    public boolean getDisabled(){
        return this.disabled;
    }
    
    public void setDisabled( boolean disabled ){
        this.disabled = disabled;
    }
    
    public String getCaption(){
        return this.caption;
    }
    
    public void setCaption( String caption ){
        this.caption = caption;
    }
    
    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
}