package it.neverworks.ui.menu;

import it.neverworks.ui.lifecycle.UIEvent;

public class MenuClickEvent extends UIEvent<MenuItem> {
    
    protected String path;
    
    protected MenuItem item;

    private Menu menu;
    
    private boolean dispatching = false; 
    
    public boolean getDispatching(){
        return this.dispatching;
    }
    
    public void setDispatching( boolean dispatching ){
        this.dispatching = dispatching;
    }
    
    public Menu getMenu(){
        return this.menu;
    }
    
    public void setMenu( Menu menu ){
        this.menu = menu;
    }
        
    public MenuItem getItem(){
        return this.item;
    }

    public void setItem( MenuItem item ){
        this.item = item;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
        
}