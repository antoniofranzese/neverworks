package it.neverworks.ui.menu;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;

public class CheckedMenuItem extends MenuItem {
    
    @Property
    private boolean checked;
    
    public CheckedMenuItem() {
        super();
    }

    public CheckedMenuItem( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    
    
    public boolean getChecked(){
        return this.checked;
    }
    
    public void setChecked( boolean checked ){
        this.checked = checked;
    }
    
}