package it.neverworks.ui.menu;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Changes;
import it.neverworks.model.description.Watcher;
import it.neverworks.model.description.Setter;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.collections.Adder;
import it.neverworks.model.collections.Remover;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.types.Type;
import it.neverworks.model.features.Resolve;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.types.AnchorType;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.Widget;

public class Menu extends MenuItem {

    @Property @Collection
    private List<MenuItem> children;
    
    @Property @Type( AnchorType.class ) @Resolve
    private Object anchor;
    
    @Property
    private Signal<UIEvent> close;

    @Property
    private Signal<UIEvent> destroy;
    
    @Property
    private boolean opened = false; 

    @Property
    private boolean autoDestroy = false; 
    
    private Map<String, MenuItem> childrenRegistry = new HashMap<String, MenuItem>();
    
    public Menu() {
        super();
    }

    public Menu( Arguments arguments ) {
        super();
        set( arguments );
    }

    public Menu( Arguments arguments, MenuItem... items ) {
        super();
        set( arguments );
        for( MenuItem item: items ) {
            getChildren().add( item );
        }
    }

    public Menu( MenuItem... items ) {
        super();
        for( MenuItem item: items ) {
            getChildren().add( item );
        }
    }
    
    public Menu add( MenuItem... items ) {
        for( MenuItem item: items ) {
            getChildren().add( item );
        }
        return this;
    }
    
    public Menu open() {
        setOpened( true );
        return this;
    }
    
    public Menu open( Object anchor ) {
        setAnchor( anchor );
        return open();
    }
    
    public Menu close() {
        event( "close" ).fire();
        return this;
    }

    public MenuItem item( String path ) {
        return this.<MenuItem>get( path );
    }
    
    protected void handleClick( Handler<MenuClickEvent> handler ) {
        if( handler.event().getDispatching() ) {
            handler.handle();

        } else {
            handler.decline();
            handler.event().setMenu( this );
            handler.event().setDispatching( true );
            
            MenuItem item = item( handler.event().getPath() );
            handler.event().setItem( item );

            List<MenuItem> clickHandlers = new ArrayList<MenuItem>();
            MenuItem current = item;
            while( current != this ) {
                if( current.getClick().size() > 0 ) {
                    clickHandlers.add( current );
                }
                current = current.<MenuItem>get( "parent" );
            }
            clickHandlers.add( this );
            
            boolean terminated = false;
            for( MenuItem clickHandler: clickHandlers ) {
                if( ! terminated ) {
                    handler.event().halt();
                    clickHandler.getClick().fire( handler.event() );
                    terminated = handler.event().isHalted();
                }
            }

            if( getOpened() == false ) {
                this.event( "close" ).fire();
            }
            
        }
    }

    protected void handleClose( Handler handler ) {
        //System.out.println( "Closing menu" );
        this.setOpened( false );
        handler.handle();
        if( this.autoDestroy ) {
            this.destroy();
        }
    }

    public Menu destroy() {
        //System.out.println( "Destroying menu" );
        event( "destroy" ).fire();
        return this;
    }
    
    protected void handleDestroy( Handler handler ) {
        Form form = (Form)getParent();
        if( form != null ) {
            handler.handle(); 
            form.remove( this );
        }
    }

    protected boolean validateParent( Widget parent ) {
        if( parent instanceof Form || parent instanceof Menu ) {
            return true;
        } else {
            throw new WidgetException( "Menu '" + this.getName() + "' must be placed within a Form or a Menu" );
        }
    }
    
    protected void addChildrenItem( Adder<MenuItem> adder ) {
        MenuItem item = adder.item();
        if( item != null ) {
            if( ! Strings.hasText( item.getName() ) ) {
                do {
                    item.setName( "Item-" + Strings.generateHexID( 8 ) );
                } while( childrenRegistry.containsKey( item.getName() ) );
            }
        
            if( childrenRegistry.containsKey( item.getName() ) ) {
                throw new WidgetException( "Duplicate MenuItem name: " + item.getName() );
            } else if( model().has( item.getName() ) ) {
                throw new WidgetException( "Cannot add " + item.getCommonName() + ": obstructed by " + this.getClass().getSimpleName() + " property" );
            }

            item.set( "parent", this );

            adder.add();
        
            childrenRegistry.put( item.getName(), item );
        } else {
            adder.decline();
        }
    }
    
    protected void removeChildrenItem( Remover<MenuItem> remover ) {
        remover.item().set( "parent", null );
        childrenRegistry.remove( remover.item().getName() );
        remover.remove();
    }
    
    protected void setChildren( Setter setter ) {
        setter.set();
        setter.change();
        if( getParent() instanceof Menu ) {
            getParent().set( "children", getParent().get( "children" ) );
        }
    }
    
    public Object retrieveItem( Object key ) {
        if( childrenRegistry.containsKey( key ) ) {
            return childrenRegistry.get( key );
        } else {
            return super.retrieveItem( key );
        }
    }
    
    public <T> T get( String name ) {
        if( childrenRegistry.containsKey( name ) ) {
            return (T)childrenRegistry.get( name );
        } else {
            return super.get( name );
        }
    }
    
    protected void watch( Watcher watcher ) {
        for( MenuItem item: getChildren() ) {
            item.model().watch();
        }
    }
    
    protected void ignore( Watcher watcher ) {
        for( MenuItem item: getChildren() ) {
            item.model().ignore();
        }
    }
    
    protected Changes changes( Changes changes ) {
        for( MenuItem item: getChildren() ) {
            for( String change: item.model().changes() ) {
                changes.add( item.getName() + "." + change );
            }
        }
        return changes;
    }
    /* Bean Accessors */
    
    public List<MenuItem> getChildren(){
        return this.children;
    }
    
    public void setChildren( List<MenuItem> children ){
        this.children = children;
    }
    
    public Object getAnchor(){
        return this.anchor;
    }
    
    public void setAnchor( Object anchor ){
        this.anchor = anchor;
    }
    
    public boolean getOpened(){
        return this.opened;
    }
    
    public void setOpened( boolean opened ){
        this.opened = opened;
    }
    
    public Signal<UIEvent> getClose(){
        return this.close;
    }
    
    public void setClose( Signal<UIEvent> close ){
        this.close = close;
    }
    
    public boolean getAutoDestroy(){
        return this.autoDestroy;
    }
    
    public void setAutoDestroy( boolean autoDestroy ){
        this.autoDestroy = autoDestroy;
    }
    
    public Signal<UIEvent> getDestroy(){
        return this.destroy;
    }
    
    public void setDestroy( Signal<UIEvent> destroy ){
        this.destroy = destroy;
    }
    
}