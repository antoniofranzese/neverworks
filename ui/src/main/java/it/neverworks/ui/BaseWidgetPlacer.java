package it.neverworks.ui;

import static it.neverworks.language.*;
import it.neverworks.model.features.Placer;

public class BaseWidgetPlacer implements WidgetPlacer, Placer {
    private Widget widget;
    private Container container;
    
    public BaseWidgetPlacer( Container container, Widget widget ) {
        this.container = container;
        this.widget = widget;
    }

    public Container before( String name ) {
        if( container.contains( name ) ) {
            return before( container.widget( name ) );
        } else {
            throw new WidgetException( "Missing '" + name + "' child in " + container.getCommonName() );
        }
    }

    public Container before( Widget reference ) {
        int index = container.getChildren().indexOf( reference );
        if( index >= 0 ) {
            container.getChildren().add( index, widget );
            return container;
        } else {
            throw new WidgetException( "Missing " + reference.getCommonName() + " child in " + container.getCommonName() );
        }
    }
    
    public Container after( String name ) {
        if( container.contains( name ) ) {
            return after( container.widget( name ) );
        } else {
            throw new WidgetException( "Missing '" + name + "' child in " + container.getCommonName() );
        }
    }
    
    public Container after( Widget reference ){
        int index = container.getChildren().indexOf( reference );
        if( index >= 0 ) {
            container.getChildren().add( index + 1, widget );
            return container;
        } else {
            throw new WidgetException( "Missing " + reference.getCommonName() + " child in " + container.getCommonName() );
        }
    }
    
    public Container first() {
        container.getChildren().add( 0, widget );
        return container;
    }
    
    public Container last() {
        container.getChildren().add( widget );
        return container;
    }
    
    public Container at( int index ) {
        container.getChildren().add( index, widget );
        return container;
    }
    
    public void placeFirst() {
        first();
    }
    
    public void placeLast() {
        last();
    }
    
    public void placeAt( int index ) {
        at( index );
    }
    
    public void placeBefore( Object item ) {
        if( item instanceof String ) {
            before( (String) item );
        } else if( item instanceof Widget ) {
            before( (Widget) item );
        } else {
            throw new IllegalArgumentException( "Invalid object for before placement: " + repr( item ) );
        }
    }
    
    public void placeAfter( Object item ) {
        if( item instanceof String ) {
            after( (String) item );
        } else if( item instanceof Widget ) {
            after( (Widget) item );
        } else {
            throw new IllegalArgumentException( "Invalid object for after placement: " + repr( item ) );
        }
    }
    
    
}
