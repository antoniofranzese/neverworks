package it.neverworks.ui.production;

public class MissingStoredWidgetException extends ProductionException {
    
    public MissingStoredWidgetException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public MissingStoredWidgetException( String message ){
        super( message );
    }
}