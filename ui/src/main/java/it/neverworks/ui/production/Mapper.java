package it.neverworks.ui.production;

public interface Mapper {
    Class<? extends Producer> map( Class<?> widgetClass );
}