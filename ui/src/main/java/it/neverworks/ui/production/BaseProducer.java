package it.neverworks.ui.production;

import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.WidgetPersistedEvent;
import it.neverworks.ui.Widget;

public abstract class BaseProducer<T extends Widget> implements Producer<T> {
    
    protected Factory factory;

    protected Context context;
    
    public BaseProducer() {
    }

    public void produce( Widget widget ) { 
        Producer producer = factory.pick( widget ) ;
        producer.render( widget );
        ((WidgetLifeCycle) widget).notify( new WidgetPersistedEvent( this, widget ) );
    }

    public Context getContext(){
        return this.context;
    }
    
    public void setContext( Context context ){
        this.context = context;
    }
    
    public void setFactory( Factory factory ){
        this.factory = factory;
    }
    
    public String toString() {
        return this.getClass().getSimpleName();
    }
}