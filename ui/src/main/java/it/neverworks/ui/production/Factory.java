package it.neverworks.ui.production;

import it.neverworks.ui.Widget;

public interface Factory {
    <T extends Widget> Producer<T> pick( T widget );
}