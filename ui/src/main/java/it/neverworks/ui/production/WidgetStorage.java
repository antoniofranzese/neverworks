package it.neverworks.ui.production;

import it.neverworks.lang.Functional;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.ui.Widget;

public interface WidgetStorage<T extends Widget> extends Functional<T> {
    boolean contains( String key );
    String reserve( T widget );
    T load( String key );
    void save( String key, T widget );
    void delete( String key );
    ObjectQuery<T> query();
    default <W extends Widget> ObjectQuery<W> query( Class<W> type ) {
        return this.query().instanceOf( type );
    }
}