package it.neverworks.ui.production;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

public class Context implements Map<String, Object>{
    protected Map<String, Object> properties;
    
    public Context() {
        this.properties = new HashMap<String, Object>();
    }
    
    public Context( Map<String, Object> properties ) {
        this.properties = properties;
    }
    
    //TODO: expressions
    public <T> T get( String name ) {
        if( properties.containsKey( name ) ) {
            return (T)properties.get( name );
        } else {
            throw new ProductionException( "Missing '" + name + "' property in production context" );
        }
    }

    public <T> T get( String name, T defaultValue ) {
        if( properties.containsKey( name ) ) {
            return (T)properties.get( name );
        } else {
            return defaultValue;
        }
    }
    
    public Context set( String name, Object value ) {
        properties.put( name, value );
        return this;
    }
    
    public boolean contains( String name ) {
        return properties.containsKey( name );
    }
    
	public void clear () {
		properties.clear();
	}

	public Set<Entry<String, Object>> entrySet () {
		return properties.entrySet();
	}

	public Object get ( Object key ) {
	    return properties.get( key );
	}
	
	public boolean isEmpty () {
		return properties.isEmpty();
	}

	@Override
	public Set<String> keySet () {
		return properties.keySet();
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
		properties.putAll( m );
	}

	public Object remove ( Object key ) {
		return properties.remove( key );
	}

	public int size () {
		return properties.size();
	}

	public Collection<Object> values () {
		return properties.values();
	}

	@Override
	public boolean containsKey(Object key) {
		return properties.containsKey( key );
	}

	@Override
	public boolean containsValue(Object value) {
		return properties.containsValue( value );
	}

	@Override
	public Object put(String key, Object value) {
		return properties.put( key, value );
	}
    
}

