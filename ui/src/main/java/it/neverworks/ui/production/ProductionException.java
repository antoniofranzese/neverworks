package it.neverworks.ui.production;

public class ProductionException extends RuntimeException {
    
    public ProductionException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public ProductionException( String message ){
        super( message );
    }
}