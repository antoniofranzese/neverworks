package it.neverworks.ui.production;

import it.neverworks.ui.Widget;

public interface Producer<T extends Widget> {
    void produce( Widget widget );
    void render( T widget );
}