package it.neverworks.ui;

public class WidgetBuildException extends WidgetException {
    
    public WidgetBuildException( String message, Throwable cause ){
        super( message, cause );
    }
    
    public WidgetBuildException( String message ){
        super( message );
    }
}