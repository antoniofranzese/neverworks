package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Size;

public class Text extends Shape {

    @AutoConvert
    public enum HorizontalAlignment {
        LEFT, CENTER, RIGHT, START, END
    }

    @AutoConvert
    public enum VerticalAlignment {
        TOP, MIDDLE, BOTTOM
    }

    @AutoConvert
    public enum Baseline {
       ALPHABETIC, TOP, HANGING, MIDDLE, IDEOGRAPHIC
    }
    
    @Property
    protected Font font;

    @Property
    protected HorizontalAlignment halign;

    @Property
    protected VerticalAlignment valign;

    @Property
    protected Baseline baseline;
    
    @Property
    protected String value;

    @Property
    protected Size width;

    @Property
    protected Size height;
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public String getValue(){
        return this.value;
    }
    
    public void setValue( String value ){
        this.value = value;
    }

    public Baseline getBaseline(){
        return this.baseline;
    }
    
    public void setBaseline( Baseline baseline ){
        this.baseline = baseline;
    }
    
    public VerticalAlignment getValign(){
        return this.valign;
    }
    
    public void setValign( VerticalAlignment valign ){
        this.valign = valign;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
}