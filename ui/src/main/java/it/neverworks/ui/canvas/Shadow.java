package it.neverworks.ui.canvas;

import java.util.Iterator;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.ui.types.Color;

@Convert( Shadow.Converter.class )
public class Shadow implements Model {
    
    @Property
    protected Color color;

    @Property @AutoConvert
    protected Integer blur;

    @Property @AutoConvert
    protected Integer x;

    @Property @AutoConvert
    protected Integer y;
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY( Integer y ){
        this.y = y;
    }
    
    public Integer getX(){
        return this.x;
    }
    
    public void setX( Integer x ){
        this.x = x;
    }
    
    public Integer getBlur(){
        return this.blur;
    }
    
    public void setBlur( Integer blur ){
        this.blur = blur;
    }

    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }

    public static class Converter extends ModelConverter {
        public Object convert( Object value ) {
            if( value instanceof Iterable ) {
                Iterator it = ((Iterable) value).iterator();
                Shadow shadow = new Shadow();
                if( it.hasNext() ) {
                    shadow.set( "color", it.next() );
                }
                if( it.hasNext() ) {
                    shadow.set( "blur", it.next() );
                    if( ! it.hasNext() ) {
                        shadow.set( "x", shadow.get( "blur" ) );
                        shadow.set( "y", shadow.get( "blur" ) );
                    }
                }
                if( it.hasNext() ) {
                    shadow.set( "x", it.next() );
                    if( ! it.hasNext() ) {
                        shadow.set( "y", shadow.get( "x" ) );
                    }
                }
                if( it.hasNext() ) {
                    shadow.set( "y", it.next() );
                }
                return shadow;
            } else if( value instanceof String && ! Arguments.isParsable( value ) ) {
                //TODO: string format: color blur x y
                return new Shadow().set( "color", value );
            } else {
                return super.convert( value );
            }
        }
    }

}