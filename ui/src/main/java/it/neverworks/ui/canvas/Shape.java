package it.neverworks.ui.canvas;

import it.neverworks.lang.Arguments;
import it.neverworks.model.CoreModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Size;

public abstract class Shape extends CoreModel {

    public Shape() {
        super();
    }
    
    public Shape( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    @Property
    protected Point place;
    
    @Property @AutoConvert
    protected Integer rotation;
    
    @Property @AutoConvert
    protected Integer hscale;
    
    @Property @AutoConvert
    protected Integer vscale;

    @Property
    protected Stroke stroke;

    @Property
    protected Fill fill;

    @Property
    protected Shadow shadow;
    
    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }
    
    public Fill getFill(){
        return this.fill;
    }
    
    public void setFill( Fill fill ){
        this.fill = fill;
    }
    
    public Stroke getStroke(){
        return this.stroke;
    }
    
    public void setStroke( Stroke stroke ){
        this.stroke = stroke;
    }
    
    public Integer getVscale(){
        return this.vscale;
    }
    
    public void setVscale( Integer vscale ){
        this.vscale = vscale;
    }

    public Integer getHscale(){
        return this.hscale;
    }
    
    public void setHscale( Integer hscale ){
        this.hscale = hscale;
    }   

    public Integer getRotation(){
        return this.rotation;
    }
    
    public void setRotation( Integer rotation ){
        this.rotation = rotation;
    }

    public Point getPlace(){
        return this.place;
    }
    
    public void setPlace( Point place ){
        this.place = place;
    }
    
    public <T extends Shape> T set( String name, Object value ) {
        return (T) this.modelInstance.assign( name, value ).actual();
    }

    public <T extends Shape> T set( Arguments arguments ) {
        return (T) this.modelInstance.assign( arguments ).actual();
    }

    public <T extends Shape> T set( String arguments ) {
        return (T) this.modelInstance.assign( arguments ).actual();
    }

}