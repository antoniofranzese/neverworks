package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Setter;
import it.neverworks.model.converters.AutoConvert;

public class Star extends Shape {

    @Property @AutoConvert
    protected Integer spikes;

    @Property @AutoConvert
    protected Integer radius;

    @Property @AutoConvert @Access( setter = "setRatio" )
    protected Float ratio;
    
    protected void setRatio( Setter<Float> setter ) {
        if( setter.defined() && setter.value() > 1 ) {
            setter.set( setter.value() / 100.0F );
        } else {
            setter.set();
        }
    }

    public Float getRatio(){
        return this.ratio;
    }
    
    public void setRatio( Float ratio ){
        this.ratio = ratio;
    }
    
    public Integer getRadius(){
        return this.radius;
    }
    
    public void setRadius( Integer radius ){
        this.radius = radius;
    }
    
    public Integer getSpikes(){
        return this.spikes;
    }
    
    public void setSpikes( Integer spikes ){
        this.spikes = spikes;
    }
    
}