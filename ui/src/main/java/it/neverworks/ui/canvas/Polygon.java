package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class Polygon extends Shape {

    @Property @AutoConvert
    protected Integer sides;

    @Property @AutoConvert
    protected Integer radius;
    
    public Integer getRadius(){
        return this.radius;
    }
    
    public void setRadius( Integer radius ){
        this.radius = radius;
    }
    
    public Integer getSides(){
        return this.sides;
    }
    
    public void setSides( Integer sides ){
        this.sides = sides;
    }
    
}