package it.neverworks.ui.canvas;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.BaseWidget;

public class Canvas extends BaseWidget implements BorderLayoutCapable {
    
    public Canvas() {
        super();
    }

    public Canvas( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    @Property
    protected Scene scene;
    
    @Property
    protected LayoutInfo layout;

    @Property @AutoConvert
    protected Boolean visible;

    @Property
    protected Size width;

    @Property
    protected Size height;
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }

    
    public Boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( Boolean visible ){
        this.visible = visible;
    }

    public Scene getScene(){
        return this.scene;
    }
    
    public void setScene( Scene scene ){
        this.scene = scene;
    }

    public LayoutInfo getLayout(){
        return this.layout;
    }

    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }

}