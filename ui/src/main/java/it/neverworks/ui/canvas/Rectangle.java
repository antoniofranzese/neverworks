package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.types.Size;

public class Rectangle extends Shape {
    
    @Property
    protected Size width;

    @Property
    protected Size height;

    @Property @AutoConvert
    protected Integer radius;
    
    public Integer getRadius(){
        return this.radius;
    }
    
    public void setRadius( Integer radius ){
        this.radius = radius;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
}