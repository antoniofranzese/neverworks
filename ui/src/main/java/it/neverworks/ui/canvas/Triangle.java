package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.ui.types.Size;

public class Triangle extends Shape {
    
    @Property
    protected Size width;

    @Property
    protected Size height;
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
}