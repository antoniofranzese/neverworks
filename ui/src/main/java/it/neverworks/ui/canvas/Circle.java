package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class Circle extends Shape {
    
    @Property @AutoConvert
    protected Integer radius;

    @Property @AutoConvert
    protected Integer from;

    @Property @AutoConvert
    protected Integer to;
    
    public Integer getTo(){
        return this.to;
    }
    
    public void setTo( Integer to ){
        this.to = to;
    }
    
    public Integer getFrom(){
        return this.from;
    }
    
    public void setFrom( Integer from ){
        this.from = from;
    }

    public Integer getRadius(){
        return this.radius;
    }
    
    public void setRadius( Integer radius ){
        this.radius = radius;
    }
}