package it.neverworks.ui.canvas;

import it.neverworks.model.Property;
import it.neverworks.ui.types.Size;

public class Ring extends Circle {
    
    @Property
    protected Size width;
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }

}