package it.neverworks.ui.canvas;

import java.util.List;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;

public class Scene implements Model {

    @Property @Collection
    private List<Shape> shapes;

    public Scene add( Shape shape ) {
        //TODO: current layer
        List<Shape> shapes = get( "shapes" );
        shapes.add( shape );
        return this;
    }

    public Scene add( Class<? extends Shape> type, Arguments arguments ) {
        return add( Reflection.newInstance( type ).set( arguments ) );
    }

    public List<Shape> getShapes() {
        return this.shapes;
    }

    public Scene rectangle( Arguments arguments ) {
        return add( new Rectangle().set( arguments ) );
    }

    public Scene circle( Arguments arguments ) {
        return add( new Circle().set( arguments ) );
    }

    public Scene ring( Arguments arguments ) {
        return add( new Ring().set( arguments ) );
    }

    public Scene line( Arguments arguments ) {
        return add( new Line().set( arguments ) );
    }

    public Scene polygon( Arguments arguments ) {
        return add( new Polygon().set( arguments ) );
    }
    
    public Scene star( Arguments arguments ) {
        return add( new Star().set( arguments ) );
    }

    public Scene text( Arguments arguments ) {
        return add( new Text().set( arguments ) );
    }

    public Scene triangle( Arguments arguments ) {
        return add( new Triangle().set( arguments ) );
    }

}