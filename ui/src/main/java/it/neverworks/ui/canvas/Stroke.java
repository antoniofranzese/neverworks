package it.neverworks.ui.canvas;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;

@Convert( Stroke.Converter.class )
public class Stroke implements Model {

    @AutoConvert
    public static enum Cap {
        BUTT, ROUND, SQUARE
    }

    @AutoConvert
    public static enum Join {
        ROUND, BEVEL, MITER
    }
    
    @Property
    protected Color color;

    @Property
    protected Cap cap;

    @Property
    protected Join join;

    @Property
    protected Integer width;
    
    public Integer getWidth(){
        return this.width;
    }
    
    public void setWidth( Integer width ){
        this.width = width;
    }
    
    public Join getJoin(){
        return this.join;
    }
    
    public void setJoin( Join join ){
        this.join = join;
    }
        
    public Cap getCap(){
        return this.cap;
    }
    
    public void setCap( Cap cap ){
        this.cap = cap;
    }

    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }

    public static class Converter extends ModelConverter {
        public Object convert( Object value ) {
            if( value instanceof String && ! Arguments.isParsable( value ) ) {
                return new Stroke().set( "color", value );
            } else {
                return super.convert( value );
            }
        }
    }

}