package it.neverworks.ui.canvas;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.ui.types.Color;

@Convert( Fill.Converter.class )
public class Fill implements Model {
    
    @Property
    protected Color color;

    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }

    public static class Converter extends ModelConverter {
        public Object convert( Object value ) {
            if( value instanceof String && ! Arguments.isParsable( value ) ) {
                return new Fill().set( "color", value );
            } else {
                return super.convert( value );
            }
        }
    }

}