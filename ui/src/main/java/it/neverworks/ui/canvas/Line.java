package it.neverworks.ui.canvas;

import java.util.List;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;
import it.neverworks.ui.types.Point;

public class Line extends Shape {
    
    @Property @Collection
    protected List<Point> stops;

    @Property
    protected Point origin;
    
    public Point getOrigin(){
        return this.origin;
    }
    
    public void setOrigin( Point origin ){
        this.origin = origin;
    }
    
    @AutoConvert
    public void setStop( Point stop ){
        if( this.stops == null || this.stops.size() == 0 ) {
            this.getStops().add( stop );
        }
    }

    public List<Point> getStops(){
        return this.stops;
    }
    
    public void setStops( List<Point> stops ){
        this.stops = stops;
    }
}