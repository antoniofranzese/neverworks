package it.neverworks.ui.encoding;

import it.neverworks.ui.data.export.XHTMLWriter;

public class HTML extends it.neverworks.encoding.HTML {
    
    public HTML( String content ) {
       super( content );
    }
    
    public static XHTMLWriter init() {
        return new XHTMLWriter().init();
    }

    public static XHTMLWriter init( String declaration ) {
        return new XHTMLWriter().init( declaration );
    }

    public static XHTMLWriter start( String name ) {
        return new XHTMLWriter().start( name );
    }

    public static XHTMLWriter div() {
        return new XHTMLWriter().div();
    }
    
    public static XHTMLWriter span() {
        return new XHTMLWriter().span();
    }
    
    public static XHTMLWriter table() {
        return new XHTMLWriter().table();
    }

    public static XHTMLWriter image() {
        return new XHTMLWriter().image();
    }
    
    public static XHTMLWriter text( String text ) {
        return new XHTMLWriter().text( text );
    }

    public static XHTMLWriter bold() {
        return new XHTMLWriter().bold();
    }
    
    public static XHTMLWriter italic() {
        return new XHTMLWriter().italic();
    }

    public static XHTMLWriter underline() {
        return new XHTMLWriter().underline();
    }

    public static XHTMLWriter rule( String rule ) {
        return new XHTMLWriter().rule( rule );
    }

}