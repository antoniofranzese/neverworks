package it.neverworks.ui;

import java.util.List;
import java.util.Iterator;

import it.neverworks.lang.Filter;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.ui.util.WidgetArrayList;

public interface GenericContainer extends Container {
    
    default <T> T get( int index ) {
        List<Widget> children = getChildren();
        if( index >= 0 && index < children.size() ) {
            return (T)children.get( index );
        } else {
            throw new IllegalArgumentException( "Child index out of range (" + ( children.size() > 0 ? ( "0->" + ( children.size() - 1 ) ) : "empty" ) + "): " + index );
        }
    }

    default Container add( Widget... widgets ) {
        for( Widget widget: widgets ) {
            if( widget != null ) {
                addWidget( widget );
            }
        }
        return this;
    }
    
    default Container remove( Integer... indexes ) {
        for( Integer index: indexes ) {
            removeWidget( (Widget) get( index ) );
        }
        return this;
    }

    default Container remove( String... names ) {
        for( String name: names ) {
            removeWidget( widget( name ) );
        }
        return this;
    }

    default Container remove( Widget... widgets ) {
        for( Widget widget: widgets ) {
            removeWidget( widget );
        }
        return this;
    }
    
    default Container remove( Filter<Widget> filter ) {
        for( int i = this.size() - 1; i >= 0; i-- ) {
            boolean remove;
            try {
                remove = filter.filter( get( i ) );
            } catch( Exception ex ){
                remove = false;
            }
            
            if( remove ) {
                this.remove( i );
            }
        }
        return this;
    }
    
    default Container clear() {
        while( size() > 0 ) {
            remove( size() - 1 );
        }
        return this;
    }

    default boolean contains( Widget widget ) {
        return getChildren().contains( widget );    
    }
    
    default boolean contains( String name ) {
        return query().by( "name" ).eq( name ).any();
    }
    
    default boolean contains( int index ) {
        return index > 0 && index < getChildren().size();
    }

    default int size() {
        return getChildren().size();
    }
    
    default WidgetPlacer place( Widget widget ) {
        return new BaseWidgetPlacer( this, widget );
    }
    
    default <T> ObjectQuery<T> query( Class<T> cls ) {
        return (ObjectQuery<T>)(new ObjectQuery<Widget>( this ).byItem().instanceOf( cls ) );
    }

    default <T> ObjectQuery<T> query() {
        return (ObjectQuery<T>)(new ObjectQuery<Widget>( this ));
    }

    default WidgetSelection<Widget> children() {
        return new WidgetArrayList<Widget>( this.getChildren() );
    }

    default <T> WidgetSelection<T> children( Class<T> type ) {
        return new WidgetArrayList<T>( new ObjectQuery<Widget>( this.getChildren() ).instanceOf( type ) );
    }
    
    default Iterator<Widget> iterator() {
        return getChildren().iterator();
    }

    List<Widget> getChildren();    
    void addWidget( Widget widget );
    void removeWidget( Widget widget );    
}