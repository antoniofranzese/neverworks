package it.neverworks.ui;

public interface WidgetPlacer {

    Container before( Widget reference );
    Container before( String name );
    Container after( Widget reference );
    Container after( String name );
    Container at( int index );
    Container first();
    Container last();
    
}


