package it.neverworks.ui;

public interface Contained {
    
    void joinParent( Widget parent );
    void leaveParent( Widget parent );
}