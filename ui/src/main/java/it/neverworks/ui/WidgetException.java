package it.neverworks.ui;

public class WidgetException extends RuntimeException {
    
    public WidgetException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public WidgetException( String message ){
        super( message );
    }
}