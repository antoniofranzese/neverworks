package it.neverworks.ui.grid;


import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.lifecycle.UIEvent;

public class HeaderClickEvent<E> extends UIEvent<E> {
    
    protected Integer x;
    protected Integer y;
    protected Integer columnIndex;
    protected GridColumn column;
    protected Grid grid;
    
    @Property
    protected Attributes attributes;
    
    public Point getOrigin() {
        if( this.x != null && this.y != null ) {
            return new Point( this.x, this.y );
        } else {
            return null;
        }
    }

    public GridColumn getColumn(){
        if( column == null ) {
            column = this.columnIndex != null ? getGrid().getColumns().get( this.columnIndex ) : null;
        }
        return this.column;
    }
    
    public Grid getGrid(){
        if( this.grid == null ) {
            this.grid = this.source instanceof Grid ? (Grid) this.source : this.<Grid>get( "source.grid" );
        }
        return this.grid;
    }
    
    public Integer getColumnIndex(){
        return this.columnIndex;
    }
    
    public void setColumnIndex( Integer columnIndex ){
        this.columnIndex = columnIndex;
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY( Integer y ){
        this.y = y;
    }
    
    public Integer getX(){
        return this.x;
    }
    
    public void setX( Integer x ){
        this.x = x;
    }
    
    public Attributes getAttributes(){
        return this.attributes;
    }
    
    public void setAttributes( Attributes attributes ){
        this.attributes = attributes;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "column" ).toString();
    }
    
}