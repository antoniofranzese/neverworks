package it.neverworks.ui.grid;

import java.io.OutputStream;
import java.io.IOException;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.SafeModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.ui.data.export.XMLWriter;
import it.neverworks.ui.types.Decoration;

public class XMLExporter extends AbstractExporter {
    
    public String getDefaultName() {
        return "export.xml";
    }
    
    public String getDefaultMimeType() {
        return "text/xml";
    }
    
    private static class NameConverter implements Converter {
        public Object convert( Object value ) {
            return str( value ).replaceAll( "\\.", "-" ).replaceAll( "\\s", "_" );
        }
    }

    private static class AttributeConverter extends NameConverter implements InstanceConverter {
        public Object convert( ModelInstance instance, Object value ) {
            if( value instanceof Boolean ) {
                return value;
            } else {
                instance.set( "name", convert( value ) );
                return true;
            }
        }
    }

    private static class ExportArguments extends SafeModel {
        @Property @Convert( NameConverter.class ) String root = "items";
        @Property @Convert( NameConverter.class ) String item = "item";
        @Property @AutoConvert boolean html = false;
        @Property @AutoConvert String namespace = null;
    }

    private static class ColumnDefinition extends SafeModel {
        @Property @Convert( AttributeConverter.class ) boolean attribute = false;
        @Property @Convert( NameConverter.class ) String name;
        @Property @AutoConvert boolean raw = false;
        @Property @AutoConvert boolean cdata = false;
        GridColumn column;
    }
    
    public void export( OutputStream stream ) throws IOException {
        ExportArguments args = new ExportArguments()
            .set( Arguments.process( grid.get( "!attributes.xml" ) ) ) 
            .set( Arguments.process( grid.get( "!exporting.xml" ) ) ) 
            .set( parameters );                                        
        
        List<ColumnDefinition> elements = new ArrayList<ColumnDefinition>();
        List<ColumnDefinition> attributes = new ArrayList<ColumnDefinition>();

        int anonymousCount = 0;
        for( GridColumn column: getColumns() ) {
            ColumnDefinition def = new ColumnDefinition()
                .set( Arguments.process( column.get( "!attributes.xml" ) ) )
                .set( Arguments.process( column.get( "!exporting.xml" ) ) );
            
            def.column = column;
            
            if( empty( def.name ) && column.getSource() instanceof PropertyCell ) {
                def.set( "name", ((PropertyCell) column.getSource() ).getProperty() );
            }
            
            if( empty( def.name ) ) {
                def.set( "name", column.getName() );
            }

            if( empty( def.name ) ) {
                def.set( "name", HTML.strip( column.get( "!title.value" ) ) );
            }

            if( empty( def.name ) ) {
                anonymousCount += 1;
                def.set( "name", "anonymous" + anonymousCount );
            }
            
            def.set( "name", def.name.replaceAll( "\\s*", "_" ) );

            if( def.attribute ) {
                attributes.add( def );
            } else {
                elements.add( def );
            }
        }
            
        XMLWriter writer = new XMLWriter( stream );
        writer.init().start( args.root );
        
        if( Strings.hasText( args.namespace ) ) {
            writer.attr( "xmlns", args.namespace );
        }

        for( Object item: getSource().getCurrentItems() ) {
            
            boolean writeRowBody = true;
            Decoration rowDecoration = grid.decorate( item );
            if( rowDecoration != null && rowDecoration instanceof RowDecoration && rowDecoration.get( "interim" ) != null ) {
                writeRowBody = false;
            }
            
            if( writeRowBody ) {
                writer.start( args.item ).in();
            
                for( ColumnDefinition def: attributes ) {
                    Object value = def.column.format( item, "xml", "text" );
                    if( ! args.html && value instanceof String ) {
                        value = HTML.strip( (String) value );
                    }
                    writer.attr( def.name, value );
                }

                for( ColumnDefinition def: elements ) {
                    Object value = def.column.format( item, "xml", "text" );
                    if( ! args.html && ! def.raw && value instanceof String ) {
                        value = HTML.strip( (String) value );
                    }
                    writer.start( def.name ).in();
                    if( def.raw ) {
                        writer.raw( value );
                    } else if( def.cdata ) {
                        writer.cdata( value );
                    } else {
                        writer.text( value );
                    }
                    writer.end( def.name );
                }
            
                writer.end( args.item );
            }
        }
        
        writer.end( args.root );
        writer.close();

    }
    
    protected String normalizeName( String name ) {
        return name.replaceAll( "\\.", "-" ).replaceAll( "\\s", "_" );
    }
    
}