package it.neverworks.ui.grid;
 
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.DataSource;
 
public interface FormattedCell {
    
    Formatter getFormat();
    void setFormat( Formatter formatter );
    
    Object getFormattedValue( DataSource source, Object item );
}