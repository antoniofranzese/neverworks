package it.neverworks.ui.grid;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watcher;
import it.neverworks.model.description.Changes;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Defended;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.collections.Adder;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.BaseParentWidget;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.data.Inventory;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.DataSourceException;
import it.neverworks.ui.data.DataSourceKeyProvider;
import it.neverworks.ui.data.IndexedDataSource;
import it.neverworks.ui.data.WatchableDataSource;
import it.neverworks.ui.data.MissingItemException;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Decorator;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.types.Scrollability;
import it.neverworks.ui.lifecycle.ContainerActivationEvent;
import it.neverworks.ui.lifecycle.ContainerPassivationEvent;
import it.neverworks.ui.lifecycle.WidgetPersistedEvent;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.GenericContainer;
import it.neverworks.ui.Widget;

public class Grid extends BaseParentWidget implements GenericContainer, Retrieve, BorderLayoutCapable, DataSourceKeyProvider, Inventory, Flavored {
    
    @AutoConvert
    public static enum DividerPosition {
        HORIZONTAL, VERTICAL, BOTH, NONE
    }

    @Property @Watch
    protected GridHeader columns;

    @Property @Child
    protected DataSource items;
    
    @Property
    protected String key = "id";
    
    @Property
    protected Object selectedKey;
    
    @Property
    protected Object selected;
    
    @Property
    private Integer selectedIndex;
    
    @Property
    protected boolean detached;
    
    @Property
    private boolean preload = true;
    
    @Property
    private Integer pageSize; 
    
    @Property
    protected Signal<SelectEvent> select;
    
    @Property
    private Signal deselect;
    
    @Property
    private Signal<TouchEvent> touch;
    
    @Property
    protected Signal<RowClickEvent> inspect;

    @Property
    private Signal<RowClickEvent> click;
    
    @Property
    private Signal<GridChangeEvent> change;
    
    @Property
    protected LayoutInfo layout;
    
    @Property
    protected Size width;
    
    @Property
    protected Size height;
    
    @Property
    protected Font font;
    
    @Property
    protected Border border;
    
    @Property @Convert( GridDetailConverter.class )
    protected Form detail;
    
    @Property @Convert( GridDetailConverter.class )
    private Form editor;
    
    @Property
    private Boolean detailed;
    
    @Property @AutoConvert
    private Size scroll;
    
    @Property @AutoConvert
    private Integer clientScroll;
    
    @Property
    private SortConfiguration sort;
    
    @Property
    private RowSelector selector; 
    
    @Property
    private RowProcessor rows;
    
    @Property @AutoConvert
    private boolean autoDeselect = false; 
    
    @Property @Access( readable = false, writable = false )
    private List<Arguments> eventQueue;
    
    private boolean selecting = false;
    
    @Property @AutoConvert
    private Boolean visible; 
    
    @Property
    private Scrollability scrollable;
    
    @Property @Collection( wrap = true, split = "," )
    private List<DecorationPriority> priority;
    
    @Property
    private StyleArtifact flavor;
    
    @Property @Defended @Ignore
    private ExportingAttributes exporting;

    @Property
    protected DividerPosition dividers;

    @Property
    protected boolean autoTouch = true;
    protected int columnIdCounter = 0;
    protected Set<String> cellNames = new HashSet<String>();
    
    public Grid() {
        super();
        on( "startup", slot( "onStartup" ).unsigned() );
    }
    
    public Grid( Arguments arguments ) {
        this();
        set( arguments );
    }

    public Grid( Arguments arguments, GridColumn... columns ) {
        this();
        set( arguments );
        set( "columns", columns );
    }

    public Grid( GridColumn... columns ) {
        this();
        set( "columns", columns );
    }

    public List<Widget> getChildren() {
        List c = probe( "columns", () -> new GridHeader() );
        return (List<Widget>) c;
    }
    
    public void addWidget( Widget widget ) {
        if( widget instanceof GridColumn ) {
            getChildren().add( (GridColumn) widget );
        } else {
            throw new IllegalArgumentException( "Widget is not a GridColumn: " + widget );
        }
    }
    
    public void removeWidget( Widget widget ) {
        if( widget instanceof GridColumn ) {
            getChildren().remove( (GridColumn) widget );
        } else {
            throw new IllegalArgumentException( "Widget is not a GridColumn: " + widget );
        }
    }

    public ExportBuilder export() {
        return new ExportBuilder( this );
    }
    
    public ExportBuilder export( String format ) {
        return export().using( format );
    }
    
    protected void addColumnsItem( Adder<GridColumn> adder ) {
        if( adder.item() != null ) {
            Cell cell = adder.item().getSource();
            if( cell.getName() == null ) {
                cell.set( "name", "c" + columnIdCounter++ );
            }
            if( cellNames.contains( cell.getName() ) ) {
                throw new WidgetException( "Duplicate cell name: " + cell.getName() );
            } else {
                adder.add();
                cellNames.add( cell.getName() );
            }
        } else {
            adder.decline();
        }
    }
    
    protected void handleSelect( Handler<SelectEvent> handler ) {
        boolean doSelect = true;
        
        if( handler.event().getForeign() && handler.event().getNew() != null ) {
            RowClickEvent rowClickEvent = new RowClickEvent()
                .set( "x", handler.event().getX() )
                .set( "y", handler.event().getY() )
                .set( "id", handler.event().getNew() )
                .set( "rowIndex", handler.event().getRowIndex() != null ? handler.event().getRowIndex() : handler.event().getIndex() )
                .set( "columnIndex", handler.event().getColumnIndex() )
                .set( "attributes", handler.event().getAttributes() );
        
            getClick().fire( rowClickEvent );
        
            if( rowClickEvent.isStopped() ) {
                doSelect = false;
            }
        }

        if( doSelect ) {
            this.selecting = true;
            
            backupSelected();
            
            if( this.getItems() instanceof IndexedDataSource ) {
                if( handler.event().getIndex() != null ? !handler.event().getIndex().equals( this.selectedIndex ) : this.selectedIndex != null ) {
                    this.setSelectedIndex( handler.event().getIndex() );
                }
            } else {
                if( handler.event().getNew() != null ? !handler.event().getNew().equals( this.selectedKey ) : this.selectedKey != null ) {
                    this.setSelectedKey( handler.event().getNew() );
                }
            }

            within( Errors.spy( error -> restoreSelected() ), x ->{
                if( handler.handle().isStopped() ) {
                    restoreSelected();
                }
            });

            this.selecting = false;

        } else {
            handler.decline();
        }
        
    }

    protected Object savedSelected;
    
    protected void backupSelected() {
        if( getItems() instanceof IndexedDataSource ) {
            this.savedSelected = get( "selectedIndex" );
        } else {
            this.savedSelected = get( "selectedKey" );
        }
    }
    
    protected void restoreSelected() {
        if( getItems() instanceof IndexedDataSource ) {
            set( "selectedIndex", this.savedSelected );
        } else {
            set( "selectedKey", this.savedSelected );
        }
    }
    
    protected void handleClick( Handler<RowClickEvent> handler ) {
        GridColumn column = handler.event().getColumn();

        boolean handleOwnClick = true;

        if( column != null && column.getRowClick().size() > 0 ) {
            column.getRowClick().fire( handler.event() );
            handleOwnClick = handler.event().isBubbled();
            handler.event().halt();
        }
        
        if( handleOwnClick ) {
            handler.handle();
        } else {
            handler.decline();
        }

        if( ! handler.event().isStopped() ) {
            enqueueEvent( "row-click", 
                arg( "rowIndex", handler.event().getRowIndex() )
                .arg( "rowId", handler.event().getId() )
                .arg( "columnIndex", handler.event().getColumnIndex() )
                .arg( "x", handler.event().getX() )
                .arg( "y", handler.event().getY() )
            );
        }

    }

    protected void handleInspect( Handler<RowClickEvent> handler ) {
        GridColumn column = handler.event().getColumn();

        boolean handleOwnInspect = true;

        if( column.getRowInspect().size() > 0 ) {
            column.getRowInspect().fire( handler.event() );
            handleOwnInspect = handler.event().isBubbled();
            handler.event().halt();
        }
        
        if( handleOwnInspect ) {
            handler.handle();
        } else {
            handler.decline();
        }

    }
    
    protected void enqueueEvent( String name, Arguments state ) {
        if( this.eventQueue == null ) {
            this.eventQueue = new ArrayList<Arguments>();
        }
        this.eventQueue.add( 
            arg( "name", name )
            .arg( "state", state )
        );
        model().touch( "eventQueue" );
    }
    
    // TODO: prima di attivare, implementare la cache di ListDataSource con LRU
    // protected Object getSelected( Getter getter ) {
    //     DataSource items = getItems();
    //     if( items instanceof IndexedDataSource ) {
    //         Integer index = get( "selectedIndex" );
    //         if( index != null ) {
    //             return ((IndexedDataSource) items).pick( index );
    //         } else {
    //             return null;
    //         }
    //     } else {
    //         Object key = get( "selectedKey" );
    //         if( key != null ) {
    //             return items.load( this.key, key );
    //         } else {
    //             return null;
    //         }
    //     }
    // }
    //

    public int getSelectedIndexOf( Object value ) {
        if( items instanceof IndexedDataSource ) {
            Object key;
            try {
                key = items.get( value, this.key );
            } catch( Exception ex ) {
                key = value;
            }
            
            return ((IndexedDataSource) items).indexOf( this.key, key );
        } else {
            throw new IllegalStateException( "Current datasource is not indexed: " + repr( this.items ) );
        }
    }

    public void setSelectedIndexOf( Object value ) {
        set( "selectedIndex", getSelectedIndexOf( value ) );
    }
    
    protected void setSelected( Setter setter ) {
        if( setter.value() != null ) {
            if( setter.value() instanceof Integer && getItems() instanceof IndexedDataSource ) {
                set( "selectedIndex", setter.value() );
            } else {
                Object key;
                try {
                    key = getItems().get( setter.value(), this.key );
                } catch( Exception ex ) {
                    key = setter.value();
                }
                set( "selectedKey", key );
            }
        } else {
            this.set( getItems() instanceof IndexedDataSource ? "selectedIndex" : "selectedKey", null );
        }
        setter.decline();
    }
    
    protected void setSelectedKey( Setter setter ) {
        if( this.autoTouch ) {
            try {
                touch( get( "selected" ) );
            } catch( MissingItemException ex ) {}
        }
            
        Object previous = this.selectedKey;
        if( setter.value() != null ) {
            try {
                Object item;
                Integer index;
                DataSource items = getCurrentItems();
                if( items instanceof IndexedDataSource ) {
                    index = ((IndexedDataSource) items).indexOf( this.key, setter.value() );
                    item = ((IndexedDataSource) items).pick( index );
                } else {
                    index = null;
                    item = items.load( this.key, setter.value() );
                }

                setter.set( items.get( item, this.key ) );
                this.model().raw()
                    .set( "selectedIndex", index )
                    .set( "selected", item );

            
            } catch( DataSourceException ex ) {
                this.model().raw()
                    .set( "selectedIndex", null )
                    .set( "selectedKey", null )
                    .set( "selected", null );
                throw ex;
            }

        } else {
            setter.set();
            this.model().raw()
                .set( "selectedIndex", null )
                .set( "selected", null );
        }

        synthesizeSelect( previous );

    }

    protected void setSelectedIndex( Setter<Integer> setter ) {
        if( this.autoTouch ) {
            try {
                touch( get( "selected" ) );
            } catch( MissingItemException ex ) {}
        }
            
        Object previous = this.selectedKey;

        if( setter.value() != null && setter.value() >= 0 ) {
            DataSource items = getCurrentItems();
            if( items instanceof IndexedDataSource ) {
                Object item = ((IndexedDataSource) items).pick( setter.value() );
                this.model().raw()
                    .set( "selected", item )
                    .set( "selectedKey", items.get( item, this.key ) );
                setter.set();


            } else {
                this.model().raw()
                    .set( "selectedIndex", null )
                    .set( "selectedKey", null )
                    .set( "selected", null );
                throw new WidgetException( "Datasource is not indexed, cannot select index" );
            }
        } else {
            setter.set( null );
            this.model().raw()
                .set( "selected", null )
                .set( "selectedKey", null );
        }

        synthesizeSelect( previous );

    }

    protected void synthesizeSelect( Object previous ) {
        //System.out.println( "Synthesizing select " + getCommonName() + ", active " + active + ", started " + this.started );
        Arguments event = arg( "old", previous ).arg( "new", this.selectedKey ).arg( "index", this.selectedIndex );
        if( this.active && !this.selecting && !this.loading && ( previous != null ? !previous.equals( this.selectedKey ) : this.selectedKey != null ) ) {
            getSelect().fire( event );
        // } else if( ! this.started ) {
        //     System.out.println( "Scheduling startup select" );
        //     set( "attributes.pendingStartupSelect", event );
        }
    }
    
    protected void onStartup() {
        if( get( "!attributes.pendingStartupChange" ) != null ) {
            getChange().fire( getAttributes().remove( "pendingStartupChange" ) );
        }
        
        if( get( "!attributes.pendingStartupSelect" ) != null ) {
            Arguments event =  get( "!attributes.pendingStartupSelect" );
            getSelect().fire( getAttributes().remove( "pendingStartupSelect" ) );
        }
    }
    
    protected void setItems( Setter setter ) {
        Arguments event = arg( "oldItems", setter.raw() );
        
        setter.set();
        
        this.model().raw()
            .set( "selected", null )
            .set( "selectedIndex", null )
            .set( "selectedKey", null )
            .set( "sort", null );
        this.set( "scroll", 0 );
        
        event.arg( "newItems", setter.raw() );
        
        if( this.started ) {
            getChange().fire( event );
        } else {
            set( "attributes.pendingStartupChange", event );
        }

    }
    
    protected void setDetail( Setter<Form> setter ) {

        if( setter.value() != null && Boolean.FALSE.equals( detailed ) ) {
            throw new WidgetException( "Cannot activate details at runtime" );
            
        } else {
            if( setter.raw() != null ) {
                this.leaveWidget( setter.raw() );
            }
            
            if( setter.value() != null ) {
                set( "editor", null );
                this.joinWidget( setter.value(), "detail" );
            }
            
            setter.set();
        }
    }

    protected void setEditor( Setter<Form> setter ) {

        if( setter.value() != null && Boolean.FALSE.equals( detailed ) ) {
            throw new WidgetException( "Cannot activate editor at runtime" );
            
        } else {
            if( setter.raw() != null ) {
                this.leaveWidget( setter.raw() );
            }
            
            if( setter.value() != null ) {
                set( "detail", null );
                this.joinWidget( setter.value(), "editor" );
            }
            
            setter.set();
        }
    }
    
    protected Size getScroll( Getter<Size> getter ) {
        if( getter.defined() ) {
            return getter.get();
        } else {
            getter.decline();
            return this.clientScroll != null ? Size.pixels( this.clientScroll ) : null;
        }
    }
    
    protected void addPriorityItem( Adder<DecorationPriority> adder ) {
        if( adder.contains( adder.item() ) ) {
            adder.decline();
        } else {
            adder.add();
        }
    }
    
    protected void spreadOut( Event event ) {
        if( columns != null ) {
            columns.spreadIn( event );
        }

        if( detail != null ) {
            detail.spreadIn( event );
        }
        
        super.spreadOut( event );
    }
    
    protected void handleWidgetPersistedEvent( WidgetPersistedEvent event ) {
        super.handleWidgetPersistedEvent( event );
        if( detail != null || editor != null ) {
            detailed = true;
        } else {
            detailed = false;
        }
    }

    public GridColumn column( Object key ) {
        if( key instanceof Number ) {
            return getColumns().get( ((Number) key).intValue() );
        } else {
            return getColumns().get( Strings.valueOf( key ) );
        }
    }
    
    public <T> T item( Object key ) {
        return (T)getItems().load( this.key, key );
    }

    public <T> T select( Object key ) {
        set( "selected", key );
        return (T)this.selected;
    }
    
    public <T> T select( int index ) {
        if( getItems() instanceof IndexedDataSource ) {
            set( "selectedIndex", index );
            return (T)this.selected;
        } else {
            throw new WidgetException( "Datasource is not indexed" );
        }
    }
    
    public <T> T deselect() {
        T selected = (T) this.selected;
        signal( "deselect" ).fire();
        return selected;
    }
    
    protected void handleDeselect( Handler handler ) {
        try {
            this.selecting = true;
            if( getItems() instanceof IndexedDataSource ) {
                set( "selectedIndex", null );
            } else {
                set( "selectedKey", null );
            }
        } finally {
            this.selecting = false;
        }
            
        handler.handle();
        
    }
    
    public <T> T reselect() {
        return (T) select( deselect() );
    }
    
    public <T> T selected() {
        return (T)get( "selected" );
    }

    public void touch() {
        signal( "touch" ).fire();
    }
    
    public void handleTouch( Handler<TouchEvent> handler ) {
        handler.handle();
        
        if( ! handler.event().isStopped() ) {

            if( handler.event().get( "item" ) != null ) {

                if( getItems() instanceof WatchableDataSource ) {
                    ((WatchableDataSource) getItems()).touch( handler.event().get( "item" ) );
                }

            } else {

                getItems().touch();
                if( getSort() != null ) {
                    if( getSort().getForeign() ) {
                        raw( "sort", null );
                    } else {
                        getSort().touch();
                    }
                }

                try {
                    set( "selected", get( "selected" ) );

                } catch( MissingItemException ex ) {
                    this.model().raw()
                        .set( "selected", null )
                        .set( "selectedIndex", null )
                        .set( "selectedKey", null );
                }
                
            }
            
        }
    }
    
    public void sort( String expression ) {
        if( Strings.hasText( expression ) ) {
            set( "sort",  new SortConfiguration()
                .set( "descending", expression.startsWith( "-" ) )
                .set( "expression", expression )
            );
        } else {
            set( "sort", null );
        }
    }

    public void touch( Object item ) {
        if( item != null ) {
            signal( "touch" ).fire( arg( "item", item ) );
        }
    }
    
    public void scroll( Object scroll ) {
        this.set( "scroll", scroll );
    }
    
    public Decoration decorate( Object item ) {
        if( this.rows != null ) {
            return this.rows.decorate( item );
        } else {
            return null;
        }
    }
    
    public Object retrieveItem( Object key ) {
        if( key instanceof String && model().knows( (String) key ) ) {
            return model().get( (String) key );
        } else {
            return getItems().load( this.key, key );
        }
    }
    
    protected void setKey( Setter<String> setter ) {
        setter.set();
        if( items != null && items.getDefaultKey() == null ) {
            items.setDefaultKey( setter.value() );
        }
    }

    public String getDataSourceKey() {
        return this.key;
    }

    protected void watch( Watcher watch ) {    
        if( getItems() instanceof WatchableDataSource ) {
            ((WatchableDataSource) getItems()).watch();
        }
    }
    
    protected void ignore( Watcher ignore ) {    
        if( getItems() instanceof WatchableDataSource ) {
            ((WatchableDataSource) getItems()).ignore();
        }
    }
    
    protected Changes changes( Changes changes ) {
        if( getItems() instanceof WatchableDataSource
            && !( changes.contains( "items" ) )  
            && ((WatchableDataSource) getItems()).changes().size() > 0 ) {
            changes.add( "changedItems" );
        }
        return changes;
    }
    
    protected void setSort( Setter<SortConfiguration> setter ) {
        SortConfiguration value = setter.value();
        if( value != null && value.equals( sort ) ) {
            //System.out.println( "Ignoring sort, already " + setter.raw().getExpression() );
            setter.decline();
        } else {
            if( value != null ) {
                value.set( "grid", this );
            }
            setter.set();
        }

        setter.ignore();

        if( model().watching() ) {
            model().touch( "items" );
        }
    }
    
    /* Virtual properties */
    
    @Virtual
    public GridHeader getHeader(){
        return this.getColumns();
    }
    
    public void setHeader( GridHeader header ){
        this.setColumns( header );
    }
    
    @Virtual
    public DataSource getChangedItems() {
        return ((WatchableDataSource) getItems()).changes() ;
    }
    
    @Virtual
    public DataSource getCurrentItems() {
        if( getSort() != null ) {
            return getSort().getItems();
        } else {
            return getItems();
        }
    }

    @Virtual
    public Object getItem(){
        return get( "selected" );
    }

    public void setItem( Object item ){
        set( "selected", item );
    }

    @Virtual
    public Object getValue(){
        return getSelectedKey();
    }

    public void setValue( Object value ){
        setSelectedKey( value );
    }

    @Virtual
    public void setTouched( Object value ) {
        if( value instanceof Boolean ) {
            if( Boolean.TRUE.equals( value ) ) {
                this.touch();
            }
        } else {
            this.touch( value );
        }
    }
    /* Bean Accessors */
    
    public Signal getDeselect(){
        return this.deselect;
    }
    
    public void setDeselect( Signal deselect ){
        this.deselect = deselect;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }

    public DataSource getItems(){
        return this.items;
    }
    
    public void setItems( DataSource items ){
        this.items = items;
    }

    public GridHeader getColumns(){
        return this.columns;
    }
    
    public void setColumns( GridHeader columns ){
        this.columns = columns;
    }
    
    public String getKey(){
        return this.key;
    }
    
    public void setKey( String key ){
        this.key = key;
    }
    
    public Object getSelectedKey() {
        return this.selectedKey;
    }
    
    public void setSelectedKey( Object selectedKey ){
        this.selectedKey = selectedKey;
    }

    public Object getSelected(){
        return this.selected;
    }
    
    public void setSelected( Object selected ){
        this.selected = selected;
    }
    
    public Integer getSelectedIndex(){
        return this.selectedIndex;
    }
    
    public void setSelectedIndex( Integer selectedIndex ){
        this.selectedIndex = selectedIndex;
    }
    
    public Signal<SelectEvent> getSelect(){
        return this.select;
    }
    
    public void setSelect( Signal<SelectEvent> change ){
        this.select = select;
    }
    
    public Signal<RowClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<RowClickEvent> click ){
        this.click = click;
    }
    
    public Signal<RowClickEvent> getInspect(){
        return this.inspect;
    }
    
    public void setInspect( Signal<RowClickEvent> inspect ){
        this.inspect = inspect;
    }
    
    public boolean getDetached(){
        return this.detached;
    }
    
    public void setDetached( boolean detached ){
        this.detached = detached;
    }

    public boolean getPreload(){
        return this.preload;
    }
    
    public void setPreload( boolean preload ){
        this.preload = preload;
    }
    
    public Integer getPageSize(){
        return this.pageSize;
    }
    
    public void setPageSize( Integer pageSize ){
        this.pageSize = pageSize;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public Form getDetail(){
        return this.detail;
    }
    
    public void setDetail( Form detail ){
        this.detail = detail;
    }
    
    public Form getEditor(){
        return this.editor;
    }
    
    public void setEditor( Form editor ){
        this.editor = editor;
    }
    
    public Boolean getDetailed(){
        return this.detailed;
    }
    
    public void setDetailed( Boolean detailed ){
        this.detailed = detailed;
    }
    
    public Size getScroll(){
        return this.scroll;
    }
    
    public void setScroll( Size scroll ){
        this.scroll = scroll;
    }
    
    public SortConfiguration getSort(){
        return this.sort;
    }
    
    public void setSort( SortConfiguration sort ){
        this.sort = sort;
    }
    
    public RowSelector getSelector(){
        return this.selector;
    }
    
    public void setSelector( RowSelector selector ){
        this.selector = selector;
    }
    
    public RowProcessor getRows(){
        return this.rows;
    }
    
    public void setRows( RowProcessor rows ){
        this.rows = rows;
    }
    
    public boolean getAutoDeselect(){
        return this.autoDeselect;
    }
    
    public void setAutoDeselect( boolean autoDeselect ){
        this.autoDeselect = autoDeselect;
    }
    
    public Boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( Boolean visible ){
        this.visible = visible;
    }
    
    public Scrollability getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( Scrollability scrollable ){
        this.scrollable = scrollable;
    }
    
    public Signal getTouch(){
        return this.touch;
    }
    
    public void setTouch( Signal touch ){
        this.touch = touch;
    }
    
    public List<DecorationPriority> getPriority(){
        return this.priority;
    }
    
    public void setPriority( List<DecorationPriority> priority ){
        this.priority = priority;
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public Signal<GridChangeEvent> getChange(){
        return this.change;
    }
    
    public void setChange( Signal<GridChangeEvent> change ){
        this.change = change;
    }
    
    public DividerPosition getDividers(){
        return this.dividers;
    }
    
    public void setDividers( DividerPosition dividers ){
        this.dividers = dividers;
    }

    public boolean getAutoTouch(){
        return this.autoTouch;
    }
    
    public void setAutoTouch( boolean autoTouch ){
        this.autoTouch = autoTouch;
    }

}
