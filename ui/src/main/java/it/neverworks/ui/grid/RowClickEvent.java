package it.neverworks.ui.grid;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.types.Point;

public class RowClickEvent<T> extends HeaderClickEvent<T> {
    
    protected Object id;
    protected Object item;
    private Integer rowIndex;
    
    public Object getItem(){
        if( item == null ) {
            item = this.id != null ? getGrid().item( this.id ) : null;
        }
        return this.item;
    }
    
    public Object getId(){
        return this.id;
    }
    
    public void setId( Object id ){
        this.id = id;
    }
    
    public Object selected() {
        return getItem();
    }
    
    public Object getSelected() {
        return getItem();
    }
    
    public Integer getRowIndex(){
        return this.rowIndex;
    }
    
    public void setRowIndex( Integer rowIndex ){
        this.rowIndex = rowIndex;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "source" )
            .add( "id" )
            .add( "x" )
            .add( "y" )
            .toString();
    }
    
}