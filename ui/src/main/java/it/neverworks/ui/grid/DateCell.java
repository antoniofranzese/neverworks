package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;

public class DateCell extends PropertyCell {
    
    protected String pattern;
    
    public DateCell() {
        super();
        this.type = "date";
    }
    
    public DateCell( String property ) {
        super();
        this.property = property;
        this.type = "date";
    }

    public DateCell( String property, String pattern ) {
        super();
        this.property = property;
        this.pattern = pattern;
        this.type = "date";
    }
    
    public DateCell( Arguments arguments ) {
        super();
        if( ! arguments.containsKey( "type" ) ) {
            arguments.arg( "type", "date" );
        }
        set( arguments );
    }
    
    public String getPattern(){
        return this.pattern;
    }
    
    public void setPattern( String pattern ){
        this.pattern = pattern;
    }
    
    
    
}