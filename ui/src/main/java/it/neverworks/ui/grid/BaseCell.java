package it.neverworks.ui.grid;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.types.Decorator;

public abstract class BaseCell extends BaseWidget implements Cell {
    
    @Property
    protected GridColumn column;

    @Property
    private Decorator cell;
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        this.column = model.as( GridColumn.class );
    }

    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        this.column = null;
    }
    
    public GridColumn getColumn(){
        return this.column;
    }

    public Decorator getCell(){
        return this.cell;
    }
    
    public void setCell( Decorator cell ){
        this.cell = cell;
    }
    
}