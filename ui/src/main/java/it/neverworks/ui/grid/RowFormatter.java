package it.neverworks.ui.grid;

public interface RowFormatter<T> {
    RowStyle format( T item );
}