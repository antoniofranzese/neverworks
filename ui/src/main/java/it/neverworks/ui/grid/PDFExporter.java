package it.neverworks.ui.grid;

import java.util.Date;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.IOException;
import java.io.File;
import it.neverworks.log.Logger;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.FormattingWriter;
import it.neverworks.encoding.HTML;
import it.neverworks.model.Property;
import it.neverworks.model.SafeModel;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.context.Inject;
import it.neverworks.model.expressions.Template;
import it.neverworks.security.context.UserInfo;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.LookupFormatter;
import it.neverworks.ui.data.export.XHTMLWriter;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.types.Font;
import it.neverworks.printing.XHTMLDocumentPrinter;
import it.neverworks.template.DateField;

public class PDFExporter extends AbstractExporter {
    
    private static final Logger logger = Logger.of( PDFExporter.class );

    @Property @Inject
    private UserInfo userInfo;

    private final static LookupFormatter HALIGN = new LookupFormatter()
        .add( GridColumn.HorizontalAlignment.LEFT, " left" )
        .add( GridColumn.HorizontalAlignment.CENTER, " center" )
        .add( GridColumn.HorizontalAlignment.RIGHT, " right" )
        .set( "empty", "" );

    private final static LookupFormatter VALIGN = new LookupFormatter()
        .add( GridColumn.VerticalAlignment.TOP, " top" )
        .add( GridColumn.VerticalAlignment.MIDDLE, " middle" )
        .add( GridColumn.VerticalAlignment.BOTTOM, " bottom" )
        .set( "empty", "" );

    private final static String PAGE = new XHTMLWriter().start( "span" ).attr( "class", "page" ).toString();
    private final static String PAGECOUNT = new XHTMLWriter().start( "span" ).attr( "class", "pagecount" ).toString();

    public String getDefaultName() {
        return "export.pdf";
    }
    
    public String getDefaultMimeType() {
        return "application/pdf";
    }
    
    private static class ExportArguments extends SafeModel {
        @Property @AutoConvert boolean borders = true;
        @Property @AutoConvert boolean portrait = false;
        @Property @AutoConvert boolean debug = false;
        @Property @AutoConvert boolean banners = true;
        @Property @AutoConvert boolean headers = true;
        @Property @AutoConvert String css = "";
        @Property @AutoConvert String header;
        @Property @AutoConvert String footer;
        @Property @AutoConvert String title;
        @Property Object resources;
        @Property Font font;
    }
    
    public void export( OutputStream stream ) throws IOException {

        ExportArguments args = new ExportArguments()
            .set( Arguments.process( grid.get( "!attributes.pdf" ) ) ) 
            .set( Arguments.process( grid.get( "!exporting.pdf" ) ) ) 
            .set( parameters );                                        
            
        Arguments variables = 
            arg( "user", userInfo )
            .arg( "now", new Date() )
            .arg( "page", PAGE )
            .arg( "pages", PAGECOUNT )
            .arg( "title", Strings.hasText( args.title ) ? args.title : "" );
        
        File htmlFile = File.createTempFile( "nwk-grid-export-",".html" );
        if( logger.isDebugEnabled() ) {
            logger.debug( "HTML Output to: {}", htmlFile.getAbsolutePath() );
        }

        try {
            
            XHTMLWriter html = new XHTMLWriter( htmlFile );

            html
                .init()
                .start( "html" )
                    .attr( "xmlns", "http://www.w3.org/1999/xhtml" )
                    .attr( "xmlns:html", "http://www.w3.org/1999/xhtml" )
                .start( "head" ).in()
                .start( "style" ).in()

                .rule( "@page" )
                    .style( "size", msg( "a4 {0}", args.portrait ? "portrait" : "landscape" ) )
                
                .rule( "body" )
                    .style( "font-family", "sans-serif" )
                    .style( "font-size", "10pt" )
                    
                .rule( "h1.title" )
                    .style( "text-align", "center" )
                
                .rule( "table.main" )
                    .style( "width", "100%" )
                    .style( "border-spacing", "0px" )
                    .style( "-fs-table-paginate", "paginate" )
                .rule( "table.main tr" )
                    .style( "page-break-inside", "avoid" )

                .rule( "th.header" )
                    .style( "padding", "1px 4px 2px 4px" )
                .rule( "td.cell" )
                    .style( "padding", "2px 4px 2px 4px" )
                    .style( "vertical-align", "top" )

                .rule( "td.cell.left" )
                    .style( "text-align", "left" )
                .rule( "td.cell.center" )
                    .style( "text-align", "center" )
                .rule( "td.cell.right" )
                    .style( "text-align", "right" )

                .rule( "td.cell.top" )
                    .style( "vertical-align", "top" )
                .rule( "td.cell.middle" )
                    .style( "vertical-align", "middle" )
                .rule( "td.cell.bottom" )
                    .style( "vertical-align", "bottom" )
                
                .rule( "td.cell.banner" )
                    .style( "padding", "2px 8px 2px 8px" )
                    
                .rule( "span.page:before" )
                    .style( "content", "counter(page)" )
                .rule( "span.pagecount:before" )
                    .style( "content", "counter(pages)" )
                    
                .rule( "div.page-header" )
                    .style( "padding-top", "30pt" )
                .rule( "div.page-footer" )
                    .style( "padding-bottom", "30pt" )
                .rule( "div.page-header, div.page-footer" )
                    .style( "font-size",  "8pt" )
                
                ;

            if( args.borders ) {
                html
                    .rule( "table.main" )
                        .style( "border", "1px solid black" )
                    .rule( "table.main > tr > td" )
                        .style( "border-left", "0.3pt solid black" )
                        .style( "border-top", "0.3pt solid black" )
                    .rule( "table.main > tr > td:first-child" )
                        .style( "border-left-width", "0px" );
            }

            if( args.font != null ) {
                html.rule( "body" ).style( args.font );
            }
            
            if( Strings.hasText( args.header ) ) {
                html.raw( "@page { margin-top: 50pt; @top-center { content: element(current); } }\n" );
            }

            if( Strings.hasText( args.footer ) ) {
                html.raw( "@page { margin-bottom: 50pt; @bottom-right { content: element(footer); } }\n" );
            }

            if( Strings.hasText( args.css ) ) {
                html.raw( "\n/* Custom CSS */\n" );
                html.raw( args.css );
                html.raw( "\n/* Custom CSS End */\n" );
            }
            
            html
                .end( "style" )
                .end( "head" )
                .start( "body" ).in();

            if( Strings.hasText( args.header ) ) {
                html
                    .start( "div" ).in()
                        .attr( "style", "position: running(current);" )
                    .start( "div" ).in()
                        .attr( "class", "page-header" )

                    .raw( new Template( args.header ).apply( variables ) )

                    .end()
                    .end();
            }

            if( Strings.hasText( args.footer ) ) {
                html
                    .start( "div" ).in()
                        .attr( "style", "position: running(footer);" )
                    .start( "div" ).in()
                        .attr( "class", "page-footer" )

                    .raw( new Template( args.footer ).apply( variables ) )

                    .end()
                    .end();
            }

            if( Strings.hasText( args.title ) ) {
                html
                    .start( "div" ).in()
                        .attr( "class", "page-title" )
                    .start( "h1" )
                        .attr( "class", "title" )
                    
                    .raw( new Template( args.title ).apply( variables ) )

                    .end()
                    .end();
            }

            html
                .start( "table" ).in()
                    .attr( "class", "main" );

            if( args.headers ) {
                html
                    .start( "thead" ).in()
                    .start( "tr").in();
                
                for( GridColumn column: getColumns() ) {
                    html
                        .start( "th" ).in()
                            .attr( "class", "header" )
                            .text( column.get( "!exporting.pdf.title", column.get( "!title.value" ) ) )
                        .end();
                }

                html
                    .end( "tr" )
                    .end( "thead" );
            }

            int count = 0;
            for( Object item: getSource().getCurrentItems() ) {

                boolean writeRowBody = true;
                Decoration rowDecoration = grid.decorate( item );
                
                if( args.banners && rowDecoration != null && rowDecoration instanceof RowDecoration ) {
                    Banner interimBanner = rowDecoration.get( "interim" );
                    if( interimBanner != null ) {
                        writeRowBody = false;
                        writeBanner( html, interimBanner );
                        
                    }
                }

                if( writeRowBody ) {
                    
                    if( args.banners && rowDecoration != null && rowDecoration instanceof RowDecoration ) {
                        writeBanner( html, rowDecoration.<Banner>get( "before" ) );
                    }

                    html.start( "tr" ).in();
                
                    for( GridColumn column: getColumns() ) {
                        Object value = column.format( item, "pdf", "html" );

                        if( !( column.getSource() instanceof HTMLCell ) ) {
                            value = HTML.encode( value );
                        }

                        html
                            .start( "td" ).in()
                                .attr( "class", msg( "cell{0}{1}"
                                    ,HALIGN.format( column.getHalign() )
                                    ,VALIGN.format( column.getValign() ) 
                                ))
                                .style( column.getFont() )
                                .raw( value )
                            .end();    
                    }
            
                    html.end( "tr" );
                    
                    if( args.banners && rowDecoration != null && rowDecoration instanceof RowDecoration ) {
                        writeBanner( html, rowDecoration.<Banner>get( "after" ) );
                    }
                    
                }

                count++;
                if( count >= 50 ) {
                    html.flush();
                    count = 0;
                }
            }
            html.end( "table" );

            html.close();

            new XHTMLDocumentPrinter(
                arg( "resources", args.get( "resources", "classpath:resources" ) )
            )
            .render( htmlFile )
            .to( stream );

        } finally {
            if( ! args.debug && htmlFile != null && htmlFile.exists() ) {
                htmlFile.delete();
            }
        }
    }
    
    protected void writeBanner( XHTMLWriter html, Banner banner ) {
        if( banner != null ) {
            String content = banner.getEncoding() == Banner.Encoding.HTML ? banner.getContent() : HTML.encode( banner.getContent() );
            html
                .start( "tr" ).in()
                    .start( "td" ).in()
                        .attr( "class", msg( "cell{0}{1} banner"
                            ,HALIGN.format( banner.getHalign() )
                            ,VALIGN.format( banner.getValign() ) 
                        ))
                        .attr( "colspan", getColumns().size() )
                        .style( "padding", banner.getPadding() )
                        .style( "height", banner.getHeight() )
                        .style( banner.getFont() )
                        .raw( content )
                    .end()
                .end();
                    
        }
    }
}