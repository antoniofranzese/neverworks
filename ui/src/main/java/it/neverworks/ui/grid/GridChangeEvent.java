package it.neverworks.ui.grid;

import it.neverworks.model.Property;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.lifecycle.UIEvent;

public class GridChangeEvent extends UIEvent {
    
    @Property
    private DataSource oldItems;
    
    @Property
    private DataSource newItems;
    
    public DataSource getNewItems(){
        return this.newItems;
    }

    public DataSource getItems(){
        return this.newItems;
    }
    
    public void setNewItems( DataSource newItems ){
        this.newItems = newItems;
    }
    
    public DataSource getOldItems(){
        return this.oldItems;
    }
    
    public void setOldItems( DataSource oldItems ){
        this.oldItems = oldItems;
    }

}