package it.neverworks.ui.grid;

import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.Recipe;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;

public class GridSource implements Model {
    
    @Property
    private Grid grid;
    
    @Property
    private List<GridColumn> columns = new ArrayList<GridColumn>();
    
    @Property
    private Arguments parameters = new Arguments();
    
    @Property
    private Recipe recipe;
    
    public Recipe getRecipe(){
        return this.recipe;
    }
    
    public void setRecipe( Recipe recipe ){
        this.recipe = recipe;
    }
    
    public Arguments getParameters(){
        return this.parameters;
    }
    
    public void setParameters( Arguments parameters ){
        this.parameters = parameters;
    }
    
    public List<GridColumn> getColumns(){
        return this.columns;
    }
    
    public void setColumns( List<GridColumn> columns ){
        this.columns = columns;
    }
    
    public Grid getGrid(){
        return this.grid;
    }
    
    public void setGrid( Grid grid ){
        this.grid = grid;
    }

    public Iterable getCurrentItems() {
        return this.recipe == null ? this.grid.getCurrentItems() : this.recipe.apply( this.grid.getCurrentItems() );
    }
}