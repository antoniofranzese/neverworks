package it.neverworks.ui.grid;

import it.neverworks.model.SafeModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Size;

@Convert( BannerConverter.class )
public class Banner extends SafeModel {
    
    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }
    
    @AutoConvert
    public static enum VerticalAlignment {
        TOP, MIDDLE, BOTTOM
    }
    
    @AutoConvert
    public static enum Encoding {
        TEXT, HTML
    }
    
    @Property @AutoConvert
    private String content;
    
    @Property
    private Font font;
    
    @Property
    private Color color;
    
    @Property
    private Color background;
    
    @Property
    private Box padding;
    
    @Property
    private Size height;
    
    @Property
    private HorizontalAlignment halign;
    
    @Property
    private VerticalAlignment valign;
    
    @Property
    private Encoding encoding;
    
    public Encoding getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( Encoding encoding ){
        this.encoding = encoding;
    }
    
    public VerticalAlignment getValign(){
        return this.valign;
    }
    
    public void setValign( VerticalAlignment valign ){
        this.valign = valign;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public String getContent(){
        return this.content;
    }
    
    public void setContent( String content ){
        this.content = content;
    }
}