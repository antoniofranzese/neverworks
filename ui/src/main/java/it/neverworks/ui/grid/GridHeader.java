package it.neverworks.ui.grid;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Modelize;
import it.neverworks.model.Property;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.collections.ArrayList;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.ui.lifecycle.ErrorHandler;
import it.neverworks.ui.lifecycle.ErrorEvent;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.Widget;
import it.neverworks.ui.Evented;

@Modelize
@Watchable
@Collection( child = true, item = GridColumn.class )
public class GridHeader extends ArrayList<GridColumn> implements Retrieve, Evented, ErrorHandler {

    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }
    
    @Property
    private Signal<HeaderClickEvent> inspect;

    @Property
    private Grid grid;
    
    @Property @AutoConvert
    private boolean visible = true; 
    
    @Property
    private Color color;
    
    @Property
    private Color background;
    
    @Property
    private Font font;
    
    @Property 
    private HorizontalAlignment halign;
    
    @Property
    private Size height;

    @Property
    protected Grid.DividerPosition dividers;

    public GridHeader() {
        super();
    }
    
    public GridHeader( Widget... columns ) {
        super();
        populateColumns( columns );
    }

    public GridHeader( Arguments arguments, Widget... columns ) {
        super();
        populateColumns( columns );
        set( arguments );
    }
    
    public <T extends Event> Signal<T> event( String name ) {
        return (Signal<T>) ModelInstance.of( this ).get( name );
    }
    
    public Object retrieveItem( Object key ) {
        if( key instanceof Number ) {
            return get( ((Number) key).intValue() );
        } else if( key instanceof String ){
            try {
                return get( Integer.parseInt( (String)key ) );
            } catch( NumberFormatException ex ) {
                GridColumn column = query().by( "name" ).eq( key ).result();
                if( column != null ) {
                    return column;
                } else {
                    return ModelInstance.of( this ).get( (String)key );
                }
            }
        } else {
            throw new IllegalArgumentException( "Unknown key: " + Objects.repr( key ) );
        }
    }

    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        super.adoptModel( model, property );
        this.grid = model.actual();
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        this.grid = null;
        super.orphanModel( model, property );
    }
    
    protected void set( Arguments arguments ) {
        for( String name: arguments.keys() ) {
            ModelInstance.of( this ).assign( name, arguments.get( name ) );
        }
    }

    public void notifyError( ErrorEvent event ) {
        this.grid.bubbleIn( event );
    }

    protected void populateColumns( Widget... columns ) {
        for( Widget column: columns ) {
            if( column instanceof GridColumn ) {
                add( (GridColumn) column );
            } else if( column != null ){
                throw new IllegalArgumentException( "Invalid column: " + Objects.repr( column ) );
            }
        }
    }

    protected void spreadOut( Event event ) {
        for( GridColumn column: this ) {
            column.spreadIn( event );
        }
    }

    public void spreadIn( Event event ) {
        notify( event );
        spreadOut( event );
    }
    
    public Event notify( Event event ) {
        return event;
    }
    
    public GridColumn get( String name ) {
        for( GridColumn column: this ) {
            if( name.equals( column.getName() ) ) {
                return column;
            }
        }
        throw new IllegalArgumentException( "Cannot find column by name: " + name );
    }

    public Signal<HeaderClickEvent> getInspect(){
        return this.inspect;
    }

    public void setInspect( Signal<HeaderClickEvent> inspect ){
        this.inspect = inspect;
    }

    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }

    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Grid.DividerPosition getDividers(){
        return this.dividers;
    }
    
    public void setDividers( Grid.DividerPosition dividers ){
        this.dividers = dividers;
    }
     
}