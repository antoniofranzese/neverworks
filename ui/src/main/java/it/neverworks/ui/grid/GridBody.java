package it.neverworks.ui.grid;

import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Watchable;
import it.neverworks.ui.types.Border;   

@Watchable
public class GridBody implements Model {
    
    @Property
    protected Border border;
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
}