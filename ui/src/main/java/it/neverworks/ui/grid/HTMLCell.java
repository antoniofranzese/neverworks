package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Properties;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.ui.data.ItemFormatter;

public class HTMLCell extends PropertyCell {

    @Property
    protected boolean secure = true; 
    
    public boolean getSecure(){
        return this.secure;
    }
    
    public void setSecure( boolean secure ){
        this.secure = secure;
    }
    
    public HTMLCell() {
        super();
    }
    
    public HTMLCell( String property ) {
        super( property );
    }
    
    public HTMLCell( Arguments arguments ) {
        super();
        set( arguments );
    }

    public HTMLCell( ItemFormatter formatter ) {
        super( formatter );
    }
   
}