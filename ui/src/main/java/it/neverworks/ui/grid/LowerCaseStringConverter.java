package it.neverworks.ui.grid;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;

public class LowerCaseStringConverter implements Converter {
    
    public Object convert( Object value ) {
        return Strings.safe( value ).trim().toLowerCase();
    }

}