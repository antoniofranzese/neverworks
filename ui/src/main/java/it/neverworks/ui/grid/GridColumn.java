package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Required;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.utils.EqualsBuilder;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Event;

import it.neverworks.ui.form.Label;
import it.neverworks.ui.lifecycle.StartupEvent;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.BaseWidget;

@Convert( GridColumnConverter.class )
public class GridColumn extends BaseWidget implements ChildModel {
    
    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }
    
    @AutoConvert
    public static enum VerticalAlignment {
        TOP, MIDDLE, BOTTOM
    }
    
    @Property 
    @Required 
    @Child 
    @Convert( SourceCellConverter.class )
    protected Cell source;
    
    @Property
    protected Label title;

    @Property
    protected Size width;
    
    @Property
    protected Size height;
    
    @Property
    protected HorizontalAlignment halign;
    
    @Property
    protected VerticalAlignment valign;
    
    @Property @AutoConvert
    protected boolean sortable = true;
    
    @Property @AutoConvert
    private boolean visible = true; 
    
    @Property @AutoConvert
    private boolean export = true; 
    
    @Property @AutoConvert
    protected Boolean wrap; 
    
    @Property @AutoConvert
    private boolean lock;

    @Property
    private Signal<RowClickEvent> rowClick;
    
    @Property
    private Signal<RowClickEvent> rowInspect;
    
    @Property
    protected Grid grid;
    
    @Property
    private Font font;
    
    @Property
    private Color color;
    
    @Property
    private Color background;
    
    
    @Property @Defended @Ignore
    private ExportingAttributes exporting;
    
    public GridColumn() {
        super();
    }
    
    public GridColumn( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        if( model.is( Grid.class ) ) {
            this.grid = (Grid)model.actual();
        } else {
            this.grid = null;
        }
    }

    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        this.grid = null;
    }
    
    protected void spreadOut( Event event ) {
        if( source != null ) {
            source.notify( event );
        }
        super.spreadOut( event );
    }

    public boolean getSortable( Getter<Boolean> getter ) {
        return getter.get() && getSource().isSortable();
    }

    public Object format( Object item ) {
        return format( item, "html" );
    }

    public Object format( Object item, String... formats ) {
        return getSource().getFormattedValue( grid.getItems(), item, formats );
    }

    public Cell getCell(){
        return this.getSource();
    }
    
    public Object read( Object item ) {
        return getSource().getValue( grid.getItems(), item );
    }
    
    public void write( Object item, Object value ) {
        getSource().setValue( grid.getItems(), item, value );
        grid.touch( item );
    }
    
    /* Virtual Properties */
    
    @Virtual
    public Signal<RowClickEvent> getInspect(){
        return getRowInspect();
    }

    @AutoConvert
    public void setInspect( Signal<RowClickEvent> rowInspect ){
        setRowInspect( rowInspect );
    }
    
    @Virtual
    public Signal<RowClickEvent> getClick(){
        return getRowClick();
    }
    
    @AutoConvert
    public void setClick( Signal<RowClickEvent> rowClick ){
        setRowClick( rowClick );
    }
 
    /* Bean Accessors */
    
    public Signal<RowClickEvent> getRowInspect(){
        return this.rowInspect;
    }
    
    public void setRowInspect( Signal<RowClickEvent> rowInspect ){
        this.rowInspect = rowInspect;
    }
    
    public Signal<RowClickEvent> getRowClick(){
        return this.rowClick;
    }
    
    public void setRowClick( Signal<RowClickEvent> rowClick ){
        this.rowClick = rowClick;
    }
    
    public Cell getSource(){
        return this.source;
    }
    
    public void setSource( Cell source ){
        this.source = source;
    }

    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Label getTitle(){
        return this.title;
    }
    
    public void setTitle( Label title ){
        this.title = title;
    }

    public boolean getSortable(){
        return this.sortable;
    }
    
    public void setSortable( boolean sortable ){
        this.sortable = sortable;
    }

    public VerticalAlignment getValign(){
        return this.valign;
    }
    
    public void setValign( VerticalAlignment valign ){
        this.valign = valign;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }

    public Boolean getWrap(){
        return this.wrap;
    }
    
    public void setWrap( Boolean wrap ){
        this.wrap = wrap;
    }
    
    public boolean getExport(){
        return this.export;
    }
    
    public void setExport( boolean export ){
        this.export = export;
    }
    
    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    } 
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }

    public boolean getLock(){
        return this.lock;
    }
    
    public void setLocked( boolean locked ){
        this.lock = lock;
    }
    
    public boolean equals( Object other ) {
        return new EqualsBuilder( this )
            .add( "title" )
            .add( "source" )
            .equals( other );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "title" )
            .add( "source" )
            .add( "sortable" )
            .toString();
            
    }

}