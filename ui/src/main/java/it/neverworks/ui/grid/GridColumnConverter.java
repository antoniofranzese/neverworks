package it.neverworks.ui.grid;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;

public class GridColumnConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof GridColumn ) {
            return value;
        } else if( value instanceof Map ) {
            return new GridColumn().set( Arguments.process( value ) );
        } else if( value instanceof Cell || value instanceof String ) {
            return new GridColumn().set( "source", value );
        } else {
            return value;
        }
    }
}