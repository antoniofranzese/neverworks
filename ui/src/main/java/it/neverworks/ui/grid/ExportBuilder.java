package it.neverworks.ui.grid;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.io.OutputStream;

import it.neverworks.lang.Recipe;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.model.BaseModel;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileInfo;

public class ExportBuilder {
    
    private List<GridSource> sources;
    private GridExporter exporter;
    private String name;
    private String type;
    
    public ExportBuilder( Grid grid ) {
        this.sources = list( new GridSource()
            .set( "grid", grid )
        );
    }
    
    public ExportBuilder add( Grid grid ) {
        this.sources.add( new GridSource()
            .set( "grid", grid )
        );
        return this;
    }
    
    protected GridSource lastSource() {
        return this.sources.get( sources.size() - 1 );
    }
    
    protected Grid lastGrid() {
        return lastSource().get( "grid" );
    }

    protected List<GridColumn> lastColumns() {
        return lastSource().get( "columns" );
    }

    protected Recipe lastRecipe() {
        return lastSource().probe( "recipe", () -> new Recipe() );
    }

    
    public ExportBuilder using( GridExporter exporter ) {
        this.exporter = exporter;
        return this;
    }
    
    public ExportBuilder using( String format ) {
        if( "csv".equalsIgnoreCase( format ) ) {
            return using( new CSVExporter() );
        } else if( "xls".equalsIgnoreCase( format ) ) {
            return using( new SheetExporter() );
        } else if( "pdf".equalsIgnoreCase( format ) ) {
            return using( new PDFExporter() );
        } else if( "xml".equalsIgnoreCase( format ) ) {
            return using( new XMLExporter() );
        } else {
            throw new IllegalArgumentException( "Unknown export format: " + format );
        }
    }

    public ExportBuilder with( String name ) {
        return set( name, true );
    }

    public ExportBuilder without( String name ) {
        return set( name, false );
    }

    public ExportBuilder with( String name, Object value ) {
        return set( name, value );
    }

    public ExportBuilder set( String name, Object value ) {
        if( "name".equals( name ) ) {
            return withName( Strings.valueOf( value ) );
        } else if( "type".equals( name ) ) {
            return withType( Strings.valueOf( value ) );
        } else {
            this.lastSource().getParameters().put( name, value );
        }
        return this;
    }
    
    public ExportBuilder withName( String name ) {
        this.name = name;
        return this;
    }

    public ExportBuilder withType( String type ) {
        this.type = type;
        return this;
    }

    public ExportBuilder including( String... names ) {
        for( String name: names ) {
            GridColumn column = this.lastGrid().getColumns().query().by( "name" ).eq( name ).result();
            if( column != null ) {
                this.lastColumns().add( column );
            } else {
                throw new IllegalArgumentException( "Invalid column name: " + name );
            }
        }
        return this;
    }

    public ExportBuilder including( GridColumn... columns ) {
        return including( Collections.<GridColumn>toIterable( columns ) ); 
    }

    public ExportBuilder including( Iterable<GridColumn> columns ) {
        for( GridColumn column: columns ) {
            if( this.lastGrid().getColumns().contains( column ) ) {
                this.lastColumns().add( column );
            } else {
                throw new IllegalArgumentException( "Column does not belong to this grid: " + repr( column ) );
            }
        }
        return this;
    }
    
    public <T> ExportBuilder filter( Filter<T> filter ) {
        lastRecipe().filter( filter );
        return this;
    }
    
    public <T,O> ExportBuilder map( Mapper<T,O> mapper ) {
        lastRecipe().map( mapper );
        return this;
    }
    
    public OutputStream to( OutputStream stream ) {
        if( this.exporter != null ) {
            
            for( GridSource source: this.sources ) {
                if( source.getColumns().size() == 0 ) {
                    source.getColumns().addAll( source.getGrid().getColumns().query().by( "export" ).eq( true ).list() );
                }
            }
            
            this.exporter.setSources( this.sources );

            try {
                this.exporter.export( stream );
                return stream;
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            throw new IllegalStateException( "Missing GridExporter" );
        }
    }
    
    public FileInfo to( FileRepository repository ) {
        return to( repository, new Arguments() );
    }

    public FileInfo to( FileRepository repository, Arguments arguments ) {
        if( this.exporter != null ) {

            if( ! arguments.has( "name" ) ) {
                if( Strings.hasText( this.name ) ) {
                    arguments.arg( "name", this.name );
                } else if( Strings.hasText( this.exporter.getDefaultName() ) ) {
                    arguments.arg( "name", this.exporter.getDefaultName() );
                } else {
                    arguments.arg( "name", "export" );
                } 
            }

            if( ! arguments.has( "type" ) ) {
                if( Strings.hasText( this.type ) ) {
                    arguments.arg( "type", this.type );
                } else if( Strings.hasText( this.exporter.getDefaultMimeType() ) ) {
                    arguments.arg( "type", this.exporter.getDefaultMimeType() );
                } else {
                    arguments.arg( "type", "application/octet-stream" );
                } 
            }
        
            FileInfo info = repository.create( arguments );
            to( info.openStream() );
            return info;
        } else {
            throw new IllegalStateException( "Missing GridExporter" );
        }
    }
    
    public ExportTask task() {
        return new ExportTask( this );
    }
}