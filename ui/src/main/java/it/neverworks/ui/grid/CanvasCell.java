package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.types.Size;

public class CanvasCell extends PropertyCell {
    
    @Property
    protected Size width;

    @Property
    protected Size height;
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public CanvasCell() {
        super();
    }

    public CanvasCell( Arguments arguments ) {
        super();
        set( arguments );
    }
    
}