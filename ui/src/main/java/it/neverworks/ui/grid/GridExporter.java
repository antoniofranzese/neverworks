package it.neverworks.ui.grid;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public interface GridExporter {
    
    String getDefaultName();
    String getDefaultMimeType();
    void setSources( List<GridSource> sources );
    void export( OutputStream stream ) throws IOException;
    
}