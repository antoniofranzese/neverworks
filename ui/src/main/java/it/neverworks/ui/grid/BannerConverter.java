package it.neverworks.ui.grid;

import java.util.Map;

import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;

public class BannerConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Banner ) {
            return value;
        } else if( value instanceof Map ) {
            return new Banner().set( Arguments.process( value ) );
        } else if( value != null ){
            return new Banner().set( "content", value );
        } else {
            return value;
        }
    }
}