package it.neverworks.ui.grid;

import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.Widget;

@Child
public class ExportingAttributes extends Attributes implements ChildModel {
    
    private final static Attributes EMPTY_ATTRIBUTES = new Attributes();
    
    private Widget parent;
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        this.parent = model.is( Widget.class ) ? model.as( Widget.class ) : null;
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        this.parent = null;
    }
    
    public boolean containsItem( Object key ) {
        return super.containsItem( key ) || this.parentAttributes().containsItem( key );
    }
    
    public Object retrieveItem( Object key ) {
        if( ! super.containsItem( key ) && parentAttributes().containsItem( key ) ) {
            return parentAttributes().retrieveItem( key );
        } else {
            return super.retrieveItem( key );
        }
    }
    
    protected Attributes parentAttributes() {
        return this.parent != null ? this.parent.get( "attributes", EMPTY_ATTRIBUTES ) : EMPTY_ATTRIBUTES;
    }
}