package it.neverworks.ui.grid;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Getter;
import it.neverworks.model.utils.EqualsBuilder;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.data.DataSource;

public class SortConfiguration extends BaseModel {
    
    @Property
    private GridColumn column;
    
    @Property
    private boolean descending;
    
    @Property
    private String expression;
    
    @Property
    private boolean foreign = false; 
    
    @Property
    private DataSource items;
    
    @Property
    private Grid grid;
    
    public void touch() {
        this.items = null;
    }
    
    protected DataSource getItems( Getter<DataSource> getter ) {
        if( getter.undefined() && grid.getItems() != null ) {
            getter.setRaw( grid.getItems().sort( expression ) );
        }
        return getter.get();
    }
    
    public DataSource getItems(){
        return this.items;
    }
    
    public void setItems( DataSource items ){
        this.items = items;
    }
    
    public String getExpression(){
        return this.expression;
    }
    
    public void setExpression( String expression ){
        this.expression = expression;
    }
    
    public boolean getDescending(){
        return this.descending;
    }
    
    public void setDescending( boolean descending ){
        this.descending = descending;
    }
    
    public GridColumn getColumn(){
        return this.column;
    }
    
    public void setColumn( GridColumn column ){
        this.column = column;
    }
    
    public boolean getForeign(){
        return this.foreign;
    }
    
    public void setForeign( boolean foreign ){
        this.foreign = foreign;
    }
    
    public boolean equals( Object other ) {
        return new EqualsBuilder( this )
            .add( "column" )
            .add( "descending" )
            .add( "expression" )
            .add( "foreign" )
            .equals( other );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "column" )
            .add( "descending" )
            .add( "expression" )
            .add( "foreign" )
            .toString();
    }

}