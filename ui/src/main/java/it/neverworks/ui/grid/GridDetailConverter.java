package it.neverworks.ui.grid;

import it.neverworks.model.converters.Converter;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.Widget;

public class GridDetailConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Form ) {
            return value;
        } else if( value instanceof Widget ) {
            return new Form().add( (Widget) value );
        } else if( value instanceof Boolean ) {
            return Boolean.TRUE.equals( value ) ? new Form() : null;
        } else {
            return value;
        }
    }
}