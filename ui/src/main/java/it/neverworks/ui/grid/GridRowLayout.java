package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.layout.LayoutWidget;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.Widget;

public class GridRowLayout extends BaseContainerWidget implements Flavored, LayoutWidget {
    
    @Property
    private Size height;
    
    @Property
    private Color color;
    
    @Property
    private Color background;
    
    @Property
    private StyleArtifact flavor;

    public GridRowLayout() {
        super();
    }
    
    public GridRowLayout( Widget... widgets ) {
        this();
        add( widgets );
    }

    public GridRowLayout( Arguments arguments ) {
        this();
        set( arguments );
    }

    public GridRowLayout( Arguments arguments, Widget... widgets ) {
        this();
        set( arguments );
        add( widgets );
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
}