package it.neverworks.ui.grid;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.Model;
import it.neverworks.model.converters.AutoConvertProcessor;

public abstract class AbstractExporter implements Model, GridExporter {
    
    protected Grid grid;
    protected Arguments parameters = new Arguments();
    protected List<GridColumn> columns;
    protected GridSource source;

    public void setSources( List<GridSource> sources ) {
        if( sources != null && sources.size() == 1 ) {
            this.source = sources.get( 0 );
            this.grid = source.getGrid();
            this.columns = source.getColumns();
            this.parameters = source.getParameters();
        } else {
            throw new IllegalArgumentException( msg( "Unsupported sources: {}", sources ) );
        }
    }

    public List<GridColumn> getColumns(){
        return this.columns;
    }
    
    public Grid getGrid(){
        return this.grid;
    }

    public Arguments getParameters() {
        return this.parameters;
    }
    
    public GridSource getSource(){
        return this.source;
    }
    
}