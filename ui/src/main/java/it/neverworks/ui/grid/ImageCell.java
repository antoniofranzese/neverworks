package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Converter;
import it.neverworks.ui.form.Image;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.data.DataSource;

public class ImageCell extends PropertyCell {
    
    private final static Converter ARTIFACT_CONVERTER = new Image.ArtifactConverter();

    @Property
    protected Size width;

    @Property
    protected Size height;
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public ImageCell() {
        super();
    }

    public ImageCell( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    @Override
    public Object getFormattedValue( DataSource source, Object item, String... formats ){
        return ARTIFACT_CONVERTER.convert( super.getFormattedValue( source, item, formats ) );
    }
    
}