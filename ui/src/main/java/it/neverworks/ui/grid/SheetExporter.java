package it.neverworks.ui.grid;

import java.util.List;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.SafeModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Virtual;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.data.export.SheetWriter;

public class SheetExporter implements GridExporter {

    public String getDefaultName() {
        return "export.xlsx";
    }
    
    public String getDefaultMimeType() {
        return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    }
    
    protected List<GridSource> sources;

    private static class ExportArguments extends SafeModel {
        @Property @AutoConvert String title = "Export";
        @Property @AutoConvert boolean headers = true;
        @Property @AutoConvert boolean streaming = true;
        @Property @AutoConvert String standard = null;
        @Property @AutoConvert int skip = 0;
        @Property @AutoConvert boolean html = false;
        @Property @AutoConvert boolean fonts = true;
        @Property @AutoConvert boolean colors = true;
        @Property @AutoConvert boolean banners = true;
        @Property @AutoConvert InputStream source = null;
        @Property int hspan;
        
        @Virtual @AutoConvert
        public void setDecorate( boolean value ) {
            set( "colors", value );
            set( "fonts", value );
            set( "banners", value );
        }
    }
    
    public void export( OutputStream stream ) throws IOException {
        ExportArguments commonArgs = new ExportArguments();
        each( sources ).inspect( s -> commonArgs.set( s.getParameters() ) );
        
        SheetWriter writer = new SheetWriter( 
            arg( "input", commonArgs.source )
            .arg( "output", stream ) 
            .arg( "streaming", commonArgs.streaming )
            .arg( "standard", commonArgs.standard )
        );

        for( GridSource source: sources ) {
            ExportArguments args = new ExportArguments()
                .set( Arguments.process( source.get( "!grid.attributes.xls" ) ) )
                .set( Arguments.process( source.get( "!grid.exporting.xls" ) ) )
                .set( source.getParameters() )
                .set( "hspan", source.getColumns().size() );

            Grid grid = source.getGrid();
            List<GridColumn> columns = source.getColumns();
            
            if( Strings.hasText( args.title ) ) {
                writer.sheet( args.title );    
            } else {
                writer.sheet();
            }

            if( args.skip > 0 ) {
                writer.skip( args.skip );
            }

            // Header
            if( args.headers ) {
                writer.row( 
                    arg( "style", 
                        arg( "font", arg( "weight", "bold" ) ) 
                        .arg( "background", "f0f0f0" )
                    ) 
                );

                for( GridColumn column: columns ) {
                    writer.cell( HTML.strip( column.get( "!attributes.xls.title", column.get( "!title.value" ) ) ) );
                }
            }

            // Rows
            for( Object item: source.getCurrentItems() ) {

                boolean writeRowBody = true;
                Decoration rowDecoration = grid.decorate( item );
            
                if( rowDecoration != null && rowDecoration instanceof RowDecoration ) {
                    Banner interimBanner = rowDecoration.get( "interim" );
                    if( interimBanner != null ) {
                        writeRowBody = false;
                        if( args.banners ) {
                            writeBanner( writer, interimBanner, args );
                        }
                    }
                }
            
                if( writeRowBody ) {

                    if( args.banners && rowDecoration != null && rowDecoration instanceof RowDecoration ) {
                        writeBanner( writer, rowDecoration.<Banner>get( "before" ), args );
                    }

                    Decoration decoration = null;

                    if( args.colors && args.fonts ) {
                        decoration = rowDecoration;
                    
                    } else if( rowDecoration != null ) {
                        if( args.fonts ) {
                            decoration = new Decoration()
                                .set( "font", rowDecoration.getFont() );

                        } else if( args.colors ) {
                            decoration = new Decoration()
                                .set( "color", rowDecoration.getColor() )
                                .set( "background", rowDecoration.getBackground() );
                        }
                    }

                    writer.row( arg( "style", decoration ) );

                    for( GridColumn column: columns ) {
                        Object value = column.format( item, "xlsx", "xls" );

                        if( ! args.html && value instanceof String ) {
                            value = HTML.strip( (String) value );
                        }

                        writer.cell( value,
                            arg( "font", args.fonts ? column.getFont() : null )
                            .arg( "color", args.colors ? column.getColor() : null )
                            .arg( "background", args.colors ? column.getBackground() : null )
                            .arg( "halign", column.getHalign() != null ? column.getHalign().toString() : null )
                            .arg( "valign", column.getValign() != null ? column.getValign().toString() : null )
                        );
                    }
                
                    writer.end();

                    if( args.banners && rowDecoration != null && rowDecoration instanceof RowDecoration ) {
                        writeBanner( writer, rowDecoration.<Banner>get( "after" ), args );
                    }
                }
 
            }

        }

        
        writer.close();

    }
    
    protected void writeBanner( SheetWriter writer, Banner banner, ExportArguments args ) {
        if( banner != null ) {
            writer.row( arg( "height", banner.getHeight() ) );

            writer.cell( HTML.strip( banner.getContent() ),
                arg( "hspan", args.hspan )
                .arg( "font", args.fonts ? banner.getFont() : null )
                .arg( "color", args.colors ? banner.getColor() : null )
                .arg( "background", args.colors ? banner.getBackground() : null )
                .arg( "halign", banner.getHalign() != null ? banner.getHalign().toString() : null )
                .arg( "valign", banner.getValign() != null ? banner.getValign().toString() : null )
            );
            
            writer.end();
        }
    }
    
    public void setSources( List<GridSource> sources ) {
        this.sources = sources;
    }
    
    
}