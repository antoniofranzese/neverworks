package it.neverworks.ui.grid;

import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.types.Size;

@Convert( RowSelectorConverter.class )
@Identification({ "visible", "width" })
public class RowSelector implements Model {
    
    @Property
    private Boolean visible;
    
    @Property
    private Size width;
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( Boolean visible ){
        this.visible = visible;
    }
}