package it.neverworks.ui.grid;

import it.neverworks.model.converters.AutoConvert;

@AutoConvert
public enum DecorationPriority  {
    ROW, COLUMN, CELL
}