package it.neverworks.ui.grid;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.features.Clear;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.TemplateFormatterConverter;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.lifecycle.StartupEvent;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.runtime.Device;

public class ItemSelector<T> extends HTMLCell implements ItemFormatter<T>, Clear {
    
    @Property
    protected String property = "selected"; 
    
    @Property
    protected Size width = Device.is( "mobile" ) ? Size.pixels( 24 ) : Size.pixels( 16 );
    
    @Property 
    protected String checked = "checked";

    @Property 
    protected String unchecked = "unchecked";
    
    @Property
    protected Template template;
    
    @Property @Convert( TemplateFormatterConverter.class )
    protected Formatter hint;
    
    @Property
    protected boolean propagate = false; 
    
    @Property
    private String attribute = null; 
    
    @Property
    protected Signal<RowClickEvent> click;
    
    @Property
    protected Signal<UIEvent> fill;
    
    @Property
    protected Signal<UIEvent> clear;
    
    @Property
    private ItemFormatter selectable = null;
    
    public ItemSelector() {
        super();
        set( "startup", slot( "startup" ) );
    }

    public ItemSelector( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void startup( StartupEvent event ) {
        set( "column.rowClick", slot( "rowClick" ) );
        if( get( "column.width" ) == null ) {
            set( "column.width", this.width );
        }
        set( "column.export", false );
        set( "format", this );
        if( template == null ) {
            template = new Template( "<div data-attribute-element=\"selector\" class=\"click {" + property + ",bool,true=" + checked + "|false=" + unchecked + "}\"></div>" );
        }
        if( Strings.hasText( attribute )) {
            set( "!column.grid.attributes." + attribute.trim(), this.hook( "selection" ) );
        }
    }

    public Object format( T item ) {
        return selectable( item ) ? template.apply( item ) : "<div style=\"display: none\">";
    }
    
    public boolean selectable( Object item ) {
        if( this.selectable != null ) {
            return Boolean.TRUE.equals( this.selectable.format( item ) );
        } else {
            return true;
        }
    }
    
    public boolean selected( Object item ) {
        return Booleans.isTrue( eval( item, property ) );
    }

    public void select( Object item ) {
        if( ! selected( item ) ) {
            assign( item, property, true );
            call( "column.grid.touch", arg( "item", item ) );
        }
    }

    public void deselect( Object item ) {
        if( selected( item ) ) {
            assign( item, property, false );
            call( "column.grid.touch", arg( "item", item ) );
        }
    }
    
    public void rowClick( Event event ) {
        if( event.value( "!attributes.element" ).equals( "selector" ) ) {
            event.set( "item." + property, ! event.<Boolean>get( "item." + property ) );
            event.<Grid>get( "source" ).touch( event.get( "item" ) );

            getClick().fire( event );
        }
        
        if( ! this.propagate ) {
            event.stop();
        }
    }

    protected void handleClear( Handler<UIEvent> handler ) {
        Grid grid = get( "column.grid" );
        grid.query( "items" ).by( property ).eq( true )
            .set( property, false )
            .inspect( item -> {
                grid.touch( item );
            });
            
        handler.handle();    
    }
    
    protected void handleFill( Handler<UIEvent> handler ) {
        Grid grid = get( "column.grid" );
        grid.query( "items" ).by( property ).eq( false )
            .inspect( item -> {
                if( selectable( item ) ) {
                    assign( item, property, true );
                    grid.touch( item );
                }
            });
        handler.handle();
    }
    
    public void clearItems() {
        getClear().fire();
    }

    public List getSelection() {
        return new SelectionList( query( "column.grid.items" ).by( property ).eq( true ).list() );
    }

    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Formatter getHint(){
        return this.hint;
    }
    
    public void setHint( Formatter hint ){
        this.hint = hint;
    }

    public String getUnchecked(){
        return this.unchecked;
    }
    
    public void setUnchecked( String unchecked ){
        this.unchecked = unchecked;
    }
    
    public String getChecked(){
        return this.checked;
    }
    
    public void setChecked( String checked ){
        this.checked = checked;
    }
    
    public Template getTemplate(){
        return this.template;
    }
    
    public void setTemplate( Template template ){
        this.template = template;
    }
    
    public String getProperty(){
        return this.property;
    }
    
    public void setProperty( String property ){
        this.property = property;
    }
    
    public String getAttribute(){
        return this.attribute;
    }
    
    public void setAttribute( String attribute ){
        this.attribute = attribute;
    }
    
    public Signal<RowClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<RowClickEvent> click ){
        this.click = click;
    }
    
    public ItemFormatter getSelectable(){
        return this.selectable;
    }
    
    public void setSelectable( ItemFormatter selectable ){
        this.selectable = selectable;
    }
    
    public Signal<UIEvent> getClear(){
        return this.clear;
    }
    
    public void setClear( Signal<UIEvent> clear ){
        this.clear = clear;
    }
    
    public Signal<UIEvent> getFill(){
        return this.fill;
    }
    
    public void setFill( Signal<UIEvent> fill ){
        this.fill = fill;
    }

    protected class SelectionList extends ArrayList implements Clear {
        
        public SelectionList() {
            super();
        }

        public SelectionList( Collection c ) {
            super( c );
        }
        
        public void clearItems() {
            ItemSelector.this.clearItems();
        }
        
    }
    
}
