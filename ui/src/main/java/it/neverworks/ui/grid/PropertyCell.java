package it.neverworks.ui.grid;

import java.util.Map;
import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Properties;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.collections.MapAdder;
import it.neverworks.model.utils.EqualsBuilder;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.TemplateFormatter;
import it.neverworks.ui.data.TemplateFormatterConverter;

public class PropertyCell extends BaseCell implements FormattedCell {

    protected String property;
    protected Converter converter;
    protected String type;
    
    @Property @Convert( TemplateFormatterConverter.class )
    protected Formatter format;
    
    @Property @Collection( item = TemplateFormatterConverter.class, key = LowerCaseStringConverter.class )
    private Map<String, Formatter> formats;
    
    @Property
    private String sort;

    @Property
    private boolean lenient = false;
    
    public PropertyCell() {
        super();
    }
    
    public PropertyCell( String property ) {
        super();
        if( Template.isTemplate( property ) ) {
            this.format = new TemplateFormatter( property );
        } else {
            this.property = property;
        }
    }
    
    public PropertyCell( ItemFormatter formatter ) {
        super();
        this.format = formatter;
    }
    
    public PropertyCell( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected void addFormatsItem( MapAdder<String,Formatter> adder ) {
        if( adder.key().indexOf( "," ) >= 0 ) {
            for( String key: adder.key().split( "," ) ) {
                adder.add( key.trim(), adder.item() );
            }
        } else {
            adder.add();
        }
    }
    
    public Formatter getFormatter( String... formats ) {
        if( this.formats != null ) {
            for( String format: formats ) {
                if( this.formats.containsKey( format ) ) {
                    return this.formats.get( format );
                }
            }
        }
        return this.format;
    }
    
    public Object getFormattedValue( DataSource source, Object item ){
        return getFormattedValue( source, item, "html" );
    }

    public Object getFormattedValue( DataSource source, Object item, String... formats ){
        Formatter format = getFormatter( formats );
        return format != null ? format.format( format instanceof ItemFormatter ? item : getValue( source, item ) ) : getValue( source, item );
    }

    public Object getValue( DataSource source, Object item ) {
        try {
            return Strings.hasText( property ) ? source.get( item, property ) : null;
        } catch( Exception ex ) {
            if( this.lenient ) {
                return null;
            } else {
                throw Errors.wrap( ex );
            }
        }
    }
    
    public void setValue( DataSource source, Object item, Object value ){
        if( Strings.hasText( property ) ) {
            source.set( item, property, value );
        }
    }
    
    protected String applyFormatter( Formatter formatter, Object item ) {
        if( formatter != null ) {
            if( formatter instanceof ItemFormatter ) {
                return Strings.valueOf( formatter.format( item ) );
            } else if( Strings.hasText( this.property ) ) {
                Object value = this.<DataSource>get( "column.grid.items" ).get( item, this.property );
                return Strings.valueOf( formatter.format( value ) );
            } else {
                throw new RuntimeException( "Cannot use (Value)Formatter, property is null for " + this.get( "column" ) + " cell");
            }
        } else {
            return null;
        }
    }
    
    public boolean isSortable() {
        return Strings.hasText( property );
    }
    
    public String getProperty(){
        return this.property;
    }
    
    public void setProperty( String property ){
        this.property = property;
    }

    public Converter getConverter(){
        return this.converter;
    }
    
    public void setConverter( Converter converter ){
        this.converter = converter;
    }
    
    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }
    
    public Formatter getFormat(){
        return this.format;
    }
    
    public void setFormat( Formatter format ){
        this.format = format;
    }
    
    public Map<String, Formatter> getFormats(){
        return this.formats;
    }
    
    public void setFormats( Map<String, Formatter> formats ){
        this.formats = formats;
    }

    public String getSort(){
        return this.sort;
    }
    
    public void setSort( String sort ){
        this.sort = sort;
    }
    
    public boolean equals( Object other ) {
        return new EqualsBuilder( this )
            .add( "property" )
            .add( "!sort")
            .add( "!converter" )
            .add( "!type" )
            .add( "!format" )
            .add( "!cell" )
            .equals( other );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "property" )
            .toString();
    }
    
}