package it.neverworks.ui.grid;

import static it.neverworks.language.*;

import it.neverworks.lang.Booleans;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.ui.types.Size;

public class RowSelectorConverter extends ModelConverter {
    
    public Object convert( Object value ) {
        if( value instanceof RowSelector ) {
            return value;
        
        } else if( value instanceof Number ) {
            return new RowSelector().set( "width", value );
            
        } else if( Booleans.isBoolean( value ) ) {
            return new RowSelector().set( "visible", Booleans.isTrue( value ) );
            
        } else if( type( Size.class ).isSuitable( value ) ) {
            return new RowSelector().set( "width", value );
        
        } else {
            return super.convert( value );
        }
    }

}