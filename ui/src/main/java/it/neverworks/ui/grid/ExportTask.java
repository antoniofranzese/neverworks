package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.persistence.entities.EntityManager;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.runtime.Task;

public class ExportTask extends Task {
    
    @Property @Inject
    private EntityManager entityManager;
    
    private ExportBuilder builder;
    private FileRepository fileRepository;
    private Arguments fileArguments;
    private FileInfo fileInfo;
    
    public ExportTask( ExportBuilder builder ) {
        this.builder = builder;
    }

    public ExportTask to( FileRepository repository ) {
        return this.to( repository, new Arguments() );
    }
    
    public ExportTask to( FileRepository repository, Arguments arguments ) {
        this.fileRepository = repository;
        this.fileArguments = arguments;
        this.start();
        return this;
    }
    
    public void execute() {
        this.own( entityManager.open() );
        this.fileInfo = this.builder.to( this.fileRepository, this.fileArguments );
    }

    public FileInfo getFileInfo(){
        return this.fileInfo;
    }
    
}