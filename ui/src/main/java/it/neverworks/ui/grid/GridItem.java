package it.neverworks.ui.grid;

import it.neverworks.model.features.Retrieve;
import it.neverworks.model.expressions.ExpressionEvaluator;

public class GridItem implements Retrieve {
    
    private Object item;
    
    public GridItem( Object item ) {
        this.item = item;
    }
    
    public Object retrieveItem( Object key ) {
        return get( (String) key );
    }
    
    public <T> T get( String expression ) {
        return (T)ExpressionEvaluator.evaluate( item, expression );
    }
    
    public GridItem set( String expression, Object value ) {
        ExpressionEvaluator.valorize( item, expression, value );
        return this;
    }
    
}