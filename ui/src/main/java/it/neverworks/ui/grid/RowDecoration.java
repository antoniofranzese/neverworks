package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.types.Decoration;

public class RowDecoration extends Decoration {
    
    @Property
    private Banner interim;
    
    @Property
    private Banner before;
    
    @Property
    private Banner after;
    
    public RowDecoration() {
        super();
    }
        
    public RowDecoration( Arguments arguments ) {
        super( arguments );
    }
    
    public Banner getAfter(){
        return this.after;
    }
    
    public void setAfter( Banner after ){
        this.after = after;
    }
    
    public Banner getBefore(){
        return this.before;
    }
    
    public void setBefore( Banner before ){
        this.before = before;
    }
    
    public Banner getInterim(){
        return this.interim;
    }
    
    public void setInterim( Banner interim ){
        this.interim = interim;
    }
    
}