package it.neverworks.ui.grid;

import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.lifecycle.UIEvent;

@Identification( string = "item" )
@Convert( TouchEventConverter.class )
public class TouchEvent<E> extends UIEvent<E> {
    
    @Property
    private Object item;
    
    public Object getItem(){
        return this.item;
    }
    
    public void setItem( Object item ){
        this.item = item;
    }
    
}
