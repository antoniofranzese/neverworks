package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Convert;
import it.neverworks.ui.types.Decorator;
import it.neverworks.ui.types.Decoration;

@Convert( RowProcessorConverter.class )
public class RowProcessor {
    
    private Decorator decorator = null;
    private RowFormatter formatter = null;
    
    public RowProcessor( Decorator decorator ) {
        this.decorator = decorator;
    }
    
    public RowProcessor( RowFormatter formatter ) {
        this.formatter = formatter;
    }
    
    public Decoration decorate( Object item ) {
        return decorator != null ? decorator.decorate( item ) : (Decoration) formatter.format( item );
    }

    public Decoration decorate( Object item, Arguments arguments ) {
        return decorator != null ? decorator.decorate( item ) : (Decoration) formatter.format( item );
    }
    
}