package it.neverworks.ui.grid;

import java.util.Map;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.model.expressions.Template;
import it.neverworks.ui.data.ItemFormatter;

public class SourceCellConverter implements Converter {
    
    private static Converter mapConverter = new ModelConverter( PropertyCell.class );
    
    public Object convert( Object value ) {
        if( value instanceof Cell ) {
            return value;
        } else if( value instanceof String ) {
            if( Template.isTemplate( value ) ) {
                return new PropertyCell().set( "format", value );
            } else {
                return new PropertyCell().set( "property", value );
            }
        } else if( value instanceof ItemFormatter ) {
            return new PropertyCell().set( "format", value );
        } else if( value instanceof Map ) {
            return mapConverter.convert( value );
        } else {
            return value;
        }
    }
    
}