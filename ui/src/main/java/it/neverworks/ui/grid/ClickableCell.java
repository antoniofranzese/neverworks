package it.neverworks.ui.grid;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.converters.Convert;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.TemplateFormatterConverter;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.lifecycle.StartupEvent;
import it.neverworks.ui.runtime.Device;

public class ClickableCell<T> extends HTMLCell implements ItemFormatter<T> {
    
    @Property
    private Signal<RowClickEvent> click;
    
    @Property @Convert( TemplateFormatterConverter.class )
    private Formatter icon = new ItemFormatter(){
        public Object format( Object item ) {
            return "default";
        }
    };

    @Property
    private Size width = Device.is( "mobile" ) ? Size.pixels( 26 ) : Size.pixels( 18 );
    
    @Property @Convert( TemplateFormatterConverter.class )
    private Formatter caption;
    
    @Property @Convert( TemplateFormatterConverter.class )
    private Formatter hint;
    
    @Property
    private boolean propagate = false; 
    
    public ClickableCell() {
        super();
        set( "startup", slot( "startup" ) );
    }

    public ClickableCell( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void startup( StartupEvent event ) {
        set( "column.rowClick", slot( "rowClick" ) );
        if( get( "column.width" ) == null ) {
            set( "column.width", this.width );
        }
        set( "column.export", false );
        set( "format", this );
    }

    public Object format( T item ) {

        String caption = applyFormatter( this.caption, item );
        String hint = applyFormatter( this.hint, item );

        return Strings.message( "<div class=\"gridIcon\" {2}><div class=\"{0}\"></div>{1}</div>"
            ,applyFormatter( this.icon, item )
            ,Strings.hasText( caption ) ? Strings.message( "<div class=\"caption\">{0}</div>", HTML.encode( caption ) ) : ""
            ,Strings.hasText( hint ) ? Strings.message( " title=\"{0}\"", HTML.encode( hint ) ) : ""
        );
    }
    
    public void rowClick( Event event ) {
        if( ! this.propagate ) {
            event.stop();
        }
        event( "click" ).fire( event );
    }

    public boolean getPropagate(){
        return this.propagate;
    }
    
    public void setPropagate( boolean propagate ){
        this.propagate = propagate;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Formatter getHint(){
        return this.hint;
    }
    
    public void setHint( Formatter hint ){
        this.hint = hint;
    }
    
    public Formatter getCaption(){
        return this.caption;
    }
    
    public void setCaption( Formatter caption ){
        this.caption = caption;
    }
    
}
