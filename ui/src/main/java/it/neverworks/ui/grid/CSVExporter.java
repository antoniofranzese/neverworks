package it.neverworks.ui.grid;

import java.io.OutputStream;
import java.io.IOException;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.Property;
import it.neverworks.model.SafeModel;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.data.export.CSVWriter;
import it.neverworks.ui.types.Decoration;

public class CSVExporter extends AbstractExporter {

    public String getDefaultName() {
        return "export.csv";
    }
    
    public String getDefaultMimeType() {
        return "text/csv";
    }
    
    private static class ExportArguments extends SafeModel {
        @Property @AutoConvert boolean html = false;
        @Property @AutoConvert boolean header = true;
        
        @Virtual
        public void setStripHTML( boolean value ) {
            set( "html", ! value );
        }
    }

    public void export( OutputStream stream ) throws IOException {
        ExportArguments args = new ExportArguments()
            .set( Arguments.process( grid.get( "!attributes.csv" ) ) ) 
            .set( Arguments.process( grid.get( "!exporting.csv" ) ) ) 
            .set( parameters );                                        

        CSVWriter writer = new CSVWriter( stream );

        if( args.header ) {
            for( GridColumn column: columns ) {
                writer.write( HTML.strip( column.get( "!exporting.csv.title", column.get( "!title.value", column.getName() ) ) ) );
            }
            writer.end();
        }
        
        for( Object item: getSource().getCurrentItems() ) {
            
            boolean writeRowBody = true;
            Decoration rowDecoration = grid.decorate( item );
            if( rowDecoration != null && rowDecoration instanceof RowDecoration && rowDecoration.get( "interim" ) != null ) {
                writeRowBody = false;
            }
            
            if( writeRowBody ) {
                for( GridColumn column: columns ) {
                    Object value = column.format( item, "csv", "text" );
                    if( ! args.html && value instanceof String ) {
                        value = HTML.strip( (String) value );
                    }
                    writer.write( value );
                }
            
                writer.end();
            }
        }
        
        writer.close();

    }
    
}