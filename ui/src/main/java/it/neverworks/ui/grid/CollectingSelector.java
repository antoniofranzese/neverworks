package it.neverworks.ui.grid;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.expressions.Template;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.lifecycle.StartupEvent;
import it.neverworks.ui.lifecycle.UIEvent;

public class CollectingSelector<T> extends ItemSelector<T> {
    
    protected List selection = new SelectionList();
    
    @Property
    private boolean sticky = false;
    
    @Property
    private boolean full = false; 
    
    public CollectingSelector() {
        super();
    }

    public CollectingSelector( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void startup( StartupEvent event ) {
        if( template == null ) {
            template = new Template( "<div data-attribute-element=\"selector\" class=\"click {/,bool,true=" + checked + "|false=" + unchecked + "}\"></div>" );
        }
        super.startup( event );
        
        on( "column.grid.change", slot( "changeItems" ) );
    }

    protected void changeItems( Event event ) {
        DataSource items = event.get( "items" );
        if( this.sticky ) {
            if( this.selection.size() > 0 ) {
                if( items != null ) {
                    String keyProperty = event.get( "source.key" );
                    this.selection = each( this.selection ).filter( key -> items.contains( keyProperty, key ) ).list();
                } else {
                    this.selection.clear();
                }
            }
        } else {
            this.selection.clear();
        }
    }

    public Object format( T item ) {
        return selectable( item ) ? template.apply( selected( item ) ) : "<div style=\"display: none\">";
    }
    
    public void rowClick( Event event ) {

        if( event.value( "!attributes.element" ).equals( "selector" ) ) {
            Object value = event.get( "item." + property );
            if( selection.contains( value ) ) {
                selection.remove( value );
            } else {
                selection.add( value );
            }
            event.<Grid>get( "source" ).touch( event.get( "item" ) );

            getClick().fire( event );
        }
        
        if( ! this.propagate ) {
            event.stop();
        }
    }

    public List getSelection(){
        if( this.selection != null && this.selection.size() > 0 && this.full ) {
            DataSource items = get( "column.grid.items" );
            String keyProperty = get( "column.grid.key" );
            if( items != null ) {
                return each( this.selection ).map( k -> items.load( keyProperty, k ) ).list();
            } else {
                return new ArrayList();
            }
        }
        return this.selection;
    }
    
    @Override
    protected void handleClear( Handler<UIEvent> handler ) {
        Grid grid = get( "column.grid" );
        for( Object value: new ArrayList( this.selection ) ) {
            this.selection.remove( value );
            grid.touch( grid.getItems().load( this.property, value ) );
        }
        handler.handle();
    }

    @Override
    protected void handleFill( Handler<UIEvent> handler ) {
        Grid grid = get( "column.grid" );
        for( Object item: grid.getItems() ) {
            select( item );
        }
        handler.handle();
    }
    
    @Override
    public boolean selected( Object item ) {
        return selection.contains( eval( item, property ) );
    }
    
    @Override
    public void select( Object item ) {
        if( ! selected( item ) && selectable( item ) ) {
            selection.add( eval( item, property ) );
            call( "column.grid.touch", item );
        }
    }

    @Override
    public void deselect( Object item ) {
        if( selected( item ) ) {
            selection.remove( eval( item, property ) );
            call( "column.grid.touch", item );
        }
    }
    
    public boolean getSticky(){
        return this.sticky;
    }
    
    public void setSticky( boolean sticky ){
        this.sticky = sticky;
    }
    
    public boolean getFull(){
        return this.full;
    }
    
    public void setFull( boolean full ){
        this.full = full;
    }
    
}
