package it.neverworks.ui.grid;

import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;

public class TouchEventConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof TouchEvent ) {
            return value;
        } else if( value instanceof Arguments ) {
            return new TouchEvent().set( (Arguments) value );
        } else if( value != null ) {
            return new TouchEvent().set( "item", value );
        } else {
            return value;
        }
    }
}