package it.neverworks.ui.grid;

import it.neverworks.model.description.ChildModel;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.Widget;
import it.neverworks.ui.types.Decorator;

public interface Cell extends Widget, ChildModel {
    
    Object getValue( DataSource source, Object item );
    void setValue( DataSource source, Object item, Object value );
    Object getFormattedValue( DataSource source, Object item );
    Object getFormattedValue( DataSource source, Object item, String... formats );
    
    GridColumn getColumn();
    boolean isSortable(); 
    Decorator getCell();   
        
}