package it.neverworks.ui;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;


import java.util.Iterator;

import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Numbers;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Adder;
import it.neverworks.model.collections.Remover;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Setter;
import it.neverworks.model.features.Append;
import it.neverworks.model.features.Delete;
import it.neverworks.model.features.Clear;
import it.neverworks.model.features.Length;
import it.neverworks.model.features.Query;
import it.neverworks.model.features.Locate;
import it.neverworks.model.features.Place;
import it.neverworks.model.features.Placer;

import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.SignalHook;
import it.neverworks.ui.lifecycle.BuildContentEvent;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.WidgetAddedEvent;
import it.neverworks.ui.lifecycle.WidgetRemovedEvent;
import it.neverworks.ui.lifecycle.WidgetAbandonedEvent;
import it.neverworks.ui.lifecycle.WidgetGainEvent;
import it.neverworks.ui.lifecycle.WidgetLoseEvent;
import it.neverworks.ui.lifecycle.ContainerActivationEvent;
import it.neverworks.ui.lifecycle.CollectorJoinEvent;
import it.neverworks.ui.lifecycle.CollectorLeaveEvent;
import it.neverworks.ui.lifecycle.NotifyEvent;
import it.neverworks.ui.lifecycle.EventChannel;
import it.neverworks.ui.util.WidgetArrayList;

public abstract class BaseContainerWidget extends BaseWidget implements Container, SwapCapable, Append, Delete, Clear, Length, Query, Locate, Place {
    
    @Property @Collection @Defended
    protected List<Widget> children;
    
    @Property @Defended
    protected boolean joined = false;
    
    @Property @Defended
    private Signal<WidgetGainEvent> gain;
    
    @Property @Defended
    private Signal<WidgetLoseEvent> lose;
    
    protected Map<String, Widget> _childrenRegistry = new HashMap<String, Widget>();
    protected List<Widget> pendingChildren;
    protected boolean iteratingOnChildren = false;
    
    public BaseContainerWidget() {
        super();
    }

    public BaseContainerWidget( Arguments arguments ) {
        super( arguments );
    }

    public BaseContainerWidget( Widget... widgets ) {
        super();
        add( widgets );
    }

    public BaseContainerWidget( Arguments arguments, Widget... widgets ) {
        super( arguments );
        add( widgets );
    }
    
    public Container add( Widget... widgets ) {
        for( Widget widget: widgets ) {
            if( widget != null ) {
                addWidget( widget );
            }
        }
        return this;
    }
    
    public Container remove( Integer... indexes ) {
        for( Integer index: indexes ) {
            removeWidget( (Widget) get( index ) );
        }
        return this;
    }

    public Container remove( String... names ) {
        for( String name: names ) {
            removeWidget( widget( name ) );
        }
        return this;
    }

    public Container remove( Widget... widgets ) {
        for( Widget widget: widgets ) {
            removeWidget( widget );
        }
        return this;
    }
    
    public Container remove( Filter<Widget> filter ) {
        for( int i = this.size() - 1; i >= 0; i-- ) {
            boolean remove;
            try {
                remove = filter.filter( get( i ) );
            } catch( Exception ex ){
                remove = false;
            }
            
            if( remove ) {
                this.remove( i );
            }
        }
        return this;
    }
    
    public Container clear() {
        while( size() > 0 ) {
            remove( size() - 1 );
        }
        return this;
    }
    
    public boolean containsItem( Object key ) {
        if( key instanceof Widget ) {
            return this.contains( (Widget) key );
        } else if( Numbers.isNumber( key ) ) {
            return this.contains( Numbers.Int( key ) );
        } else {
            String name = Strings.valueOf( key );
            return this.contains( name ) || this.has( name );
        }
    }

    public void appendItem( Object item ) {
        if( item instanceof Widget ) {
            add( (Widget) item );
        } else {
            throw new IllegalArgumentException( "Cannot add: " + Objects.repr( item ) );
        }
    }
    
    public void deleteItem( Object key ) {
        if( key instanceof String ) {
            remove( (String) key );
        } else if( key instanceof Widget ) {
            remove( (Widget) key );
        } else {
            throw new IllegalArgumentException( this.getCommonName() + ": Cannot remove " + Objects.repr( key ) );
        }
    }

    public void clearItems() {
        clear();
    }
    
    public int itemsCount() {
        return this.children != null ? this.children.size() : 0;
    }

    public Object locateItem( Object item ) {
        if( this.children != null && item instanceof Widget ) {
            int index = this.children.indexOf( (Widget) item );
            return index >= 0 ? index : null;
        } else {
            return null;
        }
    }
    
    public Placer placeItem( Object item ) {
        if( item instanceof Widget ) {
            return new BaseWidgetPlacer( this, (Widget) item );
        } else {
            throw new IllegalArgumentException( this.getClass().getSimpleName() + " cannot place: " + repr( item ) );
        }
    }

    public WidgetSelection<Widget> children() {
        return new WidgetArrayList<Widget>( this.getChildren() );
    }

    public <T> WidgetSelection<T> children( Class<T> type ) {
        return new WidgetArrayList<T>( new ObjectQuery<Widget>( this.getChildren() ).instanceOf( type ) );
    }

    public void addWidget( Widget widget ) {
        this.getChildren().add( widget );
    }

    public void removeWidget( Widget widget ) {
        if( getChildren().contains( widget ) ) {
            getChildren().remove( widget );
        } else {
            throw new WidgetException( this.getCommonName() + ": Cannot remove unknown widget " + widget.getCommonName() );
        }
    }
    
    protected void nameWidget( Widget widget ) {
        // Assegna un nome assoluto pseudo-casuale se il widget e' anonimo
        if( widget != null && ! Strings.hasText( widget.getName() ) ) {
            widget.set( "name", generateUniqueName( widget ) );
        }
    }

    protected void addChildrenItem( Adder<Widget> adder ) {

        Widget widget = adder.item();
        if( widget != null ) {
            //System.out.println( this + ", active " + this.active + ", built " + this.built + ", adding " + widget );
    
            nameWidget( widget );
            
            //System.out.println( "Adding " + widget + " to " + this );
        
            // Non consente di utilizzare nomi di widget che corrispondono a proprieta' del contenitore
            if( isPlainProperty( widget.getName() ) ) {
                throw new WidgetException( "Cannot add " + widget.getCommonName() + ": obstructed by " + this.getClass().getSimpleName() + " property" );
            }
        
            // Controlla la presenza di un eventuale widget omonimo
            if( _childrenRegistry.containsKey( widget.getName() ) ) {
                throw new WidgetException( "Duplicate widget name: " + widget.getName() );
            }
        
            // Si imposta come parent del widget aggiunto
            if( widget instanceof Contained ) {
                ((Contained) widget).joinParent( this );
            } else {
                widget.set( "parent", this );
            }
        
            // Imposta il nome unico del widget nell'ambito del parent
            widget.set( "canonicalName", generateCanonicalName( widget ) );

            // Propaga lo stato di costruzione
            if( this.built ) {
                //System.out.println( this + " propagate build to child " + widget );
                ((WidgetLifeCycle) widget).spreadIn( new BuildContentEvent( this, "Widget added" ) );
            }
 
            // Accoda ai children e al registro, se non ci sono stati errori di build
            adder.add();
            _childrenRegistry.put( widget.getName(), widget );
            if( !( widget.getName().equals( widget.getCanonicalName() ) ) ) {
                _childrenRegistry.put( widget.getCanonicalName(), widget );
            }

            // Propaga lo stato di attivazione
            if( this.active ) {
                ((WidgetLifeCycle) widget).spreadIn( new ContainerActivationEvent( this, "Widget added" ) );
            }
        
            if( this.model().watching() ) {
                widget.model().watch();
            }
        

            // Notifica ai propri contenitori l'aggiunta di un nuovo widget
            this.bubble( this.notify( new WidgetAddedEvent( this, widget, "Added to " + this.getCommonName() ) ) );
        
            // Rende modificata la proprieta' children
            this.model().touch( "children" );
        
            // Se l'aggiunta arriva in fase di iterazione, la accoda
            if( this.iteratingOnChildren ) {
                if( this.pendingChildren == null ) {
                    this.pendingChildren = new ArrayList<Widget>();
                }
                this.pendingChildren.add( widget );
            }

            WidgetGainEvent gain = new WidgetGainEvent( this, widget );
            if( this.started ) {
                signal( "gain" ).fire( gain );
            } else {
                on( "startup", slot( evt -> signal( "gain" ).fire( gain ) ).once() );
            }
        
        } else {
            adder.decline();
        }
    }
    
    protected void removeChildrenItem( Remover<Widget> remover ) {
        Widget widget = remover.item();
        
        // Rimuove il riferimento al parent
        // System.out.println( "Removing parent to " + widget );
        if( widget instanceof Contained ) {
            ((Contained) widget).leaveParent( this );
        } else {
            widget.set( "parent", (Object)null );
        }

        // Rimuove dalla collezione e dal registro

        // System.out.println( "Removing " + widget + " from " + this );
        remover.remove();
        _childrenRegistry.remove( widget.getName() );
        if( !( widget.getName().equals( widget.getCanonicalName() ) ) ) {
            _childrenRegistry.remove( widget.getCanonicalName() );
        }
        
        // Notifica ai propri contenitori la rimozione di un widget esistente
        this.bubble( this.notify( new WidgetRemovedEvent( this, widget, "Removed from " + this.getCommonName() ) ) );
        
        // Se il contenitore e' persistente, notifica al widget che non ha piu' una rappresentazione client
        if( this.persistent ) {
            ((WidgetLifeCycle) widget).notify( new WidgetAbandonedEvent( this, widget, "Removed from container" ) );
        }

        // Rende modificata la proprieta' children
        this.model().touch( "children" );
        
        // Se la rimozione arriva in fase di iterazione, controlla che non sia stata precedentemente accodata
        if( this.iteratingOnChildren && this.pendingChildren != null ) {
            if( this.pendingChildren.contains( widget ) ) {
                this.pendingChildren.remove( widget );
            }
        }
        
        signal( "lose" ).fire( new WidgetLoseEvent( this, widget ) );
        
    }

    @Override
    public <T extends AbstractWidget> T widget( Object source ) {
        if( source instanceof Number ) {
            return (T) get( ((Number) source).intValue() );
        } else {
            return super.widget( source );
        }
    }    

    public Object retrieveItem( Object key ) {
        if( Numbers.isNumber( key ) ) {
            return get( Numbers.Int( key ) );
        } else {
            return super.retrieveItem( key );
        }
    }

    public <T> T get( int index ) {
        if( index >= 0 && index < children.size() ) {
            return (T)this.getChildren().get( index );
        } else {
            throw new IllegalArgumentException( "Child index out of range (" + ( children.size() > 0 ? ( "0->" + ( children.size() - 1 ) ) : "empty" ) + "): " + index );
        }
    }

    public <T> T get( String name ) {
        if( _childrenRegistry.containsKey( name ) ) {
            return (T)_childrenRegistry.get( name );
        } else {
            return super.get( name );
        }
    }
    
    public <T extends AbstractWidget> T set( String name, Object value ) {
        String cleanName = normalizeName( name.startsWith( "!" ) ? name.substring( 1 ) : name );

        if( ExpressionEvaluator.isExpression( name ) || isPlainProperty( cleanName ) ) {
            return super.set( cleanName, value );
            
        } else if( value instanceof Widget ) {

            if( this.contains( cleanName ) ) {
                this.remove( cleanName );
            }
    
            ((Widget) value).set( "name", cleanName );
            this.add( ((Widget) value) );
            
            return (T)this;
        } else {
            throw new WidgetException( "Cannot set '" + cleanName + "' on " + this.getClass().getSimpleName() + ", it must be a property or a Widget" );
        }
    }
    
    protected boolean isPlainProperty( String name ) {
        return this.modelInstance.has( name ); 
    }

    public boolean contains( Widget widget ) {
        return getChildren().contains( widget );    
    }
    
    public boolean contains( String name ) {
        return _childrenRegistry.containsKey( name );
    }
    
    public boolean contains( int index ) {
        return index > 0 && index < getChildren().size();
    }
    
    public WidgetPlacer place( Widget widget ) {
        return new BaseWidgetPlacer( this, widget );
    }
 
    public Iterator<Widget> iterator() {
        return getChildren().iterator();
    }
    
    @Override
    public int size() {
        return getChildren().size();
    }
    
    @Override
    public <T> ObjectQuery<T> query( Class<T> cls ) {
        return (ObjectQuery<T>)(new ObjectQuery<Widget>( this ).byItem().instanceOf( cls ) );
    }

    @Override
    public <T> ObjectQuery<T> query() {
        return (ObjectQuery<T>)(new ObjectQuery<Widget>( this ));
    }

    public <T> ObjectQuery<T> queryItems() {
        return this.<T>query();
    }

    @Override
    protected Map<String, SignalHook> createSignalHooks() {
        getNotifyRouter().add( WidgetAddedEvent.class, slot( "refreshHooks" ).unsigned() );
        getNotifyRouter().add( WidgetRemovedEvent.class, slot( "refreshHooks" ).unsigned() );
        return super.createSignalHooks();
    }

    protected void spreadOut( Event event ) {
        // Copia la collezione per evitare concurrent modifications
        spreadOutToChildren( event, new java.util.ArrayList<Widget>( getChildren() ) );
        super.spreadOut( event );
    }
    
    protected void spreadOutToChildren( Event event, java.util.List<Widget> children ) {
        this.pendingChildren = null;
        this.iteratingOnChildren = true;

        for( Widget child: children ) {
            ((WidgetLifeCycle) child).spreadIn( event );
        }

        this.iteratingOnChildren = false;
        
        // Notifica eventuali children aggiunti durante lo spread
        if( this.pendingChildren != null ) {
            spreadOutToChildren( event, this.pendingChildren );
        }
    }
    
    protected void checkStartup() {
        for( Widget widget: getChildren() ) {
            if( widget instanceof BaseWidget ) {
                ((BaseWidget) widget).checkStartup();
            }
        }
        super.checkStartup();
    }

    @Override
    protected void handleCollectorJoinEvent( CollectorJoinEvent event ) {
        this.joined = true;
        super.handleCollectorJoinEvent( event );
    }
    
    @Override
    protected void handleCollectorLeaveEvent( CollectorLeaveEvent event ) {
        if( event.getWidget() == this ) {
            for( Widget widget: getChildren() ) {
                //TODO: riportare evento in notify
                this.bubble( new WidgetRemovedEvent( this, widget, "Removed on leave from " + this.getCommonName() ) );
            }
        }
        this.joined = false;
        super.handleCollectorLeaveEvent( event );
    }
        
    public void swapChild( Widget source, Widget destination ) {
        if( contains( source ) ) {
            int index = children.indexOf( source );
            remove( source );
            if( destination != null ) {
                place( destination ).at( index );
            }
        } else {
            throw new WidgetException( "Widget is not contained in this container: " + source.getCommonName() );
        }
    }
    
    @Property @Defended
    protected int canonicalNameSequence = 0;

    protected String generateCanonicalName( Widget widget ) {
        String canonicalName;
        
        do {
            canonicalName = widget.getClass().getSimpleName() + "#" + String.valueOf( canonicalNameSequence++ );
        } while( contains( canonicalName ) );
        
        return canonicalName;
    }
    
    /* Virtual Properties */

    @Virtual
    public Widget getFirst(){
        if( this.getChildren().size() > 0 ) {
            return this.getChildren().get( 0 );
        } else {
            return null;
        }
    }

    @Virtual
    public Widget getLast(){
        if( this.getChildren().size() > 0 ) {
            return this.getChildren().get( this.getChildren().size() - 1 );
        } else {
            return null;
        }
    }
    
    /* Bean Accessors */
    
    public List<Widget> getChildren(){
        return this.children;
    }

    public Signal<WidgetLoseEvent> getLose(){
        return this.lose;
    }
    
    public void setLose( Signal<WidgetLoseEvent> lose ){
        this.lose = lose;
    }
    
    public Signal<WidgetGainEvent> getGain(){
        return this.gain;
    }
    
    public void setGain( Signal<WidgetGainEvent> gain ){
        this.gain = gain;
    }
    

}