package it.neverworks.ui;

import it.neverworks.lang.Filter;
import it.neverworks.lang.Functional;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ObjectCriteriaBuilder;

public interface WidgetSelection<T> extends Functional<T> {
    ObjectQuery<T> query();
    <W> ObjectQuery<W> query( Class<W> cls );
    int size();
    T get( int index );
    WidgetSelection<T> set( String path, Object value );
    ObjectCriteriaBuilder<T> by( String path );
    ObjectQuery<T> by( Filter filter );
    //WidgetSelection<T> remove( Filter<T> filter );
    public <E> Functional<E> call( String path, Object... arguments );
    public WidgetSelection<T> exec( String path, Object... arguments );
    
}
