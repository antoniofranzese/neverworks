package it.neverworks.ui;

public interface SwapCapable {
    void swapChild( Widget source, Widget destination );
}