package it.neverworks.ui;

public abstract class AbstractWidget implements Widget {
    public <T extends AbstractWidget> T setup() {
        return (T) this;
    }
}