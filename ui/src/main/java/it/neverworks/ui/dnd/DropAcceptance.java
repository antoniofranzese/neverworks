package it.neverworks.ui.dnd;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;

@Convert( DropAcceptanceConverter.class )
public class DropAcceptance extends BaseModel {
    
    @Property
    private DropRules rules;
    
    public DropRules getRules(){
        return this.rules;
    }
    
    public void setRules( DropRules rules ){
        this.rules = rules;
    }
    
    @Property
    private boolean active = true;
    
    public boolean getActive(){
        return this.active;
    }
    
    public void setActive( boolean active ){
        this.active = active;
    }
}