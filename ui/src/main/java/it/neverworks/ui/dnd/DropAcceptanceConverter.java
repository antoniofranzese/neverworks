package it.neverworks.ui.dnd;

import it.neverworks.model.converters.Converter;

public class DropAcceptanceConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof DropAcceptance ) {
            return value;
        } else if( value instanceof DropRules ) {
            return new DropAcceptance().set( "rules", value );
        } else {
            return value;
        }
    }

}