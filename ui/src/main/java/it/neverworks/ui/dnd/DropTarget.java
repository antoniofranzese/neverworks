package it.neverworks.ui.dnd;

public interface DropTarget {
    DropAcceptance getAcceptance();
}