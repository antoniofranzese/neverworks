package it.neverworks.ui.dnd;

import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.Widget;

public class DropEvent extends UIEvent {
    
    protected Widget originator;
    
    protected Object proposal;
    
    protected Integer x;
    
    protected Integer y;
    
    public Point getOrigin() {
        if( this.x != null && this.y != null ) {
            return new Point( this.x, this.y );
        } else {
            return null;
        }
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY( Integer y ){
        this.y = y;
    }
    
    public Integer getX(){
        return this.x;
    }
    
    public void setX( Integer x ){
        this.x = x;
    }
    
    public Object getProposal(){
        return this.proposal;
    }
    
    public void setProposal( Object proposal ){
        this.proposal = proposal;
    }
    
    public Widget getOriginator(){
        return this.originator;
    }
    
    public void setOriginator( Widget originator ){
        this.originator = originator;
    }
    
}