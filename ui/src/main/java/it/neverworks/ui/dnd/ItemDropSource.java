package it.neverworks.ui.dnd;

public interface ItemDropSource {
    public Object getDropProposal( Object key );
    public Object getDropItem( Object key );
    // public Avatar getAvatar();
    public String getPath();
}