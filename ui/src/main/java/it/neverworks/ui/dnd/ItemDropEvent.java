package it.neverworks.ui.dnd;

public class ItemDropEvent extends DropEvent {
    
    protected Object item;
    
    public Object getItem(){
        return this.item;
    }
    
    public void setItem( Object item ){
        this.item = item;
    }
    
}