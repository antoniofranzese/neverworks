package it.neverworks.ui.dnd;

import java.util.List;
import java.util.ArrayList;

public class DropRules {
    
    private List<DropRule> rules = new ArrayList<DropRule>();
    
    public DropRuleBuilder by( String property ) {
        return new DropRuleBuilder( this, property );
    }
    
    public DropRuleBuilder and( String property ) {
        return by( property );
    }
    
    public DropRules add( DropRule rule ) {
        this.rules.add( rule );
        return this;
    }
    
    public List<DropRule> getRules(){
        return this.rules;
    }
}