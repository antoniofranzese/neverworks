package it.neverworks.ui.dnd;

import java.util.List;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;

public class InRule extends UnaryRule {

    @Property @Collection
    private List values;
    
    public List getValues(){
        return this.values;
    }
    
    public void setValues( List values ){
        this.values = values;
    }
   
}