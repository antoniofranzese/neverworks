package it.neverworks.ui.dnd;

import it.neverworks.model.AbstractModel;

public abstract class DropRule extends AbstractModel {
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( name );
    }

    public <T extends DropRule> T set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }
    
}