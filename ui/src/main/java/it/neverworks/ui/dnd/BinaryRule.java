package it.neverworks.ui.dnd;

import it.neverworks.model.Property;

public abstract class BinaryRule extends UnaryRule {
    
    @Property
    protected Object value;
    
    public Object getValue(){
        return this.value;
    }
    
    public void setValue( Object value ){
        this.value = value;
    }
    
}