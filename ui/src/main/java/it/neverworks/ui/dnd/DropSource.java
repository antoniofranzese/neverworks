package it.neverworks.ui.dnd;

public interface DropSource {
    public Object getProposal();
    // public Avatar getAvatar();
    public String getPath();
}