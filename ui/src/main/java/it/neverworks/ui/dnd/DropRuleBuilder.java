package it.neverworks.ui.dnd;

public class DropRuleBuilder {
    
    private DropRules rules;
    private String property;
    
    public DropRuleBuilder( DropRules rules, String property ) {
        this.rules = rules;
        this.property = property;
    }
    
    public DropRules eq( Object value ) {
        return rules.add( new EqualsRule().set( "property", property ).set( "value", value ) );
    }
    
    public DropRules ne( Object value ) {
        return rules.add( new NotEqualsRule().set( "property", property ).set( "value", value ) );
    }
    
    public DropRules startsWith( Object value ) {
        return rules.add( new StartsRule().set( "property", property ).set( "value", value ) );
    }
    
    public DropRules contains( Object value ) {
        return rules.add( new ContainsRule().set( "property", property ).set( "value", value ) );
    }
    
    public DropRules endsWith( Object value ) {
        return rules.add( new EndsRule().set( "property", property ).set( "value", value ) );
    }

    public DropRules isNull() {
        return rules.add( new NullRule().set( "property", property ) );
    }

    public DropRules notNull() {
        return rules.add( new NotNullRule().set( "property", property ) );
    }
    
    public DropRules in( Object... values ) {
        return rules.add( new InRule().set( "property", property ).set( "values", values ) );
    }
    
}