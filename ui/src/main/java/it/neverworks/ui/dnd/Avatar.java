package it.neverworks.ui.dnd;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.types.Size;

public class Avatar extends BaseWidget {
    
    @Property
    private Decoration enjoy;
    
    @Property
    private Decoration neglect;
    
    @Property
    private Decoration ignore;
    
    @Property
    private Size width;
    
    @Property
    private Size height;
    
    public Avatar() {
        super();
    }
    
    public Avatar( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Decoration getIgnore(){
        return this.ignore;
    }
    
    public void setIgnore( Decoration ignore ){
        this.ignore = ignore;
    }
    
    public Decoration getNeglect(){
        return this.neglect;
    }
    
    public void setNeglect( Decoration neglect ){
        this.neglect = neglect;
    }
    
    public Decoration getEnjoy(){
        return this.enjoy;
    }
    
    public void setEnjoy( Decoration enjoy ){
        this.enjoy = enjoy;
    }
}