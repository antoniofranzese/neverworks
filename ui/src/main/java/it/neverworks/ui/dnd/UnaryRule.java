package it.neverworks.ui.dnd;

import it.neverworks.model.Property;

public abstract class UnaryRule extends DropRule {
    
    @Property
    protected String property;
    
    public String getProperty(){
        return this.property;
    }
    
    public void setProperty( String property ){
        this.property = property;
    }
    
    
}