package it.neverworks.ui;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;

import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Functional;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.lang.MissingPropertyException;

import it.neverworks.context.Task;
import it.neverworks.context.TaskEvent;
import it.neverworks.context.TaskList;

import it.neverworks.model.Hook;
import it.neverworks.model.Value;
import it.neverworks.model.Modelize;
import it.neverworks.model.Property;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.ModelInstanceAware;
import it.neverworks.model.description.ModelCopier;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Probe;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Pointer;
import it.neverworks.model.features.Resolver;
import it.neverworks.model.features.BasePointer;
import it.neverworks.model.features.UnresolvedPointerException;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Expression;

import it.neverworks.model.events.Locatable;
import it.neverworks.model.events.Locator;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.AnonymousSlot;
import it.neverworks.model.events.Router;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.events.DispatcherHook;
import it.neverworks.model.events.Subscription;
import it.neverworks.model.events.EventPublisher;
import it.neverworks.model.events.SignalProvider;
import it.neverworks.model.events.SignalHook;

import it.neverworks.model.io.State;
import it.neverworks.ui.lifecycle.ErrorEvent;
import it.neverworks.ui.lifecycle.ErrorHandler;
import it.neverworks.ui.lifecycle.NotifyEvent;
import it.neverworks.ui.lifecycle.EnqueueEvent;
import it.neverworks.ui.lifecycle.StartupEvent;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.BuildContentEvent;
import it.neverworks.ui.lifecycle.ContainerActivationEvent;
import it.neverworks.ui.lifecycle.ContainerPassivationEvent;
import it.neverworks.ui.lifecycle.CollectorJoinEvent;
import it.neverworks.ui.lifecycle.CollectorLeaveEvent;
import it.neverworks.ui.lifecycle.WidgetPersistedEvent;
import it.neverworks.ui.lifecycle.WidgetAbandonedEvent;
import it.neverworks.ui.lifecycle.WidgetCollectionEvent;
import it.neverworks.ui.lifecycle.EventChannel;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.types.PrintingAttributes;
import it.neverworks.ui.util.TaskCollector;
import it.neverworks.ui.util.EventGenerator;
import it.neverworks.ui.util.BubbleGenerator;
import it.neverworks.ui.util.SpreadGenerator;
import it.neverworks.ui.util.WidgetArrayList;
import it.neverworks.ui.util.AsyncDispatcher;
import it.neverworks.ui.util.AsyncTask;
import it.neverworks.ui.util.Stores;
import it.neverworks.ui.WidgetSelection;

@Modelize
@Watchable
public abstract class BaseWidget extends AbstractWidget implements WidgetLifeCycle, ErrorHandler, Retrieve, Store, Probe, Inspect, Locatable, EventPublisher, Resolver  {

    @Property @Defended
    protected String name;
    
    @Property @Defended
    private String canonicalName;
    
    @Property @Defended
    protected Widget parent;
    
    @Property @Defended
    protected boolean built = false;

    @Property @Defended
    protected boolean active = false;

    @Property @Defended
    protected boolean persistent = false;
    
    @Property @Defended @Ignore
    protected Attributes attributes;
    
    @Property @Defended @Ignore
    protected Attributes production;

    @Property @Defended @Ignore
    protected PrintingAttributes printing;
    
    @Property @Defended
    private Signal<ErrorEvent> error;
    
    @Property @Defended
    private Signal<NotifyEvent> notify;
    
    @Property @Defended
    protected Signal<StartupEvent> startup;

    @Property @Defended
    protected boolean started = false;

    @Property @Defended
    protected boolean loading = false;

    private List<Event> pendingBubbles;
    private Stores pendingStores;
    
    public BaseWidget() {

    }
    
    public BaseWidget( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Locator getObjectLocator() {
        return null;
    }
    
    protected Widget _lastParent;
    protected void setParent( Setter<Widget> setter ) {
        //System.out.println( "Setting parent " + setter.value() + " for " + this  );
        if( setter.value() == null ) {
            this._lastParent = setter.raw();
        } else {
            this._lastParent = null;
        }
        setter.set();
        recoverBubbles();
    }

    protected void setName( Setter<String> setter ) {
        if( this.parent == null || this.name == null ) {
            setter.set(); 
        } else {
            throw new IllegalStateException( "Cannot rename " + getCommonName() + " widget" );
        }
    }
    
    public <T extends Widget> T parent() {
        return this.<T>get( "parent" );
    }

    public <T> T ancestor( Class<T> type ) {
        Widget current = this;
        while( current.getParent() != null ) {
            if( type.isAssignableFrom( current.getParent().getClass() ) ) {
                return (T)current.getParent();
            } else {
                current = current.getParent();
            }
        }
        return null;
    }

    public WidgetSelection<Widget> ancestors() {
        return ancestors( Widget.class );
    }
    
    public <T> WidgetSelection<T> ancestors( Class<T> type ) {
        List<T> result = new ArrayList<T>();
        Widget current = this;
        while( current.getParent() != null ) {
            if( type.isAssignableFrom( current.getParent().getClass() ) ) {
                result.add( (T)current.getParent() );
            }
            current = current.getParent();
        }
        return new WidgetArrayList<T>( result );
    }
    
    public <T extends Collector> T root() {
        WidgetSelection<Collector> collectors = ancestors( Collector.class );
        if( collectors.size() > 0 ) {
            return (T)collectors.get( collectors.size() - 1 );
        } else {
            return null;
        }
    }

    public <T extends Collector> T getRoot() {
        return root();
    }

    public WidgetSelection<Widget> descendants() {
        WidgetCollectionEvent event = new WidgetCollectionEvent( this );
        this.spread( event );
        return new WidgetArrayList<Widget>( event.getWidgets() );
    }

    public <T> WidgetSelection<T> descendants( Class<T> type ) {
        WidgetCollectionEvent event = new WidgetCollectionEvent( this, type );
        this.spread( event );
        return new WidgetArrayList<T>( (List<T> ) event.getWidgets() );
    }
    
    public WidgetSelection<Widget> children() {
        return new WidgetArrayList<Widget>();
    }
    
    public <T> WidgetSelection<T> children( Class<T> type ) {
        return new WidgetArrayList<T>( children().query().instanceOf( type ) );
    }

    public <T extends Event> Signal<T> event( String name ) {
        return signal( name );
    }   
    
    public <T extends Event> Signal<T> signal( String name ) {
        Object signal = get( name );
        if( signal instanceof Signal ) {
            return (Signal<T>) signal;
        } else if( signal == null ) {
            return (Signal<T>) new Signal( Event.class );
        } else {
            throw new WidgetException( "Invalid Signal: " + name );
        }
    }   

    public Slot slot( String name ) {
        return new Slot( this, name );
    }
    
    public Slot slot( Dispatcher dispatcher ) {
        return new AnonymousSlot( this, dispatcher );
    }

    public void load( State state ) {
        this.loading = true;
        for( String property: state.names() ) {
            // System.out.println( this + " > " + property + " > " + state.get( property ) );
            this.model().checked().set( property, state.get( property ) );
        }
        this.loading = false;
    }

    protected Router notifyRouter;
    
    protected Router getNotifyRouter() {
        if( this.notifyRouter == null ) {
            this.notifyRouter = new Router(){
                public void dispatch( Event event ) {
                    super.dispatch( ((NotifyEvent) event).getDispatched() );
                }
            };
            getNotify().add( this.notifyRouter );
        }
        return this.notifyRouter;
    }

    public Signal retrievePublisher( Class<?> eventClass ) {
        return getNotifyRouter().retrieveRoute( eventClass );
    }
    
    protected Map<String, SignalHook> signalHooks;
    
    public Signal retrieveSignal( String path ) {
        if( ExpressionEvaluator.isExpression( path ) ) {

            if( this.signalHooks == null ) {
                this.signalHooks = createSignalHooks();
            }
 
            if( ! this.signalHooks.containsKey( path ) ) {
                SignalHook hook = new SignalHook( this, path );
                this.signalHooks.put( path, hook );
                hook.refresh();
            }
            
            return signalHooks.get( path );
            
        } else {
            return (Signal) get( path );
        }
    }

    protected Map<String, SignalHook> createSignalHooks() {
        Map<String, SignalHook> signalHooks = new HashMap<String, SignalHook>();
        signal( "startup" ).add( slot( "refreshHooks" ).unsigned() );
        return signalHooks;
    }
    
    protected void refreshHooks() {
        if( this.signalHooks != null ) {
            for( SignalHook hook: this.signalHooks.values() ) {
                hook.refresh();
            }
        }
    }

    public Subscription on( Class<?> eventType, Dispatcher dispatcher ) {
        return retrievePublisher( eventType ).add( dispatcher );
    }    

    public Subscription on( Class<?> eventType, String expression ) {
        return retrievePublisher( eventType ).add( new DispatcherHook( this, expression ) );
    }    

    public Subscription on( String path, Dispatcher dispatcher ) {
        return retrieveSignal( path ).add( dispatcher );
    }    

    public Subscription on( String path, String expression ) {
        return retrieveSignal( path ).add( new DispatcherHook( this, expression ) );
    }    

    public EventGenerator bubble( Class<? extends Event> eventType ) {
        return new BubbleGenerator( this, eventType );
    }

    public EventGenerator spread( Class<? extends Event> eventType ) {
        return new SpreadGenerator( this, eventType );
    }

    public EventGenerator bubble() {
        return new BubbleGenerator( this );
    }

    public EventGenerator spread() {
        return new SpreadGenerator( this );
    }

    public final Event bubble( Event event ) {
        if( event.getSource() == null ) {
            event.setSource( this );
        }
        event.bubble();
        bubbleOut( event );
        return event;
    }
    
    protected WidgetLifeCycle getBubbleRecipient() {
        if( this.parent != null ) {
            return (WidgetLifeCycle) this.parent;
        } else if( this._lastParent != null ){
            return (WidgetLifeCycle) this._lastParent;
        } else {
            return null;
        }
    }
    
    protected void bubbleOut( Event event ) {
        // System.out.println( getCommonName() + " standard bubble " + event );
        WidgetLifeCycle recipient = getBubbleRecipient();
        if( recipient != null ) {
            recipient.bubbleIn( event );
        } else {
            if( this.pendingBubbles == null ) {
                this.pendingBubbles = new ArrayList<Event>();
            }
            this.pendingBubbles.add( event );
        }
    }
    
    public void bubbleIn( Event event ) {
        // System.out.println( getCommonName() + " standard bubbleIn " + event );
        if( event instanceof ErrorEvent ) {
            handleErrorEvent( (ErrorEvent) event );
        } else {
            notifyIn( event, EventChannel.BUBBLE );
            if( event.isBubbled() ) {
                bubbleOut( event );
            }
        }
    }
    
    protected void recoverBubbles() {
        if( this.pendingBubbles != null && this.pendingBubbles.size() > 0 ) {
            if( this.parent != null ) {
                for( Event event: this.pendingBubbles ) {
                    ((WidgetLifeCycle) this.parent).bubbleIn( event );
                }
                this.pendingBubbles = null;
            }
        }
    }

    public final Event spread( Event event ) {
        if( event.getSource() != null ) {
            event.setSource( this );
        }
        spreadOut( event );
        return event;
    }

    protected void spreadOut( Event event ) {
        
    }

    public void spreadIn( Event event ) {
        notifyIn( event, EventChannel.SPREAD );
        spreadOut( event );
    }
    
    
    public final Event notify( Event event ) {
        notifyIn( event, EventChannel.DIRECT );
        return event;
    }
    
    protected void notifyIn( Event event, EventChannel channel ) {
        if( event instanceof BuildContentEvent ) {
            handleBuildContentEvent( (BuildContentEvent) event );
        
        } else if( event instanceof ContainerActivationEvent ) {
            checkStartup();
            handleContainerActivationEvent( (ContainerActivationEvent) event );

        } else if( event instanceof ContainerPassivationEvent ) {
            handleContainerPassivationEvent( (ContainerPassivationEvent) event );

        } else if( event instanceof WidgetPersistedEvent ) {
            handleWidgetPersistedEvent( (WidgetPersistedEvent) event );

        } else if( event instanceof WidgetAbandonedEvent ) {
            handleWidgetAbandonedEvent( (WidgetAbandonedEvent) event );

        } else if( event instanceof WidgetCollectionEvent ) {
            handleWidgetCollectionEvent( (WidgetCollectionEvent) event );

        } else if( event instanceof CollectorJoinEvent ) {
            handleCollectorJoinEvent( (CollectorJoinEvent) event );
            publishEvent( event, channel );

        } else if( event instanceof CollectorLeaveEvent ) {
            handleCollectorLeaveEvent( (CollectorLeaveEvent) event );
            publishEvent( event, channel );

        } else {
            publishEvent( event, channel );
        }
    }
    
    protected void publishEvent( Event event, EventChannel channel ) {
        if( raw( "notify" ) != null && ! getNotify().empty() ) {
            getNotify().fire( new NotifyEvent( this, event, channel ) );
        }
    }
    
    protected void handleStartup( Handler<StartupEvent> handler ) {
        if( ! this.started ) {
            this.started = true;
            this.beforeStartup();
            handler.handle();
            this.afterStartup();
        } else {
            handler.decline();
        }
    }

    protected void beforeStartup() {
        this._lastParent = null;
    }
    
    protected void afterStartup() {
        
    }
    
    protected void checkStartup() {
        if( ! this.started ) {
            if( raw( "startup" ) != null ) {
                getStartup().fire();
            } else {
                this.started = true;
            }
        }
    }

    protected void handleBuildContentEvent( BuildContentEvent event ) {
        if( !built ) {
            //System.out.println( "Building " + this.getCommonName() );
            this.built = true;
            try {
                this.build();
            } catch( Exception ex ) {
                throw new WidgetBuildException( "Error building " + this.getCommonName() + ": " + ex.getMessage(), ex );
            }
            this.recoverStores();
        }
    }
    
    public void build() {}
    
    protected void handleContainerActivationEvent( ContainerActivationEvent event ) {
        // System.out.println( "Activating " + getCommonName() );
        this.active = true;
    }
    
    protected void handleContainerPassivationEvent( ContainerPassivationEvent event ) {
        // System.out.println( "Passivating " + getCommonName() );
        this.active = false;
    }
    
    protected void handleWidgetPersistedEvent( WidgetPersistedEvent event ) {
        this.persistent = true;
        this.pendingBubbles = null;
    }

    protected void handleWidgetAbandonedEvent( WidgetAbandonedEvent event ) {
        //System.out.println( "Abandoning " + getCommonName() );
        this.persistent = false;
    }
 
    protected void handleWidgetCollectionEvent( WidgetCollectionEvent event ) {
        event.add( this );
    }
    
    protected void handleCollectorJoinEvent( CollectorJoinEvent event ) {

    }
    
    protected void handleCollectorLeaveEvent( CollectorLeaveEvent event ) {

    }

    protected void handleErrorEvent( ErrorEvent event ) {
        if( getError().notEmpty() ) {
            // System.out.println( getCommonName() + " handles error" );
            event.halt();
            getError().fire( event );
        } else if( getBubbleRecipient() != null ) {
            // System.out.println( getCommonName() + " rebubbles error" );
            bubble( event );
        } else {
            // System.out.println( getCommonName() + " rethrows error" );
            event.halt();
            event.rethrow();
        }
    }
    
    public void notifyError( ErrorEvent event ) {
        this.bubbleIn( event );
    }
    
    @Virtual
    public String getCommonName(){
        return "[" + getPath() + " " + this.getClass().getSimpleName() + "]";
    }
    
    @Virtual
    public String getPath() {
        List<String> names = new ArrayList<String>();
        for( Collector collector: ancestors( Collector.class ) ) {
            names.add( 0, collector.getName() );
        }

        if( names.size() > 0 ) {
            names.remove( 0 );
        }

        names.add( this.name == null ? "<anonymous>" : this.name );

        return Collections.join( names, "." );
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "!name" ).toString();
    }
    
    /* Model Implementation */
    @Modelize
    protected ModelInstance modelInstance;
    
    public ModelInstance model() {
        return this.modelInstance;
    }
    
    public ModelInstance retrieveModelInstance() {
        return this.modelInstance;
    }
    
    public boolean containsItem( Object key ) {
        return this.has( Strings.valueOf( key ) );
    }
    
    public Object retrieveItem( Object key ) {
        return this.get( Strings.valueOf( key ) );
    }
    
    public void storeItem( Object key, Object value ) {
        this.set( (String)key, value );
    }
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( ExpressionEvaluator.isExpression( name ) ? name : normalizeName( name ) );
    }
    
    public <T> T get( String name, T defaultValue ) {
        // T value = (T)this.modelInstance.safeEval( ExpressionEvaluator.isExpression( name ) ? name : normalizeName( name ) );
        T value = this.get( name );
        return value != null ? value : defaultValue;
    }

    public <T extends AbstractWidget> T set( String name, Object value ) {
        this.modelInstance.safeAssign( normalizeName( name ), value );
        return (T)this;
    }

    public <T extends AbstractWidget> T set( Arguments arguments ) {
        for( String name: arguments.keys() ) {
            this.set( name, arguments.get( name ) );
        }
        return (T)this;
    }

    public <T extends AbstractWidget> T set( String arguments ) {
        return set( Arguments.parse( arguments ) );
    }

    public Pointer ref( String name ) {
        return new BasePointer( this, name );
    }
    
    public boolean resolves( Pointer pointer ) {
        return pointer != null && pointer.reference() instanceof String;
    }
    
    public <T> T resolve( String name ) {
        return resolve( new BasePointer( name ) );
    }
    
    public <T> T resolve( Pointer pointer ) {
        if( pointer != null ) {

            Object value = null;

            String name = Strings.safe( Strings.valueOf( pointer.reference() ) ).trim();

            if( Strings.hasText( name ) ) {

                if( name.startsWith( "/" ) ) {
                    value = root().resolve( name.substring( 1 ) );

                } else if( name.startsWith( "." ) ) {
                    Collector collector = ancestor( Collector.class );
                    if( collector != null ) {
                        value = collector.resolve( name.substring( 1 ) );
                    }

                } else {
                    try {
                        value = (T)this.get( name );
                    } catch( MissingPropertyException ex1 ) {
                        Collector collector = ancestor( Collector.class );
                        if( collector != null ) {
                            value = collector.resolve( name );
                        }
                    }
                }

            } else {
                throw new MissingPropertyException( Strings.message( "Error resolving ''{0}'' from {1}: empty pointer reference", name, getCommonName() ), this, name );
            }

            // Self resolution
            String selfResolutionResult = null;
            if( value == null ) {
                try {
                    value = pointer.resolve();
                    selfResolutionResult = "";
                } catch( UnresolvedPointerException ex ) {
                    selfResolutionResult = ex.getMessage();
                }
            }       

            if( value != null ) {
                return (T) value;
            } else {
                throw new MissingPropertyException( Strings.message( "Cannot resolve ''{0}'' from {1} (self resolution: {2})", name, getCommonName(), selfResolutionResult ), this, name );
            }
            
        } else {
            throw new MissingPropertyException( Strings.message( "Error resolving ''{0}'' from {1}: null pointer", name, getCommonName() ), this, name );
        }
    }

    protected String normalizeName( String name ) {
        if( name != null ) {
            return name.indexOf( "-" ) > 0 ? Strings.hyphen2camel( name ) : name;
        } else {
            return null;
        }
    }
    
    public <T extends AbstractWidget> T store( String name, Object value ) {
        getStore().arg( name, value );
        return (T)this;
    }
    
    public <T> T probe( String name, Wrapper initializer ) {
        return this.model().safeProbe( name, initializer );
    }

    @Virtual
    public Stores getStore() {
        if( pendingStores == null ) {
            pendingStores = new Stores(); 
        }
        return pendingStores;
    }
    
    protected void recoverStores() {
        if( pendingStores != null ) {
            for( String name: pendingStores.keys() ) {
                set( name, pendingStores.get( name ) );
            }
            pendingStores = null;
        }
    }
    
    public Widget swap( Widget destination ) {
        if( getParent() != null ) {
            if( getParent() instanceof SwapCapable ) {
                ((SwapCapable) getParent()).swapChild( this, destination );
                return this;
            } else {
                throw new WidgetException( "Cannot swap " + getCommonName() + ", parent widget class " + getParent().getClass().getName() + " is not SwapCapable" );
            }
        } else {
            throw new WidgetException( "Cannot swap " + getCommonName() + ", widget is orphan" );
        }
    }
    
    public ModelCopier copyObject() {
        return new ModelCopier( model() ).excluding( "name" );
    }
    
    public Object duplicateObject() {
        return copyObject().to( this.getClass() );
    }

    /* Utilities */

    public boolean has( String name ) {
        return this.modelInstance.has( name );
    }

    public boolean has( String name, Class<?> type ) {
        return this.modelInstance.has( name, type );
    }
    
    public <T> T as( Class<T> type ) {
        return (T)this;
    }

    protected <T> T raw( String name ) {
        return (T)this.modelInstance.manager( name ).getRawValue( this.modelInstance );
    }

    protected <T extends Widget> T raw( String name, Object value ) {
        this.modelInstance.manager( name ).setRawValue( this.modelInstance, value );
        return (T)this;
    }
    
    protected String generateUniqueName( Widget widget ) {
        return widget.getClass().getSimpleName() + "@@" + Strings.generateHexID( 8 );
    }

    @Override
    public <T extends AbstractWidget> T widget( Object source ) {
        if( source instanceof Widget ) {
            return (T) source;
        } else if( source instanceof Pointer ) {
            return (T) resolve( (Pointer) source );
        } else {
            return (T) get( Strings.valueOf( source ) );
        }
    }    

    @Override
    public <T extends Container> T container( Object source ) {
        return (T) widget( source );
    }

    @Override    
    public <T extends Collector> T collector( Object source ) {
        return (T) widget( source );
    }

    @Override
    public <T extends Container> T container() {
        return (T) ancestor( Container.class );
    }
    
    @Override
    public <T extends Collector> T collector() {
        return (T) ancestor( Collector.class );
    }
    
    public void enqueue( Event event ) {
        enqueue( evt -> this.notify( evt ), event );
    }

    public void enqueue( Dispatcher dispatcher ) {
        enqueue( dispatcher, (Event) null );
    }
    
    public void enqueue( Dispatcher dispatcher, Arguments arguments ) {
        Event event = new Event();
        event.getAttributes().set( arguments );
        enqueue( dispatcher, event );
    }
    
    public void enqueue( Dispatcher dispatcher, Event event ) {
        bubble( new EnqueueEvent( dispatcher, event ) );
    }

    public void enqueue( String name ) {
        enqueue( slot( name ) );
    }
    
    public void enqueue( String name, Arguments arguments ) {
        Event event = new Event();
        event.getAttributes().set( arguments );
        enqueue( name, event );
    }

    public void enqueue( String name, Event event ) {
        enqueue( slot( name ), event );
    }

    public TaskList getTaskList() {
        TaskCollector clt = ancestor( TaskCollector.class );
        if( clt != null ) {
            return clt.getTaskList();
        } else {
            throw new WidgetException( getCommonName() + " must be contained within a TaskCollector" );
        }
    }

    public Task fork( Dispatcher<TaskEvent> dispatcher ) {
        return getTaskList().enlist( new Task( dispatcher ).start() );
    }

    public Task fork( Dispatcher<TaskEvent> dispatcher, Arguments attributes ) {
        return getTaskList().enlist( new Task( dispatcher, attributes ).start() );
    }

    public Task fork( String name ) {
        return fork( slot( name ) );
    }

    public Task fork( String name, Arguments attributes ) {
        return fork( slot( name ), attributes );
    }

    public Task spawn( Dispatcher<TaskEvent> dispatcher ) {
        return getTaskList().enlist( new AsyncTask( this, dispatcher ).start() );
    }

    public Task spawn( Dispatcher<TaskEvent> dispatcher, Arguments attributes ) {
        return getTaskList().enlist( new AsyncTask( this, dispatcher, attributes ).start() );
    }

    public Task spawn( String name ) {
        return spawn( slot( name ) );
    }

    public Task spawn( String name, Arguments attributes ) {
        return spawn( slot( name ), attributes );
    }
    
    public Dispatcher async( Dispatcher dispatcher ) {
        return new AsyncDispatcher( this, dispatcher );
    }

    public Dispatcher async( String name ) {
        return new AsyncDispatcher( this, slot( name  ) );
    }

    /* Bean Accessors */

    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

    public Widget getParent(){
        return this.parent;
    }
    
    public boolean getActive(){
        return this.active;
    }
    
    public boolean getPersistent(){
        return this.persistent;
    }
    
    public Attributes getAttributes(){
        return this.attributes;
    }
    
    public void setAttributes( Attributes attributes ){
        this.attributes = attributes;
    }
    
    public Signal<ErrorEvent> getError(){
        return this.error;
    }
    
    public void setError( Signal<ErrorEvent> error ){
        this.error = error;
    }
    
    public String getCanonicalName(){
        return this.canonicalName;
    }
    
    public void setCanonicalName( String canonicalName ){
        this.canonicalName = canonicalName;
    }
    
    public Signal<NotifyEvent> getNotify(){
        return this.notify;
    }
    
    public void setNotify( Signal<NotifyEvent> notify ){
        this.notify = notify;
    }
    
    public Signal<StartupEvent> getStartup(){
        return this.startup;
    }

    public void setStartup( Signal<StartupEvent> startup ){
        this.startup = startup;
    }

    
}