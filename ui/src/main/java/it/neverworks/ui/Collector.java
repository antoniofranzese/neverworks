package it.neverworks.ui;

import java.util.List;

import it.neverworks.model.expressions.ObjectQuery;

public interface Collector extends Container {
    WidgetSelection<Widget> all();
    <T> WidgetSelection<T> all( Class<T> type );

    default boolean isRoot() {
        return this.root() == this;
    }
}