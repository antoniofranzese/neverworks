package it.neverworks.ui.util;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.expressions.ExpressionEvaluator;

public abstract class EventGenerator implements Dispatcher {
    
    protected Arguments arguments;
    protected Class<? extends Event> eventType;
    
    public abstract void dispatch( Event event );

    public EventGenerator( Class<? extends Event> eventType ) {
        super();
        this.eventType = eventType;
    }

    public EventGenerator() {
        super();
    }
    
    protected Event processEvent( Event received ) {
        if( this.eventType != null ) {
            Event event = (Event) Reflection.newInstance( this.eventType );
            event.setParent( received );

            if( arguments != null ) {
                for( String name: arguments.keys() ) {
                    ExpressionEvaluator.valorize( event, name, arguments.get( name ) );
                }
            }

            return event;

        } else {
            return received;
        }
    }
    
    public EventGenerator set( String name, Object value ) {
        if( this.eventType != null ) {
            if( arguments == null ) {
                arguments = new Arguments();
            }
            arguments.arg( name, value );
            return this;
        } else {
            throw new IllegalStateException( "Cannot set event parameters, untyped " + this.getClass().getSimpleName() );
        }
    }
    
    public EventGenerator set( Arguments arguments ) {
        for( String name: arguments.keys() ) {
            set( name, arguments.get( name ) );
        }
        return this;
    }
    
    public void now() {
        if( this.eventType != null ) {
            dispatch( null );
        } else {
            throw new IllegalStateException( "Cannot dispatch event, untyped " + this.getClass().getSimpleName() );
        }
    }
    
}