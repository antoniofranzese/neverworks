package it.neverworks.ui.util;

import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class BubbleGenerator extends EventGenerator {
    
    protected Widget widget;
    
    public BubbleGenerator( Widget widget, Class<? extends Event> eventType ) {
        super( eventType );
        this.widget = widget;
    }

    public BubbleGenerator( Widget widget ) {
        super();
        this.widget = widget;
    }
    
    public void dispatch( Event event ) {
        this.widget.bubble( processEvent( event ) );
    }
    
}