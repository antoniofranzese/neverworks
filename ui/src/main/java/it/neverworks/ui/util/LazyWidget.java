package it.neverworks.ui.util;

import it.neverworks.model.description.Process;
import it.neverworks.model.Property;
import it.neverworks.ui.BaseWidget;

@Process( LazyWidgetProcessor.class )
public class LazyWidget extends BaseWidget {

    @Property
    private String reference;
    
    public String getReference(){
        return this.reference;
    }
    
    public void setReference( String reference ){
        this.reference = reference;
    }


}