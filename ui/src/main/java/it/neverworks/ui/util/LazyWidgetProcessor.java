package it.neverworks.ui.util;

import static it.neverworks.language.*;
import it.neverworks.lang.Reflection;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.Getter;
import it.neverworks.ui.MissingWidgetException;
import it.neverworks.ui.Collector;
import it.neverworks.ui.Widget;

public class LazyWidgetProcessor implements PropertyProcessor, Converter {
    
    public void processProperty( MetaPropertyManager property ) {
        if( Widget.class.isAssignableFrom( property.getOwner().target() ) ) {
            property.placeGetter( new LazyGetter() ).after();
            property.setConverter( this );
            property.setType( type( Widget.class ) );
        }
    }
    
    public Object convert( Object value ) {
        if( value instanceof Widget ) {
            return value;
        } else if( value instanceof String ) {
            return new LazyWidget().set( "reference", value );
        } else {
            return value;
        }
    }

    public static class LazyGetter implements Interceptor {
    
        public Object invoke( Invocation invocation ) {
            Getter<Widget> getter = invocation.argument( 0 );
            
            if( getter.value() instanceof LazyWidget ) {
                Widget widget = null;
                String path = getter.value().<String>get( "reference" );
                
                try {
                    getter.setRaw( getter.<Widget>actual().resolve( getter.value().<String>get( "reference" ) ) );
                } catch( Exception ex ) {
                    throw new MissingWidgetException( "Cannot find lazy widget with path: " + path );
                }

            }
            
            return getter.value();

        }
    
    }

}