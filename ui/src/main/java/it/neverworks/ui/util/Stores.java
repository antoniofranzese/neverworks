package it.neverworks.ui.util;

import java.util.HashMap;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.model.expressions.Expression;
import it.neverworks.model.features.Assign;
import it.neverworks.model.features.Evaluate;
import it.neverworks.model.features.Ensure;

public class Stores extends Arguments implements Assign, Evaluate, Ensure {
    
    public void assignExpression( Expression expression, Object value ) {
        arg( expression.text(), value );
    }

    public Object evaluateExpression( Expression expression ) {
        if( containsKey( expression.text() ) ) {
            return get( expression.text() );
        } else if( expression.modifiers().nulls() ){
            return null;
        } else {
            throw new MissingPropertyException( this, expression.text() );
        }
    }
    
    public Object ensureExpression( Expression expression ) {
        return new PathBuilder( this, expression.text() );
    }

    public static class PathBuilder implements Assign, Evaluate, Ensure {
        private String prefix;
        private Stores stores;
        
        public PathBuilder( Stores stores, String prefix ) {
            this.stores = stores;
            this.prefix = prefix;
        }
        
        public void assignExpression( Expression expression, Object value ) {
            stores.arg( prefix + "." + expression.text(), value );
        }

        public Object evaluateExpression( Expression expression ) {
            if( stores.containsKey( prefix + "." + expression.text() ) ) {
                return stores.get( prefix + "." + expression.text() );
            } else if( expression.modifiers().nulls() ){
                return null;
            } else {
                throw new MissingPropertyException( stores, prefix + "." + expression.text() );
            }
        }
        
        public Object ensureExpression( Expression expression ) {
            return new PathBuilder( stores, prefix + "." + expression.text() );
        }
    }
}