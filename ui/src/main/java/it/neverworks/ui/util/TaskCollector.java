package it.neverworks.ui.util;

import it.neverworks.context.TaskList;

public interface TaskCollector  {
    TaskList getTaskList();
}