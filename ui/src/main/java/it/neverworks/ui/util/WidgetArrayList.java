package it.neverworks.ui.util;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

import it.neverworks.lang.FunctionalIterable;
import it.neverworks.lang.Functional;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Inspector;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ObjectCriteriaBuilder;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.ui.WidgetSelection;
import it.neverworks.ui.Widget;

public class WidgetArrayList<T> extends FluentArrayList<T> implements WidgetSelection<T> {
    
    private Inspector<T> adder;
    private Inspector<T> remover;
    
    public WidgetArrayList() {
        super();
    } 

    public WidgetArrayList( Collection<? extends T> collection ) {
        super( collection );
    } 

    public WidgetArrayList( Iterable<? extends T> iterable ) {
        super( iterable );
    } 
    
    public WidgetArrayList( T... widgets ) {
        super();
        for( T widget: widgets ) {
            if( widget != null ) {
                add( widget );
            }
        }
    }
    
    public ObjectCriteriaBuilder<T> by( String path ) {
        return query().by( path );
    }

    public ObjectQuery<T> by( Filter filter ) {
        return query().by( filter );
    }
    
    public ObjectQuery<T> query() {
        return new ObjectQuery<T>( this );
    }
    
    public <W> ObjectQuery<W> query( Class<W> cls ) {
        return new ObjectQuery<W>( (List<W>)this ).byItem().instanceOf( cls );
    }
    
    public WidgetSelection<T> set( String path, Object value ) {
        query().set( path, value );
        return this;
    }

    public <E> Functional<E> call( String path, Object... arguments ) {
        return query().call( path, arguments );
    }
    
    public WidgetSelection<T> exec( String path, Object... arguments ) {
        query().exec( path, arguments );
        return this;
    }
    
    // @Override
    // public WidgetSelection<T> remove( Filter<T> filter ) {
    //     if( this.remover != null ) {
    //         for( T item: filter( filter ) ) {
    //             this.remover.inspect( item );
    //         }
    //     } else {
    //         throw new IllegalStateException( "This " + this.getClass().getSimpleName() + " is not able to remove" );
    //     }
    //     return this;
    // }
    //
    // @Override
    // public boolean append( T item ) {
    //     if( this.adder != null ) {
    //         this.adder.inspect( item );
    //         return true;
    //     } else {
    //         throw new IllegalStateException( "This " + this.getClass().getSimpleName() + " is not able to add" );
    //     }
    // }
    //
    // public WidgetArrayList<T> onAdd( Inspector adder ) {
    //     this.adder = adder;
    //     return this;
    // }
    //
    // public WidgetArrayList<T> onRemove( Inspector remover ) {
    //     this.remover = remover;
    //     return this;
    // }
}