package it.neverworks.ui.util;

import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class SpreadGenerator extends EventGenerator {
    
    protected Widget widget;
    
    public SpreadGenerator( Widget widget, Class<? extends Event> eventType ) {
        super( eventType );
        this.widget = widget;
    }

    public SpreadGenerator( Widget widget ) {
        super();
        this.widget = widget;
    }
    
    public void dispatch( Event event ) {
        this.widget.spread( processEvent( event ) );
    }
    
}