package it.neverworks.ui.util;

import java.lang.ref.WeakReference;

import it.neverworks.lang.Arguments;
import it.neverworks.context.Task;
import it.neverworks.context.TaskEvent;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.Property;
import it.neverworks.ui.Widget;

public class AsyncTask extends Task {
    
    protected WeakReference<Widget> widgetRef;
    
    public AsyncTask( Widget widget, Dispatcher dispatcher ) {
        super( dispatcher );
        this.widgetRef = new WeakReference<Widget>( widget );
    }

    public AsyncTask( Widget widget, Dispatcher dispatcher, Arguments arguments ) {
        super( dispatcher, arguments );
        this.widgetRef = new WeakReference<Widget>( widget );
    }

    protected Dispatcher<TaskEvent> processDispatcher( Dispatcher dispatcher ) {
        Widget widget = this.widgetRef.get();
        if( widget != null ) {
            return (Dispatcher<TaskEvent>) ( dispatcher instanceof AsyncDispatcher ? dispatcher : new AsyncDispatcher( widget, dispatcher ) );
        } else {
            return dispatcher;
        }
    }
}