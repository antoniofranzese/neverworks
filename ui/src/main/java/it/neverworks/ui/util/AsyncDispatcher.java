package it.neverworks.ui.util;

import java.lang.ref.WeakReference;

import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class AsyncDispatcher implements Dispatcher {
    
    protected Dispatcher dispatcher;
    protected WeakReference<Widget> widgetRef;
    
    public AsyncDispatcher( Widget widget, Dispatcher dispatcher ) {
        super();
        this.widgetRef = new WeakReference<Widget>( widget );
        this.dispatcher = dispatcher;
    }
    
    @Override
    public void dispatch( Event event ) {
        Widget widget = this.widgetRef.get();

        if( widget != null ) {
            widget.enqueue( this.dispatcher, event );
        }
    }
    
}