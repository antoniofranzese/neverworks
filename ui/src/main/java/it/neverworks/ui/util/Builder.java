package it.neverworks.ui.util;

import it.neverworks.lang.Arguments;
import it.neverworks.ui.Widget;

public abstract class Builder {
    
    public abstract Widget build( Arguments arguments );
    
}