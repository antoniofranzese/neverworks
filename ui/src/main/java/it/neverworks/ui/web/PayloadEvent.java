package it.neverworks.ui.web;

import static it.neverworks.language.*;

import java.util.Map;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.model.converters.ArgumentsConverter;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.lifecycle.UIEvent;

public abstract class PayloadEvent extends UIEvent {
    
    protected abstract Object getPayload();

    public <T> T as( Class<T> target ) {
        Object payload = getPayload();
        if( payload == null ) {
            return null;
        } else if( target.isInstance( payload ) ) {
            return (T) payload;
        } else if( Model.class.isAssignableFrom( target ) ) {
            return type( target ).process( payload );
        } else {
            try {
                return (T) type( Arguments.class ).process( payload ).populate( target );
            } catch( Exception ex ) {
                throw new IllegalStateException( "Cannot convert " + Objects.className( payload ) + " payload to " + target.getName(), ex );
            }
        }
    }
}
