package it.neverworks.ui.web;

import java.util.List;

import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.io.FileInfo;

public class UploadEvent extends UIEvent<Object> {
    
    public UploadEvent( Object source ) {
        super( source );
    }

    public UploadEvent( Object source, String reason ) {
        super( source, reason );
    }
    
    @Property @Collection
    private List<FileInfo> files;
    
    public List<FileInfo> getFiles(){
        return this.files;
    }
    
    public void setFiles( List<FileInfo> files ){
        this.files = files;
    }

}
