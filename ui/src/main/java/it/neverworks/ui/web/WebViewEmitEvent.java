package it.neverworks.ui.web;

import it.neverworks.model.Property;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.lifecycle.UIEvent;

@Identification( string = { "name", "id", "result" })
public class WebViewEmitEvent extends PayloadEvent {
    
    private String id;
    
    public String getId(){
        return this.id;
    }
    
    public void setId( String id ){
        this.id = id;
    }
    
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    private Object state;
    
    public Object getState(){
        return this.state;
    }
    
    public void setState( Object state ){
        this.state = state;
    }

    // CallbackEvent compatibility
    public Object getResult(){
        return this.state;
    }
    
    protected Object getPayload() {
        return this.state;
    }

}