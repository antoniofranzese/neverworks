package it.neverworks.ui.web;

import java.util.List;
import java.io.InputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;

import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.BehavioralWidget;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.data.FileArtifact;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Handler;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;

public class Downloader extends BaseWidget implements BehavioralWidget {

    @Property
    private FileArtifact source;
    
    @Property
    private String fileName;
    
    @Property
    private String type;
    
    @Property
    private Signal<Event> send;
    
    @Property
    private String protocol;
    
    public Downloader() {
        super();
    }
    
    public Downloader( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public void send() {
        if( getSource() == null ) {
            throw new WidgetException( "Download source is required" );
        }
        model().touch( "source" );
    }

    @Deprecated
    public void send( String name, FileBadge file ) {
        send( file, name );
    }

    public void send( Object source ) {
        send( source, new Arguments() );
    }
    
    public void send( Object source, String name ) {
        send( source, arg( "fileName", name ) );
    }
    
    public void send( Object source, Arguments arguments ) {
        set( "source", source );
        arguments.keys().forEach( k -> set( "name".equals( k ) ? "fileName" : k, arguments.get( k ) ) );
    }

    /* Bean Accessors */

    public Signal<Event> getSend(){
        return this.send;
    }
    
    public void setSend( Signal<Event> send ){
        this.send = send;
    }
    
    public FileArtifact getSource(){
        return this.source;
    }
    
    public void setSource( FileArtifact source ){
        this.source = source;
    }
    
    public String getFileName(){
        return this.fileName;
    }
    
    public void setFileName( String fileName ){
        this.fileName = fileName;
    }
    
    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }
    
    public String getProtocol(){
        return this.protocol;
    }
    
    public void setProtocol( String protocol ){
        this.protocol = protocol;
    }
    
}