package it.neverworks.ui.web;

import it.neverworks.model.Property;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.lifecycle.UIEvent;

@Identification( string = { "id", "result" })
public class WebViewCallbackEvent extends PayloadEvent {
    
    private String id;
    
    public String getId(){
        return this.id;
    }
    
    public void setId( String id ){
        this.id = id;
    }
    
    private Object result;
    
    public Object getResult(){
        return this.result;
    }
    
    public void setResult( Object result ){
        this.result = result;
    }

    protected Object getPayload() {
        return this.result;
    }


}