package it.neverworks.ui.web;

import static it.neverworks.language.*;

import java.util.Map;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.model.converters.ArgumentsConverter;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.lifecycle.UIEvent;

public class ConversationMessageEvent extends PayloadEvent {
    
    @Property
    private String topic;
    
    @Property
    private Object value;
    
    public Object getValue(){
        return this.value;
    }
    
    public void setValue( Object value ){
        this.value = value;
    }
    
    public String getTopic(){
        return this.topic;
    }
    
    public void setTopic( String topic ){
        this.topic = topic;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "topic" )
            .add( "value" )
            .toString();
    }

    protected Object getPayload() {
        return this.getValue();
    }

}
