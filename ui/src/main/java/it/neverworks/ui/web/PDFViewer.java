package it.neverworks.ui.web;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.data.FileArtifact;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;

public class PDFViewer extends BaseWidget {
    
    @Property
    private FileArtifact source;
    
    @Property
    private Size width;
    
    @Property
    private Size height;
    
    @Property
    private Box margin;
    
    @Property
    private Box padding;
    
    @Property
    private LayoutInfo layout;
    
    @Property
    private String fileName;
    
    @Property
    private String type;
    
    public PDFViewer() {
        super();
    }
    
    public PDFViewer( Arguments arguments ) {
        super();
        set( arguments );
    }

    /* Bean Accessors */
    
    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }
    
    public String getFileName(){
        return this.fileName;
    }
    
    public void setFileName( String fileName ){
        this.fileName = fileName;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public FileArtifact getSource(){
        return this.source;
    }
    
    public void setSource( FileArtifact source ){
        this.source = source;
    }

}