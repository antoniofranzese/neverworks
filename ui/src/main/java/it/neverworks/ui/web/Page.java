package it.neverworks.ui.web;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.collections.Adder;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.Widget;
import it.neverworks.ui.WidgetException;

public class Page extends Form {
    
    private String path;
    
    private Boolean local;

    public Page() {
        super();
    }
    
    public Page( Arguments arguments ) {
        super();
        set( arguments );
    }

    protected void setPath( Setter<String> setter ) {
        setter.set();
        
        if( setter.value() != null ) {
            //TODO: gestire protocollo
            if( ! setter.value().startsWith( "http:" ) && this.local == null ) {
                this.local = true;
            }
        }
    }

    protected void addChildrenItem( Adder<Widget> adder ) {
        throw new WidgetException( "Page does not accept children" );
    }

    protected void setChildren( Setter setter ) {
        throw new WidgetException( "Page does not accept children" );
    }
    
    /* Bean Accessors */
    
    public Boolean getLocal(){
        return this.local;
    }
    
    public void setLocal( Boolean local ){
        this.local = local;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
}