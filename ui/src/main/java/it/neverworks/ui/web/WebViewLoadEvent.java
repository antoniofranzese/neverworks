package it.neverworks.ui.web;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.lifecycle.UIEvent;

@Identification( string = { "location", "accessible", "familiar" })
public class WebViewLoadEvent extends UIEvent {
    
    @Property
    private Location location;
    
    public Location getLocation(){
        return this.location;
    }
    
    public void setLocation( Location location ){
        this.location = location;
    }

    @Property @AutoConvert
    private boolean familiar;
    
    public boolean getFamiliar(){
        return this.familiar;
    }
    
    public void setFamiliar( boolean familiar ){
        this.familiar = familiar;
    }
    
    @Property @AutoConvert
    private boolean accessible;
    
    public boolean getAccessible(){
        return this.accessible;
    }
    
    public void setAccessible( boolean accessible ){
        this.accessible = accessible;
    }
}