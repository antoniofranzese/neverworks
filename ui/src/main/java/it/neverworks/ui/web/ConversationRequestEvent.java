package it.neverworks.ui.web;

import it.neverworks.model.Property;
import it.neverworks.ui.lifecycle.UIEvent;

public class ConversationRequestEvent extends ConversationMessageEvent {
    
    public Object getRequest(){
        return this.get( "!value.request" );
    }

    public Object getId(){
        return this.get( "!value.id" );
    }
    
}
