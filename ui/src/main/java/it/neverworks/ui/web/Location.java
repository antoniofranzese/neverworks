package it.neverworks.ui.web;

import it.neverworks.encoding.json.JSONEncodable;

import it.neverworks.model.BaseModel;
import it.neverworks.model.converters.Convert;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;

@Convert( LocationConverter.class )
public class Location extends BaseModel implements JSONEncodable {
    
    private String href;
    
    public String getHref(){
        return this.href;
    }
    
    public void setHref( String href ){
        this.href = href;
    }
    
    public String toStringURL() {
        return this.href;
    }
    
    public JsonNode toJSON() {
        return new TextNode( this.href );
    }
    
    public String toString() {
        return this.toStringURL();
    }
}