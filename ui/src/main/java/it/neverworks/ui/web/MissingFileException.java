package it.neverworks.ui.web;

public class MissingFileException extends it.neverworks.io.MissingFileException {
    
    public MissingFileException( String message ) {
        super( message );
    }

    public MissingFileException( String message, Throwable cause ) {
        super( message, cause );
    }

}