package it.neverworks.ui.web;

import it.neverworks.model.SafeModel;
import it.neverworks.model.Property;

public class ConversationMessage extends SafeModel {
    @Property String topic;
    @Property Object value;
}