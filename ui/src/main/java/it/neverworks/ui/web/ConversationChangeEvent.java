package it.neverworks.ui.web;

import it.neverworks.model.Property;
import it.neverworks.ui.lifecycle.UIEvent;

public class ConversationChangeEvent extends UIEvent {
    
    @Property
    private Object value;
    
    public Object getValue(){
        return this.value;
    }
    
    public void setValue( Object value ){
        this.value = value;
    }
    
    public Object getNew(){
        return this.value;
    }
    
    public void setNew( Object value ){
        this.value = value;
    }
    
    @Property
    private Object old;
    
    public Object getOld(){
        return this.old;
    }
    
    public void setOld( Object old ){
        this.old = old;
    }

}
