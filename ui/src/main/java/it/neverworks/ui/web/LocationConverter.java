package it.neverworks.ui.web;

import it.neverworks.model.converters.Converter;

public class LocationConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Location ) {
            return value;
        } else if( value instanceof String ) {
            Location location = new Location();
            location.setHref( (String) value );
            return location;
        } else {
            return value;
        }
    }
}