package it.neverworks.ui.web;

import static it.neverworks.language.*;

import java.util.List;
import java.util.Map;
import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.JSON;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Changes;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Promise;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.events.Subscription;
import it.neverworks.model.events.DispatcherHook;
import it.neverworks.model.io.State;
import it.neverworks.log.Logger;

import it.neverworks.ui.lifecycle.BuildContentEvent;
import it.neverworks.ui.lifecycle.ContainerPassivationEvent;

import it.neverworks.ui.AbstractWidget;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;

public class WebView extends BaseWidget implements BorderLayoutCapable {
    
    protected static Logger logger = Logger.of( WebView.class );
    
    @Property @Defended
    protected Location location;
    
    @Property @Defended @AutoConvert
    private String content;
    
    @Property @Defended
    protected String bookmark;
    
    @Property @Defended
    protected LayoutInfo layout;
    
    @Property @Defended
    protected Signal<WebViewLoadEvent> load;
    
    @Property @Defended
    protected Signal<WebViewEmitEvent> event;
    
    @Property @Defended
    protected Signal<WebViewCallbackEvent> errback;

    @Property @Defended
    protected Signal<WebViewCallbackEvent> callback;
    
    @Property @Collection @Defended
    protected List<Arguments> executionQueue;
    
    @Property @Defended
    protected Size width;

    @Property @Defended
    protected Size height;

    @Property @Defended
    protected Box margin;

    @Property @Defended
    protected Box padding;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    protected Border border;

    @Property @AutoConvert
    protected boolean visible = true; 

    @Property @Defended
    protected Boolean loader;
    
    @Property @AutoConvert @Defended
    protected boolean accessible;
    
    @Property @Defended @Collection
    protected Map<String, Object> properties;
    
    @Property @Defended
    protected Boolean displaced;
    
    @Property @Defended
    protected Map<String,Object> initProperties;
    
    protected Promise loadPromise;
    protected Map<String, Promise> waitingPromises = new HashMap<String, Promise>();
    protected Map<String, Signal> rawSignals = new HashMap<String, Signal>();
    
    public WebView() {
        super();
    }

    public WebView( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public WebView init( String property ) {
        return init( property, get( property ) );
    }
    
    public WebView init( String property, Object value ) {
        within( sync( this, t -> t.initProperties == null ), t ->{
            this.initProperties = new HashMap<String,Object>();
        });
        
        this.initProperties.put( property, value );
        return this;
    }
    
    public synchronized Promise load( String location ) {
        if( this.loadPromise != null ) {
            this.loadPromise.cancel();
        }
        
        this.loadPromise = new Promise( Event.class );
        this.set( "location", location );
        
        return this.loadPromise;
    }
    
    public void jump( String bookmark ) {
        this.set( "bookmark", bookmark );
    }
    
    public Promise back() {
        //TODO: implementazione temporanea, attendere il reload se necessario
        return wait( "history.back(); callback();" );
    }
    
    public Promise forward() {
        //TODO: implementazione temporanea, attendere il reload se necessario
        return wait( "history.forward(); callback();" );
    }
    
    protected void handleLoad( Handler<WebViewLoadEvent> handler ) {
        handler.handle();
        if( this.loadPromise != null ) {
            this.loadPromise.resolve( new Event( this ) );
            this.loadPromise = null;
        }
    }

    protected void setLocation( Setter<Location> setter ) {
        if( setter.value() != null ) {
            if( Strings.hasText( setter.value().getHref() ) ) {
                String href = setter.value().getHref();
                int index = href.indexOf( "#" );
                if( index >= 0 ) {
                    setter.value().setHref( href.substring( 0, index ) );
                    set( "bookmark", href.substring( index + 1 ) );
                }
                setter.set();
            } else {
                setter.set( null );
            }
        } else {
            setter.set();
        }
    }

    protected void handleContainerPassivationEvent( ContainerPassivationEvent event ) {
        super.handleContainerPassivationEvent( event );
        raw( "bookmark", null );
    }

    protected void handleEvent( Handler<WebViewEmitEvent> handler ) {
        logger.debug( "Event {0.event.name} state: ({0.event.result.class.name}) {0.event.result}", handler );

        if( handler.handle().isContinued() ) {
            boolean stopped = false;
            String rawName = handler.event().get( "name" );

            if( rawSignals != null && rawSignals.containsKey( rawName ) ) {
                logger.debug( "Event {0.event.name}: handled by raw signal", handler );
                rawSignals.get( rawName ).fire( handler.event() );
            } else {
                String normalizedName = normalizeName( rawName );
                if( model().has( normalizedName, Signal.class ) ) {
                    logger.debug( "Event {0.event.name}: handled by {1.class.name}.{2} signal", handler, this, normalizedName );
                    signal( normalizedName ).fire( handler.event().get( "state" ) );
                }
            }

        }
    }

    protected void handleCallback( Handler<WebViewCallbackEvent> handler ) {
        logger.debug( "Callback {0.event.id}: ({0.event.result.class.name}) {0.event.result}", handler );
        handler.handle();

        if( handler.handle().isContinued() ) {
            if( waitingPromises != null && waitingPromises.containsKey( handler.event().getId() ) ) {
                waitingPromises.get( handler.event().getId() ).resolve( handler.event() );
                waitingPromises.remove( handler.event().getId() );

            } else {
                throw new IllegalArgumentException( "Unknown callback ID: " + handler.event().getId() );
            }
        }
    }

    protected void handleErrback( Handler<WebViewCallbackEvent> handler ) {
        logger.debug( "Errback {0.event.id}: ({0.event.result.class.name}) {0.event.result}", handler );
        handler.handle();

        if( handler.handle().isContinued() ) {
            if( waitingPromises != null && waitingPromises.containsKey( handler.event().getId() ) ) {
                waitingPromises.get( handler.event().getId() ).reject( handler.event() );
                waitingPromises.remove( handler.event().getId() );

            } else {
                throw new IllegalArgumentException( "Unknown errback ID: " + handler.event().getId() );
            }
        }
    }
    
    public void execute( String statement ) {
        execSync( 
            arg( "exec", "sync" )
            .arg( "js", statement )
        );
    }
    
    public void fire( String event ) {
        execSync( 
            arg( "exec", "event" )
            .arg( "name", event )
        );
    }

    public void fire( String event, Object state ) {
        execSync( 
            arg( "exec", "event" )
            .arg( "name", event )
            .arg( "state", state )
        );
    }
    
    public Promise<WebViewCallbackEvent> request( String request ) {
        return execAsync( 
            arg( "exec", "request" )
            .arg( "name", request )
        );
    }
    
    public Promise<WebViewCallbackEvent>request( String request, Object state ) {
        return execAsync( 
            arg( "exec", "request" )
            .arg( "name", request )
            .arg( "state", state )
        );
    }

    public Promise<WebViewCallbackEvent> wait( String statement ) {
        return execAsync( 
            arg( "exec", "async" )
            .arg( "js", statement )
        );
    }
    
    protected void execSync( Arguments params ) {
        value( "executionQueue" ).add( params );
        model().touch( "executionQueue" );
    }

    protected Promise<WebViewCallbackEvent> execAsync( Arguments params ) {
        String id = Strings.generateHexID( 64 );
        
        value( "executionQueue" ).add( params.arg( "id", id ) );
        
        model().touch( "executionQueue" );
    
        Promise promise = new Promise( WebViewCallbackEvent.class );
        waitingPromises.put( id, promise );
        return promise;
    }

    @Override
    public Subscription on( String event, Dispatcher dispatcher ) {
        if( model().has( event, Signal.class ) ) {
            return signal( event ).add( dispatcher );
        } else {
            return getRawSignal( event ).add( dispatcher );
        }
    }
    
    @Override
    public Subscription on( String event, String expression ) {
        if( model().has( event, Signal.class ) ) {
            return signal( event ).add( new DispatcherHook( this, expression ) );
        } else {
            return getRawSignal( event ).add( new DispatcherHook( this, expression ) );
        }
    }
    
    protected Signal getRawSignal( String event ) {
        if( ! rawSignals.containsKey( event ) ) {
            synchronized( this ) {
                if( ! rawSignals.containsKey( event ) ) {
                    rawSignals.put( event, new Signal( WebViewEmitEvent.class ) );
                }
            }
        }
    
        return rawSignals.get( event );
    }
    
    @Override
    public <T> T get( String name ) {
        if( this.getProperties().containsKey( name ) ) {
            return (T) this.getProperties().get( name );
        } else {
            return super.get( name );
        }
    }

    @Override
    public <T extends AbstractWidget> T set( String name, Object value ) {
        if( this.model().has( name ) || ExpressionEvaluator.isExpression( name ) ) {
            return super.set( name, value );
        } else {
            this.getProperties().put( name, value );
            this.model().touch( name );
            return (T)this;
        }
    }

    @Override
    public boolean has( String name ) {
        return this.getProperties().containsKey( name ) || super.has( name );
    }

    @Override
    public void load( State state ) {
        this.loading = true;
        for( String property: state.names() ) {
            this.storeItem( property, state.get( property ) );
        }
        this.loading = false;
    }
    
    /* Bean Accessors */
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }

    public Location getLocation(){
        return this.location;
    }
    
    public void setLocation( Location location ){
        this.location = location;
    }
    
    public Signal<WebViewLoadEvent> getLoad(){
        return this.load;
    }
    
    public void setLoad( Signal<WebViewLoadEvent> load ){
        this.load = load;
    }
 
    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public Box getMargin(){
        return this.margin;
    }
    
    public void setMargin( Box margin ){
        this.margin = margin;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
    
    public Map<String, Object> getProperties(){
        return this.properties;
    }
    
    public void setProperties( Map<String, Object> properties ){
        this.properties = properties;
    }
    
    public String getBookmark(){
        return this.bookmark;
    }
    
    public void setBookmark( String bookmark ){
        this.bookmark = bookmark;
    }
    
    public Boolean getLoader(){
        return this.loader;
    }
    
    public void setLoader( Boolean loader ){
        this.loader = loader;
    }
    
    public boolean getAccessible(){
        return this.accessible;
    }
    
    public void setAccessible( boolean accessible ){
        this.accessible = accessible;
    }

    public String getContent(){
        return this.content;
    }
    
    public void setContent( String content ){
        this.content = content;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
}