package it.neverworks.ui.web;

import java.util.Map;
import java.util.HashMap;
import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.model.Property;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Promise;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.log.Logger;

import it.neverworks.ui.AbstractWidget;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.BehavioralWidget;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.form.Form;

public class Conversation extends BaseWidget implements BehavioralWidget {
    
    private final static Logger logger = Logger.of( Conversation.class );
    
    private final static List<String> RESERVED_TOPICS = list( "change", "message", "request", "response" );
    
    @Property @Defended
    private String channel;
    
    @Property @Defended
    private Signal<ConversationChangeEvent> change;
    
    @Property @Defended
    private Signal<ConversationMessageEvent> message;
    
    @Property @Defended
    private Signal<ConversationRequestEvent> request;
    
    @Property @Defended @Collection
    private Map<String, Signal> messages;
    
    @Property @Defended
    private Object value;
    
    @Property @Defended
    private TypeDefinition type;
    
    @Property @Defended @Collection
    private List<ConversationMessage> messageQueue;

    private Map<String, Promise> requestPromises = new HashMap<String, Promise>();

    public Conversation() {
        super();
    }
    
    public Conversation( Arguments arguments ) {
        super();
        set( arguments );
    }

    protected void handleChange( Handler<ConversationChangeEvent> handler ) {
        handler.event().set( "old", this.get( "value" ) );
        Object value = handler.event().get( "value" );
        if( value != null ) {
            this.raw( "value", this.type != null ? this.type.process( value ) : value );
            handler.event().set( "value", this.get( "value" ) );
        } else {
            this.raw( "value", null );
        }
        handler.handle();
    }
    
    protected void handleMessage( Handler<ConversationMessageEvent> handler ) {
        boolean dispatched = false;
        
        
        ConversationMessageEvent event = handler.event();
        
        if( "response".equals( event.getTopic() ) ) {
            String id = event.get( "!value.id" );
            Promise request = null;
            if( id != null && requestPromises.containsKey( id ) ) {
                synchronized( this ) {
                    if( requestPromises.containsKey( id ) ) {
                        request = requestPromises.get( id );
                        requestPromises.remove( id );
                    }
                }

                if( request != null ) {
                    dispatched = true;
                    logger.debug( "{0.commonName} handling response: {1.event.value}", this, handler );
                    request.resolve( new ConversationResponseEvent()
                        .set( "topic", event.getTopic() )
                        .set( "value", event.getValue() ) 
                    );
                } else {
                    logger.debug( "{0.commonName} discarding response (missing listener): {1.event.value}", this, handler );
                    
                }

            } else {
                logger.debug( "{0.commonName} discarding response (missing id or listener): {1.event.value}", this, handler );
                
            }

            
        } else if( this.model().has( event.getTopic(), Signal.class ) ) {
            logger.debug( "{0.commonName} handling message with {0.class.name}.{1.event.topic} signal: {1.event.value}", this, handler );
            dispatched = true;
            this.<Signal>get( event.getTopic() ).fire( event );

        } else if( this.messages != null && this.messages.containsKey( event.getTopic() ) ){
            logger.debug( "{0.commonName} handling message with \"{1.event.topic}\" registered listener: {1.event.value}", this, handler );
            dispatched = true;
            this.messages.get( event.getTopic() ).fire( event );
        }
        
        if( ! dispatched || event.isBubbled() ) {
            logger.debug( "{0.commonName} handling message \"{1.event.topic}\" with {0.class.name}.message signal: {1.event.value}", this, handler );
            handler.handle();
        } else {
            handler.decline();
        }
    }

    protected void handleRequest( Handler<ConversationRequestEvent> handler ) {
        handler.handle();

        if( handler.event().isReplied() ) {
            Object response = handler.event().getResponse();
            logger.debug( "{0.commonName} replying to \"{1.event.value.id}\" request: {2}", this, handler, response );
            sendMessage( "response", arg( "id", handler.event().get( "id" ) ).arg( "response", response ) );

        } else {
            logger.debug( "{0.commonName} replying to \"{1.event.value.id}\" request without response", this, handler );
            sendMessage( "response", arg( "id", handler.event().get( "id" ) ) );
            
        }
    }
    
    protected void setValue( Setter<Object> setter ) {
        setter.set( this.type != null ? this.type.process( setter.value() ) : setter.value() );
        setter.touch();
    }

    public void send( String topic, Object value ) {
        if( ! RESERVED_TOPICS.contains( topic ) ) {
            sendMessage( topic, value );
        } else {
            throw new IllegalArgumentException( "Reserved topic: " + topic );
        }
    }
    
    protected void sendMessage( String topic, Object value ) {
        logger.debug( "{0.commonName} sending message \"{1}\": {2}", this, topic, value );
        this.<List>get( "messageQueue" ).add( new ConversationMessage().set( "topic", topic ).set( "value", value ) );
        this.model().touch( "messageQueue" );
    }
    
    public Promise<ConversationResponseEvent> request( Object request ) {
        String id = Strings.generateHexID( 64 );

        sendMessage( "request", arg( "id", id ).arg( "request", request ) );
    
        Promise promise = new Promise( ConversationResponseEvent.class );
        requestPromises.put( id, promise );
        return promise;
    }
    
    public <T extends AbstractWidget> T set( String name, Object value ) {
        try {
            return super.set( name, value );
        } catch( MissingPropertyException ex ) {
            if( value instanceof Dispatcher ) {
                String topic = str( name );
                Map<String, Signal> messages = this.get( "messages" );
                if( ! messages.containsKey( topic ) ) {
                    synchronized( this ) {
                        if( ! messages.containsKey( topic ) ) {
                            messages.put( topic, new Signal( ConversationMessageEvent.class ) );
                        }
                    }
                }
                messages.get( topic ).add( (Dispatcher) value );
                return (T) this;
            } else {
                throw ex;
            }
        }
    }
    
    /* Bean Accessors */
    
    public Signal<ConversationChangeEvent> getChange(){
        return this.change;
    }
    
    public void setChange( Signal<ConversationChangeEvent> change ){
        this.change = change;
    }
    
    public String getChannel(){
        return this.channel;
    }
    
    public void setChannel( String channel ){
        this.channel = channel;
    }

    public Object getValue(){
        return this.value;
    }
    
    public void setValue( Object value ){
        this.value = value;
    }

    public Signal<ConversationRequestEvent> getRequest(){
        return this.request;
    }
    
    public void setRequest( Signal<ConversationRequestEvent> request ){
        this.request = request;
    }
    
}