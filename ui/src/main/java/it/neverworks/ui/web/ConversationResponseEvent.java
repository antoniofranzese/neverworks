package it.neverworks.ui.web;

import it.neverworks.model.Property;
import it.neverworks.ui.lifecycle.UIEvent;

public class ConversationResponseEvent extends ConversationMessageEvent {
    
    public Object getResponse(){
        return this.get( "!value.response" );
    }

    public Object getId(){
        return this.get( "!value.id" );
    }
    
    @Override
    protected Object getPayload() {
        return this.getResponse();
    }


}
