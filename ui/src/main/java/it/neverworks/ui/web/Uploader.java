package it.neverworks.ui.web;

import java.util.List;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.description.Virtual;

import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.LabelCapable;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.lifecycle.UIEvent;

public class Uploader extends BaseWidget implements LayoutCapable, LabelCapable {
    
    @Property @Collection
    private List<FileInfo> files;
    
    @Property
    private LayoutInfo layout;
    
    @Property
    private Label label;
    
    @Property
    private Signal<UIEvent> begin;
    
    @Property
    private Signal<UploadEvent> change;
    
    @Property
    private boolean visible = true;
    
    public Uploader() {
        super();
    }
    
    public Uploader( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected void handleChange( Handler<UploadEvent> handler ) {
        raw( "files", handler.event().getFiles() );
        handler.handle();
    }

    public void reset() {
        this.getFiles().clear();
    }
    
    /* Virtual Properties */
    
    @Virtual
    public FileInfo getFile() {
        if( getFiles().size() > 0 ) {
            return getFiles().get( 0 );
        } else {
            return null;
        }
    }
    
    /* Bean Accessors */
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public List<FileInfo> getFiles(){
        return this.files;
    }
    
    public void setFiles( List<FileInfo> files ){
        this.files = files;
    }
    
    public Signal<UploadEvent> getChange(){
        return this.change;
    }
    
    public void setChange( Signal<UploadEvent> change ){
        this.change = change;
    }
    
    public Label getLabel(){
        return this.label;
    }
    
    public void setLabel( Label label ){
        this.label = label;
    }
    
    public Signal<UIEvent> getBegin(){
        return this.begin;
    }
    
    public void setBegin( Signal<UIEvent> begin ){
        this.begin = begin;
    }
    
    public Boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( Boolean visible ){
        this.visible = visible;
    }
    
}