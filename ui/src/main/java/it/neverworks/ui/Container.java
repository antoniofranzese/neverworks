package it.neverworks.ui;

import java.util.List;

import it.neverworks.lang.Filter;
import it.neverworks.model.expressions.ObjectQuery;

public interface Container extends Widget, Iterable<Widget> {
    
    <T> T get( int index );
    Container add( Widget... widgets );    
    Container remove( Widget... widgets );
    Container remove( String... names );
    Container remove( Integer... indexes );
    Container remove( Filter<Widget> filter );
    Container clear();
    List<Widget> getChildren();    
    boolean contains( Widget widget );
    boolean contains( String name );
    boolean contains( int index );
    int size();
    WidgetPlacer place( Widget widget);
    <T> ObjectQuery<T> query( Class<T> cls );
    <T> ObjectQuery<T> query();
    
    default Container cat( Iterable<Widget> widgets ) {
        if( widgets != null ) {
            for( Widget widget: widgets ) {
                if( widget != null ) {
                    add( widget );
                }
            }
        }
        return this;
    }
    
}