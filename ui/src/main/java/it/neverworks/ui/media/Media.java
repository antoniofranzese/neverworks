package it.neverworks.ui.media;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.converters.Convert;
import it.neverworks.ui.data.FileArtifact;

@Convert( MediaConverter.class )
public class Media extends BaseModel {
    
    @Property
    private FileArtifact path;
    
    @Property
    private String type;
    
    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }
    
    public FileArtifact getPath(){
        return this.path;
    }
    
    public void setPath( FileArtifact path ){
        this.path = path;
    }
    
}