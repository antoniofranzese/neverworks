package it.neverworks.ui.media;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Setter;
import it.neverworks.ui.BaseWidget;

public class SoundPlayer extends BaseWidget {

    public static enum PlayMode {
        PLAY, PAUSE, STOP
    }

    @Property
    private Media source;
    
    @Property @AutoConvert
    private Byte volume;
    
    @Property @AutoConvert
    private Boolean loop;
    
    @Property @AutoConvert
    private PlayMode mode;
    
    public SoundPlayer() {
        super();
    }
    
    public SoundPlayer( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    private void setVolume( Setter<Byte> setter ) {
        if( setter.value() < 0 ) {
            setter.set( (byte) 0 );
        } else if( setter.value() > 100 ) {
            setter.set( (byte) 100 );
        } else {
            setter.set();
        }
    }
    
    public void play( Object source ) {
        set( "source", source );
        play();
    }

    public void play() {
        setMode( PlayMode.PLAY );
    }
    
    public void stop() {
        setMode( PlayMode.STOP );
    }
    
    public void pause() {
        setMode( PlayMode.PAUSE );
    }
    
    public PlayMode getMode(){
        return this.mode;
    }
    
    public void setMode( PlayMode mode ){
        this.mode = mode;
    }
    
    public Boolean getLoop(){
        return this.loop;
    }
    
    public void setLoop( Boolean loop ){
        this.loop = loop;
    } 
    
    public Byte getVolume(){
        return this.volume;
    }
    
    public void setVolume( Byte volume ){
        this.volume = volume;
    }
    
    public Media getSource(){
        return this.source;
    }
    
    public void setSource( Media source ){
        this.source = source;
    }
    
}