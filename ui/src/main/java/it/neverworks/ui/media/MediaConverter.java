package it.neverworks.ui.media;

import it.neverworks.model.converters.Converter;
import it.neverworks.ui.data.FileArtifact;

public class MediaConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Media ) {
            return value;
        } else if( value instanceof FileArtifact ) {
            return new Media().set( "path", value );
        } else {
            return value;
        }
    }

}