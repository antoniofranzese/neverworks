package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.EnumConverter;
import it.neverworks.model.types.AutoType;

public class TextArea extends BaseTextBox<String> {
    
    @Convert( ResizabilityConverter.class )
    public static enum Resizability {
        NONE, HORIZONTAL, VERTICAL, BOTH
    }
    
    public static class ResizabilityConverter implements Converter {
        private final static Converter BASE = new EnumConverter( Resizability.class );
        
        public Object convert( Object value ) {
            if( value instanceof Resizability ) {
                return value;
            } else if( value instanceof Boolean ) {
                if( Boolean.TRUE.equals( value ) ) {
                    return Resizability.BOTH;
                } else {
                    return Resizability.NONE;
                }
            } else if( value instanceof String ) {
                if( "true".equalsIgnoreCase( (String) value ) ) {
                    return Resizability.BOTH;
                } else if( "false".equalsIgnoreCase( (String) value ) ) {
                    return Resizability.NONE;
                } else {
                    return BASE.convert( value );
                }
            } else {
                return BASE.convert( value );
            }
        }
    }
   
    @Property @AutoType( String.class )
    protected Object value;
    
    @Property @AutoConvert
    protected Integer rows;
    
    @Property @AutoConvert
    protected Integer columns;
    
    @Property 
    protected Resizability resizable;
    
    public TextArea() {
        super();
    }
    
    public TextArea( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    /* Bean Accessors */

    public Integer getColumns(){
        return this.columns;
    }
    
    public void setColumns( Integer columns ){
        this.columns = columns;
    }
    
    public Integer getRows(){
        return this.rows;
    }
    
    public void setRows( Integer rows ){
        this.rows = rows;
    }
    
    public Resizability getResizable(){
        return this.resizable;
    }
    
    public void setResizable( Resizability resizable ){
        this.resizable = resizable;
    }
    
}