package it.neverworks.ui.form.ribbon;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.Widget;

public class RibbonStrip extends BaseContainerWidget {
    @Property
    private String title;

    public RibbonStrip() {
        super();
    }

    public RibbonStrip( Arguments arguments ) {
        super();
        set( arguments );
    }

    public RibbonStrip( Widget... widgets ) {
        super( widgets );
    }

    public RibbonStrip( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }

}