package it.neverworks.ui.form;

import static it.neverworks.language.*;
import it.neverworks.ui.lifecycle.UIEvent;

public class PopupOpenEvent extends UIEvent {
    
    private Object anchor = undefined; 
    
    public Object getAnchor(){
        return this.anchor;
    }
    
    public void setAnchor( Object anchor ){
        this.anchor = anchor;
    }

}