package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Required;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.types.AutoType;
import it.neverworks.ui.WidgetException;

public class ProgressBar extends BaseFormWidget {

    @Property @AutoType( Integer.class ) @Access( setter = "setIntegerValue" )
    protected Object value = 0;
    
    @Property @Required @Convert( ProgressConverter.class )
    private Integer max = 100;
    
    public ProgressBar() {
        super();
    }
    
    public ProgressBar( Arguments arguments ) {
        super();
        set( arguments );
    }

    protected void setIntegerValue( Setter<Integer> setter ) {
        if( setter.value() != null && setter.value() > getMax() ) {
            throw new WidgetException( "Progress value greater than maximum(" + getMax() + "): " + setter.value() );
        }
        setter.set();
    }
    
    public Integer getMax(){
        return this.max;
    }
    
    public void setMax( Integer max ){
        this.max = max;
    }
    
    @Virtual
    public boolean getIndeterminate() {
        return get( "value" ) == null;
    }
    
    @AutoConvert
    public void setIndeterminate( boolean indeterminate ) {
        set( "value", indeterminate ? null : 0 );
    }
}