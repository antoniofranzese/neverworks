package it.neverworks.ui.form;

import java.util.List;
import it.neverworks.model.validators.ValidationException;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.events.Event;
import it.neverworks.model.Property;

public class ValidationState extends Event {
    
    @Property @Collection
    private List<ValidationException> problems;
    
    public List<ValidationException> getProblems(){
        return this.problems;
    }
    
    public void setProblems( List<ValidationException> problems ){
        this.problems = problems;
    }

    public boolean hasProblems() {
        return this.problems != null && this.problems.size() > 0;
    }

    public boolean isValid() {
        return ! this.hasProblems();
    }
    
    public ValidationState add( ValidationException ex ) {
        getProblems().add( ex );
        return this;
    }
}