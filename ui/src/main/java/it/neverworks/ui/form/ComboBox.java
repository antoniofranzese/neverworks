package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.data.DataSource;

public class ComboBox extends BaseTextBox<String> {
    
    @Property
    private String caption = "/";
    
    @Property
    private DataSource items;
    
    public ComboBox() {
        super();
    }
    
    public ComboBox( Arguments arguments ) {
        this();
        set( arguments );
    }

    /* Bean Accessors */

    public DataSource getItems(){
        return this.items;
    }
    
    public void setItems( DataSource items ){
        this.items = items;
    }
    
    public String getCaption(){
        return this.caption;
    }
    
    public void setCaption( String caption ){
        this.caption = caption;
    }

    
}