package it.neverworks.ui.form;

import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.types.Point;

public class ClickEvent<E> extends UIEvent<E> {
    
    protected Integer x;
    protected Integer y;
    
    public Point getOrigin() {
        return new Point( this.x, this.y );
    }
    
    public Integer getX(){
        return this.x;
    }
    
    public void setX( Integer x ){
        this.x = x;
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY( Integer y ){
        this.y = y;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "x" ).add( "y" ).toString();
    }
}