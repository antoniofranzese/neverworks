package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.ContainerPassivationEvent;

public class FocusStatusEvent extends ContainerPassivationEvent {

    private boolean focused;
    
    public FocusStatusEvent( Object source ) {
        super( source );
    }
    
    public FocusStatusEvent( Object source, String reason ) {
        super( source, reason );
    }
    
    public boolean getFocused(){
        return this.focused;
    }
    
    public void setFocused( boolean focused ){
        this.focused = focused;
    }
}