package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.Property;

public class ListBox extends Select {
    
    @Property
    private Signal<ListBoxClickEvent> click;
    
    @Property
    private Signal<ListBoxClickEvent> inspect;
    
    public ListBox() {
        super();
    }
    
    public ListBox( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected void handleChange( Handler handler ) {
        ListBoxClickEvent clickEvent = new ListBoxClickEvent()
            .set( "x", handler.event().get( "x" ) )
            .set( "y", handler.event().get( "y" ) )
            .set( "id", handler.event().get( "new" ) );
        
        getClick().fire( clickEvent );
        
        super.handleChange( handler );
    }
    
    public Signal<ListBoxClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<ListBoxClickEvent> click ){
        this.click = click;
    }

    public Signal<ListBoxClickEvent> getInspect(){
        return this.inspect;
    }
    
    public void setInspect( Signal<ListBoxClickEvent> inspect ){
        this.inspect = inspect;
    }
    
}