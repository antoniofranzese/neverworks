package it.neverworks.ui.form;

import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Field;

import it.neverworks.lang.Strings;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.utils.ToStringBuilder;

public class IntakeMetadata implements PropertyProcessor, PropertyMetadata {
    
    private List<String> names;
    private boolean nullable;
    
    public IntakeMetadata( AnnotationInfo<Intake> info ) {
        Intake annotation = info.getAnnotation();
        
        final String fieldName = ((Field) info.getTarget()).getName();
        this.names = new ArrayList<String>();
        for( String name: annotation.value() ) {
            this.names.add( Strings.hasText( name ) ? name.trim() : fieldName );
        }
        
        this.nullable = annotation.nullable();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( IntakeMetadata.class ) ) {
            processProperty( property );
        }
    }
    
    public List<String> getNames(){
        return this.names;
    }
    
    public boolean isNullable(){
        return this.nullable;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "names" )
            .add( "nullable" )
            .toString();
    }
}