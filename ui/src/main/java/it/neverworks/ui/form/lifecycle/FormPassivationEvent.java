package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.ContainerPassivationEvent;

public class FormPassivationEvent extends ContainerPassivationEvent {

    public FormPassivationEvent( Object source ) {
        super( source );
    }
    
    public FormPassivationEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}