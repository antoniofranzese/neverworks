package it.neverworks.ui.form;

import java.util.Map;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.types.AutoType;
import it.neverworks.ui.types.Attributes;

public class HTMLEditor extends BaseTextBox<String> {
   
    @Property @AutoType( String.class )
    protected Object value;

    @Property
    protected Attributes styles;

    @Property @Collection( item = AutoConvert.class )
    protected Map<String,Boolean> features;
    
    public Map<String,Boolean> getFeatures(){
        return this.features;
    }
    
    public void setFeatures( Map<String,Boolean> features ){
        this.features = features;
    }

    public HTMLEditor() {
        super();
    }
    
    public HTMLEditor( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public Attributes getStyles(){
        return this.styles;
    }
    
    public void setStyles( Attributes styles ){
        this.styles = styles;
    }

}