package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.events.Signal;
import it.neverworks.ui.lifecycle.UIEvent;

public class SmartTextBox extends TextBox {
    
    @Property
    private Signal<UIEvent> click;
    
    public SmartTextBox() {
        super();
    }
    
    public SmartTextBox( Arguments arguments ) {
        this();
        set( arguments );
    }

    public Signal<UIEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<UIEvent> click ){
        this.click = click;
    }

}