package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Virtual;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.lifecycle.UIEvent;

public abstract class BaseTextBox<T> extends BaseFormWidget<T> {
    
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }
    
    @Property
    protected Signal<ChangeEvent> change;
    
    @Property
    protected Signal<UIEvent> enterPress;
    
    @Property
    protected Signal<UIEvent> tabPress;
    
    @Property @AutoConvert
    protected HorizontalAlignment horizontalAlignment;
    
    @Property
    protected Color color;
    
    @Property
    protected Color background;
    
    @Property @AutoConvert
    protected boolean sealed = false;

    @Property
    protected Corners rounding;

    @Property
    protected Shadow shadow;
    
    @Property
    protected Label title;
    
    public BaseTextBox() {
        super();
    }
    
    public BaseTextBox( Arguments arguments ) {
        super( arguments );
    }

    protected boolean canHandle( Handler<UIEvent> handler ) {
        if( ( 
                ! locked 
                && getEnabled() 
                && getWritable() 
                && getVisible()
                && ! sealed 
            ) 
            || !handler.event().getForeign() ) {
            return true;
        } else {
            model().touch( "visible" );
            model().touch( "disabled" );
            model().touch( "readonly" );
            model().touch( "sealed" );
            return false;
        }
    }

    protected void handleChange( Handler<UIEvent> handler ) {
        if( canHandle( handler ) ) {
            handler.handle();
        } else {
            handler.decline();
        }
    }

    protected void handleTabPress( Handler<UIEvent> handler ) {
        if( canHandle( handler ) ) {
            handler.handle();
        } else {
            handler.decline();
        }
    }

    protected void handleEnterPress( Handler<UIEvent> handler ) {
        if( canHandle( handler ) ) {
            handler.handle();
        } else {
            handler.decline();
        }
    }

    /* Virtual Properties */
    
    @Virtual
    public Object getHalign(){
        return get( "horizontalAlignment" );
    }
    
    public void setHalign( Object halign ){
        set( "horizontalAlignment", halign );
    }

    /* Bean Accessors */
    
    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Signal<ChangeEvent> getChange(){
        return this.change;
    }
    
    public void setChange( Signal<ChangeEvent> change ){
        this.change = change;
    }
    public Signal<UIEvent> getEnterPress(){
        return this.enterPress;
    }
    
    public void setEnterPress( Signal<UIEvent> enterPress ){
        this.enterPress = enterPress;
    }
    
    public Signal<UIEvent> getTabPress(){
        return this.tabPress;
    }
    
    public void setTabPress( Signal<UIEvent> tabPress ){
        this.tabPress = tabPress;
    }
    
    public HorizontalAlignment getHorizontalAlignment(){
        return this.horizontalAlignment;
    }
    
    public void setHorizontalAlignment( HorizontalAlignment horizontalAlignment ){
        this.horizontalAlignment = horizontalAlignment;
    }
    
    public boolean getSealed(){
        return this.sealed;
    }
    
    public void setSealed( boolean sealed ){
        this.sealed = sealed;
    }
    
    public Corners getRounding(){
        return this.rounding;
    }
    
    public void setRounding( Corners rounding ){
        this.rounding = rounding;
    }
    
    
    public Label getTitle(){
        return this.title;
    }
    
    public void setTitle( Label title ){
        this.title = title;
    }
    
}