package it.neverworks.ui.form;

import it.neverworks.model.validators.Validator;
import it.neverworks.ui.Widget;

public interface WidgetValidator<T extends Widget> extends Validator<T> {
    
}