package it.neverworks.ui.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( { ElementType.PACKAGE } )
@Retention( RetentionPolicy.RUNTIME )
public @interface MainForm {
    Class<? extends Form> value();
}