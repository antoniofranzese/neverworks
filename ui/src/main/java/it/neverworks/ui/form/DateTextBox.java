package it.neverworks.ui.form;

import java.util.Date;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.types.AutoType;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.features.Resolve;

public class DateTextBox extends BaseTextBox<Date> {
    
    //TODO: tipo a configurazione variabile con un InstanceConverter
    @Property @AutoType( Date.class )
    protected Object value;
    
    @Property @Resolve
    private TimeValue time;
    
    public DateTextBox() {
        super();
    }
    
    public DateTextBox( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected Date getValue( Getter<Date> getter ) {
        Date value = getter.get();
        if( value != null && getTime() != null ) {
            return getTime().writeTo( value );
        } else {
            return value;
        }
    }
    
    protected void setValue( Setter<Date> setter ) {
        Date value = setter.set();
        if( getTime() instanceof MutableTimeValue ) {
            ((MutableTimeValue) getTime()).readFrom( value );
        }
    }
    

    public TimeValue getTime(){
        return this.time;
    }

    public void setTime( TimeValue time ){
        this.time = time;
    }
    
}