package it.neverworks.ui.form;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.types.Type;
import it.neverworks.model.types.AutoType;
import it.neverworks.model.description.Default;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.Converter;

public class NumberTextBox<T extends Number> extends BaseTextBox<T> {
    
    @Property 
    protected Object value;

    @Property @Type( Object.class ) @Access( setter = "setValue" ) 
    protected Number min;

    @Property @Type( Object.class ) @Access( setter = "setValue" ) 
    protected Number max;
    
    @Property @Default @Ignore @AutoConvert
    protected Class<? extends Number> type = Integer.class; 
    
    @Property
    private DecimalPlaces decimals;
    
    protected Converter converter = new IntConverter();
    
    public NumberTextBox() {
        super();
    }
    
    public NumberTextBox( Arguments arguments ) {
        super();
        set( arguments );
    }

    protected void setType( Setter<Class<?>> setter ) {
        if( setter.value() != null ) {
            Class<?> value = setter.value().isPrimitive() ? Numbers.reference( setter.value() ) : setter.value();
            if( Number.class.isAssignableFrom( value ) ) {
                //TODO: TypeFactory
                setter.set( value );
                this.converter = AutoConvertProcessor.inferConverter( value );
                set( "value", get( "value" ) );

            } else {
                throw new IllegalArgumentException( "NumberTextBox type must be subclass of Number" );
            }

        } else {
            throw new IllegalArgumentException( "NumberTextBox type cannot be null"  );
        }
    }

    protected void setValue( Setter setter ) {
        try {
            setter.set( processValue( setter.value() ) );
        } catch( IllegalArgumentException ex ) {
            throw new IllegalArgumentException( "Wrong " + setter.getName() + " for " + this.getCommonName() + ": " + ex.getMessage() );
        }
    }
    
    protected Object processValue( Object value ) {
        Object processed = this.converter != null ? this.converter.convert( value ) : value;
        if( processed != null ) {
            if( getType().isAssignableFrom( processed.getClass() ) ) {
                return processed;
            } else {
                throw new IllegalArgumentException( Objects.className( this.type ) + " expected, " + Objects.repr( value ) + " provided" );
            }

        } else {
            return null;
        }
        
    }    
    
    public Class<? extends Number> getType(){
        return this.type;
    }
    
    public void setType( Class<? extends Number> type ){
        this.type = type;
    }
    
    public Number getMax(){
        return this.max;
    }
    
    public void setMax( Number max ){
        this.max = max;
    }
    
    public Number getMin(){
        return this.min;
    }
    
    public void setMin( Number min ){
        this.min = min;
    }
    
    public DecimalPlaces getDecimals(){
        return this.decimals;
    }
    
    public void setDecimals( DecimalPlaces decimals ){
        this.decimals = decimals;
    }
    
}