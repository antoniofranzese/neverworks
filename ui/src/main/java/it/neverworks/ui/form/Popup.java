package it.neverworks.ui.form;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Objects;

import it.neverworks.model.Property;
import it.neverworks.model.types.Type;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.types.Type;

import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.AnchorType;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;

import it.neverworks.ui.form.lifecycle.FocusRequestEvent;
import it.neverworks.ui.form.lifecycle.FocusReleaseEvent;
import it.neverworks.ui.form.lifecycle.FocusStatusEvent;
import it.neverworks.model.io.State;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.Widget;

public class Popup extends Form {
    
    public enum Design {
        DIALOG, TOOLTIP, SLIDEUP
    }
    
    @Property @Defended
    private boolean autoClose = true; 
    
    @Property @Defended
    protected boolean autoDestroy = false;

    @Property @Defended @AutoConvert
    protected int top;
    
    @Property @Defended @AutoConvert
    protected int left;
    
    @Property @Defended
    protected Size minimumWidth;

    @Property @Defended
    protected Size minimumHeight;
    
    @Property @Defended
    private Size maximumWidth;
    
    @Property @Defended
    private Size maximumHeight;
    
    @Property @Defended
    protected Signal<PopupOpenEvent> open;
    
    @Property @Defended @Type( AnchorType.class )
    protected Object anchor;
    
    @Property @Defended @AutoConvert
    protected Design design;
    
    @Property @Defended
    protected boolean resizable = false; 
    
    public Popup() {
        closable = true;
        opened = false;
    }
    
    public Popup( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Popup( Widget... widgets ) {
        this();
        add( widgets );
    }

    public Popup( Arguments arguments, Widget... widgets ) {
        this();
        set( arguments );
        add( widgets );
    }

    public Popup open() {
        event( "open" ).fire();
        return this;
    }
    
    public Popup open( Object anchor ) {
        event( "open" ).fire( new PopupOpenEvent().set( "anchor", anchor ) );
        return this;
    }
    
    public Popup open( int x, int y ) {
        return open( new Point( x, y ) );
    }

    @Override
    protected boolean canValidate() {
        return opened && visible && ! destroyed;
    }
    
    protected void setAnchor( Setter<Object> setter ) {
        if( setter.value() == null || setter.value() instanceof Widget || setter.value() instanceof Point ) {
            setter.set();
            
            if( anchor == null && design == Design.TOOLTIP ) {
                model()
                    .raw( "design", Design.DIALOG )
                    .touch( "design" );
            }
            
        } else {
            throw new IllegalArgumentException( "Invalid anchor: " + Objects.repr( setter.value() ) );
        }
    }
    
    protected Design getDesign( Getter<Design> getter ) {
        if( getter.defined() ) {
            return getter.get();
        } else {
            return getter.get( anchor == null ? Design.DIALOG : Design.TOOLTIP );
        }
    }
    
    protected void setDesign( Setter<Design> setter ) {
        if( setter.value() != null ) {
            setter.set();
            
            if( design != Design.DIALOG && resizable ) {
                set( "resizable", false );
            }
            
        } else {
            setter.set( Design.DIALOG );
        }
    }
    
    @Override
    protected void setOpened( Setter<Boolean> setter ) {
        if( active ) {
            if( ! setter.raw().equals( setter.value() ) ) {
                event(  Boolean.TRUE.equals( setter.value() ) ? "open" : "close" ).fire();
            }
            setter.decline();
        } else {
            setter.set();
        }
    }
    
    protected void handleOpen( Handler handler ) {
        if( ! undefined( handler.event().get( "anchor" ) ) ) {
            set( "anchor", handler.event().get( "anchor" ) );
        }
        
        if( handler.handle().isContinued() ) {
            model()
                .raw( "opened", true )
                .touch( "opened" );
        }
        
    }

    public Popup close() {
        event( "close" ).fire();
        return this;
    }
    
    public Popup destroy() {
        event( "destroy" ).fire();
        return this;
    }
    
    @Override
    protected void handleClose( Handler handler ) {
        if( this.opened || this.parent != null ) {
            model()
                .raw( "opened", false )
                .touch( "opened" );
            
            if( handler.handle().isContinued() && this.autoDestroy ) {
                this.destroy();
            }
        } else {
            handler.decline();
        }
    }
    
    @Override
    protected void handleDestroy( Handler handler ) {
        if( ! this.destroyed ) {
            Form form = (Form)getParent();
            if( form != null ) {
                if( handler.handle().isContinued() ) {
                    raw( "destroyed", true );
                    if( ! handler.event().get( "!attributes.leaving", false ) ) {
                        form.remove( this );
                    }
                } 
            } else {
                handler.decline();
            }
        } else {
            handler.decline();
        }
    }
    
    public void load( State state ) {
        super.load( state );

        for( String property: state.names() ) {
            if( "state".equals( property ) ) {
                continue;
            }
            if( ! this.resizable && ( "width".equals( property ) || "height".equals( property ) ) ) {
                continue;
            }
            this.set( property, state.get( property ) );
        }
    }
    
    public <T extends Form> T form() {
        return getForm();
    }
    
    @Virtual
    public <T extends Form> T getForm() {
        return (T)getParent();
    }
    
    @Override
    public void joinParent( Widget parent ) {
        if( parent instanceof Form ) {
            super.joinParent( parent );
        } else {
            throw new WidgetException( "Popup must be placed within a Form" );
        }
    }
    
    public void focus( Widget widget ) {
        setActiveChild( widget );
    }

    @Override
    protected void handleFocusRequestEvent( FocusRequestEvent event ) {
        focus( (Widget) event.getSource() );
    }

    @Override
    protected void handleFocusReleaseEvent( FocusReleaseEvent event ) {
        if( event.getSource() == getActiveChild() ) {
            blur();
        }
    }

    @Override
    protected void handleFocusStatusEvent( FocusStatusEvent event ) {
        event.setFocused( event.getSource() == getActiveChild() );
    }
    
    /* Bean Accessors */
    
    public boolean getOpened(){
        return this.opened;
    }
    
    public void setOpened( boolean opened ){
        this.opened = opened;
    }
    
    public Signal getOpen(){
        return this.open;
    }
    
    public int getLeft(){
        return this.left;
    }
    
    public void setLeft( int left ){
        this.left = left;
    }
    
    public int getTop(){
        return this.top;
    }
    
    public void setTop( int top ){
        this.top = top;
    }
    
    public boolean getAutoClose(){
        return this.autoClose;
    }
    
    public void setAutoClose( boolean autoClose ){
        this.autoClose = autoClose;
    }
    
    public boolean getAutoDestroy(){
        return this.autoDestroy;
    }
    
    public void setAutoDestroy( boolean autoDestroy ){
        this.autoDestroy = autoDestroy;
    }
    
    public Size getMinimumHeight(){
        return this.minimumHeight;
    }
    
    public void setMinimumHeight( Size minimumHeight ){
        this.minimumHeight = minimumHeight;
    }
    
    public Size getMinimumWidth(){
        return this.minimumWidth;
    }
    
    public void setMinimumWidth( Size minimumWidth ){
        this.minimumWidth = minimumWidth;
    }
    
    public Size getMaximumHeight(){
        return this.maximumHeight;
    }
    
    public void setMaximumHeight( Size maximumHeight ){
        this.maximumHeight = maximumHeight;
    }
    
    public Size getMaximumWidth(){
        return this.maximumWidth;
    }
    
    public void setMaximumWidth( Size maximumWidth ){
        this.maximumWidth = maximumWidth;
    }
    
    public Object getAnchor(){
        return this.anchor;
    }
    
    public void setAnchor( Object anchor ){
        this.anchor = anchor;
    }
    
    public boolean getResizable(){
        return this.resizable;
    }
    
    public void setResizable( boolean resizable ){
        this.resizable = resizable;
    }
    
}