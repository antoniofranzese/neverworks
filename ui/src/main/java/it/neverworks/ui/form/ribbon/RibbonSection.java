package it.neverworks.ui.form.ribbon;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.Widget;

public class RibbonSection extends BaseContainerWidget {
    
    public enum Orientation {
        HORIZONTAL, VERTICAL
    }
    
    @Property @AutoConvert
    Orientation orientation;
    
    public RibbonSection() {
        super();
    }

    public RibbonSection( Arguments arguments ) {
        super();
        set( arguments );
    }

    public RibbonSection( Widget... widgets ) {
        super( widgets );
    }

    public RibbonSection( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    public Orientation getOrientation(){
        return this.orientation;
    }
    
    public void setOrientation( Orientation orientation ){
        this.orientation = orientation;
    }
    
    
}