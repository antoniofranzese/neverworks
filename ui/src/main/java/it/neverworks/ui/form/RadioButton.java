package it.neverworks.ui.form;

import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.BaseWidget;

public class RadioButton extends BaseFormWidget implements LayoutCapable {
    
    @Property @Required
    private RadioGroup group;
    
    @Property @Required
    private Object item;
    
    @Property
    private boolean labelled = false;
    
    @Property
    private LayoutInfo layout;
    
    public RadioButton() {
        super();
    }

    public RadioButton( RadioGroup group, Object item ) {
        super();
        this.group = group;
        this.item = item;
    }
    
    public boolean getLabelled(){
        return this.labelled;
    }
    
    public void setLabelled( boolean labelled ){
        this.labelled = labelled;
    }
    
    public Object getItem(){
        return this.item;
    }
    
    public RadioGroup getGroup(){
        return this.group;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
}