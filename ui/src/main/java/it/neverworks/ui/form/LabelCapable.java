package it.neverworks.ui.form;

public interface LabelCapable {
    public Label getLabel();
}