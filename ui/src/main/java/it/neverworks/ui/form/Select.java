package it.neverworks.ui.form;

import static it.neverworks.language.*;

import java.util.List;
import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.Required;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.types.AutoType;

import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;

import it.neverworks.ui.data.Inventory;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.DataSourceException;
import it.neverworks.ui.data.DataSourceKeyProvider;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.ExpressionFormatter;
import it.neverworks.ui.data.PropertyFormatterConverter;
import it.neverworks.ui.lifecycle.ContainerActivationEvent;
import it.neverworks.ui.lifecycle.UIEvent;

public class Select extends BaseFormWidget<Object> implements DataSourceKeyProvider, Inventory {
    
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }
    
    public enum Encoding {
        TEXT, HTML
    }

    @Property @AutoConvert
    protected HorizontalAlignment horizontalAlignment;
    /**
    DataSource da cui prelevare gli elementi da mostrare nella lista
    */  
    @Property @Child
    protected DataSource items;
    
    /**
    Espressione da valutare sul singolo elemento per ottenere il valore effettivo (chiave)
    */  
    @Property
    protected String key = "id";
    
    /**
    Espressione da valutare sul singolo elemento per ottenere l'etichetta da mostrare nella lista
    */  
    @Property @Convert( PropertyFormatterConverter.class )
    protected ItemFormatter caption = new ExpressionFormatter( "name" );
    
    @Property @Convert( PropertyFormatterConverter.class )
    protected ItemFormatter text;

    /**
    Elemento selezionato in base al valore corrente
    */  
    @Property @Virtual
    protected Object selected;
    
    /**
    Mostra anche un elemento vuoto, con valore **null**
    */  
    @Property
    protected boolean blank = false; 
    
    @Property @AutoConvert
    private Encoding encoding;
    
    /**
    Evento della variazione del valore corrente
    */  
    @Property
    protected Signal<SelectChangeEvent> change;
    
    /**
    Evento della perdita del valore corrente a seguito di variazioni *(es. items)* che lo rendono non più valido
    */  
    @Property
    protected Signal<SelectResetEvent> reset;
    
    @Property
    protected Signal<SelectChangeEvent> init;
    
    protected boolean initValue = false;
    
    public Select() {
        super();
    }
    
    public Select( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    protected boolean canHandle( Handler<UIEvent> handler ) {
        if( ( 
                (!locked) 
                && getEnabled() 
                && getWritable() 
                && getVisible() 
            ) 
            || !handler.event().getForeign() ) {
            return true;
        } else {
            model().touch( "visible" );
            model().touch( "disabled" );
            model().touch( "readonly" );
            return false;
        }
    }

    protected void handleChange( Handler<UIEvent> handler ) {
        if( canHandle( handler ) ) {
            handler.handle();
        } else {
            handler.decline();
        }
    }
    
    protected Object getValue( Getter<Object> getter ) {
        this.checkValueInitialization();
        return getter.get();
    }
    
    protected void setValue( Setter<Object> setter ) {
        //System.out.println( this.getName() + " set value, active " + active + ", built " + built );
        if( !active && !built ) {
            //TODO: signal( "activate" ).add( slot( "initValue" ).once() )
            initValue = true;
        }
        
        Object previous = setter.raw();
        if( setter.value() != null ) {
            try {
                DataSource items = getItems();
                Object item = items.load( key, setter.value() );
                raw( "selected", item );
                // Assicura il tipo di dato giusto
                setter.set( items.get( item, key ) );
            } catch( DataSourceException ex ) {
                if( !initValue ) {
                    setter.set( null );
                    raw( "selected", null );
                    throw ex;
                } else {
                    //TODO: riconsiderare
                    setter.set();
                }
            }
        } else {
            setter.set( null );
            raw( "selected", null );
        }

        if( active && !loading && ( previous != null ? !previous.equals( setter.raw() ) : setter.raw() != null ) ) {
            //System.out.println( this.getName() + " fires change" );
            //getChange().fire( arg( "old", previous ).arg( "new", setter.raw() ).arg( "init", initValue ) );

            if( initValue /* && getInit().size() > 0 */ ) { // Riattivare && per il wiring automatico tra init e change
                //System.out.println( this.getName() + " fires init event" );
                getInit().fire( arg( "new", setter.raw() ) );
            } else {
                //System.out.println( this.getName() + " fires change event" );
                getChange().fire( arg( "old", previous ).arg( "new", setter.raw() ) );
            }
        }

    }

    protected Object getSelected( Getter<Object> getter ) {
        this.checkValueInitialization();
        return getter.get();
    }
    
    protected void setSelected( Setter<Object> setter ) {
        if( setter.value() != null ) {
            Object key;
            try {
                key = getItems().get( setter.value(), this.key );
            } catch( Exception ex ) {
                key = setter.value();
            }
            set( "value", key );
        } else {
            set( "value", null );
        }

        setter.decline();
    }
    
    protected void setItems( Setter<DataSource> setter ) {
        ////System.out.println( this.getName() + " set Items, active " + this.active );
        Object previous = raw( "value" );
        Object previousItem = raw( "selected" );
        setter.set();
        if( active ) {
            DataSource items = setter.value();
            if( previous != null ) {
                if( items != null ) {
                    try {
                        Object item = items.load( key, previous );
                        raw( "selected", item );
                        // Assicura il tipo di dato giusto
                        raw( "value", items.get( item, key ) );
                    } catch( DataSourceException ex ) {
                        raw( "selected", null );
                        raw( "value", null );
                    }
                } else {
                    raw( "selected", null );
                    raw( "value", null );
                }
            
                if( active && !loading ){
                    if( previous != null && raw( "value" ) == null ) {
                        getReset().fire( 
                            arg( "value", previous )
                            .arg( "item", previousItem ) 
                        );
                    }

                    if( !previous.equals( raw( "value" ) ) ) {
                        getChange().fire( 
                            arg( "old", previous )
                            .arg( "new", raw( "value" ) )
                            .arg( "oldItem", previous != null ? previousItem : null ) 
                        );
                    }
                } 
            }
        }
    }
    
    protected void setKey( Setter<String> setter ) {
        setter.set();
        if( items != null && items.getDefaultKey() == null ) {
            items.setDefaultKey( setter.value() );
        }
    }

    public String getDataSourceKey() {
        return this.key;
    }

    protected void setBlank( Setter<Boolean> setter ) {
        setter.set();
        model().touch( "items" );
    }
    
    public <T> T select( Object selectable ) {
        set( "selected", selectable );
        return (T)this.selected;
    }
    
    public <T> T deselect() {
        T selected = (T) this.selected;
        set( "value", null );
        return selected;
    }

    public <T> T item( Object key ) {
        return (T)getItems().load( this.key, key );
    }
    
    protected void handleContainerActivationEvent( ContainerActivationEvent event ) {
        super.handleContainerActivationEvent( event );
        this.checkValueInitialization();
    }

    protected void checkValueInitialization() {
        if( initValue ) {
            synchronized( this ) {
                if( initValue ) {
                    Object value = raw( "value" );
                    raw( "value", null );
                    set( "value", value );
                    initValue = false;
                }
            }
        }
    }
    
    /* Virtual properties */
    
    @Virtual
    public Object getItem(){
        return get( "selected" );
    }

    public void setItem( Object item ){
        set( "selected", item );
    }

    @Virtual
    public Object getHalign(){
        return get( "horizontalAlignment" );
    }
    
    public void setHalign( Object halign ){
        set( "horizontalAlignment", halign );
    }

    @Virtual @Override
    public Object getDisplayValue(){
        return get( "selected" ) != null && caption != null ? caption.format( get( "selected" ) ) : null;
    }

    /* Bean Accessors */

    public DataSource getItems(){
        return this.items;
    }
    
    public void setItems( DataSource items ){
        this.items = items;
    }  
       
    public ItemFormatter getCaption(){
        return this.caption;
    }
    
    public void setCaption( ItemFormatter caption ){
        this.caption = caption;
    }
    
    public String getKey(){
        return this.key;
    }
    
    public void setKey( String key ){
        this.key = key;
    }

    public Object getSelected(){
        return this.selected;
    }
    
    public void setSelected( Object selected ){
        this.selected = selected;
    }

    public Signal<SelectChangeEvent> getChange(){
        return this.change;
    }
    
    public void setChange( Signal<SelectChangeEvent> change ){
        this.change = change;
    }
    
    public boolean getBlank(){
        return this.blank;
    }
    
    public void setBlank( boolean blank ){
        this.blank = blank;
    }
    
    public Signal<SelectResetEvent> getReset(){
        return this.reset;
    }
    
    public void setReset( Signal<SelectResetEvent> reset ){
        this.reset = reset;
    }
    
    public Signal<SelectChangeEvent> getInit(){
        return this.init;
    }

    public void setInit( Signal<SelectChangeEvent> init ){
        this.init = init;
    }
    
    public HorizontalAlignment getHorizontalAlignment(){
        return this.horizontalAlignment;
    }
    
    public void setHorizontalAlignment( HorizontalAlignment horizontalAlignment ){
        this.horizontalAlignment = horizontalAlignment;
    }
    
    public Encoding getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( Encoding encoding ){
        this.encoding = encoding;
    }
    
    public ItemFormatter getText(){
        return this.text;
    }
    
    public void setText( ItemFormatter text ){
        this.text = text;
    }
    
}