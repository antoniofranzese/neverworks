package it.neverworks.ui.form;

import static it.neverworks.language.*;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Queue;
import java.util.ArrayDeque;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.context.TaskList;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.events.SignalHook;
import it.neverworks.model.collections.Adder;
import it.neverworks.model.collections.Remover;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.description.Watcher;
import it.neverworks.model.description.Changes;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.validators.ValidationException;
import it.neverworks.model.validators.Validator;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.types.Type;
import it.neverworks.model.io.State;
import it.neverworks.model.Property;

import it.neverworks.ui.Widget;
import it.neverworks.ui.AbstractWidget;
import it.neverworks.ui.Container;
import it.neverworks.ui.Contained;
import it.neverworks.ui.Collector;
import it.neverworks.ui.WidgetSelection;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.layout.PanelCapable;
import it.neverworks.ui.layout.TitleCapable;
import it.neverworks.ui.layout.CloseCapable;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.StructureCapable;
import it.neverworks.ui.layout.VisibilityCapable;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.util.TaskCollector;
import it.neverworks.ui.util.WidgetArrayList;;
import it.neverworks.ui.lifecycle.Destroyable;
import it.neverworks.ui.lifecycle.DestroyEvent;
import it.neverworks.ui.lifecycle.WidgetAddedEvent;
import it.neverworks.ui.lifecycle.WidgetRemovedEvent;
import it.neverworks.ui.lifecycle.WidgetAbandonedEvent;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.WidgetPersistedEvent;
import it.neverworks.ui.lifecycle.BuildContentEvent;
import it.neverworks.ui.lifecycle.ContainerActivationEvent;
import it.neverworks.ui.lifecycle.ContainerPassivationEvent;
import it.neverworks.ui.lifecycle.RotationEvent;
import it.neverworks.ui.lifecycle.EnqueueEvent;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.form.lifecycle.FormActivationEvent;
import it.neverworks.ui.form.lifecycle.FormPassivationEvent;
import it.neverworks.ui.form.lifecycle.FormJoinEvent;
import it.neverworks.ui.form.lifecycle.FormLeaveEvent;
import it.neverworks.ui.form.lifecycle.FocusRequestEvent;
import it.neverworks.ui.form.lifecycle.FocusReleaseEvent;
import it.neverworks.ui.form.lifecycle.FocusStatusEvent;

public class Form extends BaseContainerWidget implements Collector, Contained, BorderLayoutCapable, StructureCapable, PanelCapable, TitleCapable, CloseCapable, Flavored, VisibilityCapable, ValidationCapable, Destroyable, TaskCollector {
    
    @Property @Defended
    protected LayoutInfo layout;
    
    @Property @Defended
    protected String title;
    
    @Property @Defended
    protected Color background;
    
    @Property @Defended @AutoConvert
    protected boolean scrollable;
    
    @Property @Defended @AutoConvert
    protected boolean closable = false;
    
    @Property @Defended @AutoConvert
    protected boolean visible = true;
    
    @Property @Defended
    protected Form destination;
    
    @Property @Defended @Type( Size.class )
    protected Object width;
    
    @Property @Defended @Type( Size.class )
    protected Object height;
    
    @Property @Defended 
    protected Box margin;
    
    @Property @Defended
    protected Box padding;
    
    @Property @Defended
    protected Font font;
    
    @Property @Defended
    protected StyleArtifact flavor;
    
    @Property @Defended
    protected Signal<FormJoinEvent> join;
    
    @Property @Defended
    protected Signal<FormLeaveEvent> leave;

    @Property @Defended
    protected Signal<FormActivationEvent> activate;

    @Property @Defended
    protected Signal<FormPassivationEvent> passivate;
    
    @Property @Defended
    protected Signal<FormActivationEvent> activating;
    
    @Property @Defended
    protected Signal<FormPassivationEvent> passivating;
    
    @Property @Defended
    protected Signal<RotationEvent> rotate;
    
    @Property @Defended
    protected Signal<Event> dequeue;
    
    @Property @Defended
    protected Signal close;
    
    @Property @Defended
    protected Signal<DestroyEvent> destroy;
    
    @Property @Defended
    protected Signal reset;
    
    @Property @Defended
    protected Widget activeChild;
    
    @Property @Defended
    protected Signal<Event> nextTick;
    
    @Property @Defended
    protected Signal<Event> adopt;
    
    @Property @Defended
    protected Signal<Event> orphan;
    
    @Property @Defended @AutoConvert
    protected Boolean opened = true;
    
    @Property @Defended
    protected boolean destroyed = false;
    
    @Property @Defended
    protected Signal<ValidationState> validate;
    
    @Property @Collection( wrap = true )
    protected List<Validator> validation;
    
    private Map<String, Widget> bubbledChildrenRegistry = new LinkedHashMap<String, Widget>();
    private Map<String, ReferenceDescriptor> references;
    private List<Dispatcher> nextTickQueue;
    private boolean nextTicking = false;
    private TaskList tasks;
    private Queue<EnqueueEvent> queue;
    
    public Form() {
        processReferences();
    }
    
    public Form( Widget... widgets ) {
        this();
        add( widgets );
    }

    public Form( Arguments arguments ) {
        this();
        set( arguments );
    }

    public Form( Arguments arguments, Widget... widgets ) {
        this();
        set( arguments );
        add( widgets );
    }
    
    public <T extends Form> T activate() {
        if( !active ) {
            //System.out.println( "Activating " + this.getCommonName() );
            spread( notify( new BuildContentEvent( this, "Form activation" ) ) );
            signal( "activate" ).fire( spread( notify( signal( "activating" ).fire( new FormActivationEvent( this ) ) ) ) );
            //TODO: join recovery?
        }
        return (T)this;
    }
    
    public <T extends Form> T passivate() {
        if( active ) {
            //System.out.println( "Passivating " + this.getCommonName() );
            signal( "passivate" ).fire( spread( notify( signal( "passivating" ).fire( new FormPassivationEvent( this ) ) ) ) );
            //TODO: leave recovery?
        }
        return (T)this;
    }
    
    // @Override
    // protected void setParent( Setter<Widget> setter ) {
    //     Widget previous = setter.raw();
    //
    //     if( previous != null && !previous.equals( setter.value() ) ) {
    //         event( "orphan" ).fire();
    //     }
    //
    //     super.setParent( setter );
    //
    //     if( setter.raw() != null ) {
    //         event( "adopt" ).fire();
    //
    //         this.bubbleQueue();
    //     }
    //
    // }
    
    public void joinParent( Widget parent ) {
        if( event( "adopt" ).fire().isContinued() ) {
            raw( "destroyed", false );
            set( "parent", parent );
            this.bubbleQueue();
        }
    }
    
    public void leaveParent( Widget parent ) {
        if( signal( "orphan" ).fire().isContinued() ) {
            //System.out.println( "Destroying by leave" );
            signal( "destroy" ).fire( arg( "attributes.leaving", true ) );
            set( "parent", null );
        }
    }

    protected void handleDestroy( Handler<DestroyEvent> handler ) {
        if( handler.event().getForeign() ) {
            handler.decline();

        } else {

            Boolean mandatory = handler.event().getMandatory();
            boolean continued = true;

            for( Destroyable sub: query( Destroyable.class ) ) {
                if( sub.signal( "destroy" ).fire( new DestroyEvent().set( "mandatory", mandatory ) ).isStopped() ) {
                    if( ! mandatory ) {
                        continued = false;
                        break;
                    }
                }
            }

            if( continued && ! this.destroyed ) {

                if( ! mandatory && this.getParent() != null ) {

                    //Vetoable
                    if( handler.handle().isContinued() && root() != this ) {
                        performDestruction();
                        set( "parent", null );
                    }
                } else {

                    // Non vetoable
                    try {
                        handler.handle();
                    } catch( Exception ex ) {
                        if( ! mandatory ) {
                            throw Errors.wrap( ex );
                        }
                    }
                    performDestruction();
                }
            } else {

                handler.decline();
            }

        }
    }

    protected void performDestruction() {
        raw( "destroyed", true );
        raw( "opened", false );
        if( this.tasks != null ) {
            this.tasks.shutdown();
            this.tasks = null;
        }
        if( this.queue != null ) {
            synchronized( this.queue ) {
                this.queue.notifyAll();
            }
            this.queue = null;
        }
    }
    
    protected void handleClose( Handler handler ) {
        if( this.opened ) {
            handler.handle();

            if( handler.handle().isContinued() ) {
                raw( "opened", false );
                if( getParent() != null ) {
                    this.swap( null );
                } else {
                    signal( "destroy" ).fire();
                }
            }
        } else {
            handler.decline();
        }
    }

    @Override
    public TaskList getTaskList() {
        if( this.tasks == null ) {
            synchronized( this ) {
                if( this.tasks == null ) {
                    this.tasks = new TaskList();
                }
            }
        }
        return this.tasks;
    }

    @Override
    public void removeWidget( Widget widget ) {
        if( bubbledChildrenRegistry.containsValue( widget ) ) {
            if( widget.getParent() instanceof Container ) {
                ( (Container) widget.getParent() ).remove( widget );
            } else {
                throw new WidgetException( this.getCommonName() + ": Cannot remove " + widget.getCommonName() + ", remove from parent" );
            }
        } else {
            super.removeWidget( widget );
        }
    }
    
    public <T> T get( String name ) {
        if( bubbledChildrenRegistry.containsKey( name ) ) {
            return (T)bubbledChildrenRegistry.get( name );
        } else {
            return super.get( name );
        }
    }
 
    public boolean contains( String name ) {
        return bubbledChildrenRegistry.containsKey( name ) || super.contains( name );
    }

    public boolean contains( Widget name ) {
        return bubbledChildrenRegistry.containsValue( name ) || super.contains( name );
    }

    public class FormChildrenIterable implements Iterable<Widget> {
        public Iterator<Widget> iterator() {
            return new FormChildrenIterator();
        }
    }

    public class FormChildrenIterator implements Iterator<Widget> {
        private Iterator<Widget> children;
        private Iterator<Widget> bubbles;
        
        public FormChildrenIterator(){
            this.children = Form.this.getChildren().iterator();
            this.bubbles = Form.this.bubbledChildrenRegistry.values().iterator();
        }
        
        public boolean hasNext(){
            return children.hasNext() || bubbles.hasNext();
        }
        
        public Widget next() {
            if( children.hasNext() ) {
                return children.next();
            } else {
                return bubbles.next();
            }
            
        }
        
        public void remove() {
            throw new RuntimeException( "Form iterator is not mutable" );
        }
    }
    
    public WidgetSelection<Widget> all() {
        return new WidgetArrayList<Widget>( getAll() );
    }

    public <T> WidgetSelection<T> all( Class<T> type ) {
        return new WidgetArrayList<T>( query( getAll() ).instanceOf( type ) );
    }
    
    @Virtual
    public Iterable<Widget> getAll() {
        return new FormChildrenIterable();
    }
    
    public int size() {
        return bubbledChildrenRegistry.size() + super.size();
    }

    protected WidgetLifeCycle getBubbleRecipient() {
        if( this.parent != null ) {
            return (WidgetLifeCycle) this.parent;
        } else if( this.destroyed && this._lastParent != null ){
            return (WidgetLifeCycle) this._lastParent;
        } else {
            return null;
        }
    }
    
    protected void bubbleOut( Event event ) {
        
        if( ! ( 
            event instanceof WidgetAddedEvent || 
            event instanceof WidgetRemovedEvent 
        ) ) {
            super.bubbleOut( event );
        }
    }
    
    public void bubbleIn( Event event ) {
        //System.out.println( getCommonName() + " form bubbleIn " + event  );
        if( event instanceof WidgetAddedEvent ) {
            handleWidgetAddedEvent( (WidgetAddedEvent) event );
 
        } else if( event instanceof WidgetRemovedEvent ) {
            handleWidgetRemovedEvent( (WidgetRemovedEvent) event );
 
        } else if( event instanceof EnqueueEvent ) {
            handleEnqueueEvent( (EnqueueEvent) event );

        } else if( event instanceof FocusRequestEvent ) {
            handleFocusRequestEvent( (FocusRequestEvent) event );
 
        } else if( event instanceof FocusReleaseEvent ) {
            handleFocusReleaseEvent( (FocusReleaseEvent) event );
 
        } else if( event instanceof FocusStatusEvent ) {
            handleFocusStatusEvent( (FocusStatusEvent) event );
 
        } else {
            super.bubbleIn( event );
        }
    }
    
    public void spreadIn( Event event ) {
        if( event instanceof ContainerActivationEvent ) {
            this.activate();
        } else if( event instanceof ContainerPassivationEvent ) {
            this.passivate();
        } else {
            super.spreadIn( event );
        }
    }

    protected void spreadOut( Event event ) {
        super.spreadOut( event );
        if( destination != null ) {
            //System.out.println( "Spreading " + event.getClass().getSimpleName() +  " from " + this.getCommonName() + " to " + destination.getCommonName() );
            destination.spreadIn( event );
        }
    }
    
    @Override
    protected Map<String, SignalHook> createSignalHooks() {
        getNotifyRouter().add( FormJoinEvent.class, slot( "refreshHooks" ).unsigned() );
        getNotifyRouter().add( FormLeaveEvent.class, slot( "refreshHooks" ).unsigned() );
        return super.createSignalHooks();
    }

    protected void handleWidgetAddedEvent( WidgetAddedEvent event ) {
        Widget widget = event.getWidget();
        // System.out.println( this.getCommonName() + " Adding bubbled " + widget.getCommonName() );

        if( widget.getName() != null ) {
            if( _childrenRegistry.containsKey( widget.getName() ) ) {
                throw new WidgetException( "Cannot add " + widget.getCommonName() + ": child name in form" );
            } else if( bubbledChildrenRegistry.containsKey( widget.getName() ) ) {
                //TODO: Se il nome e' generato, rigenerarlo fino ad averne uno unico
                throw new WidgetException( "Cannot add " + widget.getCommonName() + ": duplicate bubbled child name" );
            } else if( isPlainProperty( widget.getName() ) ) {
                throw new WidgetException( "Cannot add " + widget.getCommonName() + ": name conflict with " + model().descriptor().property( widget.getName() ).getFullName() + " property");
            } else {
                bubbledChildrenRegistry.put( widget.getName(), widget );
                storeReferences( widget );
                
                // Se la form e' costruita o in costruzione, propaga la costruzione ai container
                if( this.built ) {
                    //System.out.println( this + " propagate build to bubbled " + widget );
                    ((WidgetLifeCycle) widget).spreadIn( new BuildContentEvent( this ) );
                }
                
                FormJoinEvent evt = new FormJoinEvent( this, widget );

                // Se la form e' attiva, notifica a tutti la comparsa di un nuovo widget
                if( this.active ) {
                    signal( "join" ).fire( spread( notify( evt ) ) );
                    
                // Altrimenti lo notifica solo al widget in esame
                } else {
                    widget.notify( evt );
                }
                //TODO: propagazioni
            }
            
        }
        
    }
    
    protected void handleWidgetRemovedEvent( WidgetRemovedEvent event ) {
        Widget widget = event.getWidget();
        if( bubbledChildrenRegistry.containsKey( widget.getName() ) ) {
            
            //System.out.println( this.getCommonName() + " Removing bubbled " + widget.getCommonName() );

            FormLeaveEvent evt = new FormLeaveEvent( this, widget );

            // Notifica la rimozione al widget in esame
            // Se attiva, l'evento sara' notificato con lo spread
            widget.notify( evt );
            
            cleanReferences( widget );
            bubbledChildrenRegistry.remove( widget.getName() );

            // Se la form e' attiva, notifica a tutti la scomparsa di un widget esistente
            if( this.active ) {
                signal( "leave" ).fire( spread( notify( evt ) ) );
            }

        }

    }
    
    protected void addChildrenItem( Adder<Widget> adder ) {
        // System.out.println( getCommonName() + " adding " + adder.item().getCommonName() + ", " + this.persistent );
        nameWidget( adder.item() );

        // Se la form e' persistente, registra il widget per la creazione da parte del produttore
        if( this.persistent ) {
            String name = adder.item().getName();
            if( get( "production.creations" ) == null ) {
                synchronized( this ) {
                    if( get( "production.creations" ) == null ) {
                        set( "production.creations", new HashSet<String>() );
                    }
                }
            }
            this.<Set>get( "production.creations" ).add( name );
        }

        super.addChildrenItem( adder );
        
        // Effettivamente aggiunto
        if( adder.item().get( "parent" ) == this ) {
            //System.out.println( getCommonName() + " added " + adder.item().getCommonName() + ", " + this.persistent );
            storeReferences( adder.item() );

            // Se la form e' attiva, notifica a tutti l'arrivo di un nuovo widget
            if( this.active ) {
                signal( "join" ).fire( spread( notify( new FormJoinEvent( this, adder.item() ) ) ) );
            }
        } else {
            //System.out.println( getCommonName() + " discarded " + adder.item().getCommonName() + ", " + this.persistent );

        }       
 
    }

    protected void removeChildrenItem( Remover<Widget> remover ) {
        
        // Se la form e' attiva, notifica a tutti l'abbandono di un widget
        if( this.active ) {
            signal( "leave" ).fire( spread( notify( new FormLeaveEvent( this, remover.item() ) ) ) );
        }

        super.removeChildrenItem( remover );

        // Effettivamente cancellato
        //System.out.println( "Removable parent " + remover.item().get( "parent" ) );
        if( remover.item().get( "parent" ) != this ) {
            //System.out.println( getCommonName() + " removed " + remover.item().getCommonName() + ", " + this.persistent );

            cleanReferences( remover.item() );

            // Se la form e' persistente, registra il widget per la rimozione da parte del produttore
            if( this.persistent ) {
                String name = remover.item().getName();
                Set creations = get( "production.creations" );
                if( creations != null && creations.contains( name ) ) {
                    creations.remove( name );
                } else {
                    if( get( "production.destructions" ) == null ) {
                        synchronized( this ) {
                            if( get( "production.destructions" ) == null ) {
                                set( "production.destructions", new HashSet<String>() );
                            }
                        }
                    }
                    this.<Set>get( "production.destructions" ).add( name );
                }
            }
        } else {
            //System.out.println( getCommonName() + " preserved " + remover.item().getCommonName() + ", " + this.persistent );
        }

    }
    

    @Override
    public <T> ObjectQuery<T> query( Class<T> cls ) {
        return (ObjectQuery<T>)(new ObjectQuery<Widget>( all() ).byItem().instanceOf( cls ) );
    }

    @Override
    public <T> ObjectQuery<T> query() {
        return (ObjectQuery<T>)(new ObjectQuery<Widget>( all() ));
    }
    
    public void load( State state ) {
        if( state.has( "state" ) ) {
            State formState = state.sub( "state" );
            for( String name: formState.names() ) {
                ((WidgetLifeCycle) this.widget( name )).load( formState.sub( name ) );
            }
        }
    }
    
    protected void watch( Watcher watch ) {
        set( "production.creations", null );
        set( "production.destructions", null );
        for( Widget child: all() ) {
            child.model().watch();
        }
        if( destination != null ) {
            //System.out.println( "Forwarding watch from " + this.getCommonName() + " to " + destination.getCommonName() );
            destination.model().watch();
        }
    }
    
    protected void ignore( Watcher ignore ) {
        set( "production.creations", null );
        set( "production.destructions", null );
        if( destination != null ) {
            //System.out.println( "Forwarding ignore from " + this.getCommonName() + " to " + destination.getCommonName() );
            destination.model().ignore();
        }
        for( Widget child: all() ) {
            child.model().ignore();
        }
    }
    
    public <T extends Form> T form() {
        return getForm();
    }
    
    @Virtual
    public <T extends Form> T getForm() {
        return (T)ancestor( Form.class );
    }
    
    @Override
    public <T extends Collector> T root() {
        T root = (T)super.root();
        return root != null ? root : (T)this;
    }

    public Widget swap( Widget destination ) {
        if( destination instanceof Form ) {
            if( root() == this ) {
                if( signal( "orphan" ).fire().isContinued() ) {
                    if( signal( "destroy" ).fire().isContinued() ) {
                        set( "destination", destination );
                    }
                }
                return this;
            } else {
                return super.swap( destination );
            }

        } else if( destination == null ) {
            if( root() != this ) {
                return super.swap( destination );
            } else {
                throw new WidgetException( "Root Form cannot be swapped with null" );
            }
            
        } else {
            throw new WidgetException( "Form must be swapped with another Form" );
        }
    }
    
    protected void setDestination( Setter<Widget> setter ) {
        setter.set();
        if( setter.value() != null ){
            if( model().watching() ) {
                //System.out.println( "Recovering watch from " + this.getCommonName() + " to " + setter.value().getCommonName() );
                setter.value().model().watch();
            }  
            if( this.active ) {
                //System.out.println( "Recovering activation from " + this.getCommonName() + " to " + setter.value().getCommonName() );
                ( (Form) setter.value() ).activate();
            }
        } 
    }
    
    public void focus( String name ) {
        focus( (Widget)get( name ) );
    }
    
    public void focus( Widget widget ) {
        if( root() == this ) {
            setActiveChild( widget );
        } else {
            bubble( new FocusRequestEvent( widget ) );
        }
    }
    
    public void blur() {
        focus( (Widget) null );
    }
    
    public void reset() {
        signal( "reset" ).fire();
    }
    
    protected void handleReset( Handler handler ) {
        query( FormWidget.class ).call( "reset" );
        handler.handle();
    }
    
    protected void handleFocusRequestEvent( FocusRequestEvent event ) {
        if( root() == this ) {
            focus( (Widget) event.getSource() );
        } else {
            super.bubbleIn( event );
        }
    }

    protected void handleFocusReleaseEvent( FocusReleaseEvent event ) {
        if( root() == this && event.getSource() == getActiveChild() ) {
            blur();
        } else {
            super.bubbleIn( event );
        }
    }

    protected void handleFocusStatusEvent( FocusStatusEvent event ) {
        if( root() == this ) {
            // Reply
            event.setFocused( event.getSource() == getActiveChild() );
        } else {
            super.bubbleIn( event );
        }
    }
    
    protected void handleContainerPassivationEvent( ContainerPassivationEvent event ) {
        //System.out.println( "Handling passivation for " + this.getCommonName() );
        super.handleContainerPassivationEvent( event );
        if( destination != null ) {
            destination.passivate();
            destination = null;
        }
    }
    
    protected void handleWidgetPersistedEvent( WidgetPersistedEvent event ) {
        // In presenza di destination, l'evento di persistenza va girato alla
        // form destinataria
        if( destination != null ) {
            //System.out.println( "Forwarding persistence from " + this.getCommonName() + " to " + destination.getCommonName() );
            destination.notify( event );
    
            // Si sintetizza un evento di abbandono
            this.notify( new WidgetAbandonedEvent( event.getSource(), this ) );
        
        } else {
            //System.out.println( "Handling persistence for " + this.getCommonName() );
            super.handleWidgetPersistedEvent( event );
        }
    }

    private class ReferenceDescriptor {
        private String expression;
        private PropertyDescriptor property;
    
        public ReferenceDescriptor( String expression, PropertyDescriptor property ) {
            this.expression = expression;
            this.property = property;
        }
    
    }
    
    protected void processReferences() {
        this.references = new HashMap<String, ReferenceDescriptor>();
        for( PropertyDescriptor property: this.model().descriptor().properties() ) {
            if( property.metadata().contains( ReferenceMetadata.class ) ) {
                ReferenceMetadata reference = property.metadata( ReferenceMetadata.class );
                this.references.put( property.getName(), new ReferenceDescriptor( reference.getExpression(), property ) );
                // System.out.println( this.getClass().getName() + " REF " + property.getName() );
            }
        }
    }
    
    protected void storeReferences( Widget widget ) {
        String name = widget.getName();
        for( ReferenceDescriptor reference: this.references.values() ) {
            // System.out.println( this.getClass().getName() + " Analyzing " + reference.expression );
            if( name.equals( reference.expression ) ) {
                // System.out.println( this.getClass().getName() + " Referencing " + reference.property.getFullName() );
                reference.property.setFrontValue( model(), widget );
            }
        }
    }

    protected void cleanReferences( Widget widget ) {
        String name = widget.getName();
        for( ReferenceDescriptor reference: this.references.values() ) {
            if( name.equals( reference.expression ) ) {
                // System.out.println( this.getClass().getName() + " Cleaning " + reference.property.getFullName() );
                reference.property.setFrontValue( model(), null );
            }
        }
    }

    protected boolean isPlainProperty( String name ) {
        return ! this.references.containsKey( name ) && this.modelInstance.has( name ); 
    }

    public void next( Dispatcher dispatcher ) {
        //TODO: valutare bubble
        if( root() == this ) {
            scheduleNextTick( dispatcher );
        } else if( root() instanceof Form ){
            ((Form) root()).next( dispatcher );
        } else {
            throw new WidgetException( "Cannot schedule next tick, missing root form" );
        }
    }
    
    protected void scheduleNextTick( Dispatcher dispatcher ) {
        if( nextTicking ) {
            if( nextTickQueue == null ) {
                nextTickQueue = new ArrayList<Dispatcher>();
            }
            nextTickQueue.add( dispatcher );
        } else {
            event( "nextTick" ).add( dispatcher );
        } 
    }
    
    protected void handleNextTick( Handler handler ) {
        // System.out.println( "Begin tick... "  + handler.signal().size() );
        nextTickQueue = null;
        nextTicking = true;
        try {
            handler.handle();

        } finally {
            nextTicking = false;
            
            // In ogni caso, l'insieme di dispatcher ha una sola occasione di essere eseguito
            handler.signal().clear();
            
            // System.out.println( "End tick... "  + handler.signal().size() );
        }
    
        if( nextTickQueue != null ) {
            // System.out.println( "Recovering ticks... "  + handler.signal().size() );
            for( Dispatcher dispatcher: nextTickQueue ) {
                scheduleNextTick( dispatcher );
            }
            // System.out.println( "End Recovering ticks... "  + handler.signal().size() );
            nextTickQueue = null;
        }
        
    }
    
    protected LayoutInfo getLayout( Getter<LayoutInfo> getter ) {
        if( ! getter.exists() ) {
            getter.setRaw( new LayoutInfo( new Arguments()
                .arg( "title", title )
                .arg( "closable", closable ) 
            ));
        }
        return getter.get();
    }

    protected void setTitle( Setter<String> setter ) {
        setter.set();
        set( "!layout.title", setter.value() );
    }

    protected void setClosable( Setter<Boolean> setter ) {
        setter.set();
        set( "!layout.closable", setter.value() );
    }
    
    public void setLayoutProperty( String name, Object value ) {
        if( name.equals( "title" ) ) {
            model().direct( "title", value );
        } else if( name.equals( "closable" ) ) {
            model().direct( "closable", value );
        }
    }

    public boolean isStructural() {
        return true;
    }
    
    /* Queue management */
    
    protected Queue<EnqueueEvent> getQueue() {
        if( this.queue == null ) {
            synchronized( this ) {
                if( this.queue == null ) {
                    this.queue = new ArrayDeque<EnqueueEvent>();
                }
            }
        }
        return this.queue;
    }

    @Override
    public void enqueue( Dispatcher dispatcher, Event event ) {
        
        if( root() == this ) {
            notifyQueue( new EnqueueEvent( dispatcher, event ) );
            
        } else {
            super.enqueue( dispatcher, event );
        }
    
    }

    protected void handleEnqueueEvent( EnqueueEvent event ) {
        if( root() == this ) {
            notifyQueue( event );
        } else {
            bubble( event );
        }
    }

    protected void notifyQueue( EnqueueEvent event ) {
        if( event != null ) {
            Queue<EnqueueEvent> queue = getQueue();
            
            synchronized( queue ) {
                queue.add( event );
                queue.notifyAll();
            }
        }
    }

    public boolean hasQueue() {
        if( this.queue == null ) {
            return false;
        } else {
            boolean result = false;
            synchronized( this.queue ) {
                result = this.queue.size() > 0;
            }
            return result;
        }
    }
    
    protected void handleDequeue( Handler<Event> handler ) {
        if( this.queue != null ) {
            
            handler.handle();

            EnqueueEvent evt;
            
            do {
                synchronized( this.queue ) {
                    evt = this.queue.poll();
                }
                
                if( evt != null ) {
                    evt.getDispatcher().dispatch( evt.getEvent() != null ? evt.getEvent() : new Event( this ) );
                }
                
            } while( evt != null );
            
            
        } else {
            handler.decline();
        }
    }
    
    protected void bubbleQueue() {
        if( this.queue != null ) {

            EnqueueEvent evt;
            
            synchronized( this.queue ) {
                while( ( evt = this.queue.poll() ) != null ) {
                    bubble( evt );
                }
            }

            synchronized( this ) {
                this.queue = null;
            }
        }
    }
    
    public Object getQueueMonitor() {
        return this.getQueue();
    }
    
    @Virtual
    public boolean getClosed(){
        return ! this.<Boolean>get( "opened" );
    }
    
    public void setClosed( boolean closed ){
        this.set( "opened", ! closed );
    }

    protected void setOpened( Setter<Boolean> setter ) {
        if( setter.value() && ! this.opened ) {
            throw new IllegalStateException( Strings.message( "{class.name} cannot be reopened", this ) );
        } else if( ! setter.value() && this.opened ) {
            signal( "close" ).fire();
        } else {
            setter.decline();
        }
    }

    protected boolean canValidate() {
        return visible && ! destroyed;
    }
    
    protected void handleValidate( Handler<ValidationState> handler ) {
        final ValidationState event = handler.event();

        if( canValidate() ) {
            try {

                if( handler.handle().isContinued() ) {
                
                    query( ValidationCapable.class ).inspect( w -> {
                        w.getValidate().fire( event ); 
                    });
            
                    if( event.isValid() && this.validation != null ) {
                        for( Validator validator: this.validation ) {
                            try {
                                validator.validate( this );
                            } catch( ValidationException ex ) {
                                throw ex.join( this );
                            }
                        }
                    }
                }

            } catch( ValidationException ex ) {
                event.add( ex );
            }
        } else {
            handler.decline();
        }
    }
    
    /* Bean Accessors */    

    public List<Validator> getValidation(){
        return this.validation;
    }
    
    public void setValidation( List<Validator> validation ){
        this.validation = validation;
    }

    public Signal<ValidationState> getValidate(){
        return this.validate;
    }
    
    public void setValidate( Signal<ValidationState> validate ){
        this.validate = validate;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public boolean getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( boolean scrollable ){
        this.scrollable = scrollable;
    }
    
    public Object getHeight(){
        return this.height;
    }
    
    public void setHeight( Object height ){
        this.height = height;
    }
    
    public Object getWidth(){
        return this.width;
    }
    
    public void setWidth( Object width ){
        this.width = width;
    }
    
    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public Box getMargin(){
        return this.margin;
    }
    
    public void setMargin( Box margin ){
        this.margin = margin;
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public boolean getClosable(){
        return this.closable;
    }
    
    public void setClosable( boolean closable ){
        this.closable = closable;
    }
    
    public boolean isClosable() {
        return this.closable;
    }
    
    public Signal<FormLeaveEvent> getLeave(){
        return this.leave;
    }
    
    public void setLeave( Signal<FormLeaveEvent> leave ){
        this.leave = leave;
    }
    
    public Signal<FormJoinEvent> getJoin(){
        return this.join;
    }
    
    public void setJoin( Signal<FormJoinEvent> join ){
        this.join = join;
    }
    
    public Signal<FormPassivationEvent> getPassivate(){
        return this.passivate;
    }
    
    public void setPassivate( Signal<FormPassivationEvent> passivate ){
        this.passivate = passivate;
    }
    
    public Signal<FormActivationEvent> getActivate(){
        return this.activate;
    }
    
    public void setActivate( Signal<FormActivationEvent> activate ){
        this.activate = activate;
    }
    
    public Signal getClose(){
        return this.close;
    }
    
    public void setClose( Signal close ){
        this.close = close;
    }
    
    public Signal getDestroy(){
        return this.destroy;
    }
    
    public void setDestroy( Signal destroy ){
        this.destroy = destroy;
    }
    
    public Widget getActiveChild(){
        return this.activeChild;
    }
    
    public void setActiveChild( Widget activeChild ){
        this.activeChild = activeChild;
    }
    
    public Signal<Event> getNextTick(){
        return this.nextTick;
    }
    
    public void setNextTick( Signal<Event> nextTick ){
        this.nextTick = nextTick;
    }
    
    public Signal<FormPassivationEvent> getPassivating(){
        return this.passivating;
    }
    
    public void setPassivating( Signal<FormPassivationEvent> passivating ){
        this.passivating = passivating;
    }
    
    public Signal<FormActivationEvent> getActivating(){
        return this.activating;
    }
    
    public void setActivating( Signal<FormActivationEvent> activating ){
        this.activating = activating;
    }
    
    public Signal<Event> getOrphan(){
        return this.orphan;
    }
    
    public void setOrphan( Signal<Event> orphan ){
        this.orphan = orphan;
    }
    
    public Signal<Event> getAdopt(){
        return this.adopt;
    }
    
    public void setAdopt( Signal<Event> adopt ){
        this.adopt = adopt;
    }
    
    public Signal<RotationEvent> getRotate(){
        return this.rotate;
    }
    
    public void setRotate( Signal<RotationEvent> rotate ){
        this.rotate = rotate;
    }
    
    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
    public Boolean getDestroyed() {
        return this.destroyed;
    }
}