package it.neverworks.ui.form;

import it.neverworks.model.converters.IntConverter;

public class ProgressConverter extends IntConverter {

    public Object convert( Object value ) {
        if( value instanceof String && ((String) value).equalsIgnoreCase( "indeterminate" ) ) {
            return null;
        } else {
            return super.convert( value );
        }
    }

}