package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.ContainerActivationEvent;

public class FormActivationEvent extends ContainerActivationEvent {

    public FormActivationEvent( Object source ) {
        super( source );
    }
    
    public FormActivationEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}