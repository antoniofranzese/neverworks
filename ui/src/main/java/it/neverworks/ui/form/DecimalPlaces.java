package it.neverworks.ui.form;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;

@Convert( DecimalPlacesConverter.class )
public class DecimalPlaces extends BaseModel {
    
    @Property @AutoConvert
    private Long min;
    
    @Property @AutoConvert
    private Long max;
    
    public Long getMax(){
        return this.max;
    }
    
    public void setMax( Long max ){
        this.max = max;
    }
    
    public Long getMin(){
        return this.min;
    }
    
    public void setMin( Long min ){
        this.min = min;
    }
}