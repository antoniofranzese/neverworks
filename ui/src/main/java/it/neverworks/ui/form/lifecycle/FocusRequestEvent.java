package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.ContainerPassivationEvent;

public class FocusRequestEvent extends ContainerPassivationEvent {

    public FocusRequestEvent( Object source ) {
        super( source );
    }
    
    public FocusRequestEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}