package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.ContainerPassivationEvent;

public class FocusReleaseEvent extends ContainerPassivationEvent {

    public FocusReleaseEvent( Object source ) {
        super( source );
    }
    
    public FocusReleaseEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}