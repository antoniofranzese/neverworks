package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.CollectorLeaveEvent;
import it.neverworks.ui.Widget;

public class FormLeaveEvent extends CollectorLeaveEvent {

    public FormLeaveEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public FormLeaveEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}