package it.neverworks.ui.form;

import java.util.Map;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.ui.data.StyleArtifact;

public class ProblemConverter implements Converter {
    
    private final static TypeDefinition<StyleArtifact> StyleArtifactType = type( StyleArtifact.class );
    
    public Object convert( Object value ) {
        if( value instanceof Exception ) {
            return value;
        } else if( value instanceof String ) {
            if( Strings.hasText( value ) ) {
                return new Exception( (String) value );
            } else {
                return null;
            }
        } else if( value instanceof Map ) {
            Map map = (Map) value;
            if( map.containsKey( "message" ) ) {
                if( map.containsKey( "flavor" ) ) {
                    return new FlavoredProblem( 
                        Strings.valueOf( map.get( "message" ) )
                        ,StyleArtifactType.process( map.get( "flavor" ) )
                    );
                } else {
                    return new Exception( Strings.valueOf( map.get( "message" ) ) );
                }
                
            } else {
                return value;
            }
        } else {
            return value;
        }
    }
}