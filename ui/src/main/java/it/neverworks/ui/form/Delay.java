package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Dispatcher;

public class Delay extends Timer {
    
    public Delay(){
        super();
        set( "running", true );
        set( "repeat", 1 );
        set( "autoDestroy", true );
    }
    
    public Delay( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public Delay( Dispatcher dispatcher, int interval ){
        this();
        set( "interval", interval );
        set( "tick", dispatcher );
    }

    public Delay( Dispatcher dispatcher ){
        this( dispatcher, 0 );
    }
    
}