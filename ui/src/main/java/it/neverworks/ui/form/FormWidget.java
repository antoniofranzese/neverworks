package it.neverworks.ui.form;

import it.neverworks.ui.Widget;

public interface FormWidget<T> extends Widget, ValidationCapable {
    
    public T getValue();
    public void setValue( T value );
    
}