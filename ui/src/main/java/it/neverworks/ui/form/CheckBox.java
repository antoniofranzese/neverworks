package it.neverworks.ui.form;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.types.AutoType;
import it.neverworks.model.events.Signal;

public class CheckBox extends BaseFormWidget {
    
    @Property @AutoType( Boolean.class)
    protected Object value = Boolean.FALSE;
    
    @Property
    protected Signal<ChangeEvent> change;
    
    public CheckBox() {
        super();
    }
    
    public CheckBox( Arguments arguments ) {
        this();
        set( arguments );
    }

    protected void setValue( Setter setter ) {
        setter.set( setter.value() == null ? false : setter.value() );
    }

    @Virtual @Override
    public Object getDisplayValue(){
        return get( "value" ) != null ? ( value( "value" ).isTrue() ? T( "Yes" ) : T( "No" ) ) : null;
    }

    /* Bean Accessors */
    
    public Signal<ChangeEvent> getChange(){
        return this.change;
    }
    
    public void setChange( Signal<ChangeEvent> change ){
        this.change = change;
    }
    
}