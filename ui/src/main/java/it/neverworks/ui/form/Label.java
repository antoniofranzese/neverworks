package it.neverworks.ui.form;


import java.io.InputStream;
import java.io.Reader;
import java.util.regex.Pattern;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.StringConverter;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Process;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Access;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.features.Resolve;
import it.neverworks.model.events.Signal;
import it.neverworks.model.types.Type;
import it.neverworks.io.Streams;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.types.DecorationStencil;
import it.neverworks.ui.Widget;

@Convert( LabelConverter.class )
public class Label extends BaseFormWidget {

    public enum HorizontalAlignment {
        LEFT, CENTER, RIGHT, JUSTIFIED
    }

    public enum VerticalAlignment {
        TOP, MIDDLE, BOTTOM
    }
    
    public enum Position {
        BEFORE, AFTER
    }
    
    public enum Encoding {
        TEXT, HTML
    }

    @Property 
    protected Object value;
    
    @Property @AutoConvert
    private HorizontalAlignment halign;
    
    @Property @AutoConvert
    private VerticalAlignment valign;
    
    @Property
    private Size minWidth;
    
    @Property
    private Size minHeight;
    
    @Property @AutoConvert
    private Encoding encoding;
    
    @Property
    private Color color;

    @Property
    private Color background;
    
    @Property @AutoConvert
    private Position position;
    
    @Property
    private Signal<ClickEvent> click;
    
    @Property
    private Signal<ClickEvent> inspect;
    
    @Property
    protected Signal<LinkEvent> link;
    
    @Property @Resolve 
    private Widget target;
    
    @Property @AutoConvert
    private Boolean wrap; 
    
    @Property @Process( DecorationStencil.class )
    private Decoration decoration;
    
    @Property @Ignore @Access( setter = "setTextModifier" )
    protected Formatter format;
    
    @Property @Ignore @Access( setter = "setTextModifier" )
    protected boolean trim = false;
    
    @Property @Ignore @AutoConvert @Access( setter = "setTextModifier" )
    protected Pattern keep;

    @Property @Ignore @AutoConvert @Access( setter = "setTextModifier" )
    protected Pattern waste;

    @Property @AutoConvert
    protected boolean blinking;

    @Property
    protected Corners rounding;

    @Property
    protected Shadow shadow;
    
    protected Object startupValue;
    
    public Label() {
        super();
        on( "startup", slot( "onStartup" ).unsigned() );
    }
    
    public Label( Arguments arguments ) {
        this();
        set( arguments );
    }

    public Label( String value ) {
        this();
        set( "value", value );
    }

    protected void onStartup() {
        if( this.startupValue != null ) {
            this.value = processValue( this.startupValue );
            this.startupValue = null;
        }
    }
    
    protected Object getValue( Getter getter ) {
        if( ! this.started ) {
            signal( "startup" ).fire();
        }
        return Strings.valueOf( getter.get() );
    }
    
    protected void setValue( Setter setter ) {
        if( ! this.started ) {
            this.startupValue = setter.value();
            setter.decline();
        } else {
            setter.set( processValue( setter.value() ) );
        }
    }

    protected void setTextModifier( Setter setter ) {
        setter.set();
        if( this.started ) {
            set( "value", get( "value" ) );
        }
    }

    public String processValue( Object value ) {
        
        if( value != null ) {
            String processed = null;
            
            if( value instanceof InputStream ) {
                value = Streams.asString( (InputStream) value );
            } else if( value instanceof Reader ) {
                value = Streams.asString( (Reader) value );
            } 

            if( this.format != null ) {
                try {
                    processed = Strings.valueOf( this.format.format( value ) );
                } catch( Exception ex ) {
                    processed = Strings.valueOf( value );
                }
            } else {
                processed = Strings.valueOf( value );
            }
        
            if( processed != null ) {
                
                if( this.keep != null ) {
                    processed = Strings.keep( processed, this.keep );
                }
        
                if( this.waste != null ) {
                    processed = Strings.waste( processed, this.waste );
                }
        
                if( this.trim ) {
                    processed = processed.trim();
                }
                
                // if( this.length != null && this.length > 0 && processed.length() > length  ) {
                //     processed = processed.substring( 0, length );
                // }

                return processed.length() > 0 ? processed : null;

            } else {
                return null;
            }

        } else {
            return null;
        }
        
    }

    protected void resetValue() {
        // Nothing to do
    }

    /* Bean Accessors */
    
    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }
    
    public Corners getRounding(){
        return this.rounding;
    }
    
    public void setRounding( Corners rounding ){
        this.rounding = rounding;
    }

    public Decoration getDecoration(){
        return this.decoration;
    }
    
    public void setDecoration( Decoration decoration ){
        this.decoration = decoration;
    }

    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }

    public VerticalAlignment getValign(){
        return this.valign;
    }
    
    public void setValign( VerticalAlignment valign ){
        this.valign = valign;
    }

    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Position getPosition(){
        return this.position;
    }
    
    public void setPosition( Position position ){
        this.position = position;
    }

    public Encoding getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( Encoding encoding ){
        this.encoding = encoding;
    }
    
    public Signal<ClickEvent> getInspect(){
        return this.inspect;
    }
    
    public void setInspect( Signal<ClickEvent> inspect ){
        this.inspect = inspect;
    }
    
    public Signal<ClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<ClickEvent> click ){
        this.click = click;
    }
    
    public Size getMinHeight(){
        return this.minHeight;
    }
    
    public void setMinHeight( Size minHeight ){
        this.minHeight = minHeight;
    }
    
    public Size getMinWidth(){
        return this.minWidth;
    }
    
    public void setMinWidth( Size minWidth ){
        this.minWidth = minWidth;
    }
    
    public Widget getTarget(){
        return this.target;
    }
    
    public void setTarget( Widget target ){
        this.target = target;
    }
    
    public Boolean getWrap(){
        return this.wrap;
    }
    
    public void setWrap( Boolean wrap ){
        this.wrap = wrap;
    }
    
    public Pattern getWaste(){
        return this.waste;
    }
    
    public void setWaste( Pattern waste ){
        this.waste = waste;
    }
    
    public Pattern getKeep(){
        return this.keep;
    }
    
    public void setKeep( Pattern keep ){
        this.keep = keep;
    }
    
    public boolean getTrim(){
        return this.trim;
    }
    
    public void setTrim( boolean trim ){
        this.trim = trim;
    }
    
    public Formatter getFormat(){
        return this.format;
    }
    
    public void setFormat( Formatter format ){
        this.format = format;
    }
    
    public Signal<LinkEvent> getLink(){
        return this.link;
    }
    
    public void setLink( Signal<LinkEvent> link ){
        this.link = link;
    }

    public boolean getBlinking(){
        return this.blinking;
    }
    
    public void setBlinking( boolean blinking ){
        this.blinking = blinking;
    }

    public String toString() {
        return new ToStringBuilder( this )
            .add( "name" )
            .add( "value" )
            .toString();
    }
}