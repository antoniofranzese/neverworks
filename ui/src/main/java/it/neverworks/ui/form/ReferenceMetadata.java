package it.neverworks.ui.form;

import java.lang.reflect.Field;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;

public class ReferenceMetadata implements PropertyProcessor, PropertyMetadata {
    
    private String expression;
    
    public ReferenceMetadata( AnnotationInfo<Reference> info ) {
        Reference annotation = info.getAnnotation();
        this.expression = "".equals( annotation.value() ) ? ((Field) info.getTarget()).getName() : annotation.value();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( ReferenceMetadata.class ) ) {
            processProperty( property );
        }
    }
    
    public String getExpression(){
        return this.expression;
    }
    
}