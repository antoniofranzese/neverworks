package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.types.Type;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.ui.types.Scrollability;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Zoom;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.FileArtifactConverter;
import it.neverworks.ui.dnd.DropSource;
import it.neverworks.ui.dnd.Avatar;
import it.neverworks.ui.lifecycle.UIEvent;

public class Image extends BaseFormWidget<Artifact> implements DropSource {

    @Property @Type( Artifact.class ) 
    @Convert( ArtifactConverter.class )
    protected Object value;
    
    @Property
    protected Signal<ClickEvent> click;
    
    @Property
    protected Signal<ClickEvent> inspect;
    
    @Property
    private Object proposal;
    
    @Property
    private Avatar avatar;
    
    @Property
    private Size maxWidth;
    
    @Property
    private Size maxHeight;
    
    @Property
    private Zoom zoom;
    
    @Property
    private Scrollability scrollable;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Background background;

    @Property
    protected Corners rounding;
 
    @Property
    protected Shadow shadow;
   
    public Image() {
        super();
    }
    
    public Image( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected void handleClick( Handler<UIEvent> handler ) {
        if( ( getEnabled() && getVisible() ) || !handler.event().getForeign() ) {
            handler.handle();
        } else {
            model().touch( "disabled" );
            model().touch( "visible" );
            handler.decline();
        }
    }
    
    protected void setZoom( Setter<Zoom> setter ) {
        if( setter.value() != null ) {
            this.maxWidth = null;
            this.maxHeight = null;
            
            if( setter.value().getValue() < 0 ) {
                setter.set( Zoom.value( 0 ) );
            } else {
                setter.set();
            }
        
        } else {
            setter.set();
        }
    }
    
    /* Virtual Properties */

    @Virtual
    public Artifact getSource(){
        return (Artifact)getValue();
    }
    
    @Convert( ArtifactConverter.class )
    public void setSource( Artifact source ){
        setValue( source );
    }

    /* Bean Accessors */
    
    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }

    public Corners getRounding(){
        return this.rounding;
    }
    
    public void setRounding( Corners rounding ){
        this.rounding = rounding;
    }
    
    
    public Size getMaxHeight(){
        return this.maxHeight;
    }
    
    public void setMaxHeight( Size maxHeight ){
        this.maxHeight = maxHeight;
    }
    
    public Size getMaxWidth(){
        return this.maxWidth;
    }
    
    public void setMaxWidth( Size maxWidth ){
        this.maxWidth = maxWidth;
    }
    public Signal<ClickEvent> getClick(){
        return this.click;
    }
    
    public void setClick( Signal<ClickEvent> click ){
        this.click = click;
    }
    
    public Signal<ClickEvent> getInspect(){
        return this.inspect;
    }
    
    public void setInspect( Signal<ClickEvent> inspect ){
        this.inspect = inspect;
    }
    
    public Object getProposal(){
        return this.proposal;
    }
    
    public void setProposal( Object proposal ){
        this.proposal = proposal;
    }
    
    public Avatar getAvatar(){
        return this.avatar;
    }
    
    public void setAvatar( Avatar avatar ){
        this.avatar = avatar;
    }
    
    public Scrollability getScrollable(){
        return this.scrollable;
    }
    
    public void setScrollable( Scrollability scrollable ){
        this.scrollable = scrollable;
    }
    
    public Zoom getZoom(){
        return this.zoom;
    }
    
    public void setZoom( Zoom zoom ){
        this.zoom = zoom;
    }
    
    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
    public static class ArtifactConverter extends FileArtifactConverter {
    
        public Object convert( Object value ) {
            if( value instanceof Artifact ) {
                return value;
            } else {
                return super.convert( value );
            }
        }

    }
}