package it.neverworks.ui.form;

import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import it.neverworks.lang.Dates;
import it.neverworks.model.converters.Convert;

public class StaticTimeValue implements TimeValue {
    
    public final static StaticTimeValue BEGINNING = new StaticTimeValue( 0, 0, 0, 0 );
    public final static StaticTimeValue ENDING = new StaticTimeValue( 23, 59, 59, 999 );
    
    private DateTime dateTime;

    public StaticTimeValue( DateTime dateTime ) {
        this.dateTime = dateTime;
    }
    
    public StaticTimeValue( Date date ) {
        this( Dates.datetime( date ) );
    }
    
    public StaticTimeValue( int hours, int minutes, int seconds, int milliseconds ) {
        this( new DateTime()
            .withHourOfDay( hours )
            .withMinuteOfHour( minutes )
            .withSecondOfMinute( seconds )
            .withMillisOfSecond( milliseconds )
        );
    }

    public DateTime getDateTimeValue() {
        return this.dateTime;
    }
    
    public String toString() {
        return DateTimeFormat.forPattern( "HH:mm:ss.SSS" ).print( this.dateTime );
    }
}