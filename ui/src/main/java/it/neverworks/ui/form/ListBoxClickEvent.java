package it.neverworks.ui.form;

import it.neverworks.ui.data.Inventory;

public class ListBoxClickEvent extends ClickEvent<ListBox> {
    
    protected Object id;
    protected Object item;
    
    public Object getItem(){
        if( this.item == null ) {
            this.item = this.id != null ? ((Inventory) getSource()).item( this.id ): null;
        }
        return this.item;
    }
    
    public Object getId(){
        return this.id;
    }
    
    public void setId( Object id ){
        this.id = id;
    }
    
    public Object getSelected() {
        return getItem();
    }
    
}