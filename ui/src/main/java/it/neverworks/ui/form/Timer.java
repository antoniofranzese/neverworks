package it.neverworks.ui.form;

import java.util.List;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.AutoConvert;

import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.BehavioralWidget;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.lifecycle.ErrorEvent;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Handler;

public class Timer extends BaseWidget implements BehavioralWidget {

    @Property @AutoConvert
    private int interval;
    
    @Property @AutoConvert
    private boolean running;
    
    @Property @AutoConvert
    private boolean concurrent = false; 
    
    @Property
    private Signal tick;
    
    @Property
    private int repeat = -1; 
    
    private boolean autoDestroy = false; 
    
    public Timer() {
        super();
    }
    
    public Timer( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    protected void handleTick( Handler handler ) throws Exception {
        try {
            handler.handle();
 
            if( repeat > 0 ) {
                repeat -= 1;
            }
 
            if( repeat == 0 ) {
                stop();
                
                if( autoDestroy ) {
                    destroy();
                }
            }
 
            this.model().touch( "running" );
        } catch( Exception ex ) {
            this.stop();
            throw ex;
        }
    }
    
    public void start( int repeat ) {
        set( "repeat", repeat );
        set( "running", true );
    }

    public void start() {
        set( "running", true );
        if( repeat == 0 ) {
            repeat = -1;
        }
    }
    
    public void stop() {
        set( "running", false );
    }
    
    public void destroy() {
        if( ancestor( Form.class ) != null ) {
            ancestor( Form.class ).remove( this );
        }
    }
        
    /* Bean Accessors */
    
    public boolean getConcurrent(){
        return this.concurrent;
    }
    
    public void setConcurrent( boolean concurrent ){
        this.concurrent = concurrent;
    }
    
    public Signal getTick(){
        return this.tick;
    }
    
    public void setTick( Signal tick ){
        this.tick = tick;
    }
    
    public boolean getRunning(){
        return this.running;
    }
    
    public void setRunning( boolean running ){
        this.running = running;
    }
    
    public int getInterval(){
        return this.interval;
    }
    
    public void setInterval( int interval ){
        this.interval = interval;
    }

    public int getRepeat(){
        return this.repeat;
    }
    
    public void setRepeat( int repeat ){
        this.repeat = repeat;
    }
    
    public boolean getAutoDestroy(){
        return this.autoDestroy;
    }
    
    public void setAutoDestroy( boolean autoDestroy ){
        this.autoDestroy = autoDestroy;
    }
    
}