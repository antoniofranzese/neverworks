package it.neverworks.ui.form;

import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.utils.ToStringBuilder;

public class SelectResetEvent<E> extends UIEvent<E> {
    
    private Object value;
    
    private Object item;
    
    public Object getItem(){
        return this.item;
    }
    
    public void setItem( Object item ){
        this.item = item;
    }
    
    public Object getValue(){
        return this.value;
    }
    
    public void setValue( Object value ){
        this.value = value;
    }

    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "value" ).toString();
    }
}