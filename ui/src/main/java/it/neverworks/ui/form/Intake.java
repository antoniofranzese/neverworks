package it.neverworks.ui.form;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.PropertyAnnotation;

@Target( { ElementType.FIELD } )
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = IntakeMetadata.class )
public @interface Intake {
    String[] value() default "";
    boolean nullable() default false;
}
