package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.WidgetException;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.model.io.State;
import it.neverworks.ui.lifecycle.UIEvent;

public class Button extends BaseFormWidget<String> {

    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }

    @AutoConvert
    public static enum Encoding {
        TEXT, HTML
    }

    @Property
    protected String caption;
    
    @Property
    protected Artifact icon;
    
    @Property
    protected Signal<ClickEvent> click;
    
    @Property
    private Signal<ClickEvent> inspect;
    
    @Property @AutoConvert
    private Boolean arrow;
    
    @Property
    private Encoding encoding;
    
    @Property
    private Color background;
    
    @Property
    private Color color;

    @Property
    private Corners rounding;
    
    @Property
    protected Shadow shadow;

    @Property
    private boolean wrap = false; 
    
    @Property
    private HorizontalAlignment halign;
    
    @Property @AutoConvert
    protected boolean blinking;
    
    public Button() {
        super();
    }
    
    public Button( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public void reset() {
        //TODO: verificare
    }
    
    public void load( State state ) {
        // Ignore
    }
    
    protected Label getLabel( Getter<Label> getter ) {
        return getter.value( null );
    }
    
    protected void setLabel( Setter<Label> setter ) {
        set( "caption", setter.value() != null ? setter.value().getValue() : null );
        setter.decline();
    }
    
    protected void setDisabled( Setter<Boolean> setter ) {
        setter.set();
        setReadonly( setter.value() );
    }
    
    protected void handleClick( Handler<UIEvent> handler ) {
        if( canHandle( handler ) ) {
            handler.handle();
        } else {
            handler.decline();
        }
    }

    protected void handleInspect( Handler<UIEvent> handler ) {
        if( canHandle( handler ) ) {
            handler.handle();
        } else {
            handler.decline();
        }
    }

    protected boolean canHandle( Handler<UIEvent> handler ) {
        if( ( 
                (!locked) 
                && getEnabled() 
                && getVisible() 
            ) 
            || !handler.event().getForeign() ) {
            return true;
        } else {
            model().touch( "visible" );
            model().touch( "disabled" );
            return false;
        }
    }
    
    public void click() {
        getClick().fire();
    }

    public void click( Object arguments ) {
        getClick().fire( arguments );
    }
    
    /* Bean Accessors */
    
    public Shadow getShadow(){
        return this.shadow;
    }
    
    public void setShadow( Shadow shadow ){
        this.shadow = shadow;
    }
    
    public Corners getRounding(){
        return this.rounding;
    }
    
    public void setRounding( Corners rounding ){
        this.rounding = rounding;
    }
    
    
    public Color getColor(){
        return this.color;
    }
    
    public void setColor( Color color ){
        this.color = color;
    }
    
    public Color getBackground(){
        return this.background;
    }
    
    public void setBackground( Color background ){
        this.background = background;
    }
    
    public Signal<ClickEvent> getClick(){
        return this.click;
    }
    
    public String getCaption(){
        return this.caption;
    }
    
    public void setCaption( String caption ){
        this.caption = caption;
    }
    
    public Artifact getIcon(){
        return this.icon;
    }
    
    public void setIcon( Artifact icon ){
        this.icon = icon;
    }
    
    public Signal<ClickEvent> getInspect(){
        return this.inspect;
    }
    
    public void setInspect( Signal<ClickEvent> inspect ){
        this.inspect = inspect;
    }

    public Boolean getArrow(){
        return this.arrow;
    }
    
    public void setArrow( Boolean arrow ){
        this.arrow = arrow;
    }
    
    public Encoding getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( Encoding encoding ){
        this.encoding = encoding;
    }
    
    public boolean getWrap(){
        return this.wrap;
    }
    
    public void setWrap( boolean wrap ){
        this.wrap = wrap;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }
    
    public boolean getBlinking(){
        return this.blinking;
    }
    
    public void setblinking( Boolean blinking ){
        this.blinking = blinking;
    }

}