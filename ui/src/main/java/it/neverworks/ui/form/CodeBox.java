package it.neverworks.ui.form;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.types.AutoType;

public class CodeBox extends BaseTextBox<String> {
   
    @Property @AutoType( String.class )
    protected Object value;

    @Property
    private String language;

    public CodeBox() {
        super();
    }
    
    public CodeBox( Arguments arguments ) {
        super();
        set( arguments );
    }    

    protected void setLanguage( Setter<String> setter ) {
        setter.set( Strings.hasText( setter.value() ) ? setter.value().toLowerCase() : null );
    }

    public String getLanguage(){
        return this.language;
    }
    
    public void setLanguage( String language ){
        this.language = language;
    }

}