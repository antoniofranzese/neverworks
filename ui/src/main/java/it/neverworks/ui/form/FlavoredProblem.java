package it.neverworks.ui.form;

import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.data.StyleArtifact;

public class FlavoredProblem extends Exception implements Flavored {
    
    public FlavoredProblem( String message, StyleArtifact flavor ) {
        super( message );
        this.flavor = flavor;
    }
    
    private StyleArtifact flavor;
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
}