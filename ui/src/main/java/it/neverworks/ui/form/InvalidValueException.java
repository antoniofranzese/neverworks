package it.neverworks.ui.form;

public class InvalidValueException extends RuntimeException {
    
    public InvalidValueException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public InvalidValueException( String message ){
        super( message );
    }
}