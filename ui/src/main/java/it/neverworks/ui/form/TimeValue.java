package it.neverworks.ui.form;

import java.util.Date;
import org.joda.time.DateTime;

import it.neverworks.lang.Dates;
import it.neverworks.model.converters.Convert;

@Convert( TimeValueConverter.class )
public interface TimeValue {
    
    default Date writeTo( Date date ) {
        if( date != null ) {
            return writeTo( Dates.DateTime( date ) ).toDate();
        } else {
            return null;
        }
    }
    
    default DateTime writeTo( DateTime date ) {
        DateTime self = getDateTimeValue();
        if( self != null && date != null ) {
            return date
                .withHourOfDay( self.hourOfDay().get() )
                .withMinuteOfHour( self.minuteOfHour().get() )
                .withSecondOfMinute( self.secondOfMinute().get() )
                .withMillisOfSecond( self.millisOfSecond().get() );
        } else {
            return date;
        }
    }

    public DateTime getDateTimeValue();
    
}