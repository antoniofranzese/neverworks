package it.neverworks.ui.form;

import java.util.Map;
import java.util.List;
import it.neverworks.lang.Arguments;
import it.neverworks.model.converters.Converter;

public class DecimalPlacesConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof DecimalPlaces ) {
            return value;
            
        } else if( value instanceof Map ) {
            return new DecimalPlaces().set( value instanceof Arguments ? (Arguments) value : new Arguments( (Map) value ) );
        
        } else if( value instanceof Number ) {
            return new DecimalPlaces().set( "max", value );
        
        } else if( value instanceof String ) {
            String[] numbers = ((String) value).split( "," );
        
            if( numbers.length == 1 ) {
                return new DecimalPlaces().set( "max", "max".equalsIgnoreCase( numbers[ 0 ].trim() ) ? Integer.MAX_VALUE : numbers[ 0 ] );
        
            } else if( numbers.length == 2 ) {
                return new DecimalPlaces().set( "min", numbers[ 0 ] ).set( "max", "max".equalsIgnoreCase( numbers[ 1 ].trim() ) ? Integer.MAX_VALUE : numbers[ 1 ] );
                
            } else {
                return value;
            }
        
        } else if( value instanceof List ) {
            List numbers = ((List) value);
    
            if( numbers.size() == 1 ) {
                return new DecimalPlaces().set( "max", numbers.get( 0 ) );
    
            } else if( numbers.size() == 2 ) {
                return new DecimalPlaces().set( "min", numbers.get( 0 ) ).set( "max", numbers.get( 1 ) );
            
            } else {
                return value;
            }
    
        } else {
            return value;
        }
    }
}