package it.neverworks.ui.form;

import it.neverworks.ui.types.Point;

public class SelectChangeEvent<E> extends ChangeEvent<E> {
    
    protected Object oldItem;
    protected Object newItem;
    
    protected Integer x;
    protected Integer y;
    
    public Point getOrigin() {
        return new Point( this.x, this.y );
    }
    
    public Integer getX(){
        return this.x;
    }
    
    public void setX( Integer x ){
        this.x = x;
    }
    
    public Integer getY(){
        return this.y;
    }
    
    public void setY( Integer y ){
        this.y = y;
    }
    
    public Object getOld() {
        if( this.old != null ) {
            return ((Select) this.source).getItems().get( getOldItem(), ((Select) this.source).getKey() );
        } else {
            return null;
        }
    }

    public Object getNew() {
        if( this.new_ != null ) {
            return ((Select) this.source).getItems().get( getNewItem(), ((Select) this.source).getKey() );
        } else {
            return null;
        }
    }
    
    public Object getOldItem() {
        if( oldItem == null && this.old != null) {
            oldItem = ((Select) this.source).item( this.old );
        }
        return oldItem;
    }
    
    public void setOldItem( Object oldItem ){
        this.oldItem = oldItem;
    }
    
    public Object getNewItem() {
        if( newItem == null && this.new_ != null) {
            newItem = ((Select) this.source).item( this.new_ );
        }
        return newItem;
    }
    
    public void setNewItem( Object newItem ){
        this.newItem = newItem;
    }
    
    public Object getItem() {
        return getNewItem();
    }

    public Object selected() {
        return getNewItem();
    }
    
    public Object getSelected() {
        return getNewItem();
    }
    
    public Object deselected() {
        return getOldItem();
    }
    
    public Object getDeselected() {
        return getOldItem();
    }
    
}