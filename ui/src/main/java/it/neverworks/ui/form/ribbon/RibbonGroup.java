package it.neverworks.ui.form.ribbon;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.Widget;

public class RibbonGroup extends BaseContainerWidget {

    @AutoConvert
    public static enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }

    @Property
    private String title;
    
    @Property
    private HorizontalAlignment halign = HorizontalAlignment.CENTER; 
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }

    public RibbonGroup() {
        super();
    }

    public RibbonGroup( Arguments arguments ) {
        super();
        set( arguments );
    }

    public RibbonGroup( Widget... widgets ) {
        super( widgets );
    }

    public RibbonGroup( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }
    
}