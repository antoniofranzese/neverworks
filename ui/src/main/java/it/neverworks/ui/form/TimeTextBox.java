package it.neverworks.ui.form;

import org.joda.time.DateTime;

import it.neverworks.lang.Dates;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.types.AutoType;

public class TimeTextBox extends DateTextBox implements MutableTimeValue {
    
    public TimeTextBox() {
        super();
    }
    
    public TimeTextBox( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public DateTime getDateTimeValue() {
        return Dates.datetime( get( "value" ) );
    }
    
    public void setDateTimeValue( DateTime date ) {
        set( "value", date );
    }
    
}