package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class PasswordTextBox extends TextBox {
    
    @Property @AutoConvert
    private Boolean revealed;
    
    public PasswordTextBox() {
        super();
    }
    
    public PasswordTextBox( Arguments arguments ) {
        super( arguments );
    }
    
    public Boolean getRevealed(){
        return this.revealed;
    }
    
    public void setRevealed( Boolean revealed ){
        this.revealed = revealed;
    }
   
}