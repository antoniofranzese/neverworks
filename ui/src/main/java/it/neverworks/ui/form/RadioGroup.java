package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Getter;

public class RadioGroup extends Select {

    public enum Arrangement {
        ROWS, COLUMNS
    }
    
    public enum Direction {
        HORIZONTAL, VERTICAL
    }
    
    @Property @AutoConvert
    protected Integer fractions;

    @Property @AutoConvert
    protected Arrangement arrangement;
    
    @Property @AutoConvert
    protected Direction direction;
    
    @Property
    protected boolean sparse = false;
    
    public RadioButton button( Object value ) {
        Object item = items.load( key, value );
        this.sparse = true;
        return new RadioButton( this, item );
    }
    
    protected boolean getVisible( Getter<Boolean> getter ) {
        if( this.sparse ) {
            return getter.value( false );
        } else {
            return getter.value();
        }
    }
    
    public RadioGroup() {
        super();
    }
    
    public RadioGroup( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    public Arrangement getArrangement(){
        return this.arrangement;
    }
    
    public void setArrangement( Arrangement arrangement ){
        this.arrangement = arrangement;
    }
    
    public Integer getFractions(){
        return this.fractions;
    }
    
    public void setFractions( Integer fractions ){
        this.fractions = fractions;
    }

    public boolean getSparse(){
        return this.sparse;
    }
    
    public Direction getDirection(){
        return this.direction;
    }
    
    public void setDirection( Direction direction ){
        this.direction = direction;
    }
    
    
    
}