package it.neverworks.ui.form;

import java.util.Date;
import org.joda.time.DateTime;
import it.neverworks.lang.Dates;

public interface MutableTimeValue extends TimeValue {
    
    default void readFrom( Date date ) {
        readFrom( Dates.datetime( date ) );
    }

    default void readFrom( DateTime date ) {
        setDateTimeValue( date );
    }

    public void setDateTimeValue( DateTime date );
    
}