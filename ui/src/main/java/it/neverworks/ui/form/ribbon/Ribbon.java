package it.neverworks.ui.form.ribbon;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.Widget;

public class Ribbon extends StackLayout {
    
    public Ribbon() {
        super();
    }

    public Ribbon( Arguments arguments ) {
        super();
        set( arguments );
    }

    public Ribbon( Widget... widgets ) {
        super( widgets );
    }

    public Ribbon( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
}