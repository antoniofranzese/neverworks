package it.neverworks.ui.form.lifecycle;

import it.neverworks.ui.lifecycle.CollectorJoinEvent;
import it.neverworks.ui.Widget;

public class FormJoinEvent extends CollectorJoinEvent {

    public FormJoinEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public FormJoinEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}