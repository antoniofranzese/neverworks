package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Calculator;
import it.neverworks.lang.Numbers;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.WidgetException;

public class NumberSpinner<T extends Number> extends NumberTextBox<T> {
    
    @Property @AutoConvert
    private boolean rolling = false; 
    
    @Property
    private Signal<UIEvent> increase;
    
    @Property
    private Signal<UIEvent> decrease;
    
    @Property @AutoConvert
    private Integer delta;
    
    @Property @AutoConvert
    private Integer boost;
    
    public NumberSpinner() {
        super();
    }
    
    public NumberSpinner( Arguments arguments ) {
        super();
        set( arguments );
    }
    
    protected void handleIncrease( Handler<UIEvent> handler ) {
        if( ! handler.event().getForeign() ) {
            Number value = new Calculator().push( get( "value" ) ).add( get( "delta", 1 ) ).to( this.type );

            if( max != null && Numbers.compare( value, max ) > 0 ) {
                if( rolling ) {
                    set( "value", min );
                } else {
                    set( "value", max );
                }
            } else {
                set( "value", value );
            }
        }

        handler.handle();
    }

    protected void handleDecrease( Handler<UIEvent> handler ) {
        if( ! handler.event().getForeign() ) {
            Number value = new Calculator().push( get( "value" ) ).sub( get( "delta", 1 ) ).to( this.type );

            if( min != null && Numbers.compare( value, min ) < 0 ) {
                if( rolling ) {
                    set( "value", max );
                } else {
                    set( "value", min );
                }
            } else {
                set( "value", value );
            }
        }

        handler.handle();
    }
    
    /* Bean Accessors */
    
    public boolean getRolling(){
        return this.rolling;
    }
    
    public void setRolling( boolean rolling ){
        this.rolling = rolling;
    }
    
    public Signal<UIEvent> getDecrease(){
        return this.decrease;
    }
    
    public void setDecrease( Signal<UIEvent> decrease ){
        this.decrease = decrease;
    }
    
    public Signal<UIEvent> getIncrease(){
        return this.increase;
    }
    
    public void setIncrease( Signal<UIEvent> increase ){
        this.increase = increase;
    }
    
    public Integer getDelta(){
        return this.delta;
    }
    
    public void setDelta( Integer delta ){
        this.delta = delta;
    }
    
    public Integer getBoost(){
        return this.boost;
    }
    
    public void setBoost( Integer boost ){
        this.boost = boost;
    }
    
}