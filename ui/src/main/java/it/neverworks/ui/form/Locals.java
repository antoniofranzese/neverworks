package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.Widget;

public class Locals extends BaseContainerWidget {
    
    public Locals() {
        super();
    }

    public Locals( Arguments arguments ) {
        super();
        set( arguments );
    }

    public Locals( Widget... widgets ) {
        super( widgets );
    }

    public Locals( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }

}