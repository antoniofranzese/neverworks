package it.neverworks.ui.form;

import it.neverworks.lang.Arguments;
import it.neverworks.model.io.State;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.BehavioralWidget;

import it.neverworks.model.Property;

public class HiddenBox extends BaseFormWidget implements BehavioralWidget {
    
    @Property
    protected boolean exposed = false;

    public HiddenBox() {
        super();
        set( "readonly", true );
    }

    public HiddenBox( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    protected Changes changes( Changes changes ) {
        if( ! exposed ) {
            return changes.clear();
        } else {
            return changes;
        }
    }

    @Override
    protected boolean canLoad( State state ) {
        if( exposed ) {
            return super.canLoad( state );
        } else {
            return false;
        }
    }

    public boolean getExposed(){
        return this.exposed;
    }
    
    public void setExposed( boolean exposed ){
        this.exposed = exposed;
    }

}