package it.neverworks.ui.form;

import static it.neverworks.language.*;

import java.util.List;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Watch;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.WatchChanges;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Handler;
import it.neverworks.model.validators.Validator;
import it.neverworks.model.validators.ValidationException;
import it.neverworks.ui.form.lifecycle.FocusRequestEvent;
import it.neverworks.ui.form.lifecycle.FocusReleaseEvent;
import it.neverworks.ui.form.lifecycle.FocusStatusEvent;
import it.neverworks.ui.lifecycle.WidgetAddedEvent;
import it.neverworks.ui.lifecycle.WidgetRemovedEvent;
import it.neverworks.ui.lifecycle.WidgetAbandonedEvent;
import it.neverworks.ui.lifecycle.CollectorJoinEvent;
import it.neverworks.ui.lifecycle.CollectorLeaveEvent;
import it.neverworks.ui.lifecycle.LockEvent;
import it.neverworks.ui.lifecycle.UnlockEvent;
import it.neverworks.ui.lifecycle.NotifyEvent;
import it.neverworks.ui.lifecycle.EventChannel;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.BaseWidget;
import it.neverworks.ui.Widget;
import it.neverworks.model.io.State;
import it.neverworks.ui.layout.VisibilityCapable;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.data.StyleArtifact;

public abstract class BaseFormWidget<T> extends BaseWidget implements FormWidget<T>, LabelCapable, LayoutCapable, VisibilityCapable, ResetCapable, Flavored {

    @Property
    protected Object value;
    
    @Property
    protected Label label;
    
    @Property
    protected LayoutInfo layout;
    
    @Property
    protected Size width;
    
    @Property
    protected Size height;
    
    @Property
    protected Box margin;
    
    @Property
    protected Box padding;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Font font;
    
    @Property @Watch( changes = WatchChanges.WHOLE )
    private Border border;
    
    @Property @AutoConvert
    protected boolean visible = true;
    
    @Property @AutoConvert
    protected boolean readonly = false;
    
    @Property @AutoConvert
    protected boolean disabled = false;
    
    @Property @AutoConvert
    protected boolean required = false;
    
    @Property @Convert( ProblemConverter.class )
    protected Exception problem;
    
    protected boolean joint = false;
    
    protected boolean locked = false;
    
    @Property
    protected String hint;
    
    @Property
    protected StyleArtifact flavor;
    
    @Property
    protected Signal reset;
    
    @Property
    protected Signal<ValidationState> validate;
    
    @Property @Collection( wrap = true )
    private List<Validator> validation;
    
    public BaseFormWidget() {
        super();
        set( "printing.property", "printValue" );
    }
    
    public BaseFormWidget( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    protected boolean canValidate() {
        return ! readonly 
            && ! disabled 
            && ! locked
            && visible;
    }
    
    protected void handleValidate( Handler<ValidationState> handler ) {
        if( canValidate() ) {
            if( get( "problem" ) instanceof ValidationException ) {
                set( "problem", null );
            }

            try {
                if( handler.handle().isContinued() ) {

                    if( this.required && get( "value" ) == null ) {
                        throw new ValidationException( T( "Missing value" ), get( "value" ), this, "value" );
                
                    } else if( this.validation != null ) {
                        for( Validator validator: this.validation ) {
                            try {
                                validator.validate( validator instanceof WidgetValidator ? this : get( "value" ) );
                            } catch( ValidationException ex ) {
                                throw ex.join( this, validator instanceof WidgetValidator ? null : "value" );
                            }
                        }
                    }

                }
        
            } catch( ValidationException ex ) {
                if( get( "problem" ) == null ) {
                    set( "problem", ex );
                }
                handler.event().add( ex );
            }
        } else {
            handler.decline();
        }
    }
    
    public void reset() {
        if( raw( "reset" ) != null ) {
            signal( "reset" ).fire();
        } else {
            resetValue();
        }
    }
    
    protected void handleReset( Handler handler ) {
        resetValue();
        if( handler != null ) {
            handler.handle();
        }
    }
    
    protected void resetValue() {
        set( "value", null );
    }
    
    public Form form() {
        return getForm();
    }
    
    public void load( State state ) {
        if( canLoad( state ) ) {
            loadState( state );
        } else {
            reject( state );
        }
    }

    protected void loadState( State state ) {
        if( state.has( "value" ) ) {
            set( "value", state.get( "value" ) );
        }
    }
    
    protected boolean canLoad( State state ) {
        if( ! readonly 
            && ! disabled 
            && ! locked
            && visible ) {
                
            return true;
            
        } else {
            return false;
        }
    }
    
    protected void reject( State state ) {
        model()
            .watch() // Anticipa l'osservazione
            .touch( "readonly" )
            .touch( "disabled" )
            .touch( "visible" );
        if( state.has( "value" ) ) {
            // Causa un ripristino del valore corrente
            model().touch( "value" );
        }
    }
    
    public void focus() {
        bubble( new FocusRequestEvent( this ) );
    }
    
    public void blur() {
        bubble( new FocusReleaseEvent( this ) );
    }

    protected void setLabel( Setter<Label> setter ) {
        // Gestisce la propagazione al Collector delle Label
        
        if( joint && setter.raw() != null && setter.raw().getName() != null ) {
            this.bubble( new WidgetRemovedEvent( this, setter.raw() ) );
        }
        
        setter.set();
        
        Label label = setter.value();
        
        if( label != null ) {
            if( label.getName() == null && this.getName() != null ) {
                label.setName( getName() + "$label" );
            }
               
            label.set( "parent", this );
            label.set( "target", this );
            
            for( String prop: list( "required", "readonly", "visible", "disabled", "problem" ) ) {
                label.set( prop, get( prop ) );
            }
            
            if( joint && label.getName() != null ) {
                this.bubble( new WidgetAddedEvent( this, label ) );
            }
        }
        
    }

    protected void setName( Setter<String> setter ) {
        //super.setName( setter );
        setter.set();
        Label label = raw( "label" );
        
        if( label != null && label.getName() == null ) {
            label.setName( getName() + "$label" );
        }
    }

    protected void setVisible( Setter<Boolean> setter ) {
        setter.set();
        if( getLabel() != null ) {
            getLabel().setVisible( setter.value() );
        }
    }

    protected void setRequired( Setter<Boolean> setter ) {
        setter.set();
        if( getLabel() != null ) {
            getLabel().setRequired( setter.value() );
        }
    }
    
    protected void setReadonly( Setter<Boolean> setter ) {
        setter.set();
        if( getLabel() != null ) {
            getLabel().setReadonly( setter.value() );
        }
    }
    
    protected void setDisabled( Setter<Boolean> setter ) {
        setter.set();
        if( getLabel() != null ) {
            getLabel().setDisabled( setter.value() );
        }
    }

    protected void setProblem( Setter<Exception> setter ) {
        setter.set();
        if( getLabel() != null ) {
            getLabel().setProblem( setter.value() );
        }
    }
    
    protected void notifyIn( Event event, EventChannel channel ) {
        if( event instanceof LockEvent ) {
            handleLockEvent( (LockEvent) event );
        } else if( event instanceof UnlockEvent ) {
            handleUnlockEvent( (UnlockEvent) event );
        } else {
            super.notifyIn( event, channel );
        }
    }    

    @Override
    protected void handleCollectorJoinEvent( CollectorJoinEvent event ) {
        if( event.getWidget() == this && getLabel() != null ) {
            this.joint = true;
            this.bubble( new WidgetAddedEvent( this, getLabel() ) );
        }
        super.handleCollectorJoinEvent( event );
    }
    
    @Override
    protected void handleCollectorLeaveEvent( CollectorLeaveEvent event ) {
        if( event.getWidget() == this && getLabel() != null ) {
            this.joint = false;
            this.bubble( new WidgetRemovedEvent( this, getLabel() ) );
        }
        super.handleCollectorLeaveEvent( event );
    }

    protected void handleLockEvent( LockEvent event ) {
        //System.out.println( "Locking " + this.getCommonName() );
        lock();
    }

    protected void handleUnlockEvent( UnlockEvent event ) {
        //System.out.println( "Unlocking " + this.getCommonName() );
        unlock();
    }
    
    public void lock() {
        this.locked = true;
    }
    
    public void unlock() {
        this.locked = false;
    }
    
    /* Virtual Properties */
    
    @Virtual
    protected boolean getFocused() {
        FocusStatusEvent status = new FocusStatusEvent( this );
        bubble( status );
        return status.getFocused();
    }
    
    protected void setFocused( boolean value ) {
        if( value ) {
            focus();
        } else {
            blur();
        }
    }
    
    @Virtual
    public Form getForm() {
        return ancestor( Form.class );
    }

    @Virtual
    public boolean getEmpty() {
        return raw( "value" ) == null;
    }
    
    @Virtual
    public boolean getReadOnly(){
        return (Boolean) get( "readonly" );
    }
    
    public void setReadOnly( boolean readOnly ){
        set( "readonly", readOnly );
    }
    
    @Virtual
    public boolean getWritable(){
        return ! (Boolean) get( "readonly" );
    }
    
    public void setWritable( boolean writable ){
        set( "readonly", !writable );
    }
    
    @Virtual
    public boolean getEnabled(){
        return ! (Boolean) get( "disabled" );
    }
    
    public void setEnabled( boolean enabled ){
        set( "disabled", !enabled );
    }
    
    @Virtual
    public Object getDisplayValue() {
        return getValue();
    }

    @Virtual
    public Object getPrintValue() {
        return getDisplayValue();
    }

    /* Bean Accessors */
    
    public String getHint(){
        return this.hint;
    }
    
    public void setHint( String hint ){
        this.hint = hint;
    }
    
    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }

    public Label getLabel(){
        return this.label;
    }
    
    public void setLabel( Label label ){
        this.label = label;
    }

    public T getValue() {
        return (T)this.value;
    }
    
    public void setValue( T value ) {
        this.value = value;
    }

    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }

    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public Box getMargin(){
        return this.margin;
    }
    
    public void setMargin( Box margin ){
        this.margin = margin;
    }

    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
    public boolean getReadonly(){
        return this.readonly;
    }
    
    public void setReadonly( boolean readonly ){
        this.readonly = readonly;
    }
    
    public boolean getDisabled(){
        return this.disabled;
    }
    
    public void setDisabled( boolean disabled ){
        this.disabled = disabled;
    }
 
    public Font getFont(){
        return this.font;
    }
    
    public void setFont( Font font ){
        this.font = font;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public boolean getRequired(){
        return this.required;
    }
    
    public void setRequired( boolean required ){
        this.required = required;
    }
    
    public Exception getProblem(){
        return this.problem;
    }
    
    public void setProblem( Exception problem ){
        this.problem = problem;
    }

    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public Signal<ValidationState> getValidate(){
        return this.validate;
    }
    
    public void setValidate( Signal<ValidationState> validate ){
        this.validate = validate;
    }
    
    public List<Validator> getValidation(){
        return this.validation;
    }
    
    public void setValidation( List<Validator> validation ){
        this.validation = validation;
    }
    
}