package it.neverworks.ui.form;

import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.model.utils.ToStringBuilder;

public class ChangeEvent<E> extends UIEvent<E> {
    
    protected Object old;
    
    protected Object new_;
    
    public Object getNew(){
        return this.new_;
    }
    
    public void setNew( Object new_ ){
        this.new_ = new_;
    }
    
    public Object getOld(){
        return this.old;
    }
    
    public void setOld( Object old ){
        this.old = old;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "old" ).add( "new" ).toString();
    }
}