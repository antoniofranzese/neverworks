package it.neverworks.ui.form;

import it.neverworks.lang.Dates;
import it.neverworks.model.converters.Converter;

public class TimeValueConverter implements Converter {
    public Object convert( Object value ) {

        if( value instanceof TimeValue ) {

            return value;

        } else if( Dates.isDate( value ) ) {
            
            return new StaticTimeValue( Dates.date( value ) );
        
        } else if( value instanceof String ) {

            if( "begin".equalsIgnoreCase( (String) value ) 
                || "beginning".equalsIgnoreCase( (String) value ) ) {
                return StaticTimeValue.BEGINNING;

            } else if( "end".equalsIgnoreCase( (String) value ) 
                || "ending".equalsIgnoreCase( (String) value ) ) {
                return StaticTimeValue.ENDING;

            } else {
                return value;
            }
            
        } else {
            return value;
        }

    }

}