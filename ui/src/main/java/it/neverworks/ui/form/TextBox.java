package it.neverworks.ui.form;

import java.util.regex.Pattern;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

import it.neverworks.model.Property;
import it.neverworks.model.types.Type;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.model.converters.StringConverter;

import it.neverworks.ui.lifecycle.StartupEvent;
import it.neverworks.ui.data.Formatter;

public class TextBox extends BaseTextBox<String> {
   
    public enum Letter {
        NORMAL, UPPER, LOWER, CAPITAL
    }

    @Property
    protected Integer length;

    @Property @Ignore @Access( setter = "setTextModifier" )
    protected Formatter format;
    
    @Property @Ignore @Access( setter = "setTextModifier" )
    protected boolean trim = true;
    
    @Property @Ignore @AutoConvert @Access( setter = "setTextModifier" )
    protected Pattern keep;

    @Property @Ignore @AutoConvert @Access( setter = "setTextModifier" )
    private Pattern waste;

    @Property @Ignore @AutoConvert @Access( setter = "setTextModifier" )
    private Letter letter;
    
    protected Object startupValue;
    
    public TextBox() {
        super();
        on( "startup", slot( "onStartup" ).unsigned() );
    }
    
    public TextBox( Arguments arguments ) {
        this();
        set( arguments );
    }    

    public String processValue( Object value ) {
        
        if( value != null ) {
            String processed = null;
            
            if( this.format != null ) {
                try {
                    processed = Strings.valueOf( this.format.format( value ) );
                } catch( Exception ex ) {
                    processed = Strings.valueOf( value );
                }
            } else {
                processed = Strings.valueOf( value );
            }
        
            if( processed != null ) {
                
                if( this.keep != null ) {
                    processed = Strings.keep( processed, this.keep );
                }
        
                if( this.waste != null ) {
                    processed = Strings.waste( processed, this.waste );
                }

                if( this.trim ) {
                    processed = processed.trim();
                }
                
                if( this.length != null && this.length > 0 && processed.length() > length  ) {
                    processed = processed.substring( 0, length );
                }
                
                if( this.letter != null ) {
                    switch( this.letter ) {
                        case UPPER:
                            processed = processed.toUpperCase();
                            break;
                        case LOWER:
                            processed = processed.toLowerCase();
                            break;
                        case CAPITAL:
                            processed = Strings.capitalize( processed );
                            break;
                        default:
                            break;
                    }
                }

                return processed.length() > 0 ? processed : null;

            } else {
                return null;
            }

        } else {
            return null;
        }
        
    }

    protected void onStartup() {
        if( this.startupValue != null ) {
            this.value = processValue( this.startupValue );
            this.startupValue = null;
        }
    }
    
    protected Object getValue( Getter getter ) {
        if( ! this.started ) {
            signal( "startup" ).fire();
        }
        return Strings.valueOf( getter.get() );
    }
    
    protected void setValue( Setter setter ) {
        if( ! this.started ) {
            this.startupValue = setter.value();
            setter.decline();
        } else {
            setter.set( processValue( setter.value() ) );
        }
    }
    
    protected void setLength( Setter<Integer> setter ) {
        if( setter.value() != null && setter.value() <= 0 ) {
            throw new IllegalArgumentException( "Invalid " + this.getCommonName() + " length: " + setter.value() );
        } else {
            setTextModifier( setter );
        }
    }
    
    protected void setTextModifier( Setter setter ) {
        setter.set();
        if( this.started ) {
            set( "value", get( "value" ) );
        }
    }
    
    @Virtual
    public boolean getEmpty() {
        return ! Strings.hasText( this.value );
    }
    
    public Integer getLength(){
        return this.length;
    }
    
    public void setLength( Integer length ){
        this.length = length;
    }

    public Formatter getFormat(){
        return this.format;
    }
    
    public void setFormat( Formatter format ){
        this.format = format;
    }
    
    public boolean getTrim(){
        return this.trim;
    }
    
    public void setTrim( boolean trim ){
        this.trim = trim;
    }
    
    public Pattern getKeep(){
        return this.keep;
    }
    
    public void setKeep( Pattern keep ){
        this.keep = keep;
    }
    
    public Pattern getWaste(){
        return this.waste;
    }
    
    public void setWaste( Pattern waste ){
        this.waste = waste;
    }
}