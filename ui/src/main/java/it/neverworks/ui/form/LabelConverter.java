package it.neverworks.ui.form;

import java.util.Map;
import it.neverworks.lang.Strings;
import it.neverworks.model.converters.ModelConverter;

public class LabelConverter extends ModelConverter {
    
    public Object convert( Object value ) {
        if( value instanceof String ) {
            return new Label().set( "value", value );
        } else if( value instanceof Number ) {
            return new Label().set( "value", Strings.valueOf( value ) );
        } else {
            return super.convert( value );
        }
    }
    
}