package it.neverworks.ui.form;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.StructureCapable;
import it.neverworks.ui.layout.VisibilityCapable;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.lifecycle.LockEvent;
import it.neverworks.ui.lifecycle.UnlockEvent;
import it.neverworks.ui.data.StyleArtifact;

public class ToolBar extends BaseContainerWidget implements LayoutCapable, Flavored, VisibilityCapable {
    
    public enum Orientation {
        HORIZONTAL, VERTICAL
    }
    
    public enum HorizontalAlignment {
        LEFT, CENTER, RIGHT
    }

    public enum VerticalAlignment {
        TOP, MIDDLE, BOTTOM
    }
    
    public enum Justification {
        NONE, WIDTH, HEIGHT, BOTH
    }
    
    @Property
    private LayoutInfo layout;
    
    @Property @AutoConvert
    private Orientation orientation;
    
    @Property @AutoConvert
    private HorizontalAlignment halign;
    
    @Property @AutoConvert
    private VerticalAlignment valign;
    
    @Property @AutoConvert
    private Justification justify;
    
    @Property 
    private Box padding;
    
    @Property 
    private Box margin;
    
    @Property 
    private Size width;
    
    @Property 
    private Size height;
    
    @Property
    private Size defaultWidth;
    
    @Property
    private Size defaultHeight;
    
    @Property @AutoConvert
    private boolean visible = true;

    @Property
    private boolean collapsible = false; 
    
    @Property
    private StyleArtifact flavor;
    
    @Property
    private StyleArtifact defaultFlavor;
    
    @Property
    private String defaultHint;
    
    @Property
    private String hint;
    
    @Property
    private Border border;
    
    @Property
    private Background background;
    
    public ToolBar() {
        super();
    }

    public ToolBar( Arguments arguments ) {
        super();
        set( arguments );
    }

    public ToolBar( Widget... widgets ) {
        super( widgets );
    }

    public ToolBar( Arguments arguments, Widget... widgets ) {
        super( widgets );
        set( arguments );
    }
    
    protected void setVisible( Setter<Boolean> setter ) {
        setter.set();
        if( Boolean.TRUE.equals( setter.value() ) ) {
            spread( new UnlockEvent( this ) );
        } else {
            spread( new LockEvent( this ) );
        }
    }

    protected StyleArtifact getDefaultFlavor( Getter<StyleArtifact> getter ) {
        if( getter.value() != null ) {
            return getter.value();
        } else {
            return getter.value( getFlavor() );
        }
    }

    protected String getDefaultHint( Getter<String> getter ) {
        if( Strings.hasText( getter.value() ) ) {
            return getter.value();
        } else {
            return getter.value( getHint() );
        }
    }

    protected void spreadOut( Event event ) {
        if( event instanceof UnlockEvent && ! visible ) {
            // do nothing
        } else {
            super.spreadOut( event );
        }
    }

    public boolean isStructural() {
        return true;
    }
    
    /* Bean Accessors */

    public String getDefaultHint(){
        return this.defaultHint;
    }
    
    public void setDefaultHint( String defaultHint ){
        this.defaultHint = defaultHint;
    }
    
    public StyleArtifact getDefaultFlavor(){
        return this.defaultFlavor;
    }
    
    public void setDefaultFlavor( StyleArtifact defaultFlavor ){
        this.defaultFlavor = defaultFlavor;
    }
    
    public boolean getCollapsible(){
        return this.collapsible;
    }
    
    public void setCollapsible( boolean collapsible ){
        this.collapsible = collapsible;
    }

    public boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( boolean visible ){
        this.visible = visible;
    }
    
    public Size getDefaultHeight(){
        return this.defaultHeight;
    }
    
    public void setDefaultHeight( Size defaultHeight ){
        this.defaultHeight = defaultHeight;
    }
    
    public Size getDefaultWidth(){
        return this.defaultWidth;
    }
    
    public void setDefaultWidth( Size defaultWidth ){
        this.defaultWidth = defaultWidth;
    }

    public LayoutInfo getLayout(){
        return this.layout;
    }
    
    public void setLayout( LayoutInfo layout ){
        this.layout = layout;
    }

    public Orientation getOrientation(){
        return this.orientation;
    }
    
    public void setOrientation( Orientation orientation ){
        this.orientation = orientation;
    }
    
    public Box getMargin(){
        return this.margin;
    }
    
    public void setMargin( Box margin ){
        this.margin = margin;
    }
    
    public Box getPadding(){
        return this.padding;
    }
    
    public void setPadding( Box padding ){
        this.padding = padding;
    }
    
    public HorizontalAlignment getHalign(){
        return this.halign;
    }
    
    public void setHalign( HorizontalAlignment halign ){
        this.halign = halign;
    }
    
    public VerticalAlignment getValign(){
        return this.valign;
    }
    
    public void setValign( VerticalAlignment valign ){
        this.valign = valign;
    }
    
    public StyleArtifact getFlavor(){
        return this.flavor;
    }
    
    public void setFlavor( StyleArtifact flavor ){
        this.flavor = flavor;
    }
    
    public Size getHeight(){
        return this.height;
    }
    
    public void setHeight( Size height ){
        this.height = height;
    }
    
    public Size getWidth(){
        return this.width;
    }
    
    public void setWidth( Size width ){
        this.width = width;
    }
        
    public String getHint(){
        return this.hint;
    }
    
    public void setHint( String hint ){
        this.hint = hint;
    }
        
    public Justification getJustify(){
        return this.justify;
    }
    
    public void setJustify( Justification justify ){
        this.justify = justify;
    }
    
    public Border getBorder(){
        return this.border;
    }
    
    public void setBorder( Border border ){
        this.border = border;
    }
    
    public Background getBackground(){
        return this.background;
    }
    
    public void setBackground( Background background ){
        this.background = background;
    }
    
}