package it.neverworks.ui.form;

public interface ResetCapable {
    void reset();
}