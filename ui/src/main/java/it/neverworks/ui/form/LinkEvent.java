package it.neverworks.ui.form;

public class LinkEvent extends ClickEvent {

    protected String link;
    
    public String getLink(){
        return this.link;
    }
    
    public void setLink( String link ){
        this.link = link;
    }

}