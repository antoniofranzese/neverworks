package it.neverworks.ui.form;

import java.util.List;
import it.neverworks.model.events.Signal;
import it.neverworks.model.validators.Validator;
import it.neverworks.ui.lifecycle.UIEvent;

public interface ValidationCapable {
    public Signal<ValidationState> getValidate();

    default boolean isValid() {
        return getValidate().fire().isValid();
    }

    default ValidationState validate() {
        return getValidate().fire();
    }
    
}