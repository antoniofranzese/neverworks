package it.neverworks.ui;

import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.NumberTextBox;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Locatable;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.lang.FluentMap;

import java.util.Map;
import java.util.HashMap;

//TODO: in disuso, valutare una forma piu' utile o cancellare
public class UI {

    public static Button Button() {
        return new Button();
    }

    public static BorderLayout BorderLayout() {
        return new BorderLayout();
    }

    public static GridLayout GridLayout( Widget... widgets ) {
        return new GridLayout( widgets );
    }
    
    public static Slot Slot( Locatable source, String method ) {
        return new Slot( source, method );
    }

    public static NumberSpinner NumberSpinner() {
        return new NumberSpinner();
    }

    public static NumberTextBox NumberTextBox() {
        return new NumberTextBox();
    }

    public static Panel Panel() {
        return new Panel();
    }

    public static TextBox TextBox() {
        return new TextBox();
    }

    public static ToolBar ToolBar( Widget... widgets ) {
        return new ToolBar( widgets );
    }
    
    public static FluentMap set( Object key, Object value ) {
        return new FluentHashMap().add( key, value );
    }
    
}