package it.neverworks.ui;

public class MissingWidgetException extends WidgetException {
    
    public MissingWidgetException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public MissingWidgetException( String message ){
        super( message );
    }
}