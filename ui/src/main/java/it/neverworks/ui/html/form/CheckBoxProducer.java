package it.neverworks.ui.html.form;

import java.util.List;

import it.neverworks.lang.Properties;
import it.neverworks.lang.Strings;

import it.neverworks.encoding.HTML;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.form.CheckBox;

public class CheckBoxProducer extends BaseFormWidgetProducer<CheckBox> {
    
    public void render( CheckBox widget ) {
        
        output.dojo.start( "div" )
            .attr( "type", "works/form/CheckBox" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "value", widget.getValue() )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "disabled", widget.getDisabled() ? true : null )
            .attr( "readonly", widget.getReadonly() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() ) 
            .style( "border", widget.getBorder() )
            .style( dumpFlavor( widget ) ); 
        
        output.end();
    }
}