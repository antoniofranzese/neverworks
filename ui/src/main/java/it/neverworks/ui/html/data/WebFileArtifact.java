package it.neverworks.ui.html.data;

import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.data.FileArtifact;


public class WebFileArtifact implements FileArtifact {
    
    private String name;
    private String url;
    
    public WebFileArtifact( String name, String url ) {
        this.name = name;
        this.url = url;
    }

    public String getURL(){
        return this.url;
    }

    public String getUrl(){
        return this.url;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "name" ).add( "url" ).toString();
    }
}