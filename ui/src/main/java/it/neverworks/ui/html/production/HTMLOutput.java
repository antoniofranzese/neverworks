package it.neverworks.ui.html.production;

import java.util.Stack;

import java.io.Writer;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.IOException;

import it.neverworks.lang.Errors;
import it.neverworks.io.StringWriter;
import it.neverworks.ui.production.ProductionException;

public class HTMLOutput {
    
    protected Writer writer;
    protected Stack<HTMLTag> document = new Stack<HTMLTag>(); 
    public final DojoOutput dojo;                     
    
    public HTMLOutput() {
        this.writer = new StringWriter();
        this.dojo = null;
    }

    public HTMLOutput( OutputStream stream ) {
        this.writer = new OutputStreamWriter( stream );
        this.dojo = new DojoOutput( this );
    }
    
    public HTMLOutput( Writer writer ) {
        this.writer = writer;
        this.dojo = new DojoOutput( this );
    }
    
    protected HTMLTag newTag( String name ) {
        return new HTMLTag( name );
    }

    public Tag start( String name ) {
        if( document.size() > 0 ) {
            document.peek().start( this );
        }
        HTMLTag tag = newTag( name );
        document.push( tag );
        return tag;
    }
    
    public HTMLOutput end( String name ) {
        if( document.size() == 0 || ! document.peek().name.equals( name ) ) {
            throw new ProductionException( "Invalid '" + name + "' tag ending, " + ( document.size() > 0 ? ( "'" + document.peek().name + "' expected" ) : "empty document" ) );
        } else {
            return end();
        }
    }
    
    public HTMLOutput end() {
        if( document.size() > 0 ) {
            HTMLTag tag = document.pop();
            tag.end( this );
        }
        return this;
    }
    
    public HTMLOutput text( String text, Object... args ) {
        if( document.size() > 0 ) {
            document.peek().start( this );
        }
        if( text != null ) {
            if( args != null && args.length > 0 ) {
                write( String.format( text, args ) );
            } else {
                write( text );
            }
        }
        return this;
    }
    
    /* package */ void write( String text ) {
        try {
            writer.write( text );
        } catch( IOException ex ) {
            throw new RuntimeException( ex.getMessage(), ex );
        }
    }
    
    public void flush() {
        try {
            this.writer.write( "\n" );
            this.writer.flush();
        } catch( IOException ex ) {
            throw new RuntimeException( ex.getMessage(), ex );
        }
    }
    
    public Writer getWriter(){
        return this.writer;
    }
    
    public String getContent() {
        if( this.writer instanceof StringWriter ) {
            try {
                this.writer.flush();
                return this.writer.toString();
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
            
        } else {
            throw new RuntimeException( "Cannot retrieve content, writer is not a StringWriter" );
        }
    }
}