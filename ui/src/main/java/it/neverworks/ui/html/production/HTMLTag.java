package it.neverworks.ui.html.production;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.JSON;

public class HTMLTag implements Tag {
    /*package*/ String name;
    protected boolean started = false;
    protected Map<String, Object> attributes;
    protected Map<String, Object> styles;
    protected List<String> classes;
    
    public HTMLTag( String name ) {
        this.name = name;
    }
    
    public Tag attr( JsonNode source ) {
        if( source != null ) {
            Iterator<String> names = source.fieldNames();
            while( names.hasNext() ) {
                String name = names.next();
                this.attr( name, JSON.decode( source.get( name ) ) );
            }
        }
        return this;
    }

    public Tag attr( Arguments source ) {
        if( source != null ) {
            for( String name: source.names() ) {
                this.attr( name, source.get( name ) );
            }
        }
        return this;
    }
    
    public Tag attr( String name, Object value ) {
        if( attributes == null ) {
            attributes = new HashMap<String,Object>();
        }
        if( value != null ) {
            attributes.put( name, value );
        }
        return this;
    }
    
    public Tag style( String name ) {
        if( Strings.hasText( name ) ) {
            String cls = name.trim();
            if( classes == null ) {
                classes = new ArrayList<String>();
            }
            if( ! classes.contains( cls ) ) {
                classes.add( cls );
            }
        }
        return this;
    }
    
    public Tag style( String name, Object value ) {
        if( value != null ) {
            String cssValue = CSSValueConverter.convert( value );

            if( cssValue != null ) {
                if( styles == null ) {
                    styles = new HashMap<String,Object>();
                }

                styles.put( name, cssValue );
            }
        }
        return this;
    }
    
    public Tag style( Arguments source ) {
        if( source != null ) {
            for( String name: source.names() ) {
                this.style( name, source.get( name ) );
            }
        }
        return this;
    }

    public void start( HTMLOutput output ) {
        if( !started ) {
            this.started = true;
            output.write( "<" + name );
            writeAttributes( output );
            output.write( ">" );
        }
    }
    
    public void end( HTMLOutput output ) {
        this.start( output );
        output.write( "</" + name + ">" );
    }
    
    protected void writeAttributes( HTMLOutput output ) {
        if( attributes != null ){
            for( String name: attributes.keySet() ) {
                output.write( " " + name + "=\"" + String.valueOf( attributes.get( name ) ) + "\"" );
            }
        }
    
        writeClasses( output );
        writeStyles( output );
        
    }
    
    protected void writeClasses( HTMLOutput output ) {
        if( classes != null ) {
            output.write( " class=\"" );
            for( String cls: classes ){
                output.write( cls );
                output.write( " " );
            }
            output.write( "\"" );
        }
    }
    
    protected void writeStyles ( HTMLOutput output ) {
        
        if( styles != null ) {
            output.write( " style=\"" );
            for( String name: styles.keySet() ) {
                output.write( name + ":" + styles.get( name ) + ";" );
            }
            output.write( "\"" );
            
        }
    }    

}
