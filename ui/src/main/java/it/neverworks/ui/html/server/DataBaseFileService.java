package it.neverworks.ui.html.server;

import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Date;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DataAccessException;
import it.neverworks.log.Logger;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.JSON;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.description.RequiredPropertyException;
import it.neverworks.model.context.Inject;
import it.neverworks.model.expressions.Template;
import it.neverworks.io.AbstractFileRepository;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;

public class DataBaseFileService extends AbstractFileRepository implements FileRepository, InitializingBean {

	private static final Logger sqlLogger = Logger.of( "SQL" );

    private static Template INSERT_KEY = new Template( "INSERT INTO {table} ({token}) VALUES (?)" ); 

    private static Template UPDATE_ALL = new Template( "UPDATE {table} SET {content} = ?, {info} = ?, {updated} = ? WHERE {token} = ?" ); 
    private static Template UPDATE_INFO = new Template( "UPDATE {table} SET {info} = ?, {updated} = ? WHERE {token} = ?" ); 
    private static Template UPDATE_CONTENT = new Template( "UPDATE {table} SET {content} = ?, {updated} = ? WHERE {token} = ?" ); 

    private static Template SELECT_CONTENT = new Template( "SELECT {content} FROM {table} WHERE {token} = ?");
    private static Template SELECT_INFO = new Template( "SELECT {info} FROM {table} WHERE {token} = ?");
        
    private static Template DELETE = new Template( "DELETE FROM {table} WHERE {token} = ?" ); 

    @Property @Required
    private String table;
    
    @Property @Required
    private String token = "token";

    @Property @Required
    private String content = "content";

    @Property @Required
    private String info = "info";
    
    private String updated = "updated"; 
    
    @Property @Inject
    private DataSource dataSource;
    
    @Property @Inject
    private LobHandler lobHandler;
    
    @Property
    private FileRepository temporaryFiles = null;
    
    public File createFile( Arguments arguments ){
        return files().createFile( arguments );
    }
    
    protected File retrieveFile( FileBadge badge ) {
        FileBadge tmp = files().putStream( getStream( badge ), arg( "extension", "dbtmp" ) );
        File file = files().getFile( tmp );
        return file;
    }

	public File getFile( FileBadge badge ){
        File file = retrieveFile( badge );
        file.setReadOnly();
        return file;
    }
    
	public FileInfo getInfo( FileBadge badge ){
        FileInfo result = new FileInfo()
            .set( "repository", this )
            .set( "badge", badge )
            .set( "attributes", readInfo( badge.getToken() ) );
        return result;
    }
    
    public OutputStream openStream( FileBadge badge ) {
        try {
            return new ConnectedOutputStream( retrieveFile( badge ), this, badge );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public Writer openWriter( FileBadge badge ) {
        try {
            return new ConnectedWriter( retrieveFile( badge ), this, badge );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    protected void writeInfo( String token, Arguments arguments ){
        Connection connection = null;
        try {
            
            String updateSQL = UPDATE_INFO.apply( getSqlInfo() );
            sqlLogger.debug( updateSQL );
            connection = DataSourceUtils.getConnection( dataSource );
            PreparedStatement ps1 = connection.prepareStatement( updateSQL );
 
            ps1.setString( 1, JSON.encode( arguments ).toString() );
            ps1.setTimestamp( 2, new Timestamp( System.currentTimeMillis() ) );
            
            ps1.setString( 3, extractToken( token ) );

            try {
                ps1.execute();
 
                if( ps1.getUpdateCount() == 0 ) {
                    throw new IllegalArgumentException( "Missing file for token: " + token );
                }

            } catch( SQLException sqlex ) {
                throw getJdbcTemplate().getExceptionTranslator().translate( "Writing content to new stream", updateSQL, sqlex );
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );

        } finally {
            DataSourceUtils.releaseConnection( connection, dataSource );
        }
    
    }
    
	public InputStream getStream( FileBadge badge ){
        Connection connection = null;
        String sql = SELECT_CONTENT.apply( getSqlInfo() );
        sqlLogger.debug( sql );
        try {
            connection = DataSourceUtils.getConnection( dataSource );
            PreparedStatement ps = connection.prepareStatement( sql );
        
            ps.setString( 1, extractToken( badge.getToken() ) );
            
            ResultSet rs = ps.executeQuery();
            
            if( rs.next() ) {
                return lobHandler.getBlobAsBinaryStream( rs, 1 );
            
            } else {
                throw new IllegalArgumentException( "Missing file for token: " + badge.getToken() );
            }

        } catch( SQLException se ) {
            throw getJdbcTemplate().getExceptionTranslator().translate( "Reading stream", sql, se );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        } finally {
            DataSourceUtils.releaseConnection( connection, dataSource );
        }
    }

	protected Arguments readInfo( String token ){
        Connection connection = null;
        String sql = SELECT_INFO.apply( getSqlInfo() );
        try {
            sqlLogger.debug( sql );
            connection = DataSourceUtils.getConnection( dataSource );
            PreparedStatement ps = connection.prepareStatement( sql );
        
            ps.setString( 1, extractToken( token ) );
            
            ResultSet rs = ps.executeQuery();
            
            if( rs.next() ) {
                Object json = JSON.decode( rs.getString( 1 ) );
                if( json instanceof Map ) {
                    return new Arguments( (Map) json );
                } else {
                    return new Arguments();
                }
            
            } else {
                throw new IllegalArgumentException( "Missing file for token: " + token );
            }
 
        } catch( SQLException se ) {
            throw getJdbcTemplate().getExceptionTranslator().translate( "Reading info", sql, se );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        } finally {
            DataSourceUtils.releaseConnection( connection, dataSource );
        }
    }

    protected String storeStream( InputStream stream, Arguments arguments ) {
        Connection connection = null;
        String id = null;
        boolean written = false;
        try {
            String insertSQL = INSERT_KEY.apply( getSqlInfo() );
            sqlLogger.debug( insertSQL );
            connection = DataSourceUtils.getConnection( dataSource );
            PreparedStatement ps = connection.prepareStatement( insertSQL );
            
            int tries = 100;
            while( ! written && tries > 0 ) {
                tries--;
                id = generateFileId();
                ps.setString( 1, id );
                
                try {
                    ps.execute();
                    written = true;

                } catch( SQLException sqlex ) {
                    DataAccessException translated = getJdbcTemplate().getExceptionTranslator().translate( "Writing new stream", insertSQL, sqlex );
                    if( !( translated instanceof DataIntegrityViolationException ) ) {
                        throw translated;
                    }
                }
                
            }
            
            if( written ) {
                String updateSQL = UPDATE_ALL.apply( getSqlInfo() );
                sqlLogger.debug( updateSQL );
                connection = DataSourceUtils.getConnection( dataSource );
                PreparedStatement ps1 = connection.prepareStatement( updateSQL );
                lobHandler.getLobCreator().setBlobAsBinaryStream( ps1, 1, stream, 0 );

                ps1.setString( 2, JSON.encode( arguments ).toString() );
                ps1.setTimestamp( 3, new Timestamp( System.currentTimeMillis() ) );
                ps1.setString( 4, id );

                try {
                    ps1.execute();

                } catch( SQLException sqlex ) {
                    throw getJdbcTemplate().getExceptionTranslator().translate( "Writing content to new stream", updateSQL, sqlex );
                }

            } else {
                throw new RuntimeException( "Cannot write stream" );
            }

            return id;

        } catch( Exception ex ) {
            
            try {
                if( written ) {
                    String deleteSQL = DELETE.apply( getSqlInfo() );
                    PreparedStatement ps2 = connection.prepareStatement( deleteSQL );
                    ps2.setString( 1, id );
                    ps2.execute();
                }
            } catch( SQLException sqlex ) {
                // Do nothing
            }
            
            throw Errors.wrap( ex );

        } finally {
            DataSourceUtils.releaseConnection( connection, dataSource );
        }
        
    }
	
    protected void updateStream( FileBadge badge, InputStream stream ) {
        Connection connection = null;
        try {
            String updateSQL = UPDATE_CONTENT.apply( getSqlInfo() );
            sqlLogger.debug( updateSQL );
            connection = DataSourceUtils.getConnection( dataSource );
            PreparedStatement ps = connection.prepareStatement( updateSQL );
            
            lobHandler.getLobCreator().setBlobAsBinaryStream( ps, 1, stream, 0 );
            ps.setTimestamp( 2, new Timestamp( System.currentTimeMillis() ) );
            
            ps.setString( 3, extractToken( badge.getToken() ) );

            try {
                ps.execute();
                if( ps.getUpdateCount() == 0 ) {
                    throw new IllegalArgumentException( "Missing file for token: " + badge.getToken() );
                }

            } catch( SQLException sqlex ) {
                throw getJdbcTemplate().getExceptionTranslator().translate( "Updating stream content", updateSQL, sqlex );
            }

        } catch( Exception ex ) {
            throw Errors.wrap( ex );

        } finally {
            DataSourceUtils.releaseConnection( connection, dataSource );
        }
	}

    public void afterPropertiesSet() {
        if( dataSource == null ) {
            throw new RequiredPropertyException( this, "dataSource" );
        }
        if( lobHandler == null ) {
            throw new RequiredPropertyException( this, "lobHandler" );
        }

        for( String name: query( model().descriptor().properties() ).by( "required" ).eq( true ).list( "name" ).as( String.class ) ) { 
            get( name );
        }
    }

    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate(){
        if( this.jdbcTemplate == null ) {
            this.jdbcTemplate = new JdbcTemplate( dataSource );
        }
        return this.jdbcTemplate;
    }
    
    private Arguments sqlInfo;

    public Arguments getSqlInfo(){
        if( this.sqlInfo == null ) {
            sqlInfo =
                arg( "table", getTable() )
                .arg( "token", getToken() )
                .arg( "content", getContent() )
                .arg( "info", getInfo() )
                .arg( "updated", getUpdated() );
        }
        return this.sqlInfo;
    }
    
    protected FileRepository files() {
        if( temporaryFiles != null ) {
            return temporaryFiles;
        } else {
            throw new RuntimeException( "Operation is not supported, configure temporary files" );
        }
    }
    
    private class ConnectedOutputStream extends FileOutputStream {
        private FileBadge badge;
        private DataBaseFileService parent;
        private File file;
        
        public ConnectedOutputStream( File file, DataBaseFileService parent, FileBadge badge ) throws FileNotFoundException {
            super( file );
            this.file = file;
            this.parent = parent;
            this.badge = badge;
        }
        
        public void close() throws IOException {
            super.close();
            parent.updateStream( badge, new FileInputStream( file ) );
        }
    }

    private class ConnectedWriter extends FileWriter {
        private FileBadge badge;
        private DataBaseFileService parent;
        private File file;
        
        public ConnectedWriter( File file, DataBaseFileService parent, FileBadge badge ) throws IOException {
            super( file );
            this.file = file;
            this.parent = parent;
            this.badge = badge;
        }
        
        public void close() throws IOException {
            super.close();
            parent.updateStream( badge, new FileInputStream( file ) );
        }
    }
    
    public String getUpdated(){
        return this.updated;
    }
    
    public void setUpdated( String updated ){
        this.updated = updated;
    }
    
    public String getTable(){
        return this.table;
    }
    
    public void setTable( String table ){
        this.table = table;
    }

    public String getContent(){
        return this.content;
    }
    
    public void setContent( String content ){
        this.content = content;
    }
    
    public String getToken(){
        return this.token;
    }
    
    public void setToken( String token ){
        this.token = token;
    }

    public String getInfo(){
        return this.info;
    }
    
    public void setInfo( String info ){
        this.info = info;
    }
    
    public DataSource getDataSource(){
        return this.dataSource;
    }
    
    public void setDataSource( DataSource dataSource ){
        this.dataSource = dataSource;
    }

    public LobHandler getLobHandler(){
        return this.lobHandler;
    }
    
    public void setLobHandler( LobHandler lobHandler ){
        this.lobHandler = lobHandler;
    }
    
    public void setTemporaryFiles( FileRepository temporaryFiles ){
        this.temporaryFiles = temporaryFiles;
    }
    
}