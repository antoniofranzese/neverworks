package it.neverworks.ui.html.form;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.NumberTextBox;

public class NumberTextBoxProducer<T extends NumberTextBox> extends BaseTextBoxProducer<T> {
    
    protected final static long MAX_PRECISION = 20L;
    
    
    protected final static long MIN_LONG = new Long( Long.MIN_VALUE ).longValue();
    protected final static long MAX_LONG = new Long( Long.MAX_VALUE ).longValue();
    protected final static long MIN_INTEGER = new Integer( Integer.MIN_VALUE ).longValue();
    protected final static long MAX_INTEGER = new Integer( Integer.MAX_VALUE ).longValue();
    protected final static long MIN_SHORT = new Short( Short.MIN_VALUE ).longValue();
    protected final static long MAX_SHORT = new Short( Short.MAX_VALUE ).longValue();
    protected final static long MIN_BYTE = new Byte( Byte.MIN_VALUE ).longValue();
    protected final static long MAX_BYTE = new Byte( Byte.MAX_VALUE ).longValue();
    protected final static long MIN_FLOAT = new Float( Float.MIN_VALUE ).longValue();
    protected final static long MAX_FLOAT = new Float( Float.MAX_VALUE ).longValue();
    protected final static long MIN_DOUBLE = new Double( Double.MIN_VALUE ).longValue();
    protected final static long MAX_DOUBLE = new Double( Double.MAX_VALUE ).longValue();
    
    public Tag start( T widget ) {
        Tag start = super.start( widget );
        if( development() ) {
            start.attr( "value$type", widget.getType().getName() );
        }

        Long minValue = widget.getMin() != null ? widget.getMin().longValue() : null;
        Long maxValue = widget.getMax() != null ? widget.getMax().longValue() : null;
        Long minDecimals = widget.get( "!decimals.min" );
        Long maxDecimals = widget.get( "!decimals.max" );

        if( minValue == null ) {
            if( widget.getType() == Integer.class ) {
                minValue = MIN_INTEGER;
            } else if( widget.getType() == Long.class ) {
                minValue = MIN_LONG;
            } else if( widget.getType() == Float.class ) {
                minValue = MIN_FLOAT;
            } else if( widget.getType() == Double.class ) {
                minValue = MIN_DOUBLE;
            } else if( widget.getType() == Short.class ) {
                minValue = MIN_SHORT;
            } else if( widget.getType() == Byte.class ) {
                minValue = MIN_BYTE;
            }
        }

        if( maxValue == null ) {
            if( widget.getType() == Integer.class ) {
                maxValue = MAX_INTEGER;
            } else if( widget.getType() == Long.class ) {
                maxValue = MAX_LONG;
            } else if( widget.getType() == Float.class ) {
                maxValue = MAX_FLOAT;
            } else if( widget.getType() == Double.class ) {
                maxValue = MAX_DOUBLE;
            } else if( widget.getType() == Short.class ) {
                maxValue = MAX_SHORT;
            } else if( widget.getType() == Byte.class ) {
                maxValue = MAX_BYTE;
            }
        }

        if( minDecimals == null && maxDecimals == null ) {
            
            if( widget.getType() == Integer.class 
                || widget.getType() == Long.class            
                || widget.getType() == Short.class
                || widget.getType() == Byte.class ) {
                
                minDecimals = 0L;
                maxDecimals = 0L;   
                
            } else if( widget.getType() == Float.class 
                    || widget.getType() == Double.class ) {
                        
                maxDecimals = MAX_PRECISION;
            }
        }

        Arguments constraints =
            arg( "min", minValue )
            .arg( "max", maxValue );

        if( minDecimals != null ) {
            if( minDecimals < 0 ) {
                minDecimals = 0L;
            } else if( minDecimals > MAX_PRECISION ) {
                minDecimals = MAX_PRECISION;
            }
        }
        
        if( maxDecimals != null ) {
            if( maxDecimals < 0 ) {
                maxDecimals = 0L;
            } else if( maxDecimals > MAX_PRECISION ) {
                maxDecimals = MAX_PRECISION;
            }
        }
        
        if( minDecimals != null && maxDecimals != null ) {
            if( maxDecimals < minDecimals ) {
                maxDecimals = minDecimals;
            }
            if( minDecimals.equals( maxDecimals ) ) {
                constraints.arg( "places", minDecimals );
            } else {
                constraints.arg( "places", msg( "{0},{1}", minDecimals, maxDecimals ) );
            }
        } else if( maxDecimals == null ) {
            constraints.arg( "places", msg( "{0},{1}", minDecimals, MAX_PRECISION ) );
        } else if( minDecimals == null ) {
            constraints.arg( "places", msg( "{0},{1}", 0, maxDecimals ) );
        }
                
        start.attr( "constraints", constraints );

        return start;
    }
    
    protected String dojoType( T widget ) {
        return "works/form/NumberTextBox";
    }
    
}