package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.ui.Widget;
import it.neverworks.ui.form.Timer;
import it.neverworks.ui.html.production.HTMLProducer;

public class TimerProducer extends HTMLProducer<Timer>{
    
    public void render( Timer widget ) {
        output.dojo.start( "div" )
            .style( "display", "none" );
        
        output.dojo.start( "div" )
            .attr( "type", "works/form/Timer" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "interval", widget.getInterval() )
            .attr( "startupRunning", widget.getRunning() );        
            
        output.end();
        output.end();
    
    }
    
}