package it.neverworks.ui.html.grid;

import static it.neverworks.language.*;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.HTML;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.grid.PropertyCell;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.types.Decorator;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.ColorConverter;

public class PropertyCellProducer<T extends PropertyCell> extends BaseCellProducer<T>{

    public ObjectNode dumpColumn( T cell ) {
        ObjectNode columnNode = super.dumpColumn( cell );
        ObjectNode decoratorNode = dumpColumnDecorator( cell );

        if( decoratorNode != null ) {
            columnNode.set( "decoratorParams", decoratorNode );
        }
        
        return columnNode;
    }
    
    protected ObjectNode dumpColumnDecorator( T cell ) {
        GridColumn column = cell.getColumn();

        if( column.getHeight() != null 
            || column.getHalign() != null 
            || column.getValign() != null
            || column.getWrap() != null 
            || column.getFont() != null
            || column.getColor() != null
            || column.getBackground() != null 
            || column.getSource().getCell() != null ) {
                
            ObjectNode decorator = json.createObjectNode();
            decorator.put( "$type", cell.getType() != null ? cell.getType() : "base" );

            if( column.getHeight() != null ) {
                decorator.put( "height", SizeConverter.toCSS( this, column.getHeight() ) );
            }
            if( column.getHalign() != null ) {
                decorator.put( "halign", dumpHorizontalAlignment( column.getHalign() ) );
            }
            if( column.getValign() != null ) {
                decorator.put( "valign", dumpVerticalAlignment( column.getValign() ) );
            }
            if( column.getWrap() != null ) {
                decorator.put( "lwrap", JSON.encode( column.getWrap() ) );
            }

            if( column.getFont() != null ) {
                decorator.put( "font", JSON.encode( FontConverter.toCSS( column.getFont() ) ) );
            }
            if( column.getColor() != null ) {
                decorator.put( "color", JSON.encode( ColorConverter.toCSS( column.getColor() ) ) );
            }
            if( column.getBackground() != null ) {
                decorator.put( "background", JSON.encode( ColorConverter.toCSS( column.getBackground() ) ) );
            }
            
            if( column.getSource().getCell() != null ) {
                decorator.put( "cell", true );
            }

            return decorator;

        } else {
            return null;
        }
    }
    
    protected String dumpVerticalAlignment( GridColumn.VerticalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ) {
            case TOP:
                return "top";
            case MIDDLE:
                return "middle";
            case BOTTOM:
                return "bottom";
            default:
                return null;
            }
        } else {
            return null;
        }
    }

    protected String dumpHorizontalAlignment( GridColumn.HorizontalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ) {
            case LEFT:
                return "left";
            case CENTER:
                return "center";
            case RIGHT:
                return "right";
            default:
                return null;
            }
        } else {
            return null;
        }
    }
    
    public void populateItem( T cell, ObjectNode itemNode, DataSource source, Object item ) {
        Object cellValue;
        try {
            cellValue = getCellValue( cell, source, item );
        } catch( Exception ex ) {
            cellValue = "ERROR: " + ( ex.getMessage() != null ? ex.getMessage() : ex.getClass().getSimpleName() );
        }
        itemNode.set( cell.getName(), JSON.js.encode( cellValue ) );
        
         if( cell.getCell() != null ) {
            ObjectNode cellNode = json.createObjectNode();

            Decoration decoration = cell.getCell().decorate( item );
            if( decoration != null ) {
                if( decoration.getColor() != null ) {
                    cellNode.put( GridProducer.COLOR_ATTR, JSON.js.encode( ColorConverter.toCSS( decoration.getColor() ) ) );
                }

                if( decoration.getBackground() != null ) {
                    cellNode.put( GridProducer.BACKGROUND_ATTR, JSON.js.encode( ColorConverter.toCSS( decoration.getBackground().getColor() ) ) );
                }

                if( decoration.getFont() != null ) {
                    cellNode.put( GridProducer.FONT_ATTR, JSON.js.encode( FontConverter.toCSS( decoration.getFont() ) ) );
                }

                // StyleArtifact flavor = decoration.getFlavor();
                // if( flavor != null ) {
                //     cellNode.set( CLASS_ATTR, JSON.js.encode( dumpFlavor( flavor ) ) );
                // }
                
                itemNode.set( ".c." + cell.getName(), cellNode );
            }
        }
    }
    
    protected Object getCellValue( T cell, DataSource source, Object item ) {
        return HTML.encode( cell.getFormattedValue( source, item ) );
    }
    
    public String translateSortAttribute( T cell, String name ) {
        if( Strings.hasText( cell.getSort() ) ) {
            return cell.getSort();
        } else if( ExpressionEvaluator.isExpression( cell.getProperty() ) ) {
            return ExpressionEvaluator.parse( cell.getProperty() ).pure();
        } else {
            return cell.getProperty();
        }
    }
    
    
}