package it.neverworks.ui.html.data;

import it.neverworks.ui.data.Artifact;

public class WebStyleArtifactResolver implements ArtifactResolver {
    
    public Object resolveArtifact( Artifact artifact ) {
        return new ClassArtifact().set( "content", ((WebStyleArtifact) artifact).getClassName() );
    }

}