package it.neverworks.ui.html.production;

import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.ui.Widget;
import it.neverworks.ui.Container;

public abstract class BaseContainerProducer<T extends Widget> extends HTMLProducer<T> {
    
    protected boolean containerNeedsRedraw( T widget ) {
        return widget.model().changes().contains( "children" );
    }
    
    public void analyze( T widget ) {
        if( containerNeedsRedraw( widget ) ) {
            analysis.bubble( new WidgetModifiedEvent( widget ) );
        } else {
            analysis.begin( this, widget );
            analyzeWidget( widget );
            analyzeChildren( widget );
            analysis.accept();
        }
    }

    protected void analyzeWidget( T widget ) {
        // TEMPLATE:
        // Changes changes = widget.model().changes();
        // if( changes.size() > 0 ) {
        //     analysis.propose( this, widget, changes.toList() );
        // }
    }
    
    protected void analyzeChildren( T widget ) {
        for( Widget child: ((Container) widget).getChildren() ) {
            inspect( child );
        }
    }
    
}