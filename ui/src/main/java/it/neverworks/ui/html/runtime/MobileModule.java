package it.neverworks.ui.html.runtime;

import net.sf.uadetector.ReadableDeviceCategory;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.OperatingSystemFamily;
import net.sf.uadetector.OperatingSystem;

import it.neverworks.lang.Wrapper;
import it.neverworks.model.BaseModel;
import it.neverworks.ui.runtime.Device;

public class MobileModule extends BaseModel implements Runnable {
    
    private static Wrapper IOS = new Wrapper() {
        public Object asObject() {
            OperatingSystem system = Device.<ReadableUserAgent>fetch( "userAgent" ).getOperatingSystem();

            if( system.getFamily() == OperatingSystemFamily.IOS ) {
                return Integer.parseInt( system.getVersionNumber().getMajor() + system.getVersionNumber().getMinor() );
            } else {
                return null;
            }
        }
    };

    private static Wrapper ANDROID = new Wrapper() {
        public Object asObject() {
            OperatingSystem system = Device.<ReadableUserAgent>fetch( "userAgent" ).getOperatingSystem();

            if( system.getFamily() == OperatingSystemFamily.ANDROID ) {
                return Integer.parseInt( system.getVersionNumber().getMajor() + system.getVersionNumber().getMinor() );
            } else {
                return null;
            }
        }
    };

    private static Wrapper WINDOWS = new Wrapper() {
        public Object asObject() {
            OperatingSystem system = Device.<ReadableUserAgent>fetch( "userAgent" ).getOperatingSystem();

            if( system.getFamily() == OperatingSystemFamily.WINDOWS ) {
                return Integer.parseInt( system.getVersionNumber().getMajor() + system.getVersionNumber().getMinor() );
            } else {
                return null;
            }
        }
    };

    private static Wrapper TABLET = new Wrapper() {
        public Object asObject() {
            return Device.<ReadableUserAgent>fetch( "userAgent" ).getDeviceCategory().getCategory() == ReadableDeviceCategory.Category.TABLET;
        }
    };

    private static Wrapper PHONE = new Wrapper() {
        public Object asObject() {
            return Device.<ReadableUserAgent>fetch( "userAgent" ).getDeviceCategory().getCategory() == ReadableDeviceCategory.Category.SMARTPHONE;
        }
    };

    private static Wrapper MOBILE = new Wrapper() {
        public Object asObject() {
            return Boolean.TRUE.equals( TABLET.asObject() ) || Boolean.TRUE.equals( PHONE.asObject() );
        }
    };

    private static Wrapper IPAD = new Wrapper() {
        public Object asObject() {
            return Boolean.TRUE.equals( TABLET.asObject() ) || Boolean.TRUE.equals( IOS.asObject() );
        }
    };

    private static Wrapper IPHONE = new Wrapper() {
        public Object asObject() {
            return Boolean.TRUE.equals( TABLET.asObject() ) || Boolean.TRUE.equals( IOS.asObject() );
        }
    };

    private static Wrapper TABLOID = new Wrapper() {
        public Object asObject() {
            return Boolean.TRUE.equals( TABLET.asObject() ) || Boolean.TRUE.equals( ANDROID.asObject() );
        }
    };

    private static Wrapper PHONOID = new Wrapper() {
        public Object asObject() {
            return Boolean.TRUE.equals( TABLET.asObject() ) || Boolean.TRUE.equals( ANDROID.asObject() );
        }
    };

    private static Wrapper DESKTOP = new Wrapper() {
        public Object asObject() {
            return Boolean.FALSE.equals( MOBILE.asObject() );
        }
    };
    
    public void run() {
        Device.register( "ios", IOS );
        Device.register( "android", ANDROID );
        Device.register( "windows", WINDOWS );
        Device.register( "tablet", TABLET );
        Device.register( "phone",  PHONE );
        Device.register( "mobile", MOBILE );
        Device.register( "touch", MOBILE );
        Device.register( "ipad", IPAD );
        Device.register( "iphone", IPHONE );
        Device.register( "tabloid", TABLOID );
        Device.register( "phonoid", PHONOID );
        Device.register( "desktop", DESKTOP );
    }

}