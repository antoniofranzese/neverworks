package it.neverworks.ui.html.types;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Objects;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.data.RawFileArtifact;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.data.ArtifactResolver;
import it.neverworks.ui.html.data.HrefArtifact;
import it.neverworks.ui.html.production.HTMLProducer;

public class BackgroundConverter extends BaseConverter {
    
    public static Arguments renderCSS( HTMLProducer producer, Object value ) {

        if( value != null ) {
            Arguments result = new Arguments();
 
            Background background = (Background) value;
        
            if( background.getColor() != null ) {
                result.arg( "background-color", ColorConverter.toCSS( background.getColor() ) );
            }
            
            if( background.getImage() != null ) {
                if( producer != null ) {
                    Object artifact = producer.resolveArtifact( background.getImage() );
                    if( artifact instanceof HrefArtifact || artifact instanceof RawFileArtifact ) {
                        result.arg( "background-image", "url(" + ((RawArtifact) artifact).getContent() + ")" );
                    } else {
                        throw new ProductionException( "Unknown background artifact: " + Objects.repr( artifact ) );
                    }
                
                    String x = SizeConverter.toCSS( background.get( "!position.x" ) );
                    String y = SizeConverter.toCSS( background.get( "!position.y" ) );
                    result.arg( "background-position", ( x != null ? x : "left" ) + " " + ( y != null ? y : "top" ) ); 
                
                    String repeat = null;
                    if( background.getRepeat() != null ) {
                        switch( background.getRepeat() ) {
                            case HORIZONTAL:
                                repeat = "repeat-x";
                                break;
                            case VERTICAL:
                                repeat = "repeat-y";
                                break;
                            case BOTH:
                                repeat = "repeat";
                                break;
                            default:
                                repeat = "no-repeat";
                        }
                    } else {
                        repeat = "no-repeat";
                    }
                    result.arg( "background-repeat", repeat );
                
                    result.arg( "background-attachment", Boolean.TRUE.equals( background.getScrollable() ) ? "scroll" : "fixed" );
                    
                } else {
                    throw new ProductionException( "Cannot render background image without an HTMLProducer" );
                }
            } 

            return result;

        } else {
            return null;
        }
                            
    }

    public static Arguments dumpCSS( HTMLProducer producer, Object value ) {
        Arguments result = renderCSS( producer, value );

        if( result != null ) {
            Background background = (Background) value;

            if( background.getColor() == null ) {
                result.arg( "background-color", "transparent" );
            }

            if( background.getImage() == null ) {
                result.arg( "background-image", "none" );
            }

            return result;

        } else {
            return new Arguments()
                .arg( "background-color", null )
                .arg( "background-image", null );
        }
                            
    }
    
}