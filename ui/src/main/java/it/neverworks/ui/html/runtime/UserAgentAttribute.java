package it.neverworks.ui.html.runtime;

import net.sf.uadetector.service.UADetectorServiceFactory;
import net.sf.uadetector.UserAgentStringParser;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.UserAgent;
import net.sf.uadetector.DeviceCategory;
import net.sf.uadetector.UserAgentFamily;
import net.sf.uadetector.OperatingSystem;
import net.sf.uadetector.OperatingSystemFamily;
import net.sf.uadetector.UserAgentType;
import net.sf.uadetector.VersionNumber;

import it.neverworks.lang.Wrapper;
import it.neverworks.log.Logger;
import it.neverworks.ui.runtime.Device;

public class UserAgentAttribute implements Wrapper {
    private final static Logger logger = Logger.of( UserAgentAttribute.class );
    
    private final static UserAgentStringParser userAgentParser = UADetectorServiceFactory.getResourceModuleParser();
    
    private final static String KEY = "NW3:USER-AGENT:INSTANCE";

    public static Wrapper PARSER = new Wrapper() {
        public Object asObject() {
            String header = Device.get( "header[user-agent]" );
            logger.debug( "User Agent: {}", header );
           
            ReadableUserAgent ua =  userAgentParser.parse( header );
            
            if( ua.getOperatingSystem().getFamily() == OperatingSystemFamily.ANDROID ) {
                
                DeviceCategory deviceCategory = header.indexOf( "Mobile" ) >= 0 
                    ? new DeviceCategory( DeviceCategory.Category.SMARTPHONE, "phone.png", "/list-of-ua/device-detail?device=Smartphone", "Smartphone" )
                    : new DeviceCategory( DeviceCategory.Category.TABLET, "tablet.png", "/list-of-ua/device-detail?device=Tablet", "Tablet" );

                return new UserAgent(
                    deviceCategory
                    ,ua.getFamily()
                    ,ua.getIcon()
                    ,ua.getName()
                    ,ua.getOperatingSystem()
                    ,ua.getProducer()
                    ,ua.getProducerUrl()
                    ,UserAgentType.MOBILE_BROWSER
                    ,"Mobile Browser"
                    ,ua.getUrl()
                    ,ua.getVersionNumber()
                );

            } else {
                return ua;
            }
        }

    };

    public Object asObject() {
        return Device.cache( KEY, PARSER );
    }

}