package it.neverworks.ui.html.canvas;

import java.util.Formatter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.canvas.Shadow;
import it.neverworks.ui.canvas.Fill;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;

public abstract class ShapeProducer<T extends Shape> {

    public ObjectNode dump( T shape ) {
        ObjectNode node = JSON.object();
 
        setPoint( node, "pt", shape.get( "place" ) );
        setInteger( node, "ro", shape.get( "rotation" ) );
        setStroke( node, "st", shape.get( "stroke" ) );
        setFill( node, "fl", shape.get( "fill" ) );
        setShadow( node, "sh", shape.get( "shadow" ) );

        return node;
    }

    protected void setPoint( ObjectNode node, String name, Point point ) {
        if( point != null ) {
            node.set( name, getPoint( point ) );
        }
    }

    protected ArrayNode getPoint( Point point ) {
        ArrayNode pointNode = JSON.array();
        pointNode.add( point.getX() );
        pointNode.add( point.getY() );
        return pointNode;
    }

    protected void setSize( ObjectNode node, String name, Size size ) {
        if( size != null ) {
            node.put( name, size.getValue() );
        }
    }

    protected void setInteger( ObjectNode node, String name, Integer number ) {
        if( number != null ) {
            node.put( name, number );
        }
    }

    protected void setStroke( ObjectNode node, String name, Stroke stroke ) {
        if( stroke != null ) {
            ObjectNode strokeNode = JSON.object();

            if( stroke.getColor() != null ) {
                strokeNode.put( "cl", convertColor( stroke.getColor() ) );
            }
            
            if( stroke.getWidth() != null ) {
                strokeNode.put( "w", stroke.getWidth() );
            }
  
            if( stroke.getCap() != null ) {
                switch( stroke.getCap() ) {
                    case BUTT:
                        strokeNode.put( "cp", "B" );
                        break;
                    case ROUND:
                        strokeNode.put( "cp", "R" );
                        break;
                    case SQUARE:
                        strokeNode.put( "cp", "S" );
                        break;
                }
            }
            
            if( stroke.getJoin() != null ) {
                switch( stroke.getJoin() ) {
                    case ROUND:
                        strokeNode.put( "jn", "R" );
                        break;
                    case BEVEL:
                        strokeNode.put( "jn", "B" );
                        break;
                    case MITER:
                        strokeNode.put( "jn", "M" );
                        break;
                    }
            }

            node.set( name, strokeNode );
        }
    }

    protected void setFill( ObjectNode node, String name, Fill fill ) {
        if( fill != null ) {
            ObjectNode fillNode = JSON.object();
            if( fill.getColor() != null ) {
                fillNode.put( "cl", convertColor( fill.getColor() ) );
            }
            node.set( name, fillNode );
        }
    }

    protected void setShadow( ObjectNode node, String name, Shadow shadow ) {
        if( shadow != null ) {
            ObjectNode shadowNode = JSON.object();
            if( shadow.getColor() != null ) {
                shadowNode.put( "cl", convertColor( shadow.getColor() ) );
            }
            if( shadow.getBlur() != null ) {
                shadowNode.put( "bl", shadow.getBlur() );
            }
            if( shadow.getX() != null ) {
                shadowNode.put( "x", shadow.getX() );
            }
            if( shadow.getY() != null ) {
                shadowNode.put( "y", shadow.getX() );
            }
            node.set( name, shadowNode );
        }
    }

    protected String convertColor( Color color ) {
        if( color.getAlpha() == Color.SOLID ) {
            return new Formatter().format( "#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue() ).toString();
        } else {
            return new Formatter().format( "rgba(%d,%d,%d,%1.2f)", color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha() / 255.0f ).toString();
        }
    }

}