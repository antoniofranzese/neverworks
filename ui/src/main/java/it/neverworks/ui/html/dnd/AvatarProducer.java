package it.neverworks.ui.html.dnd;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.HTML;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.dnd.ProposalConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.Image;
import it.neverworks.ui.dnd.Avatar;

public class AvatarProducer extends HTMLProducer<Avatar> {
    
    public void render( Avatar widget ) {

        output.dojo.start( "div" )
            .attr( "type", "works/dnd/StyledAvatar" )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "enjoyClass", dumpStyle( widget.getEnjoy() ) )
            .attr( "neglectClass", dumpStyle( widget.getNeglect() ) )
            .attr( "ignoreClass", dumpStyle( widget.getIgnore() ) )
            .attr( "contentWidth", widget.getWidth() )
            .attr( "contentHeight", widget.getHeight() );
        
        output.end();
    }

    protected Object dumpStyle( Decoration decoration ) {
        if( decoration != null && decoration.getStyle() != null ) {

            Object resolved = resolveArtifact( decoration.getStyle() );
            if( resolved instanceof RawArtifact ) {
                return ((RawArtifact) resolved).getContent();
            } else {
                return resolved;
            }
            
        } else {
            return null;
        }
    }

    protected ObjectNode dumpProperty( Avatar widget, String name ) {
        if( "enjoy".equals( name ) ) {
            return json.property( "enjoyClass", dumpStyle( widget.getEnjoy() ) );
        } else if( "neglect".equals( name ) ) {
            return json.property( "neglectClass", dumpStyle( widget.getNeglect() ) );
        } else if( "ignore".equals( name ) ) {
            return json.property( "ignoreClass", dumpStyle( widget.getIgnore() ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
}