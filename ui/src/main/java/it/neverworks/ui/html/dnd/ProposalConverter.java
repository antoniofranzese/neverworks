package it.neverworks.ui.html.dnd;

import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.encoding.JSON;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.io.State;
import it.neverworks.model.io.StateIO;
import it.neverworks.model.io.MapState;
import it.neverworks.ui.dnd.DropSource;
import it.neverworks.ui.dnd.ItemDropSource;
import it.neverworks.ui.production.ProductionException;

public class ProposalConverter {
    
    // public static JsonNode toJSON( ItemDropSource source, Object key ) {
    //     Object proposal = source.getItemProposal( key );
    //     if( proposal != null ) {
    //         return JSON.js.encode(
    //             arg( "item", key )
    //             .arg( "content", toJSON( proposal ) )
    //         );
    //     } else {
    //         return null;
    //     }
    // }
    
    // public static JsonNode toJSON( DropSource source ) {
    //     Object proposal = source.getProposal();
    //     if( source.getProposal() != null ) {
    //         return JSON.js.encode(
    //             arg( "source", source.getPath() )
    //             .arg( "content", toJSON( proposal ) )
    //         );
    //     } else {
    //         return null;
    //     }
    // }
    
    public static JsonNode toJSON( Object value ) {
        State proposal = null;

        if( value != null ) {
            if( value instanceof Map ) {
                proposal = new MapState( (Map) value );

            } else if( value instanceof String || value instanceof Number || value instanceof Boolean ) {
                proposal = new MapState( arg( "value", value ) );

            } else if( value instanceof Modelized ){
                proposal = StateIO.extract( value );

            } else {
                throw new ProductionException( "Cannot serialize proposal: " + Objects.repr( value ) );
            }
            
            return JSON.js.encode( proposal );
            
        } else {
            return null;
        }

    }
    
}