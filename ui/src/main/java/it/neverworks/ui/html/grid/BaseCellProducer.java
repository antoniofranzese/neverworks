package it.neverworks.ui.html.grid;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.grid.Cell;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.CSSValueConverter;

public abstract class BaseCellProducer<T extends Cell> extends HTMLProducer<T> implements CellProducer<T> {
    
    public void render( T widget ) {
        throw new RuntimeException( "CellProducer cannot be used to render to output" );
    }
    
    public ObjectNode dumpColumn( T cell ) {
        ObjectNode columnNode = json.createObjectNode();
        String titleValue = cell.get( "!column.title.value" );
        columnNode.put( "name", JSON.js.encode( Strings.hasText( titleValue ) ? titleValue : "&nbsp;" ) );
        columnNode.put( "field", cell.getName() );
        columnNode.put( "sortable", cell.getColumn().getSortable() );
        columnNode.put( "lock", cell.getColumn().getLock() );

        if( cell.get( "column.title" ) != null ) {
            Label title = cell.get( "column.title" );
            Arguments style = new Arguments()
                .merge( FontConverter.renderCSS( this, title.get( "font" ) ) )
                .merge( title.get( "color" ) != null ? new Arguments().arg( "color", ColorConverter.toCSS( title.get( "color" ) ) ) : null )
                .merge( title.get( "background" ) != null ? new Arguments().arg( "background-color", ColorConverter.toCSS( title.get( "background" ) ) ) : null );

            if( style.size() > 0 ) {
                columnNode.put( "titleStyle", JSON.js.encode( style ) );
            }
        }
        
        String cssWidth = CSSValueConverter.convert( cell.getColumn().getWidth() );
        if( cssWidth != null ) {
            columnNode.put( "width", cssWidth );
        }
        
        return columnNode;
    }
    
    public boolean recognizeAttribute( T cell, String name ) {
        return name.equals( cell.getName() );
    }
    
}