package it.neverworks.ui.html.tree;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.HTML;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.tree.RowProcessor;
import it.neverworks.ui.tree.PropertyProcessor;
import it.neverworks.ui.tree.AvatarProcessor;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.dnd.Avatar;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.dnd.ProposalConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.layout.BorderLayoutProducer;

public class TreeProducer extends HTMLProducer<Tree> {
    
    private final static String ROOT = "__ROOT__";
    private final static String ROW_ATTR = "row";
    private final static String CLASS_ATTR = "cs";
    private final static String COLOR_ATTR = "cl";
    private final static String BACKGROUND_ATTR = "bk";
    private final static String FONT_ATTR = "ft";
    
    public void render( Tree widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/tree/FlatTree" )
            .attr( "name", widget.getName())
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "selectedKey", widget.getSelectedKey() )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "showRoot", widget.getShowStem() != null ? widget.getShowStem() : ( widget.getStem() != null ? true : false ) )
            .attr( "autoExpand", widget.getExpanded() )
            .attr( switchEvent( widget.getClick() ) )
            .attr( "drags", widget.getAvatar() != null && Strings.hasText( widget.getProposal() ) ? true : null )
            .attr( "avatar", widget.get( "!avatar.default.path" ) )
            .attr( "disabled", widget.get( "disabled" ) )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() );
        
        ObjectNode props = json.object();
        props.set( "startupItems", dumpItems( widget ) );
        
        if( widget.getIcons() != null ) {
            ObjectNode icons = json.object();
            for( Object key: widget.getIcons().keySet() ) {
                ObjectNode def = json.object();
                def.set( "on", JSON.encode( resolveArtifact( widget.getIcons().get( key ).getOpened() ) ) );
                def.set( "off", JSON.encode( resolveArtifact( widget.getIcons().get( key ).getClosed() ) ) );
                icons.set( key.toString(), def );
            }
            props.set( "icons", icons );
        }
            
        output.start( "script" ).attr( "type", "works/props" );
        output.text( props.toString() );
        output.end( "script" );
        
        output.end( "div" );    
    }
    
    boolean itemsDumpPerformed = false;
    
    protected ObjectNode dumpProperty( Tree widget, String name ) {
        if( "items".equals( name ) || "stem".equals( name ) ) {
            if( !itemsDumpPerformed ) {
                itemsDumpPerformed = true;
                return json.property( "items", dumpItems( widget ) );
            } else {
                return json.object();
            }
        } else if( "selectedKey".equals( name ) ) {
            return json.property( "selectedKey", Strings.valueOf( widget.getSelectedKey() ) );
        } else if( "rows".equals( name ) ) {
            return null;
        } else {
            return super.dumpProperty( widget, name );
        }
    }    
    
    protected ArrayNode dumpItems( Tree widget ) {
        ArrayNode result = json.array();
        DataSource items = widget.getItems();
        String proposalProp = Strings.hasText( widget.getProposal() ) ? widget.getProposal() : null;
        RowProcessor rowFormat = widget.getRows();
        boolean html = ( widget.getEncoding() == Tree.Encoding.HTML );
        PropertyProcessor caption = widget.getCaption();
        AvatarProcessor avatar = widget.getAvatar();

        Object actualRootID;
        ObjectNode rootNode = json.object();
        rootNode.put( "id", ROOT );
        if( widget.getStem() == null ) {
            rootNode.put( "cap", "ROOT" );
            actualRootID = ROOT;
        } else {
            Object rootCaption = caption.process( widget.getStem() );
            rootNode.set( "cap", JSON.encode( html ? Strings.valueOf( rootCaption ) : HTML.encode( rootCaption ) ) );
            actualRootID = readProperty( items, widget.getStem(), widget.getKey() );
            if( proposalProp != null ) {
                Object proposal = readProperty( items, widget.getStem(), proposalProp );
                if( proposal != null ) {
                    rootNode.set( "prp", ProposalConverter.toJSON( proposal ) );
                }
            }
        }
        result.add( rootNode );
        
        for( Object item: items ) {
            if( item != widget.getStem() ) {
                ObjectNode itemNode = json.object();
                itemNode.set( "id", JSON.encode( Strings.valueOf( readProperty( items, item, widget.getKey() ) ) ) );

                Object itemCaption = caption.process( item ); 
                itemNode.set( "cap", JSON.encode( html ? Strings.valueOf( itemCaption ) : HTML.encode( itemCaption ) ) );

                if( Strings.hasText( widget.getLink() ) ) {
                    String parent = Strings.valueOf( readProperty( items, item, widget.getLink() ) );
                    itemNode.set( "par", JSON.encode( parent == null || parent.equals( actualRootID ) ? ROOT : parent ) );
                }
                
                if( Strings.hasText( widget.getIcon() ) ) {
                    Object icon = readProperty( items, item, widget.getIcon() );
                    itemNode.set( "icn", icon != null ? JSON.encode( Strings.valueOf( icon ) ) : null );
                }

                if( proposalProp != null ) {
                    Object proposal = readProperty( items, item, proposalProp );
                    if( proposal != null ) {
                        itemNode.set( "prp", ProposalConverter.toJSON( proposal ) );
                        if( avatar != null ) {
                            Avatar avatarWidget = avatar.process( item );
                            if( avatarWidget != null ) {
                                itemNode.set( "avt", JSON.encode( avatarWidget.get( "path" ) ) );
                            }
                        }
                    }
                }

                if( rowFormat != null ) {
                    Decoration rowStyle = rowFormat.decorate( item );
                    if( rowStyle != null && ! rowStyle.isEmpty() ) {
                        ObjectNode rowNode = json.createObjectNode();

                        if( rowStyle.getColor() != null ) {
                            rowNode.put( COLOR_ATTR, JSON.js.encode( ColorConverter.toCSS( rowStyle.getColor() ) ) );
                        }

                        if( rowStyle.getBackground() != null ) {
                            rowNode.put( BACKGROUND_ATTR, JSON.js.encode( ColorConverter.toCSS( rowStyle.getBackground().getColor() ) ) );
                        }

                        if( rowStyle.getFont() != null ) {
                            rowNode.put( FONT_ATTR, JSON.js.encode( FontConverter.dumpCSS( this, rowStyle.getFont() ) ) );
                        }

                        Artifact flavor = rowStyle.getFlavor();
                        if( flavor != null ) {
                            rowNode.put( CLASS_ATTR, JSON.js.encode( dumpFlavor( flavor ) ) );
                        }

                        itemNode.put( ROW_ATTR, rowNode );

                    }
                
                }

                result.add( itemNode );
            }
        }
        
        return result;
    }    
    
    protected Object readProperty( DataSource items, Object item, String key ) {
        try {
            return items.get( item, key );
        } catch( Exception ex ) {
            return null;
        }
    }
    
    public void analyze( Tree widget ) {
        Changes changes = widget.model().changes();

        if( changes.contains( "rows" ) ) {
            changes.add( "items" );
            changes.remove( "rows" );
        }

        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }
    }
    
}