package it.neverworks.ui.html.form.ribbon;

import it.neverworks.ui.form.ribbon.RibbonStrip;
import it.neverworks.ui.html.production.BaseContainerProducer;
import it.neverworks.ui.Widget;

public class RibbonStripProducer extends BaseContainerProducer<RibbonStrip> {
    
    public void render( RibbonStrip widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/layout/RibbonStrip" )
            .attr( "title", widget.getTitle() )
            .attr( "orientation", "horizontal" )
            .attr( "class", "ribbonStrip");

        output.dojo.start( "div" )
            .attr( "type", "works/layout/ContentPane" )
            .attr( "class", "ribbonStripContent" );
        
        output.start( "table" ).attr( "class", "ribbonstrip" );
        output.start( "tr" );
        
        for( Widget child: widget.getChildren() ) {
            output.start( "td" ).attr( "valign", "top" );
            produce( child );
            output.end();
        }
        
        output.end( "tr" );
        output.end( "table" );
    
        output.end( "div" );
        output.end( "div" );
        
    }

}