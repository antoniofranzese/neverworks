package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Scrollability;
import it.neverworks.ui.Widget;

public class ScrollabilityConverter extends BaseConverter {
    
    public static String toValue( Object value ) {
        if( value != null ) {
            switch( (Scrollability) value ) {
                case HORIZONTAL:
                    return "horizontal";
                case VERTICAL:
                    return "vertical";
                case BOTH:
                    return "both";
                default:
                    return null;
            }
        } else {
            return null;
        }
    }
    
}