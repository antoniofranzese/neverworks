package it.neverworks.ui.html.production.storage;

import org.springframework.beans.factory.DisposableBean;

import it.neverworks.lang.Strings;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.ui.lifecycle.Destroyable;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.MissingStoredWidgetException;
import it.neverworks.ui.Widget;

public class LimitedStorage<T extends Widget> extends BaseStorage<T> implements DisposableBean {

    protected WidgetStorage<T> widgets = new MemoryStorage<>( 0 );
    protected FluentList<String> lru = new FluentArrayList<>();
    protected int limit = 5;
    
    public boolean contains( String key ) {
        return lru.contains( key ) && widgets.contains( key );
    }
    
    public T load( String key ) {
        if( lru.contains( key ) ) {
            return widgets.load( key );
        } else {
            throw new MissingStoredWidgetException( "Widget with key '" + key + "' is not stored" );
        }
    }
    
    protected Iterable<T> all() {
        return lru
            .filter( k -> widgets.contains( k ) )
            .map( k -> widgets.load( k ) );
    }

    public void save( String key, T widget ) {
        int index = lru.indexOf( key );
        if( index != 0 ) {
            if( index > 0 ) {
                lru.remove( key );
            }
            lru.add( 0, key );
        }

        if( limit > 0 && lru.size() > limit ) {
            for( int i = lru.size() - 1; i >= limit; i-- ) {
                //System.out.println( "Deleting " + lru.get( i ) );
                widgets.delete( lru.get( i ) );
                lru.remove( i );
            }
        }  

        widgets.save( key, widget );
    }
    
    public void delete( String key ) {
        //System.out.println( "Limited deleting " + key );
        widgets.delete( key );
    }

    public void setLimit( int limit ){
        this.limit = limit;
    }
    
    public void destroy() {
        //System.out.println( "Limited Destroying" );
        lru.inspect( k -> delete( k ) );
        widgets = null;
        lru = null;
    }
    
    public void setStorage( WidgetStorage<T> storage ){
        this.widgets = storage;
    }

}