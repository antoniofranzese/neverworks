package it.neverworks.ui.html.production.storage;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import it.neverworks.lang.Strings;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.Widget;

public abstract class BaseStorage<T extends Widget> implements WidgetStorage<T> {

    public String reserve( T widget ) {
        String key = null;
        do {
            key = Strings.generateHexID( 8 ) + "-"
                + Strings.generateHexID( 8 ) + "-"
                + Strings.generateHexID( 8 ) + "-"
                + Strings.generateHexID( 8 ) + "-"
                + Strings.generateHexID( 8 ) + "-"
                + Strings.generateHexID( 8 );
        } while( contains( key ) );

        return key;
    }

    protected abstract Iterable<T> all();
    
    public Iterator<T> iterator() {
        return all().iterator();
    }   

    public ObjectQuery<T> query() {
        return new ObjectQuery<T>( all() );
    }


}