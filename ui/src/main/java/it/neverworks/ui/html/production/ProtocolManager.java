package it.neverworks.ui.html.production;

import java.io.Writer;
import java.io.StringReader;
import java.io.PrintWriter;
import java.io.OutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.Collection;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Recipe;
import it.neverworks.lang.Tuple;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Framework;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Application;
import it.neverworks.lang.Collections;
import it.neverworks.io.StringWriter;
import it.neverworks.io.Streams;
import it.neverworks.log.Logger;
import it.neverworks.ui.Widget;
import it.neverworks.ui.Evented;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.MainForm;
import it.neverworks.ui.form.IntakeMetadata;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.Context;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.production.MissingStoredWidgetException;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.ErrorEvent;
import it.neverworks.ui.lifecycle.ErrorHandler;
import it.neverworks.ui.lifecycle.UIEvent;
import it.neverworks.ui.html.runtime.PublicFormFilter;
import it.neverworks.ui.html.runtime.UserFormFilter;
import it.neverworks.model.events.Event;
import it.neverworks.model.description.Changes;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.io.State;
import it.neverworks.encoding.HTTP;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.URL;
import it.neverworks.log.Logger;

public class ProtocolManager {
    
    private final static Logger logger = Logger.of( ProtocolManager.class );
    
    private HTMLProducerFactory producerFactory;
    private WidgetStorage<Form> widgetStorage;
    private ObjectMapper json = new ObjectMapper();
    private boolean reloadable = false;
    
    private PollingStrategy pollingStrategy = new DisabledPollingStrategy(); 
    private int pollingInterval = 30000;
    private int pollingRate = 500;
    
    private Recipe filter;
    protected int keepAliveInterval = 300000;

    public ProtocolManager() {
        
    }

    public void probe( Context context ) {
        logger.debug( "Management request for {0.FormPath}", context );
        Form form = null;
        
        String method = context.get( "Method" );
        String key = Strings.safe( context.<String>get( "FormPath" ) ).trim();

        // System.out.println( "Method: " + context.<Object>get( "Method" ) );
        // System.out.println( "Parameters: " + context.<Object>get( "Parameters" ) );
        
        if( "KEEP".equals( method ) ) {
            try {
                logger.debug( "[{0}] Keeping form alive", key );
                form = (Form)widgetStorage.load( key );

                context.put( "Return", arg( "Next", this.keepAliveInterval ) );

            } catch( Exception ex ) {
                context.put( "Return", arg( "Next", 0 ) );
           
            }             
 
        } else if( "PEEK".equals( method ) ) {
            Arguments response = new Arguments().arg( "timestamp", new Date().getTime() );
            context.put( "Return", response );

            try {
                logger.debug( "[{0}] Peeking form", key );
                form = (Form)widgetStorage.load( key );

                response.put( "queue", form.hasQueue() ? "true" : "false" );

            } catch( MissingStoredWidgetException ex ) {

                logger.error( "[{0}] Form not found", key );
                    
                response.put( "queue", "false" );
                response.put( "polling", "false" );
                
            } catch( Exception ex ) {
                Exception unwrapped = Errors.unwrap( ex );
                logger.error( "[{0}] Polling error: {1,error}", key, unwrapped );
                    
                if( logger.debug() ) {
                    logger.error( unwrapped );
                }
                
                response.put( "error", unwrapped );
                response.put( "queue", "false" );
            }

        } else if( "POLL".equals( method ) ) {

            Arguments response = new Arguments().arg( "timestamp", new Date().getTime() );
            context.put( "Return", response );

            try {
                logger.debug( "[{0}] Polling form", key );
                form = (Form)widgetStorage.load( key );
                
                // Gap rectifier
                if( form.get( "!production.polling.last" ) != null ) {
                    Long elapsed = System.currentTimeMillis() - form.<Long>get( "production.polling.last" );
                    int gap = form.get( "production.polling.gap" );
                    if( elapsed < gap ) {
                        sleep( gap - elapsed );
                    }
                }
                
                boolean queueing = form.hasQueue();

                PollingParameters params = this.pollingStrategy.analyzePolling( context.get( "Parameters" ) );
                
                if( params.getEnabled() ) {

                    if( params.getCycle() > 0 ) {
                        if( ! queueing ) {
                            Object queue = form.getQueueMonitor();
                            synchronized( queue ) {
                                try {
                                    queue.wait( params.getCycle() );
                                } catch( InterruptedException ex ) {
                                    System.out.println( "INTERRUPTED" );
                                }
                            }

                        }
                    }
                    
                    response.put( "gap", params.getGap() );
                    response.put( "cycle", params.getCycle() );
                    response.put( "queue", form.hasQueue() ? "true" : "false" );

                } else {
                    response.put( "queue", "false" );
                    response.put( "polling", "false" );
                }

                form.set( "production.polling",
                    arg( "last", System.currentTimeMillis() ) 
                    .arg( "gap", params.getGap() )       
                    .arg( "enabled", params.getEnabled() )
                );

            } catch( MissingStoredWidgetException ex ) {

                logger.error( "[{0}] Form not found", key );
                    
                response.put( "queue", "false" );
                response.put( "polling", "false" );
                if( form != null ) {
                    form.set( "production.polling", null );
                }
                
            } catch( Exception ex ) {
                Exception unwrapped = Errors.unwrap( ex );
                logger.error( "[{0}] Polling error: {1,error}", key, unwrapped );
                    
                if( logger.debug() ) {
                    logger.error( unwrapped );
                }
                
                response.put( "error", unwrapped );
                response.put( "queue", "false" );
                if( form != null ) {
                    form.set( "production.polling", null );
                }
            }

        }

        
    }
    
    public void create( Context context ) throws Exception {
        logger.debug( "Creation request for {0.FormPath}", context );
            
        Arguments parameters = HTTP.queryArguments( context.<String>get( "QueryString" ) );
        context.set( "QueryParameters", parameters );

        boolean reloadable = this.reloadable && !( parameters.containsKey( "NORELOAD" ) );
        String formPath = context.<String>get( "FormPath" );
        int index = formPath.indexOf( "@" );
        
        String className;
        String key = null;

        if( index >= 0 ) {
            className = formPath.substring( 0, index ).replaceAll( "/", "." );
            key = formPath.substring( index + 1 );
        } else {
            className = formPath.replaceAll( "/", "." );
        }

        if( key != null ) {
            logger.debug( "Recreating form {}", key );
        } else {
            logger.debug( "Creating {} form", className );
        }

        Class<? extends Form> formClass = Reflection.findClass( className );

        if( formClass == null ) {
            logger.trace( "Cannot find class {}, trying package", className );
            Package formPackage = Reflection.findPackage( className );
            if( formPackage != null ) {
                MainForm packageMainForm = formPackage.getAnnotation( MainForm.class );
                if( packageMainForm != null ) {
                    formClass = packageMainForm.value();
                    logger.trace( "Found {0.name} as main class for {1}", formClass, className );                
                } else {
                    logger.trace( "Missing {} package main class", className );                
                }
            } else {
                logger.trace( "Cannot find class {} package", className );                
            }
        } else {
            logger.trace( "Found {0.name} class", formClass );                
        }

        if( formClass != null && filter != null ) {
            formClass = filter.apply( formClass );
            if( formClass == null ) {
                logger.trace( "Class {0.name} was filtered out", formClass );                
            }
        }

        if( formClass != null ) {
        

            Form form = null;
            
            if( reloadable ) {
                if( key != null ) {
                    try {
                        form = (Form)widgetStorage.load( key );
                        // if( !( form.getClass().equals( formClass ) ) ) {
                        //     throw new Exception( "Class mismatch" );
                        // }
                    } catch( Exception ex ) {
                        form = null;
                        key = null;
                    }
                }
            } else {
                key = null;
            }
            
            if( form == null ) {
                logger.trace( "Creating new {0.name} instance", formClass );                
                form = (Form)Reflection.newInstance( formClass );
            
                //Intakes

                Map<String, IntakeMetadata> intakes = parseFormIntakes( formClass );
                if( intakes.size() > 0 ) {

                    logger.trace( "Using query parameters: {}", parameters );                
                    
                    for( String property: intakes.keySet() ) {
                        Object value = undefined;
                        Object source = null;
                        Iterator<String> names = intakes.get( property ).getNames().iterator();
                        while( undefined( value ) && names.hasNext() ) {
                            String name = names.next();
                        
                            if( parameters.containsKey( name ) ) {
                                if( Strings.plane( parameters.get( name ) ) == null ) {
                                    if( intakes.get( property ).isNullable() ) {
                                        value = null;
                                        source = "empty query parameter";
                                    }
                                } else {
                                    value = Strings.safe( parameters.get( name ) ).trim();
                                    source = "query parameter";
                                }
                            }
                            
                            if( undefined( value ) ) {
                                if( Application.getProperty( name ) != null ) {
                                    value = Application.getProperty( name );
                                    source = "application property";
                            
                                } else if( System.getProperty( name ) != null ) {
                                    value = System.getProperty( name );
                                    source = "system property";
                                }
                            }
                        
                            if( ! undefined( value ) ) {
                                logger.trace( "Property ''{0}'' populated by ''{1}'' {3} intake: {2}", property, name, value, source );                
                                form.set( property, value );
                            }
                        }
                    }
                
                }
            }

            
            boolean redirect = ( reloadable && key == null );

            if( key == null ) {
                key = widgetStorage.reserve( form );
                logger.trace( "New storage key: {}", key );
            }

            if( redirect ) {
                String url = context.get( "RequestURL" );
                if( url.indexOf( "@" ) >= 0 ) {
                    url = url.substring( 0, url.indexOf( "@" ) );
                }

                logger.trace( "Redirecting to {0}@{1}", url, key );
                writeOutput( context, "<http><head><meta http-equiv=\"refresh\" content=\"0;url=" + url + "@" + key + "\" /></head></http>" );
                
            } else {
                logger.trace( "[{0}] Activating form", key );
                
                context.set( "FormProducer/Root", form );
                form.activate();

                context.set( "FormKey", key );
                context.set( "EndpointURL", context.<String>get( "BaseURL" ) + "/" + key );
                context.set( "Framework", Framework.INSTANCE );

                Page page = new Page().set( "form", form );

                logger.trace( "[{0}] Producing form", key );

                StringWriter writer = new StringWriter( 16384 );
                try {
                    producerFactory.with( context, new HTMLOutput( writer ) ).produce( page );
                    String buffer = writer.toString();
                    writeOutput( context, writer.toString() );

                } catch( Exception ex ) {
                    throw new ProductionException( "Error producing " + className + " form:" + ex.getMessage(), ex );
                }

                logger.trace( "[{0}] Passivating form", key );
                form.passivate();
            
                // La root form nel context potrebbe essere stata sostituita da un producer
                Form current = form;
                form = context.<Form>get( "FormProducer/Root" );
                if( form != current ) {
                    logger.trace( "[{0}] Root form was replaced by {1.class.name} instance", key, form );
                }

            }

            logger.trace( "[{0}] Storing form", key );
            widgetStorage.save( key, form );

            logger.trace( "[{0}] Successful creation request", key );

        } else {
            throw new ProductionException( "Cannot find form: " + className );
        }
    }

    public void handle( Context context ) throws Exception {
        logger.debug( "Handling request for {0.FormPath}", context );
        
        ObjectNode result = json.createObjectNode();
        String key = context.<String>get( "FormPath" );
        
        try {
            JsonNode body = json.readTree( new StringReader( context.<String>get( "Body" ) ) );

            // Caricamento form
            logger.trace( "[{0}] Loading form", key );
            Form form;

            try {
                form = (Form)widgetStorage.load( key );
                logger.trace( "[{0}] Loaded {1.class.name} instance", key, form );
            } catch( Exception ex ) {
                logger.error( "[{0}] Error loading form: {1,error}", key, ex );
                throw ex;
            }
            
            // Impostazione context
            context.set( "FormKey", key );
            context.set( "FormProducer/Root", form );
            context.set( "EndpointURL", context.<String>get( "BaseURL" ) + "/" + key );

            // 1: Caricamento state
            if( body.has( "state" ) ) {
                logger.trace( "[{0}] Loading form state", key );
                ((WidgetLifeCycle) form).load( new JSONState( body ) );
            }

            // 2: Attivazione
            logger.trace( "[{0}] Activating form", key );
            form.activate();

            // 3: Gestione evento
            if( body.has( "event" ) ) {
                JsonNode event = body.get( "event" );
                String eventName = event.get( "name" ).asText();
                String eventSource = event.get( "source" ).asText();
 
                logger.debug( "[{0}] Handling {1} event for {2}", key, eventName, eventSource );
                
                if( event.has( "state" ) ) {
                    logger.trace( "[{0}] Watching form", key );
                    form.model().watch();
                    
                    // Widget
                    Evented widget = eventSource.equals( "/" ) ? form : (Evented) form.get( eventSource );
                    
                    // Event
                    Event convertedEvent;
                    State eventState = new JSONState( event.get( "state" ) );
                    
                    if( widget instanceof Widget ) {
                        Collection productionEvents = ((Widget) widget).get( "!production.events" );
                        if( productionEvents != null && productionEvents.contains( eventName ) ) {
                            Producer widgetProducer = producerFactory.with( context, (JSONAnalysis) null ).pick( (Widget)widget );
                            logger.trace( "[{0}] Processing {1} state using {2.class.name} Producer", key, eventName, widgetProducer );
                            convertedEvent = ((HTMLProducer) widgetProducer).convertEvent( (Widget)widget, eventName, eventState );

                        } else {
                            logger.trace( "[{0}] Processing {1} state using {2.class.name} Widget", key, eventName, widget );
                            convertedEvent =  widget.event( eventName ).convertEvent( eventState );

                        }
                    } else {
                        logger.trace( "[{0}] Processing {1} state using {2.class.name} Object", key, eventName, widget );
                        convertedEvent =  widget.event( eventName ).convertEvent( eventState );
                    }
                    
                    if( convertedEvent instanceof UIEvent ) {
                        ((UIEvent) convertedEvent).setForeign( true );
                    }

                    try {
                        logger.trace( "[{0}] Firing {1.class.name}.{2} signal using {3.class.name} instance", key, widget, eventName, convertedEvent );
                        widget.event( eventName ).fire( convertedEvent );
                        
                    } catch( Exception ex ) {
                        Exception unwrapped = Errors.unwrap( ex );
                        
                        if( logger.debug() ) {
                            logger.debug( "[{0}] Event error", key );
                            logger.error( unwrapped );
                        }
                        
                        try {
                            ErrorEvent errorEvent = new ErrorEvent( widget, unwrapped, convertedEvent );
                            if( widget instanceof ErrorHandler ) {
                                logger.debug( "[{0}] Bubbling error to {1.commonName}", key, widget );
                                ((ErrorHandler) widget).notifyError( errorEvent );
                            } else {
                                logger.debug( "[{0}] Bubbling error to root form", key );
                                form.notifyError( errorEvent );
                            }

                        } catch( Exception unmanaged ) {
                            // Serializza l'errore non gestito e prosegue nell'analisi   
                            logger.debug( "[{0}] Dumping unmanaged error", key );
                            ObjectNode error = dumpError( unmanaged );
                            error.set( "info", JSON.encode( arg( "level", "event" ) ) );
                            result.set( "error", error );
                        }
                    }
                
                    // Analizza le variazioni della form
                    logger.trace( "[{0}] Analyzing form", key );
                    JSONAnalysis analysis = new JSONAnalysis( json );
                    producerFactory.with( context, analysis ).inspect( form );
                
                    // Unisce il risultato dell'analisi al risultato finale
                    logger.trace( "[{0}] Dumping form", key );
                    JsonNode analysisResult = analysis.getResult();
                    Iterator<String> names = analysisResult.fieldNames();
                    while( names.hasNext() ) {
                        String name = names.next();
                        // System.out.println( "Merging " + name + " into result" );
                        result.set( name, analysisResult.get( name ) );
                    }
                
                    logger.trace( "[{0}] Ignoring form", key );
                    form.model().ignore();
                
                } else {
                    throw new ProductionException( "No event state" );
                }
        
            } else if( body.has( "command" ) ) {
                JsonNode command = body.get( "command" );
                String commandName = command.get( "name" ).asText();
                String commandTarget = command.get( "target" ).asText();
                
                Widget widget = form.get( commandTarget );
                //TODO: creare un oggetto apposito per i command
                JSONAnalysis analysis = new JSONAnalysis( json );
                JsonNode commandResult = producerFactory.with( context, analysis ).execute( widget, commandName, command.has( "params" ) ? command.get( "params" ) : null );
                
                if( commandResult != null ) {
                    mixin( result, commandResult );
                }
                
            } else {
                throw new ProductionException( "Nothing to do" );
            }

            logger.trace( "[{0}] Passivating form", key );
            form.passivate();
        
            // La root form nel context potrebbe essere stata sostituita da un producer
            Form current = form;
            form = context.<Form>get( "FormProducer/Root" );
            
            if( form != current ) {
                logger.trace( "[{0}] Root form was replaced by {1.class.name} instance", key, form );
            }

            logger.trace( "[{0}] Saving form", key );
            try {
                widgetStorage.save( key, form );
            } catch( Exception ex ) {
                logger.error( "[{0}] Error saving form: {1,error}", key, ex );
            }

            logger.trace( "[{0}] Succesful handling request", key );

        // Errori tecnici residui
        } catch( Exception ex ) {
            if( logger.debug() ) {
                logger.debug( "[{0}] Protocol error", key );
                logger.error( Errors.unwrap( ex ) );
            }
            ObjectNode error = dumpError( ex );
            error.set( "info", JSON.encode( arg( "level", "protocol" ) ) );
            result.set( "error", error );
        }
        
        writeOutput( context, json.writeValueAsString( result ) );
    }
    
    protected void writeOutput( Context context, String buffer ) throws IOException {
        if( context.get( "GZipContent", false ) ) {
            Streams.gzip( buffer, context.<OutputStream>get( "OutputStream" ) );
        } else {
            context.<Writer>get( "OutputWriter" ).write( Strings.raw( buffer ), 0, buffer.length() );                    
        }
    }

    protected ObjectNode dumpError( Exception ex ) {
        Exception unwrapped = Errors.unwrap( ex );
        ObjectNode error = json.createObjectNode();

        error.put( "type", unwrapped.getClass().getName() );
        error.put( "message", Strings.hasText( unwrapped.getMessage() ) ? unwrapped.getMessage() : "Error (" + unwrapped.getClass().getSimpleName() + ")" );
        
        if( logger.debug() ) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter( sw );
            unwrapped.printStackTrace( pw );
            pw.flush();
            error.put( "stack", sw.toString() );
        }
        return error;
    }
    
    protected static void mixin( ObjectNode target, JsonNode source ) {
        Iterator<String> names = source.fieldNames();
        while( names.hasNext() ) {
            String name = names.next();
            target.set( name, source.get( name ) );
        }
    }
    
    protected Map<Class, Map<String, IntakeMetadata>> intakes = new HashMap<>();
    
    protected Map<String, IntakeMetadata> parseFormIntakes( Class<? extends Form> formClass ) {
        if( ! intakes.containsKey( formClass ) ) {
            synchronized( intakes ) {
                if( ! intakes.containsKey( formClass ) ) {
                    Map<String, IntakeMetadata> metadata = ModelDescriptor.of( formClass )
                        .properties()
                        .filter( p -> p.metadata().contains( IntakeMetadata.class ) )
                        .dict( p -> Tuple.of( p.getName(), p.metadata( IntakeMetadata.class ) ) );
                    
                    intakes.put( formClass, metadata );
                }
            }
        } 
        return intakes.get( formClass );
    }
    
    public void setProducerFactory( HTMLProducerFactory factory ){
        this.producerFactory = factory;
    }
    
    public void setWidgetStorage( WidgetStorage<Form> widgetStorage ){
        this.widgetStorage = widgetStorage;
    }
    
    public void setReloadable( boolean reloadable ){
        this.reloadable = reloadable;
    }
    
    public void setKeepAliveInterval( int keepAliveInterval ){
        this.keepAliveInterval = keepAliveInterval;
    }

    public void setPolling( Object polling ){
        if( polling instanceof PollingStrategy ) {
            this.pollingStrategy = (PollingStrategy) polling;
            
        } else if( polling instanceof String ) {
            if( "long".equalsIgnoreCase( (String)polling ) || "true".equalsIgnoreCase( (String)polling ) ) {
                this.pollingStrategy = new LongPollingStrategy();
            } else if( "short".equalsIgnoreCase( (String)polling ) ) {
                this.pollingStrategy = new ShortPollingStrategy();
            } else if( "dev".equalsIgnoreCase( (String)polling ) ) {
                this.pollingStrategy = new DevelopmentLongPollingStrategy();
            } else if( "disabled".equalsIgnoreCase( (String)polling ) || "false".equalsIgnoreCase( (String)polling ) ) {
                this.pollingStrategy = new DisabledPollingStrategy();
            } else {
                throw new IllegalArgumentException( "Invalid polling string: " + str( polling ) );
            }
        } else {
            throw new IllegalArgumentException( "Invalid polling: " + repr( polling ) );
        }
    }
    
    public void setFilter( Object filter ) {
        if( filter instanceof String && ((String) filter).indexOf( "," ) >= 0 ) {
            this.setFilters( Collections.<Object>list( ((String) filter).split( "," ) ) );
        } else {
            this.filter = new Recipe().filter( convertFilter( filter ) );
        }
    }
    
    public void setFilters( List<Object> filters ) {
        this.filter = new Recipe();
        for( Object filter: filters ) {
            this.filter.filter( convertFilter( filter ) );
        }
    }
    
    protected Filter convertFilter( Object filter ) {
        if( filter instanceof Filter ) {
            return (Filter)filter;
        } else if( filter instanceof String ) {
            if( "public".equalsIgnoreCase( (String) filter ) ) {
                return new PublicFormFilter(); 
            } else if( "user".equalsIgnoreCase( (String) filter ) ) {
                return new UserFormFilter(); 
            } else {
                Class filterClass = Reflection.findClass( ((String) filter).trim() );
                if( filterClass != null && Filter.class.isAssignableFrom( filterClass ) ) {
                    return (Filter) Reflection.newInstance( filterClass );
                } else {
                    throw new IllegalArgumentException( "Invalid filter string: " + str( filter ) );
                }
            }
        } else {
            throw new IllegalArgumentException( "Invalid filter: " + repr( filter ) );
        }
    }

}