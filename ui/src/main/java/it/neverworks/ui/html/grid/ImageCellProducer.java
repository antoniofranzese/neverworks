package it.neverworks.ui.html.grid;

import com.fasterxml.jackson.databind.node.ObjectNode;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.grid.ImageCell;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.data.ValueFormatter;
import it.neverworks.ui.data.export.XHTMLWriter;
import it.neverworks.ui.html.data.ClassArtifact;
import it.neverworks.ui.html.data.HrefArtifact;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.production.ProductionException;

public class ImageCellProducer<T extends ImageCell> extends PropertyCellProducer<T> {

    protected Object getCellValue( T cell, DataSource source, Object item ) {
        Object value = cell.getFormattedValue( source, item );

        XHTMLWriter html = new XHTMLWriter();

        html
            .start( "div" )
            .start( "img" );

        if( cell.get( "width" ) != null ) {
            html.attr( "width", SizeConverter.toCSS( cell.get( "width" ) ) );
        }

        if( cell.get( "height" ) != null ) {
            html.attr( "height", SizeConverter.toCSS( cell.get( "height" ) ) );
        }

        if( value instanceof Artifact ) {
            Object resolved = resolveArtifact( (Artifact) value );
            if( resolved instanceof ClassArtifact ) {
                html.attr( "class", ((ClassArtifact) resolved).getContent() );
            
            } else if( resolved instanceof HrefArtifact ) {
                html.attr( "src", ((HrefArtifact) resolved).getContent() );
 
            } else if( resolved instanceof RawArtifact ) {
                html.attr(
                    resolved instanceof StyleArtifact ? "class" : "src"
                    ,((RawArtifact) resolved).getContent()
                );
            } else {
                throw new ProductionException( "Unknown source for " + cell.getClass().getSimpleName() + " cell: " + Objects.repr( value ) );
            }

            return html.toString();

         } else {
            return "";
        }
    }
    
    public ObjectNode dumpColumn( T cell ) {
        ObjectNode columnNode = super.dumpColumn( cell );

        columnNode.put( "clickAttributes", true );
        
        return columnNode;
    }
    
}