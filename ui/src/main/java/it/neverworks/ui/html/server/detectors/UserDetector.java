package it.neverworks.ui.html.server.detectors;

import javax.servlet.http.HttpServletRequest;
import it.neverworks.security.model.AuthenticatedUser;
import it.neverworks.security.context.UserInfo;

public interface UserDetector {

    AuthenticatedUser detect( HttpServletRequest request );

    default void init( String specification ) {
        
    }

    default void authenticate( UserInfo info, AuthenticatedUser user ) {
        info.setAuthenticatedUser( user );
    }
}