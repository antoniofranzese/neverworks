package it.neverworks.ui.html.layout;

import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import static it.neverworks.language.*;
import it.neverworks.encoding.HTML;

import it.neverworks.lang.Strings;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.Widget;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.BaseRedrawableContainerProducer;
import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.WidgetChangeProposal;
import it.neverworks.ui.html.production.CSSValueConverter;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.types.SizeConverter;

import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.layout.PanelCapable;
import it.neverworks.ui.layout.TitleCapable;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.VisibilityCapable;

public class StackLayoutProducer<T extends StackLayout> extends BaseRedrawableContainerProducer<T> {
    
    public void render( T widget ){

        start( widget );
        
        for( Widget child: widget.getChildren() ) {
            renderChild( child );
        }

        output.dojo.end();
    }
    
    protected void renderChild( Widget child ) {
        boolean isPanel = child instanceof PanelCapable;
        if( !isPanel ) {
            output.dojo.start( "div" )
                .attr( "type", "works/layout/ContentPane" )
                .attr( "title", HTML.encode( getChildTitle( child ) ) );
        }

        produce( child );
    
        if( !isPanel ) {
            output.end();
        }
    }
    
    protected Tag start( T widget ) {
        return output.dojo.start( "div" )
            .attr( "type", dojoType() )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "startupSelected", widget.getSelectedIndex() )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "width", SizeConverter.toCSS( this, widget.getWidth() ) )
            .attr( "height", SizeConverter.toCSS( this, widget.getHeight() ) )
            .style( widget.getParent() instanceof Form ? "main" : null )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( dumpFlavor( widget ) );
    }
    
    protected String dojoType() {
        return "works/layout/StackContainer";
    }
    
    protected String getChildTitle( Widget child ) {
        String title = child instanceof TitleCapable ? ((TitleCapable) child ).getTitle() : null;

        if( Strings.isEmpty( title ) ) {
            title = child instanceof LayoutCapable ? child.<String>get( "!layout.title" ) : null;    
        }
        
        if( Strings.isEmpty( title ) ) {
            title = child.getName();
        } 
        return title;
    }
    
    
    protected List<Integer> redrawables;
    
    public void analyze( StackLayout widget ) {
        redrawables = new ArrayList<Integer>();
    
        analysis.begin( this, widget );

        // Own changes
        Changes changes = widget.model().changes();
        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.replace( "selected", "selectedIndex" ).toList() );
        }

        // Exclude new children from inspection
        List<String> creations = new ArrayList<String>();
        if( widget.getModifications() != null ) {
            for( StackLayout.Modification modification: widget.getModifications() ) {
                if( modification instanceof StackLayout.Insertion ) {
                    creations.add( modification.getChild().getName() );
                }
            }
        }
        
        // Children changes
        for( int index = 0; index < widget.getChildren().size(); index++ ) {
            Widget child = widget.getChildren().get( index );
            if( ! creations.contains( child.getName() ) ) {
                redrawRequested = false;
                inspect( child );
                if( redrawRequested ) {
                    redrawables.add( index );
                }
            }
        }

        // Force proposal
        if( changes.size() == 0 && redrawables.size() > 0 ) {
            analysis.propose( this, widget, new ArrayList<String>() );
        }
        analysis.accept();

    }
    
    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        StackLayout widget = (StackLayout)proposal.getWidget();

        //System.out.println( "Dumping " + widget.getCommonName() );
        ArrayNode operations = json.createArrayNode();
        
        for( String property: ((WidgetChangeProposal) proposal).getChanges() ) {

            if( "children".equals( property ) ) {
                if( widget.getModifications() != null ) {
                    for( StackLayout.Modification modification: widget.getModifications() ) {
                        if( modification instanceof StackLayout.Insertion ) {
                            StackLayout.Insertion insertion = (StackLayout.Insertion) modification;
                            Widget child = insertion.getChild();
                            ObjectNode operation = json.createObjectNode();
                            operation.put( "type", "insert" );
                            operation.put( "index", insertion.getIndex() );
                            operation.put( "content", drawChild( child ) );
                            operation.put( "visible", child instanceof VisibilityCapable ? child.<Boolean>get( "visible" ) : Boolean.TRUE );
                            operations.add( operation );
                    
                        } else if( modification instanceof StackLayout.Removal ) {
                            StackLayout.Removal removal = (StackLayout.Removal) modification;
                            ObjectNode operation = json.createObjectNode();
                            operation.put( "type", "remove" );
                            operation.put( "index", removal.getIndex() );
                            operations.add( operation );
                        }
                    }
                }
                
            } else {
                ObjectNode propertyNodes = dumpProperty( (T)widget, property );
                if( propertyNodes != null ) {
                    result.setAll( propertyNodes );
                }
            }
        }

        // Children redrawings
        for( int index: redrawables ) {
            Widget child = widget.getChildren().get( index );
            ObjectNode operation = json.createObjectNode();
            operation.put( "type", "redraw" );
            operation.put( "index", index );
            operation.put( "content", redrawChild( child ) );
            operation.put( "visible", child instanceof VisibilityCapable ? child.<Boolean>get( "visible" ) : Boolean.TRUE );
            operations.add( operation );
        }

        if( operations.size() > 0 ) {
            result.set( "operations", operations );
        }
        return result;
    }
    
    protected String drawChild( Widget widget ) {
        return draw( widget );
    }

    protected String redrawChild( Widget widget ) {
        return redraw( widget );
    }

    private final static String[] SURVIVING_PROPERTIES = new String[]{};
        
    protected String[] getSurvivingProperties() {
        return SURVIVING_PROPERTIES;
    }

}
