package it.neverworks.ui.html.data;

import static it.neverworks.language.*;

import it.neverworks.encoding.MIME;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.FileBadgeArtifact;
import it.neverworks.ui.data.FileInfoArtifact;

public class FileInfoArtifactResolver extends FileBadgeArtifactResolver {
    
    public Object resolveArtifact( Artifact artifact ) {
        return super.resolveArtifact( new FileBadgeArtifact( ((FileInfoArtifact) artifact).getInfo().getBadge() ) );
    }

}