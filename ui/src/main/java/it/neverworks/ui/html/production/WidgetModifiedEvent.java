package it.neverworks.ui.html.production;

import it.neverworks.model.events.Event;

public class WidgetModifiedEvent<T> extends Event<T> {
    
    public WidgetModifiedEvent( T source ) {
        super( source );
    }
    
    public WidgetModifiedEvent( T source, String reason ) {
        super( source, reason );
    }
    
}