package it.neverworks.ui.html.dnd;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.model.io.State;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.dnd.DropEvent;
import it.neverworks.ui.dnd.DropSource;
import it.neverworks.ui.dnd.ItemDropEvent;
import it.neverworks.ui.dnd.ItemDropSource;
import it.neverworks.ui.Widget;

public class DropEventConverter {
    
    private final static TypeDefinition<Integer> IntType = TypeFactory.shared( Integer.class );
    
    public static DropEvent toEvent( Widget widget, State state ) {
        if( state.has( "sender" ) ) {
            DropEvent event = null;
            
            String path = Strings.valueOf( state.get( "sender" ) );
            Widget originator = widget.root().get( path , null );

            //System.out.println( "Origin: " + originator );

            if( originator == null ) {
                throw new ProductionException( "Missing drop originator: " + path );
            }
            
            Object proposal = null;
            
            if( originator instanceof ItemDropSource ) {
                Object key = state.has( "key" ) ? state.get( "key" ) : null;
                
                if( key == null ) {
                    throw new ProductionException( "Null drop originator key: " + path );
                }

                // System.out.println( "Key: " + key );
                proposal = ((ItemDropSource) originator).getDropProposal( key );
                
                event = new ItemDropEvent();
                event.set( "item", ((ItemDropSource) originator).getDropItem( key ) );
                
            } else if( originator instanceof DropSource ) {
                proposal = ((DropSource) originator).getProposal();
                event = new DropEvent();

            } else {
                throw new ProductionException( "Originator is not a drop source: " + path );
            }
            
            if( proposal == null ) {
                throw new ProductionException( "Null proposal from source: " + path );
            }
            
            event.set( "originator", originator );
            event.set( "proposal", proposal );

            if( state.has( "x" ) && state.has( "y" ) ) {
                event.set( "x", IntType.process( state.get( "x" ) ) );
                event.set( "y", IntType.process( state.get( "y" ) ) );
            }

            //System.out.println( "Proposal: " + proposal );
            
            return event;
            
        } else {
            throw new ProductionException( "Empty drop originator" );
        }
        
    }
    
}