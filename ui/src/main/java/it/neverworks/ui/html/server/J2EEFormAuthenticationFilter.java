package it.neverworks.ui.html.server;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;

import it.neverworks.httpd.InjectableFilter;
import it.neverworks.security.context.UserInfo;

public class J2EEFormAuthenticationFilter extends InjectableFilter {
    
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws ServletException, IOException {
        if( ! userInfo.authenticated() ) {
            // System.out.println( "No auth" );
            request.setAttribute( "FormPath", this.loginForm );
        }
        chain.doFilter( request, response );
    }
    
    protected UserInfo userInfo;

    public void setUserInfo( UserInfo userInfo ){
        this.userInfo = userInfo;
    }
    
    private String loginForm = null;
    
    public void setLoginForm( String loginForm ){
        this.loginForm = loginForm.replace( ".", "/" );
    }
}