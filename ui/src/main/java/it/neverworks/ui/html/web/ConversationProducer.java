package it.neverworks.ui.html.web;

import static it.neverworks.language.*;

import java.util.List;
import java.io.IOException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.encoding.json.JSONCodec;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.io.State;
import it.neverworks.io.FileBadge;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.data.FileBadgeArtifact;
import it.neverworks.ui.web.Conversation;
import it.neverworks.ui.web.ConversationMessage;
import it.neverworks.ui.web.ConversationChangeEvent;
import it.neverworks.ui.web.ConversationMessageEvent;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.server.PrivateURL;

public class ConversationProducer extends HTMLProducer<Conversation> {
    
    public void render( Conversation widget ) {
        widget.set( "production.events", list( "change", "message" ) );

        output.dojo.start( "div" )
            .style( "display", "none" );
        
        output.dojo.start( "div" )
            .attr( "type", "works/web/Conversation" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "startupChannel", widget.getChannel() )
            .attr( "startupState", dumpValue( widget ) )
            .attr( switchEvent( widget.getChange() ) );        
            
        output.end();
        output.end();
    
    }
    
    protected ObjectNode dumpProperty( Conversation widget, String name ) {
        if( "value".equals( name ) ) {
            return json.property( "value", dumpValue( widget ) );
        } else if( "messageQueue".equals( name ) ) {
            return json.property( "messages", dumpMessageQueue( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected String dumpValue( Conversation widget ) {
        return encode( widget.getValue() ).toString();
    }
    
    protected ArrayNode dumpMessageQueue( Conversation widget ) {
        ArrayNode messagesNode = json.array();
        for( ConversationMessage msg: (List<ConversationMessage>) widget.get( "messageQueue" ) ) {
            ObjectNode messageNode = json.object();
            messageNode.put( "topic", msg.<String>get( "topic" ) );
            messageNode.put( "value", encode( msg.get( "value" ) ).toString() );
            messagesNode.add( messageNode );
        }
        widget.<List>get( "messageQueue" ).clear();
        return messagesNode;
    }
    
    public Event convertEvent( Conversation widget, String name, State state ) {
        if( "change".equals( name ) ) {
            ConversationChangeEvent event = new ConversationChangeEvent();
            event.set( "source", widget );
            if( state.has( "value" ) ) {
                event.set( "value", JSON.decode( (String) state.get( "value" ) ) );
            }
            
            return event;
            
        } else if( "message".equals( name ) ) {
            String topic = Strings.valueOf( state.get( "topic" ) );
            ConversationMessageEvent event = null;
            if( widget.model().has( topic, Signal.class ) ) {
                event = (ConversationMessageEvent) widget.<Signal>get( topic ).createEvent();
            } else {
                event = new ConversationMessageEvent();
            }
            
            event.set( "source", widget );
            event.set( "topic", topic );
            if( state.has( "value" ) ) {
                event.set( "value", JSON.decode( (String) state.get( "value" ) ) );
            }
            
            return event;

        } else {
            return super.convertEvent( widget, name, state );
        }
    }
    
    private JSONCodec codec;
    
    private JsonNode encode( Object value ) {
        if( this.codec == null ) {
            this.codec = new Codec();
        }
        return this.codec.encode( value );
    }
    
    private class Codec extends JSONCodec {
        public JsonNode encode( Object value ) {
            if( value instanceof RawArtifact ) {
                return encode( ((RawArtifact) value).getContent() );
            } else if( value instanceof Artifact ) {
                return encode( ConversationProducer.this.resolveArtifact( (Artifact) value ) );
            } else if( value instanceof FileBadge ) {
                return encode( ConversationProducer.this.resolveArtifact( new FileBadgeArtifact( (FileBadge) value ) ) );
            } else {
                return super.encode( value );
            }
        }
    }
    
}