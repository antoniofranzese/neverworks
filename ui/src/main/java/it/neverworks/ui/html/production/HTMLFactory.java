package it.neverworks.ui.html.production;

import it.neverworks.ui.production.Context;

public interface HTMLFactory {
    HTMLExecutableProducer with( Context context, HTMLOutput output );
    HTMLExecutableProducer with( Context context, JSONAnalysis analysis );
}