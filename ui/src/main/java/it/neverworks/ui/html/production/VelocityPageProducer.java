package it.neverworks.ui.html.production;

import java.util.List;
import java.util.Map;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.exception.MethodInvocationException;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Framework;
import it.neverworks.lang.Application;
import it.neverworks.context.Context;
import it.neverworks.security.context.UserInfo;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.html.production.page.TranslationAdapter;

public class VelocityPageProducer extends HTMLProducer<Page> {
    
    private static List<String> FORBIDDEN_ENTRIES = list( "Context", "Page", "Translation" );
    
    public void render( Page page ) {
        VelocityContext velocityContext = new VelocityContext();
        velocityContext.put( "Context", context );
        velocityContext.put( "Page", new PageAdapter( this, page ) );
        velocityContext.put( "Translation", new TranslationAdapter() );
        velocityContext.put( "Device", Device.INSTANCE );
        velocityContext.put( "Framework", Framework.INSTANCE );
        velocityContext.put( "Application", Application.INSTANCE );

        if( Context.contains( UserInfo.class ) ) {
            velocityContext.put( "User", Context.get( UserInfo.class ) );
        }

        if( contextEntries != null ) {
            for( String key: contextEntries.keySet() ) {
                if( ! FORBIDDEN_ENTRIES.contains( key ) ) {
                    velocityContext.put( key, contextEntries.get( key ) );
                }
            }
        }

        try {
            template.merge( velocityContext, output.getWriter() );
        } catch( MethodInvocationException ex ) {
            throw Errors.wrap( ex.getCause() );
        }
    }
    
    private Template template;
    
    public void setTemplate( Template template ){
        this.template = template;
    }
    
    private Map<String, Object> contextEntries;
    
    public void setContextEntries( Map<String, Object> contextEntries ){
        this.contextEntries = contextEntries;
    }
    
}