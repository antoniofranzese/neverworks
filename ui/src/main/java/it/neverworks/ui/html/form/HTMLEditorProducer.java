package it.neverworks.ui.html.form;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.JsonNode;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.types.BackgroundConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.HTMLEditor;

public class HTMLEditorProducer<T extends HTMLEditor> extends BaseFormWidgetProducer<T> {
    
    public void render( T widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/form/HTMLEditor" )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            
            .attr( "required", widget.getRequired() ? true : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "disabled", widget.getDisabled() ? true : null )
            .attr( "readonly", widget.getReadonly() ? true : null )

            .attr( "color", ColorConverter.toCSS( widget.getColor() ) )
            .attr( "background", BackgroundConverter.renderCSS( this, widget.getBackground() ) )
            .attr( "hint", widget.getHint() )
            .attr( "problem", dumpProblem( widget.getProblem() ) )
            
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "margin", widget.getMargin() )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( FontConverter.renderCSS( this, widget.getFont() ) )
            .style( dumpFlavor( widget ) );
        
        output.start( "script" ).attr( "type", "works/props" );
        
        output.text( JSON.js.encode(
            arg( "value", widget.getValue() )
            .arg( "customStyles", widget.getStyles() )
            .arg( "customFeatures", widget.getFeatures() )
        ).toString() );
        
        output.end( "script" );
            
        output.dojo.end();
            
    }

}