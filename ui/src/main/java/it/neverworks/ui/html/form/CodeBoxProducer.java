package it.neverworks.ui.html.form;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.JsonNode;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.CodeBox;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.DateTimeZone;

public class CodeBoxProducer<T extends CodeBox> extends BaseTextBoxProducer<T> {
    
    private final static List<String> RAW_LANGUAGES = list( "xml" );
    
    @Override
    public Tag start( T widget ) {
        Tag start = super.start( widget );

        start
            .attr( "language", widget.getLanguage() );
        
        return start;
    }
    
    protected void body( T widget ) {
        output.start( "script" ).attr( "type", "works/props" );
        
        output.text( JSON.js.encode(
            arg( "value", dumpValue( widget ) )
        ).toString() );
        
        output.end( "script" );
        
    }
    
    protected Object value( T widget ) {
        return null;
    }

    protected String dumpValue( T widget ) {
        
        if( RAW_LANGUAGES.contains( Strings.safe( widget.getLanguage() ).trim().toLowerCase() ) ) {
            return widget.getValue();
        } else {
            return HTML.encode( widget.getValue() ).replaceAll( "\n", "<br/>" );
        }
    }

    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "value".equals( name ) ) {
            return json.property( "value", dumpValue( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    @Override
    protected String dojoType( T widget ) {
        return "works/form/CodeBox";
    }

}