package it.neverworks.ui.html.server;

import it.neverworks.lang.Strings;

public class DeviceHelper {
    
    public static int ID_AGE = 31_536_000;
    
    public static String generateID() {
        return Strings.generateAlphaID( 16 ) + "-" + 
               Strings.generateAlphaID( 16 ) + "-" + 
               Strings.generateAlphaID( 16 );
    }
}