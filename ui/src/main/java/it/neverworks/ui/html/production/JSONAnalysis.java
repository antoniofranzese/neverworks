package it.neverworks.ui.html.production;

import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Arguments;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.description.Changes;
import it.neverworks.model.events.Event;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.Widget;

public class JSONAnalysis {
    
    private class ContainerBag {
        private HTMLProducer producer;
        private Widget widget;
        private List<ChangeProposal> proposals;
        private boolean collector;
        
        public ContainerBag( HTMLProducer producer, Widget widget, boolean collector ) {
            this.producer = producer;
            this.widget = widget;
            this.collector = collector;
        }
        
        public void add( ChangeProposal proposal ) {
            if( proposals == null ) {
                proposals = new ArrayList<ChangeProposal>();
            }
            proposals.add( proposal );
        }        

        public void addAll( List<ChangeProposal> proposals ) {
            if( this.proposals == null ) {
                this.proposals = proposals;
            } else {
                this.proposals.addAll( proposals );
            }
        }        
    }
    
    private Stack<ContainerBag> containers = new Stack<ContainerBag>();
    private CollectorChangeProposal rootProposal = null;
    private ObjectMapper json;
    private JsonNode result;
    private JSONMapper jsonMapper;
    
    public JSONAnalysis( ObjectMapper json ) {
        this.json = json;
        this.jsonMapper = new JSONMapper( json );
    }
    
    public void bubble( Event event ) {
        if( containers.size() > 0 ) {
            containers.peek().producer.bubble( event );
        }
    }
    
    public void beginCollector( HTMLProducer producer, Widget widget ) {
        if( containers.size() > 0 ) {
            producer.setParent( containers.peek().producer );
        }
        containers.push( new ContainerBag( producer, widget, true ) );
    }
    
    public void begin( HTMLProducer producer, Widget widget ) {
        if( containers.size() > 0 ) {
            producer.setParent( containers.peek().producer );
        }
        containers.push( new ContainerBag( producer, widget, false ) );
    }
    
    public void accept() {
        ContainerBag cb = containers.pop();
                
        if( containers.size() > 0 ) {
            if( cb.collector || cb.proposals != null ) {
                if( cb.collector ) {
                    // Colleziona le proposte di cambiamento in un pacchetto che le terra' separate da quelle
                    // del collector (form) superiore
                    // System.out.println( "Collecting proposals from " + cb.widget.getCommonName() + " to " + containers.peek().widget.getCommonName() );
                    containers.peek().add( new CollectorChangeProposal( cb.producer, cb.widget, cb.proposals != null ? cb.proposals : new ArrayList<ChangeProposal>() ) );
                
                } else {
                    // Unisce le proposte di cambiamento nel contenitore superiore
                    // System.out.println( "Merging proposals from " + cb.widget.getCommonName() + " to " + containers.peek().widget.getCommonName() );
                    containers.peek().addAll( cb.proposals );
                }
            }
                
        } else {
            // All'accettazione dell'ultima proposta, questa diventa root
            //System.out.println( "Storing root proposal on accept for " + cb.widget.getCommonName() );
            rootProposal = new CollectorChangeProposal( cb.producer, cb.widget, cb.proposals );

        }
        
    }
    
    public void reject() {
        // Il container ha altre operazioni, scarta le proposte di cambiamento
        ContainerBag cb = containers.pop();
        
        // Allo scarto dell'ultima proposta, questa diventa root, ma svuotata delle proposte
        if( containers.size() == 0 ) {
            // System.out.println( "Storing root proposal on reject for " + cb.widget.getCommonName() );
            rootProposal = new CollectorChangeProposal( cb.producer, cb.widget, new ArrayList<ChangeProposal>() );
        }
    }
    
    public void proposeCollector( HTMLProducer producer, Widget widget ) {
        containers.peek().add( new CollectorChangeProposal( producer, widget, new ArrayList<ChangeProposal>() ) );
    }

    public void propose( HTMLProducer producer, Widget widget ) {
        // Proposta di produzione per una coppia widget/producer che gestira' personalmente le variazioni
        propose( producer, widget, new ArrayList(), null );
    }

    public void propose( HTMLProducer producer, Widget widget, Arguments attributes ) {
        // Proposta di produzione per una coppia widget/producer che gestira' personalmente le variazioni
        propose( producer, widget, new ArrayList(), attributes );
    }

    public void propose( HTMLProducer producer, Widget widget, List<String> changes ) {
        propose( producer, widget, changes, null );
    }

    public void propose( HTMLProducer producer, Widget widget, List<String> changes, Arguments attributes ) {
        // System.out.println( "Proposing " + producer + " " + widget + " " + changes );
        // Un widget ha rilevato dei cambiamenti e li propone per la produzione
        WidgetChangeProposal proposal = new WidgetChangeProposal( producer, widget, changes );
        if( attributes != null ) {
            proposal.setAttributes( new ExpandoModel( attributes ) );
        }
        containers.peek().add( proposal );
    }
 
    public int size() {
        return containers.size();
    }
    
    public ChangeProposal getRootProposal() {
        if( rootProposal != null ) {
            return rootProposal;
        } else {
            throw new ProductionException( "No root proposal, current count is " + containers.size() );
        }
    }
    
    public ObjectNode createObjectNode() {
        return json.createObjectNode();
    }
    
    public JsonNode getResult(){
        return this.result;
    }
    
    public void setResult( JsonNode result ){
        this.result = result;
    }
    
    public JSONMapper getJson(){
        return this.jsonMapper;
    }
    
}