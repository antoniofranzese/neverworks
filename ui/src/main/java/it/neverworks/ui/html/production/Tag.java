package it.neverworks.ui.html.production;

import com.fasterxml.jackson.databind.JsonNode;
import it.neverworks.lang.Arguments;

public interface Tag {
    Tag attr( String name, Object value );
    //TODO: eliminare
    Tag attr( JsonNode node );
    Tag attr( Arguments source );
    Tag style( String name, Object value );
    Tag style( String name );
    Tag style( Arguments source );
}
