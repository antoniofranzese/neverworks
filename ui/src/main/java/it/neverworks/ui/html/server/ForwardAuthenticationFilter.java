package it.neverworks.ui.html.server;

public class ForwardAuthenticationFilter extends J2EEDetectingAuthenticationFilter {
        
    public ForwardAuthenticationFilter() {
        super();
        setDetectors( "header:X-Forwarded-User;basic" );
    }
    
    public void setHeader( String header ){
        setDetectors( "header:" + header + ";basic" );
    }
    
}

