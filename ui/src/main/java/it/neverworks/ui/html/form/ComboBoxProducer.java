package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.form.ComboBox;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.DateTimeZone;

public class ComboBoxProducer<T extends ComboBox> extends BaseTextBoxProducer<T> {
    
    @Override
    protected String dojoType( T widget ) {
        return "works/form/ComboBox";
    }
    
    @Override
    protected void body( T widget ) {
        output.start( "script" ).attr( "type", "works/props" );
        
        output.text( dumpItems( widget ).toString() );
        
        output.end( "script" );
    }

    @Override
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "items".equals( name ) ) {
            return dumpItems( widget );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected ObjectNode dumpItems( T widget ) {
        ArrayNode results = json.array(); 

        DataSource items = widget.getItems();
        if( items != null ) {
            int count = 0;
            for( Object item: items ) {
                ObjectNode itemNode = json.object();

                itemNode.set( "id", JSON.encode( count++ ) );
                itemNode.set( "cap", JSON.encode( items.get( item, widget.getCaption() ) ) );

                results.add( itemNode );
            }
        }
        ObjectNode result = json.object();
        result.set( "items", results );
        return result;
    }
    
}