package it.neverworks.ui.html.form;

import it.neverworks.ui.form.ListBox;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.WidgetChangeProposal;
import it.neverworks.ui.html.layout.BorderLayoutProducer;

public class ListBoxProducer extends BaseFormWidgetProducer<ListBox> {
    
    public void render( ListBox widget ) {
        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/grid/ListManager" )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "readonly", widget.getReadonly() ? true : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "initialValue", widget.getValue() != null ? String.valueOf( widget.getValue() ) : null )
            .attr( switchEvent( widget.getClick() ) )
            .attr( switchEvent( widget.getInspect() ) )
            .attr( "hint", widget.getHint() )
            .attr( "problem", dumpProblem( widget.getProblem() ) )
            .style( "width", widget.getWidth() )
            .style( "font", widget.getFont() )
            .style( "border", widget.getBorder() )
            .style( dumpFlavor( widget ) ); 
       
        ObjectNode propsNode = json.createObjectNode();
        
        // Properties
        if( widget.getHeight() == null || widget.getHeight().getKind() == Size.Kind.AUTO ) {
            propsNode.put( "autoHeight", true );
        } else {
            start.style( "height", widget.getHeight() );
        }
        
        
        // Single column
        ArrayNode columns = json.createArrayNode();
        ObjectNode column = json.createObjectNode();
        column.put( "field", "cap" );
        column.put( "name", "Caption" );
        columns.add( column );
        propsNode.set( "columns", columns );
        
        // Items
        propsNode.set( "data", dumpItems( widget ) );
        
        // Dump
        output.start( "script" ).attr( "type", "works/props"  );
        output.text( propsNode.toString() );
        output.end( "script" );
        
        output.end();
       
    }
    
    protected ObjectNode dumpProperty( ListBox widget, String name ) {
        if( "items".equals( name ) ) {
            return json.property( "items", dumpItems( widget ) );
        } else if( "value".equals( name ) ) {
            Object value = widget.getValue();
            return json.property( "value", value != null ? Strings.valueOf( value ) : null );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected JsonNode dumpItems( ListBox widget ) {
        ArrayNode itemsNode = json.createArrayNode();
        DataSource items = widget.getItems();
        String key = widget.getKey();
        ItemFormatter caption = widget.getCaption();
        boolean html = widget.getEncoding() == ListBox.Encoding.HTML;
        if( items != null ) {
            for( Object item: items ) {
                ObjectNode itemNode = json.createObjectNode();
                itemNode.set( "id", JSON.js.encode( Strings.valueOf( items.get( item, key ) ) ) );
                String cap = caption != null ? Strings.safe( caption.format( item ) ) : "";
                itemNode.set( "cap", JSON.js.encode( ( html ? cap : HTML.encode( cap ) ).replace( "\n", "<br/>" ) ) );
                itemsNode.add( itemNode );
            }
        }
        return itemsNode;
    } 
}
