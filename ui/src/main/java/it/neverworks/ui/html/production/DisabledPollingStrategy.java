package it.neverworks.ui.html.production;

import it.neverworks.lang.Arguments;

public class DisabledPollingStrategy implements PollingStrategy {
    
    @Override
    public PollingParameters analyzePolling( Arguments arguments ) {
        return new PollingParameters().set( "enabled", false );
    }
}