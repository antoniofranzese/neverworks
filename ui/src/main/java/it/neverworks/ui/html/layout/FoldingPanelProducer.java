package it.neverworks.ui.html.layout;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.FoldingPanel;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.production.Tag;

public class FoldingPanelProducer<T extends FoldingPanel> extends ExpandoPanelProducer<T> {
    
    protected String dojoType() {
        return "works/layout/FoldingPane";
    }
    
    protected Tag startTag( T widget ) {
        return super.startTag( widget )
            .attr( "closedSize", SizeConverter.toCSS( widget.get( "fold", Size.pixels( 0 ) ) ) );
    }
}