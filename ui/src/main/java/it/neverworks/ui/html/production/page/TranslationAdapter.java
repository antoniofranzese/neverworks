package it.neverworks.ui.html.production.page;

import java.util.Locale;
import it.neverworks.i18n.Translation;

public class TranslationAdapter {
    
    public Locale getCurrent(){
        return Translation.current();
    }
}