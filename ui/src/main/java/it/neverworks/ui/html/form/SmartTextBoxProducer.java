package it.neverworks.ui.html.form;

import it.neverworks.ui.form.SmartTextBox;
import it.neverworks.ui.html.production.Tag;

public class SmartTextBoxProducer<T extends SmartTextBox> extends TextBoxProducer<T>  {

    @Override
    public Tag start( T widget ) {
        return super.start( widget )
            .attr( switchEvent( widget.getClick() ) );
    }

    @Override
    protected String dojoType( T widget ) {
        return "works/form/SmartTextBox";
    }
    
}