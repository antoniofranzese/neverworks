package it.neverworks.ui.html.web;

import java.io.IOException;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.DeprecatedException;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileRepository;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.context.Inject;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.web.Downloader;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.data.HrefArtifact;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.server.PrivateURL;

public class DownloaderProducer extends HTMLProducer<Downloader> implements Factory, Model {
    
    public void render( Downloader widget ) {
        output.dojo.start( "div" )
            .style( "display", "none" );
        
        output.dojo.start( "div" )
            .attr( "type", "works/web/Downloader" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            // .attr( "url", this.getUrl() )
            .attr( "protocol", widget.getProtocol() )
            .attr( switchEvent( widget.getSend() ) );        
            
        output.end();
        output.end();
    
    }
    
    protected ObjectNode dumpProperty( Downloader widget, String name ) {
        if( "source".equals( name ) ) {
            return json.property( "source", dumpSource( widget ) );
           
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected String dumpSource( Downloader widget ) {
        Artifact artifact = widget.getSource();

        if( artifact != null ) {
            Object resolved = resolveArtifact( artifact );
            
            try {
                if( resolved instanceof HrefArtifact ) {
                    PrivateURL url = new PrivateURL( Strings.valueOf( ((HrefArtifact) resolved).getContent() ) );
                
                    if( Strings.hasText( widget.getFileName() ) ) {
                        url.set( "X-NWK-Name", widget.getFileName()  );
                    }
                    if( Strings.hasText( widget.getType() ) ) {
                        url.set( "X-NWK-Type", widget.getType()  );
                    }

                    url.set( "X-NWK-Disposition", "attachment" );

                    return url.toString();
                    
                } else if( resolved instanceof RawArtifact ) {
                    return Strings.valueOf( ((RawArtifact) resolved).getContent() ) ;

                } else {
                    throw new ProductionException( "Unknown artifact: " + Objects.repr( artifact ) );
                }
            } finally {
                widget.setSource( null );
                widget.setFileName( null );
                widget.setType( null );
            }
            
        } else {
            return null;
        }
    }
    
    public <T extends Widget> Producer<T> pick( T widget ) {
        return (Producer<T>)this;
    }
    
    @Deprecated
    public void setUrl( String url ){
        throw new DeprecatedException( "Setting repository on DownloaderProducer is no longer supported, configure artifact resolver" );
    }
    
    @Deprecated
    public void setRepository( FileRepository repository ){
        throw new DeprecatedException( "Setting repository on DownloaderProducer is no longer supported, configure artifact resolver" );
    }
}