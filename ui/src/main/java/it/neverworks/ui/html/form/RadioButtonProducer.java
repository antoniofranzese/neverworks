package it.neverworks.ui.html.form;

import java.util.List;

import it.neverworks.lang.Properties;
import it.neverworks.lang.Strings;

import it.neverworks.encoding.HTML;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.form.RadioButton;
import it.neverworks.ui.form.RadioGroup;

public class RadioButtonProducer extends BaseFormWidgetProducer<RadioButton> {
    
    public void render( RadioButton widget ) {
        
        if( widget.getLabelled() ) {
            output.start( "table" ).attr( "role", "presentation" );
            output.start( "tr" );
            output.start( "td" );

            ItemFormatter caption = widget.getGroup().getCaption();
            String cap = caption != null ? Strings.safe( caption.format( widget.getItem() ) ) : "";
            boolean html = widget.getGroup().getEncoding() == RadioGroup.Encoding.HTML;
            output.text( ( html ? cap : HTML.encode( cap ) ).replace( "\n", "<br/>" ) );
            output.end( "td" );
            output.start( "td" );
        }
        
        output.dojo.start( "div" )
            .attr( "type", "works/form/RadioButton" )
            .attr( "name",  widget.getGroup().getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "itemValue", ExpressionEvaluator.evaluate( widget.getItem(), widget.getGroup().getKey() ) )
            .style( dumpFlavor( widget ) ); 
        
        output.end();
        
        if( widget.getLabelled() ) {
            output.end( "td" );
            output.end( "tr" );
            output.end( "table" );
        }
    }


}