package it.neverworks.ui.html.form;

import static it.neverworks.language.*;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.NumberSpinner;

public class NumberSpinnerProducer<T extends NumberSpinner> extends NumberTextBoxProducer<T> {
    
    public Tag start( T widget ) {
        Tag start = super.start( widget );

        if( widget.getMin() != null || widget.getMax() != null ) {
            start.attr( "rolling", widget.getRolling() );
        }

        start.attr( "smallDelta", widget.getDelta() );
        start.attr( "deltaBoost", widget.getBoost() );

        return start;
    }

    protected String dojoType( T widget ) {
        return "works/form/NumberSpinner";
    }
    
}