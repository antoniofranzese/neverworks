package it.neverworks.ui.html.layout;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.ExpandoPanel;
import it.neverworks.ui.html.production.Tag;

public class ExpandoPanelProducer<T extends ExpandoPanel> extends PanelProducer<T> {
    
    public void render( T widget ){
        
        boolean redraw = redrawing( widget );
        
        if( !redraw ) {
            Tag start = startTag( widget );
        } 
        
        if( widget.getContent() != null ) {
            produce( widget.getContent() );
        } else {
            output.text( "&nbsp;" );
        }
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    protected Tag startTag( T widget ) {
        return output.dojo.start( "div")
            .attr( "type", dojoType() )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "title", dumpTitle( widget ) )
            .attr( "visible", widget.getVisible() )
            .attr( "startExpanded", widget.getExpanded() )

            .attr( switchEvent( widget.getToggle() ) )

            .style( "background-color", widget.getBackground() )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( dumpFlavor( widget ) );
    }
    
    protected String dojoType() {
        return "works/layout/ExpandoPane";
    }

    protected void analyzeWidget( T widget ) {
        Changes changes = widget.model().changes();
        if( changes.containsAny( getChangeableProperties() ) ) {
            if( changes.contains( "visible" ) ) {
                if( Boolean.TRUE.equals( widget.model().get( "visible" ) ) ) {
                    changes.add( "expanded" );
                } else {
                    changes.remove( "expanded" );
                }
            } else if( changes.contains( "expanded" ) && Boolean.FALSE.equals( widget.model().get( "visible" ) ) ) {
                changes.remove( "expanded" );
            }

            if( changes.size() > 0 ) {
                analysis.propose( this, widget, changes.toList() );
            }
            
        }
    }

    protected String dumpTitle( T widget ) {
        return widget.getTitle() != null ? HTML.encode( widget.getTitle() ).replace( " ", "&nbsp;" ).replace( "\n", "" ) : "&nbsp;";
    }
    
    protected String[] getChangeableProperties() {
        return new String[]{ "title", "expanded", "visible" };
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "title".equals( name ) ) {
            return json.property( "title", dumpTitle( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        } 
    }
    
}