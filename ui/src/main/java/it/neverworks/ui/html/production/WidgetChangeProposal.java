package it.neverworks.ui.html.production;

import java.util.List;

import it.neverworks.ui.Widget;

public class WidgetChangeProposal extends ChangeProposal {
    /*package*/ List<String> changes;
    
    public WidgetChangeProposal( HTMLProducer producer, Widget widget, List<String> changes ) {
        super( producer, widget );
        this.changes = changes;
    }
    
    public List<String> getChanges(){
        return this.changes;
    }
    
    public String toString() {
        return "WidgetChangeProposal(widget=" + widget.getCommonName() + ", changes=" + changes + ")";
    }
}

