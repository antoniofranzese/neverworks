package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.form.ProgressBar;

public class ProgressBarProducer extends HTMLProducer<ProgressBar> {
    
    public void render( ProgressBar widget ) {
        
        boolean enclosed = widget.getPadding() != null || widget.getMargin() != null;
        
        if( enclosed ) {
            output.dojo.start( "div" )
                .style( "padding", widget.getPadding() )
                .style( "margin", widget.getMargin() );
        }
        
        output.dojo.start( "div" )
            .attr( "type", "works/form/ProgressBar" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "value", widget.getValue() != null ? widget.getValue() : "Infinity" )
            .attr( "maximum", widget.getMax() )
            .style( "width", widget.getWidth() )
            .style( dumpFlavor( widget ) );
        
        output.end();
        
        if( enclosed ) {
            output.end();
        }
    }
    
    protected ObjectNode dumpProperty( ProgressBar widget, String name ) {
        if( "value".equals( name ) ) {
            return json.property( "value", widget.getValue() != null ? widget.getValue() : "Infinity" );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    

}