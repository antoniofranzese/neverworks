package it.neverworks.ui.html.layout;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.types.BorderSideConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.BoxConverter;
import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.layout.TabLayout;

public class TabLayoutProducer<T extends TabLayout> extends AccordionLayoutProducer<T> {
    
    protected Tag start( T widget ) {
        Tag start = super.start( widget )
            .attr( switchEvent( widget.getClosing() ) )
            .attr( switchEvent( widget.getClick() ) )
            .attr( "nested", Boolean.TRUE.equals( widget.getNested() ) ? true : null )
            //.attr( "stripFont", ColorConverter.toCSS( widget.get( "!strip.font" ) ) )
            .attr( "stripColor", ColorConverter.toCSS( widget.get( "!strip.color" ) ) )
            .attr( "stripBackground", ColorConverter.toCSS( widget.get( "!strip.background" ) ) )
            .attr( "stripPadding", BoxConverter.toCSS( widget.get( "!strip.padding" ) ) )
            .attr( "stripVisible", Boolean.FALSE.equals( widget.get( "!strip.visible" ) ) ? false : null )
            //.attr( "contentFont", FontConverter.toCSS( widget.get( "!content.font" ) ) )
            .attr( "contentColor", ColorConverter.toCSS( widget.get( "!content.color" ) ) )
            .attr( "contentBackground", ColorConverter.toCSS( widget.get( "!content.background" ) ) )
            .attr( "contentPadding", BoxConverter.toCSS( widget.get( "!content.padding" ) ) )
            .attr( "tabPosition", widget.getPosition() == TabLayout.Position.BOTTOM ? "bottom" : null );
            
        if( widget.getBorder() != null ) {
            start
                .attr( "borderTop",    widget.getBorder().getTop() != null ? BorderSideConverter.toCSS( widget.getBorder().getTop() ) : null )
                .attr( "borderBottom", widget.getBorder().getBottom() != null ? BorderSideConverter.toCSS( widget.getBorder().getBottom() ) : null )
                .attr( "borderLeft",   widget.getBorder().getLeft() != null ? BorderSideConverter.toCSS( widget.getBorder().getLeft() ) : null )
                .attr( "borderRight",  widget.getBorder().getRight() != null ? BorderSideConverter.toCSS( widget.getBorder().getRight() ) : null );
        }    

        return start;
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "strip.visible".equals( name ) ) {
            return json.property( "stripVisible", widget.get( "strip" ) );
        // } else if( "strip.font".equals( name ) ) {
        //     return json.property( "stripFont", FontConverter.toCSS( widget.get( "!strip.font" ) ) );
        } else if( "strip.color".equals( name ) ) {
            return json.property( "stripColor", ColorConverter.toCSS( widget.get( "!strip.color" ) ) );
        } else if( "strip.background".equals( name ) ) {
            return json.property( "stripBackground", ColorConverter.toCSS( widget.get( "!strip.background" ) ) );
        } else if( "strip.padding".equals( name ) ) {
            return json.property( "stripPadding", BoxConverter.toCSS( widget.get( "!strip.padding" ) ) );
        // } else if( "content.font".equals( name ) ) {
        //     return json.property( "contentFont", FontConverter.toCSS( widget.get( "!content.font" ) ) );
        } else if( "content.color".equals( name ) ) {
            return json.property( "contentColor", ColorConverter.toCSS( widget.get( "!content.color" ) ) );
        } else if( "content.background".equals( name ) ) {
            return json.property( "contentBackground", ColorConverter.toCSS( widget.get( "!content.background" ) ) );
        } else if( "content.padding".equals( name ) ) {
            return json.property( "contentPadding", BoxConverter.toCSS( widget.get( "!content.padding" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        } 
    }
    

    protected String dojoType() {
        return "works/layout/TabContainer";
    }

}