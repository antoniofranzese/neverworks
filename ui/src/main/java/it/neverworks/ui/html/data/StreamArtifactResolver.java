package it.neverworks.ui.html.data;

import static it.neverworks.language.*;

import java.io.IOException;
import it.neverworks.lang.Errors;
import it.neverworks.encoding.MIME;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.StreamArtifact;
import it.neverworks.ui.data.FileBadgeArtifact;

public class StreamArtifactResolver extends FileBadgeArtifactResolver {
    
    protected boolean marking = false;
    
    public Object resolveArtifact( Artifact artifact ) {
        StreamArtifact stream = (StreamArtifact) artifact;
        FileInfo file = stream.retrieveFileInfo();
        if( file == null ) {
            if( this.marking && stream.getStream().markSupported() ) {
                try {
                    stream.getStream().mark( Integer.MAX_VALUE );
                } catch( Exception ex ) {}
            }

            FileBadge badge = getRepository().putStream( stream.getStream() );

            if( this.marking && stream.getStream().markSupported() ) {
                try {
                    stream.getStream().reset();
                } catch( Exception ex ) {}
            }

            try {
                getRepository().describe( badge, arg( "type", MIME.detect( getRepository().getFile( badge ) ) ) );
            } catch( Exception ex ){}

            file = getRepository().getInfo( badge ); 
            stream.replace( file );
        }
        return super.resolveArtifact( new FileBadgeArtifact( file.getBadge() ) );
    }

    @Property @Required @Inject( value = { "it.neverworks.ui.FileRepository", "it.neverworks.FileRepository" }, lazy = true )
    private FileRepository repository;
    
    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }

    public boolean getMarking(){
        return this.marking;
    }
    
    public void setMarking( boolean marking ){
        this.marking = marking;
    }

}