package it.neverworks.ui.html.production;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.ui.Widget;

public class CollectorChangeProposal extends ChangeProposal {
    /*package*/ List<ChangeProposal> proposals;

    public CollectorChangeProposal( HTMLProducer producer, Widget widget, List<ChangeProposal> proposals ) {
        super( producer, widget );
        this.proposals = proposals;
    }
    
    public List<ChangeProposal> getProposals(){
        return this.proposals != null ? this.proposals : new ArrayList<ChangeProposal>( 0 );
    }
    
    public String toString() {
        return "CollectorChangeProposal(widget=" + widget.getCommonName() + ", proposals=" + proposals + ")";
    }
    
}

