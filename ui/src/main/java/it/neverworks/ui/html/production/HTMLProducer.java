package it.neverworks.ui.html.production;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.encoding.JSON;
import it.neverworks.model.description.Changes;

import it.neverworks.ui.Widget;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.io.State;
import it.neverworks.ui.production.BaseProducer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.data.ArtifactResolver;
import it.neverworks.ui.html.data.BaseArtifactResolver;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.CompositeArtifact;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.runtime.Device;

public abstract class HTMLProducer<T extends Widget> extends BaseProducer<T> {
    
    protected HTMLOutput output;
    protected HTMLProducer parent;
    protected JSONAnalysis analysis;
    protected JSONMapper json;
    
    public void setParent( HTMLProducer parent ){
        this.parent = parent;
    }
    
    public void setAnalysis( JSONAnalysis analysis ){
        this.analysis = analysis;
    }
    
    public void setOutput( HTMLOutput output ){
        this.output = output;
    }
    
    public void setJson( JSONMapper json ){
        this.json = json;
    }
    
    public void bubble( Event event ) {
        if( parent != null ) {
            parent.bubble( event );
        }
    }
    
    public void analyze( T widget ) {
        Changes changes = widget.model().changes();
        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }
    }
    
    public void inspect( Widget widget ) {
        HTMLProducer<T> producer = (HTMLProducer<T>)factory.pick( widget );
        producer.analyze( (T)widget );
    }
    
    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        // Un comune producer presume di ricevere un WidgetChangeProposal, altrimenti c'e' una casistica non gestita
        for( String property: ((WidgetChangeProposal) proposal).getChanges() ) {
            // System.out.println( "For " + proposal.getWidget() + " " + property + " " + ( proposal.getWidget().get( property ) == null ) );
            ObjectNode propertyNodes = dumpProperty( (T)proposal.getWidget(), property );
            if( propertyNodes != null ) {
                result.setAll( propertyNodes );
            }
        }
        return result;
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        try {
            Object value = widget.get( name );
            if( value instanceof Signal ) {
                return json.property( name + "$event", ((Signal) value).size() > 0 );
            } else if( "flavor".equals( name ) && widget instanceof Flavored ) {
                return json.property( "flavor", dumpFlavor( (Flavored) widget ) );
            } else if( "layout".equals( name ) ) {
                return null;
            } else {
                return json.property( name, value );
            }
        } catch( Exception ex ) {
            Exception error = Errors.unwrap( ex );
            throw new ProductionException( "Error dumping " + widget.getClass().getName() + "." + name + ": " + error.getMessage(), error );
        }
    }
    
    protected ObjectNode switchEvent( Signal signal ) {
        if( signal.size() > 0 ) {
            return json.property( signal.getName() + "$event", true );
        } else {
            return null;
        }
    }
    
    public JsonNode execute( T widget, String command, JsonNode parameters ) {
        return null;
    }
    
    protected final static String REDRAW_ATTRIBUTE = "HTMLProducer/Redraw";
        
    protected String redraw( Widget widget ) {
        context.set( REDRAW_ATTRIBUTE, widget );
        String result = draw( widget );
        context.remove( REDRAW_ATTRIBUTE );
        return result;
    }
    
    protected String draw( Widget widget ) {
        BufferedHTMLOutput bufferedOutput = new BufferedHTMLOutput();
        ((HTMLFactory) factory).with( context, bufferedOutput ).produce( widget );
        return bufferedOutput.getBuffer();
    }
    
    public boolean redrawing( Widget widget ) {
        return context.contains( REDRAW_ATTRIBUTE ) && context.get( REDRAW_ATTRIBUTE ).equals( widget );
    }
    
    public Event convertEvent( T widget, String name, State state ) {
        return widget.signal( name ).convertEvent( state );
    }
    
    public String dumpFlavor( Artifact flavorArtifact ) {
        if( flavorArtifact != null ) {
            if( flavorArtifact instanceof CompositeArtifact ) {
                StringBuilder flavors = new StringBuilder( 256 );
                for( Artifact artifact: ((CompositeArtifact) flavorArtifact).getArtifacts() ) {
                    flavors.append( "flavor-" ).append( resolveArtifact( artifact ) ).append( " " );
                }
                return flavors.toString();
            }
            return "flavor-" + Strings.valueOf( resolveArtifact( flavorArtifact ) );
        } else {
            return null;
        }
    }

    public String dumpFlavor( Flavored widget ) {
        return dumpFlavor( widget.getFlavor() );
    }

    public boolean development() {
        return context.get( "Environment" ).equals( "development" );
    }

    public boolean mobile() {
        return Device.is( "mobile" );
    }

    public boolean phone() {
        return Device.is( "phone" );
    }

    public boolean tablet() {
        return Device.is( "tablet" );
    }
    
    private ArtifactResolver artifactResolver = null;

    public ArtifactResolver getArtifactResolver(){
        return this.artifactResolver;
    }

    public void setArtifactResolver( ArtifactResolver artifactResolver ){
        this.artifactResolver = artifactResolver;
    }
    
    public Object resolveArtifact( Object artifact ) {
        return this.artifactResolver.resolveArtifact( (Artifact) artifact );
    }
    
}