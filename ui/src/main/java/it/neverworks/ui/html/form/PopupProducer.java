package it.neverworks.ui.html.form;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;

import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.model.description.Changes;
import it.neverworks.lang.Strings;
import it.neverworks.ui.Widget;
import it.neverworks.ui.BehavioralWidget;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.types.BoxConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.ViewportInfoConverter;

public class PopupProducer<T extends Popup> extends FormProducer<T> {
    
    public void render( T widget ) {
        Popup popup = (Popup)widget;
        
        boolean redraw = redrawing( widget ) && ! widget.model().changes().contains( "design" );
        
        if( !redraw ) {
            String popupID = popup.getName() + "-dialog-" + Strings.generateHexID( 8 );

            output.dojo.start( "div" )
                .attr( "type", "works/form/PopupProxy" )
                .attr( "name", popup.getName() )
                .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
                .attr( "popup", popupID )
                .attr( "startupOpened", popup.getOpened() );
    
            output.dojo.start( "div" ).style( "display", "none" );
            
            if( popup.getOpened() && popup.get( "design" ) == Popup.Design.TOOLTIP && popup.get( "anchor" ) == null ) {
                throw new ProductionException( "Tooltip Popup design needs an anchor" );
            }
            
            Object anchor = popup.get( "anchor" );
            
            String dojoType = "works/form/Popup";
            if( Popup.Design.TOOLTIP == popup.get( "design" ) ) {
                dojoType = "works/form/TooltipPopup";
                if( anchor == null ) {
                    anchor = new Point( 0, 0 );
                }
            } else if( Popup.Design.SLIDEUP == popup.get( "design" ) ) {
                dojoType = "works/form/SlideupPopup";
            }
            
            Tag popupStart = output.dojo.start( "div" )
                .attr( "type", dojoType )
                .attr( "id", popupID )
                .attr( "closable", popup.getClosable() )
                .attr( "title", popup.getTitle() != null ? HTML.encode( widget.getTitle() ).replace( " ", "&nbsp;" ).replace( "\n", "" ) : null  )
                .attr( "anchor", dumpAnchor( anchor ) )
                .attr( "resizable", popup.getResizable() )
                .attr( "height", SizeConverter.toCSS( this, popup.getHeight() ) )
                .attr( "width", SizeConverter.toCSS( this, popup.getWidth() ) )
                .attr( "minHeight", SizeConverter.toCSS( this, popup.getMinimumHeight() ) )
                .attr( "minWidth", SizeConverter.toCSS( this, popup.getMinimumWidth() ) )
                .attr( "maxHeight", SizeConverter.toCSS( this, popup.getMaximumHeight() ) )
                .attr( "maxWidth", SizeConverter.toCSS( this, popup.getMaximumWidth() ) )
                .attr( "portraitLayout", ViewportInfoConverter.toJSON( this, widget.get( "!layout.portrait" ) ) ) 
                .attr( "landscapeLayout", ViewportInfoConverter.toJSON( this, widget.get( "!layout.landscape" ) ) ) 
                
                //.attr( "padding", BoxConverter.toCSS( popup.getPadding() ) )
                .attr( "autoClose", popup.getAutoClose() )
                .style( "worksPopup" )
                .style( "top", popup.getTop() )
                .style( "left", popup.getLeft() )
                .style( "min-height", popup.getMinimumHeight() )
                .style( "min-width", popup.getMinimumWidth() )
                .style( "height", popup.getHeight() )
                .style( "width", popup.getWidth() )
                .style( "font", popup.getFont() )
                .style( dumpFlavor( widget ) );
        
            // Se la popup e' usata solo per wrappare una singola form deve perdere il padding interno
            if( widget.children().query().notInstanceOf( BehavioralWidget.class ).count() == 1 ) {
                popupStart.style( "singleChildWrapper" );
            }
        
            output.dojo.start( "div" )
                .attr( "type", "works/form/manager/FormContentPane" )
                .style( "worksFormPane" )
                .style( "main" );  
        }
        
        Tag formStart = output.dojo.start( "div" )
            .attr( "type", "works/form/Manager" )
            .attr( "name", popup.getName() )
            .attr( "class", "worksFormManagerPopup" )
            .attr( "activeChild", widget.getActiveChild() != null ? widget.getActiveChild().getPath() : null )
            .style( "margin", popup.getPadding() );
        
        if( development() ) {
            formStart
                .attr( "from", popup.getClass().getName() )
                .attr( "canonicalName", popup.getCanonicalName() );
        }

        for( Widget child: widget.getChildren() ) {
            produce( child );
        }
        
        output.end(); // Manager

        if( !redraw ) {
            output.end( "div" ); // ContentPane
            output.end( "div" ); // Popup
            output.end( "div" ); // div display:none
            output.end( "div" ); // PopupProxy
        }
        
    }
    
    public void analyze( T widget ) {
        if( widget.model().changes().contains( "design" ) && widget.getPersistent() ){
            analysis.bubble( new FormChildModifiedEvent( widget ) );
        } else {
            super.analyze( widget );
        };
    }

    protected ObjectNode dumpProperty( T widget, String name ) {
        //System.out.println( "Dumping " + name );
        if( "anchor".equals( name ) ) {
            return json.property( "anchor", dumpAnchor( widget.<Widget>get( "anchor" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }

    protected List<String> deliverableProperties() {
        return list( "title", "opened", "anchor", "width", "height", "top", "left", "activeChild" );
    }

    protected Object dumpAnchor( Object anchor ) {
        if( anchor != null ) {
            if( anchor instanceof Widget ) {
                return ((Widget) anchor).getPath();
            } else {
                Point point = (Point) anchor;
                return arg( "x", point.getX() ).arg( "y", point.getY() );
            }
        } else {
            return null;
        }
    }
}