package it.neverworks.ui.html.runtime;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;

import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Strings;
import it.neverworks.model.Value;
import it.neverworks.ui.runtime.Device;

public class LanguagesAttribute extends Value {

    private static Map<String,Locale> locales;

    static {
        locales = new HashMap();
        for( Locale lc: Locale.getAvailableLocales() ) {
            String key = lc.getLanguage() + ( Strings.hasText( lc.getCountry() ) ? "-" + lc.getCountry() : "" );
            locales.put( key.toLowerCase(), lc );
        }
    }

    private final static Wrapper PARSER = new Wrapper() {
        public Object asObject() {
            String header = Device.get( "header[Accept-Language]" );

            if( Strings.hasText( header ) ) {
                String[] langs = header.split( "," );
                List<Locale> languages = new ArrayList( langs.length );
                for( int i = 0; i < langs.length; i++ ) {
                    String lang = langs[ i ].trim().toLowerCase();
                    if( lang.indexOf( ";" ) >= 0 ) {
                        lang = lang.substring( 0, lang.indexOf( ";" ) ).trim().toLowerCase();
                    }
                    if( locales.containsKey( lang ) ) {
                        languages.add( locales.get( lang ) );
                    }
                }
                return languages;
                
            } else {
                List<Locale> languages = new ArrayList( 1 );
                languages.add( Locale.ITALIAN );
                return languages;
            }
        }
    };

    public Object asObject() {
        return Device.cache( "NW3:LANGUAGES:LOCALES", PARSER );
    }

}
