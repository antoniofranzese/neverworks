package it.neverworks.ui.html.production;

import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class DevelopmentLongPollingStrategy extends BaseModel implements PollingStrategy {
    
    public PollingParameters analyzePolling( Arguments parameters ) {

        int peers;
        try {
            peers = Integer.parseInt( parameters.get( "peers" ) );
        } catch( Exception ex ) {
            peers = 1;
        }

        switch( peers ) {
            case 1:
            case 2:
                return new PollingParameters().set( "gap", 500 ).set( "cycle", 30000 );
            case 3:
                return new PollingParameters().set( "gap", 1000 ).set( "cycle", 20000 );
            case 4:
                return new PollingParameters().set( "gap", 2000 ).set( "cycle", 10000 );
            case 5:
            case 6:
                return new PollingParameters().set( "gap", 3000 ).set( "cycle", 0 );
            case 7:
                return new PollingParameters().set( "gap", 3500 ).set( "cycle", 0 );
            case 8:
                return new PollingParameters().set( "gap", 4000 ).set( "cycle", 0 );
            case 9:
                return new PollingParameters().set( "gap", 4500 ).set( "cycle", 0 );
            default:
                return new PollingParameters().set( "gap", 5000 ).set( "cycle", 0 );
        }
    }
}