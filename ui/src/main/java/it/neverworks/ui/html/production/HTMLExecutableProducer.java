package it.neverworks.ui.html.production;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.WidgetPersistedEvent;
import it.neverworks.ui.production.Context;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.Widget;

public class HTMLExecutableProducer<T extends Widget> implements Producer<T>{

    private JSONAnalysis analysis;
    private HTMLOutput output;
    private Context context;
    private HTMLProducerFactory factory;
    
    public HTMLExecutableProducer( HTMLProducerFactory factory, Context context, HTMLOutput output ) {
        this.output = output;
        this.context = context;
        this.factory = factory;
    }

    public HTMLExecutableProducer( HTMLProducerFactory factory, Context context, JSONAnalysis analysis ) {
        this.analysis = analysis;
        this.context = context;
        this.factory = factory;
    }
    
    public void produce( Widget widget ) {
        HTMLExecutableFactory executable = new HTMLExecutableFactory( factory, context, output );
        Producer producer = executable.pick( widget );
        producer.render( widget );
        ((WidgetLifeCycle) widget).notify( new WidgetPersistedEvent( this, widget ) );
    }
    
    public void render( T widget ) {
        render( widget );
    }
    
    public void inspect( Widget widget ) {
        HTMLExecutableFactory executable = new HTMLExecutableFactory( factory, context, analysis );
        HTMLProducer producer = (HTMLProducer)executable.pick( widget );
        producer.analyze( widget );
    }
    
    public JsonNode execute( Widget widget, String command, JsonNode parameters ) {
        HTMLExecutableFactory executable = new HTMLExecutableFactory( factory, context, analysis );
        HTMLProducer producer = (HTMLProducer)executable.pick( widget );
        return producer.execute( widget, command, parameters );
    }
    
    public Producer pick( Widget widget ) {
        HTMLExecutableFactory executable = new HTMLExecutableFactory( factory, context, output );
        Producer producer = executable.pick( widget );
        return producer;
    }
    
    
}