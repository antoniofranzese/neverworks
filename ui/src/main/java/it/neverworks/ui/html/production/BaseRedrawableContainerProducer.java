package it.neverworks.ui.html.production;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;

public abstract class BaseRedrawableContainerProducer<T extends Widget> extends HTMLProducer<T> {
    
    protected boolean redrawRequested = false;
 
    public void analyze( T widget ) {
        analysis.begin( this, widget );
        
        analyzeWidget( widget );
        
        if( containerNeedsRedraw( widget ) ) {
            //System.out.println( widget.getCommonName() + " will redraw cause of own properties" );
            redrawRequested = true;
        } else {
            analyzeChildren( widget );
        }

        if( redrawRequested ) {
            analysis.reject();
            
            if( parent != null ) {
                if( widget.model().changes().containsAny( getSurvivingProperties() ) ) {
                    analysis.propose( this, widget, widget.model().changes().retain( getSurvivingProperties() ).toList() );
                } else {
                    analysis.propose( this, widget );
                }
            }

        } else {
            analysis.accept();
        }
        
    }
    
    protected void analyzeWidget( T widget ) {
    
    }
    
    protected void analyzeChildren( T widget ) {
        for( Widget child: ((Container) widget).getChildren() ) {
            inspect( child );
        }
    }

    public void bubble( Event event ) {
        if( event instanceof WidgetModifiedEvent ) {
            //System.out.println( this.getClass().getSimpleName() + " will redraw cause of " + ((Widget) event.getSource()).getCommonName() );
            redrawRequested = true;
        } else if( parent != null ) {
            parent.bubble( event );
        }
    }
    
    protected boolean containerNeedsRedraw( T widget ) {
        return widget.model().changes().contains( "children" );
    }
    
    protected abstract String[] getSurvivingProperties();
    
    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        T widget = (T) proposal.getWidget();

        if( redrawRequested ) {
            ObjectNode redrawal = dumpRedrawal( widget );
            if( redrawal != null ) {
                result.putAll( redrawal );
            }
        }

        for( String change: ((WidgetChangeProposal) proposal).getChanges() ) {
            ObjectNode property = dumpProperty( widget, change );
            if( property != null ) {
                result.putAll( property );
            }
        }

        return result;
    }
    
    protected ObjectNode dumpRedrawal( T widget ) {
        return json.property( "content", redraw( widget ) );
    }
    
}