package it.neverworks.ui.html.form.ribbon;

import it.neverworks.model.description.Changes;
import it.neverworks.ui.form.ribbon.Ribbon;
import it.neverworks.ui.html.production.BaseContainerProducer;
import it.neverworks.ui.Widget;
import it.neverworks.ui.html.layout.BorderLayoutProducer;

public class RibbonProducer extends BaseContainerProducer<Ribbon> {
    
    public void render( Ribbon widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/layout/Ribbon" )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "class", "ribbon" );
                
        output.dojo.start( "div" )
            .attr( "type", "works/layout/RibbonContainer" )
            .attr( "name", widget.getName() )
            .attr( "startupSelected", widget.getSelectedIndex() )
            .attr( "class", "ribbonContainer")
            .attr( switchEvent( widget.getClick() ) )
            .attr( switchEvent( widget.getSelecting() ) )
            .attr( switchEvent( widget.getSelect() ) );
            
        for( Widget child: widget.getChildren() ) {
            produce( child );
        }
    
        output.end();
        output.end();
    }

    protected void analyzeWidget( Ribbon widget ) {
        Changes changes = widget.model().changes();
        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.replace( "selected", "selectedIndex" ).toList() );
        }
    }

}