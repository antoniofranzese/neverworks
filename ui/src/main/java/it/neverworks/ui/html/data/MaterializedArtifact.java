package it.neverworks.ui.html.data;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.encoding.JSON;
import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.ui.data.RawArtifact;

public class MaterializedArtifact extends RawArtifact implements JSONEncodable {

    public JsonNode toJSON() {
        return JSON.encode( this.getContent() );
    }
    
}