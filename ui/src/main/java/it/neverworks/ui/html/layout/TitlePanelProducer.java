package it.neverworks.ui.html.layout;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.TitlePanel;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.production.ProductionException;

public class TitlePanelProducer<T extends TitlePanel> extends PanelProducer<T> {
    
    public void render( T widget ){
        
        TitlePanel panel = (TitlePanel) widget;
        
        // if( panel.getParent() instanceof BorderLayout ) {
        //     throw new ProductionException( "TitlePanel is not compatible with BorderLayout" );
        // }
        
        boolean redraw = redrawing( widget );
        
        if( !redraw ) {
            Tag start = output.dojo.start( "div")
                .attr( "type", dojoType() )
                .attr( "name", widget.getName() )
                .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
                .attr( BorderLayoutProducer.attributes( this, widget ) )
                .attr( "title", widget.getTitle() != null ? HTML.encode( widget.getTitle() ).replace( " ", "&nbsp;" ).replace( "\n", "" ) : "&nbsp;" )

                .attr( "collapsible", widget.getCollapsible() )
                .attr( "visible", widget.getVisible() )
                .attr( "align", panel.getAlignment() != null ? convertAlignment( panel.getAlignment() ) : null )

                .style( "background-color", widget.getBackground() )
                .style( "width", widget.getWidth() )
                .style( "height", widget.getHeight() )
                .style( "padding", widget.getPadding() )
                .style( "margin", widget.getMargin() )
                //.style( "border", widget.getBorder() )
                .style( dumpFlavor( widget ) );
        } 
        
        if( widget.getContent() != null ) {
            produce( widget.getContent() );
        } else {
            output.text( "&nbsp;" );
        }
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    protected String dojoType() {
        return "works/layout/TitlePane";
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "alignment".equals( name ) ) {
            return json.property( name, convertAlignment( widget.<TitlePanel.Alignment>get( "alignment" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    public String convertAlignment( TitlePanel.Alignment position ) {
        switch( position ) {
            case RIGHT:
                return "right";
            case CENTER:
                return "center";
            default:
                return "left";
        }
    }
    
}