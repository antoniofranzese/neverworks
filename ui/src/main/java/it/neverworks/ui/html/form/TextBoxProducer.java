package it.neverworks.ui.html.form;

import java.util.Date;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.TextBox;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.DateTimeZone;

public class TextBoxProducer<T extends TextBox> extends BaseTextBoxProducer<T> {
    
    @Override
    public Tag start( T widget ) {
        Tag start = super.start( widget );

        start.attr( "maxlength", widget.getLength() );
        start.attr( "remoteFormat", hasTextModifier( widget ) ? true : null );
        
        return start;
    }
    
    
    @Override
    protected String dojoType( T widget ) {
        return "works/form/TextBox";
    }
    
    protected boolean hasTextModifier( T widget ) {
        return widget.get( "format" ) != null 
            || widget.get( "keep" ) != null
            || widget.get( "waste" ) != null
            || widget.get( "letter" ) != null;
    }

    @Override
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "format".equals( name ) || "keep".equals( name ) ) {
            return json.property( "remoteFormat", hasTextModifier( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    public JsonNode execute( T widget, String command, JsonNode parameters ) {
        if( "format".equals( command ) ) {
            String formatted = widget.processValue( parameters.get( "value" ).asText() );
            return json.property( "value", formatted );
            
        } else {
            throw new ProductionException( "Unknown command: " + command );
        }
    }
    
}