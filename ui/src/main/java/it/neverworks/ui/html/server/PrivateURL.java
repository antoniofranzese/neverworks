package it.neverworks.ui.html.server;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.URL;
import it.neverworks.crypto.Cipher;

public class PrivateURL extends URL {
    
    private final static Cipher cipher = new Cipher().set( "encoding", "base64" );
    private Arguments xargs;
    
    public PrivateURL( String spec ) {
        super( spec );
        if( this.has( "X-NWK" ) ) {
            this.xargs = Arguments.parse( cipher.decrypt( get( "X-NWK" ) ) );
        } else {
            this.xargs = new Arguments();
        }
    }

    public Arguments getPrivateArguments() {
        return this.xargs;
    }
    
    public String get( String name ) {
        if( name.startsWith( "X-NWK-" ) ) {
            return Strings.valueOf( this.xargs.get( name ) );
        } else {
            return super.get( name );
        }
    }

    public PrivateURL set( String name, String value  ) {
        if( name.startsWith( "X-NWK-" ) ) {
            this.xargs.set( name, value );
            super.set( "X-NWK", cipher.encrypt( this.xargs.format() ) );
        } else {
            super.set( name, value );
        }
        return this;
    }

    public boolean has( String name ) {
        if( name.startsWith( "X-NWK-" ) ) {
            return this.xargs.has( name );
        } else {
            return super.has( name );
        }
    }
    
}