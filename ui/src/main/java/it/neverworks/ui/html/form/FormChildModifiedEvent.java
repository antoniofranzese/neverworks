package it.neverworks.ui.html.form;

import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class FormChildModifiedEvent extends Event<Widget> {
    
    public FormChildModifiedEvent( Widget source ) {
        super( source );
    }
    
    public FormChildModifiedEvent( Widget source, String reason ) {
        super( source, reason );
    }
    
}