package it.neverworks.ui.html.form.ribbon;

import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ribbon.RibbonSection;
import it.neverworks.ui.html.production.BaseContainerProducer;

public class RibbonSectionProducer extends BaseContainerProducer<RibbonSection> {
    
    public void render( RibbonSection widget ) {
        boolean horizontal = widget.getOrientation() == RibbonSection.Orientation.HORIZONTAL;

        output.start( "span" )
            .attr( "class", horizontal ? "horizontal" : "vertical" );
        
        output.start( "table" ).attr( "class", "ribbonsection " + ( horizontal ? "horizontal" : "vertical" ) );
        if( ! horizontal ) {
            output.start( "tr" );
        }
        
        for( Widget child: widget.getChildren() ) {

            if( horizontal ) {
                output.start( "tr" );
            }
            
            output.start( "td" ).attr( "valign", "top" );
            produce( child );
            output.end( "td" );

            if( horizontal ) {
                output.end( "tr" );
            }

        }

        if( ! horizontal ) {
            output.end( "tr" );
        }
        
        output.end( "table" );
        
        output.end();
    }

}