package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.BaseFormWidget;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.Widget;

public abstract class BaseFormWidgetProducer<T extends BaseFormWidget> extends HTMLProducer<T> {

    public void analyze( T widget ) {
        super.analyze( widget );
        Label label = widget.getLabel();
        if( label != null ) {
            if( label.getName() != null ) {
                Changes changes = label.model().changes();
                if( changes.size() > 0 ) {
                    analysis.propose( (HTMLProducer) factory.pick( label ), label, changes.toList() );
                }
            }                
        }
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "problem".equals( name ) ) {
            return json.property( "problem", dumpProblem( widget.getProblem() ) );
        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.getBorder() ) );
        } else if( "font".equals( name ) ) {
            return json.property( "font", FontConverter.dumpCSS( this, widget.getFont() ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected Object dumpProblem( Exception problem ) {
        if( problem != null ) {
            
            if( problem instanceof Flavored ) {
                ObjectNode result = json.object();
                result.put( "message", problem.getMessage() );
                result.put( "flavor", dumpFlavor( (Flavored) problem ) );
                return result;
            } else {
                return problem.getMessage();
            }
            
        } else {
            return null;
        }
    }
    
}