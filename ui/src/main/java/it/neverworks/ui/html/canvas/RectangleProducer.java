package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;
import it.neverworks.ui.canvas.Fill;
import it.neverworks.ui.canvas.Rectangle;

public class RectangleProducer extends ShapeProducer<Rectangle> {

    public ObjectNode dump( Rectangle shape ) {
        ObjectNode node = super.dump( shape );
        
        node.put( "_", "rc" );

        setSize( node, "w", shape.get( "width" ) );
        setSize( node, "h", shape.get( "height" ) );
        setInteger( node, "r", shape.get( "radius" ) );

        return node;
    }


}