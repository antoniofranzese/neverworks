package it.neverworks.ui.html.runtime;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import net.sf.uadetector.ReadableDeviceCategory;
import net.sf.uadetector.ReadableUserAgent;
import net.sf.uadetector.OperatingSystemFamily;
import net.sf.uadetector.OperatingSystem;
import net.sf.uadetector.UserAgentType;
import net.sf.uadetector.UserAgentFamily;

import it.neverworks.lang.Wrapper;
import it.neverworks.model.BaseModel;
import it.neverworks.ui.runtime.Device;

public class UserAgentModule extends BaseModel implements Runnable {
    
    private final static UserAgentAttribute USER_AGENT = new UserAgentAttribute();

    private final static Wrapper IOS = new CachingWrapper( "NW3:USER-AGENT:IOS", new Wrapper() {
        public Object asObject() {
            OperatingSystem system = ua().getOperatingSystem();

            if( system.getFamily() == OperatingSystemFamily.IOS ) {
                return Integer.parseInt( system.getVersionNumber().getMajor() + system.getVersionNumber().getMinor() );
            } else {
                return null;
            }
        }
    });

    private final static Wrapper ANDROID = new CachingWrapper( "NW3:USER-AGENT:ANDROID", new Wrapper() {
        public Object asObject() {
            OperatingSystem system = ua().getOperatingSystem();

            if( system.getFamily() == OperatingSystemFamily.ANDROID ) {
                return Integer.parseInt( system.getVersionNumber().getMajor() + system.getVersionNumber().getMinor() );
            } else {
                return null;
            }
        }
    });

    private final static Wrapper WINDOWS = new CachingWrapper( "NW3:USER-AGENT:WINDOWS", new Wrapper() {
        public Object asObject() {
            OperatingSystem system = ua().getOperatingSystem();

            if( system.getFamily() == OperatingSystemFamily.WINDOWS ) {
                return Integer.parseInt( system.getVersionNumber().getMajor() + system.getVersionNumber().getMinor() );
            } else {
                return null;
            }
        }
    });

    private final static Wrapper TABLET = new CachingWrapper( "NW3:USER-AGENT:TABLET", new Wrapper() {
        public Object asObject() {
            return ua().getDeviceCategory().getCategory() == ReadableDeviceCategory.Category.TABLET;
        }
    });

    private final static Wrapper PHONE = new CachingWrapper( "NW3:USER-AGENT:PHONE", new Wrapper() {
        public Object asObject() {
            return ua().getDeviceCategory().getCategory() == ReadableDeviceCategory.Category.SMARTPHONE;
        }
    });

    private final static Wrapper MOBILE = new CachingWrapper( "NW3:USER-AGENT:MOBILE", new Wrapper() {
        public Object asObject() {
            return ua().getType() == UserAgentType.MOBILE_BROWSER;
        }
    });
    
    private final static Wrapper DESKTOP = new CachingWrapper( "NW3:USER-AGENT:DESKTOP", new Wrapper() {
        public Object asObject() {
            return ua().getType() == UserAgentType.BROWSER;
        }
    });

    private final static Wrapper CHROME = new CachingWrapper( "NW3:USER-AGENT:CHROME", new Wrapper() {
        public Object asObject() {
            System.out.println( "CHROME" );
            ReadableUserAgent agent = ua();
            UserAgentFamily family = agent.getFamily();
            if( family == UserAgentFamily.CHROME || family == UserAgentFamily.CHROME_MOBILE ) {
                return agent.getVersionNumber().getMajor();
            } else {
                return null;
            }
        }
    });

    private final static Wrapper SAFARI = new CachingWrapper( "NW3:USER-AGENT:SAFARI", new Wrapper() {
        public Object asObject() {
            ReadableUserAgent agent = ua();
            UserAgentFamily family = agent.getFamily();
            if( family == UserAgentFamily.SAFARI || family == UserAgentFamily.MOBILE_SAFARI ) {
                return agent.getVersionNumber().getMajor();
            } else {
                return null;
            }
        }
    });


    private final static Wrapper ELECTRON = new CachingWrapper( "NW3:USER-AGENT:ELECTRON", new Wrapper() {
        private Pattern REGEX = Pattern.compile( ".*Electron/([0-9\\.]*).*" );

        public Object asObject() {
            Matcher m = REGEX.matcher( Device.get( "header[user-agent]" ) );
            if( m.matches() ) {
                return m.group( 1 );
            } else {
                return null;
            }
        }
    });
    
    private static ReadableUserAgent ua() {
        return (ReadableUserAgent) USER_AGENT.asObject();
    }
    
    public void run() {
        Device.register( "userAgent", USER_AGENT );
        Device.register( "ios",       IOS );
        Device.register( "android",   ANDROID );
        Device.register( "windows",   WINDOWS );
        Device.register( "tablet",    TABLET );
        Device.register( "phone",     PHONE );
        Device.register( "mobile",    MOBILE );
        Device.register( "touch",     MOBILE );
        Device.register( "desktop",   DESKTOP );
        Device.register( "chrome",    CHROME ); 
        Device.register( "safari",    SAFARI ); 
        Device.register( "electron",  ELECTRON );
    }

}