package it.neverworks.ui.html.data;

import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.io.FileRepository;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.LocalFileArtifact;
import it.neverworks.ui.data.FileBadgeArtifact;

public class LocalFileArtifactResolver extends FileBadgeArtifactResolver {
    
    public Object resolveArtifact( Artifact artifact ) {
        return super.resolveArtifact( new FileBadgeArtifact( getRepository().putFile(( (LocalFileArtifact) artifact).getFile() ) ) );
    }

    @Property @Required @Inject( value = { "it.neverworks.ui.FileRepository", "it.neverworks.FileRepository" }, lazy = true )
    private FileRepository repository;
    
    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }

}