package it.neverworks.ui.html.data;

import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.RawArtifact;

public class RawArtifactResolver implements ArtifactResolver {
    
    public Object resolveArtifact( Artifact artifact ) {
        return artifact;
    }

}