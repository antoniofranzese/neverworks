package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Border;
import it.neverworks.ui.Widget;

public class BorderSideConverter extends BaseConverter {
    
    public static String toCSS( Object value ) {
        if( value != null ) {
            Border.Side side = (Border.Side) value;
            StringBuilder definition = new StringBuilder();

            // Corregge lo spessore del bordo in base allo stile
            if( side.getStyle() == Border.Style.DOUBLE && 
                ( side.getSize() == null || side.getSize() < 3 ) ) {

                side.setSize( 3 );

            } else if( ( side.getStyle() != null || side.getColor() != null ) && side.getSize() == null ) {
                side.setSize( 1 );
            }
            
            definition.append( side.getSize() != null ? side.getSize() : 0 ).append( "px " );
        
            if( side.getStyle() != null ) {
                switch( side.getStyle() ) {
                    case DASHED:
                        definition.append( "dashed " );
                        break;
                    case DOTTED:
                        definition.append( "dotted " );
                        break;
                    case DOUBLE:
                        definition.append( "double " );
                        break;
                    default:
                        definition.append( "solid " );
                }
            } else {
                definition.append( "solid " );
            }
        
            definition.append( side.getColor() != null ? ColorConverter.toCSS( side.getColor() ) : "" /* ( side.getSize() != null ? "black" : "transparent" ) */ );
            return definition.toString();
            
        } else {
            return null;
        }

        
    }
    
}