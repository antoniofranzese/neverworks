package it.neverworks.ui.html.production;

import it.neverworks.ui.Widget;

public class PageAdapter {

    private Page page;
    private HTMLProducer producer;
    
    public PageAdapter( HTMLProducer producer, Page page ) {
        this.producer = producer;
        this.page = page;
    }

    public void render( String name ) {
        producer.produce( page.widget( name ) );
    }
    
    public Widget widget( String name ){
        return page.widget( name );
    }
}