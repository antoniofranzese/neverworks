package it.neverworks.ui.html.grid;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.grid.Cell;

public interface CellProducer<T extends Cell> {
    
    ObjectNode dumpColumn( T cell );
    void populateItem( T cell, ObjectNode itemNode, DataSource source, Object item );
    boolean recognizeAttribute( T cell, String name );
    String translateSortAttribute( T cell, String name );
}