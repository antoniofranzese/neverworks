package it.neverworks.ui.html.form;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.form.Select;

public class SelectProducer<T extends Select> extends BaseFormWidgetProducer<T> {
    
    public void render( T widget ) {
        
        Tag start = output.dojo.start( "div" )
            .attr( "type", this.dojoType() )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "startupValue", widget.getValue() )
            .attr( "disabled", widget.getDisabled() ? true : null )
            .attr( "required", widget.getRequired() ? true : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "readonly", widget.getReadonly() ? true : null )
            .attr( "hint", widget.getHint() )
            .attr( "problem", dumpProblem( widget.getProblem() ) )
            .attr( "blankable", widget.getBlank() )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( dumpFlavor( widget ) );
            
        if( widget.getHorizontalAlignment() != null ) {
            start.attr( "horizontalAlignment", dumpHorizontalAlignment( widget.getHorizontalAlignment() ) );
        }
    
        if( widget.getText() != null ) {
            start
                .attr( "searchAttr", "txt" )
                .attr( "labelAttr", "cap" );
        }
    
        if( widget.getEncoding() == Select.Encoding.HTML ) {
            start.attr( "labelType", "html" );
        }
        
        output.start( "script" ).attr( "type", "works/props" );
        
        output.text( dumpItems( widget ).toString() );
        
        output.end( "script" );
        
        output.end();
    }
    
    protected String dojoType() {
        return "works/form/Select";
    }
    
    protected ObjectNode dumpItems( T widget ) {
        ArrayNode results = json.array(); 

        if( widget.getBlank() ) {
            ObjectNode blankNode = json.object();
            blankNode.put( "id", "__BLANK__" );
            blankNode.put( "cap", "" );
            blankNode.put( "txt", "" );
            results.add( blankNode );
        }

        DataSource items = widget.getItems();
        ItemFormatter caption = widget.getCaption();
        ItemFormatter text = widget.getText();
        for( Object item: items ) {
            ObjectNode itemNode = json.object();

            itemNode.put( "id", JSON.js.encode( items.get( item, widget.getKey() ) ) );
            
            String cap = caption != null ? Strings.safe( caption.format( item ) ) : "";
            itemNode.put( "cap", processCaption( widget, cap ) );

            if( text != null ) {
                itemNode.put( "txt", Strings.safe( text.format( item ) ) );
            }

            results.add( itemNode );
        }
        ObjectNode result = json.object();
        result.put( "items", results );
        return result;
    }
    
    protected String processCaption( T widget, String caption ) {
        return caption.replace( "\n", " " );
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( name.equals( "items" ) ) {
            return dumpItems( widget );
        } else if( "horizontalAlignment".equals( name ) ) {
            return json.property( "horizontalAlignment", dumpHorizontalAlignment( widget.getHorizontalAlignment() ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected String dumpHorizontalAlignment( Select.HorizontalAlignment alignment ) {
        switch( alignment ){
            case LEFT:
                return "left";
            case CENTER:
                return "center";
            case RIGHT:
                return "right";
            default:
                return null;
        }
    }
    
}
