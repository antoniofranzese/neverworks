package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.TextArea;
import it.neverworks.ui.Widget;

public class TextAreaProducer extends BaseFormWidgetProducer<TextArea> {
    
    public void render( TextArea widget ) {
        
        String type;
        if( widget.getHeight() == null && widget.getRows() == null ) {
            type = "works/form/TextArea";
        } else {
            type = "works/form/SimpleTextArea";
        }
        
        Tag start = output.dojo.start( "div" )
            .attr( "type", type )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "resize", dumpResizable( widget ) )
            .attr( "readonly", widget.getReadonly() ? true : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "hint", widget.getHint() )
            .attr( "problem", dumpProblem( widget.getProblem() ) )
            .attr( "rows", Strings.valueOf( widget.get( "rows", 3 ) ) )
            .attr( "cols", Strings.valueOf( widget.get( "columns", 20 ) ) )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( "color", widget.getColor() )
            .style( "background-color", widget.getBackground() )
            .style( FontConverter.renderCSS( this, widget.getFont() ) )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( dumpFlavor( widget ) );

        output.text( HTML.encode( widget.getValue() ) );
        
        output.end();

    }
    
    protected ObjectNode dumpProperty( TextArea widget, String name ) {
        if( "value".equals( name ) ) {
            return json.property( "value", widget.getValue() );
        // } else if( "horizontalAlignment".equals( name ) ) {
        //     return json.property( "horizontalAlignment", dumpHorizontalAlignment( widget.getHorizontalAlignment() ) );
        } else if( "color".equals( name ) ) {
            return json.property( "color", ColorConverter.toCSS( widget.get( "color" ) ) );
        } else if( "background".equals( name ) ) {
            return json.property( "backgroundColor", ColorConverter.toCSS( widget.get( "background" ) ) );
        } else if( "resizable".equals( name ) ) {
            return json.property( name, dumpResizable( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }

    protected String dumpResizable( TextArea widget ) {
        if( widget.getResizable() != null ) {
            switch( widget.getResizable() ) {
                case HORIZONTAL:
                    return "horizontal";
                case VERTICAL:
                    return "vertical";
                case BOTH:
                    return "both";
                default:
                    return "none";
            }
        } else {
            return null;
        }
    }
    
}