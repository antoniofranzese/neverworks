package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Border;
import it.neverworks.ui.Widget;
import it.neverworks.lang.Arguments;
import it.neverworks.ui.html.production.HTMLProducer;

public class BorderConverter extends BaseConverter {
    
    public static String toCSS( Object value ) {

        if( value != null ) {
            String result = toCSS( renderCSS( null, value ) );
            return result.length() > 0 ? "custom;" + result : null;

        } else {
            return null;
        }
                            
    }
    
    public static Arguments renderCSS( HTMLProducer producer, Object value ) {

        if( value != null ) {
            Arguments result = new Arguments();
 
            Border border = (Border) value;
        
            if( border.getTop() != null ) {
                result.arg( "border-top", BorderSideConverter.toCSS( border.getTop() ) );
            }
            if( border.getBottom() != null ) {
                result.arg( "border-bottom", BorderSideConverter.toCSS( border.getBottom() ) );
            }
            if( border.getLeft() != null ) {
                result.arg( "border-left", BorderSideConverter.toCSS( border.getLeft() ) );
            }
            if( border.getRight() != null ) {
                result.arg( "border-right", BorderSideConverter.toCSS( border.getRight() ) );
            }

            return result;

        } else {
            return null;
        }
                            
    }

    public static Arguments dumpCSS( HTMLProducer producer, Object value ) {
        Arguments result = renderCSS( producer, value );

        if( result != null ) {
            return result;

        } else {
            return new Arguments().arg( "border", null );
        }
                            
    }
    
}