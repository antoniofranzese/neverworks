package it.neverworks.ui.html.layout;

import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.description.Changes;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.ui.Widget;
import it.neverworks.ui.layout.PinboardLayout;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.StructureCapable;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.LabelCapable;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.EmbeddableProducer;
import it.neverworks.ui.html.production.BaseContainerProducer;
import it.neverworks.ui.html.production.BaseRedrawableContainerProducer;
import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ScrollabilityConverter;

public class PinboardLayoutProducer extends BaseRedrawableContainerProducer<PinboardLayout> {
    
    private final static LayoutInfo DEFAULT_LAYOUT = new LayoutInfo();
    private final static TypeDefinition<Box> BoxType = type( Box.class );
    private final static TypeDefinition<Size> SizeType = type( Size.class );
    private final static TypeDefinition<Border> BorderType = type( Border.class );
    private final static TypeDefinition<Color> ColorType = type( Color.class );
    private final static TypeDefinition<Integer> IntegerType = type( Integer.class );
    private final static TypeDefinition<Boolean> BooleanType = type( Boolean.class );
    private final static TypeDefinition<StyleArtifact> StyleArtifactType = type( StyleArtifact.class );
    private final static TypeDefinition<PinboardLayout.CellVerticalAlignment> ValignType = type( PinboardLayout.CellVerticalAlignment.class );
    
    private class RenderingContext {
        public int column;
        public List<PinboardLayout.Column> columns;
        public boolean hasColumns;
    }
    
    public void render( PinboardLayout widget ){

        boolean redraw = redrawing( widget );

        RenderingContext ctx = new RenderingContext();
        ctx.column = 0;
        ctx.columns = widget.get( "columns" );
        ctx.hasColumns = widget.get( "columns" ) != null && widget.value( "columns.size" ).gt( 0 );
        
        if( !redraw ) {
            Tag start = output.dojo.start( "div")
                .attr( "type", "works/layout/PinboardLayout" )
                .attr( "name", widget.getName() )
                .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
                .attr( "visible", widget.getVisible() ? null : false )
                .attr( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) )
                .attr( BorderLayoutProducer.attributes( this, widget ) )
                .attr( "spacing", SizeConverter.toCSS( this, widget.get( "spacing" ) ) )
                .style( "background-color", widget.getBackground() )
                .style( "color", widget.getColor() )
                .style( "padding", widget.getPadding() )
                .style( "margin", widget.getMargin() )
                .style( "width", widget.getWidth() )
                .style( "height", widget.getHeight() )
                .style( "font", FontConverter.toCSS( widget.get( "font" ) ) )
                .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
                .style( dumpFlavor( widget ) )
                .style( widget.get( "columns" ) != null ? "columns" : null );

            if( ctx.hasColumns ) {
                start.attr( "columns", widget
                    .each( PinboardLayout.Column.class ).in( "columns" )
                    .map( c -> {
                        return arg( "width", SizeConverter.toCSS( this, c.get( "width" ) ) );
                    })
                    .list() 
                );
            }
        } 
        

        for( Widget child: widget.getChildren() ) {
            Label label = child instanceof LabelCapable ? child.get( "label" ) : null;

            if( label != null && ! label.value( "position" ).is( "after" ) ) {
                renderCell( label, ctx, arg( "style", "label" ) );
            }

            renderCell( child, ctx, null );

            if( label != null && label.value( "position" ).is( "after" ) ) {
                renderCell( label, ctx, arg( "style", "label" ) );
            }
        }
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    protected void renderCell( Widget child, RenderingContext ctx, Arguments arguments ) {

        LayoutInfo layout = child instanceof LayoutCapable ? child.<LayoutInfo>get( "layout", DEFAULT_LAYOUT ) : DEFAULT_LAYOUT;
        String childName = child.getCommonName();
        
        Size top       = SizeType.process( layout.get( "!top", layout.get( "!y" ) ), "{0}.{1}", childName, "layout.(top|x)" );
        Size left      = SizeType.process( layout.get( "!left", layout.get( "!x" ) ), "{0}.{1}", childName, "layout.(left|y)" );
        Size bottom    = SizeType.process( layout.get( "!bottom" ), "{0}.{1}", childName, "layout.bottom" );
        Size right     = SizeType.process( layout.get( "!right" ), "{0}.{1}", childName, "layout.right" );
        Integer zIndex = IntegerType.process( layout.get( "!z" ), "{0}.{1}", childName, "layout.z" );
        boolean brk    = BooleanType.process( layout.get( "!break", false ), "{0}.{1}", childName, "layout.break" );

        if( bottom == null && top != null && top.getValue() < 0 ) {
            top.setValue( top.getValue() * -1 );
            bottom = top;
            top = null;
        }

        if( right == null && left != null && left.getValue() < 0 ) {
            left.setValue( left.getValue() * -1 );
            right = left;
            left = null;
        }
        
        String position = ( top != null || bottom != null || left != null || right != null ) ? "absolute" : null;
        
        boolean hidden = child.model().descriptor().has( "visible", Boolean.class ) && Boolean.FALSE.equals( child.get( "visible" ) ) ? true : false; 

        Tag cell = output.start( "div" )
            .style( "pinboardCell" )
            .style( hidden ? "hidden" : null )
            .style( "width",                   SizeType.process( layout.get( "!width" ),           "{0}.{1}", childName, "layout.width" ) )
            .style( "height",                  SizeType.process( layout.get( "!height" ),          "{0}.{1}", childName, "layout.height" ) )
            .style( "z-index",                 zIndex )
            .style( "padding",                 BoxType.process( layout.get( "!padding" ),          "{0}.{1}", childName, "layout.padding" ) )
            .style( "margin",                  BoxType.process( layout.get( "!margin" ),           "{0}.{1}", childName, "layout.margin" ) )
            .style( "color",                   ColorType.process( layout.get( "!color" ),          "{0}.{1}", childName, "layout.color" ) )
            .style( "background",              ColorType.process( layout.get( "!background" ),     "{0}.{1}", childName, "layout.background" ) )
            .style( BorderConverter.renderCSS( this, BorderType.process( layout.get( "!border" ),  "{0}.{1}", childName, "layout.border" ) ) )
            .style( dumpFlavor(                StyleArtifactType.process( layout.get( "!flavor" ), "{0}.{1}", childName, "layout.flavor" ) ) );

        if( top != null || bottom != null || left != null || right != null ) {
            cell.style( "absolute" );
        } else {
            cell.style( "inline" )
                .style( "vertical-align", dumpValign( ValignType.process( layout.get( "!valign" ), "{0}.{1}", childName, "layout.valign" ) ) );

            if( ctx.hasColumns ) {
                cell.style( "col" )
                    .style( "col-" + ctx.column++ );
                if( brk || ctx.column >= ctx.columns.size() ) {
                    ctx.column = 0;
                }
            } else {
                if( brk ) {
                    cell.style( "break" );
                }
            }
        }
        
        if( bottom != null ) {
            cell.style( "bottom", bottom );
        } else {
            cell.style( "top", top );
        }
        
        if( right != null ) {
            cell.style( "right", right );
        } else {
            cell.style( "left", left );
        }
        
        if( arguments != null && arguments.has( "style" ) ) {
            cell.style( arguments.<String>get( "style" ) );
        }

        produce( child );
        
        output.end( "div" );

    }

    protected boolean containerNeedsRedraw( PinboardLayout widget ) {
        return widget.model().changes().containsAny( "children" );
    }

    protected String dumpValign( PinboardLayout.CellVerticalAlignment valign ) {
        if( valign != null ) {
            switch( valign ) {
                case TOP:
                    return "top";
                case BOTTOM:
                    return "bottom";
                default:
                    return "middle";
            }
        } else {
            return null;
        }
    }

    protected void analyzeWidget( PinboardLayout widget ) {
        Changes changes = widget.model().changes().clone().remove( "children" );
        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }
    }

    protected ObjectNode dumpProperty( PinboardLayout widget, String name ) {
        if( "scrollable".equals( name ) ) {
            return json.property( name, ScrollabilityConverter.toValue( widget.getScrollable() ) );
        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.toCSS( BorderType.process( widget.get( "border" ), "{0}.{1}" , widget.getCommonName(), "layout.border" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    private final static String[] SURVIVING_PROPERTIES = new String[]{ "scrollable", "border" };
        
    protected String[] getSurvivingProperties() {
        return SURVIVING_PROPERTIES;
    }
    // public JsonNode dump( ChangeProposal proposal ) {
    //     ObjectNode result = json.createObjectNode();
    //
    //     if( redrawRequested ) {
    //         result.put( "content", redraw( (PinboardLayout)proposal.getWidget() ) );
    //     }
    //
    //     return result;
    // }

}
