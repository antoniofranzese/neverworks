package it.neverworks.ui.html.production.storage;

import static it.neverworks.language.*;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.DisposableBean;

import it.neverworks.lang.Strings;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.ui.lifecycle.Destroyable;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.MissingStoredWidgetException;
import it.neverworks.ui.Widget;

public class ExpiringStorage<T extends Widget> extends BaseStorage<T> implements DisposableBean {

    private class WidgetInfo {
 
        private T widget;
        private Date last;

        public WidgetInfo( T widget ) {
            this.widget = widget;
            this.touch();
        }

        public void touch() {
            this.last = new Date();
        }

        public void update( T widget ) {
            this.widget = widget;
        }

        public boolean isExpired() {
            return ( new Date().getTime() - this.last.getTime() ) > maxAge;
        }
    }

    private class CleanupTask extends TimerTask {
        
        public void run() {
            widgets.keys().inspect( key -> {
                WidgetInfo info = widgets.get( key );
                if( info != null && info.isExpired() ) {
                    synchronized( this ) {
                        if( info.isExpired() ) {
                            widgets.remove( key );
                        }
                    }
                }
            });
        }
    
    }

    protected FluentMap<String,WidgetInfo> widgets = new FluentHashMap<>();
    protected int maxAge = 86_400_000;
    protected int cleanupDelay = 120; 
    protected int cleanupPeriod = 3600; 
    protected Timer cleanupTimer;
   
    public ExpiringStorage() {
        super();
        this.cleanupTimer = new Timer( /* daemon */ true );
        this.cleanupTimer.scheduleAtFixedRate( new CleanupTask(), this.cleanupDelay * 1000, this.cleanupPeriod * 1000 );
    }

    public ExpiringStorage( int maxAge ) {
        this();
        this.setMaxAge( maxAge );
    }

    protected WidgetInfo info( String key ) {
        WidgetInfo info = widgets.get( key );
        if( info != null ) {
            if( info.isExpired() ) {
                synchronized( this ) {
                    if( info.isExpired() ) {
                        return null;
                    }
                }
            }
            info.touch();
        }
        return info;
    }
 
    public boolean contains( String key ) {
        WidgetInfo info = info( key );
        return info != null;
    }
    
    public T load( String key ) {
        //System.out.println( "form count: " + widgets.size() );
        WidgetInfo info = info( key );
        if( info != null ) {
            return info.widget;
        } else {
            throw new MissingStoredWidgetException( "Widget with key '" + key + "' is not stored" );
        }
    }
    
    protected Iterable<T> all() {
        return widgets
            .assets()
            .filter( info -> ! info.isExpired() )
            .map( info -> info.widget );
    }

    public void save( String key, T widget ) {
        WidgetInfo info = info( key );
        if( info != null ) {
            info.update( widget );    
        } else {
            widgets.put( key, new WidgetInfo( widget ) );
        }
    }
    
    public void delete( String key ) {
        WidgetInfo info = widgets.get( key );
        if( info != null ) {
            if( info.widget instanceof Destroyable ) {
                //System.out.println( "Destroying " + info.widget );
                info.widget.signal( "destroy" ).fire( arg( "mandatory", true ) );
            }
            widgets.remove( key );
        }
    }

    public void setMaxAge( int maxAge ){
        this.maxAge = maxAge * 1000;
    }
    
    public void destroy() {
        widgets.keys().inspect( k -> delete( k ) );
        widgets = null;
        cleanupTimer.cancel();
        cleanupTimer = null;
    }

    @Override
    protected void finalize() throws Throwable {
        if( cleanupTimer != null ) {
            cleanupTimer.cancel();
        }
    }
    
    public void setCleanupPeriod( int period ) {
        this.cleanupPeriod = period;
    }

    public void setCleanupDelay( int delay ) {
        this.cleanupDelay = delay;
    }
}