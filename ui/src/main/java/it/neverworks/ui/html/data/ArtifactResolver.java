package it.neverworks.ui.html.data;

import it.neverworks.ui.data.Artifact;

public interface ArtifactResolver {
    public Object resolveArtifact( Artifact artifact );
}