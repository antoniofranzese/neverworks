package it.neverworks.ui.html.server;

import javax.servlet.ServletContextEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import it.neverworks.lang.Framework;
import it.neverworks.lang.Application;
import it.neverworks.context.spring.WebContextLoader;
import it.neverworks.app.ApplicationStartupEvent;
import it.neverworks.app.ApplicationShutdownEvent;
import it.neverworks.httpd.SessionCreatedEvent;
import it.neverworks.httpd.SessionDestroyedEvent;
import it.neverworks.httpd.j2ee.HttpSessionMapAdapter;

public class SpringContextLoaderListener extends ContextLoaderListener implements HttpSessionListener {

    static {
        Framework.boot();
    }

    // private WebApplicationContext wac;

    public SpringContextLoaderListener() {
        super();
    }

    public SpringContextLoaderListener( WebApplicationContext wac ) {
        super( wac );
    }

    @Override
    public void contextInitialized( ServletContextEvent event ) {
        System.setProperty( "web.root", event.getServletContext().getContextPath() );
        Application.setProperty( "web.root", event.getServletContext().getContextPath() );
        super.contextInitialized( event );
        //this.wac = WebApplicationContextUtils.getRequiredWebApplicationContext( event.getServletContext() );
        // if( this.wac != null ) {
        //     this.wac.publishEvent( new ApplicationStartupEvent( this ) );
        // }
    }

    @Override
    public void contextDestroyed( ServletContextEvent event ) {
        // if( this.wac != null ) {
        //     this.wac.publishEvent( new ApplicationShutdownEvent( this ) );
        // }
        super.contextDestroyed( event );
    }

    public void sessionCreated( HttpSessionEvent event ) {
        if( this.getCurrentWebApplicationContext() != null ) {
            this.getCurrentWebApplicationContext().publishEvent( new SessionCreatedEvent( this, new HttpSessionMapAdapter( event.getSession() ) ) );
        }
    }

    public void sessionDestroyed( HttpSessionEvent event ) {
        if( this.getCurrentWebApplicationContext() != null ) {
            this.getCurrentWebApplicationContext().publishEvent( new SessionDestroyedEvent( this, new HttpSessionMapAdapter( event.getSession() ) ) );
        }

    }
    
    // @Override
    // protected ContextLoader createContextLoader(){
    //     return new WebContextLoader();
    // }
    
}