package it.neverworks.ui.html.types;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;

public abstract class BaseConverter  {
    
    protected static String toCSS( Arguments arguments ) {
        if( arguments != null ) {
            StringBuilder result = new StringBuilder( 256 );
            for( String name: arguments.names() ) {
                result.append( name ).append( ":" ).append( Strings.valueOf( arguments.get( name ) ) ).append( ";" );
            }
            return result.toString();
        } else {
            return null;
        }
    }
}