package it.neverworks.ui.html.server.detectors;

import static it.neverworks.language.*;

import it.neverworks.log.Logger;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.codec.binary.Base64;

import it.neverworks.lang.Strings;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.security.model.AuthenticatedUser;

public class BasicAuthUserDetector implements UserDetector {
    
	private static final Logger logger = Logger.of( "AUTH" );

    private String header;

    public static class BasicAuthUser implements AuthenticatedUser {

        private String userID;

        public BasicAuthUser( String userID ) {
            this.userID = userID;
        }

        public String getUserID(){
            return this.userID;
        }
        
        public String getId(){
            return this.userID;
        }
        
        public boolean equals( Object other ) {
            return other instanceof BasicAuthUser 
                && this.userID != null 
                && this.userID.equals( ((BasicAuthUser) other).getUserID() );
        }

        public String toString() {
            return new ToStringBuilder( this ).add( "id" ).toString();
        }
        
    }
    
    public AuthenticatedUser detect( HttpServletRequest request ) {
        String userID = null;
        
        String auth = request.getHeader( "Authorization" );
        if( auth != null ) {
            if( auth.startsWith( "Basic" ) ) {
                String credentials = new String( new Base64().decode( auth.substring( 6 ).getBytes() ) );
                if( Strings.hasText( credentials ) ) {
                    
                    int index = credentials.indexOf( ":" );
                    if( index > 0 ) {
                        userID = credentials.substring( 0, index ).trim();
                    } else {
                        userID = credentials.trim();
                    }
                
                }
            } else {
                if( logger.isDebugEnabled() ) {
                    logger.debug( "Unknown Authorization: {}", auth );
                }
            }
        } else {
            if( logger.isDebugEnabled() ) {
                logger.debug( "Missing Basic Authorization" );
            }
        }
        
        if( Strings.hasText( userID ) ) {
            return new BasicAuthUser( userID.trim() );
        } else {
            return null;
        }
    }
    
    public void init( String specification ) {
        int index = specification.indexOf( ":" );
        if( index > 0 ) {
            this.header = specification.substring( index + 1 );
            if( Strings.isEmpty( this.header ) ) {
                throw new IllegalArgumentException( "Missing request header specification"  );
            }
        } else {
            throw new IllegalArgumentException( "Bad request header specification: " + specification );
        }
    }
    
    public String getHeader(){
        return this.header;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "header" ).toString();
    }

}