package it.neverworks.ui.html.server;

import it.neverworks.httpd.Request;

public class DevelopmentFileManager extends EmbeddedFileManager {
    
    protected void streamFile( Request request, String localPath ) throws Exception {
        System.out.println( "Requesting path: " + localPath );
        
        super.streamFile( request, localPath );
    }

}