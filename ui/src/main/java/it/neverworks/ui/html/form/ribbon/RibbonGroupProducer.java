package it.neverworks.ui.html.form.ribbon;

import it.neverworks.encoding.HTML;
import it.neverworks.ui.form.ribbon.RibbonGroup;
import it.neverworks.ui.html.production.BaseContainerProducer;
import it.neverworks.ui.Widget;

public class RibbonGroupProducer extends BaseContainerProducer<RibbonGroup> {
    
    public void render( RibbonGroup widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/layout/RibbonPane" )
            .attr( "title", HTML.encode( widget.getTitle() ).replace( " ", "&nbsp;" ) )
            .attr( "position", "bottom" );
            
        output.start( "table" )
            .attr( "class", "ribbongroup" )
            .attr( "align", "center" );
        output.start( "tr" );
            
        for( Widget child: widget.getChildren() ) {
            output.start( "td" ).attr( "valign", "top" );
            produce( child );
            output.end( "td" );
        }
        
        output.end( "tr" );
        output.end( "table" );
    
        output.end();
        
    }

    protected String dumpHorizontalAlignment( RibbonGroup.HorizontalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ) {
            case LEFT:
                return "center";
            case CENTER:
                return "center";
            case RIGHT:
                return "right";
            default:
                return null;
            }
        } else {
            return null;
        }
    }

}