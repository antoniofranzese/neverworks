package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;
import it.neverworks.ui.canvas.Fill;
import it.neverworks.ui.canvas.Line;

public class LineProducer extends ShapeProducer<Line> {

    public ObjectNode dump( Line shape ) {
        ObjectNode node = super.dump( shape );
        
        node.put( "_", "ln" );
        setPoint( node, "or", shape.get( "origin" ) );

        ArrayNode stops = JSON.array();
        for( Point stop: shape.getStops() ) {
            stops.add( getPoint( stop ) );
        }
        node.set( "pts", stops );

        return node;
    }


}