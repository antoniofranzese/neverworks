package it.neverworks.ui.html.production;

import it.neverworks.ui.Widget;

// Interfaccia con la quale i produttori possono mettere a disposizione di
// altri produttori una produzione parziale del widget, allo scopo di essere
// inglobata in altri widget
public interface EmbeddableProducer<T extends Widget> {
    
    void renderForEmbedding( T widget );
    
}