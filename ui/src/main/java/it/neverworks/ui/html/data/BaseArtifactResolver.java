package it.neverworks.ui.html.data;

import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.lang.Collections;
import it.neverworks.ui.data.LocalFileArtifact;
import it.neverworks.ui.data.FileBadgeArtifact;
import it.neverworks.ui.data.FileInfoArtifact;
import it.neverworks.ui.data.StreamArtifact;
import it.neverworks.ui.data.RawArtifact;

public class BaseArtifactResolver extends MappingArtifactResolver {
    
    public BaseArtifactResolver(){
        set( "mappings", list(
            new ArtifactMapping()
                .set( "type", WebStyleArtifact.class )
                .set( "resolver", new WebStyleArtifactResolver() )
            ,new ArtifactMapping()
                .set( "type", WebFileArtifact.class )
                .set( "resolver", new WebFileArtifactResolver() )
            ,new ArtifactMapping()
                .set( "type", StreamArtifact.class )
                .set( "resolver", new StreamArtifactResolver() )
            ,new ArtifactMapping()
                .set( "type", LocalFileArtifact.class )
                .set( "resolver", new LocalFileArtifactResolver() )
            ,new ArtifactMapping()
                .set( "type", FileInfoArtifact.class )
                .set( "resolver", new FileInfoArtifactResolver() )
            ,new ArtifactMapping()
                .set( "type", FileBadgeArtifact.class )
                .set( "resolver", new FileBadgeArtifactResolver() )
            ,new ArtifactMapping()
                .set( "type", RawArtifact.class )
                .set( "resolver", new RawArtifactResolver() )
        ));
    }
    
    public void setPrepend( List<ArtifactMapping> extensions ) {
        for( ArtifactMapping mapping: Collections.reverse( extensions ) ) {
            prepend( mapping );
        }
    }
    
    public void setAppend( List<ArtifactMapping> extensions ) {
        for( ArtifactMapping mapping: extensions ) {
            append( mapping );
        }
    }
    
}