package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Angles;
import it.neverworks.ui.Widget;

public class AnglesConverter extends BaseConverter {
    
    private final static Size ZERO = Size.pixels( 0 );

    public static String toCSS( Object value ) {
        if( value != null ) {
            Angles angles = (Angles) value;
            Size tl = angles.get( "topLeft", ZERO );
            Size tr = angles.get( "topRight", ZERO );
            Size bl = angles.get( "bottomLeft", ZERO );
            Size br = angles.get( "bottomRight", ZERO );

            if( tl.equals( tr ) && tl.equals( bl ) && tl.equals( br ) ) {
                return SizeConverter.toCSS( null, tl );

            } else {
                return SizeConverter.toCSS( null, tl )
                    + " " + SizeConverter.toCSS( null, tr )
                    + " " + SizeConverter.toCSS( null, br )
                    + " " + SizeConverter.toCSS( null, bl );
            }

        } else {
            return null;
        }
    }
    
}