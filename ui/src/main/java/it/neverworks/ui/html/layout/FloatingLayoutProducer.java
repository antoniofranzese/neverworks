package it.neverworks.ui.html.layout;

import static it.neverworks.language.*;

import it.neverworks.encoding.HTML;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.production.BufferedHTMLOutput;
import it.neverworks.ui.html.production.HTMLFactory;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.layout.FloatingLayout;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.PanelCapable;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.Widget;

public class FloatingLayoutProducer<T extends FloatingLayout> extends StackLayoutProducer<T> {
 
    private final static TypeDefinition<Size> SizeType = type( Size.class );
    private final static TypeDefinition<Integer> IntType = type( Integer.class );

    protected String dojoType() {
        return "works/layout/FloatingContainer";
    }

    protected void renderChild( Widget child ) {
        
        boolean isPanel = child instanceof PanelCapable;

        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/layout/FloatingPane" )
            .attr( "title", HTML.encode( getChildTitle( child ) ) );

        if( child instanceof LayoutCapable ) {
            LayoutInfo info = ((LayoutCapable) child).getLayout();
            
            if( info != null ) {
                boolean resizable = info.get( "!resizable", false );
                String width      = SizeConverter.toCSS( this, SizeType.process( info.get( "!width" ) ) );
                String height     = SizeConverter.toCSS( this, SizeType.process( info.get( "!height" ) ) );
                String minWidth   = SizeConverter.toCSS( this, SizeType.process( info.get( "!minWidth" ) ) );
                String minHeight  = SizeConverter.toCSS( this, SizeType.process( info.get( "!minHeight" ) ) );
                
                if( ! resizable ) {
                    if( minWidth == null ) {
                        minWidth = width;
                    }
                    if( minHeight == null ) {
                        minHeight = height;
                    }
                }
                
                start
                    .attr( "closable", info.contains( "closable" ) ? info.get( "closable" ) : false )
                    .attr( "resizable", resizable )
                    .attr( "maxable", info.contains( "resizable" ) ? info.get( "resizable" ) : false )
                    .attr( "minWidth", minWidth )
                    .attr( "minHeight", minHeight )
                    .attr( "width", width )
                    .attr( "height", height )
                    .style( "width", width )
                    .style( "height", height )
                    .attr( "x", IntType.process( info.get( "!x" ) ) )
                    .attr( "y", IntType.process( info.get( "!y" ) ) );
            }

        } else {
            
            ModelDescriptor descriptor = child.model().descriptor();
 
            if( descriptor.has( "closable", Boolean.class ) ) {
                start.attr( "closable", child.get( "closable" ) );
            } 
 
            if( descriptor.has( "resizable", Boolean.class ) ) {
                start.attr( "resizable", child.get( "resizable" ) );
            }
 
            if( descriptor.has( "origin", Point.class ) ) {
                start
                    .attr( "x", child.get( "!origin.x" ) )
                    .attr( "y", child.get( "!origin.y" ) );
            }
            
        }
        
        if( !isPanel ) {
            output.dojo.start( "div" )
                .attr( "type", "works/layout/WrappingContentPane" );
        }

        produce( child );
    
        if( !isPanel ) {
            output.end();
        }
        
        output.end();
    }
    
    protected String drawChild( Widget widget ) {
        BufferedHTMLOutput bufferedOutput = new BufferedHTMLOutput();
        
        FloatingLayoutProducer producer = (FloatingLayoutProducer) ((HTMLFactory) factory).with( context, bufferedOutput ).pick( new FloatingLayout() );
        producer.renderChild( widget );
        
        return bufferedOutput.getBuffer();
    }

    
}