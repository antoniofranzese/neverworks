package it.neverworks.ui.html.form;

import it.neverworks.ui.form.PasswordTextBox;
import it.neverworks.ui.html.production.Tag;

public class PasswordTextBoxProducer extends BaseTextBoxProducer<PasswordTextBox> {
    
    @Override
    public Tag start( PasswordTextBox widget ) {
        return super.start( widget )
            .attr( "revealed", widget.getRevealed() );
    }
    
    @Override
    protected String dojoType( PasswordTextBox widget ) {
        return "works/form/PasswordTextBox";
    }
    
}