package it.neverworks.ui.html.production;

import it.neverworks.io.StringWriter;

public class BufferedHTMLOutput extends HTMLOutput {
    
    public BufferedHTMLOutput() {
        super( new StringWriter() );
    }

    public String getBuffer() {
        return this.writer.toString();
    }
    
}