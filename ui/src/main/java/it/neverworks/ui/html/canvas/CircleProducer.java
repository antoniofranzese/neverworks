package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;
import it.neverworks.ui.canvas.Circle;

public class CircleProducer<T extends Circle> extends ShapeProducer<T> {

    public ObjectNode dump( T shape ) {
        ObjectNode node = super.dump( shape );
        
        node.put( "_", "cr" );

        setInteger( node, "r", shape.get( "radius" ) );
        setInteger( node, "fr", shape.get( "from" ) );
        setInteger( node, "to", shape.get( "to" ) );

        return node;
    }


}