package it.neverworks.ui.html.server;

import static it.neverworks.language.*;

import java.util.Date;
import java.util.regex.Matcher;
import java.io.Writer;
import java.net.URLDecoder;

import it.neverworks.lang.Threads;
import it.neverworks.io.Streams;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Framework;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.httpd.Request;
import it.neverworks.httpd.Response;
import it.neverworks.httpd.Method;
import it.neverworks.httpd.PathMatchingHandler;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.ui.production.Context;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.html.runtime.UserAgentModule;
import it.neverworks.ui.html.runtime.ClientConfigModule;
import it.neverworks.ui.html.production.ProtocolManager;

public class EmbeddedProtocolHandler extends PathMatchingHandler {
    
    static {
        Device.registerModule( new ClientConfigModule() );
        Device.registerModule( new UserAgentModule() );
    }

    public void handle( Request request ) throws Exception {
        boolean gzipped = false;
        
        try {
            Matcher matcher = regex.matcher( request.getPath() );
            if( matcher.matches() ) {
                if( matcher.groupCount() == 1 ) {
                    long start = System.currentTimeMillis();
                    
                    Response response = request.getResponse();
                    String formPath = matcher.group( 1 );

                    if( simulateRedirection 
                        && request.getMethod() == Method.POST
                        && !"0".equals( request.getHeader( "X-NWK-TTL" ) ) ) {
                        
                        response.setCode( "302 Moved Temporarily" );    
                        response.setHeader( "Location", request.getPath() );
                        if( logging() ) log( "Redirecting to " + request.getPath() );
                        
                    } else {
                        
                        response.setCode( "200" );

                        String webRoot = System.getProperty( "web.root" );
                        if( webRoot == null ) {
                            webRoot = "";
                        }
                    
                        Context context = new Context();
                        context.set( "FormPath", formPath );

                        context.set( "Environment", "development" );
                        context.set( "Timestamp", start );
                        

                        if( request.getMethod() == Method.HEAD ) {
                            context.set( "Method", request.getHeader( "X-NWK-Method" ) );

                            Arguments params = new Arguments();
                            context.put( "Parameters", params );
                            for( String rawHeader: request.getHeaders().keySet() ) {
                                String header = rawHeader.toLowerCase();
                                if( header.startsWith( "x-nwk-" ) ) {
                                    params.put( header.substring( 6 ), request.getHeader( rawHeader ) );
                                }
                            }
                            
                            protocolManager.probe( context );

                            response.setHeader( "X-NWK-Method", context.get( "Method" ) );
                            if( context.contains( "Return" ) ) {
                                Arguments ret = context.get( "Return" );
                                for( String name: ret.names() ) {
                                    response.setHeader( "X-NWK-" + Strings.capitalizeFirst( name ), Strings.valueOf( ret.get( name ) ) );
                                }
                            }

                            if( logging() ) {
                                log( "Polling cycle: " + ( System.currentTimeMillis() - start ) + " ms" );
                            }
                        
                        } else {
                            if( gzip ) {
                                try {
                                    context.set( "GZipContent", request.getHeader( "Accept-Encoding" ).indexOf( "gzip" ) >= 0 );
                                } catch( Exception ex ) {
                                    context.set( "GZipContent", false );
                                }
                            } else {
                                context.set( "GZipContent", false );
                            }
                        
                            if( context.<Boolean>get( "GZipContent" ) ) {
                                response.setHeader( "Content-Encoding", "gzip" );
                                gzipped = true;
                            }

                            response.setHeader( "X-NWK-Environment", "development" );
  
                            context.set( "ApplicationPath", webRoot );
                            context.set( "Web", new ExpandoModel( arg( "Root", webRoot ) ) );
                            context.set( "RequestURL", request.getPath() );
                            context.set( "BaseURL", request.getPath().substring( 0, request.getPath().length() - formPath.length() - 1 ) );
                            context.set( "QueryString", Strings.safe( request.getQueryString() ) );
                    
                            Device.init();
                            Device.register( "header", new EmbeddedHeaderProvider( request ) );

                            if( this.deviceId ) {
                                String id = request.getCookie( "NWK-ID" );
                            
                                if( Strings.isEmpty( id ) ) {
                                    id = DeviceHelper.generateID();
                                }
                            
                                if( request.getMethod() == Method.GET ) {
                                    response.setCookie( "NWK-ID", id, arg( "path", "/" ).arg( "age", DeviceHelper.ID_AGE ) );
                                }

                                Device.set( "id", id );
                            }
                    
                            if( request.getMethod() == Method.GET ) {
                                response.setHeader( "Content-Type", "text/html; charset=UTF-8" );
                                response.setHeader( "X-NWK-Method", "CREATE" );
                                if( context.<Boolean>get( "GZipContent" ) ) {
                                    context.set( "OutputStream", response.getStream() );
                                } else {
                                    context.set( "OutputWriter", response.getWriter() );
                                }
                                protocolManager.create( context );

                            } else if( request.getMethod() == Method.POST ) {
                                response.setHeader( "Content-Type", "application/json; charset=UTF-8" );
                                response.setHeader( "X-NWK-Method", "HANDLE" );
                                context.set( "Body", request.getBodyAsString() );
                                if( context.<Boolean>get( "GZipContent" ) ) {
                                    context.set( "OutputStream", response.getStream() );
                                } else {
                                    context.set( "OutputWriter", response.getWriter() );
                                }
                                protocolManager.handle( context );
                            } 

                            if( logging() ) {
                                log( "Request time: " + ( System.currentTimeMillis() - start ) + " ms (" + ( context.<Boolean>get( "GZipContent" ) ? "gzip" : "plain" ) + ")" );
                            }
                            
                        }

                    }
                
                    response.flush();

                }
            }
            
        } catch( Exception ex ) {
            System.out.println( "ERROR " + ex );
            ErrorPage error = new ErrorPage( ex );
            
            if( gzipped ) {
                Streams.gzip( error.toString(), request.getResponse().getStream() );
            } else {
                request.getResponse().write( error.toString() );
            }
            
            request.getResponse().flush();

        } finally {
            Threads.cleanup();
        } 
        
    }

    protected boolean logging() {
        return false;
    }    

    protected void log( String message ) {
        //System.out.println( message );
    }
    
    private ProtocolManager protocolManager;
    
    public void setProtocolManager( ProtocolManager protocolManager ){
        this.protocolManager = protocolManager;
    }
    
    private boolean simulateRedirection = false; 

    public void setSimulateRedirection( boolean simulateRedirection ){
        this.simulateRedirection = simulateRedirection;
    }
    
    private boolean gzip = true; 
    
    public void setGzip( boolean gzip ){
        this.gzip = gzip;
    }
    
    private boolean deviceId = true;
    
    public void setDeviceId( boolean deviceId ){
        this.deviceId = deviceId;
    }
    
}