package it.neverworks.ui.html.server;

import it.neverworks.log.Logger;
import it.neverworks.lang.Arguments;
import it.neverworks.io.TemporaryFileRepository;

public class TemporaryFileService extends TemporaryFileRepository {
	
    public TemporaryFileService() {
        super();
        setFilePrefix( "NWK-UI-" );
    }

    public TemporaryFileService( Arguments arguments ) {
        this();
        set( arguments );
    }
	
}