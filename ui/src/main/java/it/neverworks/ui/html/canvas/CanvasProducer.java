package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.canvas.Canvas;

public class CanvasProducer extends HTMLProducer<Canvas> {
    
    public void render( Canvas widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/canvas/Canvas" )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "visible", widget.getVisible() )
            .attr( "width", SizeConverter.toCSS( widget.getWidth() ) )
            .attr( "height", SizeConverter.toCSS( widget.getHeight() ) );
            //.style( "padding", widget.getPadding() )
            //.style( "margin", widget.getMargin() );

        if( widget.get( "scene" ) != null ) {
            output.start( "script" ).attr( "type", "works/props" );
            ObjectNode propsNode = JSON.object();
            propsNode.set( "initialScene", SceneProducer.dump( widget.get( "scene" ) ) );
            output.text( JSON.js.print( propsNode ) );
            output.end();    
        }
        
        output.end();

    }

}