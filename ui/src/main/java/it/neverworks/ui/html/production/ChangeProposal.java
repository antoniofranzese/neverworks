package it.neverworks.ui.html.production;

import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.ui.Widget;

public abstract class ChangeProposal {
    /*package*/ HTMLProducer producer;
    /*package*/ Widget widget;
    protected ExpandoModel attributes;
    
    public ChangeProposal( HTMLProducer producer, Widget widget ) {
        this.producer = producer;
        this.widget = widget;
    }
    
    public <W extends Widget> W getWidget(){
        return (W)this.widget;
    }
    
    public HTMLProducer getProducer(){
        return this.producer;
    }

    public ExpandoModel getAttributes(){
        if( this.attributes == null ) {
            this.attributes = new ExpandoModel();
        }
        return this.attributes;
    }
    
    public void setAttributes( ExpandoModel attributes ){
        this.attributes = attributes;
    }

}

