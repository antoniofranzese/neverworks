package it.neverworks.ui.html.web;

import it.neverworks.lang.Strings;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.Widget;
import it.neverworks.ui.web.Page;
import it.neverworks.ui.html.form.FormProducer;

public class PageProducer<T extends Page> extends FormProducer<T> implements Factory {
    
    protected void renderChildren( T form ) {
        output.dojo.start( "div" )
            .attr( "type", "works/form/Redirector" )
            .attr( "name", "$redirector" )
            .attr( "url", ( Boolean.FALSE.equals( form.getLocal() ) ? "" : Strings.safe( this.base ).trim() ) + Strings.safe( form.getPath() ).trim() );
        
        output.dojo.end();
    }
    
    public <T extends Widget> Producer<T> pick( T widget ) {
        return (Producer<T>)this;
    }

    // @Property @Required @Inject({ "it.neverworks.ui.web.Root" })
    private String base;
    
    public String getBase(){
        return this.base;
    }
    
    public void setBase( String base ){
        this.base = base;
    }
}