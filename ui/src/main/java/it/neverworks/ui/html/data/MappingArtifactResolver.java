package it.neverworks.ui.html.data;

import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.production.ProductionException;

public class MappingArtifactResolver extends BaseModel implements ArtifactResolver {
    
    @Property @Collection
    private List<ArtifactMapping> mappings;

    public Object resolveArtifact( Artifact artifact ) {
        if( artifact != null ) {
            for( ArtifactMapping mapping: getMappings() ) {
                if( mapping.getType().isInstance( artifact ) ) {
                    return mapping.getResolver().resolveArtifact( artifact );
                }
            }
            throw new ProductionException( msg( "Cannot resolve artifact: {0} <{1}>", artifact.getName(), artifact.getClass().getName() ) );
        } else {
            return null;
        }
    }
    
    public boolean contains( ArtifactMapping mapping ) {
        for( ArtifactMapping myMapping: getMappings() ) {
            if( myMapping.getType().equals( mapping.getType() ) ) {
                return true;
            }
        }
        return false;
    }
    
    public void prepend( ArtifactMapping mapping ) {
        if( contains( mapping ) ) {
            replace( mapping );
        } else {
            getMappings().add( 0, mapping );
        }
    }

    public void append( ArtifactMapping mapping ) {
        if( contains( mapping ) ) {
            replace( mapping );
        } else {
            getMappings().add( mapping );
        }
    }
    
    public void replace( ArtifactMapping mapping ) {
        for( ArtifactMapping myMapping: getMappings() ) {
            if( myMapping.getType().equals( mapping.getType() ) ) {
                myMapping.setResolver( mapping.getResolver() );
            }
        }
    }
    
    public List<ArtifactMapping> getMappings(){
        return this.mappings;
    }
    
    public void setMappings( List<ArtifactMapping> mappings ){
        this.mappings = mappings;
    }
}