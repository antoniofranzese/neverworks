package it.neverworks.ui.html.data;

import it.neverworks.lang.Strings;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.AbstractCatalog;

public class WebFileCatalog extends AbstractCatalog {
    
    private String baseURL;

    public WebFileCatalog() {
        super();
    }
    
    public WebFileCatalog( String baseURL ) {
        super();
        this.baseURL = baseURL;
    }

    public Artifact get( String name ) {
        if( Strings.safe( name ).startsWith( "/" ) 
            || Strings.safe( name ).startsWith( "http://" )
            || Strings.safe( name ).startsWith( "https://" ) ) {
            return new WebFileArtifact( name, name );
        } else {
            return new WebFileArtifact( name, baseURL + ( baseURL.endsWith( "/" ) ? "" : "/" ) + name );
        }
    }

    public String getBaseURL(){
        return this.baseURL;
    }
    
    public void setBaseURL( String baseURL ){
        this.baseURL = baseURL.endsWith( "/" ) ? baseURL : baseURL + "/";
    }

}