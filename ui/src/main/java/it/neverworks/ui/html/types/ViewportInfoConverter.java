package it.neverworks.ui.html.types;

import static it.neverworks.language.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Arguments;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.types.ViewportInfo;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.Widget;

public class ViewportInfoConverter extends BaseConverter {
    
    private final static TypeDefinition<ViewportInfo> ViewportInfoType = type( ViewportInfo.class );

    public static JsonNode toJSON( HTMLProducer producer, Object object ) {
        ViewportInfo info = ViewportInfoType.process( object );
        
        if( info != null ) {

            Arguments json = new Arguments();
            if( info.get( "visible" ) != null ) {
                json.arg( "visible", info.get( "visible" ) );
            }
            if( info.get( "region" ) != null ) {
                json.arg( "region", info.get( "region" ) );
            }
            if( info.get( "width" ) != null ) {
                json.arg( "width", SizeConverter.toCSS( info.<Size>get( "width" ) ) );
            }
            if( info.get( "height" ) != null ) {
                json.arg( "height", SizeConverter.toCSS( info.<Size>get( "height" ) ) );
            }
            return JSON.js.encode( json );
        } else {
            return null;
        }
    }
    
}
