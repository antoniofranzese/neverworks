package it.neverworks.ui.html.grid;

import it.neverworks.model.description.Changes;
import it.neverworks.ui.Widget;
import it.neverworks.ui.grid.GridRowLayout;
import it.neverworks.ui.layout.LayoutWidget;
import it.neverworks.ui.html.production.BaseRedrawableContainerProducer;

public class GridRowLayoutProducer extends BaseRedrawableContainerProducer<GridRowLayout> {
    
    public void render( GridRowLayout widget ){

        boolean redraw = redrawing( widget );
        
        String widgetName = widget.getCommonName();
        String flavor = dumpFlavor( widget );
        if( !redraw ) {
            output.dojo.start( "div")
                .attr( "type", "works/grid/RowLayout" )
                .attr( "name", widget.getName() )
                .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
                .style( "background-color", widget.getBackground() )
                .style( "height", widget.getHeight() )
                .style( flavor );
        } 
        
        output.start( "table" )
            .style( "gridxRowLayoutTable" )
            .style( flavor );
            
        output.start( "tr" );
        if( widget.getChildren().size() > 0 ) {
            if( widget.getChildren().size() > 1 || !( widget.getChildren().get( 0 ) instanceof LayoutWidget ) ) {
                for( Widget child: widget.getChildren() ) {
                    output.start( "td" )
                        .style( "gridxRowLayoutCell" )
                        .style( flavor );
                    produce( child );
                    output.end();
                }
            } else {
                produce( widget.getChildren().get( 0 ) );
            }
        }
        output.end( "tr" );
        output.end( "table" );
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    
    protected boolean containerNeedsRedraw( GridRowLayout widget ) {
        return widget.model().changes().containsAny( "children" );
    }
    
    protected void analyzeWidget( GridRowLayout widget ) {
        Changes changes = widget.model().changes().clone().remove( "children" );
        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }
    }

    private final static String[] SURVIVING_PROPERTIES = new String[]{};
        
    protected String[] getSurvivingProperties() {
        return SURVIVING_PROPERTIES;
    }

}