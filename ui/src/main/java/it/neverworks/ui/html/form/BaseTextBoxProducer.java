package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.ShadowConverter;
import it.neverworks.ui.html.types.CornersConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.form.BaseTextBox;
import it.neverworks.ui.Widget;

public class BaseTextBoxProducer<T extends BaseTextBox> extends BaseFormWidgetProducer<T> {
    
    public Tag start( T widget ) {
        Tag start = output.dojo.start( "div" )
            .attr( "type", dojoType( widget ) )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "value", value( widget ) )
            
            .attr( "required", widget.getRequired() ? true : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "disabled", widget.getDisabled() ? true : null )
            .attr( "readonly", widget.getReadonly() ? true : null )
            .attr( "sealed", widget.getSealed() ? true : null )

            .attr( switchEvent( widget.getChange() ) )
            .attr( switchEvent( widget.getEnterPress() ) )
            .attr( switchEvent( widget.getTabPress() ) )

            .attr( "color", ColorConverter.toCSS( widget.getColor() ) )
            .attr( "backgroundColor", ColorConverter.toCSS( widget.getBackground() ) )
            .attr( "hint", widget.getHint() )
            .attr( "problem", dumpProblem( widget.getProblem() ) )
            .attr( "shadow", ShadowConverter.renderCSS( this, widget.getShadow() ) )
            .attr( "rounding", CornersConverter.renderCSS( this, "border-radius", widget.getRounding() ) )
            .attr( "title", LabelProducer.renderStaticLabel( widget.getTitle() ) )
            
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "margin", Box.sum( widget.getMargin(), widget.getPadding() ) )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( FontConverter.renderCSS( this, widget.getFont() ) )
            .style( dumpFlavor( widget ) );
            
        if( widget.getHorizontalAlignment() != null ) {
            start.attr( "horizontalAlignment", dumpHorizontalAlignment( widget.getHorizontalAlignment() ) );
        }
        
        return start;
        
    }
    
    public void render( T widget ) {
        
        Tag start = start( widget );
        
        body( widget );
        
        output.end();

    }
    
    protected void body( T widget ) {}
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "value".equals( name ) ) {
            return json.property( "value", value( widget ) );

        } else if( "horizontalAlignment".equals( name ) ) {
            return json.property( "horizontalAlignment", dumpHorizontalAlignment( widget.getHorizontalAlignment() ) );

        } else if( "color".equals( name ) ) {
            return json.property( "color", ColorConverter.toCSS( widget.getColor() ) );

        } else if( "background".equals( name ) ) {
            return json.property( "backgroundColor", ColorConverter.toCSS( widget.getBackground() ) );

        } else if( "title".equals( name ) ) {
            return json.property( "title", LabelProducer.renderStaticLabel( widget.getTitle() ) );

        } else if( "rounding".equals( name ) ) {
            return json.property( "rounding", CornersConverter.dumpCSS( this, "border-radius", widget.getRounding() ) );

        } else if( "shadow".equals( name ) ) {
            return json.property( "shadow", ShadowConverter.dumpCSS( this, widget.getShadow() ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected String dojoType( T widget ) {
        return "works/form/TextBox";
    }
    
    protected Object value( T widget ) {
        return widget.getValue();
    }
    
    protected String dumpHorizontalAlignment( BaseTextBox.HorizontalAlignment alignment ) {
        switch( alignment ){
            case LEFT:
                return "left";
            case CENTER:
                return "center";
            case RIGHT:
                return "right";
            default:
                return null;
        }
    }
    
}