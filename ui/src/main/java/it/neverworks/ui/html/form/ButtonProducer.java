package it.neverworks.ui.html.form;

import java.util.List;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.html.data.ClassArtifact;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.types.ShadowConverter;
import it.neverworks.ui.html.types.CornersConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;

public class ButtonProducer extends HTMLProducer<Button> {
    
    public void render( Button widget ) {
        ToolBar toolbar = widget.getParent() instanceof ToolBar ? ((ToolBar) widget.getParent()) : null;
        
        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/form/Button" )
            .attr( "name",  widget.getName() ) 
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "disabled", widget.getDisabled() ? true : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "blinking", widget.getBlinking() ? true : null )
            .attr( "hint", widget.getHint() != null ? widget.getHint() : ( toolbar != null ? toolbar.getDefaultHint() : null ) )
            .attr( "arrow", widget.getArrow() )
            .attr( "wrap", widget.getWrap() )
            .attr( "color", ColorConverter.toCSS( widget.getColor() ) )
            .attr( "background", ColorConverter.toCSS( widget.getBackground() ) )
            .attr( "shadow", ShadowConverter.renderCSS( this, widget.getShadow() ) )
            .attr( "rounding", CornersConverter.renderCSS( this, "border-radius", widget.getRounding() ) )
            .attr( "halign", dumpHorizontalAlignment( widget.getHalign() ) )
            .attr( switchEvent( widget.getClick() ) )
            .attr( switchEvent( widget.getInspect() ) )

            .attr( "width", SizeConverter.toCSS( this, 
                widget.getWidth() != null ? widget.getWidth() : ( 
                    toolbar != null ? toolbar.getDefaultWidth() : null 
                ) 
            ))
            
            .attr( "height", SizeConverter.toCSS( this, 
                widget.getHeight() != null ? widget.getHeight() : (
                    toolbar != null ? toolbar.getDefaultHeight() : null 
                ) 
            ))

            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( FontConverter.renderCSS( this, widget.getFont() ) )
            .attr( "border", BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( dumpFlavor( 
                widget.getFlavor() != null ? widget.getFlavor() : ( 
                    toolbar != null ? toolbar.getDefaultFlavor() : null 
                ) 
            ));
        
        if( widget.getIcon() != null ) {
            Object resolved = resolveArtifact( widget.getIcon() );
            start.attr( resolved instanceof ClassArtifact ? "iconClass" : "iconSource", resolved );
        }
        
        output.start( "span" );

        String label;

        if( widget.getEncoding() == Button.Encoding.HTML ) {
            label = Strings.safe( widget.getCaption() ).replace( "\n", "<br/>" );
        } else {
            label = HTML.encode( widget.getCaption() ).replace( "\n", "<br/>" );
        }
        
        output.text( label );
        output.end();
        
        output.end();
    }
    
    protected ObjectNode dumpProperty( Button widget, String name ) {
        if( "icon".equals( name ) ) {
            if( widget.getIcon() != null ) {
                Object resolved = resolveArtifact( widget.getIcon() );
                return json.property( resolved instanceof ClassArtifact ? "iconStyle" : "iconSource", resolved );
            } else {
                return json.property( "iconStyle", null );
            }

        } else if( "caption".equals( name ) ) {
            if( widget.getEncoding() == Button.Encoding.HTML ) {
                return json.property( "label", Strings.safe( widget.getCaption() ).replace( "\n", "<br/>" ) );
            } else {
                return json.property( "label", HTML.encode( widget.getCaption() ).replace( "\n", "<br/>" ) );
            }

        } else if( "color".equals( name ) ) {
            return json.property( "color", ColorConverter.toCSS( widget.getColor() ) );
            
        } else if( "background".equals( name ) ) {
            return json.property( "background", ColorConverter.toCSS( widget.getBackground() ) );
            
        } else if( "rounding".equals( name ) ) {
            return json.property( "rounding", CornersConverter.dumpCSS( this, "border-radius", widget.getRounding() ) );

        } else if( "shadow".equals( name ) ) {
            return json.property( "shadow", ShadowConverter.dumpCSS( this, widget.getShadow() ) );

        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.getBorder() ) );

        } else if( "font".equals( name ) ) {
            return json.property( "font", FontConverter.dumpCSS( this, widget.getFont() ) );

        } else if( "halign".equals( name ) ) {
            return json.property( "halign", dumpHorizontalAlignment( widget.getHalign() ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected String dumpHorizontalAlignment( Button.HorizontalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ){
                case LEFT:
                    return "left";
                case RIGHT:
                    return "right";
                default:
                    return "center";
            }
        } else {
            return null;
        }
    }
    
    @Override
    public Object resolveArtifact( Object artifact ) {
        Object resolved = super.resolveArtifact( artifact );
        if( resolved instanceof RawArtifact ) {
            return new ClassArtifact().set( "content", ((RawArtifact) resolved).getContent() );
        } else {
            return resolved;
        }
    }

}