package it.neverworks.ui.html.server;

import it.neverworks.lang.Strings;
import it.neverworks.httpd.Request;
import it.neverworks.model.features.Retrieve;

public class EmbeddedHeaderProvider implements Retrieve {
    
    private Request request;

    public EmbeddedHeaderProvider( Request request ) {
        this.request = request;
    }

    public Object retrieveItem( Object key ) {
        return request.getHeader( Strings.valueOf( key ) );
    }

}