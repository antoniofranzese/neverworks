package it.neverworks.ui.html.data;

import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.data.Catalog;

public class WebStyleArtifact implements StyleArtifact {
    
    private String name;
    private String className;
    
    public WebStyleArtifact( String name, String className ) {
        this.name = name;
        this.className = className;
    }
    
    public String getClassName(){
        return this.className;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setClassName( String className ){
        this.className = className;
    }
 
    public String toString() {
        return new ToStringBuilder( this ).add( "name" ).add( "class", className ).toString();
    }
    
}