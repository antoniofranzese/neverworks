package it.neverworks.ui.html.types;

import java.util.Formatter;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.Widget;

public class ColorConverter extends BaseConverter {
    
    public static String toCSS( Object value ) {
        Color color = (Color)value;
        if( color == null ) {
            return null;
        } else if( color.equals( Color.TRANSPARENT ) ) {
            return "transparent";
        } else {
            if( color.getAlpha() == Color.SOLID ) {
                return new Formatter().format( "#%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue() ).toString();
            } else {
                return new Formatter().format( "#%02x%02x%02x%02x", color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha() ).toString();
            }
        }
        
    }
    
    public static String toRGB( Object value ) {
        Color color = (Color)value;
        if( color == null ) {
            return null;
        } else if( color.equals( Color.TRANSPARENT ) ) {
            return "rgba(0,0,0,0)";
        } else {
            if( color.getAlpha() == Color.SOLID ) {
                return new Formatter().format( "rgb(%d,%d,%d)", color.getRed(), color.getGreen(), color.getBlue() ).toString();
            } else {
                return new Formatter().format( "rgba(%d,%d,%d,%1.2f)", color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha() / 255.0f ).toString();
            }
        }
    }

}