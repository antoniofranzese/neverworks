package it.neverworks.ui.html.server;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.servlet.ServletRequestContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.encoding.MIME;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.URL;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileProvider;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;
import it.neverworks.io.MissingFileException;
import it.neverworks.log.Logger;

public class FileHelper extends BaseModel {
    
    private static final Logger logger = Logger.of( FileHelper.class );
 
    @Property
    private FileRepository repository;
    
    @Property
    private FileProvider provider;
    
    private final static char[] ENC_DIGITS = { 
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' 
    };
    
    private final static byte[] ENC_CHARS = { 
        '!', '#', '$', '&', '+', '-', '.', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y' ,'Z', '^', '_', '`',                        
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '|', '~'
    };

    public static String encodeFileName( String s ) {
        try {
            byte[] s_bytes = s.getBytes( "UTF-8" );
            int len = s_bytes.length;
            StringBuilder sb = new StringBuilder( len * 2 );
            
            sb.append( "UTF-8''" );

            for( int i = 0; i < len; ++i ) {
                byte b = s_bytes[ i ];
                if( Arrays.binarySearch( ENC_CHARS, b ) >= 0 ) {
                    sb.append((char) b);
                } else {
                    sb.append( '%' );
                    sb.append( ENC_DIGITS[ 0x0f & ( b >>> 4 ) ] );
                    sb.append( ENC_DIGITS[ b & 0x0f ] );
                }
            }

            return sb.toString();

        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public FileInfo retrieveFileInfo( String token, String queryString ) {
        Arguments args = new PrivateURL( "?" + queryString ).getPrivateArguments();
        logger.debug( "Private arguments: {}", args );
        try {
            FileInfo info = provider.getInfo( new FileBadge( token ) );
            return populateInfo( info, args );
        } catch( MissingFileException ex ) {
            int dot = token.lastIndexOf( "." );
            if( dot > 0 && dot < token.length() ) {
                try {
                    FileInfo info = populateInfo( provider.getInfo( new FileBadge( token.substring( 0, dot ) ) ), args );
                    if( empty( info.get( "type" ) ) ) {
                        info.set( "type",  MIME.detect( token ) );
                    }
                    return info;
                } catch( MissingFileException ex2 ) {
                    throw ex;
                }
            } else {
                throw ex;
            }
        }
    }

    protected FileInfo populateInfo( FileInfo source, Arguments args ) {
        FileInfo info = new FileInfo();
        
        if( args.has( "X-NWK-Rename" ) ) {
            info.set( "name", args.get( "X-NWK-Rename" ) );
        } else if( empty( source.get( "name" ) ) && args.has( "X-NWK-Name" ) ) {
            info.set( "name", args.get( "X-NWK-Name" ) );
        } else {
            info.set( "name", source.get( "name" ) );
        }
        
        if( empty( source.get( "type" ) ) && args.has( "X-NWK-Type" ) ) {
            info.set( "type", args.get( "X-NWK-Type" ) );
        } else {
            info.set( "type", source.get( "type" ) );
        }

        if( args.has( "X-NWK-Disposition" ) ) {
            info.set( "disposition", args.get( "X-NWK-Disposition" ) );
        } else {
            info.set( "disposition", source.get( "!disposition" ) );
        }
        
        info.set( "badge", source.get( "badge" ) );
        info.set( "stream", source.get( "stream" ) );
        info.set( "provider", source.get( "provider" ) );
        info.set( "repository", source.get( "repository" ) );
        
        return info;
    }
    
    public String processUpload( RequestContext ctx ) {
        return processUpload( ctx, null );
    }
    
    public String processUpload( RequestContext ctx, Arguments description ) {
        
        ObjectMapper mapper = new ObjectMapper();

        try {
            FileUpload upload = new FileUpload();
            upload.setFileItemFactory( new DiskFileItemFactory() );
        
            List<FileItem> items = upload.parseRequest( ctx );
            List<Info> infos = new ArrayList<Info>();
            boolean replyToIframe = false;
            for( FileItem item: items ) {
                if( ! item.isFormField() ) {
                    String name = item.getName();
                    // System.out.println( "NAME " + name );
                    String ext = name.indexOf( "." ) > 0 ? name.substring( name.lastIndexOf( "." ) + 1 ) : null;
                    Info info = new Info();
                
                    info.setBadge( repository.putStream( item.getInputStream(), arg( "extension", ext ) ) );
                    info.setName( name );
                    infos.add( info );
                    item.delete();

                } else {
                    //System.out.println( "Form field: " + item.getFieldName() + " = " + item.getString() );
                    if( item.getFieldName().equals( "uploadType" ) ) {
                        if( item.getString().equals( "iframe" ) ) {
                            replyToIframe = true;
                        }
                    } else if( item.getFieldName().equals( "type" ) ) {
                        Info info = infos.get( infos.size() - 1 );
                        String type = item.getString().trim();
                    
                        if( Strings.hasText( type ) && type.indexOf( "/" ) > 0 ) {
                            info.setType( type );
                        } else {
                            info.setType( inferMimeType( repository.getFile( info.getBadge() ) ) );
                        }
                    }
                }
            }

            ArrayNode results = mapper.createArrayNode();
            for( Info info: infos ) {
            
                Arguments arguments = 
                    arg( "type", info.getType() )
                    .arg( "name", info.getName() );

                if( description != null ) {
                    arguments.load( description );
                }
            
                repository.describe( info.getBadge(), arguments );
            
                ObjectNode result = mapper.createObjectNode();
                result.set( "token", JSON.encode( info.getBadge().getToken() ) );
                result.set( "name", JSON.encode( URL.encode( info.getName() ) ) );
                result.set( "type", JSON.encode( info.getType() ) );
                results.add( result );
            }

            ObjectNode resp = mapper.createObjectNode();
            resp.put( "uploaded", results );
        
            if( replyToIframe ) {
                return "<html><body><textarea>" + resp.toString() + "</textarea></body></html>";
            } else {
                return resp.toString();
            }

        } catch( Exception ex ) {
            //TODO: formattazione standard errore
            ObjectNode error = mapper.createObjectNode();
            error.put( "error", ex.getMessage() );
            //ex.printStackTrace();

            return error.toString();
        }
        
    }
    
    public String inferMimeType( File file ) {
        try {
            MagicMatch match = Magic.getMagicMatch( file, /* extensionHints */ true, /* onlyMimeMatch */ false);
    
            if( match != null ) {
                return match.getMimeType();
            } else {
                return "application/octet-stream";
            }
            
        } catch( MagicMatchNotFoundException ex ) {
            return "application/octet-stream";
            
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    
    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }
    
    public FileProvider getProvider(){
        return this.provider;
    }
    
    public void setProvider( FileProvider provider ){
        this.provider = provider;
    }
    
    private class Info {
        private String name;
        private String type;
        private FileBadge badge;
        
        public FileBadge getBadge(){
            return this.badge;
        }
        
        public void setBadge( FileBadge badge ){
            this.badge = badge;
        }

        public String getType(){
            return this.type;
        }
        
        public void setType( String type ){
            this.type = type;
        }
        
        public String getName(){
            return this.name;
        }
        
        public void setName( String name ){
            this.name = name;
        }
        
    }
    
    
}