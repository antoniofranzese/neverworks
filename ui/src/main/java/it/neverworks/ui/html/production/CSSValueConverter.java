package it.neverworks.ui.html.production;

import java.util.Formatter;

import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.types.Border;

import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.BoxConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.BorderSideConverter;

public class CSSValueConverter {
    
    public static String convert( Object object ) {

        if( object != null ) {
            if( object instanceof Size ) {
                return SizeConverter.toCSS( object );
                
            } else if( object instanceof Box ) {
                return BoxConverter.toCSS( object );
                
            } else if( object instanceof Color ) {
                return ColorConverter.toCSS( object );

            } else if( object instanceof Font ) {
                return FontConverter.toCSS( object );

            } else if( object instanceof Border ) {
                return BorderConverter.toCSS( object );

            } else if( object instanceof Border.Side ) {
                return BorderSideConverter.toCSS( (Border.Side) object );

            } else {
                return object.toString();
            }

        } else {
            return null;
        }
    }
    
}