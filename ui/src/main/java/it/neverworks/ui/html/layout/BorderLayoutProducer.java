package it.neverworks.ui.html.layout;

import static it.neverworks.language.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Event;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.Widget;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.BorderLayoutCapable;
import it.neverworks.ui.types.ViewportInfo;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.HTMLOutput;
import it.neverworks.ui.html.production.HTMLFactory;
import it.neverworks.ui.html.production.BufferedHTMLOutput;
import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.BackgroundConverter;
import it.neverworks.ui.html.types.ViewportInfoConverter;

public class BorderLayoutProducer extends HTMLProducer<BorderLayout> {
    
    private final static List<String> REGIONS = list( "tops", "bottoms", "lefts", "rights", "center" );
    
    public void render( BorderLayout widget ){
        
        output.dojo.start( "div")
            .attr( "type", "works/layout/BorderContainer" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "gutters", widget.getGutters() )
            .attr( attributes( this, widget ) )
            .attr( "design", widget.getDesign() == BorderLayout.Design.SIDEBAR ? "sidebar" : "headline" )
            .attr( "title", widget.getTitle() )
            .attr( "visible", widget.getVisible() )
            .style( widget.getParent() instanceof Form ? "main" : null )
            .style( "background-color", widget.getBackground() )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( "color", widget.getColor() )
            .style( BackgroundConverter.renderCSS( this, widget.getBackground() ) )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( FontConverter.renderCSS( this, widget.getFont() ) )
            .style( dumpFlavor( widget ) );
            

        for( BorderLayout.Frame frame: widget.getInventory() ) {
            renderChild( widget, frame.getRegion(), frame.getWidget(), this, output );
        }
        
        if( widget.getCenter() == null ) {
            output.dojo.start( "div" ) 
                .attr( "type", "dijit/layout/ContentPane" )
                .attr( "region", "center" )
                .attr( "blankRegion", true );
            output.text( "&nbsp;" );
            output.end();
        }

        output.end();
    }

    protected void renderChild( BorderLayout widget, String region, Widget child, Producer producer, HTMLOutput out ) {
        boolean wrap = !( child instanceof BorderLayoutCapable );
        
        if( wrap ) {
            out.dojo.start( "div" )
                .attr( "type", "works/layout/ContentPane" )
                .attr( "region", region )
                .attr( "splitter", child.get( "?layout.splitter" ) )
                .attr( "portraitLayout", ViewportInfoConverter.toJSON( this, child.get( "?layout.portrait" ) ) )
                .attr( "landscapeLayout", ViewportInfoConverter.toJSON( this, child.get( "?layout.landscape" ) ) )
                .attr( "regionWrapper", true );
        }
        
        producer.produce( child );
        
        if( wrap ) {
            out.end();
        }
        
    }
    
    protected String drawChild( BorderLayout widget, String region, Widget child ) {
        BufferedHTMLOutput bufferedOutput = new BufferedHTMLOutput();
        Producer producer = ((HTMLFactory) factory).with( context, bufferedOutput );
        renderChild( widget, region, child, producer, bufferedOutput );
        return bufferedOutput.getBuffer();
    }
    
    protected ObjectNode dumpRegion( BorderLayout widget, String property ) {
        ObjectNode result = json.object();
        
        if( "center".equals( property ) ) {
            Widget child = widget.getCenter();
            if( child != null ) {
                result.put( "center", drawChild( widget, "center", child ) );
            } else {
                result.put( "center", false );
            }
            
        } else {
            Collection<Widget> children = widget.get( property );
            String region = property2region( property );
            
            if( children != null && children.size() > 0 ) {
                ArrayNode contents = json.array();
                for( Widget child: children ) {
                    contents.add( drawChild( widget, region, child ) );
                }
                result.put( region, contents );
                
            } else {
                result.put( region, false );
            }
            
        }
        
        return result;
    }

    protected ObjectNode dumpChild( BorderLayout widget, String specification ) {
        String[] arguments = specification.split( ":" );
        String region = arguments[ 1 ];
        String name = arguments[ 2 ];
        Widget child = widget.children().by( "name" ).eq( name ).result();
        if( child != null ) {
            return json.property( "child:" + name, drawChild( widget, region, child ) );
        } else {
            throw new ProductionException( "Cannot locate " + widget.getCommonName() + " child: " + name );
        }
    }

    protected boolean redrawRequested;
    
    @Override
    public void analyze( BorderLayout widget ) {
        
        analysis.begin( this, widget );

        Changes changes = widget.model().changes();

        for( BorderLayout.Frame frame: widget.getInventory() ) {
            if( ! changes.contains( frame.getProperty() ) ) {
                redrawRequested = false;
                inspect( frame.getWidget() );
                if( redrawRequested ) {
                    changes.add( "child:" + frame.getRegion() + ":" + frame.getWidget().getName() );
                }
            }
        }

        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }

        analysis.accept();
    }

    @Override
    public void bubble( Event event ) {
        if( event instanceof WidgetModifiedEvent ) {
            redrawRequested = true;
        } else if( parent != null ) {
            parent.bubble( event );
        }
    }

    protected ObjectNode dumpProperty( BorderLayout widget, String name ) {
        if( "design".equals( name ) ) {
            return json.property( name, widget.getDesign() == BorderLayout.Design.SIDEBAR ? "sidebar" : "headline" );
 
        } else if( "background".equals( name ) ) {
            return json.property( "background", BackgroundConverter.dumpCSS( this, widget.getBackground() ) );
 
        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.getBorder() ) );
 
        } else if( "font".equals( name ) ) {
            return json.property( "font", FontConverter.dumpCSS( this, widget.getFont() ) );
 
        } else if( REGIONS.contains( name ) ) {
            return dumpRegion( widget, name );

        } else if( name.startsWith( "child:" ) ) {
            return dumpChild( widget, name );
 
        } else {
            return super.dumpProperty( widget, name );
        }
    }
 
    private String property2region( String name ) {
        return "center".equals( name ) ? name : name.substring( 0, name.length() - 1 );
    }
    
    public static Arguments attributes( HTMLProducer producer, Widget widget ) {

        Arguments attributes = new Arguments();
        //TODO: direct JSON
        if( widget.getParent() instanceof BorderLayout ) {
            attributes
                .arg( "region", widget.get( "!layout.region" ) )
                .arg( "splitter", widget.get( "!layout.splitter" ) );
        }

        if( widget.get( "!layout.viewport" ) != null ) {
            Map<String,ViewportInfo> info = widget.get( "layout.viewport" );
            Arguments viewport = new Arguments();

            for( String name: info.keySet() ) {
                viewport.arg( name.trim(), ViewportInfoConverter.toJSON( producer, info.get( name ) ) );
            }

            if( viewport.size() > 0 ) {
                attributes.arg( "viewport", viewport );
            }
        }

        return attributes.size() > 0 ? attributes : null;
    }
}