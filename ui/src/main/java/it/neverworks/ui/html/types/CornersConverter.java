package it.neverworks.ui.html.types;

import net.sf.uadetector.UserAgentFamily;
import net.sf.uadetector.ReadableUserAgent;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Numbers;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.Widget;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.html.production.HTMLProducer;

public class CornersConverter extends BaseConverter {
    
    private final static Size ZERO = Size.pixels( 0 );

    public static Arguments renderCSS( HTMLProducer producer, String tag, Object value ) {
        if( value != null ) {
            return new Arguments().arg( tag, toCSS( value ) );

        } else {
            return null;
        }
    }


    public static String toCSS( Object value ) {
        if( value != null ) {
            Corners corners = (Corners) value;
            Size tl = corners.get( "topLeft", ZERO );
            Size tr = corners.get( "topRight", ZERO );
            Size bl = corners.get( "bottomLeft", ZERO );
            Size br = corners.get( "bottomRight", ZERO );

            if( tl.equals( tr ) && tl.equals( bl ) && tl.equals( br ) ) {
                return SizeConverter.toCSS( null, tl );

            } else {
                return SizeConverter.toCSS( null, tl )
                    + " " + SizeConverter.toCSS( null, tr )
                    + " " + SizeConverter.toCSS( null, br )
                    + " " + SizeConverter.toCSS( null, bl );
            }

        } else {
            return null;
        }
    }

    public static Arguments dumpCSS( HTMLProducer producer, String tag, Object value ) {
        if( value != null ) {
            return renderCSS( producer, tag, value );
        } else {
            return new Arguments().arg( cssTag( tag ), null );
        }
    }
    
    public static String cssTag( String tag ) {
        ReadableUserAgent ua = Device.get( "userAgent" );

        if( ua != null ) {
            if( "border-radius".equals( tag ) ) {
                if( ua.getFamily() == UserAgentFamily.FIREFOX && Numbers.Int( ua.getVersionNumber().getMajor() ) < 50 ) {
                    tag = "-moz-border-radius";
                }            
            }
        }

        return tag;
    }
}