package it.neverworks.ui.html.runtime;

import it.neverworks.lang.Annotations;
import it.neverworks.lang.Filter;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.security.context.UserInfo;
import it.neverworks.ui.runtime.User;

public class UserFormFilter extends BaseModel implements Filter<Class> {
    
    @Property
    private boolean strict = false; 
    
    @Property @Required
    private UserInfo user;
    
    public boolean filter( Class cls ) {
        if( Annotations.has( cls, User.class ) ) {
            if( user.authenticated() ) {
                User annotation = Annotations.get( cls, User.class );

                for( String role: annotation.is() ) {
                    if( user.is( role ) ) {
                        return true;
                    }
                }

                for( String function: annotation.can() ) {
                    if( user.can( function ) ) {
                        return true;
                    }
                }
                
                return false;
                
            } else {
                return false;
            }
            
        } else {
            return ! strict;
        }
    }

    
    public UserInfo getUser(){
        return this.user;
    }
    
    public void setUser( UserInfo user ){
        this.user = user;
    }

    public boolean getStrict(){
        return this.strict;
    }
    
    public void setStrict( boolean strict ){
        this.strict = strict;
    }
    
}