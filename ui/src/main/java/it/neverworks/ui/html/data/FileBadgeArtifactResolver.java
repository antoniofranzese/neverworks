package it.neverworks.ui.html.data;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.FileBadgeArtifact;

public class FileBadgeArtifactResolver extends BaseModel implements ArtifactResolver {
    
    public Object resolveArtifact( Artifact artifact ) {
        return new HrefArtifact().set( "content", getUrl() + "/" + ((FileBadgeArtifact) artifact).getBadge().getToken() );
    }

    @Property @Required @Inject( value = { "it.neverworks.ui.web.FileRoot", "it.neverworks.ui.FileRoot" }, lazy = true )
    private String url;
    
    public String getUrl(){
        return this.url;
    }
    
    public void setUrl( String url ){
        this.url = url;
    }

}