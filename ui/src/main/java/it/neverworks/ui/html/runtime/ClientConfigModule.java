package it.neverworks.ui.html.runtime;

import it.neverworks.lang.Wrapper;
import it.neverworks.model.BaseModel;
import it.neverworks.ui.runtime.Device;

public class ClientConfigModule extends BaseModel implements Runnable {
    
    private final static Wrapper LANGUAGES = new LanguagesAttribute();

    public void run() {
        Device.register( "languages", LANGUAGES );
    }

}