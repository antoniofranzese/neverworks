package it.neverworks.ui.html.layout;

import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.lang.Strings;
import it.neverworks.model.description.Changes;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.ui.Widget;
import it.neverworks.ui.BehavioralWidget;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.layout.StructureCapable;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.LabelCapable;
import it.neverworks.ui.types.Box;
import it.neverworks.ui.types.Border;
import it.neverworks.ui.types.Corners;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Background;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.EmbeddableProducer;
import it.neverworks.ui.html.production.BaseContainerProducer;
import it.neverworks.ui.html.production.BaseRedrawableContainerProducer;
import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.CSSValueConverter;
import it.neverworks.ui.html.types.BoxConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.ShadowConverter;
import it.neverworks.ui.html.types.CornersConverter;
import it.neverworks.ui.html.types.BackgroundConverter;
import it.neverworks.ui.html.types.ScrollabilityConverter;

public class GridLayoutProducer extends BaseRedrawableContainerProducer<GridLayout> {
    
    private final static LayoutInfo DEFAULT_LAYOUT = new LayoutInfo();
    private final static TypeDefinition<Box> BoxType = type( Box.class );
    private final static TypeDefinition<Size> SizeType = type( Size.class );
    private final static TypeDefinition<Border> BorderType = type( Border.class );
    private final static TypeDefinition<Color> ColorType = type( Color.class );
    private final static TypeDefinition<Corners> CornersType = type( Corners.class );
    private final static TypeDefinition<Background> BackgroundType = type( Background.class );
    private final static TypeDefinition<StyleArtifact> StyleArtifactType = type( StyleArtifact.class );
    private final static TypeDefinition<Boolean> BooleanType = type( Boolean.class );
    private final static TypeDefinition<Integer> IntegerType = type( Integer.class );
    public final static TypeDefinition<GridLayout.CellVerticalAlignment> CellValignType = type( GridLayout.CellVerticalAlignment.class );
    public final static TypeDefinition<GridLayout.CellHorizontalAlignment> CellHalignType = type( GridLayout.CellHorizontalAlignment.class );
    
    private int column;
    private int row;
    private int columns;
    private Border innerBorder;
    
    public void render( GridLayout widget ){

        boolean redraw = redrawing( widget );
        
        String widgetName = widget.getCommonName();
        if( !redraw ) {
            output.dojo.start( "div")
                .attr( "type", "works/layout/GridLayout" )
                .attr( "name", widget.getName() )
                .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
                .attr( BorderLayoutProducer.attributes( this, widget ) )
                .attr( "visible", widget.getVisible() ? null : false )
                .attr( "collapsible", widget.getCollapsible() ? true : null )
                .attr( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) )
                .attr( "title", HTML.encode( widget.get( "title", widget.get( "!layout.title", widget.get( "name" ) ) ) ) )
                .attr( "shadow", ShadowConverter.renderCSS( this, widget.getShadow() ) )
                .attr( "rounding", CornersConverter.renderCSS( this, "border-radius", widget.getRounding() ) )
                .style( BackgroundConverter.renderCSS( this, widget.getBackground() ) )
                .style( "color", widget.getColor() )
                // .attr( "contentWidth", widget.getWidth() != null ? SizeConverter.toCSS( this, widget.getWidth() ) : null )
                // .attr( "contentHeight", widget.getHeight() != null ? SizeConverter.toCSS( this, widget.getHeight() ) : null )
                .style( "margin", widget.getMargin() )
                .style( dumpFlavor( widget ) );
        } 
        
        startLayout( widget, new LayoutInfo( arg( "columns", widget.getColumns() ) ) );
        
        for( Widget child: widget.query().notInstanceOf( BehavioralWidget.class ).as( Widget.class ) ) {

            if( child instanceof LabelCapable && !( Label.Position.AFTER == child.get( "!label.position" ) ) ) {
                renderLabel( child );
            } 
    
            LayoutInfo layout = child instanceof LayoutCapable ? child.<LayoutInfo>get( "layout" ) : null;
            boolean inner = child instanceof StructureCapable && ((StructureCapable) child).isStructural();
        
            startColumn( child, layout, inner );
            produce( child );
            endColumn( layout );

            if( child instanceof LabelCapable && Label.Position.AFTER == child.get( "!label.position" ) ) {
                renderLabel( child );
            } 

        }
        
        endLayout( widget );
        
        for( Widget child: widget.query().instanceOf( BehavioralWidget.class ).as( Widget.class ) ) {
            produce( child );
        }
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    private void renderLabel( Widget child ) {
        Label label = child.get( "label" );
        if( label != null ) {
            LayoutInfo labelInfo;
            if( child instanceof LayoutCapable && child.get( "layout" ) != null ) {
                LayoutInfo childInfo = child.<LayoutInfo>get( "layout" );
                labelInfo = new LayoutInfo( label.getLayout() != null ? label.getLayout() : new LayoutInfo() );

                if( childInfo.contains( "border" ) && ! labelInfo.contains( "border" ) ) {
                    labelInfo.set( "border", childInfo.get( "border" ) );
                }
                if( childInfo.contains( "color" ) && ! labelInfo.contains( "color" ) ) {
                    labelInfo.set( "color", childInfo.get( "color" ) );
                }
                if( childInfo.contains( "background" ) && ! labelInfo.contains( "background" ) ) {
                    labelInfo.set( "background", childInfo.get( "background" ) );
                }

            } else {
                labelInfo = label.getLayout();
            }
            startColumn( label, labelInfo );
            produce( label );
            endColumn( labelInfo );  
        }
    }    
    
    private void startLayout( GridLayout widget, LayoutInfo layout ) {
        output.start( "table" )
            .attr( "class", "worksGridLayout" )
            .attr( "width", CSSValueConverter.convert( widget.getWidth() ) )
            .attr( "height", CSSValueConverter.convert( widget.getHeight() ) )
            .attr( "align", dumpHorizontalAlignment( widget.getHalign() ) )
            // .attr( "cellspacing", IntegerType.process( layout.get( "!spacing" ), "{0}.{1}", widgetName, "layout.spacing" )
            // .attr( "cellpadding", IntegerType.process( layout.get( "!padding" ), "{0}.{1}", widgetName, "layout.padding" )
            .style( "padding", widget.getPadding() ) // giusto cosi'
            .style( "font", FontConverter.toCSS( widget.get( "font" ) ) )
            .style( "border", widget.get( "!border.outer" ) )
            .style( "border-spacing", widget.getSpacing() );
        
        output.start( "tr" );
        row = 0;
        column = 0;
        columns = layout.get( "!columns", 2 );
        innerBorder = widget.get( "!border.inner" );
    }
        
    private void endLayout( GridLayout widget ) {
        output.end( "tr" );
        output.end( "table" );   
    }

    private void startColumn( Widget child, LayoutInfo layout ) {
        startColumn( child, layout, /* inner */ false );
    }
    
    private void startColumn( Widget child, LayoutInfo layout, boolean inner ){
        //TODO: parametro skip, salta un certo numero di celle
        String childName = child.getCommonName();
        
        if( layout == null ){
            layout = DEFAULT_LAYOUT;
        }
        
        int hspan = 1;
        if( layout.contains( "hspan" ) ) {
            Object value = layout.get( "hspan" );
            if( value instanceof Number ) {
                hspan = ((Number) value).intValue();
            } else if( value instanceof String ) {
                if( "full".equalsIgnoreCase( (String) value ) ) {
                    hspan = columns;
                } else if( "end".equalsIgnoreCase( (String) value ) ) {
                    hspan = columns - column;
                    // hspan=end oltre l'ultima colonna diventa equivalente a full
                    if( hspan <= 0 ) {
                        hspan = columns;
                    }
                } else {
                    try {
                        hspan = Integer.parseInt( (String) value );
                    } catch( NumberFormatException ex ) {}
                }
            }

        };

        if( hspan > columns ) {
            hspan = columns;
        }

        if( ( column + hspan ) > columns ) {
            output.end( "tr" );
            output.start( "tr" );
            column = 0;
            row += 1;
        }
        
        boolean hidden = child.model().descriptor().has( "visible", Boolean.class ) && Boolean.FALSE.equals( child.get( "visible" ) ) ? true : false; 
        
        Tag td = output.start( "td" )
            .style( "gridLayout" )
            .style( inner ? "inner" : null )
            .style( hidden ? "hidden" : null )
            .style( dumpFlavor( StyleArtifactType.process( layout.get( "!flavor" ), "{0}.{1}", childName, "layout.flavor" ) ) );
        
        if( layout.contains( "border" ) ) {
            td.style( BorderConverter.renderCSS( this, BorderType.process( layout.get( "border" ), "{0}.{1}", childName, "layout.border" ) ) );
            td.style( "bordered" );

        } else if( innerBorder != null ) {
            if( column != 0 ) {
                td.style( "border-left", innerBorder.getLeft() );
            }
            
            if( row != 0 ) {
                td.style( "border-top", innerBorder.getTop() );
            }
        }

        if( layout.contains( "rounding" ) ) {
           td.style( CornersConverter.renderCSS( this, "border-radius", CornersType.process( layout.get( "rounding" ), "{0}.{1}", childName, "layout.rounding" ) ) );
        }

        if( hspan > 1 ) {
            td.attr( "colspan", hspan );
        }
        
        if( layout.contains( "halign" ) ) {
            td.attr( "align", dumpCellHalign( CellHalignType.process( layout.get( "halign" ), "{0}.{1}", childName, "layout.halign" ) ) );
        }
        if( layout.contains( "valign" ) ) {
            td.attr( "valign", dumpCellValign( CellValignType.process( layout.get( "valign" ), "{0}.{1}", childName, "layout.valign" ) ) );
        } else {
            td.attr( "valign", "top" );
        }
        
        if( layout.contains( "width" ) ) {
            Size width = SizeType.process( layout.get( "width" ), "{0}.{1}", childName, "layout.width" );
            if( width != null ) {
                String css = SizeConverter.toCSS( this, width );
                td.attr( "width", css );
                td.attr( "data-width-physical", width.isPhysical() );

                if( width.isPhysical() ) {
                    td.style( "min-width", css );
                    td.style( "max-width", css );
                }
            }
        }

        if( layout.contains( "height" ) ) {
            Size height = SizeType.process( layout.get( "height" ), "{0}.{1}", childName, "layout.height" );
            if( height != null ) {
                String css = SizeConverter.toCSS( this, height );
                td.attr( "height", css );

                if( height.isPhysical() ) {
                    td.style( "min-height", css );
                    td.style( "max-height", css );
                }
            }
        }

        if( layout.contains( "color" ) ) {
            td.style( "color", ColorConverter.toCSS( ColorType.process( layout.get( "color" ), "{0}.{1}", childName, "layout.color" ) ) );
        }
        if( layout.contains( "background" ) ) {
            td.style( BackgroundConverter.renderCSS( this, BackgroundType.process( layout.get( "background" ), "{0}.{1}", childName, "layout.background" ) ) );
        }
        if( layout.contains( "padding" ) ) {
            td.style( "padding", BoxConverter.toCSS( BoxType.process( layout.get( "padding" ), "{0}.{1}", childName, "layout.padding" ) ) );
        }

        column += hspan;
    }
        
    private void endColumn( LayoutInfo layout ){
        output.end( "td" );
    } 
 
    protected boolean containerNeedsRedraw( GridLayout widget ) {
        return widget.model().changes().containsAny( "children", "columns", "spacing", "padding" );
    }

    protected String dumpHorizontalAlignment( GridLayout.HorizontalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ){
                case LEFT:
                    return "left";
                case CENTER:
                    return "center";
                case RIGHT:
                    return "right";
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    public static String dumpCellValign( GridLayout.CellVerticalAlignment valign ) {
        if( valign != null ) {
            switch( valign ){
                case TOP:
                    return "top";
                case BOTTOM:
                    return "bottom";
                default:
                    return "middle";
            }
        } else {
            return null;
        }
    }
    
    public static String dumpCellHalign( GridLayout.CellHorizontalAlignment halign ) {
        if( halign != null ) {
            switch( halign ){
                case LEFT:
                    return "left";
                case RIGHT:
                    return "right";
                default:
                    return "center";
            }
        } else {
            return null;
        }
    }
    
    protected ObjectNode dumpProperty( GridLayout widget, String name ) {
        if( "halign".equals( name ) ) {
            return json.property( name, dumpHorizontalAlignment( widget.getHalign() ) );

        } else if( "scrollable".equals( name ) ) {
            return json.property( name, ScrollabilityConverter.toValue( widget.getScrollable() ) );

        } else if( "border.inner".equals( name ) ) {
            return json.property( "innerBorder", BorderConverter.dumpCSS( this, widget.get( "!border.inner" ) ) );

        } else if( "border.outer".equals( name ) ) {
            return json.property( "outerBorder", BorderConverter.dumpCSS( this, widget.get( "!border.outer" ) ) );

        } else if( "border".equals( name ) ) {
            return (ObjectNode) JSON.js.encode(
                arg( "innerBorder", BorderConverter.dumpCSS( this, widget.get( "!border.inner" ) ) )
                .arg( "outerBorder", BorderConverter.dumpCSS( this, widget.get( "!border.outer" ) ) )
            );

        } else if( "background".equals( name ) ) {
            return json.property( "background", BackgroundConverter.dumpCSS( this, widget.get( "background" ) ) );
            
        } else if( "color".equals( name ) ) {
            return json.property( "color", ColorConverter.toCSS( widget.get( "color" ) ) );
        
        } else if( "rounding".equals( name ) ) {
            return json.property( "rounding", CornersConverter.dumpCSS( this, "border-radius", widget.getRounding() ) );

        } else if( "shadow".equals( name ) ) {
            return json.property( "shadow", ShadowConverter.dumpCSS( this, widget.getShadow() ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected void analyzeWidget( GridLayout widget ) {
        Changes changes = widget.model().changes().clone().remove( "children" );
        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }
    }

    private final static String[] SURVIVING_PROPERTIES = new String[]{ "scrollable", "border", "border.inner", "border.outer" };
        
    protected String[] getSurvivingProperties() {
        return SURVIVING_PROPERTIES;
    }

}
