package it.neverworks.ui.html.production;

import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.JSON;

import it.neverworks.model.io.State;

public class JSONState implements State {
    private JsonNode node;
    
    public JSONState( JsonNode node ) {
        this.node = node;
    }
    
    public class NamesIterable implements Iterable<String> {
        public Iterator<String> iterator() {
            return JSONState.this.node.fieldNames();
        } 
    }
    
    public Iterable<String> names() {
        return new NamesIterable();
    }
    
    public boolean has( Object key ) {
        return node.has( key.toString() );
    }
    
    public Object get( Object key ) {
        return JSON.decode( node.get( key.toString() ) );
    }
    
    public State sub( Object key ) {
        return new JSONState( node.get( key.toString() ) );
    }
    
    public String toString() {
        return "JSONState: " + node.toString();
    }
    
}