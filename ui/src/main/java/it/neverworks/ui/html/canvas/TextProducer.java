package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;
import it.neverworks.ui.canvas.Fill;
import it.neverworks.ui.canvas.Text;


public class TextProducer extends ShapeProducer<Text> {


    public ObjectNode dump( Text shape ) {
        ObjectNode node = super.dump( shape );
 
        node.put( "_", "tx" );
        
        setSize( node, "w", shape.get( "width" ) );
        setSize( node, "h", shape.get( "height" ) );

        if( shape.get( "value" ) != null ) {
            node.put( "tx", shape.<String>get( "value" ) );
        }

        if( shape.get( "font" ) != null ) {
            Font font = shape.get( "font" );
            StringBuffer spec = new StringBuffer( 64 );
            
            if( font.getWeight() == Font.Weight.BOLD ) {
                spec.append( "bold" );
            } else if( font.getStyle() == Font.Style.ITALIC ) {
                spec.append( "italic" );
            }

            if( font.getSize() != null ) {
                spec.append( " " ).append( SizeConverter.toCSS( font.getSize() ) );
            }


            if( font.getFamily() != null ) {
                spec.append( " " ).append( font.getFamily() );
            }

            node.put( "ft", spec.toString() );
        }

        if( shape.get( "halign" ) != null ) {
            switch( shape.getHalign() ) {
                case LEFT:
                    node.put( "ha", "L" );
                    break;
                case CENTER:
                    node.put( "ha", "C" );
                    break;
                case RIGHT:
                    node.put( "ha", "R" );
                    break;
                case START:
                    node.put( "ha", "S" );
                    break;
                case END:
                    node.put( "ha", "E" );
                    break;
            }
        }

        if( shape.get( "valign" ) != null ) {
            switch( shape.getValign() ) {
                case TOP:
                    node.put( "va", "T" );
                    break;
                case MIDDLE:
                    node.put( "va", "M" );
                    break;
                case BOTTOM:
                    node.put( "va", "B" );
                    break;
            }

        }

        if( shape.get( "baseline" ) != null ) {
            switch( shape.getBaseline() ) {
                case ALPHABETIC:
                    node.put( "bl", "A" );
                    break;
                case TOP:
                    node.put( "bl", "T" );
                    break;
                case HANGING:
                    node.put( "bl", "H" );
                    break;
                case MIDDLE:
                    node.put( "bl", "M" );
                    break;
                case IDEOGRAPHIC:
                    node.put( "bl", "I" );
                    break;
            }

        }

        return node;
    }

}