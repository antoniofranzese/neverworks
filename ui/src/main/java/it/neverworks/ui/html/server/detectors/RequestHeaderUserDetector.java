package it.neverworks.ui.html.server.detectors;

import static it.neverworks.language.*;

import it.neverworks.log.Logger;
import javax.servlet.http.HttpServletRequest;

import it.neverworks.lang.Strings;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.security.model.AuthenticatedUser;

public class RequestHeaderUserDetector implements UserDetector {
    
	private static final Logger logger = Logger.of( "AUTH" );

    private String header;

    public static class RequestHeaderUser implements AuthenticatedUser {

        private String userID;

        public RequestHeaderUser( String userID ) {
            this.userID = userID;
        }

        public String getUserID(){
            return this.userID;
        }
        
        public String getId(){
            return this.userID;
        }
        
        public boolean equals( Object other ) {
            return other instanceof RequestHeaderUser 
                && this.userID != null 
                && this.userID.equals( ((RequestHeaderUser) other).getUserID() );
        }

        public String toString() {
            return new ToStringBuilder( this ).add( "id" ).toString();
        }
        
    }
    
    public AuthenticatedUser detect( HttpServletRequest request ) {
        String userID = request.getHeader( header );;

        if( Strings.hasText( userID ) ) {
            return new RequestHeaderUser( userID.trim() );
        } else {
            return null;
        }
    }
    
    public void init( String specification ) {
        int index = specification.indexOf( ":" );
        if( index > 0 ) {
            this.header = specification.substring( index + 1 );
            if( Strings.isEmpty( this.header ) ) {
                throw new IllegalArgumentException( "Missing request header specification"  );
            }
        } else {
            throw new IllegalArgumentException( "Bad request header specification: " + specification );
        }
    }
    
    public String getHeader(){
        return this.header;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "header" ).toString();
    }

}