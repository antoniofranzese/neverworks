package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Box;
import it.neverworks.ui.Widget;

public class BoxConverter extends BaseConverter {
    
    public static String toCSS( Object value ) {
        if( value != null ) {
            Box box = (Box) value;
            return box.get( "top", 0 ) + "px " + box.get( "right", 0 ) + "px " + box.get( "bottom", 0 ) + "px " + box.get( "left", 0 ) + "px";
        } else {
            return null;
        }
    }
    
}