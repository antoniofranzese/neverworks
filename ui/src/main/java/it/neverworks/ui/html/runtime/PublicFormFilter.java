package it.neverworks.ui.html.runtime;

import it.neverworks.lang.Annotations;
import it.neverworks.lang.Filter;
import it.neverworks.ui.runtime.Public;

public class PublicFormFilter implements Filter<Class> {
    
    public boolean filter( Class cls ) {
        return Annotations.has( cls, Public.class );
    }

}