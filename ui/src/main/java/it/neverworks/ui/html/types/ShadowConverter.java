package it.neverworks.ui.html.types;

import net.sf.uadetector.UserAgentFamily;
import net.sf.uadetector.ReadableUserAgent;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Color;
import it.neverworks.ui.types.Shadow;
import it.neverworks.ui.Widget;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.html.production.HTMLProducer;

public class ShadowConverter extends BaseConverter {
    
    private final static Size ZERO = Size.pixels( 0 );
    private final static Size ONE = Size.pixels( 1 );

    public static Arguments renderCSS( HTMLProducer producer, Object value ) {
        if( value != null ) {

            return new Arguments().arg( cssTag(), toCSS( value ) );

        } else {
            return null;
        }
    }

    public static String toCSS( Object value ) {
        if( value != null ) {
            Shadow shadow = (Shadow) value;

            Color color = shadow.get( "color", Color.LIGHTGRAY );
            Size blur = shadow.get( "blur", ONE );
            Size x = shadow.get( "x", ONE );
            Size y = shadow.get( "y", ONE );
            Size spread = shadow.get( "spread", ZERO );

            return SizeConverter.toCSS( null, x ) 
                + " " + SizeConverter.toCSS( null, y )
                + " " + SizeConverter.toCSS( null, blur )
                + " " + SizeConverter.toCSS( null, spread )
                + " " +  ColorConverter.toCSS( color );

        } else {
            return null;
        }
    }

    public static Arguments dumpCSS( HTMLProducer producer, Object value ) {
        if( value != null ) {
            return renderCSS( producer, value );
        } else {
            return new Arguments().arg( cssTag(), null );
        }
    }

    public static String cssTag() {
        ReadableUserAgent ua = Device.get( "userAgent" );
        String tag = "box-shadow";
        
        if( ua != null && ua.getFamily() == UserAgentFamily.FIREFOX && Numbers.Int( ua.getVersionNumber().getMajor() ) < 4 ) {
            tag = "-moz-box-shadow";
        }

        return tag;
    }
}