package it.neverworks.ui.html.layout;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.LabelPanel;
import it.neverworks.ui.html.production.Tag;

public class LabelPanelProducer<T extends LabelPanel> extends PanelProducer<T> {
    
    public void render( T widget ){
        
        LabelPanel panel = (LabelPanel) widget;
        
        boolean redraw = redrawing( widget );
        
        if( !redraw ) {
            Tag start = output.dojo.start( "div")
                .attr( "type", dojoType() )
                .attr( "name", widget.getName() )
                .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
                .attr( BorderLayoutProducer.attributes( this, widget ) )
                .attr( "title", widget.getTitle() != null ? HTML.encode( widget.getTitle() ).replace( " ", "&nbsp;" ).replace( "\n", "" ) : "&nbsp;" )

                .attr( "position", panel.getPosition() != null ? convertPosition( panel.getPosition() ) : null )
                .attr( "align", panel.getAlignment() != null ? convertAlignment( panel.getAlignment() ) : null )
                .attr( "visible", widget.getVisible() )

                .style( "background-color", widget.getBackground() )
                .style( "width", widget.getWidth() )
                .style( "height", widget.getHeight() )
                .style( "padding", widget.getPadding() )
                .style( "margin", widget.getMargin() )
                .style( dumpFlavor( widget ) );
        } 
        
        if( widget.getContent() != null ) {
            produce( widget.getContent() );
        } else {
            output.text( "&nbsp;" );
        }
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    protected String dojoType() {
        return "works/layout/LabelPane";
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "position".equals( name ) ) {
            return json.property( name, convertPosition( widget.<LabelPanel.Position>get( "position" ) ) );
        } else if( "alignment".equals( name ) ) {
            return json.property( name, convertAlignment( widget.<LabelPanel.Alignment>get( "alignment" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    public String convertPosition( LabelPanel.Position position ) {

        switch( position ) {
            case TOP:
                return "top";
            case BOTTOM:
                return "bottom";
            case LEFT:
                return "left";
            case RIGHT:
                return "right";
            default:    
                return null;
        }
    }
    
    public String convertAlignment( LabelPanel.Alignment position ) {
        switch( position ) {
            case TOP:
                return "top";
            case BOTTOM:
                return "bottom";
            case LEFT:
                return "left";
            case RIGHT:
                return "right";
            case CENTER:
                return "center";
            case MIDDLE:
                return "middle";
            default:
                return null;
        }
    }
    
    
}