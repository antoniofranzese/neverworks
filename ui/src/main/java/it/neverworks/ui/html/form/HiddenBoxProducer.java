package it.neverworks.ui.html.form;

import java.util.List;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.form.HiddenBox;

public class HiddenBoxProducer extends HTMLProducer<HiddenBox> {
    
    public void render( HiddenBox widget ) {
        if( widget.getExposed() ) {
            //TODO: dojo object
        }
    }
    
    protected ObjectNode dumpProperty( HiddenBox widget, String name ) {
        //TODO: value change if exposed
        return null;
    }
    
}