package it.neverworks.ui.html.form;

import static it.neverworks.language.*;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.ui.encoding.HTML;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.ShadowConverter;
import it.neverworks.ui.html.types.CornersConverter;
import it.neverworks.ui.types.Flavored;
import it.neverworks.ui.types.Size;

public class LabelProducer extends HTMLProducer<Label> {

    public void render( Label widget ) {
        // Disegna un widget interattivo solo se la Label non e' anonima
        boolean renderDojoWidget = widget.getName() != null;
        
        Tag start = output.dojo.start( "label" )
            .style( "height", widget.getHeight() )
            .style( "margin", widget.getMargin() )
            .style( "padding", widget.getPadding() )
            .style( "color", widget.getColor() )
            .style( "min-width", widget.getMinWidth() )
            .style( "min-height", widget.getMinHeight() )
            .style( "background-color", widget.getBackground() )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( FontConverter.renderCSS( this, widget.getFont() ) )
            .style( ShadowConverter.renderCSS( this, widget.getShadow() ) )
            .style( CornersConverter.renderCSS( this, "border-radius", widget.getRounding() ) )
            .style( dumpFlavor( widget ) );

        if( widget.getWidth() != null || 
            widget.getHeight() != null ||
            widget.getPadding() != null ||
            widget.getMargin() != null ) {
            start.style( "display", "inline-block" );
        }

        if( renderDojoWidget ) {
            start
                .attr( "type", "works/form/Label" )
                .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
                .attr( "name",  widget.getName() )
                
                .attr( "required", widget.getRequired() )
                .attr( "visible", widget.getVisible() ? null : false )
                .attr( "disabled", widget.getDisabled() ? true : null )
                .attr( "readonly", widget.getReadonly() ? true : null )
                .attr( "blinking", widget.getBlinking() ? true : null )
                
                .attr( "hint", widget.getHint() )
                .attr( "problem", dumpProblem( widget.getProblem() ) )
                
                .attr( "startupTarget", widget.getTarget() != null ? widget.getTarget().getPath() : null )
                
                .attr( switchEvent( widget.getClick() ) )
                .attr( switchEvent( widget.getLink() ) )
                .attr( switchEvent( widget.getInspect() ) );
        }

        if( widget.getWidth() != null ) {
            if( widget.getWidth().getKind() == Size.Kind.AUTO ) {
                start.attr( "autoWidth", true );
            } else {
                start.style( "width", widget.getWidth() );
            }
        } else if( ! widget.get( "wrap", false ) ) {
            start.attr( "nowrap", true );
        }
        
        start.style( dumpHalign( widget.getHalign() ) );
        start.style( dumpValign( widget.getValign() ) );
        
        output.text( dumpValue( widget ) );
        
        output.end();

    }
    
    protected static Arguments dumpHalign( Label.HorizontalAlignment halign ) {
        if( halign != null ) {
            switch( halign ) {
                case CENTER:
                    return arg( "text-align", "center" );

                case RIGHT:
                    return arg( "text-align", "right" );

                case JUSTIFIED:
                    return arg( "text-align", "justify" );
                
                default:
                    return null;

            }
        } else {
            return null;
        }
    }

    protected static Arguments dumpValign( Label.VerticalAlignment valign ) {
        if( valign != null ) {
            switch( valign ) {
                case MIDDLE:
                    return 
                        arg( "display", "table-cell" )
                        .arg( "vertical-align", "middle" );

                case BOTTOM:
                    return
                        arg( "display", "table-cell" )
                        .arg( "vertical-align", "bottom" );

                default:
                    return null;
            }
        } else {
            return null;
        }
        
    }
    
    protected String dumpValue( Label widget ) {
        String text;
        if( Strings.hasText( widget.getValue() ) ) {
            if( Label.Encoding.HTML == widget.getEncoding() ) {
                text = HTML.sanitize( widget.getValue() ); //TODO: encoding residuo di accentate e cr/lf
            } else {
                text = HTML.encode( widget.getValue() ).replace( "\n", "<br/>" );
                if( ! widget.get( "wrap", false ) && widget.getWidth() == null ) {
                    // In mancanza di una larghezza, rende rigida l'etichetta
                    text = text.replace( " ", "&nbsp;" );
                }
            }
        } else {
            text = "";
        }
        return text;
    }
    
    protected ObjectNode dumpProperty( Label widget, String name ) {
        if( "color".equals( name ) ) {
            return json.property( "color", ColorConverter.toCSS( widget.get( "color" ) ) );

        } else if( "background".equals( name ) ) {
            return json.property( "backgroundColor", ColorConverter.toCSS( widget.get( "background" ) ) );

        } else if( "font".equals( name ) ) {
            return json.property( "font", FontConverter.dumpCSS( this, widget.get( "font" ) ) );

        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.get( "border" ) ) );

        } else if( "value".equals( name ) ) {
            return json.property( "value", dumpValue( widget ) );

        } else if( "problem".equals( name ) ) {
            return json.property( "problem", dumpProblem( widget.getProblem() ) );

        } else if( "target".equals( name ) ) {
            return json.property( "target", widget.getTarget() != null ? widget.getTarget().getPath() : null );

        } else if( "wrap".equals( name ) ) {
            return json.property( "nowrap", ! widget.get( "wrap", false ) );

        } else if( "rounding".equals( name ) ) {
            return json.property( "rounding", CornersConverter.dumpCSS( this, "border-radius", widget.getRounding() ) );

        } else if( "shadow".equals( name ) ) {
            return json.property( "shadow", ShadowConverter.dumpCSS( this, widget.getShadow() ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected Object dumpProblem( Exception problem ) {
        if( problem != null ) {
            
            if( problem instanceof Flavored ) {
                ObjectNode result = json.object();
                result.put( "message", problem.getMessage() );
                result.put( "flavor", dumpFlavor( (Flavored) problem ) );
                return result;
            } else {
                return problem.getMessage();
            }
            
        } else {
            return null;
        }
    }
    
    public static String renderStaticLabel( Label label ) {
        if( label != null ) {
            return HTML
                .span()
                .style( "color", label.getColor() )
                .style( "background-color", label.getBackground() )
                .style( label.getFont() )
                .style( dumpHalign( label.getHalign() ) )
                .style( dumpValign( label.getValign() ) )
                .raw( label.getEncoding() == Label.Encoding.HTML ? HTML.sanitize( label.getValue() ) : HTML.encode( label.getValue() ) )
                .end()
                .toString();
        } else {
            return null;
        }
    }
    
}