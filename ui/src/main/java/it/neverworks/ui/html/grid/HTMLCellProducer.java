package it.neverworks.ui.html.grid;

import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.grid.HTMLCell;
import it.neverworks.ui.data.DataSource;

public class HTMLCellProducer<T extends HTMLCell> extends PropertyCellProducer<T>{

    protected Object getCellValue( T cell, DataSource source, Object item ) {
        if( cell.getSecure() ) {
            return HTML.sanitize( cell.getFormattedValue( source, item ) );
        } else {
            return Strings.valueOf( cell.getFormattedValue( source, item ) );
        }
    }
    
    public ObjectNode dumpColumn( T cell ) {
        ObjectNode columnNode = super.dumpColumn( cell );

        columnNode.put( "clickAttributes", true );
        
        return columnNode;
    }
    
}