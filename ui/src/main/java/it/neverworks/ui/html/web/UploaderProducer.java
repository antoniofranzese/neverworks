package it.neverworks.ui.html.web;

import static it.neverworks.language.*;

import java.net.URLDecoder;

import net.sf.uadetector.UserAgentFamily;
import net.sf.uadetector.ReadableUserAgent;

import it.neverworks.encoding.URL;
import it.neverworks.model.events.Event;
import it.neverworks.model.io.State;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.model.context.Inject;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileRepository;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.web.UploadEvent;
import it.neverworks.ui.web.Uploader;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.Widget;

public class UploaderProducer extends HTMLProducer<Uploader> implements Factory, Model {
    
    public void render( Uploader widget ) {
        widget.set( "production.events", list( "change" ) );

        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/web/Uploader" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )

            .attr( switchEvent( widget.getChange() ) )
            .attr( switchEvent( widget.getBegin() ) )

            // .attr( "showInput", "after" )
            .attr( "visible", widget.getVisible() )
            .attr( "url", getUrl() );
        
        ReadableUserAgent ua = Device.get( "userAgent" );
        if( ua.getFamily() == UserAgentFamily.FIREFOX && ua.getVersionNumber().getMajor().equals( "3" ) ) {
            start.attr( "force", "iframe" );
        }

        output.end();
    
    }
    
    public Event convertEvent( Uploader widget, String name, State state ) {
        if( "change".equals( name ) ) {
            UploadEvent event = new UploadEvent( widget ).set( "files", state.get( "files" ) );
            for( FileInfo info: event.getFiles() ) {
                info.set( "repository", getRepository() );
                info.set( "name", URL.decode( info.getName() ) );
            }
            return event;
        } else {
            return super.convertEvent( widget, name, state );
        
        }
    }
    
    public <T extends Widget> Producer<T> pick( T widget ) {
        return (Producer<T>)this;
    }
    
    @Property @Required @Inject( value = { "it.neverworks.ui.web.FileRoot", "it.neverworks.ui.FileRoot" }, lazy = true )
    private String url;
    
    public String getUrl(){
        return this.url;
    }
    
    public void setUrl( String url ){
        this.url = url;
    }
    
    @Property @Inject({ "it.neverworks.ui.web.FileRepository", "it.neverworks.ui.FileRepository", "it.neverworks.FileRepository" })
    private FileRepository repository;
    
    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }
}