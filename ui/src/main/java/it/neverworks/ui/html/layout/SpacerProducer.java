package it.neverworks.ui.html.layout;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.ui.layout.Spacer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.BackgroundConverter;

public class SpacerProducer extends HTMLProducer<Spacer> {
    
    public void render( Spacer widget ){

        output.dojo.start( "div")
            .attr( "type", "works/layout/Spacer" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "width", SizeConverter.toCSS( this, widget.getWidth() ) )
            .attr( "height", SizeConverter.toCSS( this, widget.getHeight() ) )
            .style( BackgroundConverter.renderCSS( this, widget.getBackground() ) )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( dumpFlavor( widget ) );
        
        output.text( "&nbsp;" );    
        output.end();
    }
    
    protected ObjectNode dumpProperty( Spacer widget, String name ) {
        if( "background".equals( name ) ) {
            return json.property( "background", BackgroundConverter.dumpCSS( this, widget.getBackground() ) );

        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.getBorder() ) );

        } else if( "width".equals( name ) || "height".equals( name ) ) {
            return json.property( name, SizeConverter.toCSS( this, widget.get( name ) ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
}