package it.neverworks.ui.html.canvas;

import static it.neverworks.language.*;

import java.util.Map;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Scene;
import it.neverworks.ui.canvas.Circle;
import it.neverworks.ui.canvas.Line;
import it.neverworks.ui.canvas.Polygon;
import it.neverworks.ui.canvas.Rectangle;
import it.neverworks.ui.canvas.Ring;
import it.neverworks.ui.canvas.Text;
import it.neverworks.ui.canvas.Triangle;

public class SceneProducer {

    private static Map<Class,ShapeProducer> producers = dict(
        pair( Circle.class, new CircleProducer() )
        ,pair( Line.class, new LineProducer() )
        ,pair( Polygon.class, new PolygonProducer() )
        ,pair( Rectangle.class, new RectangleProducer() )
        ,pair( Ring.class, new RingProducer() )
        ,pair( Text.class, new TextProducer() )
        ,pair( Triangle.class, new TriangleProducer() )
    );

    public static JsonNode dump( Scene scene ) {
        ArrayNode shapes = JSON.array();
        
        for( Shape shape: scene.getShapes() ) {
            if( producers.containsKey( shape.getClass() ) ) {
                shapes.add( producers.get( shape.getClass() ).dump( shape ) );
            }
        }

        return shapes;

    }

}