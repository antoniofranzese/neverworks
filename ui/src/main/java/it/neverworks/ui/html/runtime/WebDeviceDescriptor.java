package it.neverworks.ui.html.runtime;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Locale;

import net.sf.uadetector.service.UADetectorServiceFactory;
import net.sf.uadetector.UserAgentStringParser;

import it.neverworks.lang.Strings;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.runtime.Environment;

public class WebDeviceDescriptor {
    
	private static UserAgentStringParser userAgentParser = UADetectorServiceFactory.getResourceModuleParser();
    private static Map<String,Locale> locales;
    
    static {
        locales = new HashMap();
        for( Locale lc: Locale.getAvailableLocales() ) {
            String key = lc.getLanguage() + ( Strings.hasText( lc.getCountry() ) ? "-" + lc.getCountry() : "" );
            locales.put( key.toLowerCase(), lc );
        }
    }

    public static void parseAgent( String header ) {
        Device.set( "userAgent", userAgentParser.parse( header ) );
        Device.set( "environment", Environment.WEB );
    }
    
    public static void parseLanguage( String header ) {
        if( Strings.hasText( header ) ) {
            String[] langs = header.split( "," );
            List languages = new ArrayList( langs.length );
            for( int i = 0; i < langs.length; i++ ) {
                String lang = langs[ i ].trim().toLowerCase();
                if( lang.indexOf( ";" ) >= 0 ) {
                    lang = lang.substring( 0, lang.indexOf( ";" ) ).trim().toLowerCase();
                }
                if( locales.containsKey( lang ) ) {
                    languages.add( locales.get( lang ) );
                }
            }
            Device.set( "languages", languages );
        } else {
            List languages = new ArrayList( 1 );
            languages.add( Locale.ITALIAN );
            Device.set( "languages", languages );
        }
    }
}