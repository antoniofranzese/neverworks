package it.neverworks.ui.html.production;

import it.neverworks.ui.Widget;
import it.neverworks.ui.production.Producer;

public class NullProducer<T extends Widget> extends HTMLProducer<T> {
    public void produce( Widget widget ) {}
    public void render( T widget ) {}
}