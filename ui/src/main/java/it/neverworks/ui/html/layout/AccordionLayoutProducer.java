package it.neverworks.ui.html.layout;

import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.layout.AccordionLayout;

public class AccordionLayoutProducer<T extends AccordionLayout> extends StackLayoutProducer<T> {
    
    protected Tag start( T widget ) {
        return super.start( widget )
            .attr( switchEvent( widget.getSelecting() ) )
            .attr( switchEvent( widget.getSelect() ) );
    }
    
    protected String dojoType() {
        return "works/layout/AccordionContainer";
    }

}