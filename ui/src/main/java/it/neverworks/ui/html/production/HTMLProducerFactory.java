package it.neverworks.ui.html.production;

import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Reflection;
import it.neverworks.ui.html.data.ArtifactResolver;
import it.neverworks.ui.html.data.BaseArtifactResolver;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.Context;
import it.neverworks.ui.production.Mapper;
import it.neverworks.ui.Widget;

public class HTMLProducerFactory implements Factory, HTMLFactory {
    
    protected Mapper mapper = new HTMLPackageBasedMapper();
    protected Map<Class<?>, Factory> factories = new HashMap<Class<?>, Factory>();
    protected ArtifactResolver artifactResolver = new BaseArtifactResolver();
    protected boolean strict = false;
    
    public <T extends Widget> Producer<T> pick( T widget ) {
        if( factories.containsKey( widget.getClass() ) ) {
            //System.out.println( "Factory for " + widget.getClass() + " > " + factories.get( widget.getClass() ) );
            return factories.get( widget.getClass() ).pick( widget );
        } else {
            Class<? extends Producer> producerClass = mapper.map( widget.getClass() );
            //System.out.println( "For " + widget.getClass() + " > " + producerClass );
            if( producerClass != null ) {
                return (Producer)Reflection.newInstance( producerClass );
            } else {
                if( this.strict ) {
                    throw new ProductionException( "Cannot find producer for " + widget.getClass().getName() + " instance" );
                } else {
                    return new NullProducer<T>();
                }
            }
        }
        
    }
    
    public HTMLExecutableProducer with( Context context, HTMLOutput output ) {
        return new HTMLExecutableProducer( this, context, output );
    }

    public HTMLExecutableProducer with( Context context, JSONAnalysis analysis ) {
        return new HTMLExecutableProducer( this, context, analysis );
    }
    
    public void setMapper( Mapper mapper ){
        this.mapper = mapper;
    }
    
    public void setFactories( Map<Class<?>, Factory> factories ){
        this.factories = factories;
    }
    
    public ArtifactResolver getArtifactResolver(){
        return this.artifactResolver;
    }
    
    public void setArtifactResolver( ArtifactResolver artifactResolver ){
        this.artifactResolver = artifactResolver;
    }
    
    public boolean getStrict(){
        return this.strict;
    }
    
    public void setStrict( boolean strict ){
        this.strict = strict;
    }
    

}