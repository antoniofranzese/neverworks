package it.neverworks.ui.html.data;

import it.neverworks.ui.data.Artifact;

public class WebFileArtifactResolver implements ArtifactResolver {
    
    public Object resolveArtifact( Artifact artifact ) {
        return new HrefArtifact().set( "content", ((WebFileArtifact) artifact).getURL() );
    }

}