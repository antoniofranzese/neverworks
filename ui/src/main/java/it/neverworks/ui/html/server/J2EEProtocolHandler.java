package it.neverworks.ui.html.server;

import static it.neverworks.language.*;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

import it.neverworks.log.Logger;
import it.neverworks.lang.Threads;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Framework;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.io.Streams;
import it.neverworks.httpd.InjectableServlet;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.ui.production.Context;
import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.html.production.ProtocolManager;
import it.neverworks.ui.html.runtime.ClientConfigModule;
import it.neverworks.ui.html.runtime.UserAgentModule;

public class J2EEProtocolHandler extends InjectableServlet {
    
    private static final Logger logger = Logger.of( J2EEProtocolHandler.class );
    
    static {
        Device.registerModule( new ClientConfigModule() );
        Device.registerModule( new UserAgentModule() );
    }

    private ExpandoModel webConfig = null;
    private String applicationPath = null;
    private String baseURL = null;
    
    public void init( ServletConfig config ) throws ServletException {
        super.init( config );
        this.applicationPath = config.getServletContext().getContextPath();
        this.webConfig = new ExpandoModel( arg( "Root", this.applicationPath ) );
        if( this.path != null ) {
            this.baseURL = this.applicationPath + this.path;
        }
    }
 
    protected Context createContext( HttpServletRequest request, HttpServletResponse response ) throws IOException {
        response.addHeader( "X-NWK-Environment", "production" );
        
        Context context = new Context();
        context.set( "Environment", "production" );
        context.set( "Web", this.webConfig );
        context.set( "ApplicationPath", this.applicationPath ); 
        context.set( "RequestURL", request.getContextPath() + request.getServletPath() + request.getPathInfo() );
        context.set( "BaseURL", this.baseURL != null ? this.baseURL : this.applicationPath + request.getServletPath() ); 
        context.set( "QueryString", Strings.safe( request.getQueryString() ) );

        if( gzip ) {
            try {
                context.set( "GZipContent", request.getHeader( "Accept-Encoding" ).indexOf( "gzip" ) >= 0 );
            } catch( Exception ex ) {
                context.set( "GZipContent", false );
            }
        } else {
            context.set( "GZipContent", false );
        }

        if( context.<Boolean>get( "GZipContent" ) ) {
            response.addHeader( "Content-Encoding", "gzip" );
        }

        Device.register( "header", new J2EEHeaderProvider( request ) );
        
        if( this.deviceId ) {
            String id = null;
            String cookieHeader = request.getHeader( "Cookie" );
            if( Strings.hasText( cookieHeader ) ) {
                Arguments cookies = Arguments.parse( cookieHeader, '=', ';' );
                id = cookies.get( "NWK-ID" );
            }

            if( Strings.isEmpty( id ) ) {
                id = DeviceHelper.generateID();
            }

            if( "GET".equals( request.getMethod() ) ) {
                Cookie cookie = new Cookie( "NWK-ID", id );
                cookie.setPath( "/" );
                cookie.setMaxAge( DeviceHelper.ID_AGE );
                response.addCookie( cookie );
            }
        
            Device.set( "id", id );
        }
        
        return context;
    }
    
    protected String getFormPath( HttpServletRequest request ) {
        // Path richiesto da un altro componente (es. filtro)
        if( request.getAttribute( "FormPath" ) != null ) {
            return (String)request.getAttribute( "FormPath" );
            
        // Path impostato in configurazione
        } else if( this.form != null ) {
            return this.form;
            
        // Path richiesto da URL    
        } else if( request.getPathInfo() != null ) {
            return request.getPathInfo().substring( 1 );  
        
        } else {
            return "";
        }
    }
    
    protected void doHead( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException  {
        try {

            Context context = new Context();
            context.set( "Environment", "production" );
            context.set( "FormPath", request.getPathInfo().substring( 1 ) );

            context.set( "Method", request.getHeader( "x-nwk-method" ) );

            Arguments params = new Arguments();
            context.put( "Parameters", params );
            for( String rawHeader: Collections.<String>list( request.getHeaderNames() ) ) {
                String header = rawHeader.toLowerCase();
                if( header.startsWith( "x-nwk-" ) ) {
                    params.put( header.substring( 6 ), request.getHeader( rawHeader ) );
                }
            }

            try {
                context.set( "Peers", Integer.parseInt( request.getHeader( "x-nwk-peers" ) ) );
            } catch( Exception ex ) {
                context.set( "Peers", 1 );
            }

            protocolManager.probe( context );

            response.addHeader( "X-NWK-Method", context.get( "Method" ) );
            if( context.contains( "Return" ) ) {
                Arguments ret = context.get( "Return" );

                for( String name: ret.names() ) {
                    response.addHeader( "X-NWK-" + name, Strings.valueOf( ret.get( name ) ) );
                }
            }

        } finally {
            Threads.cleanup();
        }
    }
    
    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException  {
        //System.out.println( "Servicing " + request.getContextPath() + " - " + request.getServletPath() + " - " + request.getPathInfo() );
        try {

            Device.init();

            Context context = createContext( request, response );
            //System.out.println( "Form: " + getFormPath( request ) );
            context.set( "FormPath", getFormPath( request ) );
            response.addHeader( "X-NWK-Method", "CREATE" );
            response.setContentType( "text/html" );
            response.setCharacterEncoding( "UTF-8" );
        
            if( context.<Boolean>get( "GZipContent" ) ) {
                context.set( "OutputStream", response.getOutputStream() );
            } else {
                context.set( "OutputWriter", response.getWriter() );
            }

            try {
                protocolManager.create( context );
            } catch( Exception ex ) {
                throw new ServletException( Errors.unwrap( ex ) );
            }

        } finally {
            Threads.cleanup();
        }
        
    }

    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException  {
        // System.out.println( "Posting " + request.getContextPath() + " - " + request.getServletPath() + " - " + request.getPathInfo() );
        try {

            Device.init();

            Context context = createContext( request, response );
            context.set( "FormPath", request.getPathInfo().substring( 1 ) );
            context.set( "Body", Streams.asString( request.getInputStream() ) );
            response.addHeader( "X-NWK-Method", "HANDLE" );
            response.setContentType( "application/json" );
            response.setCharacterEncoding( "UTF-8" );

            if( context.<Boolean>get( "GZipContent" ) ) {
                context.set( "OutputStream", response.getOutputStream() );
            } else {
                context.set( "OutputWriter", response.getWriter() );
            }

            try {
                protocolManager.handle( context );
            } catch( Exception ex ) {
                throw new ServletException( Errors.unwrap( ex ) );
            }
            
        } finally {
            Threads.cleanup();
        }
    }
    
    private ProtocolManager protocolManager;
    
    public void setProtocolManager( ProtocolManager protocolManager ){
        this.protocolManager = protocolManager;
    }
 
    private String form = null;
    
    public void setForm( String form ){
        this.form = form.replace( ".", "/" );
    }
    
    private String path = null;
    
    public void setPath( String path ){
        this.path = path;
    }
    
    private boolean development = false; 
    
    public void setDevelopment( boolean development ){
        this.development = development;
    }
    
    private boolean gzip = false; 
    
    public void setGzip( Object gzip ){
        this.gzip = Boolean( gzip );
    }
    
    private boolean deviceId = true;
    
    public void setDeviceId( boolean deviceId ){
        this.deviceId = deviceId;
    }
    
}