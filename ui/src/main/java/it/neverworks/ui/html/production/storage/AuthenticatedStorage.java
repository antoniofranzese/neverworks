package it.neverworks.ui.html.production.storage;

import javax.annotation.Resource;

import it.neverworks.lang.Strings;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.Widget;
import it.neverworks.ui.BaseWidget;

import it.neverworks.security.context.UserInfo;
import it.neverworks.security.model.AuthenticatedUser;

public class AuthenticatedStorage<T extends Widget> extends BaseStorage<T> {
    
    private class WidgetInfo extends BaseWidget {
        public String user;
        public T widget;
        
        public WidgetInfo( String user, T widget ) {
            this.user = user;
            this.widget = widget;
        }
    }
    
    protected Iterable<T> all() {
        return storage.map( userWidget -> userWidget.widget );
    }
    
    public boolean contains( String key ) {
        return storage.contains( key );
    }
    
    public T load( String key ) {
        WidgetInfo uw = storage.load( key );
        if( uw.user == null ) {
            return uw.widget;
        } else {
            String user = userInfo.get( "id" );
            if( uw.user.equals( user ) ) {
                return uw.widget;
            } else {
                // System.out.println( "Current: " + user + ", Form: " + uw.user );
                throw new ProductionException( "Current user is not authorized for " + key + " form" );
            }
        }
    }
    
    public void save( String key, T widget ) {
        String user = userInfo.get( "id" );
        // System.out.println( "Storing for " + user );
        storage.save( key, new WidgetInfo( user, widget ) );
    }

    public void delete( String key ) {
        storage.delete( key );
    }
    
    @Resource 
    private UserInfo userInfo;
    
    public void setUserInfo( UserInfo userInfo ){
        this.userInfo = userInfo;
    }
    
    @Resource
    protected WidgetStorage<WidgetInfo> storage;

    public void setStorage( WidgetStorage<WidgetInfo> storage ){
        this.storage = storage;
    }
    
}