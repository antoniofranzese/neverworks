package it.neverworks.ui.html.menu;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import static it.neverworks.language.*;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.WidgetChangeProposal;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.menu.Menu;
import it.neverworks.ui.menu.MenuItem;
import it.neverworks.ui.menu.CheckedMenuItem;
import it.neverworks.ui.menu.MenuSeparator;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.Widget;

public class MenuProducer extends HTMLProducer<Menu> {
    
    public void render( Menu widget ) {
        
        output.dojo.start( "div" )
            .style( "display", "none" )
            .attr( "type", "works/menu/Menu" )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "startupOpened", widget.getOpened() )
            .attr( "anchor", dumpAnchor( widget ) );
        
        output.start( "script" ).attr( "type", "works/props" );
        output.text( JSON.js.encode( arg( "startupItems", dumpItems( widget ) ) ).toString() );
        output.end();    
    
        output.end();
        
    }
    
    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.object();
        Menu menu = (Menu) proposal.getWidget();
        List<String> changes = ((WidgetChangeProposal) proposal).getChanges();

        for( String property: changes ) {
            if( property.indexOf( "." ) < 0 ) {
                if( "items".equals( property ) ) {
                    result.set( "items", dumpItems( menu ) );
                } else if( "anchor".equals( property ) ) {
                    result.set( "anchor", dumpAnchor( menu ) );
                } else {
                    result.set( property, JSON.js.encode( menu.get( property ) ) );
                }
            }
        }

        if( ! changes.contains( "items" ) ) {
            ArrayNode changesNode = json.array();
            for( String property: changes ) {
                if( property.indexOf( "." ) >= 0 ) {
                    ObjectNode change = json.object();
                    change.put( "path", property );
                    change.set( "value", JSON.js.encode( menu.get( property ) ) );
                    changesNode.add( change );
                }
            }
        
            if( changesNode.size() > 0 ) {
                result.set( "changes", changesNode );
            }
        }
            
        return result;
    }
    
    protected JsonNode dumpAnchor( Menu menu ) {
        if( menu.getAnchor() instanceof Point ) {
            return JSON.js.encode(
                arg( "x", menu.get( "anchor.x" ) )
                .arg( "y", menu.get( "anchor.y" ) )
            );
        } if( menu.getAnchor() instanceof Widget ) {
            return JSON.js.encode(
                arg( "widget", menu.get( "anchor.path" ) )
            );
        } else {
            return null;
        }
    }
    protected JsonNode dumpItems( Menu menu ) {
        ArrayNode itemsNode = json.array();

        for( MenuItem item: menu.getChildren() ) {
            ObjectNode itemNode = json.object();
            itemNode.put( "path", item.getPath() );
            itemNode.put( "name", item.getName() );
            itemNode.put( "label", item.getCaption() );
            itemNode.put( "disabled", item.getDisabled() );
            itemNode.put( "visible", item.getVisible() );
            itemNode.put( "color", ColorConverter.toCSS( item.getColor() ) );
            itemNode.put( "background", ColorConverter.toCSS( item.getBackground() ) );
            itemNode.put( "font", JSON.js.encode( FontConverter.dumpCSS( this, item.getFont() ) ) );
            
            if( item instanceof Menu ) {
                itemNode.set( "items", dumpItems( (Menu) item ) );
            } else if( item instanceof CheckedMenuItem ) {
                itemNode.put( "type", "checked" );
            } else if( item instanceof MenuSeparator ) {
                itemNode.put( "type", "separator" );
            }
            
            itemsNode.add( itemNode );
        }
        
        return itemsNode;
    }
    
}