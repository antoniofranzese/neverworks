package it.neverworks.ui.html.form;

import java.util.Date;
import it.neverworks.ui.form.TimeTextBox;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class TimeTextBoxProducer<T extends TimeTextBox> extends DateTextBoxProducer<T> {
    
    protected final static DateTimeFormatter iso = ISODateTimeFormat.dateHourMinuteSecond();

    @Override
    protected String dojoType( T widget ) {
        return "works/form/TimeTextBox";
    }
    
    
    @Override
    protected Object value( T widget ) {
        Date value = widget.get( "value" );
        
        return value != null ? iso.print( value.getTime() ) : null;
    }
    
}