package it.neverworks.ui.html.form;

import java.util.Date;

import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.form.DateTextBox;

import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.DateTimeZone;

public class DateTextBoxProducer<T extends DateTextBox> extends BaseTextBoxProducer<T> {
    
    protected final static DateTimeFormatter iso = ISODateTimeFormat.date();
    
    @Override
    protected String dojoType( T widget ) {
        return "works/form/DateTextBox";
    }
    
    @Override
    protected Object value( T widget ) {
        Date value = widget.get( "value" );
        return value != null ? iso.print( value.getTime() ) : null;
    }
}