package it.neverworks.ui.html.web;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.lang.reflect.Method;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Reflection;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.layout.BorderLayoutProducer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.web.WebView;

public class WebViewProducer extends HTMLProducer<WebView> {
    
    public void render( WebView widget ) {
        
        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/web/WebView" )
            .attr( "name",  widget.getName() ) 
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "displaced", Boolean.TRUE.equals( widget.get( "displaced" ) ) ? true : null )
            .attr( "loader", widget.getLoader() )
            .attr( "startupBookmark", widget.getBookmark() )
            .attr( "visible", widget.getVisible() ? null : false )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            ;
        ObjectNode propsNode = json.object();

        if( widget.getLocation() != null ) {
            propsNode.set( "startupLocation", JSON.js.encode( widget.getLocation().toStringURL() ) );
        }
        if( widget.get( "executionQueue" ) != null ) {
            propsNode.set( "startupQueue", JSON.js.encode( widget.get( "executionQueue" ) ) );
        }
        if( widget.getContent() != null ) {
            propsNode.set( "startupContent", JSON.js.encode( widget.getContent() ) );
        }

        getEncoders( widget );
        if( widget.get( "initProperties" ) != null ) {
            ObjectNode initPropsNode = json.object();
            Map<String,Object> initProperties = widget.get( "initProperties" );
            for( String name: initProperties.keySet() ) {
                initPropsNode.setAll( dumpProperty( widget, name ) );
                // initPropsNode.set( name, JSON.js.encode( initProperties.get( name ) ) );
            }
            propsNode.set( "initProperties", initPropsNode );
        }

        output.start( "script" ).attr( "type", "works/props"  );
        output.text( propsNode.toString() );
        output.end( "script" );

        output.end();
        widget.value( "executionQueue" ).clear();
    }
    
    protected Map<String,Method> getEncoders( WebView widget ) {
        if( widget.get( "!production.encoders" ) == null ) {
            synchronized( widget ) {
                if( widget.get( "!production.encoders" ) == null ) {
                    Map<String,Method> encoders = new HashMap<String,Method>();
                    for( Method encoder: Reflection
                        .findMethods( widget.getClass(), Reflection.AnyType.class )
                        .filter( m -> m.getName().matches( "encode[A-Z]?.*" ) && ! m.getReturnType().equals( void.class ) ) 
                        ) {
            
                        String name = encoder.getName().substring( 6, 7 ).toLowerCase() + encoder.getName().substring( 7 );
                        encoders.put( name, encoder );
                    }
                    
                    widget.set( "!production.encoders", encoders );
                }
            }
        }
        return widget.get( "!production.encoders" );
    }
    
    protected ObjectNode dumpProperty( WebView widget, String name ) {
        if( "content".equals( name ) ) {
            return json.property( "bodyContent", JSON.js.encode( widget.getContent() ));
        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.getBorder() ) );
        } else if( getEncoders( widget ).containsKey( name ) ) {
            Object value = widget.get( name );
            return json.property( name, JSON.js.encode( value != null ? Reflection.invokeMethod( widget, getEncoders( widget ).get( name ), value ) : null ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }

    public JsonNode dump( ChangeProposal proposal ) {
        JsonNode result = super.dump( proposal );
        ((WebView) proposal.getWidget()).value( "executionQueue" ).clear();
        return result;
    }
}