package it.neverworks.ui.html.server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.servlet.ServletRequestContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;

import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ApplicationEventPublisher;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;
import it.neverworks.io.Streams;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.URL;
import it.neverworks.httpd.InjectableServlet;
import it.neverworks.security.context.UserInfo;
import it.neverworks.io.MissingFileException;
import it.neverworks.io.MissingFileEvent;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileProvider;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;
import it.neverworks.log.Logger;

public class J2EEFileManager extends InjectableServlet implements ApplicationEventPublisherAware {

    private static final Logger logger = Logger.of( J2EEFileManager.class );
    
    private class Info {
        private String name;
        private String type;
        private FileBadge badge;
        
        public FileBadge getBadge(){
            return this.badge;
        }
        
        public void setBadge( FileBadge badge ){
            this.badge = badge;
        }

        public String getType(){
            return this.type;
        }
        
        public void setType( String type ){
            this.type = type;
        }
        
        public String getName(){
            return this.name;
        }
        
        public void setName( String name ){
            this.name = name;
        }
        
    }
    
    protected void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        String pathInfo = request.getPathInfo();
        String queryString = request.getQueryString();
        
        logger.debug( "File request: {}, query: {}", pathInfo, queryString );
            
        if( this.download ) {
            String token = null;
            
            try {
                String parameters = pathInfo.startsWith( "/" ) ? pathInfo.substring( 1 ) : pathInfo;
                String name;
                int index = parameters.indexOf( "@" );
                if( index >= 0 ) {
                    name = parameters.substring( 0, index );
                    token = parameters.substring( index + 1 );
                } else {
                    token = parameters;
                    name = null;
                }
        
                FileInfo info = getFileHelper().retrieveFileInfo( token, queryString );
                // System.out.println( "Download info " + info );

                String fileUser = authorization != null ? info.<String>get( "!" + authorization ) : null;
                if( authorization == null || fileUser == null || fileUser.equalsIgnoreCase( authorizableUser() ) ) {
                    if( name == null ) {
                        name = info.getName();
                    }
        
                    if( name == null ) {
                        name = empty( info.get( "name" ) ) ? token : info.get( "name" );
                    }

                    String type = info.getType();
        
                    if( type == null ) {
                        type = "application/octet-stream";
                    }

                    String disposition = empty( info.get( "!disposition" ) ) ? this.disposition : info.get( "disposition" );
 
                    // System.out.println( "Download " + name + " " + token + " " + type );

                    response.addHeader( "Content-Disposition",  disposition + "; filename*=" + FileHelper.encodeFileName( name ) );
                    response.addHeader( "Content-Type", type );
        
                    Streams.copy( info.getStream(), response.getOutputStream() );

                } else {
                    response.sendError( HttpServletResponse.SC_UNAUTHORIZED , "Unauthorized" );
                }

            } catch( MissingFileException ex ) {
                if( publisher != null ) {
                    publisher.publishEvent( new MissingFileEvent( this, token ) );
                }
                response.sendError( HttpServletResponse.SC_NOT_FOUND, "Not Found" );
                
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
            
        } else {
            response.sendError( HttpServletResponse.SC_FORBIDDEN , "Forbidden" );
        }

    }

    protected void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException  {
        // System.out.println( "Upload " + System.getProperty("java.io.tmpdir") );
        
        if( this.upload ) {
            Arguments description = new Arguments();
    
            if( authorization != null ) {
                description.arg( authorization, authorizableUser() );
            }
            
            RequestContext ctx = new ServletRequestContext( request );
            response.getWriter().write( getFileHelper().processUpload( ctx, description ) );
            
        } else {
            response.sendError( HttpServletResponse.SC_FORBIDDEN , "Forbidden" );
        }
    }
    
    public String inferMimeType( File file ) {
        try {
            MagicMatch match = Magic.getMagicMatch( file, /* extensionHints */ true, /* onlyMimeMatch */ false);
    
            if( match != null ) {
                return match.getMimeType();
            } else {
                return "application/octet-stream";
            }
            
        } catch( MagicMatchNotFoundException ex ) {
            return "application/octet-stream";
            
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    protected String authorizableUser() throws ServletException {
        if( authorization != null ){
            if( userInfo != null ) {
                return userInfo.authenticated() ? userInfo.getAuthenticatedUser().getUserID() : null;
            } else {
                throw new ServletException( "Missing UserInfo in " + this.getClass().getSimpleName() );
            }
        } else {
            return null;
        }
    }
    
    private FileProvider provider;

    public FileProvider getFileProvider(){
        if( provider == null ) {
            provider = repository;
        }
        return this.provider;
    }

    public void setFileProvider( FileProvider fileProvider ) {
        this.provider= fileProvider;
    }
    
    private FileRepository repository = new TemporaryFileService();

    public void setFileRepository( FileRepository fileRepository ) {
        this.repository = fileRepository;
    }
    
    public void setTemporaryFiles( FileRepository temporaryFiles ){
        this.repository = temporaryFiles;
    }
    
    private String authorization = null;
    
    public void setAuthorization( String authorization ){
        this.authorization = authorization;
    }

    protected UserInfo userInfo;

    public void setUserInfo( UserInfo userInfo ){
        this.userInfo = userInfo;
    }
    
    private String disposition = "attachment";
    
    public void setDisposition( String disposition ){
        this.disposition = disposition;
    }
    
    private boolean upload = true;
    
    public void setUpload( Object upload ){
        this.upload = type( Boolean.class ).process( upload );
    }
    
    private boolean download = true; 
    
    public void setDownload( Object download ){
        this.download = type( Boolean.class ).process( download );
    }

    protected ApplicationEventPublisher publisher;
    
    @Override
    public void setApplicationEventPublisher( ApplicationEventPublisher publisher ) {
        this.publisher = publisher;
    }
    
    private FileHelper fileHelper;

    public FileHelper getFileHelper(){
        if( this.fileHelper == null ) {
            this.fileHelper = new FileHelper()
                .set( "provider", getFileProvider() )
                .set( "repository", repository );
        }
        return this.fileHelper;
    }
    
}