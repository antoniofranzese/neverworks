package it.neverworks.ui.html.server.detectors;

import static it.neverworks.language.*;

import it.neverworks.log.Logger;
import javax.servlet.http.HttpServletRequest;

import it.neverworks.lang.Strings;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.security.model.AuthenticatedUser;

public class IISUserDetector implements UserDetector {
    
	private static final Logger logger = Logger.of( "AUTH" );

    private String header;

    public static class IISUser implements AuthenticatedUser {

        private String userID;
        private String domain;
        
        public String getDomain(){
            return this.domain;
        }
        
        public IISUser( String userID, String domain ) {
            this.userID = userID;
            this.domain = domain;
        }

        public String getUserID(){
            return this.userID;
        }
        
        public String getId(){
            return this.userID;
        }
        
        public boolean equals( Object other ) {
            return other instanceof IISUser 
                && this.userID != null 
                && this.userID.equals( ((IISUser) other).getUserID() )
                && ( this.domain != null ? this.domain.equals( ((IISUser) other).getDomain() ) : ((IISUser) other).getDomain() == null );
        }
        
        public String toString() {
            return new ToStringBuilder( this ).add( "id" ).add( "domain" ).toString();
        }
        
    }
    
    public AuthenticatedUser detect( HttpServletRequest request ) {
        String userID = null;
        String domain = null;
        
        String qualifiedUser = request.getRemoteUser();
        
        if( Strings.hasText( qualifiedUser ) ) {
            int index = qualifiedUser.indexOf( "\\" );
            if( index >= 0 ) {
                userID = qualifiedUser.substring( index + 1 ).trim();
                domain = qualifiedUser.substring( 0, index ).trim();
            } else {
                userID = qualifiedUser.trim();
            }
        }

        if( Strings.isEmpty( domain ) ) {
            domain = null;
        }
        
        if( Strings.hasText( userID ) ) {
            return new IISUser( userID, domain );
        } else {
            return null;
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this ).toString();
    }
    

}