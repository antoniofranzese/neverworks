package it.neverworks.ui.html.server.detectors;

import static it.neverworks.language.*;

import it.neverworks.log.Logger;
import javax.servlet.http.HttpServletRequest;

import it.neverworks.lang.Strings;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.security.model.AuthenticatedUser;
import it.neverworks.security.context.UserInfo;
import it.neverworks.ui.html.server.J2EEDetectingAuthenticationFilter;

public class JOSSOUserDetector implements UserDetector {
    
	private static final Logger logger = Logger.of( "AUTH" );

    private String header;

    public static class JOSSOUser implements AuthenticatedUser {

        private String userID;

        public JOSSOUser( String userID ) {
            this.userID = userID;
        }

        public String getUserID(){
            return this.userID;
        }
        
        public String getId(){
            return this.userID;
        }
        
        public boolean equals( Object other ) {
            return other instanceof JOSSOUser 
                && this.userID != null 
                && this.userID.equals( ((JOSSOUser) other).getUserID() );
        }
        
        public String toString() {
            return new ToStringBuilder( this ).add( "id" ).toString();
        }
        
    }
    
    public AuthenticatedUser detect( HttpServletRequest request ) {
        String userID = request.getRemoteUser();

        if( Strings.hasText( userID ) ) {
            return new JOSSOUser( userID.trim() );
        } else {
            return null;
        }
    }
    
    public void authenticate( UserInfo info, AuthenticatedUser user ) {
        
        // org.josso.agent.CatalinaSSOAgent ssoAgent = (CatalinaSSOAgent)org.josso.Lookup.getInstance().lookupSSOAgent();
        // String logoutURL = ssoAgent.getGatewayLogoutUrl();

        info.setAuthenticatedUser( user );
        info.setAttribute( J2EEDetectingAuthenticationFilter.LOGOUT_URL_ATTRIBUTE, "/josso_logout/" );
    }

    public String toString() {
        return new ToStringBuilder( this ).toString();
    }
    

}