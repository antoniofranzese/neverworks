package it.neverworks.ui.html.server;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Map;
import java.util.HashMap;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import static it.neverworks.language.*;
import it.neverworks.httpd.StaticFileHandler;
import it.neverworks.httpd.Request;
import it.neverworks.httpd.Response;
import it.neverworks.httpd.UploadRequestContext;
import it.neverworks.io.MissingFileException;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileProvider;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;

public class EmbeddedFileManager extends StaticFileHandler {
    
    @Override
    protected void doPost( Request request ) throws Exception {
        Response response = request.getResponse();
        response.setCode( "200" );
        response.getWriter().write( getFileHelper().processUpload( new UploadRequestContext( request ) ) );
    }
    
    @Override
    protected void streamFile( Request request, String localPath ) throws Exception {
        Response response = request.getResponse();
        String name;
        String token;
        String path = localPath.substring( 1 );
        int index = path.indexOf( "@" );

        if( index >= 0 ) {
            name = path.substring( 0, index );
            token = path.substring( index + 1 );
        } else {
            token = path;
            name = null;
        }

        try {
            FileInfo info = getFileHelper().retrieveFileInfo( token, request.getQueryString() );
        
            if( name == null ) {
                name = info.getName();
            }
    
            if( name == null ) {
                name = empty( info.get( "name" ) ) ? token : info.get( "name" );
            }


            String type = info.getType();

            // Search known extensions
            if( type == null && name.indexOf( "." ) > 0 ) {
                String extension = name.substring( name.lastIndexOf( "." ) + 1 );
                if( this.extensions.containsKey( extension ) ) {
                    type = this.extensions.get( extension );
                }
            }
    
            if( type == null ) {
                type = this.defaultMimeType;
            }
            
            String disposition = empty( info.get( "!disposition" ) ) ? this.disposition : info.get( "disposition" );
            
            // System.out.println( "Name: " + name );
            // System.out.println( "Type: " + type );
            // System.out.println( "Disp: " + disposition );

            response.setHeader( "Content-Type", type  );
            response.setHeader( "Content-Disposition", disposition + "; filename*=" + FileHelper.encodeFileName( name ) ); 
            response.setCode( "200" );
    
            InputStream stream = info.getStream();
            response.stream( stream );
            stream.close();
            
        } catch( MissingFileException ex ) {
            response.setCode( "404" ); // Not found
        }
        
    }

    private FileHelper fileHelper;

    public FileHelper getFileHelper(){
        if( this.fileHelper == null ) {
            this.fileHelper = new FileHelper()
                .set( "provider", getProvider() )
                .set( "repository", getRepository() );
        }
        return this.fileHelper;
    }
    
    private FileProvider provider;

    public FileProvider getProvider(){
        if( provider == null ) {
            provider = repository;
        }
        return this.provider;
    }

    public void setProvider( FileProvider fileProvider ) {
        this.provider= fileProvider;
    }

    private FileRepository repository;
    
    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }
    
    public void setFiles( FileRepository repository ){
        this.repository = repository;
    }
    
    private String disposition = "attachment";
    
    public void setDisposition( String disposition ){
        this.disposition = disposition;
    }
}