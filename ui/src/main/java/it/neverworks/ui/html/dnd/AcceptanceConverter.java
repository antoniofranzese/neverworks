package it.neverworks.ui.html.dnd;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;

import static it.neverworks.language.*;
import static it.neverworks.lang.Strings.message;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.JavaScript;
import it.neverworks.model.Model;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.dnd.*;

public class AcceptanceConverter {
    
    // public static JsonNode toJSON( DropTarget target ) {
    //     if( target.getAcceptance() != null ) {
    //         DropAcceptance acceptance = target.getAcceptance();
    //         return JSON.js.encode( renderRules( acceptance ) );
    //
    //     } else {
    //         return null;
    //     }
    // }
    //
    public static JsonNode toJSON( DropAcceptance acceptance ) {
        if( acceptance != null ) {
            return JSON.js.encode( renderRules( acceptance ) );
        } else {
            return null;
        }
    }
    
    private static String renderRules( DropAcceptance acceptance ) {
        String condition;
        if( acceptance.getActive() && acceptance.getRules() != null && acceptance.getRules().getRules().size() > 0 ) {
            List<String> criteria = new ArrayList<String>();
            
            for( DropRule rule: acceptance.getRules().getRules() ) {

                if( rule instanceof EqualsRule ) {
                    criteria.add( message( "p.content[\"{0}\"]=={1}"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                        ,JavaScript.literal( rule.get( "value" ) )
                    ));

                } else if( rule instanceof NotEqualsRule ) {
                    criteria.add( message( "p.content[\"{0}\"]!={1}"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                        ,JavaScript.literal( rule.get( "value" ) )
                    ));

                } else if( rule instanceof StartsRule ) {
                    criteria.add( message( "(\"\"+p.content[\"{0}\"]).startsWith({1})"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                        ,JavaScript.literal( rule.get( "value" ) )
                    ));

                } else if( rule instanceof EndsRule ) {
                    criteria.add( message( "(\"\"+p.content[\"{0}\"]).endsWith({1})"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                        ,JavaScript.literal( rule.get( "value" ) )
                    ));

                } else if( rule instanceof ContainsRule ) {
                    criteria.add( message( "(\"\"+p.content[\"{0}\"]).indexOf({1})>=0"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                        ,JavaScript.literal( rule.get( "value" ) )
                    ));

                } else if( rule instanceof InRule ) {
                    criteria.add( message( "{1}.indexOf(p.content[\"{0}\"])>=0"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                        ,JavaScript.literal( rule.get( "values" ) )
                    ));

                } else if( rule instanceof NullRule ) {
                    criteria.add( message( "(\"\"+p.content[\"{0}\"])==null"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                    ));

                } else if( rule instanceof NotNullRule ) {
                    criteria.add( message( "(\"\"+p.content[\"{0}\"])!=null"
                        ,JavaScript.encode( rule.get( "property" ) ) 
                    ));

                }
            }

            condition = Collections.join( criteria, " && " );
            
        } else {
            condition = acceptance.getActive() ? "true" : "false";
        }

        return "function(p){return " + condition + ";}";
    }
    
    
}