package it.neverworks.ui.html.grid;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Date;

import it.neverworks.lang.Dates;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.grid.DateCell;

public class DateCellProducer extends PropertyCellProducer<DateCell>{

    public ObjectNode dumpColumn( DateCell cell ) {
        ObjectNode columnNode = super.dumpColumn( cell );

        ObjectNode decorator = json.createObjectNode();
        decorator.put( "$type", cell.getType() );
        decorator.put( "format", cell.getPattern() != null ? cell.getPattern() : "dd/MM/yyyy" );
        columnNode.set( "decoratorParams", decorator );
        
        return columnNode;
    }
    
    public void populateItem( DateCell cell, ObjectNode itemNode, DataSource source, Object item ) {
        Object value = cell.getValue( source, item );
        itemNode.set( cell.getName(), JSON.encode( value instanceof Date ? Dates.toISO( (Date)value ) : value ) );
    }
    

}