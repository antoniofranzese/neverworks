package it.neverworks.ui.html.form;

import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.model.description.Changes;
import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.LayoutCapable;
import it.neverworks.ui.layout.LayoutInfo;
import it.neverworks.ui.html.layout.GridLayoutProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.BaseContainerProducer;
import it.neverworks.ui.html.types.ViewportInfoConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.BackgroundConverter;

public class ToolBarProducer extends BaseContainerProducer<ToolBar> {
    
    public void render( ToolBar widget ){
        boolean horizontal = widget.getOrientation() == null || widget.getOrientation().equals( ToolBar.Orientation.HORIZONTAL ); 
        
        output.dojo.start( "table" )
            .attr( "type", "works/form/ToolBar" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            //.attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( "collapsible", widget.getCollapsible() )
            .attr( "halign", dumpHalign( widget.getHalign() ) )
            .attr( "valign", dumpValign( widget.getValign() ) )
            .attr( "justify", dumpJustify( widget.getJustify() ) )
            .style( "worksToolBar" )
            .style( horizontal ? "horizontal" : "vertical" )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( BackgroundConverter.renderCSS( this, widget.getBackground() ) )
            .style( dumpFlavor( widget ) );
        
        output.start( "tr" );
        output.start( "td" );

        output.start( "table" )
            .style( "worksToolBarContent" )
            .style( horizontal ? "horizontal" : "vertical" )
            .style( "width", widget.getWidth() != null ? "100%" : null )
            .style( "height", widget.getHeight() != null ? "100%" : null )
            .style( dumpFlavor( widget ) );
        
        if( horizontal ) {
            output.start( "tr" );
        }
        
        for( Widget child: widget.getChildren() ) {
            if( !horizontal ) {
                output.start( "tr" );
            }

            Tag td = output.start( "td" )
                .style( "toolbarCell" )
                .style( Boolean.FALSE.equals( child.get( "visible" ) ) ? "hidden" : null );
            
            //TODO: refactor as GridLayout
            if( child instanceof LayoutCapable ) {
                String childName = child.get( "name" );
                LayoutInfo layout = child.get( "layout" );
                if( layout != null ) {
                    if( layout.contains( "halign" ) ) {
                        td.attr( "align", GridLayoutProducer.dumpCellHalign( GridLayoutProducer.CellHalignType.process( layout.get( "halign" ), "{0}.{1}", childName, "layout.halign" ) ) );
                    }
                    if( layout.contains( "valign" ) ) {
                        td.attr( "valign", GridLayoutProducer.dumpCellValign( GridLayoutProducer.CellValignType.process( layout.get( "valign" ), "{0}.{1}", childName, "layout.valign" ) ) );
                    }
                }
            }
            
            produce( child );
            output.end( "td" );

            if( !horizontal ) {
                output.end( "tr" );
            }

        }

        if( horizontal ) {
            output.end( "tr" );
        }

        output.end( "table" );
        
        output.end( "td" );
        output.end( "tr" );
        output.end( "table" );
    }
    
    protected void analyzeWidget( ToolBar widget ) {
        Changes changes = widget.model().changes();
        if( changes.contains( "visible" ) ) {
            analysis.propose( this, widget, list( "visible" ) );
        }
    }

    protected ObjectNode dumpProperty( ToolBar widget, String name ) {
        if( "halign".equals( name ) ) {
            return json.property( name, dumpHalign( widget.getHalign() ) );
        } else if( "valign".equals( name ) ) {
            return json.property( name, dumpValign( widget.getValign() ) );
        } else if( "justify".equals( name ) ) {
            return json.property( name, dumpJustify( widget.getJustify() ) );
        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.get( "border" ) ) );
        } else if( "background".equals( name ) ) {
            return json.property( "background", BackgroundConverter.dumpCSS( this, widget.get( "background" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    protected String dumpHalign( ToolBar.HorizontalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ) {
                case CENTER:
                    return "center";
                case RIGHT:
                    return "right";
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    protected String dumpValign( ToolBar.VerticalAlignment alignment ) {
        if( alignment != null ) {
            switch( alignment ) {
                case MIDDLE:
                    return "middle";
                case BOTTOM:
                    return "bottom";
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    protected String dumpJustify( ToolBar.Justification justification ) {
        if( justification != null ) {
            switch( justification ) {
                case WIDTH:
                    return "width";
                case HEIGHT:
                    return "height";
                case BOTH:
                    return "both";
                default:
                    return "none";
            }
        } else {
            return "none";
        }
    }
 
}