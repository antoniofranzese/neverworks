package it.neverworks.ui.html.production.storage;

import static it.neverworks.language.*;

import org.springframework.beans.factory.DisposableBean;

import it.neverworks.lang.Strings;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.ui.lifecycle.Destroyable;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.MissingStoredWidgetException;
import it.neverworks.ui.Widget;

public class MemoryStorage<T extends Widget> extends BaseStorage<T> implements DisposableBean {

    protected FluentMap<String, T> widgets = new FluentHashMap<>();
    protected FluentList<String> lru = new FluentArrayList<>();
    protected int limit = 5;
    
    public MemoryStorage() {
        super();
    }

    public MemoryStorage( int limit ) {
        this();
        this.limit = limit;
    }

    public boolean contains( String key ) {
        return widgets.containsKey( key );
    }
    
    public T load( String key ) {
        T widget = widgets.get( key );
        if( widget != null ) {
            return widget;
        } else {
            throw new MissingStoredWidgetException( "Widget with key '" + key + "' is not stored" );
        }
    }
    
    protected Iterable<T> all() {
        return widgets.values();
    }

    public void save( String key, T widget ) {
        if( limit > 0 ) {
            int index = lru.indexOf( key );
            if( index != 0 ) {
                if( index > 0 ) {
                    lru.remove( key );
                }
                lru.add( 0, key );
            }

            if( lru.size() > limit ) {
                for( int i = lru.size() - 1; i >= limit; i-- ) {
                    widgets.remove( lru.get( i ) );
                    lru.remove( i );
                }
            }  
        } 
        widgets.put( key, widget );
    }
    
    public void delete( String key ) {
        T widget = widgets.get( key );
        if( widget instanceof Destroyable ) {
            widget.signal( "destroy" ).fire( arg( "mandatory", true ) );
        }
        widgets.remove( key );
    }

    public void setLimit( int limit ){
        this.limit = limit;
    }
    
    public void destroy() {
        widgets.keys().inspect( k -> delete( k ) );
        widgets = null;
        lru = null;
    }
    
}