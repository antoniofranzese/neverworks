package it.neverworks.ui.html.production;

import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Reflection;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.production.Mapper;
import it.neverworks.ui.Widget;

public class HTMLPackageBasedMapper implements Mapper {
    
    protected Map<Class<?>, Class<? extends Producer>> cache = new HashMap<Class<?>, Class<? extends Producer>>();
    protected Map<Class<?>, Class<? extends Producer>> statics = new HashMap<Class<?>, Class<? extends Producer>>();
    
    public Class<? extends Producer> map( Class<?> widgetClass ) {
        if( cache.containsKey( widgetClass ) ) {
            return cache.get( widgetClass );
        
        } else if( statics.containsKey( widgetClass ) ) {
            Class producerClass = statics.get( widgetClass );
            cache.put( widgetClass, producerClass );
            return producerClass;
        } else {
            
            //TODO: spostare su configurazione esterna
            String clsName = widgetClass.getName().replaceAll( "(it\\.neverworks\\.ui)(.*)", "$1\\.html$2Producer" );
            Class candidateClass = Reflection.findClass( clsName );

            if( candidateClass != null && Producer.class.isAssignableFrom( candidateClass ) ) {
                cache.put( widgetClass, candidateClass );
                return candidateClass;

            } else if( ! widgetClass.getSuperclass().equals( Object.class ) ) {
                Class producerClass = this.map( widgetClass.getSuperclass() );
                if( producerClass != null ) {
                    cache.put( widgetClass, producerClass );
                    return producerClass;

                } else {
                    return null;
                }

            } else {
                return null;
            }
        }
        
    }
    
    public Map<Class<?>, Class<? extends Producer>> getStatics(){
        return this.statics;
    }
    
    public void setStatics( Map<Class<?>, Class<? extends Producer>> statics ){
        this.statics = statics;
    }

}