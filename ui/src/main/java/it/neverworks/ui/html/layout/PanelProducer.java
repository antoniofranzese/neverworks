package it.neverworks.ui.html.layout;

import static it.neverworks.language.*;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.model.events.Event;
import it.neverworks.model.io.State;
import it.neverworks.ui.Widget;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.types.ScrollabilityConverter;
import it.neverworks.ui.html.types.BackgroundConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ShadowConverter;
import it.neverworks.ui.html.types.CornersConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.PointConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.BoxConverter;
import it.neverworks.ui.html.production.BaseRedrawableContainerProducer;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.WidgetChangeProposal;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.dnd.AcceptanceConverter;
import it.neverworks.ui.html.dnd.DropEventConverter;

public class PanelProducer<T extends Panel> extends BaseRedrawableContainerProducer<T> {
    
    public void render( T widget ){

        boolean redraw = redrawing( widget );
        
        boolean useOrigin = true;
        if( widget.getOrigin() != null && widget.getParent() != widget.root() ) {
            useOrigin = false;
            // throw new ProductionException( "Floating Panel must be a root Form child" );
        }
        
        if( !redraw ) {
            widget.set( "production.events", list( "drop" ) );
            
            String width = SizeConverter.toCSS( this, widget.getWidth() );
            String height = SizeConverter.toCSS( this, widget.getHeight() );

            Tag start = output.dojo.start( "div")
                .attr( "type", dojoType() )
                .attr( "name", widget.getName() )
                .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
                .attr( BorderLayoutProducer.attributes( this, widget ) )
                .attr( "title", HTML.encode( widget.value( "title" ).or( "&nbsp;" ) ) )
                .attr( "origin", useOrigin ? PointConverter.toJSON( widget.getOrigin() ) : null )
                .attr( "visible", widget.getVisible() )
                .attr( "acceptance", AcceptanceConverter.toJSON( widget.getAcceptance() ) )
                .attr( "loader", widget.get( "!layout.loader" ) )
                .attr( "displaced", widget.get( "displaced" ) )
                
                .attr( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) )
                .attr( "horizontalScroll", SizeConverter.toCSS( this, widget.get( "!scroll.horizontal" ) ) )
                .attr( "verticalScroll", SizeConverter.toCSS( this, widget.get( "!scroll.vertical" ) ) )
                .attr( "shadow", ShadowConverter.renderCSS( this, widget.getShadow() ) )
                .attr( "rounding", CornersConverter.renderCSS( this, "border-radius", widget.getRounding() ) )
                
                .style( "width", width )
                .style( "height", height )
                .attr( "contentWidth", width )
                .attr( "contentHeight", height )
                
                .style( "margin", widget.getMargin() )
                .attr( "padding", BoxConverter.toCSS( widget.getPadding() ) )
                .attr( "color", widget.getColor() )
                .attr( "background", BackgroundConverter.renderCSS( this, widget.getBackground() ) )
                .attr( "border", BorderConverter.renderCSS( this, widget.getBorder() ) )
                .attr( "font", FontConverter.renderCSS( this, widget.getFont() ) )
                .attr( "flavor", dumpFlavor( widget ) );
            
            if( widget.getParent() instanceof StackLayout ) {
                start
                    .attr( "closable", widget.isClosable() )
                    .attr( "class", "page" );
            }
            
        } 
        
        if( widget.getContent() != null ) {
            produce( widget.getContent() );
        } else {
            output.text( "&nbsp;" );
        }
        
        if( !redraw ) {
            output.dojo.end();
        }
    }
    
    protected String dojoType() {
        return "works/layout/PlainPane";
    }

    protected boolean containerNeedsRedraw( T widget ) {
        return widget.model().changes().contains( "content" );
    }

    protected void analyzeWidget( T widget ) {
        // System.out.println( "Analyze " + widget.getCommonName() );
        // System.out.println( "Changes " + widget.model().changes().toList() );
        
        // Si auto propone in caso di variazione di proprieta'
        if( widget.model().changes().containsAny( getChangeableProperties() ) ) {
            analysis.propose( this, widget, widget.model().changes().retain( getChangeableProperties() ).toList() );
        }
    }
    
    private final static String[] CHANGEABLE_PROPERTIES = new String[]{ "title", "closable", "visible", "scrollability", "scroll", "scroll.vertical", "scroll.horizontal", "background" };
        
    protected String[] getSurvivingProperties() {
        return CHANGEABLE_PROPERTIES;
    }

    protected String[] getChangeableProperties() {
        return CHANGEABLE_PROPERTIES;
    }

    protected void analyzeChildren( T widget ) {
        if( widget.getContent() != null ) {
            inspect( widget.getContent() );
        }
    }

    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        //System.out.println( "Dumping " + redrawRequested + ", " + proposal.getWidget() );

       // System.out.println( "Dump " + proposal.getWidget().getCommonName() );
        if( redrawRequested ) {
            //System.out.println( proposal.getWidget().getCommonName() + " redraw" );
            result.put( "content", redraw( (Panel)proposal.getWidget() ) );
        }
        // System.out.println( "Redraw " + redrawRequested );
        // System.out.println( "Changes " + ((WidgetChangeProposal) proposal).getChanges() );
        
        T panel = (T) proposal.getWidget();
        for( String change: ((WidgetChangeProposal) proposal).getChanges() ) {
            
            ObjectNode property = dumpProperty( panel, change );
            if( property != null ) {
                result.putAll( property );
            }
        }

        return result;
    }
    
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        if( "title".equals( name ) ) {
            return json.property( "title", widget.getTitle() );

        } else if( "closable".equals( name ) ) {
            return json.property( "closable", widget.getClosable() );

        } else if( "origin".equals( name ) ) {
            return json.property( "origin", PointConverter.toJSON( widget.getOrigin() ) );

        } else if( "acceptance".equals( name ) ) {
            return json.property( "acceptance", AcceptanceConverter.toJSON( widget.getAcceptance() ) );

        } else if( "scrollable".equals( name ) ) {
            return json.property( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) );

        } else if( "scroll.vertical".equals( name ) ) {
            return json.property( "verticalScroll", SizeConverter.toCSS( this, widget.get( "scroll.vertical" ) ) );

        } else if( "scroll.horizontal".equals( name ) ) {
            return json.property( "horizontalScroll", SizeConverter.toCSS( this, widget.get( "scroll.vertical" ) ) );

        } else if( "scroll".equals( name ) ) {
            ObjectNode scroll = json.object();
            scroll.put( "verticalScroll", SizeConverter.toCSS( this, widget.get( "scroll.vertical" ) ) ); 
            scroll.put( "horizontalScroll", SizeConverter.toCSS( this, widget.get( "scroll.horizontal" ) ) ); 
            return scroll;

        } else if( "background".equals( name ) ) {
            return json.property( "background", BackgroundConverter.dumpCSS( this, widget.getBackground() ) );

        } else if( "border".equals( name ) ) {
            return json.property( "border", BorderConverter.dumpCSS( this, widget.getBorder() ) );

        } else if( "font".equals( name ) ) {
            return json.property( "font", FontConverter.dumpCSS( this, widget.getFont() ) );

        } else if( "content".equals( name ) ) {
            return null;

        } else if( "rounding".equals( name ) ) {
            return json.property( "rounding", CornersConverter.dumpCSS( this, "border-radius", widget.getRounding() ) );

        } else if( "shadow".equals( name ) ) {
            return json.property( "shadow", ShadowConverter.dumpCSS( this, widget.getShadow() ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
    public Event convertEvent( T widget, String name, State state ) {
        if( "drop".equals( name ) ) {
            return DropEventConverter.toEvent( widget, state );
        } else {
            return super.convertEvent( widget, name, state );
        }
    }
    
}