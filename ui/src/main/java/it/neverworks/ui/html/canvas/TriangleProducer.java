package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;
import it.neverworks.ui.canvas.Triangle;

public class TriangleProducer extends ShapeProducer<Triangle> {

    public ObjectNode dump( Triangle shape ) {
        ObjectNode node = super.dump( shape );

        node.put( "_", "tr" );

        setSize( node, "w", shape.get( "width" ) );
        setSize( node, "h", shape.get( "height" ) );

        return node;
    }


}