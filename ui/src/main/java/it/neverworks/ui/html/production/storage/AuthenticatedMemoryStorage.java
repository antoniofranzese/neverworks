package it.neverworks.ui.html.production.storage;

import static it.neverworks.language.*;

import javax.annotation.Resource;

import it.neverworks.lang.Strings;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.lifecycle.Destroyable;
import it.neverworks.ui.Widget;

import it.neverworks.security.context.UserInfo;
import it.neverworks.security.model.AuthenticatedUser;

public class AuthenticatedMemoryStorage<T extends Widget> extends BaseStorage<T> {
    
    private class WidgetInfo {
        public String user;
        public T widget;
        
        public WidgetInfo( String user, T widget ) {
            this.user = user;
            this.widget = widget;
        }
    }
    
    protected FluentMap<String, WidgetInfo> widgets = new FluentHashMap<String, WidgetInfo>();
    
    protected Iterable<T> all() {
        return widgets.assets().map( info -> info.widget );
    }

    public boolean contains( String key ) {
        return widgets.containsKey( key );
    }
    
    public T load( String key ) {
        if( widgets.containsKey( key ) ) {
            WidgetInfo info = widgets.get( key );
            String user = userInfo.authenticated() ? userInfo.getAuthenticatedUser().getUserID() : null;
            if( info.user == null ? user == null : info.user.equals( user ) ) {
                return info.widget;
            } else {
                //System.out.println( "Current: " + user + ", Form: " + info.user );
                throw new ProductionException( "Current user is not authorized for " + key + " form" );
            }
        } else {
            throw new ProductionException( "Widget with key '" + key + "' is not stored" );
        }
    }
    
    public void save( String key, T widget ) {
        String user = userInfo.authenticated() ? userInfo.getAuthenticatedUser().getUserID() : null;
        // System.out.println( "Storing for " + user );
        widgets.put( key, new WidgetInfo( user, widget ) );
    }
 
    public void delete( String key ) {
        WidgetInfo info = widgets.get( key );
        if( info != null ) {
            if( info.widget instanceof Destroyable ) {
                info.widget.signal( "destroy" ).fire( arg( "mandatory", true ) );
            }
            widgets.remove( key );
        }
    }
   
    @Resource 
    private UserInfo userInfo;
    
    public void setUserInfo( UserInfo userInfo ){
        this.userInfo = userInfo;
    }
}