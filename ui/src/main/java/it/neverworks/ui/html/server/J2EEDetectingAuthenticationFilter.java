package it.neverworks.ui.html.server;

import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.util.regex.Pattern;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import it.neverworks.log.Logger;

import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ApplicationEventPublisher;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Cleanable;
import it.neverworks.httpd.InjectableFilter;
import it.neverworks.security.context.UserInfo;
import it.neverworks.security.context.UserJoinEvent;
import it.neverworks.security.context.UserLeaveEvent;
import it.neverworks.security.model.AuthenticatedUser;

import it.neverworks.ui.html.server.detectors.UserDetector;
import it.neverworks.ui.html.server.detectors.JOSSOUserDetector;
import it.neverworks.ui.html.server.detectors.IISUserDetector;
import it.neverworks.ui.html.server.detectors.BasicAuthUserDetector;
import it.neverworks.ui.html.server.detectors.RequestHeaderUserDetector;

public class J2EEDetectingAuthenticationFilter extends InjectableFilter implements ApplicationEventPublisherAware {
    
    public final static String LOGOUT_URL_ATTRIBUTE = "it.neverworks.ui.html.server.LogoutURL";
	protected static final Logger logger = Logger.of( "AUTH" );

    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws ServletException, IOException {
        
        HttpServletRequest req = ((HttpServletRequest) request);
        boolean debug = logger.isDebugEnabled();
        boolean redirected = false;
        boolean logout = logoutMatch != null && logoutMatch.matcher( req.getRequestURI() ).matches();
        boolean invalidate = invalidateMatch != null && invalidateMatch.matcher( req.getRequestURI() ).matches();
        
        if( logout || invalidate ) {

            if( logger.debug() ) {
                logger.debug( logout ? "Performing logout" : "Performing invalidate" );
            }

            if( publisher != null ) {
                publisher.publishEvent( new UserLeaveEvent( this, userInfo ) );
            }

            String logoutURL = userInfo.getAttribute( LOGOUT_URL_ATTRIBUTE );

            userInfo.logout();

            if( invalidateContext ) {
                if( debug ) {
                    logger.debug( "Invalidating session on logout" );
                }
                cleanSession( req.getSession() );
            }

            if( logout && logoutURL != null ) {
                redirected = true;
                String fullPath = req.getContextPath() + logoutURL;
                
                if( req.getRequestURI().startsWith( fullPath ) || fullPath.startsWith( req.getRequestURI() ) ) {
                    logger.debug( "Serving logout URL: {}", req.getRequestURI() );
                    chain.doFilter( request, response );
                } else {
                    logger.debug( "Redirecting to logout URL: {}", fullPath );
                    ((HttpServletResponse) response).sendRedirect( fullPath );
                }
            }

        }
        
        if( invalidate ) {
            if( Strings.hasText( this.invalidateURL ) ) {
                String fullPath = req.getContextPath() + invalidateURL;
                ((HttpServletResponse) response).sendRedirect( fullPath );
            }
        
        } else if( ! redirected  ) {

            if( logger.isTraceEnabled() ) {
                logger.trace( "Remote-user: {}", req.getRemoteUser() );
                for( String name: each( String.class ).in( req.getHeaderNames() ) ) {
                    logger.trace( "Header {}: {}", name, req.getHeader( name ) );
                }
            }

            AuthenticatedUser user = null;
            UserDetector detector = null;
        
            if( debug ) {
                logger.debug( "Current authenticated user: {}", userInfo.getAuthenticatedUser() );
            }

            if( detectors != null ) {
                for( UserDetector det: detectors ) {
                    user = det.detect( (HttpServletRequest) request );
                    if( user != null ) {
                        if( debug ) {
                            logger.debug( "User detected by {}: {}", det, user );
                        }
                        detector = det;
                        break;
                    } else {
                        if( debug ) {
                            logger.debug( "{} detection failed", det, user );
                        }
                    }
                }
            }

            if( user == null ) {
                if( debug ) {
                    logger.debug( "User detection failed!" );
                }
            }

            if( userInfo.authenticated() && ( user == null || ! userInfo.getAuthenticatedUser().equals( user ) ) ) {
                if( debug ) {
                    logger.debug( "Resetting current authenticated user: {}", userInfo.getAuthenticatedUser() );
                }

                if( publisher != null ) {
                    publisher.publishEvent( new UserLeaveEvent( this, userInfo ) );
                }
            
                userInfo.logout();

                if( invalidateContext ) {
                    if( debug ) {
                        logger.debug( "Cleaning session on user change" );
                    }
                    
                    cleanSession( req.getSession() );
                }
            
            }
        
            if( user != null && ! userInfo.authenticated() ) {
                if( logger.isDebugEnabled() ) {
                    logger.debug( "Authenticating user as {} in {}", user, req.getSession().getId() );
                }

                detector.authenticate( userInfo, user );

                if( logoutURL != null ) {
                    userInfo.setAttribute( LOGOUT_URL_ATTRIBUTE, logoutURL );
                }

                if( publisher != null ) {
                    publisher.publishEvent( new UserJoinEvent( this, userInfo ) );
                }
            }

            chain.doFilter( request, response );
            
        }

    }
    
    protected void cleanSession( HttpSession session ) {
        for( String name: each( String.class ).in( session.getAttributeNames() ) ) {
            Object attr = session.getAttribute( name );

            if( attr instanceof Cleanable ) {
                ((Cleanable) attr).clean();
            } else {
                session.removeAttribute( name );
            }
        }
        
        if( logger.isDebugEnabled() ) {
            logger.debug( "Remaining session attributes: {}", each( session.getAttributeNames() ).list() );
        }
    }

    protected UserInfo userInfo;

    public void setUserInfo( UserInfo userInfo ){
        this.userInfo = userInfo;
    }
    
    protected List<UserDetector> detectors;
    
    public void setDetectors( String detectors ){
        this.detectors = new ArrayList<UserDetector>();
        
        for( String part: Strings.safe( detectors ).split( ";" ) ) {
            if( Strings.hasText( part ) ) {
                String specification = part.trim();
                UserDetector detector;

                if( specification.startsWith( "josso" ) ) {
                    detector = new JOSSOUserDetector();
                } else if( specification.startsWith( "iis" ) ) {
                    detector = new IISUserDetector();
                } else if( specification.startsWith( "basic" ) ) {
                    detector = new BasicAuthUserDetector();
                } else if( specification.startsWith( "header" ) ) {
                    detector = new RequestHeaderUserDetector();
                } else {
                    throw new IllegalArgumentException( "Unknown user detector specification: " + specification );
                }
                
                detector.init( specification );
                this.detectors.add( detector );
                
                if( logger.isDebugEnabled() ) {
                    logger.debug( "Detector for ''{}'': {}", specification, detector );
                }
            }
        }
    }
    
    protected boolean invalidateContext = true; 
    
    public void setInvalidateSession( String invalidateSession ){
        setInvalidate( invalidateSession );
    }

    public void setInvalidate( String invalidate ){
        this.invalidateContext = Boolean( invalidate );
    }
    
    protected ApplicationEventPublisher publisher;
    
    @Override
    public void setApplicationEventPublisher( ApplicationEventPublisher publisher ) {
        this.publisher = publisher;
    }
    
    private String logoutURL;
    
    public void setLogoutURL( String logoutURL ){
        this.logoutURL = logoutURL;
    }

    private String invalidateURL;
    
    public void setInvalidateURL( String invalidateURL ){
        this.invalidateURL = invalidateURL;
    }
    
    private Pattern logoutMatch = Pattern.compile( ".*logout\\/?$" );
    
    public void setLogoutMatch( String logoutMatch ){
        this.logoutMatch = Pattern.compile( logoutMatch );
    }
    
    private Pattern invalidateMatch = Pattern.compile( ".*invalidate\\/?$" );
    
    public void setInvalidateMatch( String invalidateMatch ){
        this.invalidateMatch = Pattern.compile( invalidateMatch );
    }
    
}