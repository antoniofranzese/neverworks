package it.neverworks.ui.html.media;

import java.io.IOException;
import net.sf.uadetector.UserAgentFamily;
import net.sf.uadetector.ReadableUserAgent;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.JSON;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.FileArtifact;
import it.neverworks.ui.html.data.ArtifactResolver;
import it.neverworks.ui.html.data.BaseArtifactResolver;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.media.SoundPlayer;

public class SoundPlayerProducer extends HTMLProducer<SoundPlayer>{
    
    
    public void render( SoundPlayer widget ) {
        output.dojo.start( "div" )
            .attr( "type", "works/media/SoundPlayer" )
            .attr( "name", widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "mode", dumpMode( widget ) )
            .attr( "volume", widget.getVolume() )
            .attr( "loop", widget.getLoop() )
            .attr( "source", dumpSource( widget ) );
            
        output.end();
    }
    
    protected ObjectNode dumpProperty( SoundPlayer widget, String name ) {
        if( "mode".equals( name ) ) {
            return json.property( name, dumpMode( widget ) );
        } else if( "source".equals( name ) ) {
            return json.property( name, dumpSource( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }

    protected String dumpMode( SoundPlayer widget ) {
        String mode = null;
        if( widget.getMode() != null ) {
            switch( widget.getMode() ) {
            case PLAY:
                mode = "play";
                break;
            case STOP:
                mode = "stop";
                break;
            case PAUSE:
                mode = "pause";
                break;
            }
        }
        widget.setMode( null );
        return mode;
    }
    
    protected ObjectNode dumpSource( SoundPlayer widget ) {
        if( widget.get( "!source.path" ) != null ) {
            ObjectNode source = json.object();
            source.set( "path", JSON.js.encode( resolveArtifact( widget.getSource().getPath() ) ) );

            if( Strings.hasText( widget.get( "source.type" ) ) ) {
                source.put( "type", widget.<String>get( "source.type" ) );
            }
            
            return source;
        } else {
            return null;
        }
    }

}