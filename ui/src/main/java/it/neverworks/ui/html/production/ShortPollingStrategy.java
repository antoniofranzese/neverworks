package it.neverworks.ui.html.production;

import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class ShortPollingStrategy extends BaseModel implements PollingStrategy {
    
    @Property @AutoConvert
    private int gap = 500;
    
    public int getGap(){
        return this.gap;
    }
    
    public void setGap( int gap ){
        this.gap = gap;
    }
    
    @Override
    public PollingParameters analyzePolling( Arguments arguments ) {
        return new PollingParameters().set( "gap", this.gap ).set( "cycle", 0 );
    }
}