package it.neverworks.ui.html.production;

import it.neverworks.model.Property;
import it.neverworks.ui.BaseContainerWidget;
import it.neverworks.ui.form.Form;

public class Page extends BaseContainerWidget {
    
    @Property
    private String title;
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }
}