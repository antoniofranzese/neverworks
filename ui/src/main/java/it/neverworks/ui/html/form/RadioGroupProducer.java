package it.neverworks.ui.html.form;

import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.form.RadioGroup;
import it.neverworks.ui.html.production.Tag;

public class RadioGroupProducer<T extends RadioGroup> extends SelectProducer<T> {
    
    public void render( T widget ) {
        if( widget.getSparse() ) {
            output.dojo.start( "div" ).style( "display", "none" );
        }

        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/form/RadioGroup" )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "initialValue", widget.getValue() )
            .attr( "initialDisabled", widget.getDisabled() )
            .attr( "visible", widget.getVisible() ? null : false )
            .style( "margin", widget.getMargin() )
            .style( "padding", widget.getPadding() )
            .attr( "readonly", widget.getReadonly() ? true : null )
            .attr( "hint", widget.getHint() )
            .attr( "problem", dumpProblem( widget.getProblem() ) )
            .style( dumpFlavor( widget ) );
    
        if( widget.getSparse() ) {
            start.attr( "sparse", true );

        } else {
            start
                .attr( "arrangeByRows", RadioGroup.Arrangement.ROWS == widget.getArrangement() ? true : null )
                .attr( "horizontal", RadioGroup.Direction.VERTICAL == widget.getDirection() ? false : null ) 
                .attr( "fractions", widget.getFractions() ); 

            output.start( "script" ).attr( "type", "works/props"  );
    
            output.text( dumpItems( widget ).toString() );
    
            output.end( "script" );
            
        }
    
        output.end();

        if( widget.getSparse() ) {
            output.end();
        }

    }
    
    @Override
    protected String processCaption( T widget, String caption ) {
        return ( widget.getEncoding() == RadioGroup.Encoding.HTML ? caption : HTML.encode( caption ) ).replace( "\n", "<br/>" );
    }
    
}
