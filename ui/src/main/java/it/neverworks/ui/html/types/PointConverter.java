package it.neverworks.ui.html.types;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Arguments;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Point;
import it.neverworks.ui.Widget;

public class PointConverter extends BaseConverter {
    
    public static JsonNode toJSON( Point point ) {
        if( point != null ) {
            return JSON.js.encode( new Arguments()
                .arg( "x", point.getX() )
                .arg( "y", point.getY() )
            );
        } else {
            return null;
        }
    }
    
}
