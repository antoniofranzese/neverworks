package it.neverworks.ui.html.production;

import it.neverworks.lang.Arguments;

public interface PollingStrategy {
    PollingParameters analyzePolling( Arguments arguments );
}