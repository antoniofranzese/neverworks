package it.neverworks.ui.html.production;

public class DojoOutput extends HTMLOutput {
    
    /*package*/ DojoOutput( HTMLOutput parent ) {
        this.document = parent.document;
        this.writer = parent.writer;
    }

    protected HTMLTag newTag( String name ) {
        return new DojoTag( name );
    }
    
}