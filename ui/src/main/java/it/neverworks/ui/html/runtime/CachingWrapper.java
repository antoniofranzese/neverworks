package it.neverworks.ui.html.runtime;

import it.neverworks.lang.Wrapper;
import it.neverworks.ui.runtime.Device;

public class CachingWrapper implements Wrapper {

    private Wrapper wrapper;
    private String key;

    public CachingWrapper( String key, Wrapper wrapper ) {
        this.key = key;
        this.wrapper = wrapper;
    }

    public Object asObject() {
        return Device.cache( key, wrapper );
    }

}