package it.neverworks.ui.html.grid;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.sf.uadetector.UserAgentFamily;
import net.sf.uadetector.ReadableUserAgent;

import it.neverworks.lang.Range;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Arguments;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.description.Changes;

import it.neverworks.ui.runtime.Device;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.data.IndexedDataSource;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridHeader;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.grid.SortConfiguration;
import it.neverworks.ui.grid.Cell;
import it.neverworks.ui.grid.Banner;
import it.neverworks.ui.grid.RowProcessor;
import it.neverworks.ui.grid.RowDecoration;
import it.neverworks.ui.grid.DecorationPriority;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.types.Size;

import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.WidgetChangeProposal;
import it.neverworks.ui.html.types.ScrollabilityConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ColorConverter;
import it.neverworks.ui.html.types.FontConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.BoxConverter;
import it.neverworks.ui.html.layout.BorderLayoutProducer;

public class GridProducer extends HTMLProducer<Grid> {
    
    private final static int DEFAULT_PAGE_SIZE = 50;
    private final static String ROW_ATTR = ".r";
    private final static String INTERIM_ATTR = "in";
    private final static String BEFORE_ATTR = "bf";
    private final static String AFTER_ATTR = "af";
    public final static String CLASS_ATTR = "cs";
    public final static String COLOR_ATTR = "cl";
    public final static String BACKGROUND_ATTR = "bk";
    public final static String FONT_ATTR = "ft";
    
    public void render( Grid widget ) {
        
        ObjectNode propsNode = json.createObjectNode();
        propsNode.put( "autoDeselect", widget.getAutoDeselect() );

        Tag start = output.dojo.start( "div" )
            .attr( "type", widget.getDetached() ? "works/grid/SyncManager" : "works/grid/AsyncManager" )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "visible", widget.getVisible() )
            .attr( switchEvent( widget.getClick() ) )
            .attr( switchEvent( widget.getInspect() ) )
            .style( "height", widget.getHeight() )
            .style( "width", widget.getWidth() )
            .style( "font", widget.getFont() )
            .style( "border", widget.getBorder() )
            .style( dumpFlavor( widget ) ); 
            
        if( widget.getHeight() != null && widget.getHeight().getKind() == Size.Kind.AUTO ) {
            propsNode.put( "autoHeight", true );
        } else {
            start.style( "height", widget.getHeight() );
        }

        if( widget.getRows() != null ) {
            propsNode.put( "rowDecoration", true );
        }
        
        if( widget.model().raw( "priority" ) != null ) {
            propsNode.put( "decorationPriority", JSON.encode( list()
                .cat( widget.getPriority() )
                .cat( list( DecorationPriority.CELL, DecorationPriority.ROW, DecorationPriority.COLUMN ).discard( widget.getPriority() ) )
                .map( p -> p.toString().toLowerCase() )
            ));
        }
        
        int pageSize = widget.getPageSize() != null ? widget.getPageSize() : DEFAULT_PAGE_SIZE;
        
        
        // Configuration
        ObjectNode configurationNode = json.createObjectNode();
        propsNode.put( "configuration", configurationNode );
        
        // Rows
        ObjectNode rowsNode = json.createObjectNode();
        rowsNode.put( "selector", widget.get( "!selector.visible", true ) );
        rowsNode.put( "selectorWidth", JSON.js.encode( SizeConverter.toCSS( widget.get( "!selector.width" ) ) ) );
        rowsNode.put( "selectMultiple", false );
        rowsNode.put( "selectOnClick", true );
        if( widget.getScrollable() != null ) {
            rowsNode.put( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) );
        }
        
        configurationNode.put( "rows", rowsNode );
        
        // Header
        GridHeader header = widget.getColumns();
        propsNode.put( "sortable", true );
        propsNode.put( "lockable", query( header ).by( "lock" ).eq( true ).any() );
        propsNode.put( "headerVisible", header.getVisible() );
        if( header.getColor() != null ) {
            propsNode.put( "headerColor", ColorConverter.toCSS( header.getColor() ) );
        }
        if( header.getBackground() != null ) {
            propsNode.put( "headerBackground", ColorConverter.toCSS( header.getBackground() ) );
        }
        if( header.getFont() != null ) {
            propsNode.put( "headerFont", JSON.js.encode( FontConverter.renderCSS( this, header.getFont() ) ) );
        }
        if( header.getHalign() != null ) {
            propsNode.put( "headerHalign", JSON.js.encode( dumpHeaderHalignment( header.getHalign() ) ) );
        }
        if( header.getHeight() != null ) {
            propsNode.put( "headerHeight", JSON.js.encode( arg( "height", SizeConverter.toCSS( header.getHeight() ) ) ) );
        }
        if( header.getDividers() != null ) {
            propsNode.put( "headerDividers", header.getDividers().toString().toLowerCase() );
        }

        // Body
        if( widget.getDividers() != null ) {
            propsNode.put( "bodyDividers", widget.getDividers().toString().toLowerCase() );
        }

        // Columns
        ArrayNode columnsNode = json.createArrayNode();
        for( CellInfo info: parseCells( widget ) ) {
            columnsNode.add( info.producer.dumpColumn( info.cell ) );
        }

        propsNode.put( "columns", columnsNode );

        // Displacement
        if( widget.getScroll() != null ) {
            ObjectNode displacementNode = json.createObjectNode();
            if( widget.getScroll() != null ) {
                displacementNode.put( "scroll", SizeConverter.toCSS( this, widget.getScroll() ) );
                widget.setScroll( null );
            } else {
                displacementNode.put( "scroll", widget.<Integer>get( "clientScroll" ) );
            }
            if( widget.selected() != null ) {
                if( widget.getItems() instanceof IndexedDataSource ) {
                    displacementNode.put( "selectedIndex", JSON.js.encode( widget.getSelectedIndex() ) );
                } else {
                    displacementNode.put( "selectedKey", JSON.js.encode( widget.getSelectedKey() ) );
                }
            }
            propsNode.put( "displacement", displacementNode );
        }
        
        // Data
        if( widget.getDetached() || widget.getPreload() ) {
            DataSource items = widget.getItems();
            propsNode.put( "data", dumpItems( widget, items, 0, widget.getDetached() ? items.size() : pageSize ) );
            propsNode.put( "total", items.size() );
        }
        propsNode.put( "pageSize", pageSize );
        
        // Details
        if( widget.getDetail() != null || widget.getEditor() != null ) {
            start.attr( "data-dojo-mixins", "works/grid/manager/Details,works/grid/manager/ServerDetailEngine" );
            ObjectNode detailNode = json.createObjectNode();
            detailNode.put( "expandOnClick", true );
            detailNode.put( "editor", widget.getEditor() != null );
            
            // ReadableUserAgent ua = Device.get( "userAgent" );
            // if( ua.getFamily() == UserAgentFamily.FIREFOX && ua.getVersionNumber().getMajor().equals( "3" ) ) {
            //     detailNode.put( "loader", true );
            // }
            
            configurationNode.put( "details", detailNode );
        }
        
        // Dump
        output.start( "script" ).attr( "type", "works/props"  );
        output.text( propsNode.toString() );
        output.end( "script" );
        
        if( widget.getDetail() != null || widget.getEditor() != null ) {
            output.start( "div" )
                .style( "position", "absolute" )
                .style( "visibility", "hidden" )
                // .style( "width", "3000px" )
                // .style( "height", "2000px" )
                .style( "detailDepotNode" );
                
            output.start( "div" )
                .style( "detailNode" );
                
            produce( widget.getDetail() != null ? widget.getDetail() : widget.getEditor() );
            
            output.end();
            output.end();
        }
        
        output.end();
    }
    
    private class CellInfo {
        private CellProducer producer;
        private Cell cell;
        
        public CellInfo( Cell cell, CellProducer producer ) {
            this.producer = producer;
            this.cell = cell;
        }
    }
    
    private List<CellInfo> _cells;
    
    // Recupera la cella di ogni colonna e la associa ad un produttore
    protected List<CellInfo> parseCells( Grid widget ) {
        if( this._cells == null ) {
            this._cells = new ArrayList<CellInfo>( widget.getColumns().size() );
            for( GridColumn column: widget.getColumns() ) {
                if( column.getVisible() ) {
                    this._cells.add( new CellInfo( column.getSource(), (CellProducer)factory.pick( column.getSource() ) ) );
                }
            }
        }
        return this._cells;

    }
    
    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        Grid widget = (Grid) proposal.getWidget();
        for( String property: ((WidgetChangeProposal) proposal).getChanges() ) {
            try {
                if( "items".equals( property ) ) {
                    DataSource items = widget.getCurrentItems();
                    int pageSize = widget.getPageSize() != null ? widget.getPageSize() : DEFAULT_PAGE_SIZE;
                    result.set( "items", getSlice( widget, items, 0, widget.getDetached() ? items.size() : pageSize ) );

                } else if( "selectingKey".equals( property ) ) {
                    DataSource items = widget.getCurrentItems();
                    if( items instanceof IndexedDataSource ) {
                        result.set( "selectingIndex", JSON.js.encode( ((IndexedDataSource) items).indexOf( widget.get( "selectedKey" ) ) ) );
                    } else {
                        result.set( "selectingKey", JSON.js.encode( widget.get( "selectedKey" ) ) );
                    }
                    
                } else if( "selectingIndex".equals( property ) ) {
                    result.set( "selectingIndex", JSON.js.encode( widget.get( "selectedIndex" ) ) );
                    
                } else if( "changedItems".equals( property ) ) {
                    //System.out.println( "Dumping changed" );
                    result.set( "changedItems", dumpItems( widget, widget.getChangedItems() ) );

                } else if( "eventQueue".equals( property ) ) {
                    result.set( "eventQueue", flushEvents( widget ) );

                } else if( "detail".equals( property ) ) {
                    if( widget.getDetail() != null ) {
                        result.put( "serverDetail", draw( widget.getDetail() ) );
                        result.put( "editor", false );
                    } else if( widget.getEditor() == null ) {
                        result.put( "serverDetail", (String) null );
                    }

                } else if( "editor".equals( property ) ) {
                    if( widget.getEditor() != null ) {
                        result.put( "serverDetail", draw( widget.getEditor() ) );
                        result.put( "editor", true );
                    } else if( widget.getDetail() == null ) {
                        result.put( "serverDetail", (String) null );
                    }
                
                } else if( "selectedKey".equals( property ) ) {
                    result.put( "selectedKey", widget.getSelectedKey() != null ? JSON.js.encode( String.valueOf( widget.getSelectedKey() ) ) : null );

                } else if( "columns.visible".equals( property ) ) {
                    result.put( "headerVisible", JSON.js.encode( widget.getColumns().getVisible() ) );

                } else if( "columns.color".equals( property ) ) {
                    result.put( "headerColor", ColorConverter.toCSS( widget.getColumns().getColor() ) );
                
                } else if( "columns.background".equals( property ) ) {
                    result.put( "headerBackground", ColorConverter.toCSS( widget.getColumns().getBackground() ) );
                
                } else if( "columns.font".equals( property ) ) {
                    result.put( "headerFont", JSON.js.encode( FontConverter.renderCSS( this, widget.getColumns().getFont() ) ) );
                
                } else if( "columns.halign".equals( property ) ) {
                    result.put( "headerHalign", JSON.js.encode( dumpHeaderHalignment( widget.getColumns().getHalign() ) ) );

                } else if( "columns.height".equals( property ) ) {
                    result.put( "headerHeight", JSON.js.encode( arg( "height", SizeConverter.toCSS( widget.getColumns().getHeight() ) ) ) );
                
                } else if( "columns.dividers".equals( property ) ) {
                    result.put( "headerDividers", widget.getDividers().toString().toLowerCase() );
                
                } else if( "dividers".equals( property ) ) {
                    result.put( "bodyDividers", widget.getDividers().toString().toLowerCase() );
                
                } else if( "scroll".equals( property ) ) {
                    result.put( "scroll", SizeConverter.toCSS( this, widget.getScroll() ) );
                    widget.setScroll( null );
            
                } else if( "scrollable".equals( property ) ) {
                    result.put( "scroll", ScrollabilityConverter.toValue( widget.getScrollable() ) );
                    
                } else {
                    result.set( property, JSON.js.encode( widget.get( property ) ) );
                }
            } catch( Exception ex ) {
                throw new ProductionException( "Error dumping Grid." + property + " property: " + ex.getMessage(), ex );
            }
        }
        return result;
    }
    
    public JsonNode execute( Grid widget, String command, JsonNode parameters ) {
        if( "getSlice".equals( command ) ) {
            DataSource source;
            if( parameters.has( "sort" ) ) {
                JsonNode sort = parameters.get( "sort" );
                AttributeInfo info = translateSortAttribute( widget, sort.get( "attribute" ).asText() );
                String expression = info.attribute;

                // Se l'attributo non è traducibile, si ignora la richiesta di ordinamento
                if( expression != null ) {
                    if( sort.get( "direction" ).asInt() < 0 ) {
                        expression = "-" + expression;
                    }

                    SortConfiguration sortConfig = new SortConfiguration()
                        .set( "column", info.info.cell.getColumn() )
                        .set( "descending", sort.get( "direction" ).asInt() < 0 )
                        .set( "expression", expression )
                        .set( "foreign", true );
                    
                    widget.set( "sort", sortConfig );
 
                } else {
                    if( widget.getSort() != null && widget.getSort().getForeign() ) {
                        widget.set( "sort", null );
                    }
                }

            } else {
                if( widget.getSort() != null && widget.getSort().getForeign() ) {
                    widget.set( "sort", null );
                }
            }
            
            return getSlice( widget, widget.getCurrentItems(), parameters.get( "start" ).asInt(), parameters.get( "count" ).asInt() );
            
        } else {
            throw new ProductionException( "Unknown command: " + command );
        }
    }
    
    protected JsonNode getSlice( Grid widget, DataSource source, int start, int count ) {
        ObjectNode slice = json.createObjectNode();
        if( source != null ) {
            slice.set( "data", dumpItems( widget, source, start, count ) );
            slice.put( "total", source.size() );
        } else {
            slice.set( "data", json.createArrayNode() );
            slice.put( "total", 0 );
        }
        return slice;
    }
 
    protected JsonNode dumpItems( Grid widget, DataSource source ) {
        return dumpItems( widget, source, 0, source.size() );
    }

    protected JsonNode dumpItems( Grid widget, DataSource source, int start, int count ) {
        ArrayNode dataNode = json.createArrayNode();
        List<CellInfo> cells = parseCells( widget );
        
        //boolean keyIsExpression = ExpressionEvaluator.isExpression( widget.getKey() );
        String key = widget.getKey();
        
        RowProcessor rowFormat = widget.getRows();

        for( Object item: source.slice( start, count ) ) {
            if( item == null ) {
                throw new ProductionException( "Null item dumping " + widget.getCommonName() );
            }
            
            ObjectNode itemNode = json.createObjectNode();

            // Il campo chiave deve essere comunque presente
            itemNode.put( "id", JSON.js.encode( Strings.valueOf( source.get( item, key ) ) ) );
            
            Decoration rowStyle = null;
            boolean hasInterim = false;
            if( rowFormat != null ) {
                try {
                    rowStyle = rowFormat.decorate( item );
                } catch( Exception ex ) {
                    throw new ProductionException( msg( "Error decorating row on {0.commonName}: {1,error}", widget, ex ), ex );
                }
                hasInterim = rowStyle != null && rowStyle instanceof RowDecoration && rowStyle.get( "interim" ) != null;
            }
            
            if( ! hasInterim ) {
                for( CellInfo info: cells ) {
                    info.producer.populateItem( info.cell, itemNode, source, item );
                }
            }
            
            if( rowStyle != null ) {
                ObjectNode rowNode = json.createObjectNode();

                if( rowStyle.getColor() != null ) {
                    rowNode.put( COLOR_ATTR, JSON.js.encode( ColorConverter.toCSS( rowStyle.getColor() ) ) );
                }

                if( rowStyle.getBackground() != null ) {
                    rowNode.put( BACKGROUND_ATTR, JSON.js.encode( ColorConverter.toCSS( rowStyle.getBackground().getColor() ) ) );
                }

                if( rowStyle.getFont() != null ) {
                    rowNode.put( FONT_ATTR, JSON.js.encode( FontConverter.toCSS( rowStyle.getFont() ) ) );
                }

                StyleArtifact flavor = rowStyle.getFlavor();
                if( flavor != null ) {
                    rowNode.set( CLASS_ATTR, JSON.js.encode( dumpFlavor( flavor ) ) );
                }

                if( rowStyle instanceof RowDecoration ) {
                    if( rowStyle.get( "interim" ) != null ) {
                        rowNode.put( INTERIM_ATTR, dumpBanner( rowStyle.<Banner>get( "interim" ) ) );
                    } else {
                        if( rowStyle.get( "before" ) != null ) {
                            rowNode.put( BEFORE_ATTR, dumpBanner( rowStyle.<Banner>get( "before" ) ) );
                        }
                        if( rowStyle.get( "after" ) != null ) {
                            rowNode.put( AFTER_ATTR, dumpBanner( rowStyle.<Banner>get( "after" ) ) );
                        }
                    }
                }
                
                itemNode.put( ROW_ATTR, rowNode );

            }
                
            dataNode.add( itemNode );
        }
        return dataNode;
    }
    
    protected JsonNode dumpBanner( Banner banner ) {
        return JSON.js.encode( 
            arg( "content", banner.getEncoding() == Banner.Encoding.HTML ? HTML.sanitize( banner.getContent() ) : HTML.encode( banner.getContent() ) )
            .arg( "halign", dumpBannerHalign( banner.getHalign() ) )
            .arg( "valign", dumpBannerValign( banner.getValign() ) )
            .arg( "style",
                ( banner.getFont() != null ? FontConverter.toCSS( banner.getFont() ) : "" )
                + ( banner.getColor() != null ? ( "color:" + ColorConverter.toCSS( banner.getColor() ) + ";" ) : "" )
                + ( banner.getBackground() != null ? ( "background-color:" + ColorConverter.toCSS( banner.getBackground() ) + ";" ) : "" )
                + ( banner.getHeight() != null ? ( 
                    "height:" + SizeConverter.toCSS( banner.getHeight() ) + ";"
                    + "min-height:" + SizeConverter.toCSS( banner.getHeight() ) + ";" 
                    + "max-height:" + SizeConverter.toCSS( banner.getHeight() )  + ";"
                ) : "" )
                + ( banner.getPadding() != null ? ( "padding: " + BoxConverter.toCSS( banner.getPadding() ) + ";" ) : "" )
            )
        );
    }
    
    protected String dumpBannerHalign( Banner.HorizontalAlignment halign ) {
        if( Banner.HorizontalAlignment.CENTER == halign ) {
            return "center";
        } else if( Banner.HorizontalAlignment.RIGHT == halign ) {
            return "right";
        } else {
            return "left";
        }
    }

    protected String dumpBannerValign( Banner.VerticalAlignment valign ) {
        if( Banner.VerticalAlignment.MIDDLE == valign ) {
            return "middle";
        } else if( Banner.VerticalAlignment.BOTTOM == valign ) {
            return "bottom";
        } else {
            return "top";
        }
    }
    
    protected JsonNode flushEvents( Grid widget ) {
        List<Arguments> eventQueue = widget.model().unchecked( "eventQueue" );
        ArrayNode results = json.array();
        if( eventQueue != null && eventQueue.size() > 0 ) {
            for( Arguments event: eventQueue ) {
                ObjectNode eventNode = json.object();
                eventNode.put( "name", event.<String>get( "name" ) );
                eventNode.set( "state", JSON.js.encode( event.get( "state", null ) ) );
                results.add( eventNode );
            }
            eventQueue.clear();
        }
        return results;
    }
    
    private class AttributeInfo {
        private CellInfo info;
        private String attribute;
        
        public AttributeInfo( CellInfo info, String attribute ) {
            this.info = info;
            this.attribute = attribute;
        }
    }
    
    protected AttributeInfo translateSortAttribute( Grid widget, String attribute ) {
        for( CellInfo info: parseCells( widget ) ) {
            if( info.producer.recognizeAttribute( info.cell, attribute ) ) {
                return new AttributeInfo( info, info.producer.translateSortAttribute( info.cell, attribute ) );
            }
        }
        throw new ProductionException( "Cannot locate owner cell for: " + attribute );
    }
    
    public void analyze( Grid widget ) {
        Changes changes = widget.model().changes();

        if( changes.contains( "selectedIndex" ) ) {
            changes.remove( "selectedKey" );
        }

        if( changes.contains( "rows" ) ) {
            changes.add( "items" );
            changes.remove( "rows" );
        }
        
        if( changes.contains( "items" ) ) {
            if( changes.contains( "selectedKey" ) ) {
                changes.replace( "selectedKey", "selectingKey" );
            } else if( changes.contains( "selectedIndex" ) ) {
                changes.replace( "selectedIndex", "selectingIndex" );
            }
        }

        if( changes.size() > 0 ) {
            analysis.propose( this, widget, changes.toList() );
        }

        if( !widget.model().changes().contains( "detail" ) && widget.getDetail() != null ) {
            inspect( widget.getDetail() );
        }

        if( !widget.model().changes().contains( "editor" ) && widget.getEditor() != null ) {
            inspect( widget.getEditor() );
        }
    }
    
    public Arguments dumpHeaderHalignment( GridHeader.HorizontalAlignment halign ) {
        Arguments result = new Arguments();
        if( halign != null ) {
            switch( halign ) {
            case CENTER:
                return arg( "text-align", "center" );
            case RIGHT:
                return arg( "text-align", "right" );
            default:
                return arg( "text-align", "left" );
            }
        } else {
            return arg( "text-align", null );
        }
    }
}
