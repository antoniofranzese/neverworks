package it.neverworks.ui.html.server;

import static it.neverworks.language.*;

public class DevelopmentProtocolHandler extends EmbeddedProtocolHandler {
    
    @Override
    protected boolean logging() {
        return true;
    }

    @Override
    protected void log( String message ) {
        System.out.println( message );
    }
    
}