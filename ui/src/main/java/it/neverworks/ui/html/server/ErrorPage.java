package it.neverworks.ui.html.server;

import java.io.PrintWriter;
import it.neverworks.lang.Errors;
import it.neverworks.io.StringWriter;
import it.neverworks.encoding.HTML;

public class ErrorPage {
    
    private StringWriter error;
    
    public ErrorPage( Exception ex ) {
        
        this.error = new StringWriter();
        
        error.write( "<html><body style=\"height: 100%\">" );
        error.write( "<h1 style=\"width: 100%;border-bottom:1px solid black;margin:10px 0px;padding-left:5px\">Server Error</h1>" );
        error.write( "<pre style=\"overflow:auto;white-space:pre-wrap;padding-left:10px;height:100%\">" );
        
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter( sw );
        
        Errors.unwrap( ex ).printStackTrace( pw );
        pw.flush();
        
        error.write( HTML.encode( sw.toString() ) );
        error.write( "</pre></body></html>" );
        
    }
    
    public String toString() {
        return this.error.toString();
    }
}