package it.neverworks.ui.html.server;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static it.neverworks.language.*;
import it.neverworks.lang.Application;
import it.neverworks.io.Streams;
import it.neverworks.httpd.InjectableFilter;
import it.neverworks.log.Logger;

public class J2EEStaticFileHandler extends InjectableFilter  {

    private static final Logger logger = Logger.of( J2EEStaticFileHandler.class );
    
    public void doFilter( ServletRequest request, ServletResponse response, FilterChain chain ) throws ServletException, IOException {

        String path = ((HttpServletRequest) request).getServletPath();
        InputStream stream = null;
        String source = null;

            
        try {
            if( gzip && ((HttpServletRequest) request).getHeader( "Accept-Encoding" ).indexOf( "gzip" ) >= 0 ) {
                stream = Application.resource( path + ".gz" );
            
                if( stream != null ) {
                    ((HttpServletResponse) response).addHeader( "Content-Encoding", "gzip" );
                    source = "classpath/gzip";
                }
            }
        } catch( Exception ex ) {

        }
        
        if( stream == null ) {
            stream = Application.resource( path );
            if( stream != null ) {
                source = "classpath/plain";
            }
        }
        
        ((HttpServletResponse) response).addHeader( "Cache-Control", msg( "{0},max-age={1,int}", access, ttl ) );
        ((HttpServletResponse) response).addHeader( "X-NWK-Static", "cache" );

        if( stream != null ) {
            logger.trace( "Serving {} as {}", path, source );
            Streams.copy( stream, response.getOutputStream() );
        } else {
            logger.trace( "Passing on {}", path );
            chain.doFilter( request, response );
        }

    }
    
    private int ttl = 15_552_000; // 180 days
    
    public void setTtl( Object ttl ){
        this.ttl = Int( ttl );
    }
    
    private String access = "public"; 
    
    public void setAccess( String access ){
        this.access = access;
    }
    
    private boolean gzip = true;
    
    public void setGzip( Object gzip ){
        this.gzip = Boolean( gzip );
    }
}