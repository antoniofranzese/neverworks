package it.neverworks.ui.html.production;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.lang.Objects;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.JavaScript;
import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.ui.production.ProductionException;

public class DojoTag extends HTMLTag {
    
    public DojoTag( String name ) {
        super( name );
    }
    
    protected void writeAttributes( HTMLOutput output ) {
        StringBuilder props = new StringBuilder( 128 );
        if( attributes != null ){
            for( String name: attributes.keySet() ) {
                if( name.equals( "type" ) ) {
                    output.write( " data-dojo-type=\"" + String.valueOf( attributes.get( name ) ) + "\"" );
                
                } else if( name.equals( "class" ) 
                    || name.equals( "style" )
                    || name.equals( "id" ) 
                    || name.startsWith( "data-" ) )  {

                    output.write( " " + name + "=\"" + String.valueOf( attributes.get( name ) ) + "\"" );
                            
                } else {
                    if( props.length() > 0 ) {
                        props.append( ", " );
                    }
                    props.append( name ).append( ": " ).append( toJson( attributes.get( name ) ) );
                }
            }
            
            if( props.length() > 0 ) {
                output.write( " data-dojo-props=\"" + props.toString() + "\"" );
            }
        }

        writeClasses( output );
        writeStyles( output );
        
    }
    
    protected String toJson( Object obj ) {
        if( obj instanceof List || obj instanceof Map ) {
            obj = JSON.encode( obj );
        } else if( obj instanceof JSONEncodable ) {
            obj = ((JSONEncodable) obj).toJSON();
        }
        
        if( obj instanceof String ) {
            return "'" + JavaScript.encode( (String) obj ).replace( "\\\"", "\\x22" ) + "'";
        } else if( obj instanceof Number ) {
            return String.valueOf( obj );
        } else if( obj instanceof Boolean ) {
            return String.valueOf( obj );
        } else if( obj instanceof Boolean ) {
            return String.valueOf( obj );
        } else if( obj instanceof JsonNode ) {
            return obj.toString().replace( "\\'", "\\x27" ).replace( "\\\"", "\\x22" ).replace( "\"", "'" );    
        } else {
            throw new ProductionException( "Cannot serialize to json: " + Objects.repr( obj ) );
        }
    }
}