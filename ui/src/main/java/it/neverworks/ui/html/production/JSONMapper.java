package it.neverworks.ui.html.production;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.JSON;

public class JSONMapper {
    ObjectMapper mapper;
    
    public JSONMapper() {
        this.mapper = new ObjectMapper();
    }
    
    public JSONMapper( ObjectMapper mapper ) {
        this.mapper = mapper;
    }
    
    public ObjectNode createObjectNode() {
        return mapper.createObjectNode();
    }
    
    public ArrayNode createArrayNode() {
        return mapper.createArrayNode();
    }

    public ObjectNode object() {
        return mapper.createObjectNode();
    }
    
    public ArrayNode array() {
        return mapper.createArrayNode();
    }
    
    public JsonNode encode( Object value ) {
        return JSON.js.encode( value );
    }
    
    public ObjectNode property( String name, Object value ) {
        ObjectNode node = mapper.createObjectNode();
        node.set( name, JSON.js.encode( value ) );
        return node;
    }
    
}