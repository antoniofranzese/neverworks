package it.neverworks.ui.html.server;

import javax.servlet.http.HttpServletRequest;

import it.neverworks.lang.Strings;
import it.neverworks.model.features.Retrieve;

public class J2EEHeaderProvider implements Retrieve {
    
    private HttpServletRequest request;

    public J2EEHeaderProvider( HttpServletRequest request ) {
        this.request = request;
    }

    public Object retrieveItem( Object key ) {
        return request.getHeader( Strings.valueOf( key ) );
    }

}