package it.neverworks.ui.html.production;

import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Context;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.Widget;

public class HTMLExecutableFactory implements Factory, HTMLFactory {
    
    private HTMLProducerFactory parent;
    private Context context;
    private HTMLOutput output;
    private JSONAnalysis analysis;
    private JSONMapper json;
    
    public HTMLExecutableFactory( HTMLProducerFactory parent, Context context, HTMLOutput output ) {
        this.parent = parent;
        this.context = context;
        this.output = output;
    }
    
    public HTMLExecutableFactory( HTMLProducerFactory parent, Context context, JSONAnalysis analysis ) {
        this.parent = parent;
        this.context = context;
        this.analysis = analysis;
    }

    public <T extends Widget> Producer<T> pick( T widget ) {
        HTMLProducer producer = (HTMLProducer)parent.pick( widget );
        producer.setFactory( this );
        producer.setContext( context );
        producer.setOutput( output );
        producer.setAnalysis( analysis );
        producer.setJson( analysis != null ? analysis.getJson() : new JSONMapper() );
        producer.setArtifactResolver( parent.getArtifactResolver() );
        return producer;
    }

    public HTMLExecutableProducer with( Context context, HTMLOutput output ) {
        return new HTMLExecutableProducer( this.parent, context, output );
    }

    public HTMLExecutableProducer with( Context context, JSONAnalysis analysis ) {
        return new HTMLExecutableProducer( this.parent, context, analysis );
    }
    
}