package it.neverworks.ui.html.server;

public class JOSSOAuthenticationFilter extends J2EEDetectingAuthenticationFilter {
        
    public JOSSOAuthenticationFilter() {
        super();
        setDetectors( "josso" );
    }
    
}