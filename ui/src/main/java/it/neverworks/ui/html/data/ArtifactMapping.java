package it.neverworks.ui.html.data;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class ArtifactMapping extends BaseModel {
    @Property @AutoConvert
    private Class type;
    
    @Property
    private ArtifactResolver resolver;
    
    public ArtifactResolver getResolver(){
        return this.resolver;
    }
    
    public void setResolver( ArtifactResolver resolver ){
        this.resolver = resolver;
    }
    
    public Class getType(){
        return this.type;
    }
    
    public void setType( Class type ){
        this.type = type;
    }
}