package it.neverworks.ui.html.types;

import it.neverworks.lang.Arguments;
import it.neverworks.ui.types.Font;
import it.neverworks.ui.Widget;
import it.neverworks.ui.html.production.HTMLProducer;

public class FontConverter extends BaseConverter {
    
    public static String toCSS( Object value ) {
        if( value != null ) {
            String result = toCSS( renderCSS( null, value ) );
            return result.length() > 0 ? "custom;" + result : null;
        } else {
            return null;
        }
        
    }

    public static Arguments renderCSS( HTMLProducer producer, Object value ) {

        if( value != null ) {
            Arguments result = new Arguments();
            Font font = (Font) value;
        
            if( font.getSize() != null ) {
                result.arg( "font-size", SizeConverter.toCSS( font.getSize() ) );
            }
        
            if( font.getHeight() != null ) {
                result.arg( "line-height", SizeConverter.toCSS( font.getHeight() ) );
            }
        
            if( font.getFamily() != null ) {
                result.arg( "font-family", font.getFamily() );
            } 

            if( font.getStyle() != null ) {
                if( font.getStyle() == Font.Style.ITALIC ) {
                    result.arg( "font-style", "italic" );
                }
            }

            if( font.getWeight() != null ) {
                switch( font.getWeight() ) {
                    case ULTRALIGHT:
                        result.arg( "font-weight", "100" );
                        break;
                    case THIN:
                        result.arg( "font-weight", "200" );
                        break;
                    case LIGHT:
                        result.arg( "font-weight", "lighter" ); // 300
                        break;
                    case MEDIUM:
                        result.arg( "font-weight", "500" );
                        break;
                    case DEMI:
                        result.arg( "font-weight", "600" );
                        break;
                    case BOLD:
                        result.arg( "font-weight", "bold" ); // 700
                        break;
                    case BOLDER:
                        result.arg( "font-weight", "800" );
                        break;
                    case HEAVY:
                        result.arg( "font-weight", "900" );
                        break;
                    default:
                        result.arg( "font-weight", "normal" ); // 400
                }
            }
        
            if( font.getLine() != null ) {
                switch( font.getLine() ) {
                    case UNDER:
                        result.arg( "text-decoration", "underline" );
                        break;
                    case OVER:
                        result.arg( "text-decoration", "overline" );
                        break;
                    case STRIKE:
                        result.arg( "text-decoration", "line-through" );
                        break;
                    default:
                        result.arg( "text-decoration", "none" );
                }
            }

            if( font.getLetter() != null ) {
                switch( font.getLetter() ) {
                    case UPPER:
                        result.arg( "text-transform", "uppercase" );
                        break;
                    case LOWER:
                        result.arg( "text-transform", "lowercase" );
                        break;
                    case CAPITAL:
                        result.arg( "text-transform", "capitalize" );
                        break;
                    default:
                        result.arg( "text-transform", "none" );

                }
            }

            if( font.getStretch() != null ) {
                switch( font.getStretch() ) {
                    case ULTRACONDENSED:
                        result.arg( "font-stretch", "ultra-condensed" );
                        break;
                    case EXTRACONDENSED:
                        result.arg( "font-stretch", "extra-condensed" );
                        break;
                    case CONDENSED:
                        result.arg( "font-stretch", "condensed" );
                        break;
                    case SEMICONDENSED:
                        result.arg( "font-stretch", "semi-condensed" );
                        break;
                    case SEMIEXPANDED:
                        result.arg( "font-stretch", "semi-expanded" );
                        break;
                    case EXPANDED:
                        result.arg( "font-stretch", "expanded" );
                        break;
                    case EXTRAEXPANDED:
                        result.arg( "font-stretch", "extra-expanded" );
                        break;
                    case ULTRAEXPANDED:
                        result.arg( "font-stretch", "ultra-expanded" );
                        break;
                    case NORMAL:
                        result.arg( "font-stretch", "normal" );
                }
            }            

            return result;

        } else {
            return null;
        }
        
    }

    public static Arguments dumpCSS( HTMLProducer producer, Object value ) {
        Arguments result = renderCSS( producer, value );

        if( result != null ) {
            return result;
        } else {
            return new Arguments().arg( "font", null );
        }
        
    }
    
}