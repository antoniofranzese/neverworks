package it.neverworks.ui.html.grid;

import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.Date;

import it.neverworks.lang.Dates;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.data.DataSource;
import it.neverworks.ui.grid.CanvasCell;
import it.neverworks.ui.canvas.Scene;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.html.canvas.SceneProducer;
import it.neverworks.ui.html.types.SizeConverter;

public class CanvasCellProducer extends PropertyCellProducer<CanvasCell>{

    public ObjectNode dumpColumn( CanvasCell cell ) {
        ObjectNode columnNode = super.dumpColumn( cell );

        ObjectNode decorator = json.createObjectNode();
        decorator.put( "$type", "canvas" );
        decorator.put( "width",  SizeConverter.toCSS( cell.get( "width", Size.pixels( 50 ) ) ) );
        decorator.put( "height", SizeConverter.toCSS( cell.get( "height", Size.pixels( 50 ) ) )  );
        columnNode.set( "decoratorParams", decorator );
        
        return columnNode;
    }
    
    //TODO: esclude la decorazione, valutare anche getCellValue()
    public void populateItem( CanvasCell cell, ObjectNode itemNode, DataSource source, Object item ) {
        Object value = cell.getFormattedValue( source, item );
        if( value instanceof Scene ) {
            itemNode.set( cell.getName(), SceneProducer.dump( (Scene) value ) );
        } else {
            itemNode.set( cell.getName(), JSON.encode( null ) );
        }
    }

}