package it.neverworks.ui.html.form;

import java.util.Map;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.HTML;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.StyleArtifact;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.html.types.ScrollabilityConverter;
import it.neverworks.ui.html.types.BackgroundConverter;
import it.neverworks.ui.html.types.BorderConverter;
import it.neverworks.ui.html.types.ShadowConverter;
import it.neverworks.ui.html.types.CornersConverter;
import it.neverworks.ui.html.data.ClassArtifact;
import it.neverworks.ui.html.data.HrefArtifact;
import it.neverworks.ui.html.data.CssArtifact;
import it.neverworks.ui.html.dnd.ProposalConverter;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.types.ZoomConverter;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.form.Image;

public class ImageProducer extends HTMLProducer<Image> {
    
    protected final static Map<Class, String> ARTIFACT_TYPES = dict(
        pair( ClassArtifact.class, "class" )
        ,pair( HrefArtifact.class, "file" )
        //,pair( CssArtifact.class, "style" )
    );
    
    public void render( Image widget ) {
        
        //TODO: dojo widget
        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/form/Image" )
            .attr( "name",  widget.getName() )
            .attr( "canonicalName",  development() ? widget.getCanonicalName() : null )
            .attr( "value", dumpSource( widget ) )
            .attr( "visible", widget.getVisible() ? null : false )
            .attr( switchEvent( widget.getClick() ) )
            .attr( switchEvent( widget.getInspect() ) )
            .attr( "proposal", ProposalConverter.toJSON( widget.getProposal() ) )
            .attr( "avatar", widget.get( "!avatar.path" ) )
            .attr( "maxWidth", SizeConverter.toCSS( this, widget.getMaxWidth() ) )
            .attr( "maxHeight", SizeConverter.toCSS( this, widget.getMaxHeight() ) )
            .attr( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) )
            .attr( "zoom", ZoomConverter.toValue( widget.getZoom() ) )
            .attr( "shadow", ShadowConverter.renderCSS( this, widget.getShadow() ) )
            .attr( "rounding", CornersConverter.renderCSS( this, "border-radius", widget.getRounding() ) )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() )
            .style( "padding", widget.getPadding() )
            .style( "margin", widget.getMargin() )
            .style( BorderConverter.renderCSS( this, widget.getBorder() ) )
            .style( BackgroundConverter.renderCSS( this, widget.getBackground() ) )
            .style( dumpFlavor( widget ) );
        
        output.end();
    }

    protected Object dumpSource( Image widget ) {
        Artifact artifact = widget.get( "value" );
        if( artifact != null ) {
            Object resolved = resolveArtifact( (Artifact) artifact );
            if( ARTIFACT_TYPES.containsKey( resolved.getClass() ) ) {
                return 
                    arg( "content", resolved )
                    .arg( "type", ARTIFACT_TYPES.get( resolved.getClass() ) );

            } else if( resolved instanceof RawArtifact ) {
                return 
                    arg( "content", ((RawArtifact) resolved).getContent() )
                    .arg( "type", resolved instanceof StyleArtifact ? "class" : "file" );
                
            } else {
                throw new ProductionException( "Unknown source for " + widget.getCommonName() + ": " + repr( artifact ) );
            }
        } else {
            return arg( "type", "null" );
        }
        
    }

    protected ObjectNode dumpProperty( Image widget, String name ) {
        if( "value".equals( name ) || "source".equals( name ) ) {
            return json.property( "value", dumpSource( widget ) );

        } else if( "proposal".equals( name ) ) {
            return json.property( "proposal", ProposalConverter.toJSON( widget.getProposal() ) );

        } else if( "avatar".equals( name ) ) {
            return json.property( "avatar", widget.get( "!avatar.path" ) );

        } else if( "scrollable".equals( name ) ) {
            return json.property( "scrollable", ScrollabilityConverter.toValue( widget.getScrollable() ) );

        } else if( "zoom".equals( name ) ) {
            return json.property( "zoom", ZoomConverter.toValue( widget.getZoom() ) );

        } else if( "background".equals( name ) ) {
            return json.property( "background", BackgroundConverter.dumpCSS( this, widget.getBackground() ) );

        } else if( "width".equals( name ) ) {
            return json.property( "width", SizeConverter.toCSS( this, widget.getWidth() ) );

        } else if( "height".equals( name ) ) {
            return json.property( "height", SizeConverter.toCSS( this, widget.getHeight() ) );

        } else if( "rounding".equals( name ) ) {
            return json.property( "rounding", CornersConverter.dumpCSS( this, "border-radius", widget.getRounding() ) );

        } else if( "shadow".equals( name ) ) {
            return json.property( "shadow", ShadowConverter.dumpCSS( this, widget.getShadow() ) );

        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
}