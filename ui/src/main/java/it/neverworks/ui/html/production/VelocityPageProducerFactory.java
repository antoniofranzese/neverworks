package it.neverworks.ui.html.production;

import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.runtime.RuntimeConstants;

import it.neverworks.lang.Errors;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Producer;
import it.neverworks.ui.Widget;

public class VelocityPageProducerFactory implements Factory, InitializingBean {
    
   
    private VelocityEngine engine;
    private Template parsedTemplate;
    
    public VelocityPageProducerFactory() {

    }
    
    public <T extends Widget> Producer<T> pick( T widget ) {
        VelocityPageProducer producer = new VelocityPageProducer();
        producer.setTemplate( parsedTemplate );
        producer.setContextEntries( entries );
        return (Producer<T>)producer;
    }
    
    private String template;
    
    public void setTemplate( String template ){
        this.template = template;
    }
    
    private Map<String,Object> entries;
    
    public void setEntries( Map<String,Object> entries ){
        this.entries = entries;
    }
    
    public void afterPropertiesSet() {
        engine = new VelocityEngine();
        engine.setProperty( RuntimeConstants.RESOURCE_LOADER, "classpath" ); 
        engine.setProperty( "classpath.resource.loader.class", ClasspathResourceLoader.class.getName() );
        engine.init();

        try {
            parsedTemplate = engine.getTemplate( template );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        
    }
}