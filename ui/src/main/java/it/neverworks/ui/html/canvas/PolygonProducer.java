package it.neverworks.ui.html.canvas;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import it.neverworks.encoding.JSON;
import it.neverworks.ui.types.Size;
import it.neverworks.ui.canvas.Shape;
import it.neverworks.ui.canvas.Stroke;
import it.neverworks.ui.canvas.Fill;
import it.neverworks.ui.canvas.Polygon;

public class PolygonProducer extends ShapeProducer<Polygon> {

    public ObjectNode dump( Polygon shape ) {
        ObjectNode node = super.dump( shape );
 
        node.put( "_", "py" );
        setInteger( node, "sd", shape.get( "sides" ) );
        setInteger( node, "r", shape.get( "radius" ) );

        return node;
    }


}