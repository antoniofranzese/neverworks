package it.neverworks.ui.html.web;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.encoding.URL;
import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.RawArtifact;
import it.neverworks.ui.html.data.HrefArtifact;
import it.neverworks.ui.html.server.PrivateURL;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.web.PDFViewer;
import it.neverworks.ui.html.layout.BorderLayoutProducer;

public class PDFViewerProducer extends HTMLProducer<PDFViewer> {
    
    public void render( PDFViewer widget ) {
        
        Tag start = output.dojo.start( "div" )
            .attr( "type", "works/web/PDFViewer" )
            .attr( "name",  widget.getName() ) 
            .attr( "canonicalName", development() ? widget.getCanonicalName() : null )
            .attr( "startupSource", dumpSource( widget ) )
            .attr( BorderLayoutProducer.attributes( this, widget ) )
            .style( "width", widget.getWidth() )
            .style( "height", widget.getHeight() );
            // .style( "padding", widget.getPadding() )
            // .style( "margin", widget.getMargin() );

        output.end();
    }
    
    protected ObjectNode dumpProperty( PDFViewer widget, String name ) {
        if( "source".equals( name ) ) {
            return json.property( "source", dumpSource( widget ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }

    protected String dumpSource( PDFViewer widget ) {
        Artifact artifact = widget.getSource();
        
        if( artifact != null ) {
            Object resolved = resolveArtifact( artifact );

            if( resolved instanceof HrefArtifact ) {
                PrivateURL url = new PrivateURL( Strings.valueOf( ((HrefArtifact) resolved).getContent() ) );
                url.set( "X-NWK-Name", Strings.hasText( widget.getFileName() ) ? widget.getName() : "document.pdf" );
                url.set( "X-NWK-Type", Strings.hasText( widget.getType() ) ? widget.getType() : "application/pdf" );
                url.set( "X-NWK-Disposition", "inline" );

                return url.toString();
                
            } else if( resolved instanceof RawArtifact ) {
                  return Strings.valueOf( ((RawArtifact) resolved).getContent() ) ;
                
            } else {
                throw new ProductionException( "Unknown artifact: " + Objects.repr( artifact ) );
            }
        } else {
            return null;
        }
    }
}