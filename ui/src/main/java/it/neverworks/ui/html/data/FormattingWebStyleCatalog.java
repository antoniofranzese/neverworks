package it.neverworks.ui.html.data;

import java.util.Map;
import java.text.MessageFormat;

import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.AbstractCatalog;

public class FormattingWebStyleCatalog extends AbstractCatalog {
    
    private Map<String, String> formats;
    
    public FormattingWebStyleCatalog() {
        super();
    }

    public FormattingWebStyleCatalog( Map<String, String> formats ) {
        this.formats = formats;
    }

    public Artifact get( String name ) {
        String className;
        if( name.indexOf( "@" ) > 0 ) {
            String format = name.substring( name.indexOf( "@" ) + 1 );
            String base = name.substring( 0, name.indexOf( "@" ) );
            if( formats.containsKey( format ) ) {
                className = MessageFormat.format( formats.get( format ), base );
            } else {
                throw new IllegalArgumentException( "Unknown style format: @" + format );
            }
        } else {
            className = name;
        }
        return new WebStyleArtifact( name, className );
    }
    
    public void setFormats( Map<String, String> formats ){
        this.formats = formats;
    }
    
    
}