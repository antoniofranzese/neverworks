package it.neverworks.ui.html.form;

import java.util.List;
import java.util.Collection;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import static it.neverworks.language.*;

import it.neverworks.encoding.HTML;
import it.neverworks.encoding.JSON;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.ProductionException;
import it.neverworks.ui.lifecycle.WidgetLifeCycle;
import it.neverworks.ui.lifecycle.WidgetPersistedEvent;
import it.neverworks.ui.html.types.SizeConverter;
import it.neverworks.ui.html.layout.BorderLayoutProducer;
import it.neverworks.ui.html.production.BufferedHTMLOutput;
import it.neverworks.ui.html.production.WidgetModifiedEvent;
import it.neverworks.ui.html.production.CollectorChangeProposal;
import it.neverworks.ui.html.production.ChangeProposal;
import it.neverworks.ui.html.production.HTMLProducer;
import it.neverworks.ui.html.production.HTMLFactory;
import it.neverworks.ui.html.production.Tag;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.Widget;

public class FormProducer<T extends Form> extends HTMLProducer<T> {
    
    public void render( T widget ) {
        
        boolean root = context.get( "FormProducer/Root" ).equals( widget );
        boolean redraw = redrawing( widget );
        boolean development = context.get( "Environment" ).equals( "development" );
        T destination = widget.get( "destination" );

        T form = widget;
        // Gestione swap in creazione
        while( form.get( "destination" ) != null ) {
            form = form.get( "destination" );

        }
        if( root && form != widget ) {
            context.set( "FormProducer/Root", form );
            //((WidgetLifeCycle) form).notify( new WidgetPersistedEvent( this, form ) );
        }

        if( !redraw ) {
            output.dojo.start( "div" )
                .attr( "type", "works/form/manager/FormContentPane" )
                .attr( "title", HTML.encode( form.getTitle() ) )
                .attr( "closable", form.getClosable() )
                .attr( BorderLayoutProducer.attributes( this, widget ) )
                .attr( "contentWidth", SizeConverter.toCSS( this, widget.getWidth() ) )
                .attr( "contentHeight", SizeConverter.toCSS( this, widget.getHeight() ) )
                .attr( "class", "worksFormPane" + ( root ? " main" : "" ) )
                .attr( "visible", form.getVisible() ? null : false )
                .attr( "loader", widget.get( "!layout.loader" ) );
         }
        
        Tag main = output.dojo.start( "div" )
            .attr( "type", dojoType() )
            .attr( "name", root ? "mainform" : form.getName() )
            .attr( "canonicalName", development ? form.getCanonicalName() : null )
            .attr( "title", HTML.encode( form.getTitle() ) )
            .attr( "from",  development ? form.getClass().getName() : null )
            .attr( "class", root ? "worksFormManagerMain" : "worksFormManagerInner" )
            .attr( "visible", form.getVisible() ? null : false )
            .style( "margin", form.getMargin() )
            .style( "padding", form.getPadding() )
            .style( "background-color", form.getBackground() )
            .style( "font", form.getFont() )
            .style( dumpFlavor( widget ) );

        if( form.getScrollable() ) {
            main.style( "overflow", "scroll" );
        }
        
        if( root ) {
            main.attr( "data-dojo-id", "form" )
                .attr( "endpoint", context.get( "EndpointURL" ) )
                .attr( "activeChild", form.getActiveChild() != null ? form.getActiveChild().getPath() : null );
                
            if( form.getNextTick().size() > 0 ) {
                main.attr( "startupNextTick", true );
            }
                
        }

        renderChildren( form );
        
        output.end();
        
        if( !redraw ) {
            output.end();
        }
        
    }
    
    protected String dojoType() {
        return "works/form/Manager";
    }
    
    protected void renderChildren( T form ) {
        for( Widget child: form.getChildren() ) {
            produce( child );
        }
    }
    
    protected boolean rootAnalyzer = false;
    protected boolean redrawRequested = false;
    protected boolean replaceRequested = false;
    protected List<Widget> childrenReplacements;
    
    public void analyze( Form widget ) {
        boolean root = context.get( "FormProducer/Root" ).equals( widget );
        
        analysis.beginCollector( this, widget );

        // Se e' stato richiesta una sostituzione della form
        if( widget.model().changes().contains( "destination" ) && widget.get( "destination" ) != null ) {
            //System.out.println( widget.getCommonName() + " requests swap");
            replaceRequested = true;
            
            analysis.reject();
            if( parent != null ) {
                analysis.proposeCollector( this, widget );
            }
        
        } else {

            analyzeChildren( widget );
        
            if( redrawRequested ) {
                analysis.reject();
            
                if( parent != null ) {
                    analysis.proposeCollector( this, widget );
                }
            } else {
                analysis.accept();
            }
        
        }   

        if( root ) {
            // System.out.println( "Dumping on " + widget.getCommonName() );
            ChangeProposal rootChange = analysis.getRootProposal();
            analysis.setResult( rootChange.getProducer().dump( rootChange ) );
        }    
    }
    
    public void analyzeChildren( Form widget ) {
        // Exclude new children from inspection
        Collection<String> creations = widget.get( "production.creations" );
        for( Widget child: widget.getChildren() ) {
            if( creations == null || ! creations.contains( child.getName() ) ) {
                inspect( child );
            }
        }
    }
    
    public void bubble( Event event ) {
        if( event instanceof FormChildModifiedEvent ) {
            if( childrenReplacements == null ) {
                childrenReplacements = new ArrayList<Widget>();
            }
            //System.out.println( "Replacement requested: " + event.get( "source" ) );
            childrenReplacements.add( ((FormChildModifiedEvent) event).getSource() );
        } else if( event instanceof WidgetModifiedEvent ) {
            redrawRequested = true;
        } else if( parent != null ) {
            parent.bubble( event );
        }
    }
    
    public JsonNode dump( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        // System.out.println( proposal.getWidget().getCommonName() + " dump" );

        // Sostituisce l'intera form, se richiesto
        if( replaceRequested ) {
            result.setAll( replaceForm( (Form) proposal.getWidget() ) );

        } else {
            // Ridisegna la form, se richiesto
            if( redrawRequested ) {
                result.setAll( redrawForm( (Form) proposal.getWidget() ) );
            } 
        
            // Unisce le modifiche dell'albero dei widget contenuti
            result.setAll( dumpForm( proposal ) );
            
            Form form = proposal.getWidget();
            Collection<String> creations = form.get( "production.creations" );
            Collection<String> destructions = form.get( "production.destructions" );
            //System.out.println( form.getCommonName() + " creations: " + creations );
            //System.out.println( form.getCommonName() + " destructions: " + destructions );

            // Verifica se bisogna produrre le creazioni e le cancellazioni di singoli widget
            if( !redrawRequested && ( creations != null || destructions != null || childrenReplacements != null ) ) {
                ObjectNode state;
                if( result.has( "state" )  ) {
                    state = (ObjectNode)result.get( "state" );
                } else {
                    state = json.createObjectNode();
                    result.set( "state", state );
                }
            
                // Produce il contenuto dei nuovi widget creati
                if( creations != null ) {
                    for( String name: creations ) {
                        // Distingue la semplice creazione dalla sostituzione, l'ordine delle operazioni eseguito
                        // dal client deve essere irrilevante
                        boolean create = destructions == null || !( destructions.contains( name ) );

                        ObjectNode childNode = json.createObjectNode();
                        childNode.put( create ? "$create" : "$replace", draw( form.widget( name ) ) );
                        state.set( name, childNode );
                    
                    }
                }

                // Produce un'istruzione di cancellazione per i widget cancellati e non rimpiazzati
                if( destructions != null ) {
                    for( String name: destructions ) {
                        if( creations == null || !( creations.contains( name ) ) ) {
                            ObjectNode childNode = json.createObjectNode();
                            childNode.put( "$destroy", true );
                            state.set( name, childNode );
                        }
                    }
                }
                
                // Produce un replace per i widget segnalati durante l'analisi
                if( childrenReplacements != null ) {
                    for( Widget child: childrenReplacements ) {
                        ObjectNode childNode = json.createObjectNode();
                        childNode.put( "$replace", redraw( child ) );
                        state.set( child.getName(), childNode );
                    }
                }
            }
        }
 
        return result;
    }
    
    protected ObjectNode replaceForm( Form form ) {
        
        Form destination = form.<Form>get( "destination" );
        // form.set( "destination", null );
        // Gestione di eventuali swap a cascata
        while( destination.model().changes().contains( "destination" ) && destination.get( "destination" ) != null ) {
            destination = destination.get( "destination" );
        }
        //((WidgetLifeCycle) destination).notify( new WidgetPersistedEvent( this, destination ) );
        

        // Se e' stato richiesta la sostituzione della root form, la nuova
        // prende il suo posto nello storage
        if( context.get( "FormProducer/Root" ).equals( form ) ) {
            context.set( "FormProducer/Root", destination );
        }

        if( form.getPersistent() ) {
            ((WidgetLifeCycle) form).notify( new WidgetPersistedEvent( this, form ) );
        }

        return redrawForm( destination );
    }
    
    protected ObjectNode redrawForm( Form form ) {
        // System.out.println( "Form " + proposal.getWidget() + " will redraw" );
        ObjectNode result = json.createObjectNode();
        result.put( "content", redraw( form ) );
        return result;
    }
    
    protected List<String> deliverableProperties() {
        return list( "title", "activeChild", "visible" );
    }
    
    protected ObjectNode dumpForm( ChangeProposal proposal ) {
        ObjectNode result = json.createObjectNode();
        T form = (T)proposal.getWidget();

        // Un FormProducer presume di ricevere una CollectorChangeProposal, altrimenti c'è una casistica non gestita
        // System.out.println( "On " + proposal.getWidget().getCommonName() + " " + proposal );
        if( ((CollectorChangeProposal) proposal).getProposals().size() > 0 ) {
            ObjectNode state = json.createObjectNode();
            for( ChangeProposal childChange: ((CollectorChangeProposal) proposal).getProposals() ) {
                // System.out.println( proposal.getWidget().getName() + " dumps " + childChange.getWidget().getName() );

                JsonNode childNode = childChange.getProducer().dump( childChange );
                if( childNode != null && childNode.size() > 0 ) {
                    state.set( childChange.getWidget().getName(), childNode );
                }
            }
 
            if( state.size() > 0 ) {
                result.set( "state", state );
            }
        }
        
        // produce le variazioni alle proprieta' note della form (evitando quelle definite dal programmatore)
        List<String> deliverables = deliverableProperties();
        for( String change: form.model().changes() ) {
            if( deliverables.contains( change ) ) {
                if( "activeChild".equals( change ) ) {
                    result.set( "activeChild", JSON.encode( form.getActiveChild() != null ? form.getActiveChild().getPath() : null ) );
                    //TODO: temporaneo, bisogna ricevere in nuovo active child dal runtime
                    form.setActiveChild( null );
                } else {
                    try {
                        ObjectNode propertyNodes = dumpProperty( form, change );
                        if( propertyNodes != null ) {
                            result.setAll( propertyNodes );
                        }
                    } catch( Exception ex ) {
                        String propertyName = form.model().descriptor().property( change ).getFullName();
                        throw new ProductionException( "Error dumping " + propertyName + ": " + ex.getMessage(), ex );
                    }

                    //result.set( change, JSON.encode( form.get( change ) ) );
                }
            }
        }

        // richiede il next tick alla form se necessario
        if( form.getNextTick().size() > 0 ) {
            result.put( "nextTick", true );
        }
        
        return result;
    }
    
    protected ObjectNode dumpProperty( T widget, String name ) {
        //System.out.println( "Dumping " + name );
        if( "width".equals( name ) ) {
            return json.property( "contentWidth", SizeConverter.toCSS( this, widget.get( "width" ) ) );
        } else if( "height".equals( name ) ) {
            return json.property( "contentHeight", SizeConverter.toCSS( this, widget.get( "height" ) ) );
        } else {
            return super.dumpProperty( widget, name );
        }
    }
    
}