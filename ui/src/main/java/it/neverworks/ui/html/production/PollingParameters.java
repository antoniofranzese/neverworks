package it.neverworks.ui.html.production;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class PollingParameters extends BaseModel {
    
    @Property
    private int cycle = 30000;
    
    @Property
    private int gap = 500;
    
    @Property
    private boolean enabled = true;
    
    public boolean getEnabled(){
        return this.enabled;
    }
    
    public void setEnabled( boolean enabled ){
        this.enabled = enabled;
    }
    
    public int getGap(){
        return this.gap;
    }
    
    public void setGap( int gap ){
        this.gap = gap;
    }
    
    public int getCycle(){
        return this.cycle;
    }
    
    public void setCycle( int cycle ){
        this.cycle = cycle;
    }
}