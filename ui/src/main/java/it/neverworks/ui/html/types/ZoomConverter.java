package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Zoom;
import it.neverworks.ui.Widget;

public class ZoomConverter extends BaseConverter {
    
    public static Object toValue( Object value ) {
        if( value != null ) {
            Zoom zoom = (Zoom) value;
 
            if( Zoom.Kind.FIT.equals( zoom.getKind() ) ) {
                return "fit";
            } else {
                return zoom.getValue();
            }

        } else {
            return null;
        }
        
    }
    
}