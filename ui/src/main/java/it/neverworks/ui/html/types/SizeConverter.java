package it.neverworks.ui.html.types;

import it.neverworks.ui.types.Size;
import it.neverworks.ui.Widget;
import it.neverworks.ui.html.production.HTMLProducer;

public class SizeConverter extends BaseConverter {

    public static String toCSS( Object value ) {
        return toCSS( null, value );
    }
    
    public static String toCSS( HTMLProducer producer, Object value ) {
        if( value != null ) {
            Size size = (Size) value;
 
            if( Size.Kind.PIXELS.equals( size.getKind() ) ) {
                return size.getValue() + "px";
            } else if( Size.Kind.POINTS.equals( size.getKind() ) ) {
                return size.getValue() + "pt";
            } else if( Size.Kind.EMS.equals( size.getKind() ) ) {
                return size.getValue() + "em";
            } else if( Size.Kind.WIDTH.equals( size.getKind() ) ) {
                return size.getValue() + "vw";
            } else if( Size.Kind.HEIGHT.equals( size.getKind() ) ) {
                return size.getValue() + "vh";
            } else if( Size.Kind.PERCENTAGE.equals( size.getKind() ) ) {
                return size.getValue() + "%";
            } else {
                return "auto";
            }

        } else {
            return null;
        }
        
    }
    
}