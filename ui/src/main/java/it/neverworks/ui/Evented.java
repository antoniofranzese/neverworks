package it.neverworks.ui;

import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;

public interface Evented {
    <T extends Event> Signal<T> event( String name );
    Event notify( Event event );
}