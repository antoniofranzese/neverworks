package it.neverworks.ui;

import it.neverworks.lang.Arguments;
import it.neverworks.context.Task;
import it.neverworks.model.Value;
import it.neverworks.model.features.Get;
import it.neverworks.model.features.Describe;
import it.neverworks.model.features.Language;
import it.neverworks.model.features.Probe;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.events.Subscription;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.ui.types.Attributes;
import it.neverworks.ui.util.EventGenerator;

public interface Widget extends Evented, Get, Describe, Probe, Language {

    /* Events */
    <T extends Event> Signal<T> signal( String name );
    Slot slot( String name );
    Event notify( Event event );
    Subscription on( Class<?> eventType, Dispatcher dispatcher );
    Subscription on( Class<?> eventType, String expression );
    Subscription on( String path, Dispatcher dispatcher );
    Subscription on( String path, String expression );
    Event bubble( Event event );
    Event spread( Event event );
    EventGenerator bubble( Class<? extends Event> eventType );
    EventGenerator spread( Class<? extends Event> eventType );
    EventGenerator bubble();
    EventGenerator spread();
    
    /* Model */
    <T extends AbstractWidget> T set( String name, Object value );
    <T extends AbstractWidget> T set( Arguments arguments );
    <T extends AbstractWidget> T store( String name, Object value );
    boolean has( String name );
    boolean has( String name, Class<?> type );
    <T> T resolve( String name );
    
    /* Hierarchy */
    <T extends Widget> T parent();
    <T extends Collector> T root();
    <T> T ancestor( Class<T> type );
    WidgetSelection<Widget> ancestors();
    <T> WidgetSelection<T> ancestors( Class<T> type );
    WidgetSelection<Widget> descendants();
    <T> WidgetSelection<T> descendants( Class<T> type );
    WidgetSelection<Widget> children();
    <T> WidgetSelection<T> children( Class<T> type );
    <T extends AbstractWidget> T widget( Object source );
    <T extends Container> T container( Object source );
    <T extends Collector> T collector( Object source );
    <T extends Container> T container();
    <T extends Collector> T collector();
    
    /* Common properties */
    public String getCommonName();
    public String getName();
    public Widget getParent();
    public Attributes getAttributes();
    public String getPath();
    public String getCanonicalName();

    /* Operations */
    public Widget swap( Widget destination );
    public Task spawn( String name );
    void enqueue( Dispatcher dispatcher );
    void enqueue( Dispatcher dispatcher, Arguments arguments );
    void enqueue( Dispatcher dispatcher, Event event );
    void enqueue( String name );   
    void enqueue( String name, Arguments arguments );   
    void enqueue( String name, Event event );   
}