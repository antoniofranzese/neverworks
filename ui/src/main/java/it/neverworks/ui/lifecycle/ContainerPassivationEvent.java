package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;

public class ContainerPassivationEvent extends Event {

    public ContainerPassivationEvent( Object source ) {
        super( source );
    }
    
    public ContainerPassivationEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}