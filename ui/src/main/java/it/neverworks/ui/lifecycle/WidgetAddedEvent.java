package it.neverworks.ui.lifecycle;

import it.neverworks.ui.Widget;

public class WidgetAddedEvent extends WidgetEvent {

    public WidgetAddedEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public WidgetAddedEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}