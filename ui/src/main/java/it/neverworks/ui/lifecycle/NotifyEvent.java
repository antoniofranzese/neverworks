package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.ui.Widget;

public class NotifyEvent extends Event<Widget> {
    
    protected Event event;
    
    protected EventChannel channel;
    
    public EventChannel getChannel(){
        return this.channel;
    }
    
    public NotifyEvent( Widget source, Event event, EventChannel channel ) {
        super( source );
        this.event = event;
        this.channel = channel;
    }
    
    public NotifyEvent( Widget source, Event event, EventChannel channel, String reason ) {
        super( source, reason );
        this.event = event;
        this.channel = channel;
    }
    
    public Event getEvent(){
        return this.event;
    }
        
    public Event getDispatched(){
        return this.event;
    }
        
    public boolean contains( Class<?> eventClass ) {
        return event != null ? eventClass.isAssignableFrom( event.getClass() ) : false;
    }
}