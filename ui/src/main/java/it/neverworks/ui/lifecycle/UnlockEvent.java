package it.neverworks.ui.lifecycle;

import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class UnlockEvent extends UIEvent {

    public UnlockEvent( Object source ) {
        super( source );
    }
    
    public UnlockEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}