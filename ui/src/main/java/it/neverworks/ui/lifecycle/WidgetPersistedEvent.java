package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class WidgetPersistedEvent extends WidgetEvent {

    public WidgetPersistedEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public WidgetPersistedEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}