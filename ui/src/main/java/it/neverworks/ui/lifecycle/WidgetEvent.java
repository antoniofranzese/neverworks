package it.neverworks.ui.lifecycle;

import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class WidgetEvent extends UIEvent {

    protected Widget widget;
    
    public Widget getWidget(){
        return this.widget;
    }
    
    public void setWidget( Widget widget ){
        this.widget = widget;
    }

    public WidgetEvent( Object source, Widget widget ) {
        this.source = source;
        this.widget = widget;
    }

    public WidgetEvent( Object source, Widget widget, String reason ) {
        this.source = source;
        this.widget = widget;
        this.reason = reason;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).add( "widget" ).add( "reason" ).toString();
    }
}