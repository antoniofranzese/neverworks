package it.neverworks.ui.lifecycle;

import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class WidgetLoseEvent extends WidgetEvent {

    public WidgetLoseEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public WidgetLoseEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}