package it.neverworks.ui.lifecycle;

import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class CollectorLeaveEvent extends WidgetEvent {

    public CollectorLeaveEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public CollectorLeaveEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}