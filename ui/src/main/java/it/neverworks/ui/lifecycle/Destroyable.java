package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Signal;
import it.neverworks.ui.Widget;

public interface Destroyable extends Widget {
    Signal getDestroy();
}