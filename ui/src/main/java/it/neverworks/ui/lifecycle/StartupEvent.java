package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;

public class StartupEvent extends Event {

    public StartupEvent() {
        super();
    }

    public StartupEvent( Object source ) {
        super( source );
    }
    
    public StartupEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}