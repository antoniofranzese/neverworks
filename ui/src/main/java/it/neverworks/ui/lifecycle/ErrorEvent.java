package it.neverworks.ui.lifecycle;

import it.neverworks.lang.Errors;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.ui.Widget;

public class ErrorEvent extends Event {
    
    protected Exception exception;
    
    public ErrorEvent( Object source, Exception exception, Event parent ) {
        super( source );
        this.exception = exception;
        this.parent = parent;
    }
    
    public ErrorEvent( Object source, Exception exception, Event parent, String reason ) {
        super( source, reason );
        this.exception = exception;
        this.parent = parent;
    }
    
    public Exception getException(){
        return this.exception;
    }

    public Exception getError(){
        return this.exception;
    }
    
    public void rethrow() {
        throw Errors.wrap( exception );
    }
    
}