package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.model.events.EventException;

public class UIEvent<E> extends Event<E> {

    private Boolean foreign;
    
    public UIEvent() {
        super();
    }

    public UIEvent( E source ) {
        super( source );
    }
    
    public UIEvent( E source, String reason ) {
        super( source, reason );
    }
 
    public boolean getForeign(){
        if( this.foreign == null ) {
            this.foreign = false;
        }
        return this.foreign;
    }
    
    public void setForeign( boolean foreign ){
        if( this.foreign == null ) {
            this.foreign = foreign;
        } else {
            throw new EventException( "Foreign property is not rewritable" );
        }
    }

    
}