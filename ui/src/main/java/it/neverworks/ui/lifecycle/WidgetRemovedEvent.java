package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class WidgetRemovedEvent extends WidgetEvent {

    public WidgetRemovedEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public WidgetRemovedEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}