package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class BuildContentEvent extends Event {

    public BuildContentEvent( Object source ) {
        super( source );
    }
    
    public BuildContentEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}