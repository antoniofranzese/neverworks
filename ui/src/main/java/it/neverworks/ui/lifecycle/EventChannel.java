package it.neverworks.ui.lifecycle;

public enum EventChannel {
    SPREAD, BUBBLE, DIRECT
}
