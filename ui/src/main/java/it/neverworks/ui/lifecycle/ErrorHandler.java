package it.neverworks.ui.lifecycle;

public interface ErrorHandler {
    public void notifyError( ErrorEvent event );
}