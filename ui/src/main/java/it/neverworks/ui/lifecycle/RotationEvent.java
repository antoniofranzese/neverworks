package it.neverworks.ui.lifecycle;

import it.neverworks.model.Property;
import it.neverworks.ui.runtime.Orientation;

public class RotationEvent extends UIEvent {
    
    @Property
    private Orientation orientation;
    
    @Property
    private Integer rotation;
    
    public Integer getRotation(){
        return this.rotation;
    }
    
    public void setRotation( Integer rotation ){
        this.rotation = rotation;
    }
    
    public Orientation getOrientation(){
        return this.orientation;
    }
    
    public void setOrientation( Orientation orientation ){
        this.orientation = orientation;
    }
}