package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;

public class WidgetAbandonedEvent extends WidgetEvent {

    public WidgetAbandonedEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public WidgetAbandonedEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}