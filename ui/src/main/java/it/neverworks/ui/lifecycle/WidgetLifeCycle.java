package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.model.io.State;

public interface WidgetLifeCycle {
    Event bubble( Event event );
    void bubbleIn( Event event );
    Event spread( Event event );
    void spreadIn( Event event );
    Event notify( Event event );
    void load( State state );
}