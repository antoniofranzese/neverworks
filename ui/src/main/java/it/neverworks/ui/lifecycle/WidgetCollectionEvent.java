package it.neverworks.ui.lifecycle;

import java.util.List;
import java.util.ArrayList;
import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class WidgetCollectionEvent extends UIEvent {

    private List<Widget> widgets = new ArrayList<Widget>();
    private Class<?> type = null;

    public WidgetCollectionEvent( Object source ) {
        super( source );
    }

    public WidgetCollectionEvent( Object source, Class<?> type ) {
        super( source );
        this.type = type;
    }
    
    public WidgetCollectionEvent( Object source, String reason ) {
        super( source, reason );
    }
    
    public List<Widget> getWidgets(){
        return this.widgets;
    }
    
    public void add( Widget widget ) {
        if( type == null || type.isInstance( widget ) ) {
            widgets.add( widget );            
        }
    }
    
}