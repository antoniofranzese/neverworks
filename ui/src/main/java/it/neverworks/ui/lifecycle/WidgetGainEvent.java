package it.neverworks.ui.lifecycle;

import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class WidgetGainEvent extends WidgetEvent {

    public WidgetGainEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public WidgetGainEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}