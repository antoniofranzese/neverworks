package it.neverworks.ui.lifecycle;

public class DestroyEvent extends UIEvent {
    
    protected boolean mandatory;
    
    public boolean getMandatory(){
        return this.mandatory;
    }

    public boolean isMandatory(){
        return this.mandatory;
    }
    
    public void setMandatory( boolean mandatory ){
        this.mandatory = mandatory;
    }

}