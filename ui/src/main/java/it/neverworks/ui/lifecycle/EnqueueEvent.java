package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.ui.Widget;

public class EnqueueEvent extends Event {

    private Dispatcher dispatcher;
    private Event event;
    
    public EnqueueEvent( Event event ) {
        this( null, event );
    }
    
    public EnqueueEvent( Dispatcher dispatcher ) {
        this( dispatcher, null );
    }

    public EnqueueEvent( Dispatcher dispatcher, Event event ) {
        super();
        this.dispatcher = dispatcher;
        this.event = event;
    }
    
    public Dispatcher getDispatcher(){
        return this.dispatcher;
    }

    public Event getEvent(){
        return this.event;
    }
    
}