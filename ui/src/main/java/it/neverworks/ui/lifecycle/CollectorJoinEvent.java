package it.neverworks.ui.lifecycle;

import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class CollectorJoinEvent extends WidgetEvent {

    public CollectorJoinEvent( Object source, Widget widget ) {
        super( source, widget );
    }
    
    public CollectorJoinEvent( Object source, Widget widget, String reason ) {
        super( source, widget, reason );
    }
    
}