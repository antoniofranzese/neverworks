package it.neverworks.ui.lifecycle;

import it.neverworks.ui.lifecycle.WidgetEvent;
import it.neverworks.ui.Widget;

public class LockEvent extends UIEvent {

    public LockEvent( Object source ) {
        super( source );
    }
    
    public LockEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}