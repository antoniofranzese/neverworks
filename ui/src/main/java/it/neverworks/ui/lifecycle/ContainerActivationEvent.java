package it.neverworks.ui.lifecycle;

import it.neverworks.model.events.Event;

public class ContainerActivationEvent extends Event {

    public ContainerActivationEvent( Object source ) {
        super( source );
    }
    
    public ContainerActivationEvent( Object source, String reason ) {
        super( source, reason );
    }
    
}