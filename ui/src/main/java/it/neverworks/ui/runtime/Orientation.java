package it.neverworks.ui.runtime;

import it.neverworks.model.converters.AutoConvert;

@AutoConvert
public enum Orientation {
    LANDSCAPE, PORTRAIT
}