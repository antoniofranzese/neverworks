package it.neverworks.ui.runtime;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Threads;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Functional;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.Modifiers;
import it.neverworks.model.Value;

public class Device {
    
    protected final static Modifiers READ_MODIFIERS = new Modifiers().withNulls().withOptional();
    protected final static Modifiers REGISTER_MODIFIERS = new Modifiers().withNulls().withRaw();
    protected static List<Runnable> modules = new ArrayList<Runnable>();

    private static class Storage extends ExpandoModel {

        private boolean populated = false;
        private Map<String,Object> cache;
    
    }
    
    private static final ThreadLocal<Storage> storage = new ThreadLocal<Storage> () {
        @Override protected Storage initialValue() {
            return new Storage();
        }
    };
    
    static {
        Threads.registerCleaner( th -> { 
            storage.remove(); 
        });
    }
    
    public final static Device INSTANCE = new Device();
    
    public static void init() {
        storage.set( new Storage() );
    }

    protected static void populate() {
        storage.get().populated = true;
        for( Runnable module: modules ) {
            module.run();
        }
    }
    
    public static <T> T get( String expression ) {
        if( ! storage.get().populated ) {
            populate();
        }
        return (T) ExpressionEvaluator.evaluate( storage.get(), expression, READ_MODIFIERS );
    }

    public static <T> T fetch( String expression ) {
        return (T) storage.get().get( expression );
    }

    public static <T> T cache( String key, Wrapper initializer ) {
        Map<String,Object> cache = storage.get().cache;

        if( cache == null ) {
            cache = new HashMap<String,Object>();
            storage.get().cache = cache;
        }

        if( cache.containsKey( key ) ) {
            return (T) cache.get( key );
        } else {
            T value = (T) initializer.asObject();
            cache.put( key, value );
            return value;
        }
    }

    public static boolean has( String expression ) {
        if( ! storage.get().populated ) {
            populate();
        }
        return ExpressionEvaluator.knows( storage.get(), expression );
    }

    public static boolean not( String expression ) {
        return ! is( expression );
    }

    public static boolean is( String expression ) {
        Object v = get( expression );
        return v instanceof Boolean ? Boolean.TRUE.equals( v ) : v != null;
    }

    public static boolean is( String... expressions ) {
        for( String expression: expressions ) {
            if( ! is( expression ) ) {
                return false;
            }
        }
        return true;
    }
    
    public static Device set( String expression, Object value ) {
        ExpressionEvaluator.valorize( storage.get(), expression, value );
        return INSTANCE;
    }

    public static Device register( String expression, Object value ) {
        ExpressionEvaluator.valorize( storage.get(), expression, value, REGISTER_MODIFIERS );
        //storage.get().put( expression, value );
        return INSTANCE;
    }

    public static Value value( String expression ) {
        return Value.of( get( expression ) );
    }
    
    public static <T> ObjectQuery<T> query( String expression ) {
        return new ObjectQuery<T>( Collections.<T>toIterable( get( expression ) ) );
    }
    
    public static <T> Functional<T> each( String expression ) {
        return Collections.<T>functional( get( expression ) );
    }

    public static class FunctionalBuilder<T> extends Collections.FunctionalBuilder<T> {

        public FunctionalBuilder( Class<T> target ) {
            super( target );
        }
        
        public Functional<T> in( String expression ) {
            return build( Device.get( expression ) );
        }

    }

    public static <T> FunctionalBuilder<T> each( Class<T> target ) {
        return new FunctionalBuilder<T>( target );
    }
    
    public static Device registerModule( Runnable module ) {
        modules.add( module );
        return INSTANCE;
    }
}