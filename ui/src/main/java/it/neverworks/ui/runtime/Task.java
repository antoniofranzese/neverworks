package it.neverworks.ui.runtime;

import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Signal;

public abstract class Task extends it.neverworks.context.Task {
    
    public Slot slot( String name ) {
        return new Slot( this, name );
    }
    
    public Signal signal( String name ) {
        return this.<Signal>get( name );
    }

    public <T extends it.neverworks.context.Task> T start() {
        this.starting();
        super.start();
        this.started();
        return (T) this;
    }
    
    public void starting() {
        
    }
    
    public void started() {
        
    }
    
}

