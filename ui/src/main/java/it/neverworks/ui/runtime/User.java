package it.neverworks.ui.runtime;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Inherited;

@Inherited
@Target({ ElementType.TYPE, ElementType.FIELD })
@Retention( RetentionPolicy.RUNTIME )
public @interface User {
    String[] is() default {};
    String[] can() default {};
}
