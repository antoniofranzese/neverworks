package it.neverworks.maven;

import static it.neverworks.language.*;

import java.io.File;
import java.util.Collection;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.artifact.Artifact;

import javassist.ClassPool;
import it.neverworks.bytecode.Instrumentation;
import it.neverworks.bytecode.WeaverContext;

public abstract class AbstractBytecodePlugin extends AbstractMojo {
    
    @Parameter( defaultValue = "${project}", readonly = true, required = true )
    protected MavenProject project;

    @Parameter
    protected boolean verbose = false;

    @Parameter
    protected String[] packages;

    protected abstract File classesDirectory();
    protected abstract String folder();

    @Override
    public void execute() throws MojoExecutionException {

        try {
            
            File workingFolder = new File( project.getBuild().getDirectory() + File.separator + folder() );
            getLog().info( "Working into " + workingFolder.getCanonicalPath() );

            if( workingFolder.exists() ) {
                if( workingFolder.isDirectory() ) {
                    getLog().info( "Cleaning existing working folder" );
                    FileUtils.deleteDirectory( workingFolder );
                } else {
                    getLog().warn( "Existing working path is a file - deleting..." );
                    FileUtils.deleteQuietly( workingFolder );
                }
            }
            
            workingFolder.mkdirs();

            ClassPool pool = ClassPool.getDefault();

            String classesPath = classesDirectory().getCanonicalPath();
            pool.appendClassPath( classesPath );

            for( Artifact artifact: project.getArtifacts() ) {
                pool.appendClassPath( artifact.getFile().getCanonicalPath() );
            }

            WeaverContext.setPool( pool );
            WeaverContext.setTargetClassFolder( workingFolder.getCanonicalPath() );
            WeaverContext.setLogger( new MavenLogger( getLog() ).withVerbose( this.verbose ) );

            Instrumentation instrumentation = new Instrumentation().scanWeavers();
            instrumentation.instrument( each( packages )
                .map( pkg -> pkg + "@" + classesPath )
                .array( String.class ) );

            Collection<File> files = FileUtils.listFiles( workingFolder, FileFilterUtils.trueFileFilter(), FileFilterUtils.trueFileFilter() );
            
            if( files.size() > 0 ) {
                getLog().info( "Copying " + files.size() + " instrumented classes" );
                FileUtils.copyDirectory( workingFolder, classesDirectory(), /* preserveFileDate */ true );
            } else {
                getLog().info( "Nothing to instrument - all classes are up to date" );
            }

        } catch( Exception ex ) {
            ex.printStackTrace();
            throw new MojoExecutionException( "Error instrumenting classes", ex );
        }
    }

}