package it.neverworks.maven;

import static it.neverworks.language.*;

import java.io.File;
import java.util.Collection;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.artifact.Artifact;

import javassist.ClassPool;
import it.neverworks.bytecode.Instrumentation;
import it.neverworks.bytecode.WeaverContext;

@Mojo(  
    name = "bytecode", 
    defaultPhase = LifecyclePhase.PROCESS_CLASSES, 
    requiresDependencyResolution = ResolutionScope.COMPILE 
)
public class BytecodePlugin extends AbstractBytecodePlugin {
    
    @Parameter( property = "project.build.outputDirectory", required = true )
    protected File classesDirectory;

    protected File classesDirectory() {
        return classesDirectory;
    }

    protected String folder() {
        return "instrumented-classes";
    }

}