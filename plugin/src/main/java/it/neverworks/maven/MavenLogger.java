package it.neverworks.maven;

import it.neverworks.log.Logger;
import org.apache.maven.plugin.logging.Log;

public class MavenLogger extends Logger {
    
    protected Log log;
    protected boolean verbose = false;

    public MavenLogger( Log log ) {
        this.log = log;
    }

    public MavenLogger withVerbose( boolean verbose ){
        this.verbose = verbose;
        return this;
    }
    
    public boolean fatal() {
        return this.error();
    }
    
    public void logFatal( String message ) {
        this.error( message );
    }

    public void logFatal( Throwable throwable) {
        this.error( throwable );
    }
    
    public boolean error() {
        return this.log.isErrorEnabled();
    }
    
    public void logError( String message ) {
        if( this.error() ) this.log.error( message );
    }

    public void logError( Throwable throwable ) {
        if( this.error() ) this.log.error( throwable );
    }
    
    public boolean warn() {
        return this.log.isWarnEnabled();
    }

    public void logWarn( String message ) {
        this.log.warn( message );
    }
    
    public void logWarn( Throwable throwable ) {
        this.log.warn( throwable );
    }
    
    public boolean info() {
        return this.log.isInfoEnabled();
    }
    
    public void logInfo( String message ) {
        this.log.info( message );
    }

    public void logInfo( Throwable throwable ) {
        this.log.info( throwable );
    }

    public boolean debug() {
        return this.verbose || this.log.isDebugEnabled();
    }
    
    public void logDebug( String message ) {
        if( this.log.isDebugEnabled() ) {
            this.log.debug( message );
        } else if( this.verbose ) {
            this.log.info( message );
        }
    }
    
    public void logDebug( Throwable throwable ) {
        if( this.log.isDebugEnabled() ) {
            this.log.debug( throwable );
        } else if( this.verbose ) {
            this.log.info( throwable );
        }
    }
    
    public boolean trace() {
        return this.debug();
    }
    
    public void logTrace( String message ) {
        this.debug( message );
    }
    
    public void logTrace( Throwable throwable ) {
        this.debug( throwable );
    }
    
}