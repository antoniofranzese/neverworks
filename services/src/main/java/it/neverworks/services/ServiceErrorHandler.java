package it.neverworks.services;

public interface ServiceErrorHandler {
	void error( ServiceEvent event );
}
