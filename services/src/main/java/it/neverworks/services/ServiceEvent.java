package it.neverworks.services;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.model.events.Event;
import it.neverworks.model.Property;

public class ServiceEvent extends Event {
    
    @Property
    protected Object request;
    
    @Property
    protected Map metadata;
    
    @Property
    protected Exception error;
    
    @Property
    protected Arguments transport;

    @Property
    protected String action;
    
    public String getAction(){
        return this.action;
    }
    
    public void setAction( String action ){
        this.action = action;
    }
    
    public Arguments getTransport(){
        return this.transport;
    }
    
    public void setTransport( Arguments transport ){
        this.transport = transport;
    }
    
    public Exception getError(){
        return this.error;
    }
    
    public void setError( Exception error ){
        this.error = error;
    }
    
    public Map getMetadata(){
        return this.metadata;
    }
    
    public void setMetadata( Map metadata ){
        this.metadata = metadata;
    }
    
    public Object getRequest(){
        return this.request;
    }
    
    public void setRequest( Object request ){
        this.request = request;
    }

}