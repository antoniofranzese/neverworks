package it.neverworks.services;

import it.neverworks.aop.Invocation;
import it.neverworks.model.features.Get;

public class ServiceInvocation extends Invocation implements Get {
    
    public ServiceInvocation( Object instance ) {
        super( instance );
    }

    protected ServiceInvocation() {
        super();
    }

    public ServiceEvent event() {
        return this.<ServiceEvent>argument( 0 );
    }

    public <T> T get( String path ) {
        return event().get( path );
    }

    public ServiceInvocation set( String path, Object value ) {
        event().set( path, value );
        return this;
    }

    @Override
    public Object hatch() {
        return new ServiceInvocation();
    }
}