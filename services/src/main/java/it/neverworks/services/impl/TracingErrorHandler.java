package it.neverworks.services.impl;

import static it.neverworks.language.*;

import java.util.List;

import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.io.Export;
import it.neverworks.model.io.JSONModel;
import it.neverworks.services.ServiceEvent;
import it.neverworks.services.ServiceErrorHandler;

public class TracingErrorHandler implements ServiceErrorHandler {

    private boolean trace = false;
    private boolean detail = false; 
    
	public void error( ServiceEvent event ) {
        Error result = new Error() 
            .set( "type", event.get( "error.class.name" ) )
            .set( "message", event.get( "error.message" ) );
        
        if( this.trace || this.detail ) {

            if( this.detail ) {
                result.set( "trace", event.each( StackTraceElement.class ).in( "error.stackTrace" ).map( el ->{
                    return new TraceLine()
                        .set( "method", el.getMethodName() )
                        .set( "line", el.getLineNumber() )
                        .set( "className", el.getClassName() );
                }));
            } else {
                result.set( "trace", event.each( StackTraceElement.class ).in( "error.stackTrace" ).map( el -> el.toString() ) );
            }
            
        }    
	    
        event.reply( arg( "error", result ) );
	}

    public void setTrace( boolean trace ){
        this.trace = trace;
    }

    public void setDetail( boolean detail ){
        this.detail = detail;
    }

    public class Error implements Model, JSONModel {
        
        @Property
        private String type;
        
        @Property
        private String message;
        
        @Property
        private List trace;
        
    }
    
    public class TraceLine implements Model, JSONModel {

        @Property
        private String method;
        
        @Property @Export( "class" )
        private String className;
        
        @Property
        private Integer line;
        
    }
    
}
