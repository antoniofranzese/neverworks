package it.neverworks.services.impl;

import static it.neverworks.language.*;

import java.lang.reflect.Method;

import it.neverworks.lang.Strings;
import it.neverworks.services.Service;
import it.neverworks.services.ServiceEvent;

public class PayloadActionMappingService extends AbstractActionMappingService {

    protected String expression = "action";

    protected String getAction( ServiceEvent event ) {
        if( Strings.hasText( this.expression ) ) {
            return Strings.valueOf( eval( event.get( "request" ), this.expression ) ).trim();
        } else {
            return null;
        }
    }
    
    public String getExpression(){
        return this.expression;
    }
    
    public void setExpression( String expression ){
        this.expression = expression;
    }
    
}