package it.neverworks.services.impl;

import static it.neverworks.language.*;

import java.lang.reflect.Method;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.services.Service;
import it.neverworks.services.ServiceEvent;

public abstract class AbstractActionMappingService implements Service {

    protected FluentMap<String,Method> actionMethods = new FluentHashMap<>();
    
    public void service( ServiceEvent event ) {
        boolean serviced = false;

        String action = getAction( event );
        
        if( Strings.hasText( action ) ) {

            within( actionMethods.missing( action ), map -> {
                map.put( action, Reflection.findMethodWithHierarchy( this, action, ServiceEvent.class ) );
            });
            
            Method method = actionMethods.get( action );
            if( method != null ) {
                serviced = true;
                Reflection.invokeMethod( this, method, event );
            }
        }
 
        if( ! serviced ) {
            unknown( event );
        }

    }
    
    protected abstract String getAction( ServiceEvent event );
    
    public void unknown( ServiceEvent event ) {
        throw new UnsupportedFeatureException( msg( "Unsupported service action: {0}", getAction( event ) ) );
    }

}