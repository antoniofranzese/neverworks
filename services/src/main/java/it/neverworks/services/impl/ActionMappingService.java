package it.neverworks.services.impl;

import it.neverworks.lang.Strings;
import it.neverworks.services.ServiceEvent;
import it.neverworks.services.ServiceException;

public class ActionMappingService extends AbstractActionMappingService {
    
    protected String getAction( ServiceEvent event ) {
        String action = event.getAction();
        if( Strings.hasText( action ) ) {
            return action;
        } else {
            throw new ServiceException( Strings.message( "Missing action invoking {0.class.name} service", this ) );
        }
    }

}