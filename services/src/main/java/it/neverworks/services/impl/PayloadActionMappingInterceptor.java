package it.neverworks.services.impl;

import static it.neverworks.language.*;

import java.lang.reflect.Method;

import it.neverworks.services.ServiceInvocation;
import it.neverworks.services.ServiceInterceptor;
import it.neverworks.services.ServiceEvent;
import it.neverworks.log.Logger;

public class PayloadActionMappingInterceptor implements ServiceInterceptor {

    @Logger.ForMe private static Logger logger;

    protected String expression = "action";

    public void invoke( ServiceInvocation invocation ) {
        if( ! empty( this.expression ) && empty( invocation.get( "action" ) ) ) {
            String action = str( eval( invocation.get( "request" ), this.expression ) ).trim();

            logger.debug( "Setting action to: {0}", action );
            invocation.set( "action", action );

        } else {

            if( logger.debug() ) {
                if( empty( this.expression ) ) {
                    logger.debug( "Skipping, empty expression" );
                } else if( ! empty( invocation.get( "action" ) ) ) {
                    logger.debug( "Skipping, action is filled ({0.action})", invocation );
                }
            }

        }
        invocation.invoke();
    }

    public String getExpression() {
        return this.expression;
    }
    
    public void setExpression( String expression ){
        this.expression = expression;
    }
    
}