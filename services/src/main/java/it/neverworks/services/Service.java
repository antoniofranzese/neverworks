package it.neverworks.services;

public interface Service {
    void service( ServiceEvent request ) throws Exception;
}