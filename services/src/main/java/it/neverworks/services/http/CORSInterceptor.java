package it.neverworks.services.http;

import static it.neverworks.language.*;

import it.neverworks.httpd.Method;
import it.neverworks.httpd.Request;
import it.neverworks.httpd.Response;
import it.neverworks.httpd.PathMatchingInterceptor;

public class CORSInterceptor extends PathMatchingInterceptor {
    
    protected String methods = "GET,POST,OPTIONS";
    protected String headers = "Content-Type";
    protected String origin = "*";

    @Override
    public void handle( Request request ) throws Exception {
        
        
        if( Method.OPTIONS.equals( request.getMethod() ) ) {

            if( ! empty( this.methods ) ) {
                request.getResponse().setHeader( "Access-Control-Allow-Methods", this.methods );
            }
            if( ! empty( this.headers ) ) {
                request.getResponse().setHeader( "Access-Control-Allow-Headers", this.headers );
            }

        }

        if( ! empty( this.origin ) ) {
            request.getResponse().setHeader( "Access-Control-Allow-Origin", this.origin );
        }

        dispatch( request );
    }

    public String getMethods(){
        return this.methods;
    }
    
    public void setMethods( String methods ){
        this.methods = methods;
    }

    public String getOrigin(){
        return this.origin;
    }
    
    public void setOrigin( String origin ){
        this.origin = origin;
    }

    public String getHeaders(){
        return this.headers;
    }
    
    public void setHeaders( String headers ){
        this.headers = headers;
    }
}