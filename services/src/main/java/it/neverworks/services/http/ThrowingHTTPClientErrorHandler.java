package it.neverworks.services.http;

public class ThrowingHTTPClientErrorHandler implements HTTPClientErrorHandler {

	@Override
	public String handleError( int status, String response ) {
        throw new RuntimeException( "Error " + status );
	}

}
