package it.neverworks.services.http;

public interface HTTPClientErrorHandler {
	String handleError( int status, String response );
}
