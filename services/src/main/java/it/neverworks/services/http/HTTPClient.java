package it.neverworks.services.http;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.util.EntityUtils;
import org.apache.http.Header;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.log.Logger;
import it.neverworks.io.Streams;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Synthesize;
import it.neverworks.model.collections.Collection;
import it.neverworks.services.ServiceClient;

public class HTTPClient implements Model, ServiceClient {

    @Logger.ForMe
	protected static Logger logger;
    
    @Property @Synthesize
	protected String url;

    @Property @Synthesize
    protected String mimeType = "text/plain";

    @Property @Synthesize
    protected String encoding = "UTF-8"; 

    @Property @Synthesize
    protected boolean gzip = false;

    @Property @Synthesize
    protected HTTPClientErrorHandler errorHandler;
    
    @Property @Synthesize @Collection
	protected Map<String, String> headers;
    
	public <T> T invoke( Object request ) {
		if( this.url == null ) throw new IllegalStateException( "Missing URL" );
		return invoke( this.url, request );
	}
	
	public <T> T invoke( String url, Object message ) {
		
        HttpClientBuilder builder = HttpClients.custom();
        if( ! gzip ) {
            builder.disableContentCompression();
        }
        
        CloseableHttpClient client = builder.build();

		try {

            HttpRequestBase request;
            if( message != null ) {
                request = new HttpPost( url );
            
                StringEntity payload = new StringEntity( encode( message ), ContentType.create( this.mimeType, this.encoding ) );
                ((HttpPost) request).setEntity( payload );

            } else {
                request = new HttpGet( url );
            }
            
            if( this.headers != null ) {
                for( String key: this.headers.keySet() ) {
                    request.addHeader( key, this.headers.get( key ) );
                }
            }
            
            request.addHeader( "Connection", "close" );

            CloseableHttpResponse response = client.execute( request );

            try {
                int status = response.getStatusLine().getStatusCode();
                
                String body = EntityUtils.toString( response.getEntity() );
                EntityUtils.consume( response.getEntity() );
                
    			if( status != 200 ) {
    				return (T) error( ( this.errorHandler != null ? this.errorHandler : new ThrowingHTTPClientErrorHandler() ).handleError( status, body ) );
    			} else {
    				return (T) decode( body );
    			}
                
            } finally {
                response.close();
            }
            

		} catch( Exception ex ) {
			throw Errors.wrap( ex );
            
		} finally {
            if( client != null ){
                try {
                    client.close();
                } catch( Exception ex ) {
                    throw Errors.wrap( ex );
                }
            }
		}		
	}
    
    protected String encode( Object value ) {
        return Strings.valueOf( value );
    }
    
    protected Object decode( String value ) {
        return value;
    }

    protected Object error( String value ) {
        return decode( value );
    }
	
    public void setUrl( String url ){
        this.url = url;
    }
    
    public void setMimeType( String mimeType ){
        this.mimeType = mimeType;
    }
    
    public void setEncoding( String encoding ){
        this.encoding = encoding;
    }
    
    public void setGzip( boolean gzip ){
        this.gzip = gzip;
    }
    
    public void setErrorHandler( HTTPClientErrorHandler errorHandler ){
        this.errorHandler = errorHandler;
    }
    
    public void setHeaders( Map<String,String> headers ){
        this.headers = headers;
    }
    
}
