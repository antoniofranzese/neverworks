package it.neverworks.services.http;

import static it.neverworks.language.*;

import java.util.List;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.encoding.JSON;
import it.neverworks.httpd.Request;
import it.neverworks.httpd.Method;
import it.neverworks.httpd.PathMatchingHandler;
import it.neverworks.aop.Pointcut;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.MethodPointcut;
import it.neverworks.services.ServiceErrorHandler;
import it.neverworks.services.ServiceInvocation;
import it.neverworks.services.ServiceEvent;
import it.neverworks.services.Service;
import it.neverworks.log.Logger;

public class JSONServiceHandler extends PathMatchingHandler {
    
    @Logger.ForMe private static Logger logger;

    protected Service service;
    protected Pointcut pointcut;
    protected List<Interceptor> interceptors;
    protected ServiceErrorHandler errorHandler;
    protected boolean initialized = false;

    protected void init() {
        if( this.interceptors != null && this.interceptors.size() > 0 ) {
            this.pointcut = new MethodPointcut( 
                Reflection.getMethod( Service.class, "service", ServiceEvent.class ) 
            ).add( this.interceptors );
        }
    }
    
    public void handle( Request request ) throws Exception {

        if( ! this.initialized ) {
            synchronized( this ) {
                if( ! this.initialized ) {
                    this.init();
                    this.initialized = true;
                }
            }
        }

        if( this.service != null 
            && ! Method.OPTIONS.equals( request.getMethod() ) ) {

            ServiceEvent event = new ServiceEvent();

            Object parameters = null;
            if( Method.GET.equals( request.getMethod() ) ) {
                parameters = request.getArguments();
            } else {
                parameters = JSON.relaxed.decode( request.getBodyAsString() );
            }
            event.set( "request", parameters != null ? parameters : new Arguments() );
 
            event.set( "metadata", request.getHeaders() );
            event.set( "transport",
                arg( "method", request.getMethod().toString() )
                .arg( "path", request.getPath() )
            );
            
            try {
                if( this.pointcut != null ) {
                    this.pointcut.invoke( new ServiceInvocation( this.service ), event );
                } else {
                    this.service.service( event );
                }
            } catch( Exception ex ) {
                event.set( "error", Errors.unwrap( ex ) );
            }

            if( event.getError() != null ) {
                if( service instanceof ServiceErrorHandler ) {
                    ((ServiceErrorHandler) service).error( event );
                } else if( this.errorHandler != null ) {
                    this.errorHandler.error( event );
                } else {
                    throw event.getError();
                }
            }
            
            request.getResponse().setHeader( "Content-type", "application/json" );

            if( event.isReplied() ) {
                request.getResponse().write( JSON.print( event.getResponse() ) );
            } else {
                request.getResponse().write( "{}" );
            }
        }
    }
    
    public ServiceErrorHandler getErrorHandler(){
        return this.errorHandler;
    }
    
    public void setErrorHandler( ServiceErrorHandler errorHandler ){
        this.errorHandler = errorHandler;
    }

    public void setErrors( ServiceErrorHandler errorHandler ){
        this.errorHandler = errorHandler;
    }
    
    public Service getService(){
        return this.service;
    }
    
    public void setService( Service service ){
        this.service = service;
    }
    
    public List<Interceptor> getInterceptors(){
        return this.interceptors;
    }
    
    public void setInterceptors( List<Interceptor> interceptors ){
        this.interceptors = interceptors;
    }

}