package it.neverworks.services.http;

import java.util.Map;
import it.neverworks.model.Property;
import it.neverworks.model.description.Synthesize;
import it.neverworks.encoding.XML;

public class XMLClient extends HTTPClient {
    
    @Property @Synthesize
    protected Class responseClass;
    
    @Property @Synthesize
    protected Class errorClass;
    
    public XMLClient() {
        super();
        this.mimeType = "application/json";
    }
    
    @Override
    protected String encode( Object value ) {
        return XML.print( value ).asString();
    }
    
    @Override
    protected Object decode( String value ) {
        return decode( value, this.responseClass );
    }

    @Override
    protected Object error( String value ) {
        return decode( value, this.errorClass );
    }
    
    protected Object decode( String value, Class targetClass ) {
        if( targetClass == null ) {
            //TODO: implementare tree map
            return XML.read( value ).as( Map.class );
        } else {
            return XML.read( value ).as( targetClass );
        }
    }

}