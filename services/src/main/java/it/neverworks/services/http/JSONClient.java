package it.neverworks.services.http;

import it.neverworks.model.Property;
import it.neverworks.model.description.Synthesize;
import it.neverworks.encoding.JSON;

public class JSONClient extends HTTPClient {
    
    @Property @Synthesize
    protected Class responseClass;
    
    @Property @Synthesize
    protected Class errorClass;
    
    public JSONClient() {
        super();
        this.mimeType = "application/json";
    }
    
    @Override
    protected String encode( Object value ) {
        return JSON.print( value );
    }
    
    @Override
    protected Object decode( String value ) {
        return decode( value, this.responseClass );
    }

    @Override
    protected Object error( String value ) {
        return decode( value, this.errorClass );
    }
    
    protected Object decode( String value, Class targetClass ) {
        if( targetClass == null ) {
            return JSON.relaxed.decode( value );
        } else {
            return JSON.relaxed.deserialize( value, targetClass );
        }
    }

}