package it.neverworks.services.ldap;

import static it.neverworks.language.*;

import java.util.List;
import java.util.Hashtable;
import java.util.ArrayList;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.CommunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;

public class LdapContextSource extends org.springframework.ldap.core.support.LdapContextSource {

    private static final Logger logger = LoggerFactory.getLogger( LdapContextSource.class );
    
    protected List<String> providerUrls;
    protected boolean failover = true; 
    protected Integer timeout;
    
    @Override
    protected DirContext getDirContextInstance( Hashtable environment ) throws NamingException {
        if( this.timeout != null ) {
            logger.trace( "Setting timeout to {}", this.timeout );
            environment.put( "com.sun.jndi.ldap.connect.timeout", str( this.timeout ) );
        }
        
        if( this.failover && this.providerUrls != null && this.providerUrls.size() > 1 ) {
            logger.debug( "Using failover connection" );
            int i = 0;
            DirContext ctx = null;

            while( i < this.providerUrls.size() && ctx == null ) {
                String currentUrl = this.providerUrls.get( i );
                try {
                    logger.debug( "Trying provider '{}'", currentUrl );
                    environment.put( "java.naming.provider.url", currentUrl );
                    ctx = super.getDirContextInstance( environment );

                    logger.debug( "Successfully connected to '{}'", currentUrl );
                    
                    // Move on top
                    if( !( this.providerUrls.get( 0 ).equals( currentUrl ) ) ) {
                        logger.debug( "Selecting provider '{}' as preferred", currentUrl );
                            this.providerUrls.remove( currentUrl );
                        this.providerUrls.add( 0, currentUrl );
                    }

                } catch( Exception ex ) {
                    logger.error( "Error connecting to '{}': {} {}", currentUrl, ex.getClass().getSimpleName(), ex.getMessage() );

                    ctx = null;
                    if( i < this.providerUrls.size() ) {
                        i++;
                        logger.error( "{} failover providers available", this.providerUrls.size() - i );
                    } else {
                        logger.error( "No more providers" );
                        throw ex;
                    }
                }
            }

            return ctx;

        } else {
            logger.debug( "Skipping failover connection" );
            return super.getDirContextInstance( environment );
        }
    }
    
    @Override
    protected String assembleProviderUrlString( String[] ldapUrls ) {
        this.providerUrls = new ArrayList<String>();
        for( String ldapUrl: ldapUrls ) {
            for( String url: ldapUrl.trim().replaceAll( "\\s+", ";" ).split( ";" ) ) {
                if( ! empty( url ) ) {
                    this.providerUrls.add( super.assembleProviderUrlString( new String[]{ url.trim() } ) );
                }
            }
        }
        
        logger.info( "Configuring providers: {}", this.providerUrls );
        return super.assembleProviderUrlString( ldapUrls );
    }
    
    public boolean getFailover(){
        return this.failover;
    }
    
    public void setFailover( boolean failover ){
        this.failover = failover;
    }
    
    public Integer getTimeout(){
        return this.timeout;
    }
    
    public void setTimeout( Integer timeout ){
        this.timeout = timeout;
    }
    
}
