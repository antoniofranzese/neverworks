package it.neverworks.services;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public interface ServiceInterceptor extends Interceptor {
    
    default Object invoke( Invocation invocation ) {
        invoke( (ServiceInvocation) invocation );
        return null;
    }

    void invoke( ServiceInvocation invocation );
}