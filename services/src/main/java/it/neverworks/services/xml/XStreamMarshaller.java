package it.neverworks.services.xml;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import it.neverworks.log.Logger;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.XmlMappingException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;

import it.neverworks.encoding.xml.TransformerSupport;

public class XStreamMarshaller extends TransformerSupport implements Marshaller, Unmarshaller {

	private static final Logger logger = Logger.of( XStreamMarshaller.class );

	private XStream xstream;
	private HierarchicalStreamDriver driver;
	
	@SuppressWarnings("rawtypes" )
	private Set<Class> types = new HashSet<Class>();

	public XStreamMarshaller() {}
	
	@SuppressWarnings( "rawtypes" )
	public void setSupportedClasses( Set<Class> types ) {
		this.types = types;
	}

	@SuppressWarnings( "rawtypes" )
	public XStreamMarshaller addSupportedClass( Class type ) {
		types.add( type );
		return this;
	}

	public XStream getXStream() {
		if( xstream == null ) {
			if( driver != null ) {
				xstream = new XStream( driver );
			} else {
				xstream = new XStream();
			}
            for( Class cls: types ) {
                xstream.processAnnotations( cls );
            }
		}	
		return xstream;
	}
	
	@Override
	public Object unmarshal( Source source ) throws XmlMappingException, IOException {
		StringWriter sourceWriter = new StringWriter();
		transform( source, new StreamResult( sourceWriter ) );
		if( logger.trace() ) {
			logger.trace( "XML after unmarshal transformation = {}", sourceWriter.toString() );
		}
		Object result = getXStream().fromXML( sourceWriter.toString() );
		return result;
	}

	@Override
	public void marshal( Object graph, Result result ) throws XmlMappingException, IOException {
		String xml = getXStream().toXML( graph );
        if( logger.trace() ) {
    		logger.trace( "XML before marshal transformation = {}", xml );
        }
		transform( new StreamSource( new StringReader( xml ) ), result );
	}

	@Override
	@SuppressWarnings( { "rawtypes", "unchecked" } )
	public boolean supports( Class type ) {
		for ( Class suptype : types ) {
			if ( suptype.isAssignableFrom( type ) ) {
				return true;
			}
		}
		return false;
	}

	public void setDriver(HierarchicalStreamDriver driver) {
		this.driver = driver;
	}

	public void setStreamDriver(HierarchicalStreamDriver driver) {
		this.driver = driver;
	}

	
}
