package it.neverworks.services.xml;

import static it.neverworks.language.*;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

import it.neverworks.lang.ClassScanner;
import it.neverworks.lang.Annotations;

public class Jaxb2Marshaller extends org.springframework.oxm.jaxb.Jaxb2Marshaller {
    
    public void setScanPackage( String pkg ) {
        this.setScanPackages( list( pkg ) );
    }

    public void setScanPackages( List<String> packages ) {
        if( packages != null ) {
            Class[] jaxbClasses = new ClassScanner( packages )
                .annotatedWith( XmlRootElement.class )
                .array( Class.class );
            
            if( jaxbClasses.length > 0 ) {
                this.setClassesToBeBound( jaxbClasses );
            }
        }
    }
}