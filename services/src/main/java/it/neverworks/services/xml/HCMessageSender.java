package it.neverworks.services.xml;

import java.net.URI;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.WebServiceConnection;
import org.apache.http.auth.Credentials;

import static it.neverworks.language.*;

import it.neverworks.lang.Strings;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class HCMessageSender extends BaseModel implements WebServiceMessageSender {
    
    private Credentials credentials;
    
    private String contentType = "text/xml";
    
    private String encoding = "utf-8";
    
    public WebServiceConnection createConnection( URI uri ){
        return new HCMessageConnection(
            arg( "uri", uri )
            .arg( "credentials", this.credentials )
            .arg( "contentType", contentType )
            .arg( "encoding", encoding )
        );
    }
    
    public boolean supports( URI uri ) {
        return Strings.safe( uri.getScheme() ).startsWith( "http" );
    }

    public String getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( String encoding ){
        this.encoding = encoding;
    }
    
    public String getContentType(){
        return this.contentType;
    }
    
    public void setContentType( String contentType ){
        this.contentType = contentType;
    }
    
    public Credentials getCredentials(){
        return this.credentials;
    }
    
    public void setCredentials( Credentials credentials ){
        this.credentials = credentials;
    }
    
}