package it.neverworks.services.xml;

import java.net.URI;
import java.net.URISyntaxException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.springframework.ws.transport.WebServiceConnection;
import org.springframework.ws.WebServiceMessageFactory;
import org.springframework.ws.WebServiceMessage;

import org.apache.http.auth.AuthScope;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.auth.Credentials;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;

public class HCMessageConnection extends BaseModel implements WebServiceConnection {

    @Property @Required
    private URI uri;
    
    @Property
    private Credentials credentials;
    
    @Property
    private String contentType;
    
    @Property
    private String encoding;
    
    @Property
    private AuthScope authScope = AuthScope.ANY; 
    
    public AuthScope getAuthScope(){
        return this.authScope;
    }
    
    public void setAuthScope( AuthScope authScope ){
        this.authScope = authScope;
    }
    
    private CloseableHttpClient client;
    private CloseableHttpResponse response;
    private String error;
    
    public HCMessageConnection() {
        super();
    }
    
    public HCMessageConnection( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public void close() throws IOException {
        this.response.close();
        this.client.close();
    }
    
    public String getErrorMessage() throws IOException {
        return this.error;
    }
    
    public boolean hasError() throws IOException {
        return this.error != null;
    }
    
    public WebServiceMessage receive( WebServiceMessageFactory messageFactory ) throws IOException {
        return messageFactory.createWebServiceMessage( this.response.getEntity().getContent() );
    }
    
    public void send( WebServiceMessage message ) throws IOException {
        HttpClientBuilder builder = HttpClients.custom();

        if( this.credentials != null ) {
            CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

            credentialsProvider.setCredentials(
                this.authScope,
                this.credentials
            );
            
            builder.setDefaultCredentialsProvider( credentialsProvider );
            
        }
        
        this.client = builder.build();
        
        ByteArrayOutputStream messageStream = new ByteArrayOutputStream();
        try {
            message.writeTo( messageStream ); 
            
            HttpPost request = new HttpPost( this.uri );
            request.setEntity( new StringEntity( messageStream.toString(), ContentType.create( this.contentType, this.encoding ) ) );

            this.response = this.client.execute( request );
        
            if( this.response.getStatusLine().getStatusCode() != 200 ) {
                this.error = Strings.message( "[{0}] {1}", this.response.getStatusLine().getStatusCode(), this.response.getStatusLine().getReasonPhrase() );
            }
            
        } finally {
            messageStream.close();
        }

    }

    public URI getUri() throws URISyntaxException {
        return this.uri;
    }
    
    public void setUri( URI uri ){
        this.uri = uri;
    }

    public String getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( String encoding ){
        this.encoding = encoding;
    }
    
    public String getContentType(){
        return this.contentType;
    }
    
    public void setContentType( String contentType ){
        this.contentType = contentType;
    }
    
    public Credentials getCredentials(){
        return this.credentials;
    }
    
    public void setCredentials( Credentials credentials ){
        this.credentials = credentials;
    }
    

}