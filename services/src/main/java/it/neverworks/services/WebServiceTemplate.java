package it.neverworks.services;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import it.neverworks.log.Logger;
import it.neverworks.lang.ClassScanner;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.Errors;

public class WebServiceTemplate extends org.springframework.ws.client.core.WebServiceTemplate {
    
    @Logger.ForMe 
    private static Logger logger;

    private Jaxb2Marshaller scanMarshaller;
    
    @Override
    public void afterPropertiesSet() {
        super.afterPropertiesSet();
        if( this.getMarshaller() == null && this.getUnmarshaller() == null && this.scanPackages != null ) {
            Class[] jaxbClasses = new ClassScanner( this.scanPackages )
                .annotatedWith( XmlRootElement.class )
                .array( Class.class );
                
            if( jaxbClasses.length > 0 ) {
                if( logger.debug() ) {
                    for( Class cls: jaxbClasses ) {
                        logger.debug( "Registering as web service payload class: {}", cls );
                    }
                }
                this.scanMarshaller = createMarshaller( jaxbClasses );
                this.setMarshaller( this.scanMarshaller );
                this.setUnmarshaller( this.scanMarshaller );
            } else {
                logger.debug( "No web service payload classes scanning: {}", scanPackages );
            }
        } else {
            logger.debug( "Skipping payload class scan" );
        }
    }
    
    public <T> T call( String uri, Object request ) {
        return (T) this.marshalSendAndReceive( uri, request );
    }

    public <T> T call( Object request ) {
        return (T) this.marshalSendAndReceive( request );
    }

    public <T> T call( Object request, Class<T> responseType ) {
        return (T) createTemplate( request.getClass(), responseType ).marshalSendAndReceive( this.getDefaultUri(), request );
    }

    public <T> T call( String uri, Object request, Class<T> responseType ) {
        return (T) createTemplate( request.getClass(), responseType ).marshalSendAndReceive( uri, request );
    }

    protected org.springframework.ws.client.core.WebServiceTemplate createTemplate( Class requestType, Class responseType ) {
        org.springframework.ws.client.core.WebServiceTemplate template = new WebServiceTemplate();
        Jaxb2Marshaller marshaller = createMarshaller( requestType, responseType );
        template.setMarshaller( marshaller );
        template.setUnmarshaller( marshaller );
        template.afterPropertiesSet();
        return template;
    }
    
    protected Jaxb2Marshaller createMarshaller( Class... classes ) {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound( classes );
        try {
            marshaller.afterPropertiesSet();
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        return marshaller;
    }

    private List<String> scanPackages;
    public void setScanPackages( List<String> packages ) {
        this.scanPackages = packages;
    }

}