package it.neverworks.services;

public interface ServiceClient {

    public <T> T invoke( Object request );     

    default <T> T invoke() {
        return invoke( null );
    }

}