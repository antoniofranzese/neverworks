define([
    "dojo/_base/declare"
    ,"dojo/topic"
    ,"dojo/dom"
    ,"dojo/dom-style"
], function( declare, topic, dom, domStyle ){

    topic.subscribe( "app/ready", function(){
        if( dom.byId( "app-loader" ) ) {
            domStyle.set( "app-loader", "visibility", "hidden" );
        }
    } )
    
	var refCount = declare([],{
		counter:0
		,constructor:function() {}
		,increase: function() {
			this.counter++;
			
		}
		,decrease: function(){
			if ( this.counter > 0 ) {
				this.counter--;
			}
		}
		,count: function() {
			return this.counter;
		}
	})();

	topic.subscribe( "network/request", function(){
		if ( refCount.count() == 0 ) {
			var netloader = dom.byId( 'app-netloader' );
			if ( netloader != null ) {
				domStyle.set( netloader, "visibility", "visible");
			}
		}
		refCount.increase();
	});

	topic.subscribe( "network/response", function(){
		refCount.decrease();
		if ( refCount.count() == 0 ) {
    		var netloader = dom.byId( 'app-netloader' );
    		if ( netloader != null ) {
    	         domStyle.set( netloader, "visibility", "hidden");
    		}
		}
	});

    return {};
});