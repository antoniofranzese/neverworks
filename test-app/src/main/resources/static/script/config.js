var dojoConfig = {
    async: true
    ,baseUrl: "/neverworks"
    ,packages: [
        { name: "dojo", location: "dojo" }
        ,{ name: "dijit", location: "dijit" }
        ,{ name: "dojox", location: "dojox" }
        ,{ name: "works", location: "works" }
        ,{ name: "gridx", location: "gridx" }
        ,{ name: "ext", location: "ext" }
        ,{ name: "app", location: "/script" }
    ]
    ,gfxRenderer: "svg"
};

var worksConfig = {
    loader: true
    ,styles: [
        { module: "works/app", add:"/neverworks/works/themes/tundra.css", theme: "tundra" }
        ,{ module: "works/app", add:"/neverworks/works/themes/icons.less" }
        ,{ module: "works/app", add:"/style/app.css" }
    ]
    ,viewports: [
        { name: "narrow", media: "(max-width: 500px)" }
        ,{ name: "medium", media: "(min-width: 501px) and (max-width: 1000px)" }
        ,{ name: "wide", media: "(min-width: 1001px)" }
    ]
}    
