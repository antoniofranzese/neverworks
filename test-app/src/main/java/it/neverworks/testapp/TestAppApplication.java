package it.neverworks.testapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import it.neverworks.boot.NeverworksApplication;
import it.neverworks.io.TemporaryFileRepository;

@SpringBootApplication
public class TestAppApplication {

	public static void main(String[] args) {
		NeverworksApplication.init( SpringApplication.run( TestAppApplication.class, args ) );
	}

    @Bean( name = "TemporaryFileRepository" )
    public TemporaryFileRepository temporaryFileRepository() {
        TemporaryFileRepository repo = new TemporaryFileRepository();
        repo.setFolder( "test-app" );
        repo.setFilePrefix( "NW-TESTAPP-" );
        return repo;
    }

}

