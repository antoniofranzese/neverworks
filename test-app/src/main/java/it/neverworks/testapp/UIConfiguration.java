package it.neverworks.testapp;

import static it.neverworks.language.*;

import javax.servlet.http.HttpServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;

import it.neverworks.ui.html.production.storage.MemoryStorage;
import it.neverworks.ui.html.production.ProtocolManager;
import it.neverworks.ui.production.WidgetStorage;

import it.neverworks.ui.html.data.FormattingWebStyleCatalog;
import it.neverworks.ui.html.data.WebFileCatalog;
import it.neverworks.ui.data.Catalog;

import it.neverworks.boot.ui.ProtocolHandlerBuilder;
import it.neverworks.boot.ui.ProtocolManagerBuilder;

@Configuration
public class UIConfiguration  {
    
    @Bean    
    public ServletRegistrationBean<HttpServlet> protocolHandler( ProtocolManager protocolManager ) {
        return new ProtocolHandlerBuilder( protocolManager )
            .mapForms( "/form" )
            .build();
    }

    @Bean 
    public ProtocolManager protocolManager( WidgetStorage widgetStorage ) {
        return new ProtocolManagerBuilder( widgetStorage )
            .build();
    }

    @Bean
    @Scope( value = "session", proxyMode = ScopedProxyMode.INTERFACES )
    public WidgetStorage widgetStorage() {
        return new MemoryStorage();
    }

    @Bean( name = "IconCatalog" )
    public Catalog iconCatalog() {
        return new FormattingWebStyleCatalog( dict(
            pair( "16", "icon-16-{0}" )
            ,pair( "24", "icon-24-{0}" )
            ,pair( "32", "icon-32-{0}" )
            ,pair( "100c", "color-icons-100-{0}" )
            ,pair( "80c", "color-icons-80-{0}" )
            ,pair( "48c", "color-icons-48-{0}" )
            ,pair( "40c", "color-icons-40-{0}" )
            ,pair( "32c", "color-icons-32-{0}" )
            ,pair( "24c", "color-icons-24-{0}" )
            ,pair( "16c", "color-icons-16-{0}" )
        ));
    }

    @Bean( name = "ImageCatalog" )
    public Catalog imageCatalog() {
        return new WebFileCatalog( "/style/images" );
    }

    @Bean( name = "SoundCatalog" )
    public Catalog soundCatalog() {
        return new WebFileCatalog( "/style/sounds" );
    }

}