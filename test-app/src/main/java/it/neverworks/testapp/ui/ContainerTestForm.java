package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.context.Inject;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.web.WebView;
import it.neverworks.ui.web.Conversation;
import it.neverworks.ui.Widget;

public class ContainerTestForm extends Form {
    
    @Property @Reference
    private Conversation conversation;
    
    public void build() {
        add( new GridLayout(
            new Label(
                arg( "value", "Container Form" )
                .arg( "font", arg( "size", 16 ).arg( "weight", "bold" ) )
                .arg( "layout.hspan", "full" )
            )
            ,new TextBox(
                arg( "name", "text1" )
                .arg( "label", "Text 1" )
            )
            ,new ToolBar(
                arg( "layout.hspan", "full" )
                ,new Button(
                    arg( "caption", "Alert" )
                    .arg( "enabled", false )    
                    .arg( "click", slot( click -> {
                        conversation.send( "alert", arg( "text", "Container alert:" + get( "text1.value" ) ) ); 
                    }))
                )
                ,new Button(
                    arg( "caption", "Message" )
                    .arg( "enabled", false )    
                    .arg( "click", slot( click -> {
                        conversation.send( "text", arg( "text", get( "text1.value" ) ) ); 
                    }))
                )
                ,new Button(
                    arg( "caption", "Request" )
                    .arg( "enabled", false )    
                    .arg( "click", slot( click -> {
                        conversation.request( arg( "action", "readText" ) ).then( evt -> {
                            System.out.println( "Container response: " + evt.get( "response.text" ) );
                            add( new MessageBox()
                                .set( "message", evt.get( "response.text" ) ) 
                                .set( "title", "Container response" ) 
                            );
                        }); 
                    }))
                )
            )
            ,new WebView(
                arg( "name", "web" )
                .arg( "location", "http://localhost:8001/form/test/ui/testapp/ContainedTestForm" )
                //.arg( "location", "/form/test/ui/testapp/ContainedTestForm" )
                .arg( "width", 400 )
                .arg( "height", 300 ) 
                .arg( "border", "1px black" )
                .arg( "layout.hspan", "full" )
                .arg( "load", slot( load -> {
                    query( Button.class ).set( "enabled", true );
                }))
            )
            
        ));
        add( new Conversation(
            arg( "name", "conversation" )
            .arg( "channel", "TestChannel" )

            .arg( "text", slot( message -> {
                System.out.println( "Container: " + message.get( "topic" ) + " -> " + message.get( "value" ) );
                set( "text1.value", message.get( "value.text" ) );
            }))

            .arg( "alert", slot( message -> {
                add( new MessageBox()
                    .set( "message", message.get( "value.text" ) ) 
                    .set( "title", "Container" ) 
                );
            }))
        ));
    }
    
}