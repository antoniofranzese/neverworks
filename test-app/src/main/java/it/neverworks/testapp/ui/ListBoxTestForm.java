package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.ListBox;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.GridLayout;

public class ListBoxTestForm extends Form {
    
    private boolean linked = false;
    
    private List items5 = list(
        new Item( arg( "id", 1 ).arg( "name", "Name 1" ).arg( "city", "City 1" ).arg( "age", 10 ) )
        ,new Item( arg( "id", 2 ).arg( "name", "Name 2" ).arg( "city", "City 2" ).arg( "age", 20 ) )
        ,new Item( arg( "id", 3 ).arg( "name", "Name 3" ).arg( "city", "City 3" ).arg( "age", 30 ) )
        ,new Item( arg( "id", 4 ).arg( "name", "Name 4" ).arg( "city", "City 4" ).arg( "age", 40 ) )
        ,new Item( arg( "id", 5 ).arg( "name", "Name 5" ).arg( "city", "City 5" ).arg( "age", 50 ) )
    );

    private List items10 = list(
        new Item( arg( "id", 1 ).arg( "name", "Other Name 1" ).arg( "city", "Other City 1" ).arg( "age", 110 ) )
        ,new Item( arg( "id", 2 ).arg( "name", "Other Name 2" ).arg( "city", "Other City 2" ).arg( "age", 120 ) )
        ,new Item( arg( "id", 3 ).arg( "name", "Other Name 3" ).arg( "city", "Other City 3" ).arg( "age", 130 ) )
        ,new Item( arg( "id", 4 ).arg( "name", "Other Name 4" ).arg( "city", "Other City 4" ).arg( "age", 140 ) )
        ,new Item( arg( "id", 5 ).arg( "name", "Other Name 5" ).arg( "city", "Other City 5" ).arg( "age", 150 ) )
        ,new Item( arg( "id", 6 ).arg( "name", "Other Name 6" ).arg( "city", "Other City 6" ).arg( "age", 160 ) )
        ,new Item( arg( "id", 7 ).arg( "name", "Other Name 7" ).arg( "city", "Other City 7" ).arg( "age", 170 ) )
        ,new Item( arg( "id", 8 ).arg( "name", "Other Name 8" ).arg( "city", "Other City 8" ).arg( "age", 180 ) )
        ,new Item( arg( "id", 9 ).arg( "name", "Other Name 9" ).arg( "city", "Other City 9" ).arg( "age", 190 ) )
        ,new Item( arg( "id", 10 ).arg( "name", "Other Name 10" ).arg( "city", "Other City 10" ).arg( "age", 200 ) )
    );
    
    public void build() {
        
        set( "main", new GridLayout(
            new ListBox()
                .set( "name", "people" )
                .set( "label", "Persona" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", items5 )
                .set( "height", 105 )
                .set( "change", slot( "listChange" ) )
                .set( "click", slot( "listClick" ) )
                .set( "inspect", slot( "listInspect" ) )

            ,new TextBox()
                .set( "name", "txtCity" )
                .set( "label", "Città" )

            ,new TextBox()
                .set( "name", "txtAge" )
                .set( "label", "Età" )
            
            ,new ListBox()
                .set( "name", "people2" )
                .set( "label", "Auto" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", items5 )
                .set( "value", "3" )
                //.set( "height", "auto" ) implicit

            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button()
                    .set( "caption", "Second source" )
                    .set( "click", slot( "loadSecondSource" ) )
                ,new Button()
                    .set( "caption", "First source" )
                    .set( "click", slot( "loadFirstSource" ) )
            )

            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button()
                    .set( "caption", "Readonly" )
                    .set( "click", slot( "toggleReadonly" ) )
                ,new Button()
                    .set( "caption", "Hide" )
                    .set( "click", slot( "toggleVisible" ) )
                ,new Button()
                    .set( "caption", "Required" )
                    .set( "click", slot( "toggleRequired" ) )
            )

            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button()
                    .set( "caption", "Link" )
                    .set( "click", slot( "toggleLink" ) )
                ,new Button()
                    .set( "caption", "Problem" )
                    .set( "click", slot( "toggleProblem" ) )
            )
                
        ));
    }

    public void listClick( Event event ) {
        System.out.println( "Clicked on " + event.get( "id" ) + " at " + event.get( "x" ) + "," + event.get( "y" ) );
    }

    public void listInspect( Event event ) {
        System.out.println( "Inspected on " + event.get( "id" ) + " at " + event.get( "x" ) + "," + event.get( "y" ) );
    }

    public void listChange( ChangeEvent<ListBox> event ) {
        System.out.println( "Changed from " + event.getOld() + " to " + event.getNew() );
        if( get( "people.item" ) != null ) {
            set( "txtCity.value", get( "people.item.city" ) );
            set( "txtAge.value", get( "people.item.age" ) );
        }
        if( linked ) {
            set( "people2.value", event.getNew() );
        }
    }
    
    public void loadSecondSource( Event event ) {
        set( "people.items", items10 );
        set( "people2.items", items10 );
        resetFields();
    }

    public void loadFirstSource( Event event ) {
        set( "people.items", items5 );
        set( "people2.items", items5 );
        resetFields();
    }
    
    protected void resetFields() {
        set( "txtCity.value", null );
        set( "txtAge.value", null );
    }
    
    public void toggleReadonly( Event event ) {
        boolean readonly = event.get( "source.caption" ).equals( "Readonly" );
        set( "people.readonly", readonly );
        set( "people2.readonly", readonly );
        event.set( "source.caption", readonly ? "Writable" : "Readonly" );
    }

    public void toggleVisible( Event event ) {
        boolean visible = ! event.get( "source.caption" ).equals( "Hide" );
        set( "people.visible", visible );
        set( "people2.visible", visible );
        event.set( "source.caption", visible ? "Hide" : "Show" );
    }

    public void toggleRequired( Event event ) {
        boolean required = event.get( "source.caption" ).equals( "Required" );
        set( "txtAge.required", required );
        set( "people2.required", required );
        event.set( "source.caption", required ? "Optional" : "Required" );
    }

    public void toggleLink( Event event ) {
        this.linked = event.get( "source.caption" ).equals( "Link" );
        event.set( "source.caption", this.linked ? "Unlink" : "Link" );
    }

    public void toggleProblem( Event event ) {
        boolean problem = event.get( "source.caption" ).equals( "Problem" );
        set( "people2.problem", problem ? "Please, select an item" : null );
        event.set( "source.caption", problem ? "No problem" : "Problem" );
    }
    
}