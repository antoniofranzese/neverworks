package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import org.joda.time.DateTime;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.TabLayout;
import it.neverworks.ui.layout.Panel;

import it.neverworks.ui.data.Item;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.DateFormatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.ListDataSource;
import it.neverworks.ui.data.TemplateFormatter;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.Property;

import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.grid.SelectEvent;
import it.neverworks.ui.grid.PropertyCell;
import it.neverworks.ui.grid.DateCell;
import it.neverworks.ui.grid.HTMLCell;
import it.neverworks.ui.grid.ClickableCell;
import it.neverworks.ui.grid.RowClickEvent;

import it.neverworks.ui.menu.Menu;
import it.neverworks.ui.menu.MenuItem;

import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ResetCapable;
import it.neverworks.ui.dialogs.MessageBox;

public class DetailGridTestForm extends Form {
    
    List<Item> items1;
    List<Item> items2;
    
    ListDataSource dataSource;
    
    public void build() {
        set( "title", "Detail Grid Test" );
        
        dataSource = new ListDataSource()
            .add( "fullName", new ItemFormatter<Item>(){
                public Object format( Item item ) {
                    return item.get( "surname" ) + " " + item.get( "name" );
                }
            });
        
        items1 = new ArrayList<Item>();
        for( int i = 1; i <= 1000; i++ ) {
            int id = 100 + i;
            items1.add( new Item(
                arg( "id", id )
                .arg( "name", "Item Name " + String.valueOf( i ) )
                .arg( "surname", "Item Surname " + String.valueOf( i ) )
                .arg( "age", 100 + i ) 
                .arg( "guid", Strings.generateHexID( 8 ) + "-" + Strings.generateHexID( 8 ) )
                .arg( "birth", new DateTime().plusDays( i % 10 ).toDate() ) 
                .arg( "ten", i % 10 == 0 ) 
                .arg( "children", list(
                    new Item( arg( "id", 1 ).arg( "name", "Child 1 of " + id ).arg( "age", 10 + i ).arg( "birth", new DateTime().plusDays( i + 1 ).toDate() ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Child 2 of " + id ).arg( "age", 11 + i ).arg( "birth", new DateTime().plusDays( i + 2 ).toDate() ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Child 3 of " + id ).arg( "age", 12 + i ).arg( "birth", new DateTime().plusDays( i + 3 ).toDate() ) )
                ))
            ));
        }

        items2 = new ArrayList<Item>();
        for( int i = 1; i <= 1000; i++ ) {
            items2.add( new Item(
                arg( "id", 100 + i )
                .arg( "name", "Second Name " + String.valueOf( i ) )
                .arg( "surname", "Second Surname " + String.valueOf( i ) )
                .arg( "age", 10 + i ) 
                .arg( "guid", Strings.generateHexID( 8 ) + "-" + Strings.generateHexID( 8 ) ) 
                .arg( "birth", new DateTime().plusMonths( 2 ).plusDays( 10 + i ).toDate() ) 
                .arg( "ten", i % 10 == 0 ) 
            ));
        }

        set( "main", new BorderLayout()
            .set( "center", new Grid()
                .set( "name", "grid1" )
                .set( "detail", new Detail2().set( "name", "dt2" ) )
                .set( "columns", list(
                    new GridColumn()
                        .set( "title", "Nome" )
                        .set( "source", arg( "property", "name" ).arg( "name", "nm" ) )
                    ,new GridColumn()
                        .set( "title", "Cognome" )
                        .set( "source", "surname" )
                    ,new GridColumn()
                        .set( "title", "Nominativo" )
                        .set( "source", "fullName" )

                    ,new GridColumn()
                        .set( "title", "Età" )
                        .set( "source", new HTMLCell(
                            arg( "property", "age" )
                            .arg( "format", new TemplateFormatter( "<b>{age}</b>" ) )
                            // .arg( "format", new Formatter<Integer>(){
                            //     public Object format( Integer value ) {
                            //         return "<b>" + value + "</b>";
                            //     }
                            // }))
                        ))
                        .set( "width", "5%" )
                    ,new GridColumn()
                        .set( "title", "Nascita" )
                        .set( "source", 
                            arg( "property", "birth" )
                            .arg( "format", new DateFormatter( "dd MMM yyyy" ) ) 
                        )
                        .set( "width", 100 )

                    ,new GridColumn()
                        .set( "title", "Ten" )
                        .set( "source", 
                            // arg( "property", "ten" )
                            arg( "format", "{ten,bool,true=Yes}" ) 
                        )
                        .set( "width", 30 )
                        
                    ,new GridColumn(
                        arg( "source", new ClickableCell(
                            arg( "click", slot( "imageClick" ) )
                            .arg( "hint", "Operations on {name}" )
                            //
                            // .arg( "hint", new ItemFormatter<Item>(){
                            //     public Object format( Item item ) {
                            //         return "Operations on " + item.get( "name" );
                            //     }
                            // })

                            // .arg( "caption", new ItemFormatter<Item>(){
                            //     public Object format( Item item ) {
                            //         return item.<Integer>get( "age" ) % 2 == 0 ? "Pari" : "Dispari";
                            //     }
                            // })
                            //
                            // .arg( "width", 90 )
                        ))
                    )
                ))
                .set( "key", "id" )
                .set( "items", dataSource.wrap( items1 ) )
                .set( "select", slot( "gridSelect" ) )
            )

            .set( "top", new ToolBar(
                arg( "padding", 5 )
                ,new Button(
                    arg( "caption", "Load second" )
                    .arg( "click", slot( "loadSecond" ) )
                )
                ,new Button(
                    arg( "caption", "Load first" )
                    .arg( "click", slot( "loadFirst" ) )
                )
                ,new Button(
                    arg( "caption", "Deselect" )
                    .arg( "click", slot( "gridDeselect" ) )
                )
                ,new Button(
                    arg( "caption", "Select 10" )
                    .arg( "click", slot( "gridSelect10" ) )
                )
            ))

        );
        
    }

    public void imageClick( Event event ) {
        System.out.println( "CLICK: " + event );
        String itemName = event.get( "item.name" );

        add( new Menu(
            arg( "opened", true )
            .arg( "autoDestroy", true )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "attributes", arg( "item", event.get( "item" ) ) )
            .arg( "click", slot( "imageMenuClick" ) )
            .arg( "children", list(
                new MenuItem(
                    arg( "caption", "Operation 1 on " + itemName )
                )
                ,new MenuItem(
                    arg( "caption", "Operation 2 on " + itemName )
                )
                ,new MenuItem(
                    arg( "caption", "Operation 3 on " + itemName )
                )
            ))
        ));
        
    }

    public void imageMenuClick( Event event ) {
        
    }

    public void gridSelect10( Event event ) {
        set( "grid1.selected", 9 );
    }    
    
    public void gridDeselect( Event event ) {
        set( "grid1.selected", null );
    }    
    
    public void gridSelect( SelectEvent<Grid> event ) {
        if( event.get( "new" ) != null ) {
            
            if( get( "grid1.detail" ) instanceof Detail2 ) {
                set( "grid1.detail.txtId.value",        get( "grid1.selected.id" ) );
                set( "grid1.detail.txtName.value",      get( "grid1.selected.name" ) );
                set( "grid1.detail.txtSurname.value",   get( "grid1.selected.surname" ) );
                set( "grid1.detail.txtGuid.value",      get( "grid1.selected.guid" ) );
                set( "grid1.detail.txtBirth.value",     get( "grid1.selected.birth" ) );
                set( "grid1.detail.childrenGrid.items", get( "grid1.selected.children" ) );
                set( "grid1.detail.selectedName.value", "" );

            } else {

                if( ((Integer) get( "grid1.selected.id" )) % 2 == 0 ) {
                    set( "grid1.detail", new GridLayout(
                        arg( "padding", 10 )
                        ,new Label()
                            .set( "value", "Dettaglio pari, form" )
                            .set( "layout", arg( "hspan", "full" ) )
                        ,new TextBox()
                            .set( "name", "txtId" )
                            .set( "label", "ID")
                            .set( "value", get( "grid1.selected.id" ) )
                        ,new TextBox()
                            .set( "name", "txtName" )
                            .set( "label", "Nome")
                            .set( "value", get( "grid1.selected.name" ) )
                        ,new TextBox()
                            .set( "name", "txtSurname" )
                            .set( "label", "Cognome" )
                            .set( "value", get( "grid1.selected.surname" ) )
                        ,new ToolBar(
                            arg( "layout", arg( "hspan", "full" ) )
                            ,new Button()
                                .set( "caption", "Uppercase" )
                                .set( "click", slot( "detailUppercaseClick" ) )
                        )
                    ));

                } else {
                    set( "grid1.detail", new GridLayout(
                        arg( "padding", 10 )
                        .arg( "columns", 1 )
                        ,new Label()
                            .set( "value", "Dettaglio dispari, info" )

                        ,new Label()
                            .set( "value", Strings.message( "ID: {0}, Cognome: {1}, Nome: {2}"
                                ,get( "grid1.selected.id" ) 
                                ,get( "grid1.selected.surname" ) 
                                ,get( "grid1.selected.name" ) 
                            ))
                    ));
        
                }
            }
        }
    }
    
    public void detailUppercaseClick( Event event ) {
        if( get( "grid1.detail.txtName.value" ) != null ) {
            set( "grid1.detail.txtName.value", this.<String>get( "grid1.detail.txtName.value" ).toUpperCase() );
        }
    }
    
    public void loadFirst( Event event ) {
        Object selected = get( "grid1.selected" );
        set( "grid1.items", dataSource.wrap( items1 ) );
        set( "grid1.detail", new Detail2() );
        set( "grid1.selected", selected );
    }

    public void loadSecond( Event event ) {
        Object selected = get( "grid1.selected" );
        set( "grid1.items", dataSource.wrap( items2 ) );
        set( "grid1.detail", null );
        set( "grid1.selected", selected );
    }

    public static class Detail1 extends Form {
        public void build() {
            set( "main", new GridLayout(
                arg( "padding", 10 )
                ,new TextBox()
                    .set( "name", "txtId" )
                    .set( "label", "ID")
                ,new TextBox()
                    .set( "name", "txtName" )
                    .set( "label", "Nome")
                ,new TextBox()
                    .set( "name", "txtSurname" )
                    .set( "label", "Cognome")
                ,new TextBox()
                    .set( "name", "txtGuid" )
                    .set( "label", "GUID")
                ,new DateTextBox()
                    .set( "name", "txtBirth" )
                    .set( "label", "Nascita")
                ,new ToolBar(
                    arg( "layout", arg( "hspan", "full" ) )
                    ,new Button()
                        .set( "caption", "Uppercase" )
                        .set( "click", slot( "uppercaseClick" ) )
                    ,new Button()
                        .set( "caption", "Switch" )
                        .set( "click", slot( "switchClick" ) )
                )
            ));
        }
        
        public void uppercaseClick( Event event ) {
            if( get( "txtName.value" ) != null ) {
                set( "txtName.value", this.<String>get( "txtName.value" ).toUpperCase() );
            }
        }
        
        public void switchClick( Event event ) {
            if( get( "main.columns" ).equals( 2 ) ) {
                set( "main.columns", 4 );
            } else {
                set( "main.columns", 2 );
            }
        }
        
    }
    
    public static class Detail2 extends Form {
        public void build() {
            set( "main", new TabLayout(
                arg( "nested", true )
                .arg( "height", 180 )
                //.arg( "stripPadding", arg( "left", 22 ) )
                ,new BorderLayout(
                    arg( "title", "Details" )
                    .arg( "design", "sidebar" )
                    .arg( "left", new GridLayout(
                        arg( "padding", 10 )
                        .arg( "width", 320 )
                        ,new TextBox()
                            .set( "name", "txtId" )
                            .set( "label", "ID")
                        ,new TextBox()
                            .set( "name", "txtName" )
                            .set( "label", "Nome")
                        ,new TextBox()
                            .set( "name", "txtSurname" )
                            .set( "label", "Cognome")
                        ,new TextBox()
                            .set( "name", "txtGuid" )
                            .set( "label", "GUID")
                        ,new DateTextBox()
                            .set( "name", "txtBirth" )
                            .set( "label", "Nascita")
                        ,new ToolBar(
                            arg( "layout", arg( "hspan", "full" ) )
                            ,new Button()
                                .set( "caption", "Uppercase" )
                                .set( "click", slot( "uppercaseClick" ) )
                            ,new Button()
                                .set( "caption", "Tooltip" )
                                .set( "click", slot( "tooltipClick" ) )
                        )
                    ))
                    .arg( "center", new Grid()
                        //.set( "border", arg( "left", arg( "size", 1 ).arg( "color", "gray" ) ).arg( "bottom", arg( "size", 1 ).arg( "color", "gray" ) ) )
                        .set( "font", 10 )
                        .set( "name", "childrenGrid" )
                        .set( "key", "id" )
                        .set( "select", slot( "childSelect" ) )
                        .set( "columns", list(
                            new GridColumn()
                                .set( "title", "Nome" )
                                .set( "source", arg( "property", "name" ).arg( "name", "nm" ) )

                            ,new GridColumn()
                                .set( "title", "Età" )
                                .set( "source", new HTMLCell(
                                    arg( "property", "age" )
                                    .arg( "format", new Formatter<Integer>(){
                                        public Object format( Integer value ) {
                                            return "<b>" + value + "</b>";
                                        }
                                    }))
                                )
                                .set( "width", "20%" )
                            ,new GridColumn()
                                .set( "title", "Nascita" )
                                .set( "source", 
                                    arg( "property", "birth" )
                                    .arg( "format", new DateFormatter( "dd MMM yyyy" ) ) 
                                )
                                .set( "width", 150 )
                        ))
                    )
                
                    .arg( "bottom", new GridLayout(
                        arg( "height", 25 )
                        .arg( "padding", arg( "top", 5 ) )
                        ,new TextBox()
                            .set( "name", "selectedName" )
                            .set( "label", "Nome selezionato")
                    ))
                )
            
                ,new Panel()
                    .set( "title", "Other" )
            ));
            
        }
        
        public void uppercaseClick( Event event ) {
            if( get( "txtName.value" ) != null ) {
                set( "txtName.value", this.<String>get( "txtName.value" ).toUpperCase() );
            }
        }
        
        public void tooltipClick( Event event ) {
            add( new MessageBox()
                .set( "name", "dtool" )
                .set( "message", "Message in a tooltip" )
                .set( "anchor", event.getSource() )
            );
        }
        
        public void childSelect( Event event ) {
            set( "selectedName.value", get( "childrenGrid.selected.name" ) );
        }
        
    }
    
}
