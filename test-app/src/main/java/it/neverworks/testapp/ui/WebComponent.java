package it.neverworks.testapp.ui;

import it.neverworks.lang.Arguments;
import it.neverworks.ui.web.WebView;

public class WebComponent extends WebView {
    
    public WebComponent() {
        super();
    }
    
    public WebComponent( Arguments arguments ) {
        super();
        set( arguments );
    }

    public void build() {
        set( "location", "/test/ext/form.html" );
    }
    
    public void alert( String message ) {
        this.execute( "alert( '" + message + ":' + window.location.href );" );
    }
    
    
}