package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.DateMidnight;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Inspector;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.BaseTextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.data.ListDataSource;
import it.neverworks.ui.data.Formatter;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;

import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.grid.SelectEvent;
import it.neverworks.ui.grid.PropertyCell;
import it.neverworks.ui.grid.DateCell;
import it.neverworks.ui.grid.HTMLCell;

import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ResetCapable;

public class HugeGridTestForm extends Form {
    
    List<Item> items1;
    List<Item> items2;

    public class HugeDataSource extends ListDataSource {
        private String prefix;

        public HugeDataSource( String prefix ) {
            super( new ArrayList() );
            this.prefix = prefix;
        }
        
        public Iterable slice( int start, int count ) {
            List results = new ArrayList();
            for( int i = start; i < start + count; i++ ) {
                results.add( item( i ) );
            }                
            return results;  
        }                    
                             
        public Object findByKey( String name, Object value ) {
            if( name.equals( "id" ) ) {
                return item( Integer.parseInt( value.toString() ) );
            } else {
                throw new RuntimeException( "Unknown key: " + name );
            }
        }                     
                             
        protected Item item( int id ) {
            return new Item(
                arg( "id", id )
                .arg( "name", prefix + " Name " + id )
                .arg( "surname", prefix + " Surname è " + id )
                .arg( "age", 100 + id )
                .arg( "guid", Strings.generateHexID( 8 ) + "-" + Strings.generateHexID( 8 ) ) 
                .arg( "birth", new DateMidnight( 1972, 1, 16 ).plusDays( id ).toDate() ) 
            );
        }                    
        
        public int size() {
            return 10000;
        }
        
        public List build() {
            List results = new ArrayList();
            for( int i = 0; i < size(); i++ ) {
                results.add( item( i ) );
            }
            return results;
        }
    }
    
    public void build() {
        set( "title", "Huge Grid Test" );
        
        set( "main", new BorderLayout()
            .set( "center", new Grid()
                .set( "name", "grid1" )
                .set( "pageSize", 100 )
                .set( "columns", list(
                    new GridColumn()
                        .set( "title", "Nome" )
                        .set( "source", new PropertyCell( arg( "property", "name" ).arg( "name", "nm" ) ) )
                    ,new GridColumn()
                        .set( "title", "Cognome" )
                        .set( "source", "surname" )
                    ,new GridColumn()
                        .set( "title", "Età" )
                        .set( "source", new HTMLCell(
                            arg( "property", "age" )
                            .arg( "format", new Formatter<Integer>(){
                                public Object format( Integer value ) {
                                    return "<b>" + value + "</b>";
                                }
                            })
                        ))
                        .set( "width", "8%" )
                    ,new GridColumn()
                        .set( "title", "Nascita" )
                        .set( "source", new DateCell( "birth" ) )
                        .set( "width", 100 )
                ))
                .set( "key", "id" )
                .set( "items", new HugeDataSource( "First" ).build() )
                .set( "select", new Slot( this, "gridSelect" ) )
            )

            .set( "top", new ToolBar(
                new Button()
                    .set( "caption", "Load second" )
                    .set( "click", new Slot( this, "loadSecond" ) )
                ,new Button()
                    .set( "caption", "Load first" )
                    .set( "click", new Slot( this, "loadFirst" ) )
            ))

            .set( "right", new GridLayout(
                new TextBox()
                    .set( "name", "txtId" )
                    .set( "label", "ID")
                ,new TextBox()
                    .set( "name", "txtName" )
                    .set( "label", "Nome")
                ,new TextBox()
                    .set( "name", "txtSurname" )
                    .set( "label", "Cognome")
                ,new TextBox()
                    .set( "name", "txtGuid" )
                    .set( "label", "GUID")
                ,new DateTextBox()
                    .set( "name", "txtBirth" )
                    .set( "label", "Nascita")
            ))
        );
        
    }
    
    public void gridSelect( SelectEvent<Grid> event ) {
        if( get( "grid1.selected" ) != null ) {
            set( "txtId.value",      get( "grid1.selected.id" ) );
            set( "txtName.value",    get( "grid1.selected.name" ) );
            set( "txtSurname.value", get( "grid1.selected.surname" ) );
            set( "txtGuid.value",    get( "grid1.selected.guid" ) );
            set( "txtBirth.value",   get( "grid1.selected.birth" ) );
        }
    }
    
    public void loadFirst( Event event ) {
        set( "grid1.items", new HugeDataSource( "First" ).build() );
        resetFields();
    }

    public void loadSecond( Event event ) {
        set( "grid1.items", new HugeDataSource( "Second" ).build() );
        resetFields();
    }

    protected void resetFields() {
        query().instanceOf( BaseTextBox.class ).inspect( new Inspector<BaseTextBox>(){
            public void inspect( BaseTextBox widget ) {
                widget.reset();
            }
        });
    }
}
