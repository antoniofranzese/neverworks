package it.neverworks.testapp.ui;

import java.util.List;
import java.io.File;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Reference;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.web.Uploader;
import it.neverworks.ui.web.Downloader;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;

import it.neverworks.io.FileRepository;
import it.neverworks.io.FileBadge;

public class UploadForm extends Form {
    
    @Property @Inject( "TemporaryFileRepository" )
    FileRepository files;
    
    @Property @Reference
    Downloader downloader;
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 ) 
            
            ,new Label()
                .set( "value", "Upload" )
            
            ,new Uploader()
                .set( "name", "myfiles" )
                .set( "change", slot( "changeFiles" ) )
            
            ,new TextBox()
                .set( "name", "nameBox")
                .set( "label", "Repo name" )
                .set( "writable", false )
                .set( "width", 400 )

            ,new TextBox()
                .set( "name", "typeBox")
                .set( "label", "Type" )
                .set( "writable", false )

            ,new ToolBar(
                arg( "name", "buttons" )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )

                ,new Button()
                    .set( "name", "downloadButton" )
                    .set( "caption", "Download info")
                    .set( "click", slot( "downloadClick" ) )
                    .set( "enabled", false )

                ,new Button()
                    .set( "name", "download2Button" )
                    .set( "caption", "Download file")
                    .set( "click", slot( "download2Click" ) )
                    .set( "enabled", false )
            )
        ));
        
        set( "downloader", new Downloader() );
    }
    
    public void downloadClick( ClickEvent<Button> event ) {
        downloader.send( files.getInfo( (FileBadge) get( "myfiles.files[0].badge" ) ) );
    }

    public void download2Click( ClickEvent<Button> event ) {
        downloader.send( (File) get( "myfiles.files[0].file" ), 
            arg( "name", get( "myfiles.files[0].name" ) )
            .arg( "type", get( "myfiles.files[0].type" ) )
        );
    }
    
    public void changeFiles( Event event ) {
        print( "Files changed" );
        print( event.get( "files" ) );
        set( "nameBox.value", get( "myfiles.files[0].file.name" ) );
        set( "typeBox.value", get( "myfiles.files[0].type" ) );
        set( "downloadButton.enabled", true );
        set( "download2Button.enabled", true );
    }
    
    public FileRepository getFiles(){
        return this.files;
    }
}