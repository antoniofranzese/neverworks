package it.neverworks.testapp.ui;

import java.util.Locale;

import static it.neverworks.i18n.Wraps.T;
import static it.neverworks.language.*;
import it.neverworks.i18n.Translation;
import it.neverworks.lang.Objects;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.SelectChangeEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.runtime.Device;

public class TranslatedTestForm extends Form {
    
    public void build() {
        System.out.println( "UserAgent family: " + Device.get( "userAgent.family" ) );
        System.out.println( "Languages: " + Device.get( "languages" ) );
        
        set( "main", new GridLayout(
            new Select()
                .set( "name", "people" )
                .set( "label", T("Person") )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", list(
                    new Item( arg( "id", 1L ).arg( "name", "Name1" ).arg( "city", "City1" ).arg( "age", 10 ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Name2" ).arg( "city", "City2" ).arg( "age", 20 ) )
                    ,new Item( arg( "id", 3L ).arg( "name", "Name3" ).arg( "city", "City3" ).arg( "age", 30 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Name4" ).arg( "city", "City4" ).arg( "age", 40 ) )
                    ,new Item( arg( "id", 5L ).arg( "name", "Name5" ).arg( "city", "City5" ).arg( "age", 50 ) )
                ))

            ,new TextBox()
                .set( "name", "txtCity" )
                .set( "label", T("City") )

            ,new TextBox()
                .set( "name", "txtAge" )
                .set( "label", T("Age") )

            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button()
                    .set( "caption", T("Swap") )
                    .set( "click", slot( "toggle" ) )
            )
            
        ));
    }

    public void toggle( Event event ) {
        Translation.select( Translation.current().equals( Locale.ITALIAN ) ? Locale.ENGLISH : Locale.ITALIAN );
        swap( new TranslatedTestForm()
            .store( "people.value", get( "people.value" ) )
            .store( "txtCity.value", get( "txtCity.value" ) )
            .store( "txtAge.value", get( "txtAge.value" ) )
        );
    }
}