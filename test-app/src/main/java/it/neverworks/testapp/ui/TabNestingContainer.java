package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.CloseEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.AccordionLayout;

public class TabNestingContainer extends Form
{
	
	@Property @Reference
    private AccordionLayout pages;
	
	
	public void build()
	{
		set( "main", new BorderLayout(
            arg( "gutters", true )

            .arg( "center", new AccordionLayout(
                arg( "name", "pages" )	               
                ,new Panel(
                    arg( "title", "Pag. 1" )
                    .arg( "content", new TabNestingForm() )
                )	                
                ,new Panel(
                    arg( "title", "Pag. 2" )
                )
            ))

        ));

	}
	
}
