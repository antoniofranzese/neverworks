package it.neverworks.testapp.ui;

import java.util.List;
import java.io.File;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Catalog;
import it.neverworks.ui.media.SoundPlayer;

public class SoundTestForm extends Form {
    
    @Property @Inject( "SoundCatalog" )
    Catalog sounds;
    
    @Property @Reference
    SoundPlayer player;
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 ) 
            
            ,new ToolBar(
                arg( "name", "buttons" )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )

                ,new Button()
                    .set( "caption", "Play")
                    .set( "click", slot( "playClick" ) )

                ,new Button()
                    .set( "caption", "Stop")
                    .set( "click", slot( "stopClick" ) )

                ,new Button()
                    .set( "caption", "Alarm 2")
                    .set( "click", slot( "alarm2Click" ) )

                ,new Button()
                    .set( "caption", "Alarm 1")
                    .set( "click", slot( "alarm1Click" ) )

                ,new Button()
                    .set( "caption", "Loop")
                    .set( "click", slot( "loopClick" ) )

                ,new Button()
                    .set( "caption", "Volume 50%")
                    .set( "click", slot( "volumeClick" ) )

            )
        ));
        
        set( "player", new SoundPlayer(
            arg( "source", get( "sounds[alarm1.wav]" ) )
        ));
    }
    
    public void playClick( Event event ) {
        player.play();
    }
    
    public void stopClick( Event event ) {
        player.stop();
    }

    public void alarm1Click( Event event ) {
        player.set( "source", get( "sounds[alarm1.wav]" ) );
    }
    
    public void alarm2Click( Event event ) {
        player.set( "source", get( "sounds[alarm2.wav]" ) );
    }
 
    public void loopClick( Event event ) {
        boolean loop = event.get( "source.caption" ).equals( "Loop" );
        player.set( "loop", loop );
        event.set( "source.caption", loop ? "No loop" : "Loop" );
    }

    public void volumeClick( Event event ) {
        int volume = event.get( "source.caption" ).equals( "Volume 50%" ) ? 50 : 100;
        player.set( "volume", volume );
        event.set( "source.caption", volume == 50 ? "Volume 100%" : "Volume 50%" );
    }
    
}