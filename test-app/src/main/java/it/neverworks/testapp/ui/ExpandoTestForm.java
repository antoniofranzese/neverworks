package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.ExpandoPanel;

public class ExpandoTestForm extends Form {
    
    public void build() {
        add( new BorderLayout(
            arg( "name", "main" )
            
            .arg( "top", new ExpandoPanel(
                arg( "title", "Top Expando Panel" )
                .arg( "height", 150 )
                .arg( "content", new Label()
                    .set( "value", "TOP" )
                )
            ))

            .arg( "bottom", new ExpandoPanel(
                arg( "title", "Bottom Expando Panel" )
                .arg( "height", 150 )
                .arg( "content", new Label()
                    .set( "value", "BOTTOM" )
                )
            ))

            .arg( "left", new ExpandoPanel(
                arg( "title", "Left Expando Panel" )
                .arg( "width", 200 )
                .arg( "content", new Label()
                    .set( "value", "LEFT" )
                )
                .arg( "toggle", slot( "togglingLeftPanel" ) )
            ))
        
            .arg( "right", new ExpandoPanel(
                arg( "title", "Right Expando Panel" )
                .arg( "width", 200 )
                .arg( "content", new Label()
                    .set( "value", "RIGHT" )
                )
                // .arg( "expand", slot( "expandingRightPanel" ) )
                // .arg( "collapse", slot( "collapsingRightPanel" ) )
            ))
            
            .arg( "center", new Panel(
                arg( "content", new GridLayout(
                    new ToolBar(
                        new Button()
                            .set( "caption", "Toggle <->" )
                            .set( "click", slot( "toggleLeftRight" ) )
                        ,new Button()
                            .set( "caption", "Toggle All" )
                            .set( "click", slot( "toggleAll" ) )
                    )
                ))
            ))
        
        ));
    }
    
    public void toggleLeftRight( Event event ) {
        this.<ExpandoPanel>get( "main.left" ).toggle();
        this.<ExpandoPanel>get( "main.right" ).toggle();
    }

    public void toggleAll( Event event ) {
        this.<ExpandoPanel>get( "main.left" ).toggle();
        this.<ExpandoPanel>get( "main.right" ).toggle();
        this.<ExpandoPanel>get( "main.top" ).toggle();
        this.<ExpandoPanel>get( "main.bottom" ).toggle();
    }

    public void togglingLeftPanel( Event event ) {
        if( event.<Boolean>get( "collapsing" ) ) {
            event.set( "source.title", "Collapsed Left Expando Panel" );
        } else {
            event.set( "source.title", "Left Expando Panel" );
        }
    }

    // public void collapsingRightPanel( Event event ) {
    //     event.set( "source.title", "Collapsed Right Expando Panel" );
    // }
    //
    // public void expandingRightPanel( Event event ) {
    //     event.set( "source.title", "Right Expando Panel" );
    // }

}