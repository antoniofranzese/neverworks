package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.ValueFormatter;

public class TextBoxTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
             new TextBox()
                .set( "name", "text1" )
                .set( "label", "Text 1" )

            ,new Button()
                .set( "caption", "Monitor" )
                .set( "name", "monitorButton" )
                .set( "click", slot( "monitorClick" ) )

            ,new Label()
                .set( "name", "changeLabel1" )

            ,new TextBox()
               .set( "name", "text2" )
               .set( "label", "Limited (10)" )
               .set( "length", 10 )

            ,new TextBox()
                .set( "name", "text3" )
                .set( "label", "Uppercase" )
                .set( "format", new ValueFormatter<String>(){
                    public Object format( String value ) {
                        return value.toUpperCase();
                    }
                })

            ,new TextBox()
                .set( "name", "text4" )
                .set( "label", "Switchable uppercase" )

            ,new Button()
                .set( "caption", "Format" )
                .set( "name", "formatButton" )
                .set( "click", slot( "formatClick" ) )
                .set( "layout", arg( "hspan", "full" ) )

            ,new ToolBar(
                arg( "name", "buttons")
                .arg( "layout",
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
            )

        ));

    }
    
    public void monitorClick( Event event ) {
        if( event( "text1.change" ).size() > 0 ) {
            event( "text1.change" ).remove( slot( "text1Changed" ) );
            set( "monitorButton.caption", "Monitor" );
        } else {
            event( "text1.change" ).add( slot( "text1Changed" ) );
            set( "monitorButton.caption", "Unmonitor" );
        }
    }
    
    public void text1Changed( Event event ) {
        if( get( "text1.value" ).equals( "error" ) ) {
            add( new MessageBox( "Error in TextBox" ) );
            set( "text1.value", null );

        } else {
            set( "changeLabel1.value", "Text1 = " + get( "text1.value" ) );

        }
    }
    
    public void formatClick( Event event ) {
        if( get( "text4.format" ) == null ) {
            set( "text4.format", new ValueFormatter<String>(){
                public Object format( String value ) {
                    return value.toUpperCase();
                }
            });
            set( "formatButton.caption", "No format" );

        } else {
            set( "text4.format", null );
            set( "formatButton.caption", "Format" );

        }
    }
    
}