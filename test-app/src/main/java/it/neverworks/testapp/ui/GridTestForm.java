package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import org.joda.time.DateTime;

import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.context.Inject;
import it.neverworks.model.expressions.Template;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.data.Formatter;
import it.neverworks.ui.data.DateFormatter;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.data.ListDataSource;
import it.neverworks.ui.data.Catalog;
import it.neverworks.ui.data.Artifact;

import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.grid.GridHeader;
import it.neverworks.ui.grid.SelectEvent;
import it.neverworks.ui.grid.PropertyCell;
import it.neverworks.ui.grid.DateCell;
import it.neverworks.ui.grid.HTMLCell;
import it.neverworks.ui.grid.ItemSelector;
import it.neverworks.ui.grid.CollectingSelector;
import it.neverworks.ui.types.Decorator;
import it.neverworks.ui.types.Decoration;
import it.neverworks.ui.web.Downloader;
import it.neverworks.io.FileRepository;

import it.neverworks.ui.menu.Menu;
import it.neverworks.ui.menu.MenuItem;

import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ResetCapable;
import it.neverworks.ui.dialogs.MessageBox;

public class GridTestForm extends Form {
    
    List<Item> items1;
    List<Item> items2;
    
    ListDataSource dataSource;
    
    @Property @Reference
    Grid grid1;

    @Property @Reference
    Downloader downloader;
    
    @Property @Inject( "IconCatalog" )
    private Catalog styles;

    @Property @Inject( "TemporaryFileRepository" )
    private FileRepository files;
    
    public void build() {
        set( "title", "Grid Test" );
        set( "downloader", new Downloader() );
        
        dataSource = new ListDataSource()
            .add( "fullName", new ItemFormatter<Item>(){
                public Object format( Item item ) {
                    return item.get( "surname" ) + " " + item.get( "name" );
                }
            });
        
        items1 = new ArrayList<Item>();
        for( int i = 1; i <= 100; i++ ) {
            items1.add( new Item(
                arg( "id", i * ( i % 2 == 0 ? -1 : 1 ) )
                .arg( "name", "Item Namè " + String.valueOf( i ) )
                .arg( "surname", "Item Surnamé " + String.valueOf( i ) )
                .arg( "age", 100 + i ) 
                .arg( "guid", Strings.generateHexID( 8 ) + "-" + Strings.generateHexID( 8 ) )
                .arg( "birth", new DateTime().plusDays( i ).toDate() ) 
                .arg( "selected", false )
                .arg( "type", i % 3 )
            ));
        }

        items2 = new ArrayList<Item>();
        for( int i = 1; i <= 9; i++ ) {
            items2.add( new Item(
                arg( "id", 100 + i )
                .arg( "name", "Second Name " + String.valueOf( i ) )
                .arg( "surname", "Second Surname " + String.valueOf( i ) )
                .arg( "age", 10 + i ) 
                .arg( "guid", Strings.generateHexID( 8 ) + "-" + Strings.generateHexID( 8 ) ) 
                .arg( "birth", new DateTime().plusMonths( 2 ).plusDays( 10 + i ).toDate() ) 
                .arg( "selected", false )
                .arg( "type", i % 3 )
            ));
        }

        // final Artifact underlined = get( "styles[underlined]" );
        //
        set( "main", new BorderLayout(
            arg( "center", new Grid(
                arg( "name", "grid1" )
                .arg( "key", "id" )
                .arg( "items", dataSource.wrap( items1 ) )
                .arg( "select", slot( "gridSelect" ) )
                .arg( "inspect", slot( "gridInspect" ) )
                .arg( "click", slot( "gridClick" ) )
                .arg( "rows", new Decorator<Item>(){
                    public Decoration decorate( Item item ) {
                        // int type = item.<Integer>get( "type" );
                        // if( type == 0 ) {
                        //     return new Decoration( arg( "color", "red"  ) );
                        // }   if( type == 1 ) {
                        //     return new Decoration( arg( "color", "black" ) );
                        //
                        // }  else {
                        //     return new Decoration( arg( "color", "yellow" ) );
                        // }
                        if( item.<Boolean>get( "selected" ) ) {
                            return new Decoration( arg( "background", "yellow" ) );
                        } else if( item.<Integer>get( "age" ) % 10 == 0 ) {
                            return new Decoration( arg( "background", "lightgray" ) );

                        } else if( item.<Integer>get( "age" ) % 5 == 0 ) {
                            return new Decoration( arg( "color", "red" ) );

                        } else if( item.<Integer>get( "age" ) % 2 == 0 ) {
                            return new Decoration( arg( "font.style", "italic" ) );

                        } else {
                            return new Decoration( arg( "style", "underlined" ) );
                        }
                    }
                })
                // .arg( "selector", false )
                .arg( "columns", new GridHeader(
                    arg( "inspect", slot( "columnInspect" ) )
                    //.arg( "visible", false )
                    ,new GridColumn(
                        arg( "name", "selector" )
                        .arg( "width", 16 )
                        .arg( "source", new ItemSelector(
                            arg( "property", "selected" )
                            .arg( "attribute", "selection1" )
                            .arg( "click", new Dispatcher(){
                                public void dispatch( Event event ) {
                                    System.out.println( "CLICKED " + event.get( "item.name" ) );
                                }
                            })
                        ))
                        //.arg( "lock", true )
                        // .arg( "source", new HTMLCell(
                        //     //arg( "format", "<div class=\"click {selected,bool,true=checked|false=unchecked}\"></div>" )
                        //
                        //     arg( "format", new ItemFormatter<Item>(){
                        //         Template template = new Template( "<div class=\"click {selected,bool,true=checked|false=unchecked}\"></div>" );
                        //
                        //         public Object format( Item item ) {
                        //             if( item.<Integer>get( "id") % 5 == 0 ) {
                        //                 return "";
                        //             } else {
                        //                 return template.apply( item );
                        //             }
                        //         }
                        //     })
                        // ))
                        // .arg( "row-click", new Dispatcher(){
                        //     public void dispatch( Event event ) {
                        //         if( event.<Integer>get( "item.id") % 5 != 0 ) {
                        //             event.set( "item.selected", ! event.<Boolean>get( "item.selected" ) );
                        //             event.<Grid>get( "source" ).touch( event.get( "item" ) );
                        //         }
                        //         event.stop();
                        //     }
                        // })
                    )

                    ,new GridColumn(
                        arg( "name", "collector" )
                        // .arg( "width", 16 )
                        // .arg( "attributes", arg( "selection", new ArrayList<Integer>() ) )
                        .arg( "source", new CollectingSelector(
                            arg( "property", "id" )
                            .arg( "attribute", "selection2" )
                        ))
                        //.arg( "lock", true )
                        // .arg( "source", new HTMLCell(
                        //     arg( "format", new ItemFormatter<Item>(){
                        //         Template template = new Template( "<div class=\"click {/,bool,true=checked|false=unchecked}\"></div>" );
                        //         public Object format( Item item ) {
                        //             return template.apply( selection.contains( item.get( "id" ) ) );
                        //         }
                        //
                        //         List<Integer> selection;
                        //
                        //         public void setSelection( List<Integer> selection ){
                        //             this.selection = selection;
                        //         }
                        //     })
                        // ))
                        // .arg( "row-click", new Dispatcher(){
                        //     public void dispatch( Event event ) {
                        //         List<Integer> selection = event.get( "column.attributes.selection");
                        //         Integer id = event.get( "item.id" );
                        //         if( selection.contains( id ) ) {
                        //             selection.remove( id );
                        //         } else {
                        //             selection.add( id );
                        //         }
                        //         event.<Grid>get( "source" ).touch( event.get( "item" ) );
                        //         event.stop();
                        //     }
                        // })
                        // .arg( "startup", new Dispatcher(){
                        //     public void dispatch( Event event ) {
                        //         event.set( "source.cell.format.selection", event.get( "source.attributes.selection" ) );
                        //     }
                        // })
                    )
                    
                    ,new GridColumn(
                        arg( "name", "name" )
                        .arg( "title",
                            arg( "value", "Nome (unsortable)" )
                            .arg( "font.weight", "bold" )
                        )
                        .arg( "source", arg( "property", "name" ).arg( "name", "nm" ) )
                        .arg( "sortable", false )
                        .arg( "row-inspect", slot( "nameInspect" ) )
                        .arg( "attributes.xls.title", "Nome (XLS)")
                        .arg( "height", 30 )
                        .arg( "background", "lightgray" )
                        //.arg( "lock", true )
                    )
                    ,new GridColumn(
                        arg( "title", 
                            arg( "value", "Cognome" ) 
                            .arg( "background", "yellow" )
                        )
                        .arg( "name", "surname" )
                        .arg( "source", "surname" )
                        .arg( "halign", "right" )
                    )
                    ,new GridColumn(
                        arg( "title", 
                            arg( "value", "Nominativo" )
                            .arg( "font.weight", "bold" )
                            .arg( "color", "red" )
                        )
                        .arg( "name", "fullname" )
                        .arg( "wrap", false )
                        .arg( "source", "fullName" )
                        .arg( "attributes.xml.name", "the-full-name" )
                        .arg( "attributes.xml.cdata", true )
                    )                
                    // ,new GridColumn()
                    //     .set( "title", "Nominativo" )
                    //     .set( "source", arg( "format", new ItemFormatter<Item>(){
                    //         public Object format( Item item ) {
                    //             return item.get( "surname" ) + " " + item.get( "name" );
                    //         }
                    //     }))
                    ,new GridColumn(
                        arg( "title", "Età" )
                        .arg( "name", "age" )
                        .arg( "source", new HTMLCell(
                            arg( "property", "age" )
                            .arg( "format", new Formatter<Integer>(){
                                public Object format( Integer value ) {
                                    return "<b>" + value + "</b>";
                                }   
                            })     
                            .arg( "formats", 
                                arg( "xls", new Formatter<Integer>(){
                                    public Object format( Integer value ) {
                                        return value;
                                    }   
                                })
                            )     
                        ))           
                        .arg( "width", "5%" ) 
                        .arg( "attributes.xml.attribute", true )
                    )
                    ,new GridColumn(
                        arg( "title", "Nascita" )
                        .arg( "name", "birth" )
                        .arg( "source", 
                            arg( "property", "birth" )
                            .arg( "formats", 
                                arg( "html,csv", new DateFormatter( "dd MMM yyyy" ) ) 
                                .arg( "xml", new DateFormatter( "yyyy-MM-dd'T'hh:mm:ssZ" ) )
                                .arg( "xls", new Formatter(){
                                    public Object format( Object value ) {
                                        return arg( "value", value ).arg( "pattern", "dd/mm/yyyy" );
                                    }   
                                }) 
                            ) 
                        )           
                        .arg( "width", 100 )
                    )
                    ,new GridColumn(
                        arg( "source", new HTMLCell(
                            arg( "property", "id" )
                            .arg( "format", new ItemFormatter<Item>() {
                                public Object format( Item item ) {
                                    String icon = null;
                                    if( item.get( "!loading", false ) ) {
                                        icon = "loader";
                                    } else {
                                        icon = ( item.get( "id", 0 ) % 2 == 0 ) ? "settings" : "globe";   
                                    }
                                    String img = Strings.message( "<div class=\"click icon-16-{0}\"></div>", icon );
                                    return img;
                                }   
                            })      
                        ))          
                        .arg( "width", 18 )
                        .arg( "attributes", arg( "context", true ) )
                        .arg( "row-click", slot( "imageClick" ) ) 
                        .arg( "export", false )
                    )
                ))                      
            ))

            .arg( "top", new ToolBar(
                new Button(
                    arg( "caption", "Load second" )
                    .arg( "click", new Slot( this, "loadSecond" ) )
                )
                ,new Button(
                    arg( "caption", "Load first" )
                    .arg( "click", new Slot( this, "loadFirst" ) )
                    .arg( "attributes", arg( "zone", "right" ) )
                )
                ,new Button(
                    arg( "caption", "Deselect" )
                    .arg( "click", new Slot( this, "gridDeselect" ) )
                    .arg( "attributes", arg( "zone", "right" ) )
                )

                ,new Button(
                    arg( "caption", "No Headers" )
                    .arg( "click", new Slot( this, "toggleHeaders" ) )
                )

                ,new Button(
                    arg( "caption", "Dump" )
                    .arg( "click", new Slot( this, "dumpSelection" ) )
                )

                ,new Button(
                    arg( "caption", "CSV" )
                    .arg( "click", new Slot( this, "exportCsv" ) )
                )

                ,new Button(
                    arg( "caption", "XLSX" )
                    .arg( "click", new Slot( this, "exportExcel" ) )
                )

                ,new Button(
                    arg( "caption", "PDF" )
                    .arg( "click", new Slot( this, "exportPdf" ) )
                )

                ,new Button(
                    arg( "caption", "XML" )
                    .arg( "click", new Slot( this, "exportXml" ) )
                )

            ))

            .arg( "right", new GridLayout(
                new TextBox(
                    arg( "name", "txtId" )
                    .arg( "label", "ID")
                    .arg( "writable", false )
                    .arg( "value", "\"value\"" )
                    .arg( "attributes", arg( "reset", true ) ) 
                )
                ,new TextBox(
                    arg( "name", "txtName" )
                    .arg( "label", "Nome")
                    .arg( "value", "'value' & \"value\"" )
                    .arg( "attributes", arg( "reset", true ) )
                )
                ,new TextBox(
                    arg( "name", "txtSurname" )
                    .arg( "label", "Cognome")
                    .arg( "attributes", arg( "reset", true ) )
                )
                ,new TextBox(
                    arg( "name", "txtGuid" )
                    .arg( "label", "GUID")
                    .arg( "attributes", arg( "reset", true ) )
                )
                ,new DateTextBox(
                    arg( "name", "txtBirth" )
                    .arg( "label", "Nascita")
                    .arg( "attributes", arg( "reset", true ) )
                )
                ,new TextBox(
                    arg( "name", "txtColumn" )
                    .arg( "label", "Column")
                    .arg( "writable", false )
                    .arg( "attributes", arg( "reset", true ) )
                )
            ))
        ));
        
    }
    
    public void exportCsv( Event event ) {
        downloader.send(
            grid1.export( "csv" )
            .with( "stripHTML" )
            .to( files, arg( "name", "grid.csv" ) )
        );
    }

    public void exportExcel( Event event ) {
        downloader.send(
            grid1.export( "xls" )
            .with( "title", "Grid Export" )
            .to( files, arg( "name", "grid.xlsx" ) )
        );
    }

    public void exportPdf( Event event ) {
        downloader.send(
            grid1.export( "pdf" )
            .with( "debug" )
            .with( "css", "body { font-size: 10pt; }" )
            .with( "footer", "Ciccio buffo" )
            .to( files )
        );
    }

    public void exportXml( Event event ) {
        downloader.send(
            grid1.export( "xml" )
            .with( "root", "grid" )
            .with( "namespace", "http://www.neverworks.it/grid" )
            .to( files )
        );
    }
    
    public void gridSelect( SelectEvent<Grid> event ) {
        System.out.println( "old " + event.getOld() + " new " + event.getNew() + " selected " + get( "grid1.selected" ) );

        if( event.get( "deselected" ) != null ) {
            event.set( "deselected.name",    get( "txtName.value" ) );
            event.set( "deselected.surname", get( "txtSurname.value" ) );
            event.set( "deselected.guid",    get( "txtGuid.value" ) );
            event.set( "deselected.birth",   get( "txtBirth.value" ) );
            grid1.touch( event.get( "deselected" ) );
        }
        
        if( event.get( "selected" ) != null ) {
            set( "txtId.value",      event.get( "selected.id" ) );
            set( "txtName.value",    event.get( "selected.name" ) );
            set( "txtSurname.value", event.get( "selected.surname" ) );
            set( "txtGuid.value",    event.get( "selected.guid" ) );
            set( "txtBirth.value",   event.get( "selected.birth" ) );
            set( "txtColumn.value",  event.get( "column.title" ) );
            
            printm( "Formatted: {0} {1} {2}\n"
                ,grid1.column( 2 ).format( event.get( "selected" ) ) 
                ,grid1.column( "age" ).format( event.get( "selected" ) ) 
                ,grid1.column( "birth" ).format( event.get( "selected" ) ) 
            );

        } else {
            set( "txtId.value",      null );
            set( "txtName.value",    null );
            set( "txtSurname.value", null );
            set( "txtGuid.value",    null );
            set( "txtBirth.value",   null );
            set( "txtColumn.value",  null );
        }
    }

    public void dumpSelection( Event event ) {
        //for( Item item: this.<Item>query( "grid1.items" ).by( "selected" ).eq( true ) ) {
        println( "Collection1:", get( "grid1.attributes.selection1" ) );
        for( Item item: value( "grid1.attributes.selection1" ).each( Item.class ) ) {
            println( "Selected:", item.get( "name" ) );
        }

        for( Item item: value( "grid1.items" ).query( Item.class ).by( "selected" ).eq( true ) ) {
            println( "Item:", item.get( "name" ) );
        }
        
        System.out.println( "Collection2: " + get( "grid1.attributes.selection2" ) );
    }
    
    public void gridDeselect( Event event ) {
        set( "grid1.selected", null );
    }
    
    public void toggleHeaders( Event event ) {
        if( event.get( "source.caption" ).equals( "No Headers" ) ) {
            set( "grid1.columns.visible", false );
            event.set( "source.caption", "Headers" );
        } else {
            set( "grid1.header.visible", true );
            event.set( "source.caption", "No Headers" );
        }
    }
    
    public void loadFirst( Event event ) {
        set( "grid1.items", dataSource.wrap( items1 ) );
        resetFields();
    }

    public void loadSecond( Event event ) {
        set( "grid1.items", dataSource.wrap( items2 ) );
        resetFields();
    }

    protected void resetFields() {
        for( ResetCapable widget: query( ResetCapable.class ).by( "attributes.reset" ).eq( true ) ) {
            widget.reset();
        }
    }
    
    public void gridClick( Event event ) {
        System.out.println( "Click on " + event.get( "column.title" ) + ": " + event.get( "!item.name" ) );
        System.out.println( "Click source: " + event.getSource() );
    }

    public void imageClick( Event event ) {
        System.out.println( "ImageClick source: " + event.getSource() );
        gridInspect( event );
        event.stop();
    }
    
    public void gridInspect( Event event ) {
        System.out.println( "Column " + event.get( "column" ) );
        String itemName = event.get( "item.name" );
        event.set( "item.loading", true );
        value( "grid1.touch" ).call( event.get( "item" ) );
        // grid1.touch( event.get( "item" ) );
        
        add( new Menu(
            arg( "name", "gridMenu" )
            .arg( "opened", true )
            .arg( "autoDestroy", true )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "attributes", arg( "item", event.get( "item" ) ) )
            .arg( "click", slot( "gridMenuClick" ) )
            .arg( "close", slot( "gridMenuClose" ) )
            .arg( "children", list(
                new MenuItem(
                    arg( "caption", "Operation 1 on " + itemName )
                )
                ,new MenuItem(
                    arg( "caption", "Operation 2 on " + itemName )
                )
                ,new MenuItem(
                    arg( "caption", "Operation 3 on " + itemName )
                )
            ))
        ));
    }

    public void nameInspect( Event event ) {
        add( new Menu(
            arg( "name", "nameMenu" )
            .arg( "opened", true )
            .arg( "autoDestroy", true )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "attributes", arg( "item", event.get( "item" ) ) )
            .arg( "click", slot( "gridMenuClick" ) )
            .arg( "children", list(
                new MenuItem(
                    arg( "caption", "Name Operation 1")
                )
                ,new MenuItem(
                    arg( "caption", "Name Operation 2" )
                )
                ,new MenuItem(
                    arg( "caption", "Name Operation 3" )
                )
            ))
        ));
    }

    
    public void gridMenuClick( Event event ) {
        add( new MessageBox(
            arg( "message", Strings.message( "Clicked {0}, with surname: {1}", event.get( "item.caption" ), event.get( "source.attributes.item.surname" ) ) )
        ));
    }

    public void gridMenuClose( Event event ) {
        event.set( "source.attributes.item.loading", false );
        grid1.touch( event.get( "source.attributes.item" ) );
    }
    
    public void columnInspect( Event event ) {
        String columnName = event.get( "column.title" );
        add( new Menu(
            arg( "name", "columnMenu" )
            .arg( "opened", true )
            .arg( "autoDestroy", true )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "attributes", arg( "columns", event.get( "source" ) ) )
            .arg( "click", slot( "columnMenuClick" ) )
            .arg( "children", list(
                new MenuItem(
                    arg( "caption", "Operation 1 on column " + columnName )
                )
                ,new MenuItem(
                    arg( "caption", "Operation 2 on column " + columnName )
                )
                ,new MenuItem(
                    arg( "caption", "Operation 3 on column " + columnName )
                )
            ))
        ));
    }

    public void columnMenuClick( Event event ) {
        add( new MessageBox(
            arg( "message", Strings.message( "Clicked {0}", event.get( "item.caption" ) ) )
        ));
    }
    
}
