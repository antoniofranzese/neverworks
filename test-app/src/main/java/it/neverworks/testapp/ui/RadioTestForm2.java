package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.RadioGroup;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;

public class RadioTestForm2 extends Form {
    
	public void build()
	{
		
		set("coordPuntoMain", new BorderLayout(
			 arg("gutters", true)
			 .arg("top", new Panel( 
                 arg("name","coordSysPuntoPanel")
    			 .arg("height", "20%")
    			 .arg( "content", new RadioGroup()
    					.set( "name", "coordPuntoSys" )
    					.set( "label", T("Sistema") )
    					.set( "key", "id" )
    					.set( "caption", "name" )
    					.set( "arrangement", "rows" )
    					.set( "direction", "horizontal" )
    					.set( "items", list(
    						 new Item( arg( "id", 1 ).arg( "name", "LatLon" ) ) 
    						 ,new Item( arg( "id", 2 ).arg( "name", "MGRS" ) )
    						 ,new Item( arg( "id", 3 ).arg( "name", "UTM" ) )
					 
    					))
    					.set( "change", slot( "selectChange" ) )
    					.set( "value", 1 )
				)
			 ))
            .arg("center", new Panel( 
                arg("name", "coordPuntoInput") 
			))
		));
		
	}
	
	public void selectChange( ChangeEvent<RadioGroup> event ) 
	{
        System.out.println(  "Changed from " + event.getOld() + " to " + event.getNew() );
        String selected = get( "coordPuntoSys.item.name" );
    }

}