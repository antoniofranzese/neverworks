package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.ValueFormatter;

public class OrientationTestForm extends Form {
    
    public void build() {
        set( "main", new BorderLayout(
            arg( "font.size", 20 )
            .arg( "gutters", true )
            .arg( "lefts", list(
                new Panel(
                    arg( "name", "left1" )
                    .arg( "content", new Label(
                        arg( "value", "left 1" )
                    ))
                    .arg( "width", "100px" )
                    .arg( "layout.portrait.visible", false )
                )
                ,new Panel(
                    arg( "content", new Label(
                        arg( "value", "left 2" )
                    ))
                    .arg( "width", "100px" )
                )
            ))

            .arg( "top", new Panel(
                arg( "content", new Label(
                    arg( "value", "top" )
                ))
                .arg( "height", "100px" )
                .arg( "layout.landscape", false )
            ))

            .arg( "bottom", new Panel(
                arg( "content", new Label(
                    arg( "value", "bottom" )
                ))
                .arg( "height", "100px" )
                .arg( "layout.landscape", false )
            ))

            .arg( "center", new Panel(
                arg( "content", new StackLayout(
                    new Panel(
                        arg( "content", new Label(
                            arg( "value", "PORTRAIT CENTER" )
                        ))
                        .arg( "layout.portrait", true )
                    )
                    ,new Panel(
                        arg( "content", new Label(
                            arg( "value", "LANDSCAPE CENTER" )
                        ))
                        .arg( "layout.landscape", true )
                    )
                ))
            ))
        ));

    }
    
    
}