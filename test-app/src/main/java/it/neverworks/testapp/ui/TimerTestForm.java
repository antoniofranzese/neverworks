package it.neverworks.testapp.ui;

import java.util.Date;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Timer;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.Formatter;

public class TimerTestForm extends Form {
    
    private final static Formatter TIMESTAMP = new Formatter(){
       public Object format( Object value ) {
           return Strings.message( "{0,date,dd/MM/yyyy HH:mm:ss}", value );
       }
   };
    
    public void build() {
        set( "main", new GridLayout(
            new TextBox()
                .set( "name", "manualTime" )
                .set( "label", "Manual" )
                .set( "writable", false )
                .set( "format", TIMESTAMP )

            ,new TextBox()
               .set( "name", "tickTime" )
               .set( "label", "Tick" )
               .set( "writable", false )
               .set( "format", TIMESTAMP )
            
            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )    
                ,new Button()
                    .set( "caption", "Start" )
                    .set( "click", slot( "startTimer" ) )
                ,new Button()
                    .set( "caption", "Start 5" )
                    .set( "click", slot( "startTimer5" ) )
                ,new Button()
                    .set( "caption", "Stop" )
                    .set( "click", slot( "stopTimer" ) )
                ,new Button()
                    .set( "caption", "Manual" )
                    .set( "click", slot( "manualTick" ) )
                ,new Button()
                    .set( "caption", "Timeout" )
                    .set( "click", slot( "timeoutRequest" ) )
            )

        ));
        
        set( "timer", new Timer()
            .set( "interval", 2 ) 
            .set( "tick", slot( "timerTick" ) )
        );

    }
    
    public void startTimer( Event event ) {
        value( "timer.start" ).call( -1 );
    }

    public void startTimer5( Event event ) {
        value( "timer.start" ).call( 5 );
    }

    public void stopTimer( Event event ) {
        set( "timer.running", false );
    }
    
    public void timerTick( Event event ) throws Exception{
        //Thread.sleep( 5000 );
        set( "tickTime.value", new Date() );
    }

    private boolean manualNext = false;
    
    public void manualTick( Event event ) throws Exception{
        Thread.sleep( 2500 );
        set( "manualTime.value", new Date() );
        if( ! manualNext ) {
            manualNext = true;
            next( slot( "manualTick" ) );
        } else {
            manualNext = false;
        }
    }

    public void timeoutRequest( Event event ) throws Exception{
        Thread.sleep( 20000 );
        set( "manualTime.value", new Date() );
    }
    
}