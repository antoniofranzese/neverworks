package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.SelectChangeEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.GridLayout;

public class SelectTestForm extends Form {
    
    @Property @Reference 
    private Select people;

    @Property @Reference 
    private Select people2;
    
    public void build() {
        set( "main", new GridLayout(
            new Select()
                .set( "name", "people" )
                .set( "label", "Persona" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", list(
                    new Item( arg( "id", 1L ).arg( "name", "\"Name 1\"" ).arg( "city", "City1" ).arg( "age", 10 ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "'Name 2'" ).arg( "city", "City2" ).arg( "age", 20 ) )
                    ,new Item( arg( "id", 3L ).arg( "name", "Namé 3" ).arg( "city", "City3" ).arg( "age", 30 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Name 4" ).arg( "city", "City4" ).arg( "age", 40 ) )
                    ,new Item( arg( "id", 5L ).arg( "name", "Name 5" ).arg( "city", "City5" ).arg( "age", 50 ) )
                ))
                .set( "change", new Slot( this, "selectChange" ) )

            ,new TextBox()
                .set( "name", "txtCity" )
                .set( "label", "Città" )

            ,new TextBox()
                .set( "name", "txtAge" )
                .set( "label", "Età" )

            ,new Select()
                .set( "name", "blankablePeople" )
                .set( "label", "Persona (blankable)" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "blank", true )
                .set( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Name1" ).arg( "city", "City1" ).arg( "age", 10 ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Name2" ).arg( "city", "City2" ).arg( "age", 20 ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Name3" ).arg( "city", "City3" ).arg( "age", 30 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Name4" ).arg( "city", "City4" ).arg( "age", 40 ) )
                    ,new Item( arg( "id", 5 ).arg( "name", "Name5" ).arg( "city", "City5" ).arg( "age", 50 ) )
                ))
                .set( "change", new Slot( this, "selectChange2" ) )

            ,new TextBox()
                .set( "name", "txtCity2" )
                .set( "label", "Città" )

            ,new TextBox()
                .set( "name", "txtAge2" )
                .set( "label", "Età" )
                
            ,new Select()
                .set( "name", "people2" )
                .set( "label", "Disabilitato" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", list(
                    new Item( arg( "id", 1L ).arg( "name", "Name1" ).arg( "city", "City1" ).arg( "age", 10 ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Name2" ).arg( "city", "City2" ).arg( "age", 20 ) )
                    ,new Item( arg( "id", 3L ).arg( "name", "Name3" ).arg( "city", "City3" ).arg( "age", 30 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Name4" ).arg( "city", "City4" ).arg( "age", 40 ) )
                    ,new Item( arg( "id", 5L ).arg( "name", "Name5" ).arg( "city", "City5" ).arg( "age", 50 ) )
                ))
                .set( "enabled", false )
            ,new Select()
                .set( "name", "people3" )
                .set( "label", "Readonly" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", list(
                    new Item( arg( "id", 1L ).arg( "name", "Name1" ).arg( "city", "City1" ).arg( "age", 10 ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Name2" ).arg( "city", "City2" ).arg( "age", 20 ) )
                    ,new Item( arg( "id", 3L ).arg( "name", "Name3" ).arg( "city", "City3" ).arg( "age", 30 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Name4" ).arg( "city", "City4" ).arg( "age", 40 ) )
                    ,new Item( arg( "id", 5L ).arg( "name", "Name5" ).arg( "city", "City5" ).arg( "age", 50 ) )
                ))
                .set( "writable", false )
            
                
            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button()
                    .set( "caption", "Enable" )
                    .set( "click", slot( "toggle" ) )
                ,new Button()
                    .set( "caption", "Readonly" )
                    .set( "click", slot( "toggleReadonly" ) )
                ,new Button()
                    .set( "caption", "Select 3" )
                    .set( "click", slot( "select3" ) )
                ,new Button()
                    .set( "caption", "Deselect" )
                    .set( "click", slot( "deselect" ) )
            )
            
        ));
    }

    public void select3( Event event ) {
        set( "people.value", 3 );
        set( "blankablePeople.value", 3 );
        set( "people2.value", 3 );
    }
    
    public void deselect( Event event ) {
        set( "people.value", null );
        set( "blankablePeople.value", null );
        set( "people2.value", null );
    }
    
    public void selectChange( SelectChangeEvent<Select> event ) {
        System.out.println( "Changed from " + event.<Long>get( "!oldItem.id" ) + " to " + event.<Long>get( "!newItem.id" ) );
        System.out.println( "Item Changed from " + event.getOldItem() + " to " + event.getNewItem() );
        System.out.println( "Value Changed from " + event.getOld() + " " + Objects.className( event.getOld() ) 
            + " to " + event.getNew() + " " + Objects.className( event.getNew() ) 
            + " select value " + event.getSource().getValue() + " " + Objects.className( event.getSource().getValue() ) );
            
        if( get( "people.selected" ) != null ) {
            set( "txtCity.value", get( "people.selected.city" ) );
            set( "txtAge.value", get( "people.selected.age" ) );
        } else {
            set( "txtCity.value", null );
            set( "txtAge.value", null );
        }
    }

    public void selectChange2( ChangeEvent<Select> event ) {
        System.out.println( "Changed from " + event.getOld() + " to " + event.getNew() + ", selected: " + get( "blankablePeople.selected" ) );
        if( event.getNew() == null ) {
            set( "txtCity2.value", "<blank>" );
            set( "txtAge2.value", "<blank>" );
        } else {
            set( "txtCity2.value", get( "blankablePeople.selected.city" ) );
            set( "txtAge2.value", get( "blankablePeople.selected.age" ) );
        }
    }

    public void toggle( Event event ) {
        boolean enable = ! people2.getEnabled();
        people.setEnabled( enable );
        people2.setEnabled( enable );
        event.set( "source.caption", enable ? "Disable" : "Enable" );
    }
    
    public void toggleReadonly( Event event ) {
        Boolean readonly = get( "people3.readonly" );
        set( "people.readonly", readonly );
        set( "blankablePeople.readonly", readonly );
        set( "people3.writable", readonly );
        event.set( "source.caption", readonly ? "Writable" : "Readonly" );
    }
}