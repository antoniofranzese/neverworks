package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.List;

import it.neverworks.ui.Container;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.RadioGroup;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Item;

public class CosmoTestForm extends Form {
    
    public void build() {
        List<String> questions = list(
            "Qual è la sua opinione sulla qualità generale del supporto che ha ricevuto dal nostro Servizio Clienti?"
            ,"Qual è la sua opinione sulla qualità del supporto che ha ricevuto per quello che riguarda l'assistenza alla Programmazione di nuove acquisizioni?"
            ,"Qual è la sua opinione sulla qualità del supporto che ha ricevuto per quello che riguarda l'assistenza Tecnica?"
            ,"Qual è la sua opinione sul tempo di risposta del Servizio Clienti?"
            ,"Qual è la sua opinione sul tempo di risposta del Servizio per l'assistenza alle Programmazioni?"
            ,"Qual è la sua opinione sul tempo di risposta del Servizio per l'assistenza Tecnica?"         
        );

        List<Item> answers = list(
             new Item( arg( "key",  0 ).arg( "caption", "Inaccettabile" ) )
            ,new Item( arg( "key",  1 ).arg( "caption", "Scarso" ) )
            ,new Item( arg( "key",  2 ).arg( "caption", "Accettabile" ) )
            ,new Item( arg( "key",  3 ).arg( "caption", "Buono" ) )
            ,new Item( arg( "key",  4 ).arg( "caption", "Ottimo" ) )
            ,new Item( arg( "key", -1 ).arg( "caption", "Non ho utilizzato il servizio" ) )
        );
        
        GridLayout main = new GridLayout(
           new Label().set( "value", "" )
        ).set( "columns", 7 );
        
        // Intestazione
        for( Item item: answers ) {
            main.add( new Label()
                .set( "value", item.get( "caption" ) ) 
                .set( "width", 85 )
                .set( "height", 50 )
                .set( "halign", "center" )
                .set( "valign", "middle" )
            );
        }
    
        add( main );

        // Domande
        for( int i = 0; i < questions.size(); i++ ) {
            
            // Widget risposta
            RadioGroup group = new RadioGroup()
               .set( "name", "question" + i )
               .set( "columns", 6 )
               .set( "items", answers );
            
            add( group );
            
            // Testo della domanda       
            main.add(
               new Label()
                   .set( "value", questions.get( i ) )
                   .set( "width", 350 )
                   .set( "height", 50 )
            );
            
            // Risposte
            for( int btn = 0; btn < answers.size(); btn++ ) {
                main.add(
                    group.button( answers.get( btn ).get( "key" ) )
                        .set( "layout", arg( "halign", "center" ).arg( "valign", "middle" ) )
                );
            }
        }
        
    }
    
}
