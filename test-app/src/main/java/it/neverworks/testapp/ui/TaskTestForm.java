package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.Date;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.NumberTextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.dialogs.TaskBox;

public class TaskTestForm extends Form {
    
    public void build() {
        add( new GridLayout(
            new NumberTextBox(
                arg( "label", "Duration" )
                .arg( "name", "durationBox")
                .arg( "value", 5000 )
            )
            ,new TextBox(
                arg( "label", "Last" )
                .arg( "name", "lastBox")
            )
            ,new ToolBar(
                arg( "layout.hspan", "full" )
                ,new Button(
                    arg( "caption", "Start" )
                    .arg( "click", slot( evt -> {
                        add( new TaskBox(
                            arg( "message", "Waiting for task..." )
                            .arg( "interval", 1000 )

                            .arg( "task", fork( task -> {
                                sleep( get( "durationBox.value" ) );
                                task.set( "attributes.result", new Date() );
                                if( Int( get( "durationBox.value" ) ) > 5000 ) {
                                    throw new RuntimeException( "TOO MUCH" );
                                }
                            }))

                            .arg( "poll", slot( task -> {
                                task.set( "source.message", msg( "Wait: {0,time}", new Date() ) );
                            }))
 
                            .arg( "complete", slot( task -> {
                                set( "lastBox.value", task.get( "attributes.result" ) );
                            }))

                            .arg( "reject", slot( task -> {
                                set( "lastBox.value", task.get( "error.message" ) );
                            }))
                        ));
                    }))
                )
            )
        ));
    }
 }