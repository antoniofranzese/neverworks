package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.events.Event;
import it.neverworks.model.context.Inject;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Image;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.data.Catalog;

public class ImageTestForm extends Form {
    
    @Property @Inject( "IconCatalog" )
    private Catalog icons;
    
    @Property @Inject( "ImageCatalog" )
    private Catalog images;
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 3 )
            ,new Select()
                .set( "label", "Icon" )
                .set( "change", slot( "changeIcon" ) )
                .set( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "upload" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "globe" ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "home" ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "nas" ) )
                    ,new Item( arg( "id", 5 ).arg( "name", "tag" ) )
                ))
                
            ,new Image()
                .set( "name", "iconImage" )
                
            ,new Select()
                .set( "label", "File" )
                .set( "change", slot( "changeFile" ) )
                .set( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "load1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "load2" ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "load3" ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "load4" ) )
                ))
            
            ,new Image()
                .set( "name", "fileImage" )
                
            ,new Select()
                .set( "label", "Mixed" )
                .set( "change", slot( "changeMixed" ) )
                .set( "items", list(
                     new Item( arg( "id", 1 ).arg( "name", "File load2" ).arg( "artifact", "load2.gif" ).arg( "type", "file" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "File load4" ).arg( "artifact", "load4.gif" ).arg( "type", "file" ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Icon home" ).arg( "artifact", "home@32" ).arg( "type", "icon" ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Icon nas" ).arg( "artifact", "nas@32" ).arg( "type", "icon" ) )
                    ,new Item( arg( "id", 5 ).arg( "name", "Icon tag" ).arg( "artifact", "tag@32" ).arg( "type", "icon" ) )
                ))
        
            ,new Image()
                .set( "name", "mixedImage" )
            
            
        ));
    }
    
    public void changeIcon( Event event ) {
        set( "iconImage.source", getIcons().get( event.<String>get( "source.selected.name" ) + "@32" ) );
    }

    public void changeFile( Event event ) {
        set( "fileImage.source", getImages().get( event.<String>get( "source.selected.name" ) + ".gif" ) );
    }

    public void changeMixed( Event event ) {
        String artifact = event.get( "source.selected.artifact" );
        set( "mixedImage.source", "file".equals( event.get( "source.selected.type" ) ) ? getImages().get( artifact ) : getIcons().get( artifact ) );
    }

 
    public Catalog getImages(){
        return this.images;
    }
    
    public void setImages( Catalog images ){
        this.images = images;
    }

    public Catalog getIcons(){
        return this.icons;
    }
    
    public void setIcons( Catalog icons ){
        this.icons = icons;
    }
    
}