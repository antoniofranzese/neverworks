package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.lang.Application;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.data.RawFileArtifact;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.web.PDFViewer;
import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;

public class LabelTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 1 ) 
            ,new Label()
                .set( "name", "html" )
                //.set( "value", Application.resource( this, "intro.html" ) )
                .set( "width", 500 )
                .set( "height", 200 )
                .set( "encoding", "html" )
                .set( "font.height", "150%" )
                .set( "link", slot( evt -> {
                    add( new PDFView()
                        .store( "pdf.source", new RawFileArtifact( "http://www.mit.gov.it/sites/default/files/media/normativa/2017-03/Decreto%20ministeriale%20numero%2065%20del%2007-03-2017.pdf" ) )
                    );
                }))
            ,new Grid(
                arg( "key", "[0]" )
                .arg( "columns", range( 1, 6 ).map( i -> new GridColumn( arg( "source", msg( "[{0}]", i ) ) ) ) )
                .arg( "items", list(
                    list( 1, 11, 12, 13, 14, 15 )
                    ,list( 2, 21, 22, 23, 24, 25 )
                    ,list( 3, 31, 32, 33, 34, 35 )
                    ,list( 4, 41, 42, 43, 44, 45 )
                    ,list( 5, 51, 52, 53, 54, 55 )
                ))
            )
        ));
    }
    
    public class PDFView extends Popup {

        public void build() {
            set( "width", 800 );
            set( "height", 600 );
            set( "opened", true );
            set( "autoDestroy", true );
            set( "resizable", true );

            add( new BorderLayout(
                arg( "center", new PDFViewer(
                    arg( "name", "pdf" )
                ))
            ));
        }
    }
}