package it.neverworks.testapp.ui;

import java.util.Map;
import it.neverworks.model.converters.Converter;

public class MapCoordinatesConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof MapCoordinates ) {
            return value;
        } else if( value instanceof Map ) {
            Map map = (Map) value;
            return new MapCoordinates()
                .set( "latitude", map.get( "latitude" ) )
                .set( "longitude", map.get( "longitude" ) );
        } else {
            return value;
        }
    }    

}