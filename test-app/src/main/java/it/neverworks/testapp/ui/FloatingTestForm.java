package it.neverworks.testapp.ui;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.model.Property;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.FloatingLayout;
import it.neverworks.ui.layout.CloseEvent;
import it.neverworks.ui.layout.SelectEvent;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.data.Item;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;

public class FloatingTestForm extends Form {
    
    private int count = 1;
    
    @Property @Reference
    private FloatingLayout pages;
    
    
    @Override
    public void build() {
        set( "title", "Floating Test" );
        set( "main", new BorderLayout(
            arg( "gutters", true )
            // .arg( "top", new Panel()
            //     .set( "height", 100 )
            //     .set( "content", new Label().set( "value", "Top" ) )
            // )

            .arg( "center", new FloatingLayout(
                arg( "name", "pages" )
                .arg( "close", slot( "closePage" ) )
                .arg( "border.bottom.size", 0 )
                .arg( "border.left.size", 0 )
                .arg( "border.right.size", 0 )
                    
                // ,new AddressForm()
                //     .set( "name", "mainAddressForm" )
                //     .set( "title", "Form Page" )
                //
                ,new Panel(
                    arg( "title", "Panel Page" )
                    .arg( "closable", true )
                    .arg( "layout", 
                        arg( "resizable", true )
                        .arg( "width", 500 )
                        .arg( "height", 300 )
                        .arg( "minWidth", 500 )
                        .arg( "min-height", 300 )
                    )
                    .arg( "name", "panelPage" )
                    .arg( "padding", 10 )
                )

                // ,new Panel(
                //     arg( "title", "Panel 2" )
                //     .arg( "name", "panel2" )
                //     .arg( "closable", true )
                //     .arg( "content", new GridLayout(
                //         new TextBox()
                //             .set( "name", "txt1G" )
                //             .set( "label", "Campo 1" )
                //             .set( "value", "val1")
                //
                //         ,new TextBox()
                //             .set( "name", "txt2G" )
                //             .set( "label", "Campo 2" )
                //             .set( "value", "val2")
                //     ))
                // )

                // ,new GridLayout(
                //     arg( "name", "gridLayout")
                //     .arg( "columns", 2 )
                //     .arg( "padding", 5 )
                //     .arg( "layout", arg( "title", "Simple GridLayout" ) )
                //     ,new TextBox()
                //         .set( "name", "txtFather" )
                //         .set( "label", "Padre" )
                //     ,new TextBox()
                //         .set( "name", "txtMother")
                //         .set( "label", "Madre" )
                //     ,new ToolBar(
                //         arg( "name", "gridLayoutButtons" )
                //         .arg( "layout",
                //             arg( "hspan", "full" )
                //             .arg( "halign", "right" )
                //         )
                //         ,new Button()
                //             .set( "caption", "Scambia")
                //             .set( "click", slot( "swapClick" ) )
                //         ,new Button()
                //             .set( "caption", "Switch")
                //             .set( "click", slot( "switchClick" ) )
                //
                //     )
                // )
            ))

            .arg( "bottom", new GridLayout(
                arg( "columns", 1 )

                ,new ToolBar(
                    new Button(
                        arg( "caption", "Load Panel Page" )
                        .arg( "click", slot( "loadPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Blank Panel Page" )
                        .arg( "click", slot( "blankPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select Panel Page" )
                        .arg( "click", slot( "selectPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select Panel 2" )
                        .arg( "click", slot( "selectPage3" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add start" )
                        .arg( "click", slot( "addStart" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add&select" )
                        .arg( "click", slot( "addAndSelect" ) )
                    )
                    ,new Button(
                        arg( "caption", "Remove first" )
                        .arg( "click", slot( "removeFirst" ) )
                    )
                    ,new Button(
                        arg( "caption", "Rename first 2" )
                        .arg( "click", slot( "renameFirsts" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select first" )
                        .arg( "click", slot( "selectFirst" ) )
                    )
                )

                ,new ToolBar(
                    new Button(
                        arg( "caption", "Add rigid complex" )
                        .arg( "click", slot( "addComplex" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add sizable complex" )
                        .arg( "click", slot( "addResizableComplex" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add form complex" )
                        .arg( "click", slot( "addFormComplex" ) )
                    )
                )

                ,new GridLayout(
                    arg( "columns", 99 )
                    ,new Button()
                        .set( "name", "selectOnButton" )
                        .set( "caption", "Select on" )
                        .set( "click", slot( "selectOn" ) )
                    ,new Button()
                        .set( "name", "selectOffButton" )
                        .set( "caption", "Select off" )
                        .set( "click", slot( "selectOff" ) )
                        .set( "enabled", false )
                    ,new TextBox()
                        .set( "name", "currentPageName" )
                        .set( "writable", false )

                    ,new Button()
                        .set( "name", "selectingOnButton" )
                        .set( "caption", "Selecting on" )
                        .set( "click", slot( "selectingOn" ) )
                    ,new Button()
                        .set( "name", "selectingOffButton" )
                        .set( "caption", "Selecting off" )
                        .set( "enabled", false )
                        .set( "click", slot( "selectingOff" ) )

                    ,new Button()
                        .set( "name", "closingOnButton" )
                        .set( "caption", "Closing on" )
                        .set( "click", slot( "closingOn" ) )
                    ,new Button()
                        .set( "name", "closingOffButton" )
                        .set( "caption", "Closing off" )
                        .set( "enabled", false )
                        .set( "click", slot( "closingOff" ) )
                )

            ))        

        ));

        //pages = get( "pages" );
        set( "pages.selected", get( "pages.last" ) );
    }

    public void selectFirst( Event event ) {
        set( "pages.selected", get( "pages.first" ) );
    }
    
    public void selectOn( Event event ) {
        if( event( "pages.select" ).size() == 0 ) {
            event( "pages.select" ).add( slot( "onSelect" ) );
        }
        set( "selectOnButton.enabled", false );
        set( "selectOffButton.enabled", true );
    }
    
    public void selectOff( Event event ) {
        if( event( "pages.select" ).size() > 0 ) {
            event( "pages.select" ).remove( slot( "onSelect" ) );
        }
        set( "selectOnButton.enabled", true );
        set( "selectOffButton.enabled", false );
    }
    
    public void onSelect( SelectEvent event ) {
        set( "currentPageName.value", event.getSelected().get( "name" ) );
    }

    public void selectingOn( Event event ) {
        if( event( "pages.selecting" ).size() == 0 ) {
            event( "pages.selecting" ).add( slot( "onSelecting" ) );
        }
        set( "selectingOnButton.enabled", false );
        set( "selectingOffButton.enabled", true );
    }
    
    public void selectingOff( Event event ) {
        if( event( "pages.selecting" ).size() > 0 ) {
            event( "pages.selecting" ).remove( slot( "onSelecting" ) );
        }
        set( "selectingOnButton.enabled", true );
        set( "selectingOffButton.enabled", false );
    }

    public void onSelecting( SelectEvent event ) {
        System.out.println( "Selecting " + event.getSelected().getCommonName() );
        if( event.getSelected() == widget( "panelPage" ) ) {
            add( new MessageBox( "Non puoi selezionare Panel Page" ) );
        } else {
            event.select();
            // oppure: pages.select( event.getSelected() );
        }
    }

    public void closingOn( Event event ) {
        if( event( "pages.closing" ).size() == 0 ) {
            event( "pages.closing" ).add( slot( "onClosing" ) );
        }
        set( "closingOnButton.enabled", false );
        set( "closingOffButton.enabled", true );
    }
    
    public void closingOff( Event event ) {
        if( event( "pages.closing" ).size() > 0 ) {
            event( "pages.closing" ).remove( slot( "onClosing" ) );
        }
        set( "closingOnButton.enabled", true );
        set( "closingOffButton.enabled", false );
    }
    
    public void onClosing( CloseEvent event ) {
        System.out.println( "Closing " + event.getClosed().getCommonName() );
        if( event.getClosed() == widget( "panelPage" ) ) {
            add( new MessageBox( "Non puoi chiudere Panel Page" ) );
        } else {
            event.close();
            // oppure: pages.close( event.getClosed() );
        }
    }

    public void loadPanelPage( Event event ) {
        set( "panelPage.content", new TestForm() );
    }
    
    public void blankPanelPage( Event event ) {
        set( "panelPage.content", null );
    }
    
    public void selectPanelPage( Event event ) {
        set( "pages.selected", get( "pages.panelPage" ) );
    }
    
    public void selectPage3( Event event ) {
        set( "pages.selected", get( "pages.panel2" ) );
    }
    
    public void addAndSelect( Event event ) {
        addStart( event );
        set( "pages.selected", get( "pages[0]" ) );
    }

    public void addStart( Event event ) {
        pages.place( newPage( "start" ) ).first();
        set( "pages.first.content.txtAddress.label.font.weight", "bold" );
        set( "pages.first.title", "Added" );
    }

    public void addEnd( Event event ) {
        pages.place( newPage( "end" ) ).last();
    }
    
    private Widget newPage( String position ) {
        count++;
        return new Panel(
            arg( "name", "newPanel" + count )
            .arg( "title", "New Page " + count + " at " + position )
            .arg( "closable", true )
            .arg( "content", new AddressForm().set( "name", "addressForm" + count ) )
        );
    }
    
    public void removeFirst( Event event ) {
        pages.remove( 0 );
    }
    
    public void closePage( CloseEvent event ) {
        System.out.println( "Close " + event.getClosed().getCommonName() + ", size after close " + pages.size() );
    }

    public void swapClick( ClickEvent<Button> event ) {
        String value = get( "txtMother.value" );
        set( "txtMother.value", get( "txtFather.value" ) );
        set( "txtFather.value", value );
    }
    
    public void switchClick( ClickEvent event ) {
        if( this.<Integer>get( "gridLayout.columns" ) == 2 ) {
            set( "gridLayout.columns", 4 );
        } else {
            set( "gridLayout.columns", 2 );
        }
    }
    
    public void renameFirsts( Event event ) {
        if( pages.size() > 0 ) {
            set( "pages.children[0].title", "Renamed first" );
        }

        if( pages.size() > 1 ) {
            set( "pages.children[1].title", "Renamed second" );
        }
    }
    
    public void addComplex( Event event ) {
        pages.add( createComplexWidget() );
        pages.select( pages.getLast() );
    }

    public void addResizableComplex( Event event ) {
        pages.add( createComplexWidget().set( "layout.resizable", true ).set( "title", "Sizable Complex Layout" ) );
        pages.select( pages.getLast() );
    }

    public void addFormComplex( Event event ) {
        pages.add( new ComplexForm()
            .set( "layout.closable", true )
            .set( "layout.resizable", true )
            .set( "layout.width", 500 )
            .set( "layout.height", 300 ) 
        );
        pages.select( pages.getLast() );
    }
    
    private Widget createComplexWidget() {
        return new BorderLayout(
            arg( "gutters", true )
            .arg( "title", "Complex layout")
            .arg( "layout", arg( "width", 500 ).arg( "height", 300 ).arg( "closable", true ) )
            .arg( "top", new Panel()
                .set( "height", 50 )
            )
            .arg( "left", new Tree(
                arg( "width", 140 )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "expanded", true )
                .arg( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Item 1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ).arg( "parent", 1 ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", 2 ) )
                    ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ).arg( "parent", 1 ) )
                    ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ).arg( "parent", 5 ) )
                    ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 5 ) )
                    ,new Item( arg( "id", 8 ).arg( "name", "Item 8" ).arg( "parent", 7 ) )
                    ,new Item( arg( "id", 9 ).arg( "name", "Item 9" ).arg( "parent", 7 ) )
                    ,new Item( arg( "id", 10 ).arg( "name", "Item 10" ).arg( "parent", 5 ) )
                    
                ))
            ))
            .arg( "center", new Grid(
                arg( "preload", false )
                .arg( "columns", list(
                     new GridColumn()
                         .set( "title", "Column 1" )
                         .set( "source", "prop1" )
                         .set( "width", "34%" )
                     ,new GridColumn()
                         .set( "title", "Column 2" )
                         .set( "source", "prop2" )
                         .set( "width", "33%" )
                     ,new GridColumn()
                         .set( "title", "Column 3" )
                         .set( "source", "prop3" )
                         .set( "width", "33%" )
                 ))
                 .arg( "items", list(
                     new Item( arg( "id", 1 ).arg( "prop1", "Property 1.1" ).arg( "prop2", "Property 2.1" ).arg( "prop3", "Property 3.1" ) )
                     ,new Item( arg( "id", 2 ).arg( "prop1", "Property 1.2" ).arg( "prop2", "Property 2.2" ).arg( "prop3", "Property 3.2" ) )
                     ,new Item( arg( "id", 3 ).arg( "prop1", "Property 1.3" ).arg( "prop2", "Property 2.3" ).arg( "prop3", "Property 3.3" ) )
                     ,new Item( arg( "id", 4 ).arg( "prop1", "Property 1.4" ).arg( "prop2", "Property 2.4" ).arg( "prop3", "Property 3.4" ) )
                     ,new Item( arg( "id", 5 ).arg( "prop1", "Property 1.5" ).arg( "prop2", "Property 2.5" ).arg( "prop3", "Property 3.5" ) )
                     ,new Item( arg( "id", 6 ).arg( "prop1", "Property 1.6" ).arg( "prop2", "Property 2.6" ).arg( "prop3", "Property 3.6" ) )
                     ,new Item( arg( "id", 7 ).arg( "prop1", "Property 1.7" ).arg( "prop2", "Property 2.7" ).arg( "prop3", "Property 3.7" ) )
                     ,new Item( arg( "id", 8 ).arg( "prop1", "Property 1.8" ).arg( "prop2", "Property 2.8" ).arg( "prop3", "Property 3.8" ) )
                     ,new Item( arg( "id", 9 ).arg( "prop1", "Property 1.9" ).arg( "prop2", "Property 2.9" ).arg( "prop3", "Property 3.9" ) )
                     ,new Item( arg( "id", 10 ).arg( "prop1", "Property 1.10" ).arg( "prop2", "Property 2.10" ).arg( "prop3", "Property 3.10" ) )
                 ))
            ))
        );
    }

    public class ComplexForm extends Form {
        public void build() {
            set( "title", "Sizable Complex Form" );
            add( createComplexWidget() );
        }
    }
    
}