package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ProgressBar;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.web.Uploader;
import it.neverworks.ui.web.Downloader;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;

public class NextTickForm extends Form {
    
    public class WaitPopup extends Popup {
        public void build() {
            set( "opened", true );
            set( "closable", false ); 
            set( "title", "Progress");
            set( "main", new GridLayout(
                arg( "columns", 1 )
                ,new Label()
                    .set( "name", "message" )
                    .set( "padding", 10 )
                    .set( "layout", arg( "halign", "center" ) )
                ,new ProgressBar()
                    .set( "name", "progress" )
                    .set( "value", 20 )
                    .set( "width", 200 )
                    
            ));
        }
    }
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 ) 
            
            ,new TextBox()
                .set( "label", "Field 1" )
                .set( "writable", false )

            ,new TextBox()
                .set( "label", "Field 2" )
                .set( "writable", false )

            ,new ProgressBar()
                .set( "name", "progress" )
                .set( "width", 400 )
                .set( "layout", 
                    arg( "hspan", "full" )
                )
                
            ,new ToolBar(
                arg( "name", "buttons" )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )

                ,new Button()
                    .set( "caption", "Progress")
                    .set( "click", slot( "progressClick" ) )

                ,new Button()
                    .set( "caption", "Do")
                    .set( "click", slot( "doClick" ) )

                ,new Button()
                    .set( "caption", "Switch & Do")
                    .set( "click", slot( "switchAndDoClick" ) )

                ,new Button()
                    .set( "caption", "Switch ")
                    .set( "click", slot( "switchClick" ) )

            )
        ));
        
    }
    
    int counter;
    
    
    public void doClick( Event event ) {
        counter = 5;
        add( new WaitPopup()
            .set( "name", "waitPopup" )
            .store( "message.value", "Wait " + counter + " seconds..." ) 
        );
        next( slot( "completeJob" ) );
    }
    
    public void completeJob( Event event ) throws Exception {
        System.out.println( "Complete... " + event( "nextTick" ).size() );
        Thread.sleep( 1000 );
        if( --counter > 0 ) {
            set( "waitPopup.message.value", "Wait " + counter + " seconds..." );
            set( "waitPopup.progress.value", ( 5 - counter + 1 ) * 20 );
            next( slot( "completeJob" ) );
        } else {
            this.<Popup>get( "waitPopup" ).destroy();
        }
    }
    
    public void switchClick( Event event ) {
        set( "main.columns", get( "main.columns" ).equals( 2 ) ? 4 : 2 );
    }

    public void switchAndDoClick( Event event ) {
        switchClick( event );
        doClick( event );
    }
    
    public void progressClick( Event event ) {
        if( this.<Integer>get( "progress.value" ).equals( 100 ) ) {
            set( "progress.value", 0 );
        } else {
            set( "progress.value", this.<Integer>get( "progress.value" ) + 20 );
        }
    }
}