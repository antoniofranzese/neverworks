package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.Date;
import java.util.List;

import it.neverworks.model.Property;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.PinboardLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.web.WebView;
import it.neverworks.ui.web.WebViewLoadEvent;
import it.neverworks.ui.web.WebViewCallbackEvent;
import it.neverworks.ui.dialogs.MessageBox;

public class MapTestForm extends Form {
    
    @Property @Reference
    private MapComponent map;
    
    public void build() {
        set( "main", new BorderLayout(
            arg( "top", new GridLayout(
                arg( "columns", "unbound" )
                .arg( "layout.splitter", true )

                ,new ToolBar(
                    new Button()
                        .set( "name", "loadButton" )
                        .set( "caption", "Load" )
                        .set( "click", slot( "loadPoints" ) )
                        .set( "enabled", false )
                    ,new Button()
                        .set( "name", "placeButton" )
                        .set( "caption", "Firenze" )
                        .set( "click", slot( "placeCenter" ) )
                        .set( "enabled", false )
                )

                ,new TextBox()
                    .set( "name", "message" )
                    .set( "label", "Message" )
                    .set( "width", 350 )

                ,new TextBox()
                    .set( "name", "address" )
                    .set( "label", "Last address" )
                    .set( "width", 250 )
                                
            ))

            .arg( "center", new PinboardLayout(
                new MapComponent(
                    arg( "name", "map" )
                    .arg( "load", slot( "loaded" ) )
                    .arg( "ready", slot( "ready" ) )
                    .arg( "move", slot( "updatePosition" ) )
                    .arg( "markerClick", slot( "markerClick" ) )
                    .arg( "layout",
                        arg( "top", 0 )
                        .arg( "left", 0 )
                        .arg( "width", "full" )
                        .arg( "height", "full" )
                    )
                )
                ,new Panel(
                    arg( "content", new Button(
                        arg( "caption", "OK" )    
                    ))
                    .arg( "layout",
                        arg( "top", 0 )
                        .arg( "right", 0 )
                    )

                )
            ))
                               
        ));
        
    }
    
    private List markers = list(
        arg( "id", 1 ).arg( "nome", "SERGIO BIANCHI" ).arg( "longitudine", 12.63500379 ).arg( "latitudine", 42.57188769 )
        ,arg( "id", 2 ).arg( "nome", "MARIO ROSSI" ).arg( "longitudine", 11.33333 ).arg( "latitudine", 43.83333 )
    );

    public void loadPoints( Event event ) {

        map.loadPoints( markers );

    }

    public void loaded( WebViewLoadEvent event ) {
        set( "address.value", event.getLocation().toStringURL() );
    }
    
    public void ready( Event event ) {
        set( "message.value", "Ready!" );
        query( Button.class ).set( "enabled", true );
        System.out.println( "Center: " + map.getCenter() );
    }

    public void updatePosition( Event event ) {
        set( "message.value", "Lat: " + event.get( "lat") + ", Lon: " + event.get( "lon" ) );
        System.out.println( "Updated center: " + map.getCenter() );
    }
    
    public void markerClick( Event event ) {
        System.out.println( "Origin: " + map.get( "origin" ) );
        Object marker = query( markers ).by( "id" ).eq( event.get( "marker" ) ).result();
        add( new MessageBox( msg( "Marker {0} for {1.nome} clicked", event.get( "marker" ), marker ) ).open( event.<Integer>get( "x" ), event.<Integer>get( "y" ) ) );
    }
    
    public void placeCenter( Event event ) {
        if( event.get( "source.caption" ).equals( "Firenze" ) ) {
            map.set( "center", new MapCoordinates().set( "latitude", 43.83333 ).set( "longitude", 11.33333 ) );
            event.set( "source.caption", "Roma" );
        } else {
            map.set( "center", new MapCoordinates().set( "latitude", 41.8954656 ).set( "longitude", 12.4823243 ) );
            event.set( "source.caption", "Firenze" );
        }
        set( "message.value", "Last zoom: " + map.get( "!properties.zoom" ) );
    }
}