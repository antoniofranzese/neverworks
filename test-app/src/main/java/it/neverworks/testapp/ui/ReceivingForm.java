package it.neverworks.testapp.ui;

import java.util.List;
import java.io.File;

import static it.neverworks.language.*;

import it.neverworks.lang.Application;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Reference;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.web.Conversation;
import it.neverworks.ui.web.Downloader;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Subscribe;
import it.neverworks.model.Value;

import it.neverworks.io.FileRepository;
import it.neverworks.io.FileBadge;

public class ReceivingForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 ) 
            
            ,new Label(
                arg( "font.size", 48 )
                .arg( "value", "Receiver" )
                .arg( "layout.hspan", "full" )
            )

            ,new ToolBar(
                arg( "name", "buttons" )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )

                ,new Button()
                    .set( "caption", "Reply")
                    .set( "click", slot( "send" ) )

             )
        ));
        
    }
    
    public void send( Event event ) {

    }

    @Subscribe( MessageEvent.class )
    public void receive( MessageEvent event ) {
        query( MessageBox.class ).call( "close" );

        add( new MessageBox()
            .set( "title", "Message received" )
            .set( "message", event.getMessage() )
        );
    }

}