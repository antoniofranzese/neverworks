package it.neverworks.testapp.ui;

import java.util.Date;
import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.events.Event;
import it.neverworks.model.io.Internal;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Image;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.TabLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.LabelPanel;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.Catalog;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.dnd.Avatar;
import it.neverworks.ui.dnd.DropRules;
import it.neverworks.ui.menu.Menu;
import it.neverworks.ui.menu.MenuItem;
import it.neverworks.ui.menu.CheckedMenuItem;
import it.neverworks.ui.menu.MenuSeparator;

public class MenuTestForm extends Form {
    
    @Property @Inject( "IconCatalog" )
    private Catalog icons;
    
    public void build() {
        
        List treeItems = list(
             new Item( arg( "id", "root" )                             .arg( "name", "Elements" ) )           
            ,new Item( arg( "id", "CUGS" )                             .arg( "name", "CUGS" )               .arg( "parent", "root" )                         .arg( "icon", null ) )                             
            ,new Item( arg( "id", "CUGS:CNT" )                         .arg( "name", "CNT" )                .arg( "parent", "CUGS" )                         .arg( "icon", null ) )                            
            ,new Item( arg( "id", "CUGS:CNT:OSM" )                     .arg( "name", "OSM" )                .arg( "parent", "CUGS:CNT" )                     .arg( "icon", null ) )                            
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server" )          .arg( "name", "osm-server" )         .arg( "parent", "CUGS:CNT:OSM" )                 .arg( "icon", null ) )                            
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:C" )        .arg( "name", "CPU" )                .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "cpu" ) ) 
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:R" )        .arg( "name", "RAM" )                .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "memory" ) )
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:D" )        .arg( "name", "Disk (/opt)" )        .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "disk" ) )
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:P" )        .arg( "name", "OSMMRM_XMgrMain" )    .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "process" ) )
            ,new Item( arg( "id", "CUGS:PM01" )                        .arg( "name", "PM01" )               .arg( "parent", "CUGS" )                         .arg( "icon", null ) )  
            ,new Item( arg( "id", "CUGS:PM01:osmPF1" )                 .arg( "name", "osmPF1" )             .arg( "parent", "CUGS:PM01" )                    .arg( "icon", null ) )                           
            ,new Item( arg( "id", "CUGS:PM01:osmPF1:pm01-server" )     .arg( "name", "pm01-server" )        .arg( "parent", "CUGS:PM01:osmPF1" )             .arg( "icon", null ) )                           
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:C" )   
                .arg( "name", "CPU" )                
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "cpu" )      
            )
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:R" )   
                .arg( "name", "RAM" )                
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "memory" )   
            )
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:D1" )  
                .arg( "name", "Disk (/opt)" )        
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "disk" )     
            )
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:D2" )  
                .arg( "name", "Disk (/archive)" )    
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "disk" )     
            )
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:P1" )  
                .arg( "name", "APES" )               
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "process" )  
            )
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:P2" )  
                .arg( "name", "osmPF" )              
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "process" )  
            )
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:P3" )  
                .arg( "name", "OSMAM_XMain" )        
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "process" )  
            )
        
        );
        
        add( new Menu(
            arg( "name", "menu1" )
            .arg( "click", slot( "mainClick" ) )
            .arg( "close", slot( "mainClose" ) )
            .arg( "children", list(
                new MenuItem(
                    arg( "name", "item0" )
                    .arg( "caption", "Simple menu item" )
                )
                ,new MenuItem(
                    arg( "name", "item1" )
                    .arg( "caption", "Disabled menu item" )
                    .arg( "disabled", true )
                )
                ,new CheckedMenuItem(
                    arg( "name", "item3" )
                    .arg( "caption", "checkable menu item" )
                )
                ,new MenuSeparator()
                ,new Menu(
                    arg( "name", "sub" )
                    .arg( "caption", "Submenu")
                    .arg( "children", list(
                        new MenuItem(
                            arg( "name", "item4" )
                            .arg( "caption", "Submenu item 1" )
                        )
                        ,new MenuItem(
                            arg( "name", "item5" )
                            .arg( "caption", "Submenu item 2" )
                            .arg( "click", slot( "subClick" ) ) 
                        )
                        ,new Menu(
                            arg( "name", "sub2" )
                            .arg( "caption", "Very Submenu")
                            .arg( "click", slot( "verySubClick" ) ) 
                            .arg( "children", list(
                                new MenuItem(
                                    arg( "name", "item4" )
                                    .arg( "caption", "Very Submenu item 1" )
                                )
                                ,new MenuItem(
                                    arg( "name", "item5" )
                                    .arg( "caption", "Very Submenu item 2" )
                                )
                            ))
                        )
                    ))
                )
            ))
        ));
        
        add( new BorderLayout(
            arg( "gutters", true )

            .arg( "left", new GridLayout(
                arg( "columns", 1 )
                .arg( "width", 40 )
                ,new Image()
                    .set( "source", get( "icons[home@32]" ) )
                    .set( "click", slot( "imageClick" ) )
                    .set( "inspect", slot( "imageInspect" ) )
                    .set( "attributes", arg( "type", "home" ) )
                ,new Image()
                    .set( "source", get( "icons[globe@32]" ) )
                    .set( "click", slot( "imageClick" ) )
                    .set( "inspect", slot( "imageInspect" ) )
                    .set( "attributes", arg( "type", "globe" ).arg( "checked", false ) )
                ,new Image()
                    .set( "source", get( "icons[tag@32]" ) )
                    .set( "click", slot( "imageClick" ) )
                    .set( "inspect", slot( "imageInspect" ) )
                    .set( "attributes", arg( "type", "tag" ) )
            ))

            .arg( "center", new GridLayout(
                arg( "columns", 1 )
                ,new Panel(
                    arg( "name", "panel1" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Homes only" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                )
                ,new Panel(
                    arg( "name", "panel2" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Globes only" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                )
                ,new Panel(
                    arg( "name", "panel3" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Tags only" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                )
                ,new Panel(
                    arg( "name", "panel4" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Tags or globes" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                )
                ,new TextBox()
                    .set( "name", "log" )
                    .set( "width", 300 )
                    .set( "writable", false )
            ))
            
            .arg( "right", new Tree()
                .set( "name", "elements" )
                .set( "items", treeItems )
                .set( "root", treeItems.get( 0 ) )
                .set( "width", 300 )
                .set( "expanded", true )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "link", "parent" )
                .set( "icon", "icon" )
                .set( "icons", 
                    arg( "cpu",      get( "icons[icon16-cpu]" ) )
                    .arg( "memory",  get( "icons[icon16-memory]" ) )
                    .arg( "disk",    get( "icons[icon16-disk]" ) ) 
                    .arg( "process", get( "icons[icon16-process]" ) )
                )
                .set( "inspect", slot( "inspectTree" ) )
            )

            .arg( "bottom", new ToolBar(
                new Button()
                    .set( "caption", "Menu on click" )
                    .set( "click", slot( "buttonClick" ) )
                    .set( "arrow", true )
                ,new Button()
                    .set( "caption", "Menu on right" )
                    .set( "inspect", slot( "buttonInspect" ) )
            ))

        ));
        
    }
    
    public void imageClick( Event event ) {
        set( "log.value", "Image click: " + new Date() );
    }
    
    public void imageInspect( Event event ) {
        set( "menu1.item3.visible", event.get( "!source.attributes.checked", true ) );

        set( "menu1.item0.caption", "Item for " + event.get( "source.attributes.type" ) + " image" );
        set( "menu1.sub.item4.caption", "Submenu item 1 for " + event.get( "source.attributes.type" ) );
        set( "menu1.sub.sub2.item4.caption", "Very Submenu item 1 for " + event.get( "source.attributes.type" ) );
        set( "menu1.anchor", event.get( "origin" ) );
        set( "menu1.opened", true );
    }
    
    public void mainClose( Event event ) {
        set( "log.value", "Menu close: " + new Date() );
    }

    public void mainClick( Event event ) {
        add( new MessageBox( 
            arg( "title", "Main Click" )
            .arg( "message", "Click: " + event.get( "item.caption" ) )
        ));
    }
    
    public void subClick( Event event ) {
        add( new MessageBox( 
            arg( "title", "Sub Click" )
            .arg( "message", "SubClick: " + event.get( "item.caption" ) )
        ));
    }

    public void verySubClick( Event event ) {
        add( new MessageBox( 
            arg( "title", "Very Sub Click" )
            .arg( "message", "VerySubClick: " + event.get( "item.caption" ) )
        ));
        //event.propagate();
    }
    
    public void inspectTree( Event event ) {
        add( new Menu(
            arg( "name", "menu2" )
            .arg( "autoDestroy", true )
            .arg( "click", slot( "treeClick" ) )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "opened", true )
            .arg( "attributes", arg( "treeItem", event.get( "item" ) ) )
            .arg( "children", list(
                new MenuItem(
                    arg( "name", "item0" )
                    .arg( "caption", "Item for " + event.get( "item.name" ) )
                )
                ,new MenuSeparator()
                ,new Menu(
                    arg( "name", "sub" )
                    .arg( "caption", "Submenu")
                    .arg( "children", list(
                        new MenuItem(
                            arg( "name", "item4" )
                            .arg( "caption", "Submenu item 1" )
                        )
                        ,new MenuItem(
                            arg( "name", "item5" )
                            .arg( "caption", "Submenu item 2" )
                            .arg( "click", slot( "subClick" ) ) 
                        )
                    ))
                )
            ))
        ));
    }

    public void treeClick( Event event ) {
        add( new MessageBox( 
            arg( "title", "Tree Click" )
            .arg( "message", 
                "Click: " + event.get( "item.caption" ) 
                + " on " + event.get( "menu.attributes.treeItem.icon" )
                + " of " + event.get( "menu.attributes.treeItem.id" ) 
            )
        ));
    }
    
    public void buttonClick( Event event ) {
        add( new Menu(
            arg( "autoDestroy", true )
            .arg( "click", slot( "mainClick" ) )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "opened", true )
            .arg( "children", list(
                new MenuItem(
                    arg( "name", "item0" )
                    .arg( "caption", "Item for " + event.get( "source.caption" ) + " button" )
                )
                ,new MenuSeparator()
                ,new Menu(
                    arg( "name", "sub" )
                    .arg( "caption", "Submenu")
                    .arg( "children", list(
                        new MenuItem(
                            arg( "name", "item4" )
                            .arg( "caption", "Submenu item 1" )
                        )
                        ,new MenuItem(
                            arg( "name", "item5" )
                            .arg( "caption", "Submenu item 2" )
                            .arg( "click", slot( "subClick" ) ) 
                        )
                    ))
                )
            ))
        ));
        
    }
    
    public void buttonInspect( Event event ) {
        add( new Menu(
            arg( "autoDestroy", true )
            .arg( "click", slot( "mainClick" ) )
            .arg( "anchor", event.get( "origin" ) )
            .arg( "opened", true )
            .arg( "children", list(
                new MenuItem(
                    arg( "name", "item0" )
                    .arg( "caption", "Right for " + event.get( "source.caption" ) + " button" )
                )
                ,new MenuSeparator()
                ,new Menu(
                    arg( "name", "sub" )
                    .arg( "caption", "Submenu")
                    .arg( "children", list(
                        new MenuItem(
                            arg( "name", "item4" )
                            .arg( "caption", "Submenu item 1" )
                        )
                        ,new MenuItem(
                            arg( "name", "item5" )
                            .arg( "caption", "Submenu item 2" )
                            .arg( "click", slot( "subClick" ) ) 
                        )
                    ))
                )
            ))
        ));
        
    }
}