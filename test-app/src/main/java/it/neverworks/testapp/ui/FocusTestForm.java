package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.NumberTextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.ValueFormatter;

public class FocusTestForm extends Form {
    
    public class SubForm extends Form {
        public void build() {
            set( "main", new GridLayout(
                new TextBox()
                    .set( "name", "text1" )
                    .set( "label", "SubText 1" )
                    .set( "tabPress", slot( "stayThere" ) )
                ,new TextBox()
                    .set( "name", "text2" )
                    .set( "label", "SubText 2" )
                ,new TextBox()
                    .set( "name", "text3" )
                    .set( "label", "SubText 3" )
                ,new TextBox()
                    .set( "name", "text4" )
                    .set( "label", "SubText 4" )
            ));

        }
        
        public void stayThere( Event event ) {
        
        }
    }
    
    @Property @Reference
    private TextBox text1;
    
    public void build() {
        set( "main", new GridLayout(
            new TextBox()
                .set( "name", "text1" )
                .set( "label", "Text 1" )
                .set( "enterPress", slot( "enterPressed" ) )
                .set( "attributes", arg( "next", "text2" ) )
            ,new TextBox()
                .set( "name", "text2" )
                .set( "label", "Text 2" )
                .set( "enterPress", slot( "enterPressed" ) )
                .set( "attributes", arg( "next", "text3" ) )
            ,new TextBox()
                .set( "name", "text3" )
                .set( "label", "Text 3" )
                .set( "enterPress", slot( "enterPressed" ) )
                .set( "attributes", arg( "next", "text4" ) )
            ,new TextBox()
                .set( "name", "text4" )
                .set( "label", "Text 4" )
                .set( "enterPress", slot( "enterPressed" ) )
                .set( "attributes", arg( "next", "text1" ) )
                
            ,new SubForm()
                .set( "name", "sub" )
                .set( "layout", arg( "hspan", "full" ) )
                
            ,new ToolBar(
                arg( "layout",
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "caption", "Focus 1" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "text1" ) )
                ,new Button()
                    .set( "caption", "Focus 2" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "text2" ) )
                ,new Button()
                    .set( "caption", "Focus 3" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "text3" ) )
                ,new Button()
                    .set( "caption", "Focus 4" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "text4" ) )
            )

            ,new ToolBar(
                arg( "layout",
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "caption", "Focus Sub 1" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "sub.text1" ) )
                ,new Button()
                    .set( "caption", "Focus Sub 2" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "sub.text2" ) )
                ,new Button()
                    .set( "caption", "Focus Sub 3" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "sub.text3" ) )
                ,new Button()
                    .set( "caption", "Focus Sub 4" )
                    .set( "click", slot( "focusClick" ) )
                    .set( "attributes", arg( "widget", "sub.text4" ) )
            )
            
            ,new ToolBar(
                arg( "layout",
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "caption", "Focus 1 w/method" )
                    .set( "click", slot( "focus1Click" ) )
                ,new Button()
                    .set( "caption", "Focus 2 w/value" )
                    .set( "click", slot( "focus2Click" ) )
                ,new Button()
                    .set( "caption", "Blur" )
                    .set( "click", slot( "blurClick" ) )
            )

            ,new NumberTextBox()
                .set( "name", "text5" )
                .set( "label", "Constrained (0->5)" )
                .set( "change", slot( "constrainedChange" ) )
            ,new NumberTextBox()
                .set( "name", "text6" )
                .set( "label", "Unonstrained" )
            

        ));
        
        //focus( "text1" );
        focus( "sub.text1" );

    }
    
    public void focusClick( Event event ) {
        focus( (String) event.get( "source.attributes.widget" ) );
    }

    public void blurClick( Event event ) {
        blur();
    }
    
    public void focus1Click( Event event ) {
        text1.focus();
    }

    public void focus2Click( Event event ) {
        set( "text2.value", "focused" );
        focus( "text2" );
    }
    
    public void enterPressed( Event event ) {
        if( event.get( "!source.attributes.next" ) != null ) {
            focus( (String) event.get( "source.attributes.next" ) );
        }
    }
    
    public void constrainedChange( Event event ) {
        // if( event.<Integer>get( "source.value" ) < 0
        //     || event.<Integer>get( "source.value" ) > 5 ) {
        //
        //     add( new MessageBox( "Tra 0 e 5!!!" )/*.set( "confirm", slot( "focusField" ) ) */ );
        //     event.set( "source.value", 0 );
        //     focus( event.<NumberTextBox>get( "source" ) );
        // }
    }
    
    public void focusField( Event event ) {
        focus( "text5" );
    }
    
    
    
}