package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.CloseEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.TabLayout;

public class TabNestingForm extends Form
{
	
	@Property @Reference
    private TabLayout pages;
	
	
	public void build()
	{
		set( "main", new BorderLayout(
	            arg( "gutters", true )

	            .arg( "center", new TabLayout(
	                arg( "name", "pages" )	               
	                ,new Panel(
	                    arg( "title", "Tab. 1" )
	                    .arg( "closable", true )
	                )	                
	                ,new Panel(
	                    arg( "title", "Tab. 2" )
	                    .arg( "closable", true )                    
	                )
	                ,new Panel(
		                    arg( "title", "Tab. 3" )
		                    .arg( "closable", true )                    
		                )
	                ,new Panel(
		                    arg( "title", "Tab. 4" )
		                    .arg( "closable", true )                    
		                )
	                ,new Panel(
		                    arg( "title", "Tab. 5" )
		                    .arg( "closable", true )                    
		                )
	            ))

	            .arg( "bottom", new GridLayout(
	                arg( "columns", 1 )
	                ,new ToolBar(
	                    new Button(
	                        arg( "caption", "Add" )
	                        .arg( "click", slot( "addTab" ) )
	                    )
	                )
	            ))        

	        ));

	}
	
	 public void addTab( Event event ) {
		 pages.place( new Panel(
            arg( "title", "New Tab" )
            .arg("closable", true)		            
         )).last();
	 }
	 

}
