package it.neverworks.testapp.ui;

import java.util.List;

import it.neverworks.lang.Functional;
import it.neverworks.lang.Mapper;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Widget;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.NumberTextBox;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import static it.neverworks.language.*;


 

public class FormTest extends Form{
	
	public void build(){
		add(new Popup(arg("opened",true).arg("width",600).arg("height",400))
			.add(new test())
				);
	}
	
	public class test extends Form {
		 BorderLayout b = new BorderLayout(
					arg("center",
							new GridLayout(
								arg("columns",1)
								,new TextBox(arg("name","text1").arg("required",false).arg("enabled",true))
								,new NumberTextBox<Number>(arg("name","text2").arg("required",false).arg("enabled",true))
								,new NumberTextBox<Integer>(arg("name","text4").arg("required",false).arg("enabled",true))
								,new NumberSpinner<Number>(arg("min",1).arg("max", 20).arg("name","text3").arg("required",false).arg("enabled",true))
							)
						)//fine center
						.arg("bottom"
							,new ToolBar(
								new Button(arg("caption","set problem").arg("click",slot("setProblem")))
								,new Button(arg("caption","delete problem").arg("click",slot("deleteProblem")))
								,new Button(arg("caption","readValue").arg("click",slot("read")))
								,new Button(arg("caption","enabledRequired").arg("click",slot("enabledRequired")))
								,new Button(arg("caption","disabledRequired").arg("click",slot("disabledRequired")))
							)
						)
					);
		 public void build(){
			add(b);
					 
				 
		 }
		 
		 public void enabledRequired(Event event){
			 set("text1.required",true);
			 set("text2.required",true);
			 set("text3.required",true);
			 set("text4.required",true); 
			 
		 }
		 
		 
		 public void disabledRequired(Event event){
			 set("text1.required",false);
			 set("text2.required",false);
			 set("text3.required",false);
			 set("text4.required",false); 
			 
		 }
		 
		 public void setProblem(Event event){
			 set("text1.problem","problem text1");
			 set("text2.problem","problem text2");
			 set("text3.problem","problem text3");
			 set("text4.problem","problem text4");
			 
		 }
		 public void deleteProblem(Event event){
			 set("text1.problem",null);
			 set("text2.problem",null);
			 set("text3.problem",null);
			 set("text4.problem",null);
		}
		 public void read(Event event){
			 String message = "";
			message = "text1:" +get("text1.value") + " text2:" +get("text2.value") +" text3:" +get("text3.value") + " text4: "+ get("text4.value")	;
			add( new MessageBox(arg("autoDestroy",true)
					.arg("opened",true)
					.arg("message",message)
					)); 
		 }
	}
	
	}
			
 


