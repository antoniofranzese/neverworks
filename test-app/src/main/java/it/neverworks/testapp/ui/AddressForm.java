package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;

public class AddressForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            arg( "border", 1 )
            ,new TextBox()
                .set( "name", "txtAddress" )
                .set( "label", "Indirizzo" )
            ,new TextBox()
                .set( "name", "txtCity")
                .set( "label", "Comune" )
            ,new ToolBar(
                new Button()
                    .set( "caption", "Test")
                    .set( "font", 9 )
                    .set( "height", 10 )
                ,new Button()
                    .set( "name", "btnRelocate" )
                    .set( "caption", "Sposta")
                    .set( "click", UI.Slot( this, "relocateClick" ) )
                ,new Button()
                    .set( "name", "btnSwitch" )
                    .set( "caption", "Switch")
                    .set( "width", 100 )
                    .set( "height", 100 )
                    .set( "click", UI.Slot( this, "switchClick" ) )

                ).set( "name", "buttons" )            
                .set( "layout", UI
                    .set( "hspan", "full" )
                    .set( "halign", "right" ) 
                )
                        
            )
            .set( "columns", 2 ) 
        );
    }
    
    public void relocateClick( ClickEvent<Button> event ) {
        System.out.println( "RELOCATE A" );
        set( "txtCity.value", get( "txtAddress.value" ) );
        set( "txtAddress.value", "" );
    }
    
    public void switchClick( ClickEvent event ) {
        System.out.println( "SWITCH A" );

        if( this.<Integer>get( "main.columns" ) == 2 ) {
            set( "main.columns", 4 );
        } else {
            set( "main.columns", 2 );
        }
    }
    
}