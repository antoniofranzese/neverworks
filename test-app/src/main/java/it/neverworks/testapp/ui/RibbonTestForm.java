package it.neverworks.testapp.ui;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.ribbon.Ribbon;
import it.neverworks.ui.form.ribbon.RibbonStrip;
import it.neverworks.ui.form.ribbon.RibbonGroup;
import it.neverworks.ui.form.ribbon.RibbonSection;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.TabLayout;
import it.neverworks.ui.layout.CloseEvent;
import it.neverworks.ui.layout.SelectEvent;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;
import it.neverworks.ui.data.Catalog;
import it.neverworks.ui.data.Artifact;

public class RibbonTestForm extends Form {
    
    @Property @Inject( "IconCatalog" )
    private Catalog icons;
    
    private Artifact icon( String name ) {
        return this.<Catalog>get( "icons" ).get( name );
    }
    
    @Override
    public void build() {
        set( "title", "Ribbon Test" );
        set( "main", new BorderLayout(

            arg( "top", new Ribbon(
                new RibbonStrip(
                    arg( "title", "Administrator" )

                    ,new RibbonGroup(
                        arg( "title", "File" )
                        ,new Button()
                            .set( "caption", "Change\ncenter" )
                            .set( "icon", icon( "save@32" ) )
                            .set( "click", slot( "changeCenter" ) )
                        ,new Button()
                            .set( "caption", "Export" )
                            .set( "icon", icon( "upload@32" ) )
                        ,new Button()
                            .set( "caption", "Exit" )
                            .set( "icon", icon( "exit@32" ) )
                    )

                    ,new RibbonGroup(
                        arg( "title", "G/S Structure" )
                        ,new Button()
                            .set( "caption", "Global\ncomponents" )
                            .set( "icon", icon( "globe@32" ) )
                        ,new Button()
                            .set( "caption", "External\nFTP sites" )
                            .set( "icon", icon( "nas@32" ) )
                    )
                )
                
                ,new RibbonStrip(
                    arg( "title", "Operator" )

                    ,new RibbonGroup(
                        arg( "title", "File" )
                        ,new Button()
                            .set( "caption", "Change\ncenter" )
                            .set( "icon", icon( "save@32" ) )
                            .set( "click", slot( "changeCenter" ) )
                        ,new Button()
                            .set( "caption", "Refresh" )
                            .set( "icon", icon( "update@32" ) )
                        ,new Button()
                            .set( "caption", "Archive" )
                            .set( "icon", icon( "archive@32" ) )
                    )

                    ,new RibbonGroup(
                        arg( "title", "Status & Logs" )
                        ,new Button()
                            .set( "caption", "Control\nCM Instance" )
                            .set( "icon", icon( "remotecontrol@32" ) )

                        ,new RibbonSection(
                            arg( "orientation", "horizontal" )
                            ,new Button()
                                .set( "caption", "Internal status" )
                                .set( "icon", icon( "speedometer@16" ) )
                            ,new Button()
                                .set( "caption", "Remote instances status" )
                                .set( "icon", icon( "speedometer@16" ) )
                            ,new Button()
                                .set( "caption", "Event log" )
                                .set( "icon", icon( "watch@16" ) )
                        )
                    )
                
                )
            ))

            .arg( "center", new Label().set( "value", "center" ) )
            
            .arg( "bottom", new ToolBar(
                new Button(
                    arg( "caption", "OK" )
                )
            ))

        ));

    }
    
    public void changeCenter( Event event ) {
        set( "main.center", new Panel().set( "content", new Label().set( "value", "Changed" ) ) );
    }
    
}