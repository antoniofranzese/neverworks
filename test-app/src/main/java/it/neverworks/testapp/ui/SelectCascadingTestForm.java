package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.SelectChangeEvent;
import it.neverworks.ui.form.SelectResetEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;

public class SelectCascadingTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            new Select()
                .set( "name", "selectA" )
                .set( "label", "Select A" )
                .set( "blank", true )
                .set( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Name1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Name2" ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Name3" ) )
                ))
                .set( "change", slot( "selectChangeA" ) )

            ,new Select()
                .set( "name", "selectB" )
                .set( "label", "Select B" )
                .set( "blank", true )
                .set( "change", slot( "selectChangeB" ) )
                .set( "reset", slot( "selectResetB" ) )
            
            ,new TextBox()
                .set( "name", "textB" )
                .set( "label", "B selection" )
                
            ,new ToolBar(
                arg( "!layout.hspan", "full" )
                ,new Button()
                    .set( "caption", "Reset" )
                    .set( "click", slot( "reset" ) )
                    
            )
            
        ));
    }

    public void selectChangeA( SelectChangeEvent<Select> event ) {
        System.out.println( "change A" );
        
        if( get( "selectA.selected" ) == null ) {
            set( "selectB.items", null );
        } else if( get( "selectA.selected.id" ).equals( 1 ) ) {
            set( "selectB.items", list(
                 new Item( arg( "id", 1 ).arg( "name", "Sub1 Name1" ) )
                ,new Item( arg( "id", 2 ).arg( "name", "Sub1 Name2" ) )
                ,new Item( arg( "id", 3 ).arg( "name", "Sub1 Name3" ) )
            ));
        } else if( get( "selectA.selected.id" ).equals( 2 ) ) {
            set( "selectB.items", list(
                 new Item( arg( "id", 1 ).arg( "name", "Sub2 Name1" ) )
                ,new Item( arg( "id", 2 ).arg( "name", "Sub2 Name2" ) )
                ,new Item( arg( "id", 3 ).arg( "name", "Sub2 Name3" ) )
            ));
        } else if( get( "selectA.selected.id" ).equals( 3 ) ) {
            set( "selectB.items", list(
                 new Item( arg( "id", 1 ).arg( "name", "Sub3 Name1" ) )
                ,new Item( arg( "id", 2 ).arg( "name", "Sub3 Name2" ) )
                ,new Item( arg( "id", 3 ).arg( "name", "Sub3 Name3" ) )
            ));
        }
    }

    public void selectChangeB( SelectChangeEvent<Select> event ) {
        System.out.println( "change B" );
        set( "textB.value", get( "!selectB.selected.name" ) );
    }

    public void selectResetB( SelectResetEvent<Select> event ) {
        System.out.println( "reset B: " + event.get( "!item.id" ) );
    }
    
    public void reset( Event event ) {
        set( "selectA.value", null );
    }

}