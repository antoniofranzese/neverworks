package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.RadioGroup;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.GridLayout;

public class RadioTestForm extends Form {
    
    public void build() {
        List items5 = list(
            new Item( arg( "id", 1 ).arg( "name", "Name1" ).arg( "city", "City1" ).arg( "age", 10 ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Name2" ).arg( "city", "City2" ).arg( "age", 20 ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Name3" ).arg( "city", "City3" ).arg( "age", 30 ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Name4" ).arg( "city", "City4" ).arg( "age", 40 ) )
            ,new Item( arg( "id", 5 ).arg( "name", "Name5" ).arg( "city", "City5" ).arg( "age", 50 ) )
        );
        
        List items10 = list(
            new Item( arg( "id", 1 ).arg( "name", "Item 1" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ) )
            ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ) )
            ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ) )
            ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ) )
            ,new Item( arg( "id", 8 ).arg( "name", "Item 8" ) )
            ,new Item( arg( "id", 9 ).arg( "name", "Item 9" ) )
            ,new Item( arg( "id", 10 ).arg( "name", "Item 10" ) )
        );
        
        set( "main", new GridLayout(
            new RadioGroup()
                .set( "name", "people" )
                .set( "label", "Persona" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", items5 )
                .set( "change", new Slot( this, "selectChange" ) )

            ,new TextBox()
                .set( "name", "txtCity" )
                .set( "label", "Città" )

            ,new TextBox()
                .set( "name", "txtAge" )
                .set( "label", "Età" )
            
            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button()
                    .set( "caption", "Disable" )
                    .set( "click", slot( "toggleDisable" ) )
                ,new Button()
                    .set( "caption", "Hide" )
                    .set( "click", slot( "toggleHide" ) )
            )
            ,new RadioGroup()
                .set( "name", "peopleH" )
                .set( "label", "Horizontal" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "arrangement", "rows" )
                .set( "items", items5 )

            ,new RadioGroup()
                .set( "label", "3 Columns Hor" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "fractions", 3 )
                .set( "items", items10 )

            ,new RadioGroup()
                .set( "label", "3 Rows Hor" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "fractions", 3 )
                .set( "arrangement", "rows" )
                .set( "items", items10 )

            ,new RadioGroup()
                .set( "label", "3 Columns Ver" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "fractions", 3 )
                .set( "direction", "vertical" )
                .set( "items", items10 )

            ,new RadioGroup()
                .set( "label", "3 Rows Ver" )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "fractions", 3 )
                .set( "arrangement", "rows" )
                .set( "direction", "vertical" )
                .set( "items", items10 )
            
        ));
    }

    public void selectChange( ChangeEvent<RadioGroup> event ) {
        System.out.println( "Changed from " + event.getOld() + " to " + event.getNew() );
        set( "txtCity.value", get( "people.item.city" ) );
        set( "txtAge.value", get( "people.item.age" ) );
    }
    
    public void toggleDisable( Event event ) {
        boolean disable = event.get( "source.caption" ).equals( "Disable" );
        set( "people.disabled", disable );
        set( "peopleH.disabled", disable );
        event.set( "source.caption", disable ? "Enable" : "Disable" );
    }

    public void toggleHide( Event event ) {
        boolean visible = ! event.get( "source.caption" ).equals( "Hide" );
        set( "people.visible", visible );
        set( "peopleH.visible", visible );
        event.set( "source.caption", visible ? "Hide" : "Show" );
    }

}