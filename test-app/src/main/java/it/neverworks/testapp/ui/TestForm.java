package it.neverworks.testapp.ui;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.lang.Dates;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.form.TimeTextBox;
import it.neverworks.ui.form.NumberTextBox;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.Catalog;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;
import it.neverworks.ui.web.Downloader;
import it.neverworks.ui.UI;
import it.neverworks.ui.runtime.Device;

public class TestForm extends Form {
    
    private int count = 1;
    
    @Property @Inject( "IconCatalog" )
    private Catalog icons;
    
    @Override
    public void build() {

        System.out.println( "H: " + Device.get( "header[user-agent]" ) );
        System.out.println( "is: " + Device.is( "ios" ) );
        System.out.println( "is2: " + Device.is( "chrome" ) );
        System.out.println( "lang: " + Device.get( "languages" ) );
        set( "startup", slot( "startup" ) );
        
        this.set( "main", new GridLayout(
            arg( "columns", 2 ) 
            .arg( "collapsible", true )
            .arg( "startup", slot( "startup" ) )
            
            ,new TextBox()
                .set( "name", "txt1" )
                .set( "label", new Label()
                    .set( "value", "Campo 1" )
                    .set( "layout", 
                        arg( "color", "red" )
                        .arg( "background", "lightgray" )
                    )  
                    .set( "click", slot( "labelClick" ) )             
                )
                .set( "value", "val1<script>alert('!')</script>")
                .set( "hint", "Un po' di testo")
                .set( "layout", 
                    arg( "border", arg( "bottom", arg( "size", 1 ).arg( "style", "dashed" ).arg( "color", "black") ) ) 
                )
            
            ,new TextBox()
                .set( "name", "txt2" )
                .set( "label", new Label()
                    .set( "value", "Campo 2" ) 
                    .set( "name", "txt2Label" )
                    .set( "color", "lightgreen" ) 
                    .set( "inspect", slot( "labelClick" ) ) 
                    .set( "hint", "Il secondo campo" )            
                )
                .set( "value", "val2")
                .set( "halign", "center" )
                .set( "length", 10 )
                .set( "rounding", "8,0" )

            ,new TextBox()
                .set( "name", "txt3" )
                .set( "label", "Campo 3" ) 
                .set( "visible", false )

            ,new TextBox()
                .set( "name", "txt4" )
                .set( "label", new Label()
                    .set( "value", "Campo 4" ) 
                )
                .set( "required", true )

            ,new DateTextBox()
                .set( "name", "date1" )
                .set( "label", new Label()
                    .set( "value", "Data" ) 
                    .set( "layout", arg( "background", null ) )
                )
                .set( "layout", arg( "background", "gray" ) )

            ,new TimeTextBox()
                .set( "name", "time1" )
                .set( "label", new Label()
                    .set( "value", "Ora" ) 
                )

            ,new NumberSpinner()
                .set( "name", "number1" )
                .set( "min", 0 )
                .set( "max", "10" )
                .set( "halign", "right" )
                // .set( "value", 1 )
                .set( "rolling", true )
                .set( "hint", "Un numero da 0 a 10")
                .set( "label", new Label()
                    .set( "value", "Numero 1" ) 
                )

            ,new NumberTextBox()
                .set( "name", "number2" )
                .set( "halign", "right" )
                .set( "hint", "Un numero qualunque")
                .set( "label", new Label()
                    .set( "value", "Numero 2" ) 
                )
                .set( "type", Float.class )
                .set( "shadow", "gray" )

            ,new ToolBar(
                arg( "name", "buttons")
                .arg( "layout",
                    arg( "hspan", "full" )
                )
                //.arg( "visible", false )
                ,new Button()
                    .set( "name", "btnLabelColor" )
                    .set( "caption", "Colored" )
                    .set( "click", slot( "btnLabelColorClick" ) )
                    .set( "rounding", 5 )
                ,new Button()
                    .set( "name", "btnReadOnly" )
                    .set( "caption", "Read Only" )
                    .set( "icon", get( "icons[ globe@16 ]" ) )
                    .set( "click", slot( "btnReadOnlyClick" ) )
                    .set( "shadow", "gray 3 3 3" )
                    //.set( "flavor", "naked" )
                ,new Button()
                    .set( "name", "btnDisable" )
                    .set( "caption", "Disable" )
                    .set( "click", slot( "btnDisableClick" ) )
                    .set( "flavor", "naked" )
                ,new Button()
                    .set( "name", "btnRequired" )
                    .set( "caption", "Required" )
                    .set( "hint", "Imposta lo stato di required")
                    .set( "click", slot( "btnRequiredClick" ) )
                    .set( "flavor", "naked" )
                ,new Button()
                    .set( "name", "btnHidden" )
                    .set( "caption", "Hide" )
                    .set( "click", slot( "btnHiddenClick" ) )
                ,new Button()
                    .set( "name", "btnProblem" )
                    .set( "caption", "Problem" )
                    .set( "click", slot( "btnProblemClick" ) )

                ,new Button()
                    .set( "name", "btn1" )
                    .set( "caption", "Uppercase" )
                    .set( "click", slot( "btn1Click" ) )

                ,new Button()
                    .set( "name", "btn2" )
                    .set( "caption", "Switch" )
                    .set( "click", slot( "switchClick" ) )

                ,new Button()
                    .set( "caption", "Blink" )
                    .set( "click", slot( "blinkClick" ) )

            )

            ,new ToolBar(
                arg( "name", "editButtons")
                .arg( "collapsible", true )
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "name", "editButton" )
                    .set( "caption", "Edit" )
                    .set( "click", slot( "editBeginClick" ) )

                ,new Button()
                    .set( "name", "saveButton" )
                    .set( "caption", "Save" )
                    .set( "click", slot( "editEndClick" ) )
                    .set( "visible", false )
             
                ,new Button()
                    .set( "name", "cancelButton" )
                    .set( "caption", "Cancel" )
                    .set( "click", slot( "editEndClick" ) )
                    .set( "visible", false )
            )        

            ,new Panel()
                .set( "name", "formPanel" )                
                .set( "layout", 
                    arg( "hspan", "full" )
                )

            ,new ToolBar(
                arg( "name", "formButtons")
                .arg( "collapsible", true )
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "name", "btnAddress" )
                    .set( "caption", "Indirizzo" )
                    .set( "click", slot( "addressClick" ) )
                 
                ,new Button()
                    .set( "name", "btnFamily" )
                    .set( "caption", "Famiglia" )
                    .set( "click", slot( "familyClick" ) )
                 
                ,new Button()
                    .set( "name", "btnLayout" )
                    .set( "caption", "Layout" )
                    .set( "click", slot( "layoutClick" ) )
                 
                ,new Button()
                    .set( "name", "btnAddField" )
                    .set( "caption", "+1" )
                    .set( "click", slot( "addFieldClick" ) )
                 
                ,new Button()
                    .set( "name", "btnBlank" )
                    .set( "caption", "Vuoto" )
                    .set( "click", slot( "blankClick" ) )
                 
                ,new Button()
                    .set( "name", "btnHideToolbar" )
                    .set( "caption", "Toggle toolbar" )
                    .set( "click", slot( "toolbarHideClick" ) )
             
                )
        
            )        


        );
    }
    
    public void blinkClick( Event event ) {
        boolean blink = ! value( "txt2Label.blinking" ).isTrue();
        set( "txt2Label.blinking", blink );
        set( "btnReadOnly.blinking", blink );
        event.set( "source.caption", blink ? "No Blink" : "Blink" );
    }

    public void labelClick( Event event ) {
        add( new MessageBox( event.get( "source.value" ) + " clicked" ) );
    }
    
    public void editBeginClick( Event event ) {
        set( "editButton.visible", false );
        set( "saveButton.visible", true );
        set( "cancelButton.visible", true );
    }
    
    public void editEndClick( Event event ) {
        set( "editButton.visible", true );
        set( "saveButton.visible", false );
        set( "cancelButton.visible", false );
    }
    
    public void startup( Event event ) {
        println( "STARTUP:", event.getSource() );
    }
    
    public void btnLabelColorClick( ClickEvent event ) {
        System.out.println( "is2: " + Device.is( "chrome" ) );
        System.out.println( "is2: " + Device.is( "chrome" ) );
        if( event.<String>get( "source.caption" ).equals( "Colored" ) ) {
            set( "txt1.color", "red" );
            set( "txt1.background", "lightyellow" );
            
            set( "txt2Label.value", "Field 2" );
            set( "txt2Label.color", "red" );
            set( "txt2Label.background", "lightgray" );
            set( "btnLabelColor.caption", "Normal" );
        } else {
            set( "txt1.color", null );
            set( "txt1.background", null );

            set( "txt2Label.value", "Campo 2" );
            set( "txt2Label.color", "lightgreen" );
            set( "txt2Label.background", null );
            set( "btnLabelColor.caption", "Colored" );
        }
    }
    
    public void btnDisableClick( ClickEvent event ) {
        boolean disable = ! this.<Boolean>get( "btn1.disabled" );
        set( "date1.disabled", disable );
        set( "btn1.disabled", disable );
        set( "number1.disabled", disable );
        set( "btnDisable.caption", disable ? "Enable" : "Disable" );
    }

    public void btnReadOnlyClick( ClickEvent event ) {
        boolean writeable = ! this.<Boolean>get( "date1.writable" );
        set( "txt1.writable", writeable );
        set( "txt2.writable", writeable );
        set( "date1.writable", writeable );
        set( "number1.writable", writeable );
        set( "btnReadOnly.caption", writeable ? "Read Only" : "Writeable" );
        set( "btnReadOnly.icon", writeable ? get( "icons[ globe@16 ]" ) : get( "icons[ home@16 ]" ) );
        
    }

    public void btnRequiredClick( ClickEvent event ) {
        boolean required = ! this.<Boolean>get( "date1.required" );
        set( "txt1.required", required );
        set( "date1.required", required );
        set( "number1.required", required );
        set( "btnRequired.caption", required ? "Not required" : "Required" );
        set( "btnRequired.hint", required ? "Rimuove lo stato di required" : "Imposta lo stato di required" );
    }

    public void btnHiddenClick( ClickEvent event ) {
        boolean visible = ! get( "btnHidden.caption" ).equals( "Hide" );
        set( "txt2.visible", visible );
        set( "date1.label.visible", visible );
        set( "btnReadOnly.visible", visible );
        set( "btnLayout.visible", visible );
        set( "btnHidden.caption", visible ? "Hide" : "Show" );
    }

    public void btnProblemClick( ClickEvent event ) {
        // query().by( "problem" ).notNull().set( "problem", null );
        //
        // set( "txt1.problem", get( "txt1.value" ) == null ? "Campo richiesto" : null );
        // if( ! query().by( "problem" ).notNull().any() ){
        //
        // }
        //
        boolean problem = get( "btnProblem.caption" ).equals( "Problem" );
        set( "txt2.problem", problem ? "Text field problem" : null );
        set( "date1.problem", problem ? "Date field problem" : null );
        set( "number1.problem", problem ? "Number field problem" : null );
        set( "btnProblem.caption", problem ? "No problem" : "Problem" );
    }
    
    public void btn1Click( ClickEvent event ) {
        System.out.println( "UPPERCASE" );

        set( "txt1.value", this.<String>get( "txt1.value" ).toUpperCase() );
        set( "txt1.hint", null );
    }

    public void switchClick( ClickEvent event ) {
        System.out.println( "SWITCH" );

        if( this.<Integer>get( "main.columns" ) == 2 ) {
            set( "main.columns", 4 );
        } else {
            set( "main.columns", 2 );
        }
    }
    
    public void addressClick( ClickEvent event ) {
        if( !( this.get( "formPanel.content" ) instanceof AddressForm ) ) {
            this.set( "formPanel.content", new AddressForm().set( "name", "addressForm" ) );
        }
    }

    public void familyClick( ClickEvent event ) {
        if( !( this.get( "formPanel.content" ) instanceof FamilyForm ) ) {
            this.set( "formPanel.content", new FamilyForm() );
        }
    }

    public void layoutClick( ClickEvent event ) {
        if( !( this.get( "formPanel.content" ) instanceof GridLayout ) ) {
            this.set( "formPanel.content", new GridLayout(
                arg( "border.outer", 
                    arg( "top.style", "solid" )
                    .arg( "bottom.style", "solid" ) 
                )
                .arg( "border.inner.horizontal.style", "dashed" )
                .arg( "startup", slot( "startup" ) )
                ,new TextBox().set( "label", "New 1" )
                ,new TextBox().set( "label", "New 2" )    
            ) );
        }
    }

    public void addFieldClick( ClickEvent event ) {
        if( this.get( "formPanel.content" ) instanceof GridLayout ) {
            this.<Container>get( "formPanel.content").place(
                new TextBox().set( "label", "Other " + count++ )
            ).last();
        }
    }

    public void blankClick( ClickEvent event ) {
        this.set( "formPanel.content", null );
    }
    
    public void dateClick( Event event ) {
        System.out.println( "Current date: " + get( "date1.value" ) );
        System.out.println( "Current time: " + get( "time1.value" ) );
        System.out.println( "Merged datetime: " + Dates.mergeDateAndTime( this.<Date>get( "date1.value" ), this.<Date>get( "time1.value" ) ) );
        set( "date1.value", new Date() );
        // System.out.println( get( "date1.value" ) );
        // Obj o1 = new Obj();
        // o1.setDate( (Date) get( "date1.value" ) );
        //
        // Obj o2 = new Obj( (Date) get( "date1.value" ) );
        // System.out.println( "O1 " + o1.getDate() );
        // System.out.println( "O2 " + o2.getDate() );

    }
    
    public void toolbarHideClick( Event event ) {
        set( "buttons.visible", ! this.<Boolean>get( "buttons.visible" ) );
    }
    
    public static class Obj {
        public Obj(){
            super();
        }
        
        public Obj( Date date ) {
            this.date = date;
        }
        
        private Date date;
        
        public Date getDate(){
            return this.date;
        }
        
        public void setDate( Date date ){
            this.date = date;
        }
        
    }
}