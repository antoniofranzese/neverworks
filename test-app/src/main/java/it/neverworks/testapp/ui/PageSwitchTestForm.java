package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.web.Page;

public class PageSwitchTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            new ToolBar(
                arg( "name", "buttons" )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "caption", "Switch")
                    .set( "click", slot( "switchClick" ) )
                )
                        
            )
        );
    }
    
    public void switchClick( ClickEvent<Button> event ) {
        swap( new Page().set( "path", "/test/button.html" ) );
    }
    
}