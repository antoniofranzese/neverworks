package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Image;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Catalog;

public class IconsTestForm extends Form {
    
    @Property @Inject
    private Catalog icons;

    public void build() {

        add( new GridLayout(
            arg( "columns", 2 )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@100c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@100c]" ))
            )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@80c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@80c]" ))
            )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@48c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@48c]" ))
            )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@40c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@40c]" ))
            )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@32c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@32c]" ))
            )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@24c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@24c]" ))
            )
            ,new Button(
                arg( "flavor", "command" )
                .arg( "icon", get( "icons[marker@16c]" ))
                .arg( "caption", "Click me" )
            )
            ,new Image(
                arg( "source", get( "icons[marker@16c]" ))
            )
        ));

    }

}