package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.context.Context;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.context.Inject;
import it.neverworks.printing.TemplatePrinter;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Form;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileBadge;
import it.neverworks.ui.web.Downloader;

public class PrintTestForm extends Form {
    
    @Property @Inject
    private TemplatePrinter printer;
    
    @Property @Inject( "TemporaryFileRepository" )
    private FileRepository repository;
    
    public void build() {
        add( 
            new Button()
                .set( "caption", "Print" )
                .set( "click", slot( "print" ) )
        );
        
        add( 
            new Downloader()
                .set( "name", "downloader" )
        );
        
    }
    
    public void print( Event event ) {
        this.<Downloader>get( "downloader" ).send( printer.render( "test" ).to( repository ), "print.pdf" );
    }
    
    
}