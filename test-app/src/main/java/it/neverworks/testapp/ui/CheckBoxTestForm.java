package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;

import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.CheckBox;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Slot;

public class CheckBoxTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 ) 

            ,new Label(
                arg( "value", "Check" )
            )
            
            ,new CheckBox(
                arg( "name", "check1" )
                .arg( "change", new Slot( this, "checkChange" ) )
            )
            
            ,new TextBox(
                arg( "name", "text1")
                .arg( "label", "Checked" )
            )

            ,new CheckBox(
                arg( "name", "check2" )
                .arg( "label", "False on start" )
                .arg( "value", false )
                .arg( "change", new Slot( this, "checkChange" ) )
            )
            
            ,new CheckBox(
                arg( "name", "check3" )
                .arg( "label", "True on start" )
                .arg( "value", true )
                .arg( "change", new Slot( this, "checkChange" ) )
            )

            // ,new ToolBar(
            //     new Button()
            //         .set( "caption", "DO")
            //         .set( "click", UI.Slot( this, "doClick" ) )
            //
            //     ).set( "name", "buttons" )
            //     .set( "layout", UI
            //         .set( "hspan", "full" )
            //         .set( "halign", "right" )
            //     )
            //
            // )
            
        ) );
    }
    
    public void checkChange( Event event ) {
        set( "text1.value", this.<Boolean>get( "check1.value" ) ? "Checked" : "Unchecked" );
    }
    
}