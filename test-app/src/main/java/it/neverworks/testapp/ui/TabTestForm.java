package it.neverworks.testapp.ui;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.model.Property;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.TabLayout;
import it.neverworks.ui.layout.CloseEvent;
import it.neverworks.ui.layout.SelectEvent;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;

public class TabTestForm extends Form {
    
    private int count = 1;
    
    @Property @Reference
    private TabLayout pages;
    
    
    @Override
    public void build() {
        set( "title", "Tab Test" );
        set( "main", new BorderLayout(
            arg( "gutters", true )
            .arg( "width", "100%" )
            .arg( "height", "100%" )
            .arg( "center", new TabLayout(
                arg( "name", "pages" )
                .arg( "close", slot( "closePage" ) )
                .arg( "select", slot( "onSelect" ) )
                // .arg( "border.bottom.size", 0 )
                // .arg( "border.left.size", 0 )
                // .arg( "border.right.size", 0 )
                    
                ,new AddressForm()
                    .set( "title", "Formz Page" )

                ,new Panel(
                    arg( "title", "Panel Page" )
                    .arg( "closable", true )
                    .arg( "name", "panelPage" )
                    .arg( "layout",
                        arg( "loader", true )
                    )
                )

                ,new Panel(
                    arg( "title", "Panel 2" )
                    .arg( "name", "panel2" )
                    .arg( "closable", true )
                    .arg( "content", new GridLayout(
                        new TextBox()
                            .set( "name", "txt1G" )
                            .set( "label", "Campo 1" )
                            .set( "value", "val1")

                        ,new TextBox()
                            .set( "name", "txt2G" )
                            .set( "label", "Campo 2" )
                            .set( "value", "val2")
                    ))
                )

                ,new GridLayout(
                    arg( "name", "gridLayout")
                    .arg( "columns", 2 )
                    ,new TextBox()
                        .set( "name", "txtFather" )
                        .set( "label", "Padre" )
                    ,new TextBox()
                        .set( "name", "txtMother")
                        .set( "label", "Madre" )
                    ,new ToolBar(
                        arg( "name", "gridLayoutButtons" )
                        .arg( "layout",
                            arg( "hspan", "full" )
                            .arg( "halign", "right" )
                        )
                        ,new Button()
                            .set( "caption", "Scambia")
                            .set( "click", slot( "swapClick" ) )
                        ,new Button()
                            .set( "caption", "Switch")
                            .set( "click", slot( "switchClick" ) )

                    )
                )
            ))

            .arg( "bottom", new GridLayout(
                arg( "columns", 1 )
                ,new ToolBar(
                    new Button(
                        arg( "caption", "Load panelPage" )
                        .arg( "click", slot( "loadPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Blank panelPage" )
                        .arg( "click", slot( "blankPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select panelPage" )
                        .arg( "click", slot( "selectPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select page 3" )
                        .arg( "click", slot( "selectPage3" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add before 3" )
                        .arg( "click", slot( "addBefore3" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add start" )
                        .arg( "click", slot( "addStart" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add end" )
                        .arg( "click", slot( "addEnd" ) )
                    )
                    ,new Button(
                        arg( "caption", "Remove first" )
                        .arg( "click", slot( "removeFirst" ) )
                    )
                    ,new Button(
                        arg( "caption", "Rename firsts" )
                        .arg( "click", slot( "renameFirsts" ) )
                    )
                    ,new Button(
                        arg( "caption", "Re-add panel" )
                        .arg( "click", slot( "readdPanel" ) )
                    )
                )

                ,new GridLayout(
                    arg( "columns", 99 )
                    ,new Button()
                        .set( "name", "selectOnButton" )
                        .set( "caption", "Select on" )
                        .set( "click", slot( "selectOn" ) )
                    ,new Button()
                        .set( "name", "selectOffButton" )
                        .set( "caption", "Select off" )
                        .set( "click", slot( "selectOff" ) )
                        .set( "enabled", false )
                    ,new TextBox()
                        .set( "name", "currentPageName" )
                        .set( "writable", false )

                    ,new Button()
                        .set( "name", "selectingOnButton" )
                        .set( "caption", "Selecting on" )
                        .set( "click", slot( "selectingOn" ) )
                    ,new Button()
                        .set( "name", "selectingOffButton" )
                        .set( "caption", "Selecting off" )
                        .set( "enabled", false )
                        .set( "click", slot( "selectingOff" ) )

                    ,new Button()
                        .set( "name", "closingOnButton" )
                        .set( "caption", "Closing on" )
                        .set( "click", slot( "closingOn" ) )
                    ,new Button()
                        .set( "name", "closingOffButton" )
                        .set( "caption", "Closing off" )
                        .set( "enabled", false )
                        .set( "click", slot( "closingOff" ) )
                )

            ))        

        ));

        //set( "pages.selected", get( "pages.panelPage" ) );
        //pages = get( "pages" );
    }
    
    public void readdPanel( Event event ) {
        if( ! pages.contains( "panelPage" ) ) {
            pages.add( new Panel(
                arg( "title", "Panel Page (r)" )
                .arg( "closable", true )
                .arg( "name", "panelPage" )
            ));
        }
    }

    public void selectOn( Event event ) {
        if( event( "pages.select" ).size() == 0 ) {
            event( "pages.select" ).add( slot( "onSelect" ) );
        }
        set( "selectOnButton.enabled", false );
        set( "selectOffButton.enabled", true );
    }
    
    public void selectOff( Event event ) {
        if( event( "pages.select" ).size() > 0 ) {
            event( "pages.select" ).remove( slot( "onSelect" ) );
        }
        set( "selectOnButton.enabled", true );
        set( "selectOffButton.enabled", false );
    }
    
    public void onSelect( SelectEvent event ) {
        System.out.println( "Select " + event.get( "selected.name" ) );
        set( "currentPageName.value", event.getSelected().get( "name" ) );
    }

    public void selectingOn( Event event ) {
        if( event( "pages.selecting" ).size() == 0 ) {
            event( "pages.selecting" ).add( slot( "onSelecting" ) );
        }
        set( "selectingOnButton.enabled", false );
        set( "selectingOffButton.enabled", true );
    }
    
    public void selectingOff( Event event ) {
        if( event( "pages.selecting" ).size() > 0 ) {
            event( "pages.selecting" ).remove( slot( "onSelecting" ) );
        }
        set( "selectingOnButton.enabled", true );
        set( "selectingOffButton.enabled", false );
    }

    public void onSelecting( SelectEvent event ) {
        System.out.println( "Selecting " + event.getSelected().getCommonName() );
        if( contains( "panel2" ) && event.getSelected() == widget( "panel2" ) ) {
            add( new MessageBox( "Non puoi selezionare Panel 2" ) );
        } else {
            event.select();
            // oppure: pages.select( event.getSelected() );
        }
    }

    public void closingOn( Event event ) {
        if( event( "pages.closing" ).size() == 0 ) {
            event( "pages.closing" ).add( slot( "onClosing" ) );
        }
        set( "closingOnButton.enabled", false );
        set( "closingOffButton.enabled", true );
    }
    
    public void closingOff( Event event ) {
        if( event( "pages.closing" ).size() > 0 ) {
            event( "pages.closing" ).remove( slot( "onClosing" ) );
        }
        set( "closingOnButton.enabled", true );
        set( "closingOffButton.enabled", false );
    }
    
    public void onClosing( CloseEvent event ) {
        System.out.println( "Closing " + event.getClosed().getCommonName() );
        if( event.getClosed() == widget( "panel2" ) ) {
            add( new MessageBox( "Non puoi chiudere Panel 2" ) );
        } else {
            event.close();
            // oppure: pages.close( event.getClosed() );
        }
    }

    public static class TestPanel extends Panel {
        
        public void build() {
            set( "content", new BorderLayout(
                arg( "center", new GridLayout(
                    new TextBox()
                        .set( "name", "itxt2" )
                        .set( "label", new Label()
                            .set( "value", "Inner Campo 2" )
                        )
                        .set( "value", "val2")
                        .set( "length", 10 )

                    ,new DateTextBox()
                        .set( "name", "idate1" )
                        .set( "label", new Label()
                            .set( "value", "Inner Data" )
                        )
                ))
            ));
        }
    }

    public void loadPanelPage( Event event ) {
        //set( "panelPage.content", new TestForm().set( "name", "PanelPageInnerForm" ) );
        
        set( "panelPage.content", new TestPanel().set( "name", "PanelPageInner" ) );
    }
    
    public void blankPanelPage( Event event ) {
        set( "panelPage.content", null );
    }
    
    public void selectPanelPage( Event event ) {
        set( "pages.selected", get( "pages.panelPage" ) );
    }
    
    public void selectPage3( Event event ) {
        set( "pages.selected", get( "pages[ 2 ]" ) );
    }
    
    public void addBefore3( Event event ) {
        pages.place( new Panel(
            arg( "title", "New Page " + count++ )
            .arg( "content", new AddressForm() )
        )).before( pages.widget( 2 ) );
    }

    public void addStart( Event event ) {
        pages.place( new Panel(
            arg( "title", "Start Page " + count++ )
            .arg( "closable", true )
            .arg( "content", new TestForm() )
        )).first();
        //pages.select( 0 );
    }

    public void addEnd( Event event ) {
        Panel panel = new Panel(
            arg( "title", "End Page " + count++ )
            .arg( "closable", true )
            .arg( "content", new TestForm() )
        );
        pages.place( panel ).last();
        System.out.println( "Performing" );
        pages.select( panel );
        System.out.println( "Performed" );
    }
    
    public void removeFirst( Event event ) {
        pages.remove( 0 );
    }
    
    public void closePage( CloseEvent event ) {
        System.out.println( "Close " + event.getClosed().getCommonName() + ", size after close " + pages.size() );
    }

    public void swapClick( ClickEvent<Button> event ) {
        String value = get( "txtMother.value" );
        set( "txtMother.value", get( "txtFather.value" ) );
        set( "txtFather.value", value );
    }
    
    public void switchClick( ClickEvent event ) {
        if( this.<Integer>get( "gridLayout.columns" ) == 2 ) {
            set( "gridLayout.columns", 4 );
        } else {
            set( "gridLayout.columns", 2 );
        }
    }
    
    public void renameFirsts( Event event ) {
        if( pages.size() > 0 ) {
            set( "pages.children[0].title", "Renamed first" );
        }

        if( pages.size() > 1 ) {
            set( "pages.children[1].title", "Renamed second" );
        }
    }
    
}