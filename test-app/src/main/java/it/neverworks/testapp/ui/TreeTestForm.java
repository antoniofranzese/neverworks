package it.neverworks.testapp.ui;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.data.Item;
import it.neverworks.ui.data.Catalog;
import it.neverworks.ui.data.Artifact;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.tree.ItemClickEvent;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;
import it.neverworks.ui.types.Decorator;
import it.neverworks.ui.types.Decoration;

public class TreeTestForm extends Form {
    
    private int count = 1;
    
    private List items1;
    private List items2;
    private List rootedItems1;
    private List rootedItems1_1;
    private List rootedItems1_2;
    private List rootedItems2;
    private Decorator rowsBy2;
    private Decorator rowsBy3;
    
    @Property @Inject( "IconCatalog" )
    private Catalog icons;
    
    @Override
    public void build() {
        
        items1 = list(
            new Item( arg( "id", 1 ).arg( "name", "Item 1" ).arg( "icon", "folder" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ).arg( "icon", "folder" ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ).arg( "icon", "nas" ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", 2 ) .arg( "icon", "nas" ))
            ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ).arg( "icon", "folder" ) )
            ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ).arg( "parent", 5 ).arg( "icon", "tag" ) )
            ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 6 ).arg( "icon", "home" ) )
        );

        items2 = list(
            new Item( arg( "id", 21 ).arg( "name", "Other Item 1" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Other Item 2" ) )
            ,new Item( arg( "id", 23 ).arg( "name", "Other Item 3" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 24 ).arg( "name", "Other Item 4" ).arg( "parent", 23 ) )
            ,new Item( arg( "id", 25 ).arg( "name", "Other Item 5" ) )
            ,new Item( arg( "id", 26 ).arg( "name", "Other Item 6" ).arg( "parent", 25 ) )
            ,new Item( arg( "id", 27 ).arg( "name", "Other Item 7" ).arg( "parent", 25 ) )
            ,new Item( arg( "id", 28 ).arg( "name", "Other Item 8" ).arg( "parent", 25 ) )
            ,new Item( arg( "id", 29 ).arg( "name", "Other Item 9" ) )
        );

        rootedItems1 = list(
            new Item( arg( "id", "root1" ).arg( "name", "Root Item" ) )
            ,new Item( arg( "id", 1 ).arg( "name", "<b>Item 1</b>" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ).arg( "parent", 5 ) )
            ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 6 ) )
        );

        rootedItems1_1 = list(
            new Item( arg( "id", "root1" ).arg( "name", "Root Item" ) )
            ,new Item( arg( "id", 1 ).arg( "name", "Item 1" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Item 2.1" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 5 ).arg( "name", "Item 5.1" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 6 ).arg( "name", "Item 6.1" ).arg( "parent", 5 ) )
            ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 6 ) )
            ,new Item( arg( "id", 8 ).arg( "name", "Item 8" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 9 ).arg( "name", "Item 9" ).arg( "parent", 8 ) )
            ,new Item( arg( "id", 10 ).arg( "name", "Item 10" ).arg( "parent", 9 ) )
            ,new Item( arg( "id", 11 ).arg( "name", "Item 11" ).arg( "parent", 5 ) )
        );

        rootedItems1_2 = list(
            new Item( arg( "id", "root1" ).arg( "name", "Root Item" ) )
            ,new Item( arg( "id", 1 ).arg( "name", "Item 1" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ).arg( "parent", "root1" ) )
            ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 6 ) )
        );

        rootedItems2 = list(
            new Item( arg( "id", "root2" ).arg( "name", "Other Root Item" ) )
            ,new Item( arg( "id", 1 ).arg( "name", "Other Item 1" ).arg( "parent", "root2" ) )
            ,new Item( arg( "id", 2 ).arg( "name", "Other Item 2" ).arg( "parent", "root2" ) )
            ,new Item( arg( "id", 3 ).arg( "name", "Other Item 3" ).arg( "parent", 2 ) )
            ,new Item( arg( "id", 4 ).arg( "name", "Other Item 4" ).arg( "parent", 3 ) )
            ,new Item( arg( "id", 5 ).arg( "name", "Other Item 5" ).arg( "parent", "root2" ) )
            ,new Item( arg( "id", 6 ).arg( "name", "Other Item 6" ).arg( "parent", 5 ) )
            ,new Item( arg( "id", 7 ).arg( "name", "Other Item 7" ).arg( "parent", 5 ) )
        );
        
        rowsBy2 = new Decorator<Item>(){
            public Decoration decorate( Item item ) {
                if( item.<Integer>get( "id" ) % 2 == 0 ) {
                    return new Decoration( 
                        arg( "style", "oddrow" ) 
                        .arg( "background", "lightgray" )
                    );
                } else {
                    return null;
                }
            }
        };

        rowsBy3 = new Decorator<Item>(){
            public Decoration decorate( Item item ) {
                Decoration decoration;
                if( item.<Integer>get( "id" ) % 3 == 0 ) {
                    decoration = new Decoration( 
                        arg( "style", "threerow" ) 
                        .arg( "background", "yellow" )
                    );
                } else {
                    decoration = new Decoration();
                }

                if( item.<Integer>get( "id" ) % 2 == 0 ) {
                    decoration.set( "!font.weight", "bold" );
                }
                    
                return decoration;
            }
        };
        
        set( "main", new BorderLayout(
            arg( "gutters", true )

            .arg( "left", new Tree(
                arg( "name", "implicitRootTree" )
                .arg( "key", "id" )
                .arg( "caption", "<i>{name}</i>" )
                .arg( "items", items1 )
                .arg( "expanded", true )
                .arg( "click", slot( "clickItem" ) )
                .arg( "width", 200 )
                .arg( "icon", "icon" )
                .arg( "icons",
                    arg( "folder", 
                        arg( "opened", icon( "folderopen@16" ) )
                        .arg( "closed", icon( "folderclose@16" ) ) 
                    ) 
                    .arg( "nas", icon( "nas@16" ) ) 
                    .arg( "tag", icon( "tag@16" ) ) 
                    .arg( "home", icon( "home@16" ) ) 
                )
                .arg( "rows", rowsBy2 )
                .arg( "encoding", "html" )
            ))

            .arg( "right", new Tree(
                arg( "name", "rootedTree" )
                .arg( "key", "id" )
                .arg( "caption", new ItemFormatter<Item>(){
                    public Object format( Item item ) {
                        if( item.get( "id" ) instanceof Integer && item.<Integer>get( "id" ) % 2 == 0 ) {
                            return "<span class=\"square\"></span><span class=\"text\">" + item.get( "name") + "</span>";
                        } else {
                            return item.get( "name" );
                        }
                    }
                })
                .arg( "items", rootedItems1 )
                .arg( "root", rootedItems1.get( 0 ) )
                .arg( "click", slot( "clickItem" ) )
                .arg( "width", 200 )
                .arg( "encoding", "html" )
            ))
                    
            .arg( "bottom", new GridLayout(
                arg( "width", "100%" )
                ,new ToolBar(
                    new Button(
                        arg( "caption", "Other" )
                        .arg( "click", slot( "loadOther" ) )
                    )
                    ,new Button(
                        arg( "caption", "Original" )
                        .arg( "click", slot( "loadOriginal" ) )
                    )
                    ,new Button(
                        arg( "caption", "Other Dec" )
                        .arg( "click", slot( "loadOtherDecorator" ) )
                    )
                    ,new Button(
                        arg( "caption", "Disable" )
                        .arg( "click", slot( "toggleDisable" ) )
                    )
                )
                ,new ToolBar(
                    arg( "layout", arg( "halign", "right" ) )
                    ,new Button(
                        arg( "caption", "Other" )
                        .arg( "click", slot( "loadRootedOther" ) )
                    )
                    ,new Button(
                        arg( "caption", "Original" )
                        .arg( "click", slot( "loadRootedOriginal" ) )
                    )
                    ,new Button(
                        arg( "caption", "Modified" )
                        .arg( "click", slot( "loadRootedModified" ) )
                    )
                    ,new Button(
                        arg( "caption", "Changed parents" )
                        .arg( "click", slot( "loadRootedAdopted" ) )
                    )
                )
            ))
        ));

    }
    
    public void clickItem( ItemClickEvent event ) {
        System.out.println( "Clicked " + ((Item) event.getItem()).get( "name" ) + " at " + event.getX() + ", " + event.getY() + " with " + event.getButton() );
    }
    
    public void loadOther( Event event ) {
        set( "implicitRootTree.items", items2 );
        set( "implicitRootTree.rows", rowsBy3 );
    }

    public void loadOtherDecorator( Event event ) {
        set( "implicitRootTree.rows", rowsBy3 );
    }

    public void loadOriginal( Event event ) {
        set( "implicitRootTree.items", items1 );
        set( "implicitRootTree.rows", rowsBy2 );
    }
    
    public void loadRootedOther( Event event ) {
        set( "rootedTree.items", rootedItems2 );
        set( "rootedTree.root", rootedItems2.get( 0 ) );
    }

    public void loadRootedOriginal( Event event ) {
        set( "rootedTree.items", rootedItems1 );
        set( "rootedTree.root", rootedItems1.get( 0 ) );
    }
    
    public void loadRootedModified( Event event ) {
        set( "rootedTree.items", rootedItems1_1 );
        set( "rootedTree.root", rootedItems1_1.get( 0 ) );
    }

    public void loadRootedAdopted( Event event ) {
        set( "rootedTree.items", rootedItems1_2 );
        set( "rootedTree.root", rootedItems1_2.get( 0 ) );
    }
    
    public void toggleDisable( Event event ) {
        boolean disable = event.get( "source.caption" ).equals( "Disable" );
        set( "implicitRootTree.disabled", disable );
        event.set( "source.caption", disable ? "Enable" : "Disable" );
    }
    
    public Catalog getIcons(){
        return this.icons;
    }
    
    public void setIcons( Catalog icons ){
        this.icons = icons;
    }
    
    public Artifact icon( String value ) {
        return getIcons().get( value );
    }
    
}