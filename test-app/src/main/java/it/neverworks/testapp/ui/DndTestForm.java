package it.neverworks.testapp.ui;

import java.util.Date;
import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.events.Event;
import it.neverworks.model.io.Internal;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Image;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.TabLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.LabelPanel;
import it.neverworks.ui.data.Catalog;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.dnd.Avatar;
import it.neverworks.ui.dnd.DropRules;

public class DndTestForm extends Form {
    
    @Property @Inject( "IconCatalog" )
    private Catalog icons;
    
    public void build() {
        
        List treeItems = list(
             new Item( arg( "id", "root" )                             .arg( "name", "Elements" ) )           
            ,new Item( arg( "id", "CUGS" )                             .arg( "name", "CUGS" )               .arg( "parent", "root" )                         .arg( "icon", null ) )                             
            ,new Item( arg( "id", "CUGS:CNT" )                         .arg( "name", "CNT" )                .arg( "parent", "CUGS" )                         .arg( "icon", null ) )                            
            ,new Item( arg( "id", "CUGS:CNT:OSM" )                     .arg( "name", "OSM" )                .arg( "parent", "CUGS:CNT" )                     .arg( "icon", null ) )                            
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server" )          .arg( "name", "osm-server" )         .arg( "parent", "CUGS:CNT:OSM" )                 .arg( "icon", null ) )                            
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:C" )        .arg( "name", "CPU" )                .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "cpu" ) )   //     host: "osm-server",  
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:R" )        .arg( "name", "RAM" )                .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "memory" ) )   //  host: "osm-server",  
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:D" )        .arg( "name", "Disk (/opt)" )        .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "disk" ) )   //    host: "osm-server",      pm: "/opt" }
            ,new Item( arg( "id", "CUGS:CNT:OSM:osm-server:P" )        .arg( "name", "OSMMRM_XMgrMain" )    .arg( "parent", "CUGS:CNT:OSM:osm-server" )      .arg( "icon", "process" ) )   // host: "osm-server",      om: "OSMMRM_XMgrMain" }
            ,new Item( arg( "id", "CUGS:PM01" )                        .arg( "name", "PM01" )               .arg( "parent", "CUGS" )                         .arg( "icon", null ) )  
            ,new Item( arg( "id", "CUGS:PM01:osmPF1" )                 .arg( "name", "osmPF1" )             .arg( "parent", "CUGS:PM01" )                    .arg( "icon", null ) )                           
            ,new Item( arg( "id", "CUGS:PM01:osmPF1:pm01-server" )     .arg( "name", "pm01-server" )        .arg( "parent", "CUGS:PM01:osmPF1" )             .arg( "icon", null ) )                           
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:C" )   
                .arg( "name", "CPU" )                
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "cpu" )      
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "cpu" ) 
                ) 
            )   //     prp:{ $type: "element", icon: "cpu",     host: "pm01-server" },                           host: "pm01-server",  }
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:R" )   
                .arg( "name", "RAM" )                
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "memory" )   
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "memory" ) 
                ) 
            )   //  prp:{ $type: "element", icon: "memory",  host: "pm01-server" },                           host: "pm01-server",  }
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:D1" )  
                .arg( "name", "Disk (/opt)" )        
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "disk" )     
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "/opt" ) 
                ) 
            )   //    prp:{ $type: "element", icon: "disk",    host: "pm01-server", resource: "/opt" },         host: "pm01-server", , pm: "/opt" }
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:D2" )  
                .arg( "name", "Disk (/archive)" )    
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "disk" )     
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "/archive" ) 
                ) 
            )   //    prp:{ $type: "element", icon: "disk",    host: "pm01-server", resource: "/archive" },     host: "pm01-server", , pm: "/archive" }
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:P1" )  
                .arg( "name", "APES" )               
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "process" )  
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "APES" ) 
                ) 
            )   // prp:{ $type: "element", icon: "process", host: "pm01-server", resource: "APES" },         host: "pm01-server", , pm: "APES"        }
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:P2" )  
                .arg( "name", "osmPF" )              
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "process" )  
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "osmPF" ) 
                ) 
            )   // prp:{ $type: "element", icon: "process", host: "pm01-server", resource: "osmPF" },        host: "pm01-server", , pm: "osmPF"       }
            ,new Item( 
                arg( "id", "CUGS:PM01:osmPF1:pm01-server:P3" )  
                .arg( "name", "OSMAM_XMain" )        
                .arg( "parent", "CUGS:PM01:osmPF1:pm01-server" ) 
                .arg( "icon", "process" )  
                .arg( "proposal", new CustomProposal()
                    .set( "type", "tag" )
                    .set( "value", "OSMAM_XMain" ) 
                ) 
            )   // prp:{ $type: "element", icon: "process", host: "pm01-server", resource: "OSMAM_XMain" },  host: "pm01-server", , pm: "OSMAM_XMain" }
        
        );
        
        add( new Avatar(
            arg( "name", "avatar" )
            .arg( "enjoy",   "testAvatarEnjoy" )
            .arg( "neglect", "testAvatarNeglect" )
            .arg( "ignore",  "testAvatarIgnore" )
        ));

        add( new BorderLayout(
            arg( "gutters", true )

            .arg( "left", new GridLayout(
                arg( "columns", 1 )
                .arg( "width", 40 )
                ,new Image()
                    .set( "source", get( "icons[home@32]" ) )
                    .set( "click", slot( "imageClick" ) )
                    .set( "avatar", get( "avatar" ) )
                    .set( "proposal", 
                        arg( "type", "home" ) 
                        .arg( "value", 10 ) 
                    )
                ,new Image()
                    .set( "source", get( "icons[globe@32]" ) )
                    .set( "click", slot( "imageClick" ) )
                    .set( "avatar", get( "avatar" ) )
                    .set( "proposal", new CustomProposal()
                        .set( "type", "globe" ) 
                        .set( "value", 20 ) 
                    )
                ,new Image()
                    .set( "source", get( "icons[tag@32]" ) )
                    .set( "click", slot( "imageClick" ) )
                    .set( "avatar", get( "avatar" ) )
                    .set( "proposal", new CustomProposal()
                        .set( "type", "tag" ) 
                        .set( "value", 30 ) 
                    )
            ))

            .arg( "center", new GridLayout(
                arg( "columns", 1 )
                ,new Panel(
                    arg( "name", "panel1" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Homes only" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                    .arg( "acceptance", new DropRules().by( "type" ).eq( "home" ) )
                    .arg( "drop", slot( "drop" ) )
                )
                ,new Panel(
                    arg( "name", "panel2" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Globes only" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                    .arg( "acceptance", new DropRules().by( "type" ).eq( "globe" ) )
                    .arg( "drop", slot( "drop" ) )
                )
                ,new Panel(
                    arg( "name", "panel3" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Tags only" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                    .arg( "acceptance", new DropRules().by( "type" ).eq( "tag" ) )
                    .arg( "drop", slot( "drop" ) )
                )
                ,new Panel(
                    arg( "name", "panel4" )
                    .arg( "border.size", 1 )
                    .arg( "content", new Label()
                        .set( "value", "Tags or globes" )
                        .set( "width", 300 )
                        .set( "height", 100 )
                    )
                    .arg( "acceptance", new DropRules().by( "type" ).in( "globe", "tag" ) )
                    .arg( "drop", slot( "drop" ) )
                )
                ,new TextBox()
                    .set( "name", "log" )
                    .set( "width", 300 )
                    .set( "writable", false )
            ))
            
            .arg( "right", new Tree()
                .set( "name", "elements" )
                .set( "items", treeItems )
                .set( "root", treeItems.get( 0 ) )
                .set( "width", 300 )
                .set( "expanded", true )
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "link", "parent" )
                .set( "proposal", "proposal" )
                .set( "avatar", get( "avatar" ) )
                .set( "icon", "icon" )
                .set( "icons", 
                    arg( "cpu",      get( "icons[icon16-cpu]" ) )
                    .arg( "memory",  get( "icons[icon16-memory]" ) )
                    .arg( "disk",    get( "icons[icon16-disk]" ) ) 
                    .arg( "process", get( "icons[icon16-process]" ) )
                )
            )

        ));
        
    }

    public void drop( Event event ) {
        System.out.println( "Drop from " + event.get( "originator" ) );
        System.out.println( "Dropped " + event.get( "proposal" ) );
        String str = event.get( "proposal.type" ) + ( event.get( "!proposal.value" ) != null ? " (" + event.get( "!proposal.value" ) + ")" : "" );
        
        event.set( "source.content.value", event.get( "source.content.value" ) + ", " + str );
    }
    
    public static class CustomProposal extends BaseModel {
        @Property
        private String type;
        
        @Property @Internal
        private Object value;
        
        public Object getValue(){
            return this.value;
        }
        
        public void setValue( Object value ){
            this.value = value;
        }
        
        public String getType(){
            return this.type;
        }
        
        public void setType( String type ){
            this.type = type;
        }
    }
    
    public void imageClick( Event event ) {
        set( "log.value", "Image click: " + new Date() );
    }
}