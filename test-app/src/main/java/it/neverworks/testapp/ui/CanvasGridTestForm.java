package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.DateMidnight;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.encoding.HTML;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.BaseTextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.data.ListDataSource;
import it.neverworks.ui.data.ItemFormatter;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;

import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.grid.GridHeader;
import it.neverworks.ui.grid.SelectEvent;
import it.neverworks.ui.grid.PropertyCell;
import it.neverworks.ui.grid.HTMLCell;
import it.neverworks.ui.grid.CanvasCell;

import it.neverworks.ui.canvas.Scene;

import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ResetCapable;

public class CanvasGridTestForm extends Form {
    
    List<Item> items1;
    List<Item> items2;

    public class HugeDataSource extends ListDataSource {
        private String prefix;

        public HugeDataSource( String prefix ) {
            super( new ArrayList() );
            this.prefix = prefix;
        }
        
        public Iterable slice( int start, int count ) {
            List results = new ArrayList();
            for( int i = start; i < start + count; i++ ) {
                results.add( item( i ) );
            }                
            return results;  
        }                    
                             
        public Object findByKey( String name, Object value ) {
            if( name.equals( "id" ) ) {
                return item( Integer.parseInt( value.toString() ) );
            } else {
                throw new RuntimeException( "Unknown key: " + name );
            }
        }                     
                             
        protected Item item( int id ) {
            return new Item(
                arg( "id", id )
                .arg( "name", prefix + " Name " + id )
                .arg( "surname", prefix + " Surname " + id )
                .arg( "progress", Numbers.Int( Numbers.random( 0, 100 ) ) )
                .arg( "sales", range( 5 ).map( i -> Numbers.Int( Numbers.random( 10, 50 ) ) ).list() )
            );
        }                    
        
        public int size() {
            return 1000;
        }
        
        public List build() {
            List results = new ArrayList();
            for( int i = 0; i < size(); i++ ) {
                results.add( item( i ) );
            }
            return results;
        }
    }
    
    public void build() {
        set( "title", "Canvas Grid Test" );
        
        set( "main", new BorderLayout()
            .set( "center", new Grid()
                .set( "name", "grid1" )
                .set( "selector", false )
                .set( "dividers", "horizontal" )
                .set( "columns", new GridHeader(
                    arg( "dividers", "horizontal" )
                    ,new GridColumn(
                        arg( "title", "Name" )
                        .arg( "source", new HTMLCell(
                            arg( "property", "surname" )
                            .arg( "format", (ItemFormatter<Item>) item -> {
                                // return HTML
                                //     .table()
                                //         .row()
                                //             .cell()
                                //                 .style( "font-weight", "bold" )
                                //                 .text( item.get( "surname" ) )
                                //             .end()
                                //         .end()
                                //         .row()
                                //             .cell()
                                //                 .text( item.get( "name" ) )
                                //             .end()
                                //         .end()
                                //     .end();
                                return null;
                            })
                        ))
                    )
                    ,new GridColumn()
                        .set( "title", "Sales" )
                        .set( "width", 110 )
                        .set( "sortable", false )
                        .set( "source", new CanvasCell(
                            arg( "width", 110 )
                            .arg( "height", 60 )
                            .arg( "name", "bar" )
                            .arg( "format", (ItemFormatter<Item>) item -> {
                                List<Integer> sales = item.get( "sales" );
                                Scene scene = new Scene();
                                for( int i: range( sales ) ) {
                                    scene.rectangle(
                                        arg( "place", arg( "x", 5 + i * 20 ).arg( "y", 55 ) )
                                        .arg( "height", 19 )
                                        .arg( "width", sales.get( i ) )
                                        .arg( "rotation", -90 )
                                        .arg( "fill", "0066cc" )
                                    );
                                }
                                return scene;
                            })
                        ))


                    ,new GridColumn()
                        .set( "title", "Progress" )
                        .set( "width", 64 )
                        .set( "source", new CanvasCell(
                            arg( "property", "progress" )
                            .arg( "width", 64 )
                            .arg( "height", 64 )
                            .arg( "format", (ItemFormatter<Item>) item -> {
                                Integer progress = item.get( "progress" );
                                return new Scene()
                                    .ring(
                                        arg( "place", arg( "x", 30 ).arg( "y", 30 ) )
                                        .arg( "width", 13 )
                                        .arg( "radius", 30 )
                                        .arg( "fill", "lightgray" )
                                        .arg( "shadow", list( "gray", 2, 2, 2 ) )
                                    )
                                    .ring(
                                        arg( "place", arg( "x", 30 ).arg( "y", 30 ) )
                                        .arg( "width", 13 )
                                        .arg( "radius", 30 )
                                        .arg( "fill", progress < 30 ? "red" : ( progress > 70 ? "green" : "f2f20d" ) )
                                        .arg( "from", 0 )
                                        .arg( "to", 360 * progress / 100 )
                                    )
                                    .text(
                                        arg( "place", arg( "x", 30 ).arg( "y", 30 ) )
                                        .arg( "value", msg( "{progress,number}%", item ) )
                                        .arg( "halign", "center" )
                                        .arg( "baseline", "middle" )
                                        .arg( "font", arg( "size", "13px" ).arg( "family", "Helvetica" ) )
                                        .arg( "fill", "black" )
                                    );
                            })
                        ))
                ))
                .set( "key", "id" )
                .set( "items", new HugeDataSource( "First" ).build() )
            )

            .set( "top", new ToolBar(
                new Button()
                    .set( "caption", "Button 1" )
                    .set( "click", slot( evt -> {

                    }))
                ,new Button()
                    .set( "caption", "Button 2" )
                    .set( "click", slot( evt -> System.out.println( "click" ) ) )
            ))

        );
        
    }
    
}
