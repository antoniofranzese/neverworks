package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.model.events.Event;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.LabelPanel;
import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.Widget;

public class LabelPaneTestForm extends Form {
    
    public void build() {
        add( new BorderLayout(
            arg( "top", new LabelPanel(
                arg( "height", 100)
                .arg( "title", "Pannello di sopra" )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "content", new Label( 
                    arg( "value", "sopra" ) 
                )
            )) )
            
            .arg( "center", new Panel(
                arg( "content", new LabelPanel(
                    arg( "name", "centerPanel" )
                    .arg( "position", "left" )
                    .arg( "title", "Il mio pannello" )
                    .arg( "content", createComplexWidget() )
                )) 
            ))
            
            .arg( "bottom", new LabelPanel(
                arg( "title", "Pannello di sotto" )
                .arg( "position", "bottom")
                .arg( "height", 100 )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "content", new ToolBar(
                    arg( "halign", "right" )
                    ,new Button(
                        arg( "caption", "Change" )
                        .arg( "click", slot( "changeCenter" ) )
                    )
                ))
            )) 
            
            .arg( "left", new LabelPanel(
                arg( "title", "Sinistra" )
                .arg( "width", 100 )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "content", new Label( 
                    arg( "value", "sinistra" ) ) 
                )
            )) 
            .arg( "right", new LabelPanel(
                arg( "title", "Destra" )
                .arg( "position", "right" )
                .arg( "width", 100 )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "content", new Label( 
                    arg( "value", "destra" ) ) 
                )
            )) 
        ));
    }
    
    public void changeCenter( Event event ) {
        if( get( "centerPanel.content" ) instanceof BorderLayout ) {
            set( "centerPanel.content", createSimpleWidget() );
        } else {
            set( "centerPanel.content", createComplexWidget() );
        }
    }
    
    public void simpleClick( Event event ) {
        add( new MessageBox( arg( "message", "Simple Click" ) ) );
    }
    
    private Widget createSimpleWidget() {
        return new GridLayout(
            new TextBox(
                arg( "name", "text1" )
                .arg( "label", "Text 1" )
            )
            ,new TextBox(
                arg( "name", "text2" )
                .arg( "label", "Text 2" )
            )
            ,new Button(
                arg( "caption", "ClickMe" )
                .arg( "click", slot( "simpleClick" ) )
            )
        );
    }
    
    private Widget createComplexWidget() {
        return new BorderLayout(
            arg( "gutters", true )
            .arg( "title", "Complex layout")
            .arg( "layout", 
                arg( "width", 500 )
                .arg( "height", 300 )
                .arg( "closable", true ) 
            )
            .arg( "top", new Panel(
                arg( "height", 50 )
            ))
            .arg( "left", new Tree(
                arg( "width", 140 )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "expanded", true )
                .arg( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Item 1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ).arg( "parent", 1 ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", 2 ) )
                    ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ).arg( "parent", 1 ) )
                    ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ).arg( "parent", 5 ) )
                    ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 5 ) )
                    ,new Item( arg( "id", 8 ).arg( "name", "Item 8" ).arg( "parent", 7 ) )
                    ,new Item( arg( "id", 9 ).arg( "name", "Item 9" ).arg( "parent", 7 ) )
                    ,new Item( arg( "id", 10 ).arg( "name", "Item 10" ).arg( "parent", 5 ) )
                    
                ))
            ))
            .arg( "center", new Grid(
                arg( "name", "grid1" )
                .arg( "columns", list(
                     new GridColumn(
                         arg( "title", "Column 1" )
                         .arg( "source", "prop1" )
                         .arg( "width", "34%" )
                     )
                     ,new GridColumn(
                         arg( "title", "Column 2" )
                         .arg( "source", "prop2" )
                         .arg( "width", "33%" )
                     )
                     ,new GridColumn(
                         arg( "title", "Column 3" )
                         .arg( "source", "prop3" )
                         .arg( "width", "33%" )
                     )
                 ))
                 .arg( "items", list(
                     new Item( arg( "id", 1 ).arg( "prop1", "Property 1.1" ).arg( "prop2", "Property 2.1" ).arg( "prop3", "Property 3.1" ) )
                     ,new Item( arg( "id", 2 ).arg( "prop1", "Property 1.2" ).arg( "prop2", "Property 2.2" ).arg( "prop3", "Property 3.2" ) )
                     ,new Item( arg( "id", 3 ).arg( "prop1", "Property 1.3" ).arg( "prop2", "Property 2.3" ).arg( "prop3", "Property 3.3" ) )
                     ,new Item( arg( "id", 4 ).arg( "prop1", "Property 1.4" ).arg( "prop2", "Property 2.4" ).arg( "prop3", "Property 3.4" ) )
                     ,new Item( arg( "id", 5 ).arg( "prop1", "Property 1.5" ).arg( "prop2", "Property 2.5" ).arg( "prop3", "Property 3.5" ) )
                     ,new Item( arg( "id", 6 ).arg( "prop1", "Property 1.6" ).arg( "prop2", "Property 2.6" ).arg( "prop3", "Property 3.6" ) )
                     ,new Item( arg( "id", 7 ).arg( "prop1", "Property 1.7" ).arg( "prop2", "Property 2.7" ).arg( "prop3", "Property 3.7" ) )
                     ,new Item( arg( "id", 8 ).arg( "prop1", "Property 1.8" ).arg( "prop2", "Property 2.8" ).arg( "prop3", "Property 3.8" ) )
                     ,new Item( arg( "id", 9 ).arg( "prop1", "Property 1.9" ).arg( "prop2", "Property 2.9" ).arg( "prop3", "Property 3.9" ) )
                     ,new Item( arg( "id", 10 ).arg( "prop1", "Property 1.10" ).arg( "prop2", "Property 2.10" ).arg( "prop3", "Property 3.10" ) )
                 ))
            ))
        );
    }
        
}