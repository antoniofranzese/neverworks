package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.encoding.JSON;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Promise;
import it.neverworks.model.utils.Identification;
import it.neverworks.ui.web.WebView;

public class MapComponent extends WebView {
    
    @Identification( string = { "lat", "lon" })
    public static class MapMoveEvent extends Event {
        @Property
        private Double lat;
        
        @Property
        private Double lon;
    }
    
    @Identification( string = { "marker", "x", "y" })
    public static class MarkerClickEvent extends Event {
        @Property 
        private Integer x;
        
        @Property
        private Integer y;
        
        @Property
        private Integer marker;
    }

    @Property
    protected Signal<MapMoveEvent> move;

    @Property
    protected Signal<Event> ready;
    
    @Property
    protected Signal<MarkerClickEvent> markerClick;
    
    @Property
    private MapCoordinates center;
    
    public MapComponent() {
        super();
    }
    
    public MapComponent( Arguments arguments ) {
        super();
        set( arguments );
    }

    public void build() {
        set( "location", "/maps/index.html" );

        on( "load", signal( "ready" ) );

        on( "idle", slot( event -> {
            signal( "move" ).fire( 
                arg( "lat", event.get( "result.lat" ) )
                .arg( "lon", event.get( "result.lng" ) )
            );
        }));
    }
    
    public void loadPoints( Object points ) {
        this.execute( msg( "MapApp.loadPoints( {0,json,js} );", points ) );
    }
    
    public MapCoordinates getCenter(){
        return this.center;
    }
    
    public void setCenter( MapCoordinates center ){
        this.center = center;
    }
    
}