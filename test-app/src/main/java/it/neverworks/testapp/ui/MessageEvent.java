package it.neverworks.testapp.ui;

import it.neverworks.model.events.Event;

public class MessageEvent extends Event {
    
    protected String message;
    
    public String getMessage(){
        return this.message;
    }
    
    public void setMessage( String message ){
        this.message = message;
    }

    public MessageEvent( String message ) {
        this.message = message;
    }

}