package it.neverworks.testapp.ui;

import java.util.List;
import java.io.File;

import static it.neverworks.language.*;

import it.neverworks.lang.Application;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Reference;
import it.neverworks.io.FileInfo;
import it.neverworks.ui.web.Conversation;
import it.neverworks.ui.web.Downloader;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.model.Value;

import it.neverworks.io.FileRepository;
import it.neverworks.io.FileBadge;

public class ConversationTestForm extends Form {
    
    @Property @Inject( "TemporaryFileRepository" )
    private FileRepository files;
    
    private int counter = 0;
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 ) 
            
            ,new TextBox()
                .set( "name", "valueBox")
                .set( "label", "Value" )
                 .set( "width", 400 )


            ,new ToolBar(
                arg( "name", "buttons" )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )

                ,new Button()
                    .set( "caption", "Send Alert")
                    .set( "click", slot( "sendAlert" ) )

                ,new Button()
                    .set( "caption", "Change value")
                    .set( "click", slot( "changeClick" ) )
                ,new Button()
                    .set( "caption", "Download" )
                    .set( "click", slot( evt -> {
                        widget( "downloader" )
                            .set( "source", files.putStream( Application.resource( "test/ui/testapp/test.pdf" ) ) )
                            .set( "fileName", "my.pdf" )
                            .set( "type", "application/pdf" );
                    }))
            )
        ));
        
        set( "conversation", new Conversation(
            arg( "channel", "TestChannel" )
            .arg( "change", slot( "changeState" ) )
            .arg( "request", slot( "handleRequest" ) )
            .arg( "alert", slot( "handleAlert" ) )
        ));

        set( "downloader", new Downloader() );
        
        set( "conversation2", new Conversation(
            arg( "channel", "AppCookies" )
            .arg( "value", arg( "cookies", "TwoCookies" ) )
        ));
    }
    
    public void changeState( Event event ) {
        System.out.println( "CHANGED: " + event.get( "value" ) + " " + event.get( "value" ).getClass() );
        set( "valueBox.value", event.get( "value" ) );
    }
    
    public void changeClick( Event event ) {
        set( "conversation.value", arg( "message", get( "valueBox.value" ) ) );
    }
    
    public void handleRequest( Event event ) {
        System.out.println( "Request: " + event.get( "request" ) );
        
        if( event.value( "!request.action" ).equals( "time" ) ) {
            for( Value id: event.value( "!request.ids" ).each() ) {
                System.out.println( "ID: " + id.toInt() );
            }
            
            event.reply( arg( "now", date() ) );
        
        } else if( event.value( "!request.action" ).equals( "count" ) ) {
            event.reply( 
                arg( "count", ++counter ) 
                //.arg( "file", files.putStream( Application.resource( "test/ui/testapp/test.pdf" ) ) )
            );
        }
    }

    public void handleAlert( Event event ) {
        System.out.println( "Message: " + event.get( "topic" ) + "->" + event.get( "value" ) );
        add( new MessageBox( event.<String>get( "!value.text" ) ) );
    }
    
    public void sendAlert( Event event ) {
        value( "conversation.send" ).call( "alert", arg( "text", get( "valueBox.value" ) ) ); 
    }

}