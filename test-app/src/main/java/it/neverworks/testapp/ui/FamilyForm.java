package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;

public class FamilyForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            arg( "border", 
                arg( "outer.size", 1 )
                .arg( "outer.style", "double" )
                .arg( "inner",
                    arg( "size", 1 )
                    .arg( "style", "dotted" )
                )
            )
            ,new TextBox()
                .set( "name", "txtFather" )
                .set( "label", "Padre" )
            ,new TextBox()
                .set( "name", "txtMother")
                .set( "label", "Madre" )
            ,new ToolBar(
                new Button()
                    .set( "name", "btnRelocate" )
                    .set( "caption", "Scambia")
                    .set( "click", UI.Slot( this, "relocateClick" ) )
                ,new Button()
                    .set( "name", "btnSwitch" )
                    .set( "caption", "Switch")
                    .set( "click", UI.Slot( this, "switchClick" ) )
                ,new Button()
                    .set( "caption", "Border")
                    .set( "click", UI.Slot( this, "borderClick" ) )

                ).set( "name", "buttons" )            
                .set( "layout", UI
                    .set( "hspan", "full" )
                    .set( "halign", "right" ) 
                )
                        
            )
            .set( "columns", 2 ) 
        );
        
        if( form() != null ) {
            set( "startup", form().slot( "startup" ) );
        }
        
    }
    
    public void relocateClick( ClickEvent<Button> event ) {
        System.out.println( "RELOCATE F" );
        String value = get( "txtMother.value" );
        set( "txtMother.value", get( "txtFather.value" ) );
        set( "txtFather.value", value );
    }
    
    public void switchClick( ClickEvent event ) {
        System.out.println( "SWITCH F" );

        if( this.<Integer>get( "main.columns" ) == 2 ) {
            set( "main.columns", 4 );
        } else {
            set( "main.columns", 2 );
        }
    }
    
    public void borderClick( ClickEvent event ) {
        set( "main.border.outer.color", "red" );
        set( "main.border.inner.style", "solid" );
    }
    
}