package it.neverworks.testapp.ui;

import com.fasterxml.jackson.databind.JsonNode;
import static it.neverworks.language.*;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.ToStringBuilder;

@Convert( MapCoordinatesConverter.class )
public class MapCoordinates extends BaseModel implements JSONEncodable {
    
    @Property
    private Double latitude;
    
    @Property
    private Double longitude;
    
    public Double getLongitude(){
        return this.longitude;
    }
    
    public void setLongitude( Double longitude ){
        this.longitude = longitude;
    }
    
    public Double getLatitude(){
        return this.latitude;
    }
    
    public void setLatitude( Double latitude ){
        this.latitude = latitude;
    }
    
    public JsonNode toJSON() {
        return JSON.lang.encode( arg( "latitude", latitude ).arg( "longitude", longitude ) );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "latitude" )
            .add( "longitude" )
            .toString();
    }
}