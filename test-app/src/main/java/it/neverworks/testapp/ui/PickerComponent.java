package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.encoding.JSON;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Promise;
import it.neverworks.ui.web.WebView;

public class PickerComponent extends WebView {
    
    @Property
    protected Signal<Event> ready;
    
    
    public PickerComponent() {
        super();
    }
    
    public PickerComponent( Arguments arguments ) {
        super();
        set( arguments );
    }

    public void build() {
        set( "location", "/nw3/webView/palette/index.html" );
        // La presenza dell'oggetto di namespace indica che l'applicazione client e' stata inizializzata
        waitObject( "window.PickerApp" ).then( slot( "appInit" ) );
    }
    
    public void appInit( Event event ) {
        // Aspetta il deferred che indica il primo caricamento completo
        signal( "ready" ).fire();
    }
    
    protected Promise waitObject( String objectName ) {
        // Aspetta che un oggetto sia definito
        return wait( "require([ 'works/wait' ], function( wait ){ wait( function(){ return " + objectName + " !== undefined }).then( callback ); });" );
    }
    
}