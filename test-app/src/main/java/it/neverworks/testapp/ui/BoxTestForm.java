package it.neverworks.testapp.ui;

import it.neverworks.lang.Objects;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.form.TimeTextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;

public class BoxTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(
            new TextBox()
                .set( "name", "text" )
                .set( "label", "TextBox" )
            ,new DateTextBox()
                .set( "name", "date")
                .set( "label", "DateTextBox" )
            ,new TimeTextBox()
                .set( "name", "time")
                .set( "label", "TimeTextBox" )
            ,new ToolBar(
                new Button()
                    .set( "name", "btnTest" )
                    .set( "caption", "Test")
                    .set( "click", UI.Slot( this, "testClick" ) )

                ).set( "name", "buttons" )            
                .set( "layout", UI
                    .set( "hspan", "full" )
                    .set( "halign", "right" ) 
                )
                        
            )
            .set( "columns", 2 ) 
        );
    }
    
    public void testClick( ClickEvent<Button> event ) {
        System.out.println( "Text: " + get( "text.value" ) + " " + Objects.className( get( "text.value" ) ) );
        System.out.println( "Date: " + get( "date.value" ) + " " + Objects.className( get( "date.value" ) ) );
        System.out.println( "Time: " + get( "time.value" ) + " " + Objects.className( get( "time.value" ) ) );
    }
    
}