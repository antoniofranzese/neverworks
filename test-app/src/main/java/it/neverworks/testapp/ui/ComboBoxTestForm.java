package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;

import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ComboBox;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.data.Item;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Slot;

public class ComboBoxTestForm extends Form {
    
    private static List items1 = list( "uno", "due", "tre" );
    private static List items2 = list( 
        new Item( arg( "label", "one" ) ), 
        new Item( arg( "label", "two" ) ), 
        new Item( arg( "label", "three" ) ), 
        new Item( arg( "label", "four" ) ) 
    );
    
    
    public void build() {
        set( "main", new GridLayout(
            new ComboBox(
                arg( "name", "combo1" )
                .arg( "label", "combo1" )
                .arg( "change", new Slot( this, "comboChange" ) )
                .arg( "items", items1 )
            )
            
            ,new TextBox(
                arg( "name", "text1" )
                .arg( "writable", false )
                .arg( "label", "On Change" )
            )
            
            ,new ToolBar(
                arg( "layout", arg( "hspan", "full" ) )
                ,new Button(
                    arg( "name", "swapButton" )
                    .arg( "caption", "Items 2" )
                    .arg( "click", slot( "swapItems" ) )
                )
            )
        ) );
    }
    
    public void comboChange( Event event ) {
        set( "text1.value", get( "combo1.value" ) );
    }
    
    public void swapItems( Event event ) {
        if( get( "swapButton.caption" ).equals( "Items 2" ) ) {
            set( "combo1.caption", "label" );
            set( "combo1.items", items2 );
            set( "swapButton.caption", "Items 1" );
        } else {
            set( "combo1.caption", "/" );
            set( "combo1.items", items1 );
            set( "swapButton.caption", "Items 2" );
        }
    }
    
}