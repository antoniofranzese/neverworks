package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.model.Property;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.web.WebView;
import it.neverworks.ui.web.WebViewLoadEvent;

public class WebViewForm extends Form {
    
    private WebComponent web;
    
    public void build() {
        set( "main", new BorderLayout(
            arg( "top", new GridLayout(
                arg( "columns", 99 )

                ,new ToolBar(
                    new Button()
                        .set( "caption", "Load1" )
                        .set( "click", new Slot( this, "load1" ) )
                    ,new Button()
                        .set( "caption", "Load2" )
                        .set( "click", new Slot( this, "load2" ) )
                    ,new Button()
                        .set( "caption", "Alert" )
                        .set( "click", new Slot( this, "alert" ) )
                    ,new Button()
                        .set( "caption", "Manual" )
                        .set( "click", new Slot( this, "manual" ) )
                )

                ,new TextBox()
                    .set( "name", "message" )
                    .set( "label", "Message" )
                    .set( "width", 200 )

                ,new TextBox()
                    .set( "name", "address" )
                    .set( "label", "Last address" )
                    .set( "width", 300 )
                                
            ))
            .arg( "center", new WebComponent(
                arg( "name", "web" )
                .arg( "load", new Slot( this, "loaded" ) )
            ))
                               
        ));
        
        web = get( "web" );
    }
    
    public void alert( Event event ) {
        web.alert( (String) get( "message.value" ) );
    }
    
    public void load1( Event event ) {
        set( "web.location", "http://localhost:8000/test/button.html" );
    }

    public void load2( Event event ) {
        set( "web.location", "http://localhost:8000/test/tab.html" );
    }
    
    public void loaded( WebViewLoadEvent event ) {
        set( "address.value", event.getLocation().toStringURL() );
    }
    
    public void manual( Event event ) {
        add( new ManualForm().set( "opened", true ) );
    }
    
    public static class ManualForm extends Popup {
        
        @Property @Reference
        private WebView web;
        
        public void build() {
            add( new BorderLayout(
                arg( "width", 800 )
                .arg( "height", 400 )
                .arg( "top", new ToolBar(
                    new Button()
                        .set( "caption", "ToC" )
                        .set( "click", slot( "jumpToToc" ) )
                    ,new Button()
                        .set( "caption", "Back" )
                        .set( "click", slot( "goBack" ) )
                    ,new Button()
                        .set( "caption", "Forward" )
                        .set( "click", slot( "goForward" ) )
                ))
                
                .arg( "center", new Panel(
                    arg( "border", arg( "size", 1 ).arg( "color", "lightgray" ) )
                    .arg( "content", new WebView()
                        .set( "name", "web" )
                        .set( "loader", true )
                        .set( "location", "/manual/SYS-CM-UMD-001-1.0.html#toc" )
                        .set( "load", slot( "load" ) )
                    )
                ))
            ));
        
        }
        
        public void load( Event event ) {
            web.execute( "document.documentElement.style[ 'overflow-x' ] = 'hidden';" );
        }
        
        public void jumpToToc( Event event ) {
            web.jump( "toc" );
        }

        public void goBack( Event event ) {
            web.back();
        }

        public void goForward( Event event ) {
            web.forward();
        }
    }
}