package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.canvas.Canvas;
import it.neverworks.ui.canvas.Scene;

public class CanvasTestForm extends Form {
    
    public void build() {
        add( new Canvas(
            arg( "name", "canvas" )
            .arg( "width", 500 )
            .arg( "height", 400 )
            .arg( "scene", new Scene()
                .rectangle(
                    arg( "place", arg( "x", 10 ).arg( "y", 10 ) )
                    .arg( "width", 40 )
                    .arg( "height", 30 )
                    .arg( "stroke", arg( "color", "red" ) )
                    .arg( "fill", arg( "color", "lightgreen" ) )
                )
 
                .rectangle(
                    arg( "place", arg( "x", 210 ).arg( "y", 10 ) )
                    .arg( "width", 100 )
                    .arg( "height", 80 )
                    .arg( "radius", 15 )
                    .arg( "stroke", "red" )
                    .arg( "fill", "lightgreen" )
                )

                .line(
                    arg( "place", arg( "x", 15 ).arg( "y", 15 ) )
                    .arg( "stops", list(
                        arg( "x", 205 ).arg( "y", 235 )
                        ,arg( "x", 235 ).arg( "y", 205 )
                        ,arg( "x", 265 ).arg( "y", 235 )
                    ))
                    .arg( "stroke", arg( "color", "yellow" ).arg( "width", 5 ).arg( "cap", "round" )  )
                )
 
                .circle(
                    arg( "place", arg( "x", 115 ).arg( "y", 115 ) )
                    .arg( "radius", 50 )
                )

                .triangle(
                    arg( "place", arg( "x", 115 ).arg( "y", 115 ) )
                    .arg( "height", 30 )
                    .arg( "width", 30 )
                    .arg( "fill", "green" )
                )
             
                .circle(
                    arg( "place", arg( "x", 315 ).arg( "y", 90 ) )
                    .arg( "radius", 50 )
                )

                .polygon(
                    arg( "place", arg( "x", 315 ).arg( "y", 90 ) )
                    .arg( "sides", 3 )
                    .arg( "radius", 50 )
                    .arg( "rotation", 45 )
                    .arg( "fill", "008000A0" )
                )

                .ring(
                    arg( "place", arg( "x", 315 ).arg( "y", 225 ) )
                    .arg( "width", 20 )
                    .arg( "radius", 50 )
                    .arg( "stroke", arg( "color", "violet" ).arg( "width", 2 ) )
                    .arg( "fill", "lightgray" )
                )

                .circle(
                    arg( "place", arg( "x", 115 ).arg( "y", 225 ) )
                    .arg( "radius", 40 )
                    .arg( "stroke", arg( "color", "lightgray" ).arg( "width", 20 ) )
                )

                .ring(
                    arg( "place", arg( "x", 115 ).arg( "y", 225 ) )
                    .arg( "width", 20 )
                    .arg( "radius", 50 )
                    .arg( "from", 30 )
                    .arg( "to", 120 )
                    .arg( "stroke", arg( "color", "yellow" ).arg( "width", 2 ) )
                    .arg( "fill", "blue" )
                )

                .ring(
                    arg( "place", arg( "x", 115 ).arg( "y", 225 ) )
                    .arg( "width", 20 )
                    .arg( "radius", 50 )
                    .arg( "from", 180 )
                    .arg( "to", 270 )
                    .arg( "fill", "red" )
                )

                .ring(
                    arg( "place", arg( "x", 115 ).arg( "y", 225 ) )
                    .arg( "width", 20 )
                    .arg( "radius", 50 )
                    .arg( "from", 300 )
                    .arg( "to", 380 )
                    .arg( "fill", "green" )
                )

                .text(
                    arg( "place", arg( "x", 115 ).arg( "y", 225 ) )
                    .arg( "value", "100%" )
                    .arg( "halign", "center" )
                    .arg( "baseline", "middle" )
                    .arg( "font", arg( "size", "16px" ).arg( "family", "Helvetica" ) )
                    .arg( "fill", "black" )
                )

            )
        
        ));
    
    }

}