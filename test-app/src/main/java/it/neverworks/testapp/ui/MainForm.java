package it.neverworks.testapp.ui;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.lang.Reflection;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Slot;
import it.neverworks.model.events.Event;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;

public class MainForm extends Form {
    
    private int count = 1;
    
    @Override
    public void build() {
        this.set( "title", "Framework Tests" );
        this.set( "main", new BorderLayout()
            .set( "width", "100%" )
            .set( "height", "100%" )
            .set( "left", new ToolBar(
                arg( "padding", 5 )
                .arg( "defaultWidth", 80 )
                .arg( "orientation", "vertical" )
                ,new Button()
                    .set( "caption", "Test" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", TestForm.class )
    
                ,new Button()
                    .set( "caption", "Popups" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", PopupTestForm.class )

                ,new Button()
                    .set( "caption", "Select" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", SelectTestForm.class )

                ,new Button()
                    .set( "caption", "Select 2" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", SelectCreatesSelectForm2.class )

                ,new Button()
                    .set( "caption", "Radio" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", RadioTestForm.class )

                ,new Button()
                    .set( "caption", "Grid" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", GridTestForm.class )

                ,new Button()
                    .set( "caption", "HugeGrid" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", HugeGridTestForm.class )
    
                ,new Button()
                    .set( "caption", "Tabs" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", TabTestForm.class )
    
                ,new Button()
                    .set( "caption", "Accordion" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", AccordionTestForm.class )

                ,new Button()
                    .set( "caption", "Stack" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", StackTestForm.class )

                ,new Button()
                    .set( "caption", "Translated" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", TranslatedTestForm.class )

                ,new Button()
                    .set( "caption", "Next Tick" )
                    .set( "click", slot( "loadForm" ) )
                    .set( "!attributes.form", NextTickForm.class )
    
                )
            )

            .set( "center", new Panel().set( "name", "mainPane" ) )
            
        );
        
        add( new Panel()
            .set( "origin", "-20,10" )
            // .set( "border", arg( "size", 1 ).arg( "color", "lightgray" ) )
            .set( "content", new Label()
                .set( "name", "titleLabel" )
                .set( "value", "<nothing>" )
            )
        );
    }
    
    public void loadForm( Event event ) {
        Class formClass = event.<Class>get( "source.attributes.form" );
        if( get( "mainPane.content" ) == null || !formClass.isAssignableFrom( get( "mainPane.content" ).getClass() ) ) {
            set( "mainPane.content", Reflection.newInstance( formClass ) );
            set( "titleLabel.value", formClass.getName() );
        }
    }
    
}