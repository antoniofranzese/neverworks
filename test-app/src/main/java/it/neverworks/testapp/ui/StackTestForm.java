package it.neverworks.testapp.ui;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.model.Property;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.NumberSpinner;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.StackLayout;
import it.neverworks.ui.layout.CloseEvent;
import it.neverworks.ui.layout.SelectEvent;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.Container;
import it.neverworks.ui.Widget;

public class StackTestForm extends Form {
    
    private int count = 1;
    
    @Property @Reference
    private StackLayout pages;
    
    
    @Override
    public void build() {
        
        set( "main", new BorderLayout(
            arg( "gutters", true )

            .arg( "center", new StackLayout(
                arg( "name", "pages" )
                    
                ,new AddressForm()
                    .set( "title", "Form Page" )
                
                ,new Panel(
                    arg( "title", "Panel Page" )
                    .arg( "name", "panelPage" )
                )
                
                ,new Panel(
                    arg( "title", "Panel 2" )
                    .arg( "name", "panel2" )
                    .arg( "content", new GridLayout(
                        new TextBox()
                            .set( "name", "txt1G" )
                            .set( "label", "Campo 1" )
                            .set( "value", "val1")
            
                        ,new TextBox()
                            .set( "name", "txt2G" )
                            .set( "label", "Campo 2" )
                            .set( "value", "val2") 
                    ))
                )
                
                ,new GridLayout(
                    arg( "name", "gridLayout")
                    .arg( "columns", 2 ) 
                    ,new TextBox()
                        .set( "name", "txtFather" )
                        .set( "label", "Padre" )
                    ,new TextBox()
                        .set( "name", "txtMother")
                        .set( "label", "Madre" )
                    ,new ToolBar(
                        arg( "name", "gridLayoutButtons" )            
                        .arg( "layout", 
                            arg( "hspan", "full" )
                            .arg( "halign", "right" ) 
                        )
                        ,new Button()
                            .set( "caption", "Scambia")
                            .set( "click", slot( "swapClick" ) )
                        ,new Button()
                            .set( "caption", "Switch")
                            .set( "click", slot( "switchClick" ) )
                         
                    )
                )
            ))

            .arg( "bottom", new GridLayout(
                arg( "columns", 1 )
                ,new ToolBar(
                    new Button(
                        arg( "caption", "Load panelPage" )
                        .arg( "click", slot( "loadPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Blank panelPage" )
                        .arg( "click", slot( "blankPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select page 1" )
                        .arg( "click", slot( "selectPage1" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select panelPage (2)" )
                        .arg( "click", slot( "selectPanelPage" ) )
                    )
                    ,new Button(
                        arg( "caption", "Select page 3" )
                        .arg( "click", slot( "selectPage3" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add before 3" )
                        .arg( "click", slot( "addBefore3" ) )
                    )
                    ,new Button(
                        arg( "caption", "Add start" )
                        .arg( "click", slot( "addStart" ) )
                    )
                    ,new Button(
                        arg( "caption", "Remove first" )
                        .arg( "click", slot( "removeFirst" ) )
                    )
                )
 
            ))        

        ));

    }
    
    public void loadPanelPage( Event event ) {
        set( "panelPage.content", new TestForm() );
    }
    
    public void blankPanelPage( Event event ) {
        set( "panelPage.content", null );
    }
    
    public void selectPanelPage( Event event ) {
        set( "pages.selected", get( "pages.panelPage" ) );
    }
    
    public void selectPage1( Event event ) {
        pages.select( 0 );
    }

    public void selectPage3( Event event ) {
        set( "pages.selected", get( "pages[ 2 ]" ) );
    }
    
    public void addBefore3( Event event ) {
        pages.place( new Panel(
            arg( "title", "New Page " + count++ )
            .arg( "content", new AddressForm() )
        )).before( pages.widget( 2 ) );
    }

    public void addStart( Event event ) {
        pages.place( new Panel(
            arg( "title", "Start Page " + count++ )
            .arg( "content", new AddressForm() )
        )).first();
    }
    
    public void removeFirst( Event event ) {
        pages.remove( 0 );
    }
    
    public void swapClick( ClickEvent<Button> event ) {
        String value = get( "txtMother.value" );
        set( "txtMother.value", get( "txtFather.value" ) );
        set( "txtFather.value", value );
    }
    
    public void switchClick( ClickEvent event ) {
        if( this.<Integer>get( "gridLayout.columns" ) == 2 ) {
            set( "gridLayout.columns", 4 );
        } else {
            set( "gridLayout.columns", 2 );
        }
    }
    
}