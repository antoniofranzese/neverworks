package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.model.Property;
import it.neverworks.ui.form.Form;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;

public class SelectCreatesSelectForm2 extends Form {
    
    @Property @Reference
    private GridLayout main;
    
    public void build() {
        set( "templatePanelMain", new BorderLayout(
			arg( "gutters", true )

			.arg( "center", new GridLayout(
				arg( "columns", 2 )
				.arg( "padding", 20 )
                .arg( "name", "main" )
				,new Select()
                    .set( "name", "master" )
                    .set( "label", "Master" )
                    .set( "key", "id" )
                    .set( "caption", "name" )
                    .set( "items", list(
                        new Item( arg( "id", 1 ).arg( "name", "Select1" ) )
                        ,new Item( arg( "id", 2 ).arg( "name", "Select2" ) )
                    ))
                    .set( "change", slot( "selectChange" ) )

                ,new Select()
                    .set( "name", "master2" )
                    .set( "label", "Master2" )
                    .set( "key", "id" )
                    .set( "caption", "name" )
                    .set( "items", list(
                        new Item( arg( "id", 1 ).arg( "name", "Select1" ) )
                        ,new Item( arg( "id", 2 ).arg( "name", "Select2" ) )
                    ))
                    .set( "change", slot( "selectChange" ) )
            
            ))
            
        ));
        
    }

    public void selectChange( ChangeEvent<Select> event ) {
        System.out.println( "CHANGE for " + event.getSource().getCommonName() );

        if( main.contains( "select1" ) ) {
            main.remove( "select1" );
        }

        if( main.contains( "select2" ) ) {
            main.remove( "select2" );
        }

        if( get( "master.value" ).equals( 1 ) ) {

            main.place( new Select()
                .set( "name", "select1" )
                .set( "label", "Select 1")
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Select1 value 1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Select1 value 2" ) )
                ))
                .set( "change", slot( "selectChange" ) )

            ).after( "master" );

        } else {

            main.place( new Select()
                .set( "name", "select2" )
                .set( "label", "Select 2")
                .set( "key", "id" )
                .set( "caption", "name" )
                .set( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Select2 value 1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Select2 value 2" ) )
                ))
                .set( "change", slot( "selectChange" ) )
                
            ).after( "master" );
            
        }
    }
}