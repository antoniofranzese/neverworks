package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;

public class AsyncTestForm extends Form {
    
    public void build() {
        add( new BorderLayout(
            arg( "top", new ToolBar(
                new Button(
                    arg( "caption", "Message" )
                    .arg( "click", slot( evt -> {
                        set( "messageBox", new MessageBox( "Pippo" ) );
                        spawn( task -> {
                            sleep( 5000 );
                        }).then( task -> {
                            if( contains( "messageBox" ) ) {
                                System.out.println( "CLOSING" );
                                call( "messageBox.close" );
                            } else {
                                System.out.println( "ALREADY CLOSED" );
                            }
                        });
                    }))
                )
            ))
            .arg( "center", new GridLayout(
                new TextBox(
                    arg( "label", "last" )
                    .arg( "width", 300 )
                    .arg( "name", "lastBox" )
                )
            ))
        ));

        spawn( task -> {
            while( task.isRunning() ) {
                sleep( 10000 );
                System.out.println( "Another cycle" );
            }
        }).every( 5000, task -> {
            set( "lastBox.value", date() );
        });
    }

}