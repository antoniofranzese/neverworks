package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.ui.Widget;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.NumberTextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Reference;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Slot;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.ValueFormatter;

public class SimpleValidationForm extends Form {
    
    public interface Validator<T extends Widget> {
        void validate( T widget );
    }
    
    public class ValidationError extends RuntimeException{
        private Object resetValue;

        public ValidationError( String message, Object resetValue ) {
            super( message );
            this.resetValue = resetValue;
        }
        
        public Object getResetValue(){
            return this.resetValue;
        }
        
    }
    
    public class ValidatingTextBox extends TextBox {
        private Validator validator;
        private ValidationError lastError;
        
        public ValidatingTextBox() {
            super();
            event( "change" ).add( new Slot( this, "validation" ) );
        }
        
        public void validation( Event event ) {
            if( validator != null ) {
                try {
                    validator.validate( this );
                } catch( ValidationError ex ) {
                    form().add( new MessageBox()
                        .set( "message", ex.getMessage() )
                        .set( "confirm", new Slot( this, "resetAfterError" ) ) 
                    );
                    lastError = ex;
                }
            }
        }
        
        public void resetAfterError( Event event ) {
            set( "value", lastError.getResetValue() );
            lastError = null;
            focus();
        }

        public Validator getValidator(){
            return this.validator;
        }
        
        public void setValidator( Validator validator ){
            this.validator = validator;
        }
    }
    
    public void build() {
        set( "main", new GridLayout(
            new ValidatingTextBox()
                .set( "name", "text1" )
                .set( "label", "Text 1" )
                .set( "validator", new Validator() {
                    public void validate( Widget widget ) {
                        if( widget.<String>get( "value" ).length() > 10 ) {
                            throw new ValidationError( "Max 10", "" );
                        }
                    }
                })

            ,new ValidatingTextBox()
                .set( "name", "text2" )
                .set( "label", "Text 2" )
            ,new ValidatingTextBox()
                .set( "name", "text3" )
                .set( "label", "Text 3" )
            ,new ValidatingTextBox()
                .set( "name", "text4" )
                .set( "label", "Text 4" )
                
        ));
        
    }
    
    
    
}