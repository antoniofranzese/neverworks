package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.Widget;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.grid.Grid;
import it.neverworks.ui.grid.GridColumn;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.dialogs.ConfirmBox;
import it.neverworks.ui.dialogs.QuestionBox;
import it.neverworks.ui.dialogs.WaitBox;
import it.neverworks.ui.dialogs.DialogEvent;
import it.neverworks.ui.web.Downloader;
import it.neverworks.io.FileBadge;
import it.neverworks.ui.web.WebView;
import it.neverworks.ui.tree.Tree;
import it.neverworks.ui.data.Item;

public class PopupTestForm extends Form {
    
    public class SimplePopup extends Popup {
        public void build() {
            set( "title", "Popup Test" );

            set( "main", new GridLayout(
                arg( "columns", 2 ) 
                
                ,new TextBox()
                    .set( "name", "txtField1" )
                    .set( "label", "Campo Popup 1" )

                ,new TextBox()
                    .set( "name", "txtField2" )
                    .set( "label", "Campo Popup 2" )

                ,new ToolBar(
                    arg( "name", "buttons" )            
                    .arg( "layout",
                        arg( "hspan", "full" )
                        .arg( "halign", "right" ) 
                    )
                    ,new Button()
                        .set( "name", "btnCopy" )
                        .set( "caption", "Copia" )
                        .set( "click", slot( "copyClick" ) )
                     
                    ,new Button()
                        .set( "name", "btnSwitch" )
                        .set( "caption", "Switch" )
                        .set( "click", slot( "switchClick" ) )
                     
                    ,new Button()
                        .set( "name", "btnTooltip" )
                        .set( "caption", "Tooltip" )
                        .set( "click", slot( "tooltipClick" ) )
                     
                    ,new Button()
                        .set( "name", "btnWebView" )
                        .set( "caption", "WebView" )
                        .set( "click", slot( "webviewClick" ) )
                     
                    ,new Button()
                        .set( "name", "btnClose" )
                        .set( "caption", "Chiudi" )
                        .set( "click", slot( "closeClick" ) )

                    ,new Button()
                        .set( "name", "btnDestroy" )
                        .set( "caption", "Elimina" )
                        .set( "click", slot( "destroyClick" ) )
                 
                     
                    )
                        
                )
                
            );
            
        }
        
        public void copyClick( ClickEvent event ) {
            System.out.println( "POPUP COPY" );
            set( "parent.txtField1.value", get( "txtField1.value" ) );
        }
        
        public void closeClick( ClickEvent event ) {
            this.close();
        }
        
        public void destroyClick( ClickEvent event ) {
            add( new ConfirmBox(
                arg( "message", "Eliminare?" )
                .arg( "confirm", slot( "performDestroy" ) )
            ));
        }
        
        public void performDestroy( Event event ) {
            this.destroy();
        }
        
        public void tooltipClick( ClickEvent event ) {
            add( new MessageBox()
                .set( "message", "Message in a Tooltip" )
                .set( "anchor", event.getSource() )
            );
        }
        
        public void switchClick( ClickEvent event ) {
            if( this.<Integer>get( "main.columns" ) == 2 ) {
                set( "main.columns", 4 );
            } else {
                set( "main.columns", 2 );
            }
        }
        
        public void webviewClick( ClickEvent event ) {
            add( new Popup(
                arg( "name", "popupColorPicker" ) 
                .arg( "title", "Colore" ) 
                .arg( "opened", true ) 
                .arg( "anchor", event.getSource() )
                .arg( "autoDestroy", true )
                //.arg( "close", slot( "closePopupColorPicker" ))  
                .arg( "main", new Panel()
                    .set( "width", 250 )
                    .set( "height", 250 )
                    .set("content", new PickerComponent(
                        arg( "name", "pickerComponent" )  
                        // .arg( "width", 250 )
                        // .arg( "height", 250 )
                        .arg( "ready", slot( "loadColor" ) )   
                    ))
                 )
            ));
        }
        
        public void loadColor( Event event ) {
            this.<WebView>get( "popupColorPicker.pickerComponent").execute( "PickerApp.SetColor( 'red' );" );
        }
        
    }
    
    public void build() {
        set( "main", new GridLayout(
            arg( "columns", 2 )
            ,new TextBox()
                .set( "name", "txtField1" )
                .set( "label", "Campo 1" )
            ,new TextBox()
                .set( "name", "txtField2" )
                .set( "label", "Campo 2" )

            ,new ToolBar(
                arg( "name", "buttons" )            
                .arg( "defaultWidth", 100 )            
                .arg( "layout",
                    arg( "hspan", "full" )
                    .arg( "halign", "right" ) 
                )
                ,new Button()
                    .set( "name", "btnExisting" )
                    .set( "caption", "Apri esistente")
                    .set( "click", slot( "existingClick" ) )
                ,new Button()
                    .set( "name", "btnTooltip" )
                    .set( "caption", "Apri qui")
                    .set( "click", slot( "tooltipClick" ) )
                ,new Button()
                    .set( "name", "btnTooltip2" )
                    .set( "caption", "E qui")
                    .set( "click", slot( "tooltipClick" ) )
                ,new Button()
                    .set( "name", "btnNew" )
                    .set( "caption", "Apri nuova")
                    .set( "click", slot( "newPopup" ) )
                ,new Button()
                    .set( "name", "btnNewHere" )
                    .set( "caption", "Nuova qui")
                    .set( "click", slot( "newPopupHere" ) )
                ,new Button()
                    .set( "name", "btnDestroy" )
                    .set( "caption", "Elimina")
                    .set( "click", slot( "destroyPopup" ) )

                ,new Button()
                    .set( "name", "btnFixed" )
                    .set( "caption", "Fixed")
                    .set( "click", slot( "openFixedPopup" ) )
                ,new Button()
                    .set( "name", "btnResizable" )
                    .set( "caption", "Resizable")
                    .set( "click", slot( "openResizablePopup" ) )
                 
                )

            ,new ToolBar(
                arg( "name", "dialogButtons" )
                .arg( "orientation", "vertical" )
                .arg( "defaultWidth", 100 )            
                .arg( "layout", 
                    arg( "hspan", "full" )
                    .arg( "halign", "left" ) 
                )
                ,new Button()
                    .set( "name", "btnMessageBox" )
                    .set( "caption", "Message")
                    .set( "click", slot( "openMessage" ) )
                ,new Button()
                    .set( "name", "btnMessageBoxHere" )
                    .set( "caption", "Message here")
                    .set( "click", slot( "openMessageHere" ) )
                ,new Button()
                    .set( "name", "btnConfirm" )
                    .set( "caption", "Confirm")
                    .set( "click", slot( "openConfirm" ) )
                ,new Button()
                    .set( "name", "btnQuestion" )
                    .set( "caption", "Question")
                    .set( "click", slot( "openQuestion" ) )
                ,new Button()
                    .set( "name", "btnWait" )
                    .set( "caption", "Wait")
                    .set( "click", slot( "openWait" ) )
                ,new Button()
                    .set( "name", "btnFastWait" )
                    .set( "caption", "Fast Wait")
                    .set( "click", slot( "openFastWait" ) )
                 
                )
                        
            )
             
        );
        
        set( "existingPopup", new SimplePopup()
            .set( "title", "Popup esistente" )
            .set( "closable", false )
            .set( "close", slot( "closePopup" ) ) 
        );
        
        set( "complexFixed", new ComplexPopup() );
        
        set( "downloader", new Downloader() );
    }
    
    private int counter = 1;
    
    public void existingClick( ClickEvent<Button> event ) {
        this.<Popup>get( "existingPopup" ).open();
    }

    public void tooltipClick( ClickEvent<Button> event ) {
        this.<Popup>get( "existingPopup" ).open( event.getSource() );
        //this.<Popup>get( "existingPopup" ).open( 100, 720 );
    }

    
    public void closePopup( Event<Popup> event ) {
        System.out.println( event.getSource().getCommonName() + " closed" );
    }
    
    public void newPopup( ClickEvent event ) {
        if( !( contains( "newPopup" ) ) ) {
            set( "newPopup", new SimplePopup()
                .set( "title", "Nuova popup " + counter++ )
                .set( "close", slot( "closePopup" ) )
                .set( "flavor", "naked" )
                .store( "txtField1.value", "Value1-" + Strings.generateHexID( 8 ) ) 
                .store( "txtField2.value", "Value2-" + Strings.generateHexID( 8 ) ) 
            );
        }
        this.<Popup>get( "newPopup" ).open();
    }

    public void newPopupHere( ClickEvent event ) {
        newPopup( event );
        this.<Popup>get( "newPopup" ).open( (Widget) event.getSource() );
    }
    
    public void destroyPopup( ClickEvent event ) {
        if( contains( "newPopup" )  ) {
            remove( "newPopup" );
        }
    }
    
    public void openMessage( ClickEvent event ) {
        add( new MessageBox()
            .set( "title", "Message Box" )
            .set( "message", "Un messaggio di avviso" )
            .set( "confirm", slot( "confirmed" ) )
        );
    }

    public void openMessageHere( ClickEvent event ) {
        add( new MessageBox()
            .set( "title", "Message Box Here" )
            .set( "message", "Un messaggio di avviso qui" )
            .set( "confirm", slot( "confirmed" ) )
            .set( "anchor", event.getSource() )
        );
    }

    public void openConfirm( ClickEvent event ) {
        add( new ConfirmBox()
            .set( "title", "Confirm Box" )
            .set( "message", "Una richiesta di conferma" )
            .set( "confirm", slot( "confirmed" ) )
            .set( "cancel", slot( "cancelled" ) )
        );
    
    }

    public void openQuestion( ClickEvent event ) {
        add( new QuestionBox()
            .set( "title", "Question Box" )
            .set( "message", "Una richiesta a cui rispondere con un sì o un no" )
            .set( "confirm", slot( "confirmed" ) )
            .set( "reject", slot( "rejected" ) )
            .set( "cancel", slot( "cancelled" ) )
        );
    
    }

    public void openWait( ClickEvent event ) {
        add( new WaitBox()
            .set( "name", "waitBox" )
            .set( "title", "Wait Box" )
            .set( "message", "Attendere, prego..." )
        );
        
        next( slot( "closeWait" ) );
    }
    
    public void closeWait( Event event ) throws Exception {
        Thread.sleep( 2000 );
        event( "waitBox.close" ).fire();
    }
    
    public void openFastWait( ClickEvent event ) {
        add( new WaitBox()
            .set( "name", "waitBox" )
            .set( "title", "Fast Wait Box" )
            .set( "message", "Attendere, poco..." )
        );
        set( "txtField2.value", "wait" );
        next( slot( "closeFastWait" ) );
    }

    public void closeFastWait( Event event ) throws Exception {
        set( "txtField2.value", "closed" );
        event( "waitBox.close" ).fire();
        add( new MessageBox()
            .set( "title", "Message Box" )
            .set( "message", "Un messaggio di avviso" )
        );
    }

    public void confirmed( DialogEvent event ) {
        System.out.println( "CONFIRMED" );
    }
    
    public void cancelled( DialogEvent event ) {
        System.out.println( "CANCELLED" );
    }
    
    public void rejected( DialogEvent event ) {
        System.out.println( "REJECTED" );
    }
    
    public void openFixedPopup( Event event ) {
        set( "complexFixed.opened", true );
    }

    public void openResizablePopup( Event event ) {
        if( ! contains( "complexSizable" ) ) {
            set( "complexSizable", new ComplexPopup().set( "resizable", true ) );
        }
        set( "complexSizable.opened", true );
    }
    
    private Widget createComplexWidget() {
        return new BorderLayout(
            arg( "gutters", true )
            .arg( "title", "Complex layout")
            .arg( "top", new Panel()
                .set( "height", 50 )
            )
            .arg( "left", new Tree(
                arg( "width", 140 )
                .arg( "layout", arg( "splitter", true ) )
                .arg( "expanded", true )
                .arg( "items", list(
                    new Item( arg( "id", 1 ).arg( "name", "Item 1" ) )
                    ,new Item( arg( "id", 2 ).arg( "name", "Item 2" ).arg( "parent", 1 ) )
                    ,new Item( arg( "id", 3 ).arg( "name", "Item 3" ).arg( "parent", 2 ) )
                    ,new Item( arg( "id", 4 ).arg( "name", "Item 4" ).arg( "parent", 2 ) )
                    ,new Item( arg( "id", 5 ).arg( "name", "Item 5" ).arg( "parent", 1 ) )
                    ,new Item( arg( "id", 6 ).arg( "name", "Item 6" ).arg( "parent", 5 ) )
                    ,new Item( arg( "id", 7 ).arg( "name", "Item 7" ).arg( "parent", 5 ) )
                    ,new Item( arg( "id", 8 ).arg( "name", "Item 8" ).arg( "parent", 7 ) )
                    ,new Item( arg( "id", 9 ).arg( "name", "Item 9" ).arg( "parent", 7 ) )
                    ,new Item( arg( "id", 10 ).arg( "name", "Item 10" ).arg( "parent", 5 ) )
                    
                ))
            ))
            .arg( "center", new Grid(
                arg( "preload", false )
                .arg( "columns", list(
                     new GridColumn()
                         .set( "title", "Column 1" )
                         .set( "source", "prop1" )
                         .set( "width", "34%" )
                     ,new GridColumn()
                         .set( "title", "Column 2" )
                         .set( "source", "prop2" )
                         .set( "width", "33%" )
                     ,new GridColumn()
                         .set( "title", "Column 3" )
                         .set( "source", "prop3" )
                         .set( "width", "33%" )
                 ))
                 .arg( "items", list(
                     new Item( arg( "id", 1 ).arg( "prop1", "Property 1.1" ).arg( "prop2", "Property 2.1" ).arg( "prop3", "Property 3.1" ) )
                     ,new Item( arg( "id", 2 ).arg( "prop1", "Property 1.2" ).arg( "prop2", "Property 2.2" ).arg( "prop3", "Property 3.2" ) )
                     ,new Item( arg( "id", 3 ).arg( "prop1", "Property 1.3" ).arg( "prop2", "Property 2.3" ).arg( "prop3", "Property 3.3" ) )
                     ,new Item( arg( "id", 4 ).arg( "prop1", "Property 1.4" ).arg( "prop2", "Property 2.4" ).arg( "prop3", "Property 3.4" ) )
                     ,new Item( arg( "id", 5 ).arg( "prop1", "Property 1.5" ).arg( "prop2", "Property 2.5" ).arg( "prop3", "Property 3.5" ) )
                     ,new Item( arg( "id", 6 ).arg( "prop1", "Property 1.6" ).arg( "prop2", "Property 2.6" ).arg( "prop3", "Property 3.6" ) )
                     ,new Item( arg( "id", 7 ).arg( "prop1", "Property 1.7" ).arg( "prop2", "Property 2.7" ).arg( "prop3", "Property 3.7" ) )
                     ,new Item( arg( "id", 8 ).arg( "prop1", "Property 1.8" ).arg( "prop2", "Property 2.8" ).arg( "prop3", "Property 3.8" ) )
                     ,new Item( arg( "id", 9 ).arg( "prop1", "Property 1.9" ).arg( "prop2", "Property 2.9" ).arg( "prop3", "Property 3.9" ) )
                     ,new Item( arg( "id", 10 ).arg( "prop1", "Property 1.10" ).arg( "prop2", "Property 2.10" ).arg( "prop3", "Property 3.10" ) )
                 ))
            ))
        );
    }

    public class ComplexPopup extends Popup {
        public void build() {
            set( "title", "Sizable Complex Popup" );
            set( "width", 500 );
            set( "height", 300 );

            set( "minimumWidth", 500 );
            set( "minimumHeight", 300 );

            set( "closable", true );
            //set( "resizable", true );
            
            add( createComplexWidget() );
        }
    }
    
}
