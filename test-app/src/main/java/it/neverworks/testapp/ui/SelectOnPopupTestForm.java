package it.neverworks.testapp.ui;

import java.util.List;

import static it.neverworks.language.*;

import it.neverworks.model.Property;
import it.neverworks.ui.data.Item;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Popup;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.form.ChangeEvent;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Signal;

public class SelectOnPopupTestForm extends Form {
    
    public void build() {
        set( "main", new GridLayout(

            new ToolBar(
                new Button()
                    .set( "caption", "Popup semplice" )
                    .set( "click", slot( "openSimple" ) )            
                ,new Button()
                    .set( "caption", "Popup con evento passante" )
                    .set( "click", slot( "openPassing" ) )            
                ,new Button()
                    .set( "caption", "Popup con evento gestito" )
                    .set( "click", slot( "openEvent" ) )            
            )

            ,new Label()
                .set( "name", "result" )
        ));
    }
    

    public List getTestItems() {
        return list(
            new Item( arg( "key", 1 ).arg( "caption", "Item 1" ) )
            ,new Item( arg( "key", 2 ).arg( "caption", "Item 2" ) )
            ,new Item( arg( "key", 3 ).arg( "caption", "Item 3" ) )
        );
    }

    // ************* Simple ************

    public class SimplePopup extends Popup {
        public void build() {
            set( "main", new GridLayout(
                new Select()
                    .set( "name", "select" )
                    .set( "items", SelectOnPopupTestForm.this.getTestItems() )
            ));
        }
    }

    public void openSimple( Event event ) {
        set( "popup", new SimplePopup().open()
            .store( "select.change", slot( "simpleChange" ) )
        );
    }
    
    public void simpleChange( ChangeEvent<Select> event ) {
        set( "result.value", "Semplice: " + ( (Item) event.getSource().getItem() ).get( "caption" ) );
        remove( "popup" );
    }

    // ************* Passing ************

    public class PassingPopup extends Popup {
        
        @Property
        Signal selectionChange;
        
        public void build() {
            set( "main", new GridLayout(
                new Select()
                    .set( "name", "select" )
                    .set( "items", SelectOnPopupTestForm.this.getTestItems() )
                    .set( "change", event( "selectionChange" ) )
            ));
            
        }

    }

    public void openPassing( Event event ) {
        set( "popup", new PassingPopup().open()
            .set( "selectionChange", slot( "passingChange" ) )
        );
    }
    
    public void passingChange( Event event ) {
        ChangeEvent<Select> change = (ChangeEvent<Select>) event.getParent();
        set( "result.value", "Passante: " + ( (Item) change.getSource().getItem() ).get( "caption" ) );
        remove( "popup" );
    }

    // ************* Event ************

    public void openEvent( Event event ) {
        set( "popup", new EventPopup().open()
            .set( "selectionChange", slot( "eventChange" ) )
        );
    }
    
    public void eventChange( SelectionChangeEvent event ) {
        set( "result.value", "Evento: " +  event.get( "selected.caption" ) );
        remove( "popup" );
    }

    public static class SelectionChangeEvent extends Event {
        @Property Item selected;
    }
    
    public class EventPopup extends Popup {
        
        @Property
        Signal<SelectionChangeEvent> selectionChange;
        
        public void build() {
            set( "main", new GridLayout(
                new Select()
                    .set( "name", "select" )
                    .set( "items", SelectOnPopupTestForm.this.getTestItems() )
                    .set( "change", slot( "onChange" ) )
            ));
            
        }

        public void onChange( ChangeEvent<Select> event ) {
            event( "selectionChange" ).fire( arg( "selected", event.getSource().getItem() ) );
        }
    }
    
    
}