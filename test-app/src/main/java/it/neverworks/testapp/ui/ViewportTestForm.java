package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Select;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.PinboardLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.model.events.Event;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.data.ValueFormatter;

public class ViewportTestForm extends Form {
    
    public void build() {
        set( "main", new BorderLayout(
            arg( "font.size", 20 )
            .arg( "gutters", true )
            .arg( "left",
                new Panel(
                    arg( "content", new Label(
                        arg( "value", "Left Pane" )
                    ))
                    .arg( "padding", 2 )
                    .arg( "name", "leftpane" )
                    .arg( "width", "300px" )
                    .arg( "background", "lightcyan" )
                    .arg( "viewport", 
                        arg( "narrow", 
                            arg( "region", "bottom" )
                            .arg( "height", 100 )
                        )
                        .arg( "medium",
                            arg( "width", 200 )
                        )
                        .arg( "portrait",
                            arg( "region", "top" )
                            .arg( "height", 400 )
                        )
                    )
                )
            )

            .arg( "center", new Panel(
                arg( "content", new Label(
                    arg( "value", "Center Pane" )
                ))
                .arg( "name", "centerpane" )
                .arg( "padding", 2 )
            ))

            .arg( "right",
                new BorderLayout(
                    arg( "top", new Panel(
                        arg( "content", new Label(
                            arg( "value", "Right Pane" )
                        ))
                        .arg( "height", 30 )
                        .arg( "padding", 2 )
                    ))
                    .arg( "center", new PinboardLayout(
                        arg( "columns", list( 100, 200 ) )
                        .arg( "spacing", 3 )
                        .arg( "padding", 5 )
                        .arg( "font.size", 12 )
                        ,new TextBox(
                            arg( "label", "Text 1" )
                        )
                        ,new TextBox(
                            arg( "label", "Text 2" )
                        )
                        ,new Select(
                            arg( "label", "Select" )
                            .arg( "items", list(
                                arg( "id", 1 ).arg( "name", "Option 1" )
                                ,arg( "id", 2 ).arg( "name", "Option 2" )
                                ,arg( "id", 3 ).arg( "name", "Option 3" )
                            ))
                        )
                    ))
                    .arg( "name", "rightpane" )
                    .arg( "width", "300px" )
                    .arg( "background", "lightyellow" )
                    .arg( "viewport", 
                        arg( "< wide", 
                            arg( "region", "top" )
                            .arg( "height", 200  )
                        )
                        .arg( "portrait",
                            arg( "region", "bottom" )
                            .arg( "height", 400 )
                        )
                    )
                )
            )

        ));

    }
    
}