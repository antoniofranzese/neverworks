package it.neverworks.testapp.ui;

import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Streams;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.CodeBox;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.DateTextBox;
import it.neverworks.ui.form.TimeTextBox;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;

public class CodeBoxTestForm extends Form {
    
    private String code1 = "<?xml version=\"1.0\"?>"
        + "<beans default-autowire=\"byName\" xmlns=\"http://www.springframework.org/schema/beans\">"
        + "<bean id=\"ImageCatalog\" class=\"it.neverworks.ui.html.data.WebFileCatalog\">"
        + "<property name=\"baseURL\" value=\"/nw3/style/images\"/>"
        + "</bean>"
        + "<bean id=\"IconCatalog\" class=\"it.neverworks.ui.html.data.FormattingWebStyleCatalog\">"
        + "<property name=\"formats\">"
        + "<map>"
        + "<entry key=\"16\" value=\"icon-16-{0}\"/>"
        + "<entry key=\"32\" value=\"icon-32-{0}\"/>"
        + "</map>"
        + "</property>"
        + "</bean>"
        + "\n"
        + "<bean id=\"TemporaryFileRepository\" class=\"it.neverworks.ui.html.server.TemporaryFileService\"/>"
        + "</beans>";
    
    
    public void build() {
        set( "main", new BorderLayout(
            arg( "left", new Panel().set( "content", new Label().set( "value", "CodeBox" ) ) )
            .arg( "center", new CodeBox()
                .set( "name", "code" )
                .set( "label", "Code" )
                .set( "width", 800 )
                .set( "height", 300 )
                .set( "language", "xml" )
                .set( "value", code1 )
            
            )
            .arg( "bottom", new ToolBar(
                arg( "name", "buttons" )            
                ,new Button()
                    .set( "caption", "Code 2")
                    .set( "click", slot( "loadCode2" ) )
                ,new Button()
                    .set( "caption", "Code 1")
                    .set( "click", slot( "loadCode1" ) )
                ,new Button()
                    .set( "caption", "Code 3")
                    .set( "click", slot( "loadCode3" ) )
            ))
        ));
    }
    
    public void loadCode2( ClickEvent<Button> event ) {
        set( "code.value", Streams.asString( this.getClass().getClassLoader().getResourceAsStream( "websample.xml" ) ) );
        set( "code.language", "xml" );
    }

    public void loadCode1( ClickEvent<Button> event ) {
        set( "code.value", code1 );
        set( "code.language", "xml" );
    }

    public void loadCode3( ClickEvent<Button> event ) {
        set( "code.value", "A simple text\nwith newlines" );
        set( "code.language", null );
    }
    
}