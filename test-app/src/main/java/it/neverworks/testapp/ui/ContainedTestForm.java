package it.neverworks.testapp.ui;

import static it.neverworks.language.*;

import it.neverworks.model.events.Event;
import it.neverworks.ui.form.Form;
import it.neverworks.ui.UI;
import it.neverworks.ui.form.ClickEvent;
import it.neverworks.ui.form.Button;
import it.neverworks.ui.form.TextBox;
import it.neverworks.ui.form.Label;
import it.neverworks.ui.form.ToolBar;
import it.neverworks.ui.layout.GridLayout;
import it.neverworks.ui.layout.BorderLayout;
import it.neverworks.ui.layout.Panel;
import it.neverworks.ui.dialogs.MessageBox;
import it.neverworks.ui.web.WebView;
import it.neverworks.ui.web.Conversation;
import it.neverworks.ui.Widget;

public class ContainedTestForm extends Form {
    
    public void build() {
        add( new GridLayout(
            new Label(
                arg( "value", "Contained Form" )
                .arg( "font", arg( "size", 14 ) )
                .arg( "layout.hspan", "full" )
            )
            ,new TextBox(
                arg( "name", "text1" )
                .arg( "label", "Text 1" )
            )
            ,new TextBox(
                arg( "name", "text2" )
                .arg( "label", "Text 2" )
            )
            ,new ToolBar(
                arg( "layout.hspan", "full" )
                ,new Button(
                    arg( "caption", "Alert" )
                    .arg( "click", slot( click -> {
                        call( "conversation.send", "alert", arg( "text", "Contained alert: " + get( "text1.value" ) ) ); 
                    }))
                )

                ,new Button(
                    arg( "caption", "Message" )
                    .arg( "click", slot( click -> {
                        call( "conversation.send", "text", arg( "text", get( "text1.value" ) ) ); 
                    }))
                )
            )
        ));
 
        add( new Conversation(
            arg( "name", "conversation" )
            .arg( "channel", "TestChannel" )

            .arg( "text", slot( message -> {
                System.out.println( "ContaineD: " + message.get( "topic" ) + " -> " + message.get( "value" ) );
                set( "text1.value", message.get( "value.text" ) );
            }))

            .arg( "alert", slot( message -> {
                add( new MessageBox()
                    .set( "message", message.get( "value.text" ) ) 
                    .set( "title", "Contained" ) 
                );
            }))
            
            .arg( "request", slot( request -> {
                System.out.println( "ContainedD request: " + request );
                request.reply( arg( "text", "Text 2: " + get( "text2.value" ) ) );
            }))
        ));
    }
    
}