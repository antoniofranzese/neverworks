package it.neverworks.security.authentication;

public interface PasswordEncoder {
    String encodePassword( String rawPass );
    String encodePassword( String rawPass, Object salt );
    boolean isPasswordValid( String encPass, String rawPass );
    boolean isPasswordValid( String encPass, String rawPass, Object salt );
}