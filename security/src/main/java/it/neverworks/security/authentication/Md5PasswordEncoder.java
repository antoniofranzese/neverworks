package it.neverworks.security.authentication;

import org.apache.commons.codec.digest.DigestUtils;

public class Md5PasswordEncoder extends DigestPasswordEncoder {
    
    public String digest( String password ) {
        return DigestUtils.md5Hex( password );
    }

}