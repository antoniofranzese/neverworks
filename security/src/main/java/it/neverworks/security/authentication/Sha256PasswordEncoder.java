package it.neverworks.security.authentication;

import org.apache.commons.codec.digest.DigestUtils;

public class Sha256PasswordEncoder extends DigestPasswordEncoder {
    
    public String digest( String password ) {
        return DigestUtils.sha256Hex( password );
    }

}