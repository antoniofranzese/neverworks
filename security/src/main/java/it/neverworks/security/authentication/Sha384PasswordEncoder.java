package it.neverworks.security.authentication;

import org.apache.commons.codec.digest.DigestUtils;

public class Sha384PasswordEncoder extends DigestPasswordEncoder {
    
    public String digest( String password ) {
        return DigestUtils.sha384Hex( password );
    }

}