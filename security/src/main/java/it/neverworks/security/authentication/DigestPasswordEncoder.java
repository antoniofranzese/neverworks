package it.neverworks.security.authentication;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.expressions.Template;

public abstract class DigestPasswordEncoder implements PasswordEncoder {
    
    public String encodePassword( String rawPass ) {
        return encodePassword( rawPass, null );
    }
    
    public String encodePassword( String rawPass, Object salt ) {
        return digest( merge( rawPass, Strings.valueOf( salt ) ) );
    }
    
    public boolean isPasswordValid( String encPass, String rawPass ) {
        return isPasswordValid( encPass, rawPass, null );
    }
    
    public boolean isPasswordValid( String encPass, String rawPass, Object salt ) {
        return Strings.safe( encPass ).equals( digest( merge( rawPass, Strings.valueOf( salt ) ) ) );
    }
    
    protected String merge( String password, String salt ) {
        if( Strings.hasText( salt ) ) {
            return mergeTemplate.apply( arg( "password", password ).arg( "salt", salt ) );
        } else {
            return password;
        }
    }
    
    private Template mergeTemplate = new Template( "{password}'{'{salt}'}'" ); 
    
    public Template getMergeTemplate(){
        return this.mergeTemplate;
    }
    
    public void setMergeTemplate( Template mergeTemplate ){
        this.mergeTemplate = mergeTemplate;
    }
    protected abstract String digest( String password );
    
}