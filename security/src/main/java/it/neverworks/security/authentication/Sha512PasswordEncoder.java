package it.neverworks.security.authentication;

import org.apache.commons.codec.digest.DigestUtils;

public class Sha512PasswordEncoder extends DigestPasswordEncoder {
    
    public String digest( String password ) {
        return DigestUtils.sha512Hex( password );
    }

}