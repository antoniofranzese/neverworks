package it.neverworks.security.profile;

import it.neverworks.lang.Strings;
import it.neverworks.security.context.UserInfo;

public abstract class AbstractEntityRoleProvider extends AbstractEntityProfileProvider implements EntityRoleProvider {
    
    public boolean claimsRole( Class entity, String role ) {
        if( Strings.hasText( prefix ) ) {
            return knows( entity ) && Strings.safe( role ).startsWith( prefix );
        } else {
            return knows( entity );
        }
    }

    public boolean hasRole( UserInfo user, Object entity, String role ) {
        if( descriptor != null ) {
            String key = role + "@" + descriptor.identifyEntity( entity );
            Boolean result = cache( user ).get( key );
            if( result == null ) {
                result = retrieveRole( user, entity, role );
                cache( user ).put( key, result );
            }
            return result;
        } else {
            return retrieveRole( user, entity, role );
        }
    }
    
    public abstract boolean retrieveRole( UserInfo user, Object entity, String role );

}
