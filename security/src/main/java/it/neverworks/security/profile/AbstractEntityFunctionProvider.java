package it.neverworks.security.profile;

import it.neverworks.lang.Strings;
import it.neverworks.security.context.UserInfo;

public abstract class AbstractEntityFunctionProvider extends AbstractEntityProfileProvider implements EntityFunctionProvider {
    
    public boolean claimsFunction( Class entity, String function ) {
        if( Strings.hasText( prefix ) ) {
            return knows( entity ) && Strings.safe( function ).startsWith( prefix );
        } else {
            return knows( entity );
        }
    }

    public boolean hasFunction( UserInfo user, Object entity, String function ) {
        if( descriptor != null ) {
            String key = function + "@" + descriptor.identifyEntity( entity );
            Boolean result = cache( user ).get( key );
            if( result == null ) {
                result = retrieveFunction( user, entity, function );
                cache( user ).put( key, result );
            }
            return result;
        } else {
            return retrieveFunction( user, entity, function );
        }
    }
    
    public abstract boolean retrieveFunction( UserInfo user, Object entity, String function );

}
