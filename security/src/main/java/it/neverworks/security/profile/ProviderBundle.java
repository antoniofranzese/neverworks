package it.neverworks.security.profile;

import java.util.List;

public interface ProviderBundle extends ProfileProvider {
    List<ProfileProvider> getProviders();
}