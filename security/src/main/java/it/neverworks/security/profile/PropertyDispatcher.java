package it.neverworks.security.profile;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.security.context.UserInfo;

public class PropertyDispatcher extends AbstractPropertyDispatcher implements PropertyProvider {
    
    public boolean claimsProperty( String name ) {
        return description.has( name );
    }
    
    public Object getProperty( UserInfo user, String name ) {
        if( description.has( name ) & description.property( name ).isReadable() ) {
            return description.property( name ).get( user );
        } else {
            throw new UnsupportedFeatureException( msg( "Cannot read ''{0}'' property on {1.class.name}", name, this ) );
        }
    }
    
    public void setProperty( UserInfo user, String name, Object value ) {
        if( description.has( name ) & description.property( name ).isWritable() ) {
            description.property( name ).set( user, value );
        } else {
            throw new UnsupportedFeatureException( msg( "Cannot write ''{0}'' property on {1.class.name}", name, this ) );
        }
    }

}