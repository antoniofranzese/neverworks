package it.neverworks.security.profile;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;
import it.neverworks.security.context.UserInfo;
import it.neverworks.security.profile.PropertyProvider;
import it.neverworks.persistence.entities.Entities;

public class UserPersistentEntityProvider extends BaseModel implements PropertyProvider {
    
    @Property
    protected String property = "user";
    
    @Property @Required
    protected Class userClass;
    
    public boolean claimsProperty( String name ) {
        return property.equals( name );
    }
    
    public boolean hasProperty( UserInfo user, String name ) {
        return property.equals( name );
    }
    
    public Object getProperty( UserInfo user, String name ) {
        if( property.equals( name ) && user.authenticated() ) {
            return loadEntity( user );
        } else {
            return null;
        }
    }
    
    protected Object loadEntity( UserInfo user ) {
    	return Entities.load( getUserClass(), user.getAuthenticatedUser().getUserID() );
    }
    
    public void setProperty( UserInfo user, String name, Object value ) {
        // nothing to do
    }

    public Class getUserClass(){
        return this.userClass;
    }
    
    public void setUserClass( Class userClass ){
        this.userClass = userClass;
    }
    
    public String getProperty(){
        return this.property;
    }
    
    public void setProperty( String property ){
        this.property = property;
    }
    
    public void setAttribute( String property ){
        this.property = property;
    }
    
}