package it.neverworks.security.profile;

import it.neverworks.security.context.UserInfo;

public interface EntityFunctionProvider extends ProfileProvider {
    public boolean claimsFunction( Class type, String function );
    public boolean hasFunction( UserInfo user, Object entity, String function );
}