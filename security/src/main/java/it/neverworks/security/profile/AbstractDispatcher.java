package it.neverworks.security.profile;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Method;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.security.context.UserInfo;
import it.neverworks.lang.UnsupportedFeatureException;

public abstract class AbstractDispatcher {
    
    protected abstract class Property {

        protected Method getter;
        protected Method setter;

        public boolean isReadable() {
            return this.getter != null;
        }

        public boolean isWritable() {
            return this.setter != null;
        }
        
        public Method getSetter(){
            return this.setter;
        }
        
        public void setSetter( Method setter ){
            this.setter = setter;
        }
        
        public Method getGetter(){
            return this.getter;
        }
        
        public void setGetter( Method getter ){
            this.getter = getter;
        }
        
    }
    
    protected abstract class Description<P extends Property> {
        
        private Map<String,P> properties = new HashMap<String,P>();
        
        public boolean has( String name ) {
            return this.properties.containsKey( name );
        }
        
        public P property( String name ) {
            if( ! properties.containsKey( name ) ) {
                synchronized( this ) {
                    if( ! properties.containsKey( name ) ) {
                        properties.put( name, newProperty() );
                    }
                }
            }
            return (P) this.properties.get( name );
        }
        
        protected abstract P newProperty();
    }
    
    protected <P extends Property> Description<P> buildDescription( Description<P> ds, Object getterSignature, Object setterSignature ) {

        if( getterSignature != null ) {
            for( Method getter: Reflection
                .findMethods( this.getClass(), Collections.toArray( Class.class, getterSignature ) )
                .filter( m -> m.getName().matches( "get[A-Z]?.*" ) ) 
                ) {
            
                String name = getter.getName().substring( 3, 4 ).toLowerCase() + getter.getName().substring( 4 );
                if( ds.property( name ).getGetter() == null ) {
                    ds.property( name ).setGetter( getter );                
                } else {
                    throw new RuntimeException( msg( "Duplicate getter for {0} in {1.class.name}", name, this ) );
                }
        
            }
        }

        if( setterSignature != null ) {
            for( Method setter: Reflection
                .findMethods( this.getClass(), Collections.toArray( Class.class, setterSignature ) ) 
                .filter( m -> m.getName().matches( "set[A-Z]?.*" ) ) 
                ) {
            
                String name = setter.getName().substring( 3, 4 ).toLowerCase() + setter.getName().substring( 4 );
                if( ds.property( name ).getSetter() == null ) {
                    ds.property( name ).setSetter( setter );
                } else {
                    throw new RuntimeException( msg( "Duplicate setter for {0} in {1.class.name}", name, this ) );
                }
            
            }
        }
        
        return ds;
    }

    protected String qualify( String name ) {
        return this.getClass().getName() + "." + name;
    }

}