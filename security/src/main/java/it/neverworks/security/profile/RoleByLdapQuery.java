package it.neverworks.security.profile;

import java.util.Map;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.AttributesMapper;

import it.neverworks.security.context.UserInfo;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.Property;

public class RoleByLdapQuery extends AbstractRoleProvider {
    
    @Property
    private LdapTemplate ldapTemplate;

    @Property @Collection
    private Map<String, Template> roles;

    @Override
    public boolean claimsRole( String role ) {
        return roles.containsKey( role );
    }

    @Override
    public boolean retrieveRole( UserInfo user, String role ) {
        String query = roles.get( role ).apply( user );
        boolean result = ldapTemplate.search( "", query, new AttributesMapper() {
            public Object mapFromAttributes( Attributes attrs ) throws NamingException {
                return true;
            }
        }).size() > 0;
        if( authLogger.isDebugEnabled() ) {
            authLogger.debug( "LDAP role query: {} = {}", query, result );
        }
        return result;
    }

    public Map getRoles(){
        return this.roles;
    }
    
    public void setRoles( Map roles ){
        this.roles = roles;
    }

    public LdapTemplate getLdapTemplate(){
        return this.ldapTemplate;
    }
    
    public void setLdapTemplate( LdapTemplate ldapTemplate ){
        this.ldapTemplate = ldapTemplate;
    }
}
