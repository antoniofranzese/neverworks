package it.neverworks.security.profile;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.security.context.UserInfo;

public class RoleDispatcher extends AbstractPropertyDispatcher implements RoleProvider {
    
    public boolean claimsRole( String role ) {
        return description.has( role );
    }
    
    public boolean hasRole( UserInfo user, String role ) {
        if( description.has( role ) & description.property( role ).isReadable() ) {
            return Boolean.TRUE.equals( description.property( role ).get( user ) );
        } else {
            throw new UnsupportedFeatureException( msg( "Cannot read ''{0}'' role on {1.class.name}", role, this ) );
        }
    }
    

}