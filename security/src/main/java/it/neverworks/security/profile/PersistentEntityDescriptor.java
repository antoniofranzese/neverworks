package it.neverworks.security.profile;

import static it.neverworks.language.*;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.persistence.entities.Entities;

public class PersistentEntityDescriptor implements EntityDescriptor {
    
    public String identifyEntity( Object entity ) {
        if( entity != null ) {
            Object id = eval( entity, Entities.metadata( entity.getClass() ).identifier().getName() );
            if( id != null ) {
                return entity.getClass().getName() + "#" + str( id );
            } else {
                throw new IllegalArgumentException( "Cannot identify entity with null id: " + entity.getClass().getName() );
            }
        } else {
            throw new IllegalArgumentException( "Cannot identify null entity" );
        }
    }
    
    public Object retrieveEntity( String identifier ) {
        if( ! empty( identifier ) ) {
            String[] parts = identifier.split( "#" );
            if( parts.length < 2 ) {
                throw new IllegalArgumentException( "Invalid entity identifier: " + identifier );
            } else {
                Class cls = Reflection.findClass( parts[ 0 ] );
                if( cls != null ) {
                    String id;
                    if( parts.length == 2 ) {
                        id = parts[ 1 ];
                    } else {
                        id = Collections.join( Collections.list( parts ).subList( 1, parts.length - 1 ), "#" );
                    }
                    return Entities.load( cls, id );
                } else {
                    throw new IllegalArgumentException( "Invalid entity class: " + parts[ 0 ] );
                }
            }
        } else {
            throw new IllegalArgumentException( "Cannot retrieve entity from empty/null identifier" );
        }
    }
    
}