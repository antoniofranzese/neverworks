package it.neverworks.security.profile;

import it.neverworks.security.context.UserInfo;

public interface RoleProvider extends ProfileProvider {
    public boolean claimsRole( String role );
    public boolean hasRole( UserInfo user, String role );
}