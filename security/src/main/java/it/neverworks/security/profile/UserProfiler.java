package it.neverworks.security.profile;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import org.springframework.beans.factory.InitializingBean;

import it.neverworks.cache.Cache;
import it.neverworks.cache.LRUCache;
import it.neverworks.model.BaseModel;
import it.neverworks.security.context.UserInfo;

public class UserProfiler extends BaseModel implements InitializingBean {
    
    private List<ProfileProvider> providers;
    
    private List<RoleProvider> userRoleProviders;
    private List<FunctionProvider> userFunctionProviders;
    private List<PropertyProvider> userPropertyProviders;
    private Cache<RoleProvider> userRoleCache;
    private Cache<FunctionProvider> userFunctionCache;
    private Cache<PropertyProvider> userPropertyCache;

    private List<EntityRoleProvider> entityRoleProviders;
    private List<EntityFunctionProvider> entityFunctionProviders;
    private List<EntityPropertyProvider> entityPropertyProviders;
    
    private Map<Class, Cache<EntityRoleProvider>> entityRoleCaches;
    private Map<Class, Cache<EntityFunctionProvider>> entityFunctionCaches;
    private Map<Class, Cache<EntityPropertyProvider>> entityPropertyCaches;
    
    public void init() {
        for( ProviderBundle bundle: query( providers ).instanceOf( ProviderBundle.class ) ) {
            expandBundle( bundle );
        }
        
        userRoleProviders = query( providers ).instanceOf( RoleProvider.class ).list();
        userFunctionProviders = query( providers ).instanceOf( FunctionProvider.class ).list();
        userPropertyProviders = query( providers ).instanceOf( PropertyProvider.class ).list();

        userRoleCache = new LRUCache<RoleProvider>().set( "max", 100 );
        userFunctionCache = new LRUCache<FunctionProvider>().set( "max", 500 );
        userPropertyCache = new LRUCache<PropertyProvider>().set( "max", 100 );

        entityRoleProviders = query( providers ).instanceOf( EntityRoleProvider.class ).list();
        entityFunctionProviders = query( providers ).instanceOf( EntityFunctionProvider.class ).list();
        entityPropertyProviders = query( providers ).instanceOf( EntityPropertyProvider.class ).list();

        entityRoleCaches = new HashMap<Class, Cache<EntityRoleProvider>>();
        entityFunctionCaches = new HashMap<Class, Cache<EntityFunctionProvider>>();
        entityPropertyCaches = new HashMap<Class, Cache<EntityPropertyProvider>>();
    }
    
    protected void expandBundle( ProviderBundle bundle ) {
        int index = providers.indexOf( bundle );
        for( ProfileProvider provider: bundle.getProviders() ) {
            providers.add( ++index, provider );
            if( provider instanceof ProviderBundle ) {
                expandBundle( (ProviderBundle) provider );
            }
        }
    }

    /* User Roles */
    
    public boolean claimsUserRole( String role ) {
        return getUserRoleProviderFor( role ).claimsRole( role );
    }

    public boolean hasUserRole( UserInfo user, String role ) {
        if( user.authenticated() ) {
            return getUserRoleProviderFor( role ).hasRole( user, role );
        } else {
            return false;
        }
    }
    
    protected RoleProvider getUserRoleProviderFor( String role ) {
        RoleProvider provider = userRoleCache.get( role );
        if( provider == null ) {
            provider = findUserRoleProviderFor( role );
            userRoleCache.put( role, provider );
        }
        return provider;
    }
    
    protected RoleProvider findUserRoleProviderFor( String role ) {
        for( RoleProvider provider: userRoleProviders ) {
            if( provider.claimsRole( role ) ) {
                return provider;
            }
        }
        return NULL_USER_ROLE_PROVIDER;
    }
    
    /* User Functions */
    
    public boolean claimsUserFunction( String function ) {
        return getUserFunctionProviderFor( function ).claimsFunction( function );
    }

    public boolean hasUserFunction( UserInfo user, String function ) {
        if( user.authenticated() ) {
            return getUserFunctionProviderFor( function ).hasFunction( user, function );
        } else {
            return false;
        }
    }
    
    protected FunctionProvider getUserFunctionProviderFor( String function ) {
        FunctionProvider provider = userFunctionCache.get( function );
        if( provider == null ) {
            provider = findUserFunctionProviderFor( function );
            userFunctionCache.put( function, provider );
        }
        return provider;
    }
    
    protected FunctionProvider findUserFunctionProviderFor( String function ) {
        for( FunctionProvider provider: userFunctionProviders ) {
            if( provider.claimsFunction( function ) ) {
                return provider;
            }
        }
        return NULL_USER_FUNCTION_PROVIDER;
    }
    

    /* User Properties */
    
    public boolean claimsUserProperty( String property ) {
        return getUserPropertyProviderFor( property ).claimsProperty( property );
    }

    public boolean hasUserProperty( UserInfo user, String property ) {
        if( user.authenticated() ) {
            return getUserPropertyProviderFor( property ).hasProperty( user, property );
        } else {
            return false;
        }
    }

    public Object getUserProperty( UserInfo user, String property ) {
        if( user.authenticated() ) {
            return getUserPropertyProviderFor( property ).getProperty( user, property );
        } else {
            return null;
        }
    }

    public void setUserProperty( UserInfo user, String property, Object value ) {
        if( user.authenticated() ) {
            getUserPropertyProviderFor( property ).setProperty( user, property, value );
        }
    }
    
    protected PropertyProvider getUserPropertyProviderFor( String property ) {
        PropertyProvider provider = userPropertyCache.get( property );
        if( provider == null ) {
            provider = findUserPropertyProviderFor( property );
            userPropertyCache.put( property, provider );
        }
        return provider;
    }
    
    protected PropertyProvider findUserPropertyProviderFor( String property ) {
        for( PropertyProvider provider: userPropertyProviders ) {
            if( provider.claimsProperty( property ) ) {
                return provider;
            }
        }
        return NULL_USER_PROPERTY_PROVIDER;
    }

    /* Entity Roles */
    
    public boolean claimsEntityRole( Class type, String role ) {
        return getEntityRoleProviderFor( type, role ).claimsRole( type, role );
    }

    public boolean hasEntityRole( UserInfo user, Object entity, String role ) {
        if( user.authenticated() ) {
            return getEntityRoleProviderFor( entity.getClass(), role ).hasRole( user, entity, role );
        } else {
            return false;
        }
    }
    
    protected EntityRoleProvider getEntityRoleProviderFor( Class type, String role ) {
        if( ! entityRoleCaches.containsKey( type ) ) {
            synchronized( entityRoleCaches ) {
                if( ! entityRoleCaches.containsKey( type ) ) {
                    entityRoleCaches.put( type, new LRUCache<RoleProvider>().set( "max", 100 ) );
                }
            }
        }
        
        Cache<EntityRoleProvider> entityRoleCache = entityRoleCaches.get( type );
        
        EntityRoleProvider provider = entityRoleCache.get( role );
        if( provider == null ) {
            provider = findEntityRoleProviderFor( type, role );
            entityRoleCache.put( role, provider );
        }
        return provider;
    }
    
    protected EntityRoleProvider findEntityRoleProviderFor( Class type, String role ) {
        for( EntityRoleProvider provider: entityRoleProviders ) {
            if( provider.claimsRole( type, role ) ) {
                return provider;
            }
        }
        return NULL_ENTITY_ROLE_PROVIDER;
    }
    
    /* Entity Functions */
    
    public boolean claimsEntityFunction( Class type, String function ) {
        return getEntityFunctionProviderFor( type, function ).claimsFunction( type, function );
    }

    public boolean hasEntityFunction( UserInfo user, Object entity, String function ) {
        if( user.authenticated() ) {
            return getEntityFunctionProviderFor( entity.getClass(), function ).hasFunction( user, entity, function );
        } else {
            return false;
        }
    }
    
    protected EntityFunctionProvider getEntityFunctionProviderFor( Class type, String function ) {
        if( ! entityFunctionCaches.containsKey( type ) ) {
            synchronized( entityFunctionCaches ) {
                if( ! entityFunctionCaches.containsKey( type ) ) {
                    entityFunctionCaches.put( type, new LRUCache<FunctionProvider>().set( "max", 500 ) );
                }
            }
        }
        
        Cache<EntityFunctionProvider> entityFunctionCache = entityFunctionCaches.get( type );
        
        EntityFunctionProvider provider = entityFunctionCache.get( function );
        if( provider == null ) {
            provider = findEntityFunctionProviderFor( type, function );
            entityFunctionCache.put( function, provider );
        }

        return provider;
    }
    
    protected EntityFunctionProvider findEntityFunctionProviderFor( Class type, String function ) {
        for( EntityFunctionProvider provider: entityFunctionProviders ) {
            if( provider.claimsFunction( type, function ) ) {
                return provider;
            }
        }
        return NULL_ENTITY_FUNCTION_PROVIDER;
    }
    

    /* Entity Properties */
    
    public boolean claimsEntityProperty( Class type, String property ) {
        return getEntityPropertyProviderFor( type, property ).claimsProperty( type, property );
    }

    public boolean hasEntityProperty( UserInfo user, Object entity, String property ) {
        if( user.authenticated() ) {
            return getEntityPropertyProviderFor( entity.getClass(), property ).hasProperty( user, entity, property );
        } else {
            return false;
        }
    }

    public Object getEntityProperty( UserInfo user, Object entity, String property ) {
        if( user.authenticated() ) {
            return getEntityPropertyProviderFor( entity.getClass(), property ).getProperty( user, entity, property );
        } else {
            return null;
        }
    }

    public void setEntityProperty( UserInfo user, Object entity,  String property, Object value ) {
        if( user.authenticated() ) {
            getEntityPropertyProviderFor( entity.getClass(), property ).setProperty( user, entity, property, value );
        }
    }
    
    protected EntityPropertyProvider getEntityPropertyProviderFor( Class type, String property ) {
        if( ! entityPropertyCaches.containsKey( type ) ) {
            synchronized( entityPropertyCaches ) {
                if( ! entityPropertyCaches.containsKey( type ) ) {
                    entityPropertyCaches.put( type, new LRUCache().set( "max", 100 ) );
                }
            }
        }
        
        Cache<EntityPropertyProvider> entityPropertyCache = entityPropertyCaches.get( type );
        
        EntityPropertyProvider provider = entityPropertyCache.get( property );
        if( provider == null ) {
            provider = findEntityPropertyProviderFor( type, property );
            entityPropertyCache.put( property, provider );
        }

        return provider;
    }
    
    protected EntityPropertyProvider findEntityPropertyProviderFor( Class type, String property ) {
        for( EntityPropertyProvider provider: entityPropertyProviders ) {
            if( provider.claimsProperty( type, property ) ) {
                return provider;
            }
        }
        return NULL_ENTITY_PROPERTY_PROVIDER;
    }
    
    /* Null Providers */

    private final static RoleProvider NULL_USER_ROLE_PROVIDER = new RoleProvider() {
        
        public boolean claimsRole( String role ) {
            return false;
        }
        
        public boolean hasRole( UserInfo user, String role ) {
            return false;
        }
        
    };

    private final static FunctionProvider NULL_USER_FUNCTION_PROVIDER = new FunctionProvider() {
        
        public boolean claimsFunction( String function ) {
            return false;
        }
        
        public boolean hasFunction( UserInfo user, String function ) {
            return false;
        }
        
    };

    private final static PropertyProvider NULL_USER_PROPERTY_PROVIDER = new PropertyProvider() {
        
        public boolean claimsProperty( String property ) {
            return false;
        }
        
        public boolean hasProperty( UserInfo user, String property ) {
            return false;
        }

        public Object getProperty( UserInfo user, String name ) {
            return null;
        }

        public void setProperty( UserInfo user, String name, Object value ) {
            
        }
        
    };
    
    private final static EntityRoleProvider NULL_ENTITY_ROLE_PROVIDER = new EntityRoleProvider() {
        
        public boolean claimsRole( Class type, String role ) {
            return false;
        }
        
        public boolean hasRole( UserInfo user, Object entity, String role ) {
            return false;
        }
        
    };

    private final static EntityFunctionProvider NULL_ENTITY_FUNCTION_PROVIDER = new EntityFunctionProvider() {
        
        public boolean claimsFunction( Class type, String function ) {
            return false;
        }
        
        public boolean hasFunction( UserInfo user, Object entity, String function ) {
            return false;
        }
        
    };

    private final static EntityPropertyProvider NULL_ENTITY_PROPERTY_PROVIDER = new EntityPropertyProvider() {
        
        public boolean claimsProperty( Class type, String property ) {
            return false;
        }
        
        public boolean hasProperty( UserInfo user, Object entity, String property ) {
            return false;
        }

        public Object getProperty( UserInfo user, Object entity, String name ) {
            return null;
        }

        public void setProperty( UserInfo user, Object entity, String name, Object value ) {
            
        }
        
    };
 
    /* Configuration */
    
    public void afterPropertiesSet() {
        this.init();
    }
    
    public List<ProfileProvider> getProviders(){
        return this.providers;
    }
    
    public void setProviders( List<ProfileProvider> providers ){
        this.providers = providers;
    }
    
    
}