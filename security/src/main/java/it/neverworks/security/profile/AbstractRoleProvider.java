package it.neverworks.security.profile;

import it.neverworks.lang.Strings;
import it.neverworks.security.context.UserInfo;

public abstract class AbstractRoleProvider extends AbstractProfileProvider implements RoleProvider {
    
    public boolean claimsRole( String role ) {
        if( Strings.hasText( prefix ) ) {
            return Strings.safe( role ).startsWith( prefix );
        } else {
            return true;
        }
    }

    public boolean hasRole( UserInfo user, String role ) {
        Boolean result = cache( user ).get( role );
        if( result == null ) {
            result = retrieveRole( user, role );
            cache( user ).put( role, result );
        }
        return result;
    }
    
    public abstract boolean retrieveRole( UserInfo user, String role );

}
