package it.neverworks.security.profile;

import it.neverworks.security.context.UserInfo;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.lang.Objects;

public interface EntityPropertyProvider extends ProfileProvider {
    public boolean claimsProperty( Class type, String name );
    public Object getProperty( UserInfo user, Object entity, String name );
    
    default public boolean hasProperty( UserInfo user, Object entity, String name ) {
        return true;
    }
    
    default public void setProperty( UserInfo user, Object entity, String name, Object value ) {
        throw new UnsupportedFeatureException( "Cannot write '" + name + "' property on entity: " + Objects.repr( entity ) );
    }
    
}