package it.neverworks.security.profile;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.security.context.UserInfo;

public class FunctionDispatcher extends AbstractPropertyDispatcher implements FunctionProvider {
    
    public boolean claimsFunction( String function ) {
        return description.has( function );
    }
    
    public boolean hasFunction( UserInfo user, String function ) {
        if( description.has( function ) & description.property( function ).isReadable() ) {
            return Boolean.TRUE.equals( description.property( function ).get( user ) );
        } else {
            throw new UnsupportedFeatureException( msg( "Cannot read ''{0}'' function on {1.class.name}", function, this ) );
        }
    }
    

}