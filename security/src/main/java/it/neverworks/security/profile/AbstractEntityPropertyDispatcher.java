package it.neverworks.security.profile;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.security.context.UserInfo;

public abstract class AbstractEntityPropertyDispatcher<T> extends AbstractDispatcher implements EntityPropertyProvider {
    
    protected class EntityProperty extends Property {

        public Object get( UserInfo user, Object entity ) {
            return Reflection.invokeMethod( AbstractEntityPropertyDispatcher.this, this.getter, user, entity );
        }

        public void set( UserInfo user, Object entity, Object value ) {
            Reflection.invokeMethod( AbstractEntityPropertyDispatcher.this, this.setter, user, entity, value );
        }
    }

    protected Description<EntityProperty> description = buildDescription( 
        new Description<EntityProperty>(){
            protected EntityProperty newProperty() {
                return new EntityProperty();
            }
        }
        ,list( UserInfo.class, getOwnType() ) 
        ,list( UserInfo.class, getOwnType(), Reflection.AnyType.class )
    );
    
    protected Class ownType;

    protected Class getOwnType() {
        if( this.ownType == null ) {
            this.ownType = Reflection.getGenericType( this.getClass(), 0 );
            if( this.ownType == null ) {
                throw new RuntimeException( msg( "Cannot detect generic type for {0.class.name}, bad implementation", this ) );
            }
        }
        return this.ownType;
    }

}