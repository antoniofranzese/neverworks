package it.neverworks.security.profile;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.security.context.UserInfo;

public abstract class AbstractPropertyDispatcher extends AbstractDispatcher {
    
    protected class UserProperty extends Property {

        public Object get( UserInfo user ) {
            return Reflection.invokeMethod( AbstractPropertyDispatcher.this, this.getter, user );
        }

        public void set( UserInfo user, Object value ) {
            Reflection.invokeMethod( AbstractPropertyDispatcher.this, this.setter, user, value );
        }
    }

    protected Description<UserProperty> description = buildDescription( 
        new Description<UserProperty>(){
            protected UserProperty newProperty() {
                return new UserProperty();
            }
        }
        ,list( UserInfo.class ) 
        ,list( UserInfo.class, Reflection.AnyType.class )
    );

}