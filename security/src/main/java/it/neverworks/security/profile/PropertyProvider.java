package it.neverworks.security.profile;

import it.neverworks.security.context.UserInfo;
import it.neverworks.lang.UnsupportedFeatureException;

public interface PropertyProvider extends ProfileProvider {
    public boolean claimsProperty( String name );
    public Object getProperty( UserInfo user, String name );
    
    default public boolean hasProperty( UserInfo user, String name ) {
        return true;
    }
    
    default public void setProperty( UserInfo user, String name, Object value ) {
        throw new UnsupportedFeatureException( "Cannot write '" + name + "' property on user profile" );
    }
    
}