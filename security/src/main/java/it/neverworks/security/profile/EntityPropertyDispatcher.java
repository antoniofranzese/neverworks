package it.neverworks.security.profile;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.UnsupportedFeatureException;
import it.neverworks.security.context.UserInfo;

public class EntityPropertyDispatcher<T> extends AbstractEntityPropertyDispatcher<T> implements EntityPropertyProvider {
    
    public boolean claimsProperty( Class type, String name ) {
        return type.equals( this.getOwnType() ) && description.has( name );
    }

    public Object getProperty( UserInfo user, Object entity, String name ){
        if( description.has( name ) & description.property( name ).isReadable() ) {
            return description.property( name ).get( user, entity );
        } else {
            throw new UnsupportedFeatureException( msg( "Cannot read ''{0}'' property on {1.class.name}", name, this ) );
        }
    }
    
    public void setProperty( UserInfo user, Object entity, String name, Object value ) {
        if( description.has( name ) & description.property( name ).isReadable() ) {
            description.property( name ).set( user, entity, value );
        } else {
            throw new UnsupportedFeatureException( msg( "Cannot read ''{0}'' property on {1.class.name}", name, this ) );
        }
    }


}