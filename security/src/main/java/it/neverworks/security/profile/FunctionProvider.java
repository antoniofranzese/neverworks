package it.neverworks.security.profile;

import it.neverworks.security.context.UserInfo;

public interface FunctionProvider extends ProfileProvider {
    public boolean claimsFunction( String function );
    public boolean hasFunction( UserInfo user, String function );
}