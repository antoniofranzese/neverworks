package it.neverworks.security.profile;

import it.neverworks.security.context.UserInfo;

public interface EntityRoleProvider extends ProfileProvider {
    public boolean claimsRole( Class type, String role );
    public boolean hasRole( UserInfo user, Object entity, String role );
}