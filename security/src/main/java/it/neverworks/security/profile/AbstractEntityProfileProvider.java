package it.neverworks.security.profile;

import java.util.Set;
import java.util.HashSet;
import java.util.List;

import it.neverworks.lang.ClassScanner;
import it.neverworks.model.Property;

public abstract class AbstractEntityProfileProvider extends AbstractProfileProvider {
    
    @Property
    protected EntityDescriptor descriptor;
    
    public EntityDescriptor getDescriptor(){
        return this.descriptor;
    }
    
    public void setDescriptor( EntityDescriptor descriptor ){
        this.descriptor = descriptor;
    }

    @Property
    private List<Class> knownClasses;
    
    public List<Class> getKnownClasses(){
        return this.knownClasses;
    }
    
    public void setKnownClasses( List<Class> knownClasses ){
        this.knownClasses = knownClasses;
    }
    
    private List<String> scanPackages;
    
    public List<String> getScanPackages(){
        return this.scanPackages;
    }
    
    public void setScanPackages( List<String> scanPackages ){
        this.scanPackages = scanPackages;
    }
    
    protected Set<Class> classes;

    protected boolean knows( Class cls ) {
        if( this.classes == null ) {
            this.classes = new HashSet<Class>();
            if( this.knownClasses != null ) {
                this.classes.addAll( this.knownClasses );
            }
            if( this.scanPackages != null ) {
                this.classes.addAll( new ClassScanner( this.scanPackages ).allClasses() );
            }
        }
        
        if( this.classes.size() > 0 ) {
            return this.classes.contains( cls );
        } else {
            return true;
        }
    }

}
