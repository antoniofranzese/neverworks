package it.neverworks.security.profile;

import it.neverworks.log.Logger;

import it.neverworks.lang.Strings;
import it.neverworks.security.context.UserInfo;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Synthesize;
import it.neverworks.cache.SimpleLRUCache;
import it.neverworks.cache.NullCache;
import it.neverworks.cache.Cache;

public abstract class AbstractProfileProvider extends BaseModel {
    
	protected static final Logger authLogger = Logger.of( "AUTH" );

    @Property @Synthesize
    protected String prefix;

    @Property @Synthesize @AutoConvert
    private int cacheSize = 50;
    
    @Property @Synthesize
    private String cacheKey = null; 

    @Property @Synthesize
    protected Integer cacheTtl;
    
    protected Cache<Boolean> cache( UserInfo user ) {
        if( cacheKey == null ) {
            cacheKey = this.getClass().getName() + ".cache" + ( Strings.hasText( prefix ) ? ( "[" + prefix + "]" ) : "" );
        }
        return user.probeAttribute( cacheKey, () -> cacheSize > 0 ? createCache() : new NullCache() );
    }

    protected Cache createCache() {
        return new SimpleLRUCache()
            .set( "max", cacheSize )
            .set( "ttl", cacheTtl != null ? cacheTtl : -1 );
    }

    

}
