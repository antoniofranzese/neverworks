package it.neverworks.security.profile;

public interface EntityDescriptor {
    String identifyEntity( Object entity );
    Object retrieveEntity( String identifier );
}