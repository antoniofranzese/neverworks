package it.neverworks.security.profile;

import it.neverworks.lang.Strings;
import it.neverworks.security.context.UserInfo;

public abstract class AbstractFunctionProvider extends AbstractProfileProvider implements FunctionProvider {
    
    public boolean claimsFunction( String function ) {
        if( Strings.hasText( prefix ) ) {
            return Strings.safe( function ).startsWith( prefix );
        } else {
            return true;
        }
    }

    public boolean hasFunction( UserInfo user, String function ) {
        Boolean result = cache( user ).get( function );
        if( result == null ) {
            result = retrieveFunction( user, function );
            cache( user ).put( function, result );
        }
        return result;
    }
    
    public abstract boolean retrieveFunction( UserInfo user, String function );

}
