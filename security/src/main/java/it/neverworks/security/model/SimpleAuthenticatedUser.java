package it.neverworks.security.model;

import it.neverworks.security.model.AuthenticatedUser;

public class SimpleAuthenticatedUser implements AuthenticatedUser {
    
    private String userID;
    
    public String getUserID(){
        return this.userID;
    }
    
    public void setUserID( String userID ){
        this.userID = userID;
    }
}