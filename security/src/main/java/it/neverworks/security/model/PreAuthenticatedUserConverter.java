package it.neverworks.security.model;

import it.neverworks.model.converters.Converter;

public class PreAuthenticatedUserConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof AuthenticatedUser ) {
            return value;
        } else if( value instanceof String ) {
            return new PreAuthenticatedUser( (String) value );
        } else {
            return value;
        }
    }

}