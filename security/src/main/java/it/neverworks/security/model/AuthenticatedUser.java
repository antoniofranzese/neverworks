package it.neverworks.security.model;

public interface AuthenticatedUser {
    public String getUserID();
}