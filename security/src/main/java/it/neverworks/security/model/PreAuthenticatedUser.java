package it.neverworks.security.model;

import it.neverworks.security.model.AuthenticatedUser;

public class PreAuthenticatedUser implements AuthenticatedUser {
    
    private String userID;
    
    public PreAuthenticatedUser( String userID ) {
        this.userID = userID;
    }
    
    public String getUserID(){
        return this.userID;
    }
    
    public void setUserID( String userID ){
        this.userID = userID;
    }
    
    public String toString() {
        return this.getClass().getSimpleName() + "(" + this.userID + ")";
    }
}