package it.neverworks.security.context;

import it.neverworks.lang.Guard;
import it.neverworks.security.model.AuthenticatedUser;

public class AuthenticationContext implements Guard<UserInfo> {
    
    private UserInfo info;
    private AuthenticatedUser user;
    
    /* package */ AuthenticationContext( UserInfo info, AuthenticatedUser user ) {
        this.info = info;
        this.user = user;
        this.info.push( this.user );
    }
    
    public UserInfo enter() {
        return this.info;
    }
    
    public Throwable exit( Throwable error ) {
        this.info.pop();
        return error;
    }
    
}