package it.neverworks.security.context;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.Stack;
import java.util.List;

import it.neverworks.security.profile.UserProfiler;
import it.neverworks.security.model.AuthenticatedUser;
import it.neverworks.security.model.PreAuthenticatedUser;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.description.Setter;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Store;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.Property;
import it.neverworks.model.Value;
import it.neverworks.model.Hook;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Wrapper;

public class UserInfo extends AbstractModel implements Retrieve, Store, Inspect {
    
    private final static List<String> RESERVED_PROPERTIES = list( "id", "authenticatedUser", "profiler" );
    
    @Property
    protected AuthenticatedUser authenticatedUser;
    
    @Property
    protected UserProfiler profiler;
    
    protected Map<String, Object> attributes;
    
    protected Stack<StoredUser> userStack;
    
    private class StoredUser {
        private AuthenticatedUser user;
        private Map<String, Object> attributes;
    }

    public AuthenticationContext login( String user ) {
        return new AuthenticationContext( this, new PreAuthenticatedUser( user ) );
    }   
    
    public AuthenticationContext login( AuthenticatedUser user ) {
        return new AuthenticationContext( this, user );
    }   
    
    public void logout() {
        if( authenticated() ) {
            pop();
        }
    }
    
    public void shutdown() {
        while( authenticated() ) {
            pop();
        }
    }
    
    public AuthenticatedUser push( AuthenticatedUser user ) {
        AuthenticatedUser previous = null;
        if( userStack == null ) {
            userStack = new Stack<StoredUser>();
        }
        if( authenticatedUser != null ) {
            StoredUser su = new StoredUser();
            su.user = authenticatedUser;
            su.attributes = attributes;
            userStack.push( su );
            previous = authenticatedUser;
        }
        authenticatedUser = user;
        attributes = null;
        return previous;
    }
    
    public AuthenticatedUser pop() {
        AuthenticatedUser previous = authenticatedUser;
        if( userStack == null || userStack.size() == 0 ) {
            if( authenticatedUser != null ) {
                authenticatedUser = null;
                attributes = null;
            } else {
                throw new IllegalStateException( "No authenticated user to pop" ); 
            }
        } else {
            StoredUser su = userStack.pop();
            authenticatedUser = su.user;
            attributes = su.attributes;
        }
        return previous;
    }
    
    public <T> T getAttribute( String name ) {
        if( attributes != null ) {
            return (T) attributes.get( name );
        } else {
            return null;
        }
    }
    
    public <T> T probeAttribute( String name, Wrapper initializer ) {
        if( attributes == null || ! attributes.containsKey( name ) ) {
            synchronized( this ) {
                if( attributes == null || ! attributes.containsKey( name ) ) {
                    setAttribute( name, initializer != null ? initializer.asObject() : null );
                }
            }
        }
        return getAttribute( name );
    }
    
    public void setAttribute( String name, Object value ) {
        if( attributes == null ) {
            synchronized( this ) {
                if( attributes == null ) {
                    attributes = new HashMap<String, Object>();
                }
            }
        }
        attributes.put( name, value );
    }

    public void removeAttribute( String name ) {
        if( attributes != null  ) {
            attributes.remove( name );
        }
    }

    public boolean hasAttribute( String name ) {
        return attributes != null && attributes.containsKey( name );
    }
    
    public boolean authenticated() {
        return authenticatedUser != null;
    }
    
    public EntityInfo on( Object entity ) {
        return new EntityInfo( this, this.profiler, entity );
    }
    
    public boolean is( String role ) {
        if( profiler != null && profiler.claimsUserRole( role ) ) {
            return profiler.hasUserRole( this, role );
        } else {
            return false;
        }
    } 

    public boolean is( Object entity, String role ) {
        return on( entity ).is( role );
    } 
    
    public boolean can( String function ) {
        if( profiler != null && profiler.claimsUserFunction( function ) ) {
            return profiler.hasUserFunction( this, function );
        } else {
            return false;
        }
    }
    
    public boolean can( Object entity, String function ) {
        return on( entity ).can( function );
    }
    
    public <T> T get( String name ) {
        if( ExpressionEvaluator.isExpression( name ) ) {
            return (T) model().eval( name );
        } else {
            return (T) retrieveItem( name );
        }
    }
    
    public <T> T get( Object entity, String name ) {
        return on( entity ).get( name );
    }

    public boolean has( String name ) {
        return containsItem( name );
    }
    
    public boolean has( Object entity, String name ) {
        return on( entity ).has( name );
    }

    public <T extends UserInfo> T set( String name, Object value ) {
        if( ExpressionEvaluator.isExpression( name ) ) {
            model().assign( name, value );
        } else {
            storeItem( name, value );
        }
        return (T) this;
    }
    
    public <T extends UserInfo> T set( Object entity, String name, Object value ) {
        on( entity ).set( name, value );
        return (T)this;
    }

    public boolean containsItem( Object key ) {
        String name = Strings.valueOf( key );
        if( ! RESERVED_PROPERTIES.contains( name ) && profiler != null && profiler.claimsUserProperty( name ) ) {
            return profiler.hasUserProperty( this, name );
        } else {
            return model().has( name );
        }
    }
    
    public Object retrieveItem( Object key ) {
        String name = Strings.valueOf( key );
        if( ! RESERVED_PROPERTIES.contains( name ) && profiler != null && profiler.claimsUserProperty( name ) ) {
            return profiler.getUserProperty( this, name );
        } else {
            return model().get( name );
        }
    }
    
    public void storeItem( Object key, Object value ) {
        String name = Strings.valueOf( key );
        if( ! RESERVED_PROPERTIES.contains( name ) && profiler != null && profiler.claimsUserProperty( name ) ) {
            profiler.setUserProperty( this, name, value );
        } else {
            model().set( name, value );
        }
    }
    
    public Value value( String name ) {
        return Value.of( get( name ) );
    }
    
    public Value hook( String name ) {
        return Hook.of( get( name ) );
    }
    
    @Virtual
    public String getId(){
        return get( "!authenticatedUser.userID" );
    }

    public String toString() {
        return this.getClass().getSimpleName() + "(" + ( authenticated() ? getId() : "<unknown>" ) + ")";
    }

    protected void setAuthenticatedUser( Setter setter ) {
        setter.set();
        this.attributes = null;
    }
    
    public UserProfiler getProfiler(){
        return this.profiler;
    }
    
    public void setProfiler( UserProfiler profiler ){
        this.profiler = profiler;
    }
    
    public AuthenticatedUser getAuthenticatedUser(){
        return this.authenticatedUser;
    }
    
    public void setAuthenticatedUser( AuthenticatedUser authenticatedUser ){
        this.authenticatedUser = authenticatedUser;
    }

} 