package it.neverworks.security.context;

import it.neverworks.lang.Strings;
import it.neverworks.model.Model;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.security.profile.UserProfiler;

public class EntityInfo implements Model, Retrieve, Store, Inspect {

    private UserInfo user;
    private UserProfiler profiler;
    private Object entity;

    public EntityInfo( UserInfo user, UserProfiler profiler, Object entity ) {
        this.user = user;
        this.profiler = profiler;
        this.entity = entity;
    }
    
    public boolean is( String role ) {
        if( entity != null && profiler != null && profiler.claimsEntityRole( entity.getClass(), role ) ) {
            return profiler.hasEntityRole( user, entity, role );
        } else {
            return false;
        }
    } 
    
    public boolean can( String function ) {
        if( entity != null && profiler != null && profiler.claimsEntityFunction( entity.getClass(), function ) ) {
            return profiler.hasEntityFunction( user, entity, function );
        } else {
            return false;
        }
    }
    
    public <T> T get( String name ) {
        return (T) ExpressionEvaluator.evaluate( this, name );
    }

    public boolean has( String name ) {
        return hasEntityProperty( name );
    }

    @Override
    public <T extends Model> T set( String name, Object value ) {
        if( entity != null && profiler != null && profiler.claimsEntityProperty( entity.getClass(), name ) ) {
            profiler.setEntityProperty( user, entity, name, value );
        }
        return (T) this;
    }

    protected boolean hasEntityProperty( String name ) {
        if( entity != null && profiler != null && profiler.claimsEntityProperty( entity.getClass(), name ) ) {
            return profiler.hasEntityProperty( user, entity, name );
        } else {
            return false;
        }
        
    }

    public boolean containsItem( Object key ) {
        String name = Strings.valueOf( key );
        if( hasEntityProperty( name ) ) {
            return true;
        } else {
            return model().has( name );
        }
    }
    
    public Object retrieveItem( Object key ) {
        String name = Strings.valueOf( key );
        if( hasEntityProperty( name ) ) {
            return profiler.getEntityProperty( user, entity, name );
        } else {
            return model().get( name );
        }
    }
    
    public void storeItem( Object key, Object value ) {
        String name = Strings.valueOf( key );
        if( hasEntityProperty( name ) ) {
            profiler.setEntityProperty( user, entity, name, value );
        } else {
            model().set( name, value );
        }
    }
    
}