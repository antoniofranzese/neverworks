package it.neverworks.security.context;

import it.neverworks.log.Logger;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.context.Inject;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.Required;
import it.neverworks.security.model.AuthenticatedUser;
import it.neverworks.security.model.PreAuthenticatedUserConverter;

public class AuthenticationTask extends BaseModel implements Runnable {

    private static final Logger logger = Logger.of( "AUTH" );
    
    public void run() {
        if( user != null ) {
            logger.debug( "Authenticating as {}", user );
            getUserInfo().push( user );
        }
    }
    
    @Property @Convert( PreAuthenticatedUserConverter.class )
    private AuthenticatedUser user;
    
    public void setUser( AuthenticatedUser user ){
        this.user = user;
    }
    
    @Property @Required @Inject( lazy = true ) 
    private UserInfo userInfo;
    
    public UserInfo getUserInfo(){
        return this.userInfo;
    }
    
    public void setUserInfo( UserInfo userInfo ){
        this.userInfo = userInfo;
    }
}