package it.neverworks.security.context;

import it.neverworks.context.ApplicationEvent;

public class UserLeaveEvent extends ApplicationEvent {

    private UserInfo userInfo;
    
    public UserLeaveEvent( Object source, UserInfo info ) {
        super( source );
        this.userInfo = info;
    }
    
    public UserInfo getUserInfo(){
        return this.userInfo;
    }
}