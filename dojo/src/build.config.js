var dojoConfig = {
    packages: [
        { name: "dojo", location: "dojo" }
        ,{ name: "dijit", location: "dijit" }
        ,{ name: "dojox", location: "dojox" }
        ,{ name: "works", location: "works" }
        ,{ name: "gridx", location: "gridx" }
        ,{ name: "ext", location: "ext" }
    ]
};
