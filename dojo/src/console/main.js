define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"works/json"
    ,"works/storage"
], function( req, declare, lang, has, json, storage ){

    var UserConsole = declare([],{

        levels: {
            "none": 0
            ,"error": 1
            ,"warn": 2
            ,"info": 3
            ,"debug": 4
            ,"trace": 5
        }

        ,constructor: function( name, wrapper ){
            this.name = name;
            this.prefix = has( "ie" ) ? "[" + this.name + "]" : "%c" + this.name + " >";
            this.wrapper = wrapper;
            this.level = this.levels[ "info" ];
        }

        ,parseArguments: function( args, usePrefix ) {
            if( usePrefix == undefined ) {
                usePrefix = true;
            }
            var result = [];

            if( usePrefix ) {
                if( has( "ie") ) {
                    result.push( this.prefix  );
                } else {
                    result.push( this.prefix, "color:gray;font-style:italic;font-size:80%" );
                }
            }

            for( var i=0; i < args.length; i++ ) {
                result.push( args[i] );
            }
            return result;
        }

        ,log: function() {
            if( this.level >= this.levels[ "info" ]  ) {
                var instance = this.wrapper.instance();
                var args = this.parseArguments( arguments );
                instance.log.apply( instance, args );
            }
        }

        ,dir: function() {
            if( this.level >= this.levels[ "info" ]  ) {
                var instance = this.wrapper.instance();
                var args = this.parseArguments( arguments, false );
                for( var i=0; i < args.length; i++ ) {
                    if( lang.isObject( args[i] ) ) {
                        instance.dir.apply( instance, [ args[i] ] );
                    } else {
                        this.log( "DIR: "+ args[i] );
                    }
                }
            }
        }

        ,error: function() {
            if( this.level >= this.levels[ "error" ]  ) {
                var instance = this.wrapper.instance();
                instance.error.apply( instance, this.parseArguments( arguments ) );
            }
        }

        ,warn: function() {
            if( this.level >= this.levels[ "warn" ]  ) {
                var instance = this.wrapper.instance();
                instance.warn.apply( instance, this.parseArguments( arguments ) );
            }
        }

        ,info: function() {
            if( this.level >= this.levels[ "info" ]  ) {
                var instance = this.wrapper.instance();
                instance.info.apply( instance, this.parseArguments( arguments ) );
            }
        }

        ,debug: function() {
            if( this.level >= this.levels[ "debug" ]  ) {
                var instance = this.wrapper.instance();
                instance.debug.apply( instance, this.parseArguments( arguments ) );
            }
        }

        ,trace: function() {
            if( this.level >= this.levels[ "trace" ]  ) {
                var instance = this.wrapper.instance();
                instance.debug.apply( instance, this.parseArguments( arguments ) );
            }
        }

        ,setup: function( level ) {
            var levelKey = ( level + "" ).toLowerCase();
            if( levelKey in this.levels ) {
                this.level = this.levels[ levelKey ];
            }
        }

    });

    var NullConsole = declare([], {
        log: function(){}
        ,dir: function(){}
        ,error: function(){}
        ,warn: function(){}
        ,info: function(){}
        ,debug: function(){}
    });

    var IEConsole = declare([], {
        createMessage: function( args ) {
            var message = "";
            for( var i=0; i < args.length; i++ ) {
                message += ( args[i] + " " );
            }
            return message;
        }
        ,log: function(){
            if( window[ "console" ] != undefined ) {
                console.info( this.createMessage( arguments ) );
            }
        }
        ,dir: function(){
            if( window[ "console" ] != undefined ) {
                console.info( this.createMessage( arguments ) );
            }
        }
        ,error: function(){
            if( window[ "console" ] != undefined ) {
                console.error( this.createMessage( arguments ) );
            }
        }
        ,warn: function(){
            if( window[ "console" ] != undefined ) {
                console.warn( this.createMessage( arguments ) );
            }
        }
        ,info: function(){
            if( window[ "console" ] != undefined ) {
                console.info( this.createMessage( arguments ) );
            }
        }
        ,debug: function(){
            if( window[ "console" ] != undefined ) {
                console.info( this.createMessage( arguments ) );
            }
        }
    });

    var ConsoleWrapper = declare([], {

        constructor: function(){
            this.nullConsole = new NullConsole();
            if( has( "ie" ) ) {
                this.ieConsole = new IEConsole();
            }
        }

        ,instance: function() {
            if( has( "ie" ) ) {
                return this.ieConsole;
            } else if( window[ "console" ] != undefined ) {
                return window.console;
            } else {
                return this.nullConsole;
            }
        }

    });

    var LoggingConsoles = declare([], {
        key: "ts_console_config"

        ,constructor: function() {
            this.consoles = {};
            this.get();

            var handleStorage = lang.hitch( this, function( event ) {
                event = event || window.event;
                //console.log( "Handling In", window );
                //console.dir( event );
                this.updateAll();
            })

            if( window.addEventListener ) {
                window.addEventListener( "storage", handleStorage, false );
            } else {
                window.attachEvent( "onstorage", handleStorage );
            };
        }

        ,loadConfiguration: function() {
            if( this["config"] != undefined) {
                return this.config;
            } else {
                var config = storage.getItem( this.key );
                //console.log( "Retrieved configuration ", config );
                if( config != null ) {
                    try {
                        config = json.parse( config );
                        this.config = config;
                        return config;
                    } catch( e ) {
                        console.error( e.message );
                        return undefined;
                    }
                } else {
                    return undefined;
                }
            }
        }

        ,saveConfiguration: function( config ) {
            //console.log( "Saving configuration", json.stringify( config ) );
            storage.setItem( this.key, json.stringify( config ) );
            this.config = config;
        }

        ,getConfiguration: function() {
            var config = this.loadConfiguration();
            //console.log( "Returned configuration", config );
            if( config == undefined ) {
                config = { logs: [], "default": "error"};
                this.saveConfiguration( config );
                return config;
            } else {
                return config;
            }
        }

        ,setConfiguration: function( value ) {
            //console.log( "Saving", value );
            var config = this.getConfiguration();
            if( "logs" in value ) {
                config.logs = [];
                for( var i = 0; i < value.logs.length; i++ ) {
                    config.logs.push( lang.mixin( {}, value.logs[i] ) );
                }
            }
            if( "default" in value ) {
                config["default"] = value["default"];
            }
            this.saveConfiguration( config );
            this.updateAll();
        }

        ,setup: function( name, level ) {
            if( level == undefined ) {
                level = name;
                name = "*";
            }
            if( name == "*" ) {
                for( var k in self.consoles ) {
                    self.consoles[ k ].setup( l );
                }
            } else {
                this.get( name ).setup( l );
            }
        }

        ,searchLog: function( pattern ) {
            var config = this.getConfiguration();
            //console.log( "Searching", pattern, "in", config );
            if( pattern == "*" ) {
                return { pattern: "*", level: config["default"] };
            } else {
                for( var l in config.logs ) {
                    if( config.logs[l].pattern == pattern ) {
                        return config.logs[l];
                    }
                }
            }
            return undefined;
        }

        ,updateAll: function() {
            for( var name in this.consoles ) {
                this.update( this.consoles[ name ] );
            }
        }

        ,update: function( cns ) {
            var config = this.getConfiguration();
            var name = cns.name;
            var patterns = [ cns.name ];
            while( name.lastIndexOf( "." ) >= 0 ) {
                name = name.substring( 0, name.lastIndexOf( "." ) );
                patterns.push( name );
            }
            patterns.push( "*" );

            for( var i in patterns) {
                var log = this.searchLog( patterns[i] );
                if( log != undefined ) {
                    //console.log( "Configuring", cns.name, "as", log.pattern, log.level );
                    cns.setup( log.level );
                    break;
                }
            }
        }

        ,get: function( name ) {
            name = name || "console";
            if( !( name in this.consoles ) ) {
                var cns = UserConsole( name, new ConsoleWrapper() );
                this.update( cns );
                this.consoles[ name ] = cns;
            }
            return this.consoles[ name ];
        }
    })();

	var ServerConsoleLogger = declare([],{
		constructor: function( consoles ) {
			this.consoles = consoles;
		}
		,log: function( logs ) {
            for( var i = 0; i < logs.length; i++ ) {
				var log = logs[ i ];
				if( log[ "message" ] != undefined ) {
					var source = log[ "source" ] || "server";
					var level = log[ "level" ] || "log";
					this.consoles.get( source )[ level.toLowerCase() ]( log.message )
				}
            }
		}
	})( LoggingConsoles );

    return {

        dynamic: true,

        load: function( id, require, callback ){

            if( id == "MANAGER" ) {
                callback( LoggingConsoles );
			} else if( id == "SERVER" ) {
				callback( ServerConsoleLogger );
            } else {

                if( "" == id ) {
                    if( require.module != undefined ) {
                        var name = require.module.mid.replace( /\//g, "." );
                    } else {
                        var name = null;
                    }
                } else {
                    var name = id;
                }

                callback( LoggingConsoles.get( name ) );

            }

        }

    }

})
