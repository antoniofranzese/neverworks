define([
    "dojo/_base/declare"
    ,"dojo/_base/array"
], function( declare, array ) {
    
    return declare( [], {
        
        log: function() {
            console.log.apply( console, [ this.id + ":" ].concat( array.filter( arguments, function(){ return true; } ) ) );
        }
        
    });

})