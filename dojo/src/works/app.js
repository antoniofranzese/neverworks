define([
    "require"
    ,"works/lang/declare"
	,"dojo/_base/kernel"
	,"dojo/_base/lang"
    ,"dojo/has"
	,"dojo/parser"
	,"dojo/topic"
    ,"dojo/Deferred"
    ,"dojo/Stateful"
    ,"dojo/promise/all"
    ,"works/style"
    ,"works/messages"
    
    ,"dojo/NodeList-dom"
    ,"dojo/NodeList-traverse"
    ,"dojo/NodeList-manipulate"
    
    ,"works/_base/dom-class"
    ,"works/_base/dom-style"
    ,"works/_base/os"
    ,"works/_base/browser"
    ,"works/_base/uacss"
    ,"works/_base/mobile"
    ,"works/_base/json-utils"
    ,"works/_base/environment"
    ,"works/_base/mouse"
    ,"works/_base/defaults"
    ,"works/_base/viewport"
	
    ,"dojo/domReady!"

], function( req, declare, kernel, lang, has, parser, topic, Deferred, Stateful, all, style, messages ) {
	var waitDeferreds = [];
    
    messages.bubble( "neverworks.app.Loading" );    

    topic.subscribe( "app/wait", function( deferred ){
        waitDeferreds.push( deferred );
    })
	
    if( "worksConfig" in kernel.global ) {
        var cfg = kernel.global.worksConfig;
        
        if( "styles" in cfg ) {
            style.configure( cfg.styles );
        }
    }

    var App = declare([ Stateful ],{
        
        constructor: function() {
            this.readyDeferred = new Deferred();
            this.container = "browser";
            this.variant = 1;
            this.ready = false;
        }
        
        ,then: function( resolvedCallback, errorCallback, progressCallback ) {
            return this.readyDeferred.then( resolvedCallback, errorCallback, progressCallback );
        }
        
        ,select: function( parameters ) {
            parameters = parameters || "browser";
            var parts = parameters.split( "/" );

            if( parts.length > 0 ) {
                this.container = parts[ 0 ].trim();
            }

            if( parts.length > 1 ) {
                this.variant = parseInt( parts[ 1 ] );
                if( isNaN( this.variant ) ) {
                    this.variant = 1;
                }
            }
        }
        
    })();
    

    has.add( "container", function( global, document, anElement ){
        return App.get( "container" );
    });

    has.add( "variant", function( global, document, anElement ){
        return App.get( "variant" );
    });

    messages.on( "neverworks.app.Status", function() {
        console.log( "Received status request" );
        if( App.ready ) {
            messages.bubble( "neverworks.app.Ready" );
        }
    });
    
    window.addEventListener( "beforeunload", function( evt ) {
        messages.bubble( "neverworks.app.Close" );
        topic.publish( "app/shutdown" );
    });

    document.addEventListener( "visibilitychange", function() {
        if( document.visibilityState == "hidden" ) {
            topic.publish( "app/hide" );
        } else {
            topic.publish( "app/show" );
        }
    });

    style.flush( req.module.mid ).then( function(){
        //console.log( "App style flushed" );

    	parser.parse().then( function(){
    		topic.publish( "app/startup" );
            all( waitDeferreds ).then( function(){
                App.ready = true;
                messages.bubble( "neverworks.app.Ready" );    
        		topic.publish( "app/ready" );
                console.log( "App ready!" );
                if( document.visibilityState == "hidden" ) {
                    topic.publish( "app/hide" );
                }
                App.readyDeferred.resolve();
            });
    	});
    });


    /* Scroll Fix */
    if( has( "webkit" ) && ! has( "mobile" ) ) {
        document.body.scrollTop = 0;
        document.addEventListener( "scroll", function(){
            document.body.scrollTop = 0;
        });
    }

    return App;

});