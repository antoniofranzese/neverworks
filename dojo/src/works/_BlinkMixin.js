define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/topic"
    ,"dojo/dom-class"
    ,"works/config"
], function( _require, declare, lang, array, topic, domClass, config ){

    var blinker = new declare( [], {
        references: 0
        ,running: false
        ,on: false
        ,delay: config( "blinkDelay", 1000 )

        ,request: function() {
            this.references++;
            if( this.references == 1 ) {
                this.start();
            }
        }
        
        ,release: function() {
            if( this.references > 0 ) {
                this.references--;
                if( this.references == 0 ) {
                    this.stop();
                }
            }
        }
        
        ,start: function() {
            if( this.references > 0 && ! this.running ) {
                if( this.blinkTimeout !== undefined ) {
                    clearTimeout( this.blinkTimeout );
                }
                this.running = true;
                this.blinkTimeout = setTimeout( lang.hitch( this, this.blink ), this.delay );

            }
        }
        
        ,stop: function() {
            if( this.running ) {
                this.running = false;
                if( this.blinkTimeout !== undefined ) {
                    clearTimeout( this.blinkTimeout );
                    topic.publish( "works/blink/off" );
                }
                delete this.blinkTimeout;
                this.on = false;

            }
        }
        
        ,blink: function() {
            this.on = ! this.on
            topic.publish( this.on ? "works/blink/on" : "works/blink/off" );
            this.blinkTimeout = setTimeout( lang.hitch( this, this.blink ), this.delay );
        }
    })();
    
    window.blinker = blinker;
    
    return declare( [], {
        blinking: false
        ,blinkingClass: "worksBlink"

        ,destroy: function() {
            if( this.blinking ) {
                this.stopBlinking();
            }
            this.inherited( arguments );
        }
        
        ,_setBlinkingAttr: function( value ) {
            this._set( "blinking", value == true );
            if( value == true ) {
                this.startBlinking();
            } else {
                this.stopBlinking();
            }
        }
        
        ,startBlinking: function() {
            if( this.blinkTopics === undefined ) {
                this.blinkTopics = [
                    topic.subscribe( "works/blink/on", lang.hitch( this, this.addBlinking ) )
                    ,topic.subscribe( "works/blink/off", lang.hitch( this, this.removeBlinking ) )
                ];
                blinker.request();
            }
        }
        
        ,stopBlinking: function() {
            if( this.blinkTopics !== undefined ) {
                blinker.release();
                array.forEach( this.blinkTopics, function( handler ){ handler.remove() } );
                delete this.blinkTopics;
                this.removeBlinking();
            }
        }
        
        ,restartBlinking: function() {
            if( this.blinking ) {
                this.startBlinking();
            }
        }

        ,suspendBlinking: function() {
            this.stopBlinking();
        }

        ,addBlinking: function() {
            domClass.add( this.blinkNode || this.domNode, this.blinkingClass );
        }

        ,removeBlinking: function() {
            domClass.remove( this.blinkNode || this.domNode, this.blinkingClass );
        }
        
    });
    
});