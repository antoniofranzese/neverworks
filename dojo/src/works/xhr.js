define([ 'dojo/_base/xhr', 'dojo/aspect', 'dojo/_base/json' ], function( xhr, aspect, json ) {
	aspect.before(dojo, "xhr", function( method, parameters ){

    	if( parameters.handleAs == undefined ){
			parameters.handleAs = "json";
		};
		
		if( parameters.body != undefined ) {
			parameters.headers = parameters.headers || {};
			parameters.headers[ "Content-Type" ] = "application/json";
			parameters.postData = json.toJson( parameters.body );
			delete parameters.body;
		}
  	});
	return xhr;
})
