define([
    "works/lang/declare"
    ,"dojo/has"
    ,"works/json"

], function( declare, has, json ){

    has.add( "local-storage", function( global, document, anElement ){
        return "localStorage" in window && window[ "localStorage" ] !== null;
    });

    var KEY = "NeverWorks3";

    var load = function() {
        var storage = localStorage.get( KEY );
        if( storage && typeof( storage ) == "string" && storage.length > 0 ) {
            try {
                return json.parse( storage );
            } catch( ex ) {
                save( {} );
                return {};
            }
        } else {
            save( {} );
            return {};
        }
    }

    var save = function( storage ) {
        storage = storage || {};
        localStorage.setItem( KEY, json.stringify( storage ) );
    }

    return declare([], {

        supported: function() {
            return has( "local-storage" );
        }
        
        ,get: function( key ) {
            var storage = load();
            return key in item ? key[ item ] : {};
        }

        ,set: function( key, value ) {
            var storage = load();
            storage[ key ] = value;
            save( storage );
        }

    })();

});