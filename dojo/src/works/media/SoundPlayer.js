define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-construct"
    ,"dojo/dom-style"
    ,"dojo/on"
    ,"dojo/has"
    ,"dijit/_WidgetBase"
    ,"works/form/_WidgetMixin"
    ,"works/form/_FormWidgetMixin"
    
], function( _require, declare, domConstruct, domStyle, on, has, _WidgetBase, _WidgetMixin, _FormWidgetMixin ){
    
    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

        ,source: null
        ,loop: false
        ,volume: 100
        ,mode: "pause"
        ,playing: false

        ,buildRendering: function() {
            this.inherited( arguments );
            this.audioNode = domConstruct.create( "audio" );
            this.domNode.appendChild( this.audioNode );
            domStyle.set( this.domNode, { display: "none" } );
            this.own(
                on( this.audioNode, "play", lang.hitch( this, this.onPlayStarted ) )
                ,on( this.audioNode, "ended", lang.hitch( this, this.onPlayEnded ) )
            );
        }

        ,onPlayStarted: function() {
            this.playing = true;
        }

        ,onPlayEnded: function() {
            if( has( "safari" ) && this.playing == true ) {
                this.audioNode.pause();
                this.audioNode.load();
            }
            this.playing = false;
        }

        ,play: function() {
            if( ! this.playing ) {
                this.playing = true;
                this.audioNode.play();
            }
        }
        
        ,pause: function() {
            if( this.playing ) {
                this.audioNode.pause();
                this.playing = false;
            }
        }

        ,stop: function() {
            if( this.playing ) {
                this.audioNode.pause();
                this.audioNode.load();
                this.playing = false;
            }
        }
        
        ,_setLoopAttr: function( value ) {
            value = ( value == true );
            this.audioNode.loop = value;
            this._set( "loop", value );
        }

        ,_setVolumeAttr: function( value ) {
            if( value > 100 ) {
                value = 100;
            } else if( value < 0 ) {
                value = 0;
            }

            value = value / 100.0;
            this.audioNode.volume = value;
            this._set( "volume", value );
        }

        ,_setModeAttr: function( value ) {
            if( value == "play" ) {
                this.play();
            } else if( value == "stop" ) {
                this.stop();
            } else {
                this.pause();
            }
            this._set( "mode", value );
        }

        ,_setSourceAttr: function( value ) {
            if( this.playing ) {
                this.stop();
            }
            
            if( this.sourceNode != undefined ) {
                domConstruct.destroy( this.sourceNode );
                delete this.sourceNode;
            }

            if( typeof value == "string" ) {
                value = { path: value };
            }

            if( value !== undefined && value != null && value.path !== undefined ) {
                if( value.type === undefined ) {
                    if( value.path.endsWith( ".ogg" ) ) {
                        value.type = "audio/ogg";
                    } else if( value.path.endsWith( ".mp3" ) ) {
                        value.type = "audio/mp3";
                    } else if( value.path.endsWith( ".wav" ) ) {
                        value.type = "audio/wav";
                    }
                }
                this.sourceNode = domConstruct.create( "source", { src: value.path, type: value.type }, this.audioNode );
                this.audioNode.load();
            }

            this._set( "source", value );
        }

        ,setWidgetState: function( state ) {
            if( state.mode !== undefined ) {
                var mode = state.mode;
                this.inherited( arguments );
                setTimeout( lang.hitch( this, function(){
                    this.set( "mode", mode );
                }))
            } else {
                this.inherited( arguments );
            }
        }
    });
    
});