define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-style"
    ,"dijit/MenuItem"
], function( _require, declare, domStyle, MenuItem ){
    
    return declare( [ MenuItem ], {
        _setVisibleAttr: function( value ) {
            domStyle.set( this.domNode, "display", value ? null : "none" );
        }
        
        ,_setColorAttr: function( value ) {
            domStyle.set( this.domNode, "color", value );
            domStyle.set( this.containerNode, "opacity", value ? "1" : null );
        }

        ,_setBackgroundAttr: function( value ) {
            domStyle.set( this.domNode, "background-color", value );
            domStyle.set( this.containerNode, "opacity", value ? "1" : null );
        }

        ,_setFontAttr: function( value ) {
            domStyle.set( this.containerNode, value );
        }
        
        ,_setCaptionAttr: function( value ) {
            this.set( "label", value );
        }
    });
    
});