define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-style"
    ,"dijit/CheckedMenuItem"
], function( _require, declare, domStyle, CheckedMenuItem ){
    
    return declare( [ CheckedMenuItem ], {
        _setVisibleAttr: function( value ) {
            domStyle.set( this.domNode, "display", value ? null : "none" );
        }
        
        ,_setCaptionAttr: function( value ) {
            this.set( "label", value );
        }
    });
    
});