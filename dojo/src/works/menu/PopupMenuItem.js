define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-style"
    ,"dijit/PopupMenuItem"
], function( _require, declare, domStyle, PopupMenuItem ){
    
    return declare( [ PopupMenuItem ], {
        _setVisibleAttr: function( value ) {
            domStyle.set( this.domNode, "display", value ? null : "none" );
        }
        
        ,_setCaptionAttr: function( value ) {
            this.set( "label", value );
        }
    });
    
});