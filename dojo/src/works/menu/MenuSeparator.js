define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-style"
    ,"dijit/MenuSeparator"
], function( _require, declare, domStyle, MenuSeparator ){
    
    return declare( [ MenuSeparator ], {
        _setVisibleAttr: function( value ) {
            domStyle.set( this.domNode, "display", value ? null : "none" );
        }
    })
    
})