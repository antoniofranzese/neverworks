define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"works/registry"
    ,"dijit/_WidgetBase"
    ,"works/form/_FormWidgetMixin"
    ,"works/form/_WidgetMixin"
    ,"works/layout/Underlay"
    ,"dijit/focus"
    ,"dijit/popup"
    ,"dijit/Menu"
    ,"./MenuItem"
    ,"./CheckedMenuItem"
    ,"./MenuSeparator"
    ,"./PopupMenuItem"
    
    ,"works/style!./resources/Menu.css"
], function( req, declare, array, lang, on, has, dom, domStyle, domConstruct, registry, _WidgetBase, _FormWidgetMixin, _WidgetMixin, Underlay, 
            focus, dijitPopup, dijitMenu, MenuItem, CheckedMenuItem, MenuSeparator, PopupMenuItem ){
    
    var Menu = declare([ dijitMenu ],{
        __openMyself: function() {
            this.inherited( arguments );
            this._onBlur = function() {}
        }

        ,open: function( args ) {
            var parent;
            if( dijitPopup.getTopPopup() !== undefined ) {
                parent = dijitPopup.getTopPopup().widget;
            }
            this._openMyself( lang.mixin({
                parent: parent
            }, args ));
            this._closeMyself = this._onBlur;
            this._onBlur = function() {}
        }

        ,close: function() {
            this._closeMyself();
        }
    });
    
    var MenuWidget = declare( declare.className( req ), [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {
        name: ""
        ,hideRefusalDelay: 0
        ,clicking: false
        
        ,joinForm: function( form ){
            this.inherited( arguments );
            this.announceEvent( "close" );
            this.announceEvent( "clicked", { rename: "click" } );
        }

        ,leaveForm: function(){
            this.inherited( arguments );
            this.retireEvent( "clicked" );
            this.retireEvent( "close" );
        }

        ,startup: function() {
            this.inherited( arguments );
            if( this.startupItems ) {
                this.set( "items", this.startupItems );
                delete this.startupItems;
            }
            if( this.startupOpened ) {
                this.set( "opened", this.startupOpened );
                delete this.startupOpened;
            }
        }

        ,destroy: function(){
            this.destroyMenu();

            this.inherited( arguments );
        }
        
        ,destroyMenu: function() {
            if( this.menu !== undefined ) {
                this.menu.destroyDescendants();
                this.menu.destroy();
                delete this.menu;
            }
            if( this.underlay !== undefined ) {
                this.underlay.destroy();
                delete this.underlay;
            }
        }
            
        ,getWidgetState: function() {
            return undefined;
        }
        
        ,_setItemsAttr: function( items ) {
            if( ! this._started ) {
                this.startupItems = items;
            } else {
                if( this.menu !== undefined ) {
                    this.destroyMenu();
                }
                
                this.menu = this.createMenu( items );
                on( this.menu, "close", lang.hitch( this, this.onMenuClose ) );
                
            }
        }

        ,onMenuExecute: function() {
            this.clicking = true;
            // console.log( "execute" );
        }
        
        ,onMenuClose: function() {
            
            // domStyle.set( this.underlay(), {
            //     display: "none"
            // });
            this.underlay && this.underlay.hide();

            if( this.clicking != true ) {
                this.emitAlways( "close", { state: {} } );
            }
        }

        ,onItemClick: function( item, evt ) {
            // console.log( "click" );
            this.clicking = false;
            //evt.stopPropagation();
            this.emitAlways( "clicked", { state:{ path: item.get( "path" ) } } );
        }
        
        ,createMenu: function( items ) {
            var menu = new Menu();
            array.forEach( items, function( item ){
                if( item.items === undefined ) {
                    var itemClass;
                    
                    if( item.type == "checked" ) {
                        itemClass = CheckedMenuItem;
                    } else if( item.type == "separator" ) {
                        itemClass = MenuSeparator;
                    } else {
                        itemClass = MenuItem;
                    }
                    delete item.type;
                    
                    var menuItem = new itemClass( item );
                    on( menuItem, "click", lang.hitch( this, lang.partial( this.onItemClick, menuItem ) ) );
                    menu.addChild( menuItem );
                    
                } else {
                    item.popup = this.createMenu( item.items );
                    var popupMenuItem = new PopupMenuItem( item );
                    menu.addChild( popupMenuItem );
                    
                }
            }, this );

            on( menu, "execute", lang.hitch( this, this.onMenuExecute ) );
            on( menu, "contextmenu", function( evt ){ 
                evt.preventDefault();
                evt.stopPropagation();
            });
            
            return menu;
        }
        
        ,createUnderlay: function() {
            var underlay = new Underlay();
            
            on( underlay, "touch", lang.hitch( this, function( evt ) {
                if( this._acceptHide == true ) {
                    this.menu.close();
                }
            }));

            on( underlay, "over", lang.hitch( this, function( evt ) {
                var menu = this.menu;
                while ( menu.get( "selected" ) && menu.get( "selected" ).isInstanceOf( PopupMenuItem ) ) {
                    menu = menu.get( "selected" ).popup;
                }
                menu.set( "selected", null );
            }));
            
            return underlay;
        }
        
        
        ,_setAnchorAttr: function( value ) {
            this._set( "anchor", value );
            delete this._openArguments;
        }
        
        ,_getOpenArgumentsAttr: function() {
            var anchor = this.get( "anchor" );
            if( this._openArguments == undefined ) {
                if( anchor && anchor.widget !== undefined ) {
                    var w = widget( anchor.widget );
                    if( w ) {
                        this._openArguments = { target: registry.getMainNode( w ) };
                    } else {
                        throw new Error( "Invalid menu anchor widget: " + value.widget );
                    }
                } else if( anchor && anchor.x !== undefined && anchor.y !== undefined ){
                    this._openArguments = { coords: { x: anchor.x, y: anchor.y } };
                } else {
                    throw new Error( "Invalid menu anchor: " + anchor );
                }
            }
            return this._openArguments;
        }
        
        ,_setOpenedAttr: function( value ) {
            if( ! this._started ) {
                this.startupOpened = value;
            } else {
                if( value ) {
                    this.show();
                } else {
                    this.hide();
                }
            }
        }
        
        ,_getOpenedAttr: function() {
            return this.menu !== undefined ? this.menu.activated : false;
        }
        
        ,getWidgetState: function() {
            return {
                opened: this.get( "opened" )
            }
        }
        
        ,_setChangesAttr: function( changes ) {
            array.forEach( changes, function( change ){
                var index = change.path.lastIndexOf( "." );
                var path = change.path.substring( 0, index );
                var property = change.path.substring( index + 1 );
                var item = this.findItem( path );
                if( item !== undefined ) {
                    item.set( property, change.value );
                } else {
                    throw new Error( "Cannot find menu item for: " + path );
                }
            }, this );
        }
        
        ,setWidgetState: function( state ) {
            if( state.changes ) {
                this.set( "changes", state.changes );
            }

            if( state.anchor ) {
                this.set( "anchor", state.anchor );
            }
            
            if( state.opened ) {
                this.set( "opened", state.opened );
            }
        }
        
        ,findItem: function( path ) {
            var steps = path.split( "." );
            var index = 0;
            var menu = this.menu;
            for( var s = 0; s < steps.length; s++ ) {
                var child;
                for( var c = 0; c < menu.getChildren().length; c++ ) {
                    if( menu.getChildren()[ c ].get( "name" ) == steps[ s ] ) {
                        child = menu.getChildren()[ c ];
                        break;
                    }
                }
                
                if( child !== undefined ) {
                    if( s == steps.length - 1 ) {
                        return child;
                    } else {
                        menu = child.get( "popup" );
                        child = undefined;
                    }
                } else {
                    return undefined;
                }
            }
        }
        
        ,show: function( anchor ) {
            this.clicking = false;

            if( anchor !== undefined ) {
                this.set( "anchor", anchor );
            }

            if( this.get( "anchor" ) !== undefined && this.menu !== undefined ) {
                this.menu.open( this.get( "openArguments" ) );
                focus.focus( this.menu.domNode );
                
                if( this.hideRefusalDelay > 0 ) {
                    this._acceptHide = false;
                    setTimeout( lang.hitch( this, function(){
                        this._acceptHide = true;
                    }), this.hideRefusalDelay );

                } else {
                    this._acceptHide = true;
                }
                
                var zIndex = domStyle.getComputedStyle( this.menu.domNode.parentNode ).zIndex;
                
                if( this.underlay === undefined ) {
                    this.underlay = this.createUnderlay();
                }
                this.underlay.show({ zIndex: zIndex - 1 });
                // domStyle.set( this.underlay(), {
                //     display: "block"
                //     ,zIndex: zIndex - 1
                // });

            }
        }
        
        ,hide: function() {
            this.menu && this.menu.close();
        }
        
    });
    
    MenuWidget.markupFactory = _WidgetMixin.markupFactory;
    
    return MenuWidget;

});