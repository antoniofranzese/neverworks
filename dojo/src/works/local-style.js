define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/dom-attr"
    ,"dojo/dom-construct"
    ,"works/lang/StringBuilder"
    
], function( req, declare, lang, array, domAttr, domConstruct, StringBuilder ){
    
    var LocalStyler = declare( [], {
        
        constructor: function( node, section ) {
            this.node = node;
            this.section = section;
            this.id = "#" + domAttr.get( node, "id" );
            this.style = new StringBuilder( 512 );
        }

        ,set: function( rule, style ) {
            if( style != null && style !== undefined ) {
                this.style.append( rule.replaceAll( "##", this.id ) ).append( " {\n" );
                if( lang.isObject( style ) ) {
                    array.forEach( Object.keys( style ), function( key ){
                        this.style.append( key ).append( ": " ).append( style[ key ] ).append( ";\n");
                    }, this );
                } else {
                    this.style.append( "" + style ).append( ";\n" );
                }
                this.style.append( "}\n" );
            }
            return this;
        }
        
        ,write: function() {
            
            var styleNodes = query( ">style[data-works-local=" + this.section + "]", this.node );
            var styleNode;
            if( styleNodes.length == 0 ) {
                styleNode = domConstruct.create( "style", { 
                    type: "text/css"
                    ,"data-works-local": this.section 
                }, this.node, "last" );
            } else {
                styleNode = styleNodes[ 0 ];
            }
            
            styleNode.innerHTML = this.style.toString();
        }

        ,remove: function() {
            query( ">style[data-works-local=" + this.section + "]", this.node ).remove();
        }
    });
    
    var builder = function( source, section ) {
        var node = source ? ( source.localStyleNode || source.domNode ) : null;
        if( node ) {
            return new LocalStyler( node, section );
        } else {
            throw new Error( "Cannot retrieve main node for " + source )
        }
    }
    
    return builder;
    
});