define([
    "require"
    ,"works/lang/declare"
    ,"dojox/charting/themes/PrimaryColors"
], function( req, declare, base ){
    
    var theme = lang.mixin( lang.clone( base ), {
		axis:{
			stroke:	{ // the axis itself
				color: "#888c76",
				width: 1
			},
			tick: {	// used as a foundation for all ticks
				color:     "#888c76",
				position:  "center",
				font:      "normal normal normal 7pt DejaVuSans, Verdana, Arial, sans-serif",	// labels on axis
				fontColor: "#888c76"								// color of labels
			}
            ,title: {
                font: "normal normal normal 9pt DejaVuSans, Verdana, Arial, sans-serif"
                ,gap: 8
				,fontColor:     "#55584a"
            }
		}
    });
    
    return theme;
    
});