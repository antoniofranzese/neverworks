define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/window"
    ,"dojo/_base/lang"
    ,"dojo/aspect"
    ,"dojo/on"
    ,"dojo/dom-construct"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"works/lang/Model"
    ,"works/layout/Screen"

    ,"works/style!./resources/Charting.css"

], function( req, declare, win, lang, aspect, on, domConstruct, domClass, domStyle, domGeometry, Model, Screen ){
    
    return declare( declare.className( req ), [ Model ], {
        
        constraint: "none"
        ,baseClass: "worksChartSelector"
        
        ,constructor: function( parent ) {
            this.parent = parent;
            this.own( aspect.after( this.parent, "resize", lang.hitch( this, this.resize ) ) );

            this.domNode = domConstruct.create( "div", { "class": this.baseClass }, this.parent.domNode );
            this.marqueeNode = domConstruct.create( "div", { "class": this.baseClass + "Marquee" }, this.domNode );
            
            this.screen = new Screen();

            this.own(
                on( this.domNode, "mousedown", lang.hitch( this, this.onPointerDown ) )
                ,on( this.domNode, "mousemove", lang.hitch( this, this.onPointerMove ) )
                ,on( this.domNode, "mouseup", lang.hitch( this, this.onPointerUp ) )
                ,on( this.screen, "click", lang.hitch( this, this.onCancel ) )
            );
        }
        
        ,destroy: function() {
            if( this.domNode !== undefined ) {
                domConstruct.destroy( this.domNode );
                delete this.marqueeNode;
                delete this.domNode;
            }
            this.inherited( arguments );
        }
        
        ,show: function() {
            this.resize();
            
            domClass.add( this.domNode, "visible" );
            domClass.remove( this.domNode, "selecting" );
            domClass.removeAll( this.marqueeNode, /constraint-.*/ );
            domClass.add( this.marqueeNode, "constraint-" + this.constraint );
            
            this.keyDownSubscription = on( win.doc, "keydown", lang.hitch( this, this.onKeyDown ));
            domGeometry.setMarginBox( this.marqueeNode, {
                t: -50
                ,l: -50
                ,h: 0
                ,w: 0
            });
            
            this.screen.show( this.domNode );
        }
        
        ,hide: function() {
            this.screen.hide();
            
            domClass.remove( this.domNode, "visible" );
            domClass.remove( this.domNode, "selecting" );
            if( this.keyDownSubscription) this.keyDownSubscription.remove();
            delete this.start;
        }
        
        ,resize: function() {
            this.marginBox = this.parent.get( "plotMarginBox" );
            domGeometry.setMarginBox( this.domNode, this.marginBox );
            this.aspectRatio = parseFloat( this.marginBox.w ) / parseFloat( this.marginBox.h );
        }
    
        ,onPointerDown: function( evt ) {
            this.position = domGeometry.position( this.domNode );
            this.start = {
                x: Math.floor( evt.clientX ) - this.position.x
                ,y: Math.floor( evt.clientY ) - this.position.y
            };

            domClass.add( this.domNode, "selecting" );

            this.onPointerMove( evt );
        }

        ,onPointerMove: function( evt ) {
            if( this.start !== undefined ) {
                var points = this.normalizePoints( evt );

                domGeometry.setMarginBox( this.marqueeNode,{
                    t: points.top.y
                    ,l: points.top.x
                    ,h: points.bottom.y - points.top.y
                    ,w: points.bottom.x - points.top.x
                });
            }
        }

        ,normalizePoints: function( evt ) {

            var here = {
                x: Math.floor( evt.clientX ) - this.position.x 
                ,y: Math.floor( evt.clientY ) - this.position.y 
            };
            
            if( this.constraint == "aspect" ) {
                var w = Math.max( this.start.x, here.x ) - Math.min( this.start.x, here.x ) + 1.0;
                var h = Math.max( this.start.y, here.y ) - Math.min( this.start.y, here.y ) + 1.0;
                var currentRatio = w / h;

                if( currentRatio > this.aspectRatio ) {
                    h = w / this.aspectRatio;
                    here.y = here.y > this.start.y ? this.start.y + h - 1 : this.start.y - h + 1;
                } else {
                    w = h * this.aspectRatio;
                    here.x = here.x > this.start.x ? this.start.x + w - 1 : this.start.x - w + 1;
                }
            }
            
            var top = {
                x: Math.max( 0, Math.min( this.start.x, here.x ) )
                ,y: Math.max( 0, Math.min( this.start.y, here.y ) )
            }

            var bottom = {
                x: Math.min( this.marginBox.w, Math.max( this.start.x, here.x ) )
                ,y: Math.min( this.marginBox.h, Math.max( this.start.y, here.y ) )
            }
            
            if( this.constraint == "x" ) {
                top.y = 0;
                bottom.y = this.marginBox.h;
                
            } else if( this.constraint == "y" ) {
                top.x = 0;
                bottom.x = this.marginBox.w
            
            }
            
            return {
                top: top
                ,bottom: bottom
            }
        }
        
        ,onPointerUp: function( evt ) {
            if( this.start !== undefined ) {
            
                var points = this.normalizePoints( evt );
            
                this.hide();

                this.emit( "select", { bubbles: false, view: {
                    top: {
                        x: Math.floor( points.top.x * 10000 / this.marginBox.w ) / 100
                        ,y: Math.floor( points.top.y * 10000 / this.marginBox.h ) / 100
                    }
                    ,bottom: {
                        x: Math.floor( points.bottom.x * 10000 / this.marginBox.w ) / 100
                        ,y: Math.floor( points.bottom.y * 10000 / this.marginBox.h ) / 100
                    } 
                }});
            }
        }
        
        ,onKeyDown: function( evt ) {
            if( evt.keyCode == 27 ) {
                this.onCancel();
            }
        }
        
        ,onCancel: function() {
            this.hide();
            this.emit( "cancel", { bubbles: false });
        }
        
    });
    
});