define([    
    "works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/query"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-attr"
    ,"dojo/dom-geometry"
	,"dojox/charting/plot2d/Base"
    ,"./Selector"
    ,"./Highlighter"

    ,"works/style!./resources/Charting.css"
    ,"dojox/gfx/svgext" 
    
], function( declare, lang, array, query, on, has, domAttr, domGeometry, BasePlot, Selector, Highlighter ){
    
    var _renderLabel = BasePlot.prototype.renderLabel;
    
    lang.extend( BasePlot, {
        renderLabel: function( group, x, y, label, theme, block, align ) {
            // var node = this.inherited( arguments, [ group, x, y, label.value ? label.value : label, theme, block, align ] );
            var node = _renderLabel.apply( this, arguments );
            domAttr.set( node.firstChild, { innerHTML: label } );
            return node;
        }
    });

    return declare( [], {
        
        joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "clicked", { rename: "click" } );
            this.announceEvent( "select" );
            this.announceEvent( "cancel" );
        }

        ,leaveForm: function( form ) {
            this.retireEvent( "cancel" );
            this.retireEvent( "clicked" );
            this.retireEvent( "select" );
            this.inherited( arguments );
        }

        ,startup: function() {
           this.inherited( arguments );

           this.padding = this.padding || {};
           array.forEach([ "t", "b", "l", "r"], function( s ){
               if( this.padding[ s ] === undefined ) {
                   this.padding[ s ] = 10;
               }
           }, this );
           
           this.createChart();
           
           if( this.startupZoom !== undefined ) {
               this.set( "zoom", this.startupZoom );
               delete this.startupZoom;
           }

           if( this.startupHighlight !== undefined ) {
               this.set( "highlight", this.startupHighlight );
               delete this.startupHighlight;
           }
           
           if( this.startupSelector !== undefined ) {
               this.select( this.startupSelector );
               delete this.startupSelector;
           }
           
        }
        
        ,destroy: function() {
            this.inherited( arguments );
        }
        
        ,destroyDescendants: function() {

            if( this.selectorWidget !== undefined ) {
                this.selectorWidget.destroy();
                delete this.selectorWidget;
            }
            
            if( this.highlightWidget !== undefined ) {
                this.highlightWidget.destroy();
                delete this.highlightWidget;
            }
            
        }
       
        ,createTheme: function( theme ) {
            var theme = lang.clone( theme );

            if( this.background ) {
                theme.plotarea.fill = this.background.plot || "transparent";
                theme.chart.fill = this.background.chart || "transparent";
            } else {
                theme.plotarea.fill = "transparent";
                theme.chart.fill = "transparent";
            }
            
            if( this.border ) {
                // set border
            } else {
                this.chart.stroke = null;
            }

            return theme;
        }
    
        ,resize: function( size ) {
            this.inherited( arguments );
            if( this.chart !== undefined ) {
                var s = ( size !== undefined ? size : domGeometry.getMarginBox( this.containerNode ) );
                this.chart.resize({ w: s.w, h: s.h });
            }
        }
        
        ,redraw: function() {
            this.destroyDescendants();

            if( this.chart !== undefined ) {
                
                if( this.get( "zoom" )) {

                    if( this.get( "zoom" ).offset ) {
                        var zoom = this.get( "zoom" );
                        this.chart.setWindow( zoom.scale.x, zoom.scale.y, zoom.offset.x, zoom.offset.y );
                        this.chart.render();

                    } else {
                        this.chart.render();
                        this.set( "zoom", this.get( "zoom" ) );
                    }
                    
                } else {
                    this.chart.render();
                }
                
            }
            
            query( "svg", this.domNode ).attr( "draggable", "false" );
            
            if( this.get( "highlight" ) ) {
                this.set( "highlight", this.get( "highlight" ) );
            }
        }
        
        ,select: function( constraint ) {
            if( constraint != null && constraint !== undefined ) {
                if( this.highlightWidget !== undefined ) {
                    this.highlightWidget.hide();
                }

                if( this.selectorWidget === undefined || this.selectorWidget == null ) {
                    this.selectorWidget = new Selector( this );
                    this.selectorWidget.own( on( this.selectorWidget, "select", lang.hitch( this, this.onSelectorSelect ) ) );
                    this.selectorWidget.own( on( this.selectorWidget, "cancel", lang.hitch( this, this.onSelectorCancel ) ) );
                } 

                this.selectorWidget.set( "constraint", constraint );
                this.selectorWidget.show();

            } else {
                if( this.selectorWidget ) {
                    this.selectorWidget.hide();
                }

                if( this.get( "highlight" ) ) {
                    this.set( "highlight", this.get( "highlight" ) );
                }
            }
        }
        
        ,onSelectorSelect: function( evt ) {
            this.emitAlways( "select", { state: { selection: evt.view } } );

            if( this.get( "highlight" ) ) {
                this.set( "highlight", this.get( "highlight" ) );
            }
        }
        
        ,onSelectorCancel: function( evt ) {
            this.emitAlways( "cancel", { state: {} } );
        }
        
        ,_getPlotMarginBoxAttr: function() {
            if( this.chart !== undefined ) {
                var offsets = this.chart.offsets;
                var marginBox = domGeometry.getMarginBox( this.domNode );

                marginBox.t += offsets.t + 1;
                marginBox.l += offsets.l ; 
                marginBox.h -= offsets.t + offsets.b;
                marginBox.w -= offsets.l + offsets.r;
                
                return marginBox;
            }
        }
        
        ,_getPlotAreaAttr: function() {
            if( this.chart !== undefined ) {
                return this.chart.plotArea;
            }
        }

        ,_setZoomAttr: function( view ) {
            if( this.chart !== undefined ) {
                
                if( view === undefined || view == null ) {
                    this.chart.setWindow( 1, 1, 0, 0 );
                    this._set( "zoom", null );

                } else {
                    view = this.normalizeView( view );
                    var area = this.chart.plotArea;
                    var xScale = 1 / ( ( view.bottom.x - view.top.x ) / 100 );
                    var yScale = 1 / ( ( view.bottom.y - view.top.y ) / 100 );
                    var xOffset = area.width * view.top.x / 100 * xScale;
                    var yOffset = area.height * ( 100 - view.bottom.y ) / 100 * yScale;
            
                    this.chart.setWindow( xScale, yScale, xOffset, yOffset );

                    this._set( "zoom", lang.mixin({
                        scale: {
                            x: xScale
                            ,y: yScale
                        }
                        ,offset: {
                            x: xOffset
                            ,y: yOffset
                        }
                    }, view ) );
                }

                this.chart.render();

            }
            
        }
        
        ,_setHighlightAttr: function( view ) {
            if( view !== undefined && view != null ) {
                if( this.highlightWidget === undefined ) {
                    this.highlightWidget = new Highlighter( this );
                }
                view = this.normalizeView( view );
                this.highlightWidget.show( view );

                this._set( "highlight", view );
                
            } else {
                if( this.highlightWidget !== undefined ) {
                    this.highlightWidget.hide();
                }
                
                this._set( "highlight", null );
            }

        }
        
        ,normalizeView: function( view ) {
            return {
                top: {
                    x: Math.min( 100, Math.max( 0, Math.min( view.top.x, view.bottom.x ) ) )
                    ,y: Math.min( 100, Math.max( 0, Math.min( view.top.y, view.bottom.y ) ) )
                }
                ,bottom: {
                    x: Math.min( 100, Math.max( 0, Math.max( view.top.x, view.bottom.x ) ) )
                    ,y: Math.min( 100, Math.max( 0, Math.max( view.top.y, view.bottom.y ) ) )
                }
            }
        }
        
        ,processAxis: function( axis ) {
            var errorCorrection = 0.0;
            if( ! has( "safari" ) ) {
                errorCorrection = 0.00001;
            }
            
            if( axis.majorTick === undefined ) {
                axis.majorTick = {};
            }
            if( axis.majorTick.width === undefined ) {
                axis.majorTick.width = 1;
            }
            axis.majorTick.width = parseFloat( axis.majorTick.width ) + errorCorrection;

            if( axis.minorTick === undefined ) {
                axis.minorTick = {};
            }
            if( axis.minorTick.width === undefined ) {
                axis.minorTick.width = 1;
            }
            axis.minorTick.width = parseFloat( axis.minorTick.width ) + errorCorrection;
            
            return axis;
        }
    });
    
});