define([
    "require"
    ,"works/lang/declare"
    ,"dojo/dom-geometry"
    ,"./Selector"
], function( req, declare, domGeometry, Selector ){
    
    return declare( declare.className( req ), [ Selector ], {
        baseClass: "worksChartHighlighter"
        
        ,show: function( view ) {
            this.inherited( arguments );
            
            domGeometry.setMarginBox( this.marqueeNode, {
                t: Math.floor( this.marginBox.h * view.top.y / 100 )
                ,l: Math.floor( this.marginBox.w * view.top.x / 100 )
                ,h: Math.floor( this.marginBox.h * ( view.bottom.y - view.top.y + 1 ) / 100 )
                ,w: Math.floor( this.marginBox.w * ( view.bottom.x - view.top.x + 1 ) / 100 )
            });
        }

        ,onPointerDown: function( evt ) {
        
        }
        
        ,onKeyDown: function( evt ) {
        
        }
        
    });
    
});