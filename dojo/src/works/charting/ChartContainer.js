define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"works/layout/ContentPane"
    ,"dojox/charting/Chart"
    ,"./BaseTheme"
	,"./Axis2D"
    ,"works/_WidgetUtils"

    ,"./LineChart"
    ,"./BarChart"
    ,"./PieChart"
    ,"./AreaChart"
    ,"./_ChartMixin"
    
], function( req, declare, array, lang, on, domGeometry, domStyle, domAttr, ContentPane, Chart, Theme, CustomAxis, _WidgetUtils,
    LineChart, BarChart, PieChart, AreaChart, _ChartMixin ){
    
    var TYPES = {
        "Line": LineChart
        ,"Bar": BarChart
        ,"Pie": PieChart
        ,"Area": AreaChart
    }
    
    var ChartContainer = declare( declare.className( req ), [ ContentPane, _ChartMixin ], {
        
        createChart: function() {
            if( this.chart === undefined ) {
                this.chart = new Chart( this.containerNode, {
                    margins: this.padding
                });
            
                this.chart.setTheme( this.createTheme( Theme ) );

                if( this.get( "axes" ) ) {
                    this.set( "axes", this.get( "axes" ) );
                }

                if( this.get( "plots" ) ) {
                    this.set( "plots", this.get( "plots" ) );
                }
            
                if( this.get( "series" ) ) {
                    this.set( "series", this.get( "series" ) );
                }

                this.postCreatePlots();
                
                this.redraw();
            
            }
        }
        
        ,destroyChart: function() {

            if( this.chart !== undefined ) {
                if( this.plotHandlers ) {
                    array.forEach( this.plotHandlers, function( handler ){
                        handler.remove();
                    });
                    delete this.handlers;
                }

                if( this.plots ) {
                    array.forEach( this.plots, function( plot ){
                        plot._plotter.preDestroyPlot( this.chart, plot._name );
                        plot._plotter.destroy();
                        delete plot._plotter;
                    }, this );
                    
                }
                
                this.chart.destroy();
                delete this.chart;
            }
        }
        

        ,postCreatePlots: function(){
            array.forEach( this.plots, function( plot ){
                plot._plotter.createGrid( this.chart, plot._name );
                plot._plotter.postCreatePlot( this.chart, plot._name );
            }, this );
        }
        
        ,_setAxesAttr: function( axes ) {
            this._set( "axes", axes );
             
            if( this.chart !== undefined ) {
                array.forEach( axes, function( axis ){
                    var name = axis._name;
                    
                    axis.type = CustomAxis;
                    this.chart.addAxis( name, this.processAxis( axis ) );
                    
                }, this );
                
            }
        }
        
        ,_setSeriesAttr: function( series ) {
            this._set( "series", series );

            if( this.chart !== undefined ) {
                array.forEach( series, function( seriesDef ){
                    this.chart.addSeries( seriesDef.label, seriesDef.data, seriesDef.attributes );
                }, this );
            }
        }
        
        ,_setPlotsAttr: function( plots ) {
            this._set( "plots", plots );
            
            if( this.chart !== undefined ) {
                this.plotHandlers = [];
                
                array.forEach( plots, function( plot ){
                    var plotterType = TYPES[ plot._type ];
                    var name = plot._name;
                    
                    var plotter = new plotterType( plot );
                    plotter.createPlot( this.chart, name );
                    
                    this.plotHandlers.push( on( plotter, "click", lang.hitch( this, this.onPlotClick ) ) );
                    plot._plotter = plotter;
                    
                }, this );
                
            }
        }
        
        ,onPlotClick: function( state ) {
            this.emitAlways( "clicked", { state: state } );
        }
        
        ,setWidgetState: function( state ) {
            if( state.axes !== undefined ) {
                this.destroyChart();
                this.set( "axes", state.axes );
            }

            if( state.series !== undefined ) {
                this.destroyChart();
                this.set( "series", state.series );
            }

            if( state.plots !== undefined ) {
                this.destroyChart();
                this.set( "plots", state.plots );
            }

            if( state.background !== undefined ) {
                this.destroyChart();
                this.background = state.background;
            }

            this.createChart();
            
            if( state.zoom !== undefined ) {
                this.set( "zoom", state.zoom );
            }
            
            if( state.highlight !== undefined ) {
                this.set( "highlight", state.highlight );
            }
            
            if( state.selector !== undefined ) {
                this.select( state.selector );
            }
        }
        
    });
    
    ChartContainer.markupFactory = _WidgetUtils.markupFactory;

    return ChartContainer;
    
});