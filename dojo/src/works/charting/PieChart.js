define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"./BaseChart"
    ,"dojox/charting/plot2d/Pie"
	,"dojox/charting/action2d/Tooltip"
	,"dojox/charting/action2d/MoveSlice"
    
], function( req, declare, lang, BaseChart, PiePlot, Tooltip, MoveSlice ){
    
    var PieChart = declare( declare.className( req ), [ BaseChart ], {
        
        radius: 200
        ,animation: false
        ,labels: false
        ,labelOffset: 20
        ,labelStyle: "default"
        
        ,createPlot: function( chart, plot ) {
            plot = plot || "default";
            
            var opt = {
                type: PiePlot
                //,radius: this.radius
                ,fontColor: "black"
                ,labels: this.labels
                ,labelOffset: this.labelOffset * -1
                //,labelStyle: this.labelStyle && this.labelStyle.startsWith( "column" ) ? "columns" : "default"
            }
            
            if( this.font ) {
                opt.font = this.font;
            }
            
            chart.addPlot( plot, opt );
            
            if( this.animation ) {
                this.sliceMover = new MoveSlice( chart, plot, { shift: 10 } );
            }
        }
        
        ,postCreatePlot: function( chart, plot ) {
            plot = plot || "default";
            
            this.clickHandler = chart.connectToPlot( plot, lang.hitch( this, function( evt ) {
                if( evt.type == "onclick" ) {
                    var e = this.parseEvent( evt );
                    this.onClick({
                        index: e.index
                        ,value: e.item.y
                        ,key: e.item.k
                        ,series: e.series.serverIndex
                        ,plot: plot
                    });
                }
            }));

            this.tooltipWidget = new Tooltip( chart, plot, {
                text: lang.hitch( this, function( evt ){
                    var item = this.parseEvent( evt ).item;
                    return item.hint ? item.hint : null;
                })
            });
        }

        ,preDestroyPlot: function( chart, plot ) {
            if( this.clickHandler ) {
                this.clickHandler.remove();
            }
            
            if( this.tooltipWidget ) {
                this.tooltipWidget.destroy();
            }
        }
        
    });
    
    PieChart.markupFactory = BaseChart.markupFactory;
   
    return PieChart;
});