define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"./BaseChart"
	,"dojox/charting/plot2d/Areas"
	,"dojox/charting/plot2d/StackedAreas"
    ,"dojox/charting/action2d/Tooltip"
    ,"dojox/gfx/filters"
    
], function( req, declare, lang, BaseChart, Areas, StackedAreas, Tooltip, filters ){
    
    var AreaChart = declare( declare.className( req ), [ BaseChart ], {
        
        grouping: "layered"
        ,markers: false
        ,shadow: false
        
        ,createPlot: function( chart, plot ) {
            plot = plot || "default";
			chart.addPlot( plot, {
				type: this.grouping == "stacked" ? StackedAreas : Areas
                ,hAxis: this.hAxis
                ,vAxis: this.vAxis
                ,markers: this.markers
                ,filter: this.shadow ? filters.shadows.smallDropShadowLight() : undefined
			});
        }
        
        ,postCreatePlot: function( chart, plot ) {
            plot = plot || "default";
            
            this.clickHandler = chart.connectToPlot( plot, lang.hitch( this, function( evt ) {
                if( evt.type == "onclick" ) {
                    var e = this.parseEvent( evt );
                    this.onClick({
                        index: e.index
                        ,value: e.item.y
                        ,key: e.item.k
                        ,series: e.series.serverIndex
                        ,plot: plot
                    });
                }
            }));

            this.tooltipWidget = new Tooltip( chart, plot, {
                text: lang.hitch( this, function( evt ){
                    var item = this.parseEvent( evt ).item;
                    return item.hint ? item.hint : null;
                })
            });
        }

        ,preDestroyPlot: function( chart, plot ) {
            if( this.clickHandler ) {
                this.clickHandler.remove();
            }
            
            if( this.tooltipWidget ) {
                this.tooltipWidget.destroy();
            }
        }
        
    });
    
    AreaChart.markupFactory = BaseChart.markupFactory;

    return AreaChart;
});