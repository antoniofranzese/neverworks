define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"./BaseChart"
	,"dojox/charting/plot2d/Lines"
    ,"dojox/charting/plot2d/MarkersOnly"
	,"dojox/charting/action2d/Tooltip"
    ,"dojox/gfx/filters"

	,"dojox/charting/plot2d/Markers"
    
], function( req, declare, lang, BaseChart, LinesPlot, MarkersOnly, Tooltip, filters ){
    
    var LineChart = declare( declare.className( req ), [ BaseChart ], {
        
        markers: false
        ,shadow: false
        ,lines: true

        ,createPlot: function( chart, plot ) {
            plot = plot || "default";

			chart.addPlot( plot, {
				type: this.lines ? LinesPlot : MarkersOnly
				,markers: this.lines ? this.markers : true
                //,shadow: this.shadow ? { dx: 2, dy: 2, width: 2, color: [0, 0, 0, 0.3] } : null
                ,hAxis: this.hAxis
                ,vAxis: this.vAxis
                ,filter: this.shadow ? filters.shadows.smallDropShadowLight() : undefined
			});
        }
        
        ,postCreatePlot: function( chart, plot ) {
            plot = plot || "default";
            
            this.clickHandler = chart.connectToPlot( plot, lang.hitch( this, function( evt ) {
                if( evt.type == "onclick" ) {
                    var e = this.parseEvent( evt );
                    this.onClick({
                        index: e.index
                        ,value: e.item.y
                        ,key: e.item.k
                        ,series: e.series.serverIndex
                        ,plot: plot
                    });
                }
            }));

            this.tooltipWidget = new Tooltip( chart, plot, {
                text: lang.hitch( this, function( evt ){
                    var item = this.parseEvent( evt ).item;
                    return item.hint ? item.hint : null;
                })
            });
        }
        
        ,preDestroyPlot: function( chart, plot ) {
            if( this.clickHandler ) {
                this.clickHandler.remove();
            }
            
            if( this.tooltipWidget ) {
                this.tooltipWidget.destroy();
            }
        }

    });
    
    LineChart.markupFactory = BaseChart.markupFactory;

    return LineChart;
});