define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"./BaseChart"
	,"dojox/charting/plot2d/Bars"
	,"dojox/charting/plot2d/Columns"
	,"dojox/charting/plot2d/ClusteredBars"
	,"dojox/charting/plot2d/ClusteredColumns"
	,"dojox/charting/plot2d/StackedBars"
	,"dojox/charting/plot2d/StackedColumns"
    ,"dojox/charting/action2d/Magnify"
    ,"dojox/charting/action2d/Highlight"
    ,"dojox/charting/action2d/Tooltip"
    ,"dojox/gfx/filters"

], function( req, declare, lang, BaseChart, PlainBars, PlainCols, ClusteredBars, ClusteredCols, 
    StackedBars, StackedCols, Magnify, Highlight, Tooltip, filters ){
    
    var PLOTS = {
        layeredBars: PlainBars
        ,layeredCols: PlainCols
        ,clusteredBars: ClusteredBars
        ,clusteredCols: ClusteredCols
        ,stackedBars: StackedBars
        ,stackedCols: StackedCols
    }
    
    var BarChart = declare( declare.className( req ), [ BaseChart ], {
        
        horizontal: false
        ,gap: 0
        ,grouping: "clustered"
        ,labelPosition: "outside"
        ,labels: false
        ,shadow: false
        
        ,createPlot: function( chart, plot ) {
            plot = plot || "default";
			chart.addPlot( plot, {
				type: PLOTS[ this.grouping + ( this.horizontal ? "Bars" : "Cols" )]
                ,hAxis: this.hAxis
                ,vAxis: this.vAxis
                ,gap: this.gap
                ,labels: this.labels
                ,labelStyle: this.labelPosition
                ,labelFunc: this.labels ? function( value, fixed, precision ) {
                    return value.text !== undefined ? value.text : "" ;
                } : undefined
                ,filter: this.shadow ? filters.shadows.smallDropShadowLight() : undefined
			});
        }
        
        ,postCreatePlot: function( chart, plot ) {
            plot = plot || "default";
            
            this.clickHandler = chart.connectToPlot( plot, lang.hitch( this, function( evt ) {
                if( evt.type == "onclick" ) {
                    var e = this.parseEvent( evt );
                    this.onClick({
                        index: e.index
                        ,value: e.item.y
                        ,key: e.item.k
                        ,series: e.series.serverIndex
                        ,plot: plot
                    });
                }
            }));

            this.tooltipWidget = new Tooltip( chart, plot, {
                text: lang.hitch( this, function( evt ){
                    var item = this.parseEvent( evt ).item;
                    return item.hint ? item.hint : null;
                })
            });
            
        }
        
        ,preDestroyPlot: function( chart, plot ) {
            if( this.clickHandler ) {
                this.clickHandler.remove();
            }
            
            if( this.tooltipWidget ) {
                this.tooltipWidget.destroy();
            }
        }
        
    });
    
    BarChart.markupFactory = BaseChart.markupFactory;

    return BarChart;
});