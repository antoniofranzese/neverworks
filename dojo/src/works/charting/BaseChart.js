define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"works/lang/abstract"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"works/_WidgetUtils"
    ,"works/layout/ContentPane"
    ,"dojox/charting/Chart"
    ,"dojox/charting/plot2d/Grid"
    ,"./BaseTheme"
	,"./Axis2D"
    ,"./_ChartMixin"
    
], function( req, declare, array, lang, has, abstr, domGeometry, domStyle, domAttr, _WidgetUtils, ContentPane, Chart, Grid, theme, CustomAxis, _ChartMixin ){
    
    var BaseChart = declare( declare.className( req ), [ ContentPane, _ChartMixin ], {
        
        hAxis: "x"
        ,vAxis: "y"
        
        ,createChart: function() {
            if( this.chart === undefined ) {
                this.chart = new Chart( this.containerNode, {
                    margins: this.padding
                });
            
                this.chart.setTheme( this.createTheme( theme ) );

                this.createPlot( this.chart );
                this.createGrid( this.chart );

                if( this.get( "axes" ) ) {
                    this.set( "axes", this.get( "axes" ) );
                }
            
                if( this.get( "series" ) ) {
                    this.set( "series", this.get( "series" ) );
                }

                this.redraw();
            
                this.postCreatePlot( this.chart );
            }
        }
        
        ,destroy: function() {
            this.preDestroyPlot();
            this.inherited( arguments );
        }
        
        ,createPlot: abstr.method( "createPlot( chart )" )
        
        ,createGrid: function( chart, plot ) {
            plot = plot || "default";
            
            
            if( this.grid ) {
                
                delete this.grid.type;
                delete this.grid.hAxis;
                delete this.grid.vAxis;
                var hWidth = this.grid.hWidth !== undefined ? this.grid.hWidth : 1;
                var hColor = this.grid.hColor !== undefined ? this.grid.hColor : "#e9e9e9";
                var vWidth = this.grid.vWidth !== undefined ? this.grid.vWidth : 1;
                var vColor = this.grid.vColor !== undefined ? this.grid.vColor : "#e9e9e9";

                delete this.grid.hWidth;
                delete this.grid.hColor;
                delete this.grid.vWidth;
                delete this.grid.vColor;

                var errorCorrection = 0.0;
                if( has( "ff" ) ) {
                    errorCorrection = 0.01;
                } else if( has( "chrome" ) ) {
                    errorCorrection = 0.0001;
                }

                chart.addPlot( plot + "-grid", lang.mixin({
                    type: Grid
                    ,hMajorLines: false
                    ,hMinorLines: false
                    ,vMajorLines: false
                    ,vMinorLines: false
                    ,majorHLine: { color: hColor, width: parseFloat( hWidth ) + errorCorrection }
                    ,minorHLine: { color: hColor, width: parseFloat( hWidth ) + errorCorrection }
                    ,majorVLine: { color: vColor, width: parseFloat( vWidth ) + errorCorrection } 
                    ,minorVLine: { color: vColor, width: parseFloat( vWidth ) + errorCorrection }
                    ,hAxis: this.hAxis
                    ,vAxis: this.vAxis
                    
                }, this.grid ));
                
            }
        
        }
        
        ,postCreatePlot: function(){}
        ,preDestroyPlot: function(){}
        
        ,_setAxesAttr: function( axes ) {
            this._set( "axes", axes ) 
            if( this.chart !== undefined ) {
                array.forEach( Object.keys( this.chart.axes ), function( key ){
                    this.chart.removeAxis( key );
                }, this );
                
                array.forEach( axes, function( axis ){
                    var name = axis._name;
                    
                    axis.type = CustomAxis;
                    
                    this.chart.addAxis( name, this.processAxis( axis ) );
                    
                }, this );
                
            }
        }
        
        ,_setSeriesAttr: function( series ) {
            this._set( "series", series );
            if( this.chart !== undefined ) {
                while( this.chart.series && this.chart.series.length > 0 ) {
                    this.chart.removeSeries( this.chart.series[ 0 ].name );
                }
                
                array.forEach( series, function( seriesDef ){
                    this.chart.addSeries( seriesDef.label, seriesDef.data, seriesDef.attributes );
                }, this );
            }
        }
        
        ,setWidgetState: function( state ) {
            var render = false;
            
            if( state.axes !== undefined ) {
                this.set( "axes", state.axes );
                render = true;
            }

            if( state.series !== undefined ) {
                this.set( "series", state.series );
                render = true;
            }

            if( state.background !== undefined ) {
                this.background = state.background;
                render = true;
            }

            if( render ) {
                this.chart.render();
            }
            
            if( state.zoom !== undefined ) {
                this.set( "zoom", state.zoom );
            }
            
            if( state.highlight !== undefined ) {
                this.set( "highlight", state.highlight );
            }
            
            if( state.selector !== undefined ) {
                this.select( state.selector );
            }
            
        }
        
        ,onClick: function( state ) {
            this.emitAlways( "clicked", { state: state });
        }
        
        ,parseEvent: function( evt ) {
            return {
                index: evt.index
                ,item: evt.run.data[ evt.index ]
                ,series: evt.run
            }
        }
        
    });
    
    BaseChart.markupFactory = _WidgetUtils.markupFactory;

    return BaseChart;
    
});