define([
    "require"
    ,"works/lang/declare"
    ,"works/lang/strings"
    ,"dojo/date/locale"
    ,"dojo/number"
    ,"dojo/dom-style"
    ,"dojox/charting/axis2d/Default"
], function( req, declare, strings, dojoDate, dojoNumber, domStyle, DefaultAxis ){
    
    var dateFormat = function( value, parameters ) {
        var date = new Date();
        date.setTime( value );
        return dojoDate.format( date, parameters );
    }
    
    var numberFormat = function( value, parameters ) {
        return dojoNumber.format( value, parameters );
    }
    
    return declare( declare.className( req ), [ DefaultAxis ], {
        
        constructor: function( chart, kwargs ) {
            this.opt.labelFunc = lang.hitch( this, this.createLabel );
            
            var pattern;
            if( kwargs ) {
                this.unit = kwargs.unit || "";
                this.pattern = kwargs.pattern;
                this.labelGap = kwargs.labelGap;
                pattern = kwargs.pattern;
            }
            
            if( this.pattern ) {
                if( this.pattern.match( "(dd|MM|yy|hh|HH|mm|ss)" ) ) {
                    this.formatter = dateFormat;
                    this.formatterParameters = { datePattern: pattern, selector: "date" };
                } else {
                    this.formatter = numberFormat;
                    this.formatterParameters = { pattern: pattern };
                }
            }
        
        }

        ,createText: function() {
            var t = this.inherited( arguments );
            if( t.firstChild ) {
                domStyle.set( t.firstChild, {
                    display: "inline-block"
                    ,textAlign: "center"
                });
            }
            return t;
        }

        ,createLabel: function( text, value, precision ) {
            if( this.formatter ) {
                return this.formatter( value, this.formatterParameters ) + this.unit;

            } else if( this.labels && this.labels[ value - 1 ] !== undefined ) {
                return this.labels[ value - 1 ] + this.unit;
                
            } else {
                return text + this.unit;
            }
        }
        
    });
    
});