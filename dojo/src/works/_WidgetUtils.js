define([
    "dojo/query"
    ,"dojo/_base/json"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/has"
    ,"dojo/dom-style"
	,"./lang/strings"
	,"./registry"
    
    
], function( query, json, lang, array, has, domStyle, strings, registry ){

    var parseJsonFragment = function( text ) {
        text = lang.trim( text );
        text = text.indexOf( "{" ) != 0 ? "{" + text + "}" : text; 
        return json.fromJson( text );
        
    }
    
    var parseOptions = function( domNode ) {
        var results = {};
        query( ">script[type='works/props']", domNode).forEach(function(node){
            var body = lang.trim( node.innerHTML );
            if( body ) {
                lang.mixin( results, parseJsonFragment( body ) );
            }
            node.parentNode.removeChild( node );
        });
        
        return results;
    }

    var parseFragments = function( domNode ) {
        var results = {};

        var parseNode = function( node ){
            var props = parseJsonFragment( node.getAttribute( "data-works-props" ) );
            var body = lang.trim( node.innerHTML );
            if( "name" in props ) {
                results[ props.name ] = lang.mixin( props, { body: body });
            }
            node.parentNode.removeChild( node );
        }
        
        query( ">script[type='works/fragment']", domNode).forEach( parseNode );
        query( ">div[data-works-type='works/fragment']", domNode).forEach( parseNode );
        
        return results;
    }
    
    var markupFactory = function( params, node, ctor ) {
        var options = parseOptions( node );
        dojo.mixin( params, options );
        return new ctor( params, node );
    }

    var inferDeclaredClass = function( _require ) {
        return _require.module.mid.replace( new RegExp( "\\/", "g" ), "." );
    }
    
    var registerWidget = function( obj ) {
        var mainNode = obj.domNode !== undefined ? obj.domNode : obj.srcNodeRef;
        obj.id = mainNode !== undefined && strings.hasText( mainNode.id ) ? mainNode.id : registry.getUniqueId( obj.declaredClass.replace( new RegExp( "\\.", "g" ), "_" ) );
        registry.add( obj );
    }

    var unregisterWidget = function( obj ) {
        registry.remove( obj );
    }
    
    var parseEventOrigin = function( evt ) {
        if( has( "ie" ) ) {
            clickX = evt.clientX + this.domNode.scrollLeft;
            clickY = evt.clientY + this.domNode.scrollTop;
        } else {
            clickX = evt.pageX;
            clickY = evt.pageY;
        }
        return {
            clickX: Math.floor( clickX )
            ,clickY: Math.floor( clickY )
        }
    }
    
    return {
        parseOptions: parseOptions
        ,parseFragments: parseFragments
        ,parseEventOrigin: parseEventOrigin
        ,markupFactory: markupFactory
        ,inferDeclaredClass: inferDeclaredClass
        ,registerWidget: registerWidget
        ,unregisterWidget: unregisterWidget
    }

});