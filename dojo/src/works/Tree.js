define([
    "require"
    ,"dojo/_base/declare"
    ,"dijit/Tree"
    ,"./store/TreeMemory"
    ,"./_WidgetUtils"
], function( _require, declare, Tree, TreeMemory, _WidgetUtils ){
    
    var Tree = declare( [ Tree ], {
        declaredClass: _WidgetUtils.inferDeclaredClass( _require )
        
        ,postMixInProperties: function() {
            this.inherited( arguments );
            
            if( this.startupItems ) {
                this.set( "model", new TreeMemory({
                    idProperty: 'id'               
                    ,childrenProperty: 'children'  
                    ,rootName: this.rootName || 'Root'
                    ,data: this.startupItems              
                }));                                
                delete this.startupItems;
            }
            
        }
        
        ,getIconClass: function( node, opened ) {
            if( this.icons ) {
                if( node.icon in this.icons ) {
                    return this.icons[ node.icon ]
                } else {
                    return "dijitEditorIcon dijitEditorIconFullScreen";
                }
            } else {
                return this.inherited( arguments );
            }
            
        }
        
        ,_setItemsAttr: function( value ) {
			this.model.loadAll( value );
        }
        
    });
    
    Tree.markupFactory = _WidgetUtils.markupFactory;
    
    return Tree
})