define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/Evented"
    ,"./BaseAvatar"
    ,"../form/_WidgetMixin"
    
], function( _require, declare, Evented, BaseAvatar, _WidgetMixin ){
    
    return declare( [ BaseAvatar, Evented ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,onEnjoy: function() {
            this.emit( "enjoy", { bubbles: false } );
        }

        ,onNeglect: function() {
            this.emit( "neglect", { bubbles: false } );
        }                       
        
        ,onIgnore: function() {
            this.emit( "ignore", { bubbles: false } );
        }                       
    
    });
    
});