define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/dom-geometry"
    ,"dojo/Evented"
    ,"./Manager"
    
], function( _require, declare, lang, on, topic, domGeometry, Evented, Manager ){
    
    return declare( [ Evented ], {
        dragging: false
        ,over: false

        ,constructor: function( params, node ) {
            declare.safeMixin( this, params );
            if( !lang.isFunction( params.position ) ) {
                if( node ) {
                    this.position = function() {
                        var position = domGeometry.position( node );
                        return {
                            x1: position.x
                            ,y1: position.y
                            ,x2: position.x + position.w - 1
                            ,y2: position.y + position.h - 1
                        }
                    }
                } else {
                    throw new Error( "Missing position() callback or node in Target" );
                }
            }

            this.handlers = [
                topic.subscribe( "works/dnd/start", lang.hitch( this, this.onDragStart ) )
                ,topic.subscribe( "works/dnd/fly", lang.hitch( this, this.onDragFly ) )
                ,topic.subscribe( "works/dnd/stop", lang.hitch( this, this.onDragStop ) )
            ];
            
        }
        
        ,onDragStart: function( evt ) {
            this.drop = this.position();
            this.dragging = true;
            this.over = false;
        }
        
        ,onDragFly: function( evt ) {
            //TODO: Target annidati.
            //Se il target ha dei subtarget, manda l'evento prima a questi
            //e lo gestisce in proprio solo se nessun subtarget ha reclamato la gestione
            
            var drop = this.drop;
            if( evt.x >= drop.x1 && evt.x <= drop.x2
                && evt.y >= drop.y1 && evt.y <= drop.y2 ) {
                
                if( ! this.over ) {
                    this.over = true;
                    Manager.enter( this, evt );
                }
            } else {
                if( this.over ) {
                    this.over = false
                    Manager.leave( this, evt );
                }
            }
        }
        
        ,onDragStop: function( evt ) {
            this.dragging = false;
            this.over = false;
        }
        
        ,destroy: function() {
            for( var i = 0; i < this.handlers.length; i++ ) {
                this.handlers[ i ].remove();
            }
            delete this.handlers;
        }

        ,offer: function( proposal ) {
            var event = {
                accepted: false
                ,bubbles: false
                ,proposal: proposal

                ,accept: function(){
                    this.accepted = true;
                }

                ,reject: function(){
                    this.accepted = false;
                }
            };
            
            this.emit( "offer", event );
            return event.accepted == true;
        }
        
        ,deliver: function( proposal, x, y ) {
            this.emit( "deliver",{
                bubbles: false
                ,proposal: proposal
                ,x: x
                ,y: y
            });
        }
        
    })
    
})