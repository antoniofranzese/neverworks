define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/window"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"dijit/_WidgetBase"
    ,"works/registry"
    ,"./Manager"
    ,"../form/_WidgetMixin"
    ,"../form/_FormWidgetMixin"
    
    ,"works/style!./resources/dnd.css"
    
], function( _require, declare, win, on, domClass, domStyle, domConstruct, _WidgetBase, registry, Manager, _WidgetMixin, _FormWidgetMixin  ){
    
    var ENJOYED = 1;
    var NEGLECTED = -1;
    var IGNORED = 0;

    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,baseClass: ""
        
        ,buildRendering: function( params, node ) {
            domStyle.set( this.srcNodeRef, { display: "none" } );
            
            this.avatarNode = domConstruct.create( "div", { 
                style:{ 
                    width : this.contentWidth || this.srcNodeRef.style.width
                    ,height: this.contentHeight || this.srcNodeRef.style.height
                }
            });
            
            win.body().appendChild( this.avatarNode );
            
            domStyle.set( this.avatarNode, { 
                visibility: "hidden"
                ,position: "absolute"
            });
            
            domClass.add( this.avatarNode, "worksDndAvatar" );

            if( this.baseClass ) {
                domClass.add( this.avatarNode, this.baseClass );
            }
            
            if( this.offset ) {
                this.offsetX = this.offset.x ? parseInt( this.offset.x ) : 0;
                this.offsetY = this.offset.y ? parseInt( this.offset.y ) : 0;
            } else {
                this.offsetX = 0;
                this.offsetY = 0;
            }

            this.domNode = this.srcNodeRef;

            _WidgetMixin.registerWidget( this );
            
        }
        
        ,destroy: function() {
            _WidgetMixin.unregisterWidget( this );
        }
        
        ,style: function( params ) {
            domStyle.set( this.avatarNode, params );
        }

        ,lift: function( x, y ) {
            domStyle.set( this.avatarNode, {
                visibility: "visible"
                ,top: ( y + this.offsetY ) + "px"
                ,left: ( x + this.offsetX ) + "px"
            });

            this.onIgnore();
        }

        ,land: function() {
            domStyle.set( this.avatarNode, { visibility: "hidden" } );
        }

        ,fly: function( x, y ) {
            domStyle.set( this.avatarNode, { 
                top: ( y + this.offsetY ) + "px"
                ,left: ( x + this.offsetX ) + "px" 
            });
        }
        
        ,enjoy: function( target ) {
            this.target = target;
            this.onEnjoy();
        }
    
        ,neglect: function( target ) {
            this.target = target;
            this.onNeglect();
        }
        
        ,ignore: function( target ) {
            if( target === this.target || this.target === undefined ) {
                delete this.target;
                this.onIgnore();
            }
        }
        
        ,onEnjoy: function() {}
        ,onNeglect: function() {}
        ,onIgnore: function() {}
    
    });
    
});