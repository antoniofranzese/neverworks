define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/on"
    ,"dojo/topic"
    
], function( _require, declare, on, topic ){
    
    var DnDManager = declare( [], {
        constructor: function( doc ) {
            this.document = doc;
        }

        ,start: function( source, evt ) {
            this.source = source;
            this.proposal = source.get( "currentProposal" );
            this.avatar = source.get( "currentAvatar" );

            if( this.proposal !== undefined ) {
                //console.log( "Drag start", this.proposal );
                topic.publish( "works/dnd/start", { proposal: this.proposal });
            
                this.avatar.lift( Math.floor( evt.clientX ), Math.floor( evt.clientY ) );

                this.movingHandler = on( this.document, "mousemove", lang.hitch( this, function( evt ){ 
                    var x = Math.floor( evt.clientX ), y =  Math.floor( evt.clientY );
                    this.avatar.fly( x, y );
                    topic.publish( "works/dnd/fly", { x: x, y: y });
                }));
            
                this.upHandler = on( this.document, "mouseup", lang.hitch( this, function( evt ){
                    this.stop( evt );
                }));
            
                this.dragging = true;
            } else {
                //console.log( "Skip dragging" );
            }
        }
    
        ,stop: function( evt ) {
            if( this.dragging == true ) {
                //console.log( "Drag stop" );
                this.avatar.land();
                topic.publish( "works/dnd/stop" );
                this.dragging = false;

                this.movingHandler.remove();
                this.upHandler.remove();
                delete this.movingHandler;
                delete this.upHandler;

                if( this.acceptor !== undefined ) {
                    this.acceptor.deliver( this.proposal, Math.floor( evt.clientX ), Math.floor( evt.clientY ) );
                    delete this.acceptor;
                }

                delete this.proposal;
                delete this.avatar;
                delete this.source;
            }
        }
        
        ,enter: function( target, evt ) {
            if( this.dragging ) {
                if( target.offer( this.proposal ) ){
                    this.avatar.enjoy( target );
                    this.acceptor = target;
                } else {
                    this.avatar.neglect( target );
                    delete this.acceptor;
                }
            }
        }
        
        ,leave: function( target, evt ){
            if( this.dragging ) {
                this.avatar.ignore( target );
                if( target === this.acceptor ) {
                    delete this.acceptor;
                }
            }
        }

    });
    
    return new DnDManager( window.document );
})