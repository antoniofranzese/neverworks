define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/window"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"works/registry"
    ,"./Manager"
    ,"../form/_WidgetMixin"
    
], function( _require, declare, win, on, domClass, domStyle, registry, Manager, _WidgetMixin  ){
    
    var ENJOYED = 1;
    var NEGLECTED = -1;
    var IGNORED = 0;

    return declare( [], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,constructor: function( params, node ) {
            declare.safeMixin( this, params );
            _WidgetMixin.registerWidget( this );
            
        }
        
        ,destroy: function() {
            _WidgetMixin.unregisterWidget( this );
        }
        
        ,style: function( params ) {
        }

        ,lift: function( x, y ) {
        }

        ,land: function() {
        }

        ,fly: function( x, y ) {
        }
        
        ,enjoy: function( target ) {
        }
    
        ,neglect: function( target ) {
        }
        
        ,ignore: function( target ) {
        }
        
        ,onEnjoy: function() {}
        ,onNeglect: function() {}
        ,onIgnore: function() {}
    
    });
    
});