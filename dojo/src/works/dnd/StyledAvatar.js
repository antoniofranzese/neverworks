define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-class"
    ,"../lang/strings"
    ,"dojo/Evented"
    ,"./BaseAvatar"
    ,"../form/_WidgetMixin"
    
], function( _require, declare, domClass, strings, Evented, BaseAvatar, _WidgetMixin ){
    
    var StyledAvatar = declare( [ BaseAvatar ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,constructor: function() {
            if( ! strings.hasText( this.ignoreClass ) ) {
                delete this.ignoreClass;
            }

            if( ! strings.hasText( this.enjoyClass ) ) {
                delete this.enjoyClass;
            }

            if( ! strings.hasText( this.neglectClass ) ) {
                delete this.neglectClass;
            }
        }
        
        ,onEnjoy: function() {
            this.setStyleClass( this.enjoyClass );
        }

        ,onNeglect: function() {
            this.setStyleClass( this.neglectClass );
        }                       
        
        ,onIgnore: function() {
            this.setStyleClass( this.ignoreClass );
        }
        
        ,setStyleClass: function( cls ) {
            if( this.lastClass ) {
                domClass.remove( this.avatarNode, this.lastClass );
                delete this.lastClass;
            }

            if( cls ) {
                domClass.add( this.avatarNode, cls );
                this.lastClass = cls;
            }
        }                       
    
    });

    StyledAvatar.markupFactory = _WidgetMixin.markupFactory;
    
    return StyledAvatar;
    
});