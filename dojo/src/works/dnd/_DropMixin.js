define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-geometry"
    ,"works/lang/strings"
    ,"./Target"
], function( _require, declare, lang, on, domGeometry, strings, Target ){
    
    return declare( [], {
        dropEventName: "drop"
        
        ,getDropTargetNode: function() {
            return this.domNode || this.srcNodeRef;
        }
        
        ,joinForm: function() {
            this.inherited( arguments );
            if( this.acceptance != null ) {
                this.announceEvent( "drop" );
            }
        }

        ,leaveForm: function() {
            this.inherited( arguments );
            if( this.acceptance != null ) {
                this.retireEvent( "drop" );
            }
        }
        
        ,_setAcceptanceAttr: function( value ) {
            if( value !== null ) {
                var acceptance;
                if( value ) {
                    if( typeof value == "string" ) {
                        var code = value.trim();
                        if( code.startsWith( "function" ) ) {
                            var name = "__dnd_acc_" + Math.round( Math.random() * 10000000000 );
                            code = name + "=" + code;
                            acceptance = eval( code );
                            eval( "delete " + name );
                        } else {
                            throw new Error( "Unknown acceptance function specification: " + code );
                        }
                    } else if( lang.isFunction( value ) ) {
                        acceptance = value;
                    } else {
                        throw new Error( "Unknown acceptance: " + value );
                    }
                } else {
                    acceptance = function(){ return false };
                }
                this._set( "acceptance", acceptance );

                if( this.dropTarget === undefined ) {
                    this.dropTarget = new Target( {}, this.getDropTargetNode() );

                    this.own(
                        on( this.dropTarget, "offer", lang.hitch( this, this.onDropOffer ) )
                        ,on( this.dropTarget, "deliver", lang.hitch( this, this.onDropDeliver ) )
                    );
                }

                this.announceEvent( this.dropEventName );
            } else {

                this._set( "acceptance", undefined );
                this.retireEvent( this.dropEventName );
            }
        }
        
        ,onDropOffer: function( evt ) {
            try {
                var acceptance = this.get( "acceptance" );
            
                if( acceptance !== undefined && acceptance( evt.proposal ) ) {
                    evt.accept();
                } else {
                    evt.reject();
                }
            } catch( ex ) {
                evt.reject();
            }
        }
        
        ,onDropDeliver: function( evt ) {
            //console.log( "Delivered", evt.proposal );
            var pos = this.getDropPosition( evt );
            this.emitAlways( this.dropEventName, { 
                state:{ 
                    sender: evt.proposal.source
                    ,key: evt.proposal.key  
                    ,x: pos.x
                    ,y: pos.y 
                } 
            });
        }
        
        ,getDropPosition: function( evt ) {
            var node = this.getDropTargetNode();
            var pos = domGeometry.position( node );
            return {
                x: evt.x - pos.x + node.scrollLeft
                ,y: evt.y - pos.y + node.scrollTop
            }
        }
        
    });
    
})