define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/on"
    ,"works/registry"
    ,"works/_WidgetUtils"
    ,"dojo/Stateful"
    ,"./Manager"
    ,"./NullAvatar"
    
    
], function( _require, declare, on, registry, _WidgetUtils, Stateful, Manager, NullAvatar ){
    
    var NULL_AVATAR = new NullAvatar();
    
    var Source = declare( [ Stateful ], {
        timeout: 0
        
        ,constructor: function( params, node ) {
            declare.safeMixin( this, params );
            this.domNode = node;
            
            this.handlers = [
                on( this.domNode, "mousedown", lang.hitch( this, this.onMouseDown ) )
                ,on( this.domNode, "mouseup", lang.hitch( this, this.onMouseUp ) )
            ];
            
        }
        
        ,onMouseDown: function( evt ) {
            evt.preventDefault ? evt.preventDefault() : event.returnValue = false;
            this.dragTimeout = setTimeout( lang.hitch( this, function(){
                this.onDragStart();
                Manager.start( this, evt );
            }), this.timeout );
        }
        
        ,onMouseUp: function( evt ) {
            if( this.dragTimeout ) {
                clearTimeout( this.dragTimeout );
                delete this.dragTimeout;
            }
        }
        
        ,onDragStart: function() {}
        
        ,_currentProposalGetter: function() {
            var prp = lang.isFunction( this.proposal ) ? this.proposal() : this.proposal;
            return prp !== undefined ? lang.mixin( {}, prp ) : undefined;
        }

        ,_currentAvatarGetter: function() {
            var avt = lang.isFunction( this.avatar ) ? this.avatar() : this.avatar;
            if( typeof avt == "string" ) {
                avt = registry.byId( avt );
            }  
            return avt !== undefined ? avt : NULL_AVATAR;
        }
        
        ,_activeGetter: function() {
            return this.active;
        }
        
        ,_activeSetter: function( value ) {
            this.active = value;
        }
        
        ,destroy: function() {
            for( var i = 0; i < this.handlers.length; i++ ) {
                this.handlers[ i ].remove();
            }
            delete this.handlers;
        }
    });
    
    Source.markupFactory = _WidgetUtils.markupFactory;
    
    return Source;
})