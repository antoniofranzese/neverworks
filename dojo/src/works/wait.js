define([
   "dojo/_base/lang"
   ,"dojo/Deferred"
   ,"works/lang/types" 

], function( lang, Deferred, types ){
    
    return function( runnable, mixin ) {

        if( types.isNumberLike( mixin ) ) {
            mixin = {
                timeout: types.toNumber( mixin )
            }
        }

        if( types.isFunction( runnable ) ) {

            runnable = lang.mixin( {}, mixin, {
                condition: runnable
            });

        } else if( types.isString( runnable ) ) {
            var expression = runnable;

            runnable = lang.mixin( {}, mixin, {
                condition: function(){
                    return lang.getObject( expression ) !== undefined;
                }
            });
            
        } 

    	if( !runnable.delay ) {
    		runnable.delay = 500;
    	}

    	if( !runnable.timeout ) {
    		runnable.timeout = 60000;
    	}
	
    	if( !runnable.condition ) {
    		runnable.condition = function(){ return false; };
    	}
	
        runnable.status = "starting";
    	runnable._elapsed = 0;
        runnable._deferred = new Deferred();
        
    	runnable.cancel = function() {
    		this.cancelled = true;
    	}
	
    	runnable._run = function() {
            if( this.log ) {
                console.log( "Waiting:", this.log );
            }
            
    		this._elapsed += this.delay;

    		if( this.cancelled ) {
                this.status = "cancelled";
    			if( types.isFunction( this.onCancel ) ) {
    				this.onCancel();
    			}
                this._deferred.cancel( this );
    		
            } else if( this.condition( this ) ) {
                this.status = "ok";
				if( types.isFunction( this.onExecute ) ){
                    this.onExecute();
                }
                this._deferred.resolve( this );

			} else if( this.timeout >0 && this._elapsed >= this.timeout ) {
                this.status = "timeout";
				if( types.isFunction( this.onTimeout ) ) {
                    this.onTimeout();
                };
                this._deferred.reject( this );

			} else {
                this.status = "waiting";
				if( types.isFunction( this.onWait ) ){ 
                    this.onWait();
                }
                this._deferred.progress( this );

				setTimeout( lang.hitch( this, this._run ), this.delay );
			}

    	}
	
    	setTimeout( lang.hitch( runnable, runnable._run ), 1 );
    	return runnable._deferred;	
    }    
});
