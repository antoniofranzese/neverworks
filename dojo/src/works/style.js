define([
    "require"
    ,"dojo/_base/array"
    ,"dojo/query"
    ,"dojo/dom-construct"
    ,"dojo/dom-attr"
    ,"dojo/has"
    ,"dojo/_base/window"
    ,"dojo/Deferred"
    ,"dojo/promise/all"
    
    ,"works/_base/browser"

], function( _require, array, query, domConstruct, domAttr, has, win, Deferred, all ){
    
    var cssMid = _require.toAbsMid( "./css" );
    
    var currentTheme = domAttr.get( win.body(), "class").trim();
    
    var registry = {
        mids: {}

        ,append: function( mid, url, theme ) {
            if( !( mid in this.mids ) ) {
                this.mids[ mid ] = { styles: [], flushed: false };
            }
            var cfg = this.mids[ mid ];
            cfg.styles.push( { url: url, theme: theme } );
            if( cfg.flushed ) {
                //console.log( "Reflush for", mid, url );
                this.flush( mid );
            } // else {
//                 console.log( "Appending for", mid, url );
//             }
            
        }
        
        ,flush: function( mid ) {
            //console.log( "Flushing", mid );
            var d = new Deferred();
            var requires = [];
            if( mid in this.mids ) {
                var styles = this.mids[ mid ].styles;
                
                var requires = array.map( styles, function( style ){
                    if( !style.theme || style.theme == currentTheme ) {
                        //console.log( "Recovering", mid, ":", style.url);
                        var r = new Deferred();
                        require([ cssMid + "!" + style.url ], function() {
                            //console.log( "Recovered", mid, ":", style.url);
                            r.resolve();
                        });
                        return r;
                    }
                    
                }, this );
                
                // for( var i = 0; i < styles.length; i++  ) {
                //     var style = this.mids[ mid ].styles[ i ];
                //     if( !style.theme || style.theme == currentTheme ) {
                //         console.log( "Recovering", mid, ":", style.url);
                //         var r = new Deferred()
                //         requires.push( r );
                //         require([ cssMid + "!" + style.url ], function() {
                //             console.log( "Recovered", mid, ":", style.url);
                //             r.resolve();
                //         });
                //     }
                // }

                this.mids[ mid ] = { styles: [], flushed: true }; 
                all( requires ).then( function(){
                    //console.log( "All resolved", mid )
                    d.resolve();
                });
            } else {
                d.resolve();
            }
            return d;
        }
        
        
    }

    return {
        dynamic: true
        
        ,configure: function( config ) {
            for( var i in config ) {
                //console.log( "Configuring", config[i] );
                registry.append( config[i].module, config[i].add, config[i].theme );
            }
        }
        
        ,load: function( id, require, callback ) {
            try {
                //console.log( require.module.mid, "requested", id );
                require([ cssMid + "!" + id ], function(){
                    registry.flush( require.module.mid ).then( callback );
                    //callback();
                });
                
            } catch( e ) {
                console.error( "Error loading", id, "style:", e );
            }
        }
        
        ,append: function( mid, url ) {
            registry.append( mid, url );
        }
        
        ,flush: function( mid ) {
            return registry.flush( mid );
        }
        
    }
})