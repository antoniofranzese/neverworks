define([
    "dojo/_base/declare"
], function( declare ) {
    
    // Marker mixin
    return declare( [], {
        acquireFocus: function() {}
        ,resignFocus: function() {}
    });

})