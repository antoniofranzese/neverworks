define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"dojo/date/stamp"
    ,"dojo/has!mobile?./_MobileTextBox:dijit/form/TimeTextBox"
    ,"./_TextMixin"
    ,"./_WidgetMixin"
], function( req, declare, lang, has, dateStamp, baseTextBox, _TextMixin, _WidgetMixin ){
    
    var TimeTextBox = has( "mobile" )
    
        ? declare( declare.className( req ), [ baseTextBox ], {
            type: "time"
            ,iconClass: "time"
        })
    
    
        : declare( declare.className( req ), [ baseTextBox, _WidgetMixin, _TextMixin ], {
            getWidgetState: function() {
                var value = this.get( "value" );
                return {
                    value: value != null ? dateStamp.toISOString( value, { selector: "time" } ) : null
                }
            }
        });
    
    TimeTextBox.markupFactory = _WidgetMixin.markupFactory;
    
    return TimeTextBox;
});