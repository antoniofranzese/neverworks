define([
    "require"
    ,"works/lang/declare"
    ,"dojo/has"
    ,"dojo/dom-attr"
    ,"dijit/form/NumberTextBox"
    ,"./_TextMixin"
    ,"./_WidgetMixin"
], function( req, declare, has, domAttr, NumberTextBox, _TextMixin, _WidgetMixin ){
    
    var NumberTextBox = declare( declare.className( req ), [ NumberTextBox, _WidgetMixin, _TextMixin ], {
        
        getWidgetState: function() {
            if( !( this.isReallyValid( /* focused */ false ) ) ) {
                this._setValueAttr( this._lastValidValue, /* priority */ false );
            }
            return this.inherited( arguments );
        }

        ,_setValueAttr: function(/*Number*/ value, /*Boolean?*/ priorityChange, /*String?*/ formattedValue){
            this.inherited( arguments, [ value, priorityChange, value == 0 ? "0" : formattedValue ]);
        }
    });
    
    if( has( "mobile" ) ) {
        NumberTextBox = declare( declare.className( req ), [ NumberTextBox ], {
            type: "number"
        
            ,buildRendering: function() {
                this.inherited( arguments );
                domAttr.set( this.focusNode, "type", "number" );
            }
        });
    }
    
    NumberTextBox.markupFactory = _WidgetMixin.markupFactory;
    
    return NumberTextBox;
});