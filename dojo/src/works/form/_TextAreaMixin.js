define([
    "dojo/_base/declare"
    ,"dojo/dom-class"
    ,"dojo/dom-attr"
    ,"dojo/dom-style"
    ,"works/_WidgetUtils"
], function( declare, domClass, domAttr, domStyle, utils ){
    
    return declare( [], {
        
        getWidgetState: function() {
            if( ! this.readonly ) {
                return {
                    value: this.get( "value" )
                }
            }
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setReadonlyAttr: function( value ) {
            this.inherited( arguments );
            domClass[ value == true ? "add" : "remove" ]( this.textbox, "readonly" );
            domAttr[ value == true ? "set" : "remove" ]( this.textbox, "readonly", true );
            this._set( "readonly", value );
        }
        
        ,_setResizeAttr: function( value ) {
            domStyle.set( this.textbox, "resize", value );
            delete this.resize;
        }

        ,_setColorAttr: function( value ) {
            domStyle.set( this.domNode, "color", value );
        }
        
        ,_setBackgroundColorAttr: function( value ) {
            domStyle.set( this.domNode, "backgroundColor", value );
        }
	 
        ,_setFontAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setBorderAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }


    });

});