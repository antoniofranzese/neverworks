define([
    "dojo/_base/array"
], function( array ){
    
     var formatter = function( xml ) {
        var formatted = '';
        xml = xml.replace( /(>)\s*(<)(\/*)/g, '$1\r\n$2$3' );
        var pad = 0;
        array.forEach( xml.split( '\r\n' ), function( node ) {
            //console.log( "node", "|" + node + "|" );
            var indent = 0;
            if( node.match( /.+<\/\w[^>]*>$/ ) ) {
                indent = 0;
            } else if( node.match( /^<\/\w/ ) ) {
                if( pad != 0 ) {
                    pad -= 1;
                }
            } else if( node.match( /^<\w[^>]*[^\/]>.*$/ ) ) {
                indent = 1;
            } else {
                indent = 0;
            }

            var padding = '';
            for( var i = 0; i < pad; i++ ) {
                padding += '\t';
            }

            formatted += padding + node.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;') + '\r\n';
            pad += indent;
        });

        return formatted;
    }

    return formatter;
    
})