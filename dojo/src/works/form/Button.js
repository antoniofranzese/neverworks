define([
    "require"
    ,"works/lang/declare"
	,"dojo/_base/lang"
    ,"dojo/has"
    ,"dojo/topic"
    ,"dojo/query"
    ,"dojo/on"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-attr"
    ,"dojo/dom-construct"
    ,"works/_base/click"
    ,"dijit/form/Button"
    ,"works/_BlinkMixin"
    ,"./_WidgetMixin"
    ,"./_PickMixin"
    ,"./_HintMixin"
], function( req, declare, lang, has, topic, query, on, domStyle, domClass, domGeometry, domAttr, domConstruct, click, Button, _BlinkMixin, _WidgetMixin, _PickMixin, _HintMixin ){
    
    var TOUCH_INSPECT_DELAY = lang.getObject( "worksConfig.touchInspectDelay" );
    var TOUCH_DRAG_TRESHOLD = lang.getObject( "worksConfig.touchDragTreshold" );

    var Button = declare( declare.className( req ), [ Button, _WidgetMixin, _PickMixin, _HintMixin, _BlinkMixin ], {

        locked: false
        ,blinkingClass: "worksButtonBlink"

        ,buildRendering: function() {
            this.inherited( arguments );
            if( has( "mobile" ) ) {
                this.own(
                    on( this.domNode, "touchstart", lang.hitch( this, this.onTouchStart ) )
                    ,on( this.domNode, "touchend", lang.hitch( this, this.onTouchEnd ) )
                    ,on( this.domNode, "touchmove", lang.hitch( this, this.onTouchMove ) )
                );
            } else {
                this.domNode.oncontextmenu = lang.hitch( this, function( evt ){
                    this.onRightClick( evt );
                    return false;
                });
            }
        }
        
        ,startup: function() {
            this.inherited( arguments );
            if( this._height ) {
                this.set( "height", this._height );
            }
        }
        
        ,joinForm: function() {
            this.inherited( arguments );
            this.announceEvents([
                has( "mobile" ) ? { name: "touch", rename: "click" } : { name: "click" }
                ,{ name: "inspect" }
            ]);
        }
        
        ,leaveForm: function() {
            this.abandonForm();
            this.inherited( arguments );
        }
        
        ,__onClick: function( evt ) {
            // Conserva il punto di click in questo metodo a causa di un bug di Dojo 1.9.x
            // che comporta la perdita delle coordinate nell'evento che arriva a onClick
    		if( has( "ie" ) ) {
    			this.clickX = evt.clientX + this.domNode.scrollLeft;
    			this.clickY = evt.clientY + this.domNode.scrollTop;
    		} else {
    			this.clickX = evt.pageX;
    			this.clickY = evt.pageY;
    		}
            
            if( this.eventActive( "click" ) ) {
                return this.inherited( arguments );
            }
        }
        
        ,unlock: function() {
            if( this.lockTimeout ) {
                clearTimeout( this.lockTimeout );
                delete this.lockTimeout;
            }
            setTimeout( lang.hitch( this, function(){
                this.locked = false;
            }), 250 );
        }
        
        ,lock: function() {
            if( this.lockTimeout ) {
                clearTimeout( this.lockTimeout );
                delete this.lockTimeout;
            }
            this.locked = true;
            this.lockTimeout = setTimeout( lang.hitch( this, this.unlock ), 10000 );
        }
        
        ,onClick: function( evt ) {
            if( ! this.locked ) {
                this.lock();
                lang.mixin( evt, {
                    state: {
                        x: ( typeof this.clickX == "number" ) ? Math.floor( this.clickX ) : 0,
                        y: ( typeof this.clickY == "number" ) ? Math.floor( this.clickY ) : 0
                    }
                    ,widget: this
                    ,listener: lang.hitch( this, this.unlock )
                })
            } else {
                lang.mixin( evt,{
                    stopped: true
                })
            }
            return this.inherited( arguments );
        }
        
        ,onRightClick: function( evt ) {
            var origin = this.parseEventOrigin( evt );
            this.emitEvent( "inspect", { state: {
                x: origin.clickX
                ,y: origin.clickY
            }});
        }

        ,_cssMouseEvent: has( "mobile" ) ? function(){} : function() { this.inherited( arguments ); }
        
        ,onTouchStart: function( evt ) {
            evt.preventDefault();
            //evt.stopPropagation();
            
            this._setActive( false );
            setTimeout( lang.hitch( this, function(){
                this.moving = true;
                if( this.touching ) {
                    this._setActive( true );
                }
            }), 100 );
            
            this.touching = true;
            this.touchStartPoint = click.point( evt );
            
            this.inspectTimeout = setTimeout( lang.hitch( this, function(){
                delete this.inspectTimeout;
                
                this._setActive( false );

                if( this.touching ) {
                    this.touching = false;

                    this.emitEvent( "inspect", { 
                        state: this.touchStartPoint
                        ,changedTouches: evt.changedTouches
                        ,widget: this }
                    );

                }
            }), TOUCH_INSPECT_DELAY );
            
        }

        ,onTouchMove: function( evt ) {
            if( click.distance( this.touchStartPoint, evt ) >= TOUCH_DRAG_TRESHOLD ) {
                this._setActive( false );
                this.touching = false;
            }
        }
        
        ,onTouchEnd: function( evt ) {

            if( this.touching ) {
                this._setActive( true );
                setTimeout( lang.hitch( this, function(){
                    this._setActive( false );
                }), 200 );
            }

            if( this.inspectTimeout ) {
                clearTimeout( this.inspectTimeout );
                delete this.inspectTimeout;
            }
            
            if( this.touching ) {
                this.touching = false;
                
                this.emitEvent( "touch", { 
                    state: this.touchStartPoint
                    ,changedTouches: evt.changedTouches
                    ,widget: this } 
                );
            }
            
        }
        
        ,_setActive: function( value ) {
            this.active = value;
            try {
                this._setStateClass();
            } catch( ex ){}
        }

        ,_fireClickEvent: function( state ) {
            //this._setActive( true );
            
            setLocalTimeout( lang.hitch( this, function(){
                this._setActive( false );
            }), 500 );

            if( state ) {
                this.clickX = state.x;
                this.clickY = state.y;
            } else {
        		var position = domGeometry.position( this.domNode, true );
        		this.clickX = Math.round( position.x + ( position.w / 2 ) )
    	        this.clickY = Math.round( position.y + ( position.h / 2 ) )
            }
            this.valueNode.click();
        }

        ,getWidgetState: function() {
            // Non ci sono attributi modificabili dall'utente
            return undefined;
        }
        
        ,_setIconStyleAttr: function( value ) {
            this.set( "iconClass", value );
            domAttr.set( this.iconNode, "src", null );
        }

        ,_setIconSourceAttr: function( value ) {
            domAttr.set( this.iconNode, "src", value );
            this.set( "iconClass", null );
        }
        
        ,_setCaptionAttr: function( value ) {
            this.set( "label", value );
        }
        
        ,_setWidthAttr: function( value ) {
            domStyle.set( this.domNode, { width: value } );
        }
        
        ,_setHeightAttr: function( value ) {
            this._height = value;
            if( this._started ) {
                var buttonNode = query( ".dijitButtonNode", this.domNode )[0];
                var actualHeight = Math.round( domStyle.toPixelValue( buttonNode, value ) - domGeometry.getPadBorderExtents( buttonNode ).h ) ; 
                //console.log( "Button", this.get( "label" ), domStyle.toPixelValue( buttonNode, value ), domGeometry.getPadBorderExtents( buttonNode ).h );
                domStyle.set( buttonNode, { height: actualHeight + "px" } );
            
                var iconNode = query( ".dijitIcon", this.domNode )[0];
                if( domClass.contains( iconNode, "dijitNoIcon" ) ) {
                    var textNode = query( ".dijitButtonText", this.domNode )[0];
                    //TODO: rivedere lo stile e togliere la correzione webkit
                    domStyle.set( textNode, { lineHeight: actualHeight - ( has( "webkit" ) ? 2 : 1 ) + "px" });
                }
            }
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setArrowAttr: function( value ) {
            if( value == true && this.arrowNode === undefined ) {
                this.arrowNode = domConstruct.create( "span", { "class": "dijitReset dijitInline dijitArrowButtonInner" }, this.focusNode );
            } else if( ( value == false || value == null ) && this.arrowNode !== undefined ) {
                domConstruct.destroy( this.arrowNode );
                delete this.arrowNode;
            }
        }

        ,_setBorderAttr: function( value ) {
            query( ".dijitButtonNode", this.domNode ).style( value );
        }
        
        ,_setColorAttr: function( value ) {
            query( ".dijitButtonText", this.domNode ).style( "color", value );
        }

        ,_setBackgroundAttr: function( value ) {
            query( ".dijitButtonNode", this.domNode ).style( "background-color", value );
        }

        ,_setRoundingAttr: function( value ) {
            query( ".dijitButtonNode", this.domNode ).style( value );
        }
        
        ,_setShadowAttr: function( value ) {
            query( ".dijitButtonNode", this.domNode ).style( value );
        }

        ,_setWrapAttr: function( value ) {
            domClass[ value ? "add" : "remove" ]( this.domNode, "wrap" );
        }
        
        ,_setHalignAttr: function( value ) {
            domClass.removeAll( this.domNode, /halign-.*/ );
            if( value ) {
                domClass.add( this.domNode, "halign-" + value );
            }
        }
        
        ,_getHintPositionAttr: function() {
            return ['above-centered', 'below-centered' ];
        }

        ,_getHintTargetAttr: function() {
            return has( "mobile" ) ? undefined : domAttr.get( this.domNode, "widgetid" );
        }
        
        ,_getDevelopmentDescriptionAttr: function() {
            return lang.mixin( this.inherited( arguments),{
                properties: [ "caption", "width", "height", "disabled" ]
                ,events: [ "click" ] 
            });
        }
    });
    
    Button.markupFactory = _WidgetMixin.markupFactory;
    
    return Button;
});