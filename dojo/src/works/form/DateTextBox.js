define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/date/stamp"
    ,"dojo/has!mobile?./_MobileTextBox:dijit/form/DateTextBox"
    ,"./_TextMixin"
    ,"./_WidgetMixin"
], function( req, declare, lang, has, domStyle, domAttr, domClass, dateStamp, baseTextBox, _TextMixin, _WidgetMixin ){
    
    var DateTextBox = has( "mobile" )

        ? declare( declare.className( req ), [ baseTextBox ], {
            type: "date"
            ,iconClass: "date"
        })
    
        : declare( declare.className( req ), [ baseTextBox, _WidgetMixin, _TextMixin ], {
        
            getWidgetState: function() {
                var state = this.inherited( arguments );
                if( state != undefined && state.value != undefined && state.value != null ) {
                    state.value = dateStamp.toISOString( state.value, { selector: "date" } )
                }
                return state;
            }
        
            ,_setReadonlyAttr: function( value ) {
                if( value == true ) {
                    domAttr.set( this.focusNode, "readonly", "readonly" );
                    domClass.add( this.domNode, "readonly" )
                } else {
                    domAttr.remove( this.focusNode, "readonly" );
                    domClass.remove( this.domNode, "readonly" )
                }
                //this.readOnly = value;
            }
        });
    
    DateTextBox.markupFactory = _WidgetMixin.markupFactory;
    
    return DateTextBox;
});