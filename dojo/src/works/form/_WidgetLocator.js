define([
    "dojo/_base/declare"
    
], function( declare ){
    
    return declare( [], {
        findRelativeChild: function( name ) {
            var found;
            var descendants = this.getDescendants();
            for( var i = 0; i < descendants.length; i++ ) {
                var item = descendants[ i ];
                if( item.getParent() === this && ( item.get( "name" ) == name || item.get( "canonicalName" ) == name ) ) {
                    found = item;
                    break;
                }
            }
            return found;
        }
    });
});