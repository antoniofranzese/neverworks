define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dijit/form/ValidationTextBox"
    ,"./_TextMixin"
    ,"./_WidgetMixin"
], function( _require, declare, lang, TextBox, _TextMixin, _WidgetMixin ){
    
    var TextBox = declare( [ TextBox, _WidgetMixin, _TextMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,remoteFormat: false 
        
        ,handleChangeEvent: function() {
            //console.log( "Change Value", this.get( "value" ), "Last", this._lastValue );
            if( this.get( "value" ) != this._lastValue ) {
                if( this.remoteFormat ) {
                    if( this.mainForm == undefined ) {
                        this.queryMainForm( lang.hitch( this, function( form ){
                            this.mainForm = form;
                        }));
                    }
                
                    this.mainForm.sendServerCommand({
                        target: this
                        ,command: "format"
                        ,params: {
                            value: this.get( "value" )
                        }

                    }).then( lang.hitch( this, function( result ){
                        if( result.value ) {
                            this._setValueAttr( result.value, false );
                        }
                        if( this.eventActive( "changed" ) ) {
                            this.emitEvent( "changed", { state: {} } );
                            this._lastValue = this.get( "value" );
                        }

                    }));
                
                } else {
                    if( this.eventActive( "changed" ) ) {
                        this.emitEvent( "changed", { state: {} } );
                        this._lastValue = this.get( "value" );
                    }
                }
            }
        }
        
    });
    
    TextBox.markupFactory = _WidgetMixin.markupFactory;
    
    return TextBox;
});