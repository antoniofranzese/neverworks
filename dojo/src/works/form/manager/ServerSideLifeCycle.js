define([
    "dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/_base/json"
    ,"dojo/_base/unload"
	,"dojo/on"
    ,"dojo/topic"
    ,"dojo/aspect"
    ,"dojo/cookie"
    ,"dojo/promise/all"
    ,"dojo/Deferred"
    ,"dojo/errors/RequestError"
    ,"dojo/errors/RequestTimeoutError"
    ,"dojox/html/entities"
    ,"works/registry"
    ,"works/lang/strings"
    ,"works/lang/QueueManager"
    ,"works/xhr"
    ,"works/config"
    ,"works/local-storage"
    ,"works/runtime/dialogs/MessageBox"
    ,"dojo/i18n!works/runtime/errors/nls/ErrorMessages"

], function( declare, lang, array, json, unload, on, topic, aspect, cookie, all, Deferred, RequestError, RequestTimeoutError,
        htmlEntities, registry, strings, QueueManager, xhr, config, localStorage, MessageBox, errorMessages ){
    
    var TIMEOUT = config( "requestTimeout", 60000);
    var RETRY_DELAY = config( "retryDelay", 500 );
    var TTL = config( "requestTTL", 2 );
    var ROTATION_EVENT = config( "mobile.rotationEvent", false );
    
    var pollID = strings.generateHexID( 8 );

    var polling = {
        active: config( "polling.active", config( "polling", true ) )
        ,gap: config( "polling.gap", 500 )
        ,retry: config( "polling.retry", 5000 )
        ,cycle: 30000
        ,latency: 0
        ,timeout: 120000
        ,waitCap: 300000
        ,waitPenalty: 10
        ,id: pollID
        ,peers: 10
        ,subscribed: false
    }

    var keepAlive = {
        active: config( "keepAlive.active", config( "keepAlive", true ) )
        ,cycle: config( "keepAlive.cycle", 300000 )
    }
    
    return declare( [], {
        serverRoot: null
        
        ,startup: function() {
            
            this.polling = polling;
            
            if( this.isMainForm() ) {
                this.log( "Main form");
                
                this.serverQueue = new QueueManager({ name: "form-events" });
                this.parentPane = this.getParent();
                this.widgetEventRegistry = {};

                this.own(
                    on( this, "nexttick", lang.hitch( this, function( evt ){
                        
                        this.handleServerEvent({
                            source: this
                            ,sourceName: this.name
                            ,eventName: "nextTick"
                            ,state: evt[ "state" ]
                            ,wait: evt.wait
                        });
                        
                        evt.stopPropagation();
                        
                    }))
                    
                    ,this.subscribeMessage( "widget/form/query", lang.hitch( this, function( msg ) {
                        msg.callback( this );
                    }))
                    
                    ,this.subscribeMessage( "widget/event/announce", lang.hitch( this, function( msg ) {
                        this.subscribeWidgetEvent( msg );
                    }))

                    ,this.subscribeMessage( "widget/event/announce-all", lang.hitch( this, function( msg ) {
                        this.subscribeWidgetEvents( msg );
                    }))
                    
                    ,this.subscribeMessage( "widget/event/retire", lang.hitch( this, function( msg ) {
                        this.unsubscribeWidgetEvent( msg );
                    }))
                    
                    ,this.subscribeMessage( "widget/form/abandon", lang.hitch( this, function( msg ) {
                        this.unsubscribeAllWidgetEvents( msg );
                    }))

                    ,this.subscribeMessage( "widget/task/start", lang.hitch( this, function( msg ) {
                        //console.log( "TASK START:", msg.widget ? msg.widget.get( "id" ) : "No widget", "->", msg.reason || "no reason" );
                        if( this.parentPane ) {
                            this.parentPane.startTask();
                        }
                    }))

                    ,this.subscribeMessage( "widget/task/end", lang.hitch( this, function( msg ) {
                        //console.log( "TASK END:", msg.widget ? msg.widget.get( "id" ) : "No widget", "->", msg.reason || "no reason"  );
                        if( this.parentPane ) {
                            this.parentPane.endTask();
                        }
                    }))
                    
                    ,this.subscribeMessage( "widget/form/busy", lang.hitch( this, function( msg ) {
                        this.ready().enlist( msg.deferred, { reason: "Busy child" } );
                    }))
                );
                
                if( ROTATION_EVENT == true ) {
                    this.own(
                        topic.subscribe( "viewport/orientation/before", lang.hitch( this, function( params ){
                            //console.log( "Orientation ", params );
                            this.emit( "rotate", {
                                state: params
                                ,bubbles: false
                                ,widget: this
                            })
                        }))
                    )

                    this.subscribeWidgetEvent({
                        widget: this
                        ,event: "rotate"
                    });
                }
                
                if( polling.active ) {
                    
                    this.subscribeWidgetEvent({
                        widget: this
                        ,event: "dequeue"
                    });
                    
                    polling.subscribed = true;
                    
                    this.startupPolling();
                    this.own( topic.subscribe( "app/shutdown", lang.hitch( this, function() {
                        this.removeDomainPeer();
                        this.shutdownPolling();
                    })));

                }

                if( keepAlive.active ) {
                    this.ready().then( lang.hitch( this, this.startupKeepAlive ) );
                    unload.addOnUnload( lang.hitch( this, this.shutdownKeepAlive ) );
                    this.own( topic.subscribe( "app/shutdown", lang.hitch( this, function() {
                        this.shutdownKeepAlive();
                    })));
                }
                
            } else {
                this.log( "Sub form " + this.name );
            }
            
            this.inherited( arguments );
        }

        ,subscribeWidgetEvents: function( msg ) {
            array.forEach( msg.events, function( event ){
                this.subscribeWidgetEvent({
                    widget: msg.widget
                    ,event: event.name
                    ,rename: event.rename
                })
            }, this );
        }
        
        ,subscribeWidgetEvent: function( msg ) {
            var widgetName = this.getWidgetQualifiedName( msg.widget );
            if( strings.hasText( widgetName ) ) {
                var eventRegistry = this.getWidgetEventRegistry( widgetName );
                    
                if( eventRegistry[ msg.event ] != undefined ) {
                    this.log( "Replaced event", msg.event, "from", widgetName );
                    eventRegistry[ msg.event ].remove();
                }
                    
                //this.log( "Announced event", msg.event, "from", widgetName );
                var eventWidget = msg.widget;
                var eventName = msg.rename || msg.event;
                eventRegistry[ msg.event ] = on( msg.widget, msg.event + "", lang.hitch( this, function( evt ){
                    //console.log( "Catch", evt );
                    if( evt.stopped != true ) {
                        if( evt.widget === eventWidget ) {
                            var targetEvent = {
                                source: eventWidget
                                ,expr: evt[ "expr" ]
                                ,sourceName: widgetName
                                ,eventName: eventName
                                ,state: evt[ "state" ]
                                ,listener: evt[ "listener" ]
                                ,attributes: evt[ "attributes" ]
                            }

                            this.handleServerEvent( targetEvent );
                        } else {
                            console.log( "Wrong event sender", evt );
                            console.log( "Expected", eventWidget );
                            console.log( "Obtained", evt.widget );
                        }
                    } else {
                        console.log( "Stopped event " );
                    }
                    
                    if( lang.isFunction( evt.stopPropagation ) ) {
                        evt.stopPropagation();
                    }
                }));
            } else {
                this.log( "Ignoring event", msg.event, "from anonymous", msg.widget.get( "id" ) );
            }
            
        }
        
        ,unsubscribeWidgetEvent: function( msg ) {
            var widgetName = this.getWidgetQualifiedName( msg.widget );
            var eventRegistry = this.getWidgetEventRegistry( widgetName );

            if( eventRegistry[ msg.event ] != undefined ) {
                eventRegistry[ msg.event ].remove();
                delete eventRegistry[ msg.event ];
                //this.log( "Retired event", msg.event, "from", widgetName );
                
                if( Object.keys( eventRegistry ).length == 0 ) {
                    //this.log( "Removing registry for", widgetName );
                    delete this.widgetEventRegistry[ widgetName ];
                }
            }
        }
        
        ,unsubscribeAllWidgetEvents: function( msg ) {
            var widgetName = this.getWidgetQualifiedName( msg.widget );
            if( this.widgetEventRegistry[ widgetName ] != undefined ) {
                var eventRegistry = this.widgetEventRegistry[ widgetName ];
                for( var eventName in eventRegistry ) {
                    //this.log( "Abandoned event", eventName, "from", widgetName );
                    eventRegistry[ eventName ].remove();
                }
                delete this.widgetEventRegistry[ widgetName ];
            }
        }
        
        ,getWidgetEventRegistry: function( widgetName ) {
            if( this.widgetEventRegistry[ widgetName ] == undefined ) {
                this.widgetEventRegistry[ widgetName ] = {}
            }

            return this.widgetEventRegistry[ widgetName ];
            
        }
        
        ,destroy: function( ) {
            //this.log( "Shutdown" );
            if( this.isMainForm() ) {
                this.shutdownPolling();
                this.shutdownKeepAlive();
                
                if( polling.subscribed ) {
                    this.unsubscribeWidgetEvent({
                        widget: this
                        ,event: "dequeue"
                    });
                }
                
            }
            
            for( var widgetName in this.widgetEventRegistry ) {
                var eventRegistry = this.widgetEventRegistry[ widgetName ];
                
                for( var event in eventRegistry ) {
                    console.error( this.id,": Stale event", event, "from", widgetName );
                    eventRegistry[ event ].remove();
                }
                
                delete this.widgetEventRegistry[ widgetName ];
            }

            return this.inherited( arguments );
        }

        ,getEndpointRoot: function() {
            if( this.serverRoot ) {
                return this.serverRoot;
            } else {
                return window.location.protocol + "//" + window.location.host;
            }
        }

        ,getEndpointURL: function() {
            return this.getEndpointRoot() + this.endpoint; 
        }
        
        ,handleServerEvent: function( evt ) {
            //this.log( "Enqueue", evt );
            this.serverQueue.enqueue({
                //id: "evt-" + evt.eventName, 
                job: lang.hitch( this, lang.partial( this.postServerEvent, evt ) )
            });
        }
        
        ,postServerEvent: function( evt ) {
            //this.log( "Post", evt );
            if( this.endpoint ) {

                if( evt.deferred === undefined ) {
                    evt.deferred = new Deferred();
                }

                if( registry.exists( evt.source ) ) {
                    
                    if( evt.ttl === undefined ) {
                        evt.ttl = TTL;
                        this.startFormTask({ reason: "Event handler: " + evt.eventName });
                    }
                
                    evt.ttl -= 1;

                    if( evt.ttl >= 0 ) {

                        var path = evt.expr ? "." + evt.expr : "";
                        this.log( "Handling", evt.sourceName + path, "->", evt.eventName + ", TTL", evt.ttl );
                
                        if( lang.isFunction( evt.listener ) ) {
                            evt.deferred.then( function( result ) {
                                evt.listener( evt, result );
                            }, function( error ){
                                evt.listener( evt, error );
                            });
                        }
                
                        var form = this;
                    
                        try {
                            
                            if( evt.body == undefined ) {
                                evt.body = {
                                    event: {
                                        source: this.getWidgetQualifiedName( evt.source ) + path,
                                        name: evt.eventName,
                                        state: evt.state
                                    }
                                    ,state: this.gatherFormState()
                                };
                            }

                            //console.log( "BODY", evt.body );
                            
                            xhr.post({
                                url: this.getEndpointURL()
                                ,body: evt.body 
                                ,timeout: TIMEOUT
                                ,handleAs: "text"
                                ,preventCache: true
                    
                                ,headers: {
                                    "X-NWK-TTL": "" + evt.ttl
                                }
                    
                                ,load: lang.hitch( this, function( result, ioArgs ){
                                    if( evt.wait ) {
                                        this.log( "Deferring load" );
                                        evt.wait.then( lang.hitch( this, function(){
                                            this.handleEventLoad( evt, result, ioArgs );
                                        }));
                                    } else {
                                        this.handleEventLoad( evt, result, ioArgs );
                                    }
                                })
                    
                                ,error: lang.hitch( this, function( error ){
                                    if( evt.wait ) {
                                        this.log( "Deferring error" );
                                        evt.wait.then( lang.hitch( this, function(){
                                            this.handleEventError( evt, error );
                                        }));
                                    } else {
                                        this.handleEventError( evt, error );
                                    }
                                })

                            });
                        
                        } catch( ex ) {
                            console.error( "Event exception", ex );
                            this.endFormTask({ reason: "Event sender exception (" + evt.eventName + "): " + ex });
                            evt.deferred.reject({ message: "" + ex });
                        }
                
                    } else {
                        console.error( "Event TTL expired: " + evt.eventName );
                        var e = evt.error || { 
                            type: "EventTTLError" 
                            ,message: "Event TTL expired"
                        };
                    
                        this.endFormTask({ reason: "Event handler (expired): " + evt.eventName });
                        evt.deferred.reject( e );
                    
                        this.handleError( e );
                    }
                
                } else {
                    console.log( "Destroyed widget: " + evt.sourceName );
                    evt.deferred.reject({ message: "Destroyed widget: " + evt.sourceName })
                }
            
                return evt.deferred;
                
            } else {
                console.error( "Missing form endpoint" )
            }
        }
        
        ,handleEventLoad: function( evt, result, ioArgs ){
            //this.log( "Event method: " + ioArgs.xhr.getResponseHeader( "X-NWK-Method" ) );

            if( "HANDLE" == ioArgs.xhr.getResponseHeader( "X-NWK-Method" ) ) {
                this.handleResult( evt, json.fromJson( result ) );
    
                this.endFormTask({ reason: "Event handler: " + evt.eventName });
                evt.deferred.resolve( result );
    
            } else {
                this.log( "Reposting event on wrond method", evt.eventName + ", TTL", evt.ttl );
                this.postServerEvent( evt );
            }

        }
        
        ,handleEventError: function( evt, error ){
            console.log( "Event error", error );
            this.endFormTask({ reason: "Event handler (error): " + evt.eventName });
            
            var e = {
                type: error.name || "Unknown"
                ,message: error.message || "No message"
            };
            
            if( error.name == "RequestError" && evt.ttl > 0 ) {
                this.log( "Reposting event on RequestError", evt.eventName + ", TTL", evt.ttl );
                evt.error = e;
                setTimeout( lang.hitch( this, function(){
                    this.postServerEvent( evt );
                }), RETRY_DELAY );

            } else {
                evt.deferred.reject( e );
                this.handleError( e );
            }
    
        }
        
        ,handleResult: function( evt, result ) {
            if( result == null || result === undefined ) {
                this.handleError( { type: "RequestError" });

            } else {
                
                if( result.error ) {
                    this.handleError( result.error );
                }

                this.setWidgetState( result );
            }
        }
        
        ,handleError: function( error ) {
            //console.log( "Handling error: ", error );
            if( error.stack != undefined ) {
                console.error( error.stack );
            }

            MessageBox.show({ title: errorMessages.errorTitle, message: htmlEntities.encode( error.message ) });
        }
        
        ,sendServerCommand: function( cmd ) {
        //     if( this.isMainForm() ) {
        //         var d = new Deferred();
        //         this.serverQueue.enqueue({
        //             id: "cmd-" + cmd.command
        //             ,job: lang.hitch( this, function(){
        //                 return this.postServerCommand( cmd ).then( d.resolve );
        //             })
        //         });
        //         return d;
        //     } else {
        //         return this.getMainForm().sendServerCommand( cmd );
        //     }
        // }
        //
        // ,postServerCommand: function( cmd ) {
            if( this.isMainForm() ) {
                if( cmd.deferred === undefined ) {
                    cmd.deferred = new Deferred();
                }
                
                if( cmd.ttl === undefined ) {
                    cmd.ttl = TTL;
                    this.startFormTask({ reason: "Server command: " + cmd.command });
                }
                
                cmd.ttl -= 1;

                if( cmd.ttl >= 0 ) {
                    xhr.post({
                        url: this.getEndpointURL()
                        ,body: {
                            command: {
                                target: this.getWidgetQualifiedName( cmd.target )
                                ,name: cmd.command
                                ,params: cmd.params
                            }
                        }

                        ,timeout: TIMEOUT
                        ,handleAs: "text"
                        ,preventCache: true

                        ,headers: {
                            "X-NWK-TTL": "" + cmd.ttl
                        }

                        ,load: lang.hitch( this, function( rawResult, ioArgs ){
                            //this.log( "Command method: " + ioArgs.xhr.getResponseHeader( "X-NWK-Method" ) );

                            if( "HANDLE" == ioArgs.xhr.getResponseHeader( "X-NWK-Method" ) ) {
                                var result = json.fromJson( rawResult );
                                
                                if( result.error ) {
                                    this.handleError( result.error );
                                }

                                cmd.deferred.resolve( result );
                                this.endFormTask({ reason: "Server command: " + cmd.command });
                            
                            } else {
                                this.log( "Reposting command", cmd.command + ", TTL", cmd.ttl );
                                this.sendServerCommand( cmd );
                            }

                        })

                        ,error: lang.hitch( this, function( error ){

                            cmd.deferred.reject( error );
                            this.endFormTask({ reason: "Server command (error): " + cmd.command });
                            // console.log( error );

                            this.handleError({
                                type: error.name || "Unknown"
                                ,message: error.message || "No message"
                            });
                            
                        })
                        
                    });
                    
                    // .then(
                    //     lang.hitch( this, function( result ){
                    //         if( result.error ) {
                    //             this.handleError( result.error );
                    //         }
                    //
                    //         //console.log( "Fine", cmd.command );
                    //         cmd.deferred.resolve( result );
                    //         this.endFormTask({ reason: "Server command: " + cmd.command });
                    //     })
                    //
                    //     ,lang.hitch( this, function( error ){
                    //
                    //         cmd.deferred.reject( error );
                    //         this.endFormTask({ reason: "Server command (error): " + cmd.command });
                    //         // console.log( error );
                    //
                    //         this.handleError({
                    //             type: error.name || "Unknown"
                    //             ,message: error.message || "No message"
                    //         });
                    //
                    //     })
                    // );
            
                    
                } else {
                    console.error( "Command TTL expired: " + cmd.command );
                    var e = { 
                        type: "CommandTTLError" 
                        ,message: "Command TTL expired"
                    };
                    
                    this.endFormTask({ reason: "Server command (expired): " + cmd.command });
                    cmd.deferred.reject( e );
                    
                    this.handleError( e );
                    
                }

                return cmd.deferred;

            } else {
                return this.getMainForm().sendServerCommand( cmd );
            }
        }

        ,poll: function() {
            this.requestPoll();
        }
        
        ,startupPolling: function() {
            if( polling.active ) {
                polling.running = true;
                polling.schedulePoll = lang.hitch( this, this.schedulePoll );
                polling.requestPoll = lang.hitch( this, this.requestPoll );
                polling.handlePollResponse = lang.hitch( this, this.handlePollResponse );
                polling.handlePollError = lang.hitch( this, this.handlePollError );
            
                this.ready().then( lang.hitch( this, function(){
                    console.log( "Starting polling cycle with " + polling.gap + "ms gap" );
                    this.schedulePoll();
                }))
            } else {
                console.log( "Form polling is disabled" );
                polling.running = false;
            }
            delete polling.lastError;
        }
        
        ,shutdownPolling: function() {
            if( polling.lastRequest && ! polling.lastRequest.isCanceled() ) {
                polling.lastRequest.cancel();
                delete polling.lastRequest;
            }

            if( polling.active ) {
                console.log( "Shutting down polling cycle" );

                polling.running = false;
                if( polling.lastTimeout ) {
                    clearTimeout( polling.lastTimeout );
                    delete polling.lastTimeout;
                }
            }
        }

        ,schedulePoll: function() {
            if( polling.running && polling.active ) {
                polling.actualGap = Math.max( 500, polling.gap + Math.floor( Math.random() * 100 ) );
                polling.lastTimeout = setTimeout( polling.requestPoll, polling.actualGap );
            }
        }

        ,requestPoll: function() {
            delete polling.lastTimeout;

            polling.peers = Object.keys( this.refreshDomainPeers() ).length;
            if( isNaN( polling.peers ) || typeof polling.peers != "number" ) {
                polling.peers = 1;
            }
            polling.lastPoll = new Date().getTime();

            polling.lastRequest = xhr( "HEAD", {
                url: this.getEndpointURL()
                ,load: polling.handlePollResponse
                ,error: polling.handlePollError
                ,timeout: polling.timeout
                ,preventCache: true
                ,failOk: true
                ,headers: {
                    "X-NWK-Peers": "" + polling.peers
                    ,"X-NWK-Method": "POLL"
                }
                
            });
        }

        ,handlePollResponse: function( rawResult, ioArgs ) {
            polling.cycle = new Date().getTime() - polling.lastPoll;
            if( polling.lastResponse ) {
                polling.latency = new Date().getTime() - polling.lastResponse - polling.actualGap - polling.cycle;
            }
            polling.lastResponse = new Date().getTime();
            
            var error = ioArgs.xhr.getResponseHeader( "X-NWK-Error" );
            if( error ) {
                this.handlePollError( error );

            } else {

                var method = ioArgs.xhr.getResponseHeader( "X-NWK-Method" );
                if( method == "POLL" ) {
                    delete polling.lastError;

                    var active = ioArgs.xhr.getResponseHeader( "X-NWK-Polling" );
                    if( active !== undefined && active != null ) {
                        active = active.toLowerCase() != "false";
                        if( active != polling.active ) {
                            console.log( "Polling " + ( active ? "enabled" : "disabled" ) );
                        }
                        polling.active = active;
                    }
                    
                    var logParams = false;

                    var gap = ioArgs.xhr.getResponseHeader( "X-NWK-Gap" );
                    if( gap !== undefined && gap != null ) {
                        gap = parseInt( gap );
                        
                        if( !isNaN( gap ) && gap != polling.gap  ) {
                            polling.gap = gap;
                        }
                    }

                    var queue = ioArgs.xhr.getResponseHeader( "X-NWK-Queue" ).toLowerCase();
                    if( queue == "true" ) {
                        console.log( "Dequeueing" );
                    
                        this.emit( "dequeue", {
                            bubbles: false
                            ,state: {}
                            ,widget: this
                            ,listener: lang.hitch( this, function() {
                                this.schedulePoll();
                            })
                        });
                    } else {
                        this.schedulePoll();
                    }
                    
                } else {
                    this.handlePollError( "Wrong polling method: " + method );
                }
                
            }
            
            if( polling.active ) {
                console.log( "Polling: peers=" + polling.peers + ", gap=" + polling.gap + "ms, cycle=" + polling.cycle + "ms, latency=" + polling.latency + "ms" );
            }
        }
        
        ,handlePollError: function( error ) {

            if( polling.lastError == undefined ) {
                polling.lastError = new Date().getTime();
            } 

            var elapsed = new Date().getTime() - polling.lastError;
            var retry = Math.floor( polling.retry * Math.min( polling.waitPenalty, Math.min( elapsed, polling.waitCap ) / polling.waitCap * polling.waitPenalty + 1 ) );

            setTimeout( polling.schedulePoll, retry );
            
        }

        ,loadDomainPeers: function() {
            var ck = cookie( "NWK-PEERS" ) || "";
            var peers = {};
            var now = Math.floor( new Date().getTime() / 1000 );
            
            array.forEach( ck.split( "-" ), function( item ){
                if( strings.hasText( item ) ) {
                    var parts = item.split( "." );
                    if( parts.length == 2 ) {
                        var time = parseInt( parts[ 1 ] );
                        
                        if( now - time <= 120 ) {
                            peers[ parts[ 0 ] ] = time;
                        }
                        
                    }
                }
            });
            return peers;
        }
        
        ,saveDomainPeers: function( peers ) {
            
            var builder = strings.builder();
            array.forEach( Object.keys( peers ), function( key ){
                builder.append( key ).append( "." ).append( peers[ key ].toString( 10 ) ).append( "-" );
            });
            
            ck = builder.cut( 1 ).toString();
            cookie( "NWK-PEERS", ck );
        }
        
        ,refreshDomainPeers: function() {
            var peers = this.loadDomainPeers();
            peers[ polling.id ] = Math.floor( new Date().getTime() / 1000 );
            this.saveDomainPeers( peers );
            
            return peers;
        }
        
        ,removeDomainPeer: function() {
            var peers = this.loadDomainPeers();
            delete peers[ polling.id ];
            this.saveDomainPeers( peers );
        }

        ,startupKeepAlive: function() {
            this.shutdownKeepAlive();
            if( this.endpoint ) {
                if( polling.active ) {
                    keepAlive.nextTimeout = setTimeout( lang.hitch( this, this.startupKeepAlive ), keepAlive.cycle );

                } else {
                    xhr( "HEAD", {
                        url: this.getEndpointURL() 
                        ,timeout: 60000
                        ,headers: {
                            "X-NWK-Method": "KEEP"
                        }
                        ,preventCache: true
                        ,failOk: true
                        ,load: lang.hitch( this, function( rawResult, ioArgs ) {
                            var method = ioArgs.xhr.getResponseHeader( "X-NWK-Method" );
                            if( method == "KEEP" ) {
                                var next = ioArgs.xhr.getResponseHeader( "X-NWK-Next" );

                                if( next !== undefined && next != null ) {
                                    next = parseInt( next );
                                    
                                    if( isNaN( next ) ) {
                                        next = keepAlive.cycle;
                                    }
                                    
                                    if( next > 10000 ) {
                                        keepAlive.nextTimeout = setTimeout( lang.hitch( this, this.startupKeepAlive ), next );
                                    } else {
                                        console.log( "Keep-alive disabled" );
                                    }
                                }
                            }
                        })
                    });
                }
            }
        }

        ,shutdownKeepAlive: function() {
            if( keepAlive.nextTimeout ) {
                clearTimeout( keepAlive.nextTimeout );
                delete keepAlive.nextTimeout;
            }
        }
        
    });
    
});
