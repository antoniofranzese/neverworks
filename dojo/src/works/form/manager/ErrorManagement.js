define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
], function( _require, declare, lang, array ){
    
    return declare( [], {
        
        startup: function() {
            this.inherited( arguments );
            this.errorHandlersRegistry = {};
        }
        
        ,registerErrorHandler: function( type, handler ) {
            if( handler ) {
                if( lang.isFunction( handler.handleError ) ) {
                    this.errorHandlersRegistry[ type ] = handler;   
                } else {
                    throw new Error( "Invalid handler, missing errorHandler( error ) implementation: " + handler );
                }
            } else {
                throw new Error( "Missing handler" );
            }
        }
        
        ,handleError: function( error ) {
            error = error || {};
            var type = error.type || "UnknownError";
            if( type in this.errorHandlersRegistry ) {
                this.errorHandlersRegistry[ type ].handleError( error );
            } else {
                this.inherited( arguments );
            }
        }
        
        ,inheritForm: function( form ) {
            this.inherited( arguments );
            if( form.errorHandlersRegistry !== undefined && Object.keys( form.errorHandlersRegistry ).length > 0  ) {
                array.forEach( Object.keys( form.errorHandlersRegistry ), function( type ){
                    if( !( type in this.errorHandlersRegistry ) ) {
                        this.errorHandlersRegistry[ type ] = form.errorHandlersRegistry[ type ];
                    }
                }, this );
            }
        }
        
    });
    
});