define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/dom-geometry"
    ,"dijit/layout/_LayoutWidget"
    ,"works/layout/ContentPane"
    ,"works/form/_WidgetMixin"
    ,"works/form/_SupportingWidget"
    
], function( _require, declare, lang, on, topic, domGeometry, _LayoutWidget, ContentPane, _WidgetMixin, _SupportingWidget ){
    
    return declare( [ ContentPane, _SupportingWidget ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,title: ""
        
        ,startup: function() {
            this.inherited( arguments );
            this.own( on( this, "loaded", lang.hitch( this, this.registerChildForm ) ) );
            this.registerChildForm();
            if( this.childForm && this.childForm.isMainForm() ) {
                topic.subscribe( "works/widget/change", lang.hitch( this, function(){
                    this.widgetChangeReceived = true; 
                }));
            }
        }

        ,registerChildForm: function() {
            this.childForm = this.getDescendants()[0];
        }
        
        ,findRelativeChild: function( name ) {
            if( this.childForm ) {
                return this.childForm.findRelativeChild( name );
            } else {
                return undefined;
            }
        }
        
        ,_getNameAttr: function() {
            if( this.childForm ) {
                return this.childForm.get( "name" );
            } else {
                return undefined;
            }
            
        }

        ,_getCanonicalNameAttr: function() {
            if( this.childForm ) {
                return this.childForm.get( "canonicalName" );
            } else {
                return undefined;
            }
            
        }
        ,widget: function( name ) {
            if( this.childForm ) {
                return this.childForm.widget( name );
            } else {
                return undefined;
            }
        }
        ,getActualForm: function() {
            if( this.childForm ) {
                return this.childForm;
            } else {
                return undefined;
            }
        }
        
        ,startTask: function() {
            if( this.childForm === undefined ) {
                this.registerChildForm();
            }
            
            if( ! this.taskCount ) {
                this.taskCount = 0;
                //console.log( "START", this.childForm.id );
                this.emit( "task-start", { bubbles: false } );
                topic.publish( "form/task/start" );
                
            }
            topic.publish( "network/request" );
            this.taskCount++;
            //console.log( "COUNT+", this.taskCount );
        }
        
        ,endTask: function() {
            this.taskCount--;
            //console.log( "COUNT-", this.taskCount );
            topic.publish( "network/response" );

            if( this.taskCount == 0 ) {
                delete this.taskCount;
                //console.log( "STOP", this.childForm.id );
                this.emit( "task-end", { bubbles: false } );
                topic.publish( "form/task/end" );
                
                if( this.widgetChangeReceived ) {
                    //console.log( "GLOBAL RESIZE", this.childForm.id );
                    delete this.widgetChangeReceived;
                    // console.time( "global_resize" );
                    this.childForm.resize();
                    topic.publish( "form/resize/global" );
                    // console.timeEnd( "global_resize" );
                }
            }
        }
        
        ,setContentSize: function() {
            if( this.contentHeight != undefined ) {
                this.domNode.style.height = this.contentHeight;
            } else {
                this.domNode.style.height = null;
            }
            if( this.contentWidth != undefined ) {
                this.domNode.style.width = this.contentWidth;
            } else {
                this.domNode.style.width = null;
            }
        }
        
        ,resize: function( size ) {
            if( this.getParent() && ( this.getParent().isInstanceOf( _LayoutWidget ) || ( this.getParent().getParent() && this.getParent().getParent().isInstanceOf( _LayoutWidget ) ) ) ) {
                if( size !== undefined ) {
                    this.inherited( arguments );
                    domGeometry.setMarginBox( this.domNode, size );
                }
            } else {
                this.inherited( arguments );
            }
        }
    });
    
})
