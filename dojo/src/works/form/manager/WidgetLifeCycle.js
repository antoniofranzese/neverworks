define([
    "dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
	,"dojo/_base/kernel"
    ,"dojo/topic"
    ,"dojo/parser"
    ,"dojo/dom-construct"
    ,"dojo/Deferred"
    ,"dojo/promise/all"
    ,"works/registry"
    ,"works/config"
    ,"works/layout/ContentPane"
    ,"../_FocusingWidget"

], function( declare, array, lang, kernel, topic, parser, domConstruct, Deferred, all, registry, config, ContentPane, _FocusingWidget ) {
    
    var TIMING = config( "timing", false );
    
    return declare( [], {
        
        startup: function() {
            this.inherited( arguments );
            
            this.pendingCreations = {};
            
            this.startupDeferred = new Deferred();
            this.ready().enlist( this.startupDeferred, { reason: "Startup" } );

            this.appDeferred = new Deferred();
            if( TIMING ) console.time( this.id + "[ready]" );
            this.ready().then( lang.hitch( this, function() {
                this.log( "Form ready" );
                if( TIMING ) console.timeEnd( this.id + "[ready]" );
                if( this.isMainForm() ) {
                    // console.log( "Resizing", this.id );
                    this.resize();
                }
                this.appDeferred.resolve();
                if( this.startupNextTick ) {
                    delete this.startupNextTick;
                    this.log( "Next tick on startup" );
                    setTimeout( lang.hitch( this, this.scheduleNextTick ), 0 );
                }
            }))

            // Main Form startup
            if( this.isMainForm() ) {
                if( kernel.global[ "form/app/startup/spent" ] === undefined ) {
                    //this.log( "Subscribing page startup" );
                    topic.subscribe( "app/startup", lang.hitch( this, function(){
                        topic.publish( "app/wait", this.appDeferred );
                        this.startupForm();
                        kernel.global[ "form/app/startup/spent" ] = true;
                    }));

                } else {
                    this.log( "Immediate startup" );
                    this.startupForm();
                }
                
                topic.subscribe( "widget/resize", lang.hitch( this, function(){
                    //console.log( "Widget resize" );
                    this.resize();
                }));
            }

        }
        
        ,startupForm: function(){
            this.notifyMessage( { topic: "form/startup", body: { form: this } } );
            if( this.activeChild != undefined  ) {
                setTimeout( lang.hitch( this, function(){
                    this.focusChild( this.activeChild );
                    delete this.activeChild;
                }), 0 );
            }
            this.startupDeferred.resolve();
        }
        
        ,joinForm: function( form ) {
            // Child Form startup;
            this.log( "Child startup" );
            this.startFormTask( { reason: "Child form startup" } );
            this.ready().then( lang.hitch( this, function(){
                this.endFormTask( { reason: "Child form startup" } );
            }));
            this.registerWidgets();
            this.startupForm();
        }
        
        ,leaveForm: function( form ) {
            this.log( "Child shutdown" );
            this.unregisterWidgets();
        }
        
        ,focusChild: function( child ) {
            var active = this.getActiveChild();
            //console.log( "Focusing", child, "Blurring", active ? active.get( "name" ) : "none" );
            this.blurChild( active );
            
            child = typeof child == "string" ? this.widget( child ) : child;
            
            if( child != undefined ) {
                if( child.isInstanceOf( _FocusingWidget ) ) {
                    child.acquireFocus();
                } else if( lang.isFunction( child.focus ) ) {
                    child.focus();
                }
            }
        }

        ,blurChild: function( child ) {
            child = typeof child == "string" ? this.widget( child ) : child;
            
            if( child != undefined ) {
                if( child.isInstanceOf( _FocusingWidget ) ) {
                    child.resignFocus();
                } else if( lang.isFunction( child.blur ) ) {
                    child.blur();
                }
            }
        }
        
        ,getActiveChild: function() {
            //TODO: aggiungere una verifica di contenimento
            var widget = registry.getEnclosingWidget( document.activeElement );
            if( widget != undefined && this.isWidgetContained( widget ) ) {
                return widget;
            } else {
                return undefined;
            }
        }
        
        ,setActiveChild: function( child ) {
            if( child ) {
                this.focusChild( child );
            } else {
                this.blurChild( this.getActiveChild() );
            }
        }
        
        ,gatherFormState: function() {
        	return this.inspectWidgets( function( widget, name ){
            	return widget.getWidgetState ? widget.getWidgetState() : undefined;
            }) || {};
        }
        
        
        ,setFormState: function( state ) {
            array.forEach( Object.keys( state ), function( name ){

                if( "$create" in state[ name ] ) {
                    this.performWidgetCreation( name, state[ name ][ "$create" ] );

                } else if( "$replace" in state[ name ] ) {
                    this.log( "Replacing", name );
                    this.performWidgetRemoval( name ).then( lang.hitch( this, function(){
                        this.performWidgetCreation( name, state[ name ][ "$replace" ] );
                    }));
                    
                } else if( "$destroy" in state[ name ] ) {
                    this.performWidgetRemoval( name );
                    
                } else {
                    var widget = this.widget( name );
                    if( widget ) {
                        widget.setWidgetState( state[ name ] );
                    }

                }
            
            }, this );
        }
        
        ,performWidgetCreation: function( name, content ) {
            if( this.widget( name ) != undefined ) {
                throw Error( "Cannot create already existing widget (" + operation + "): " + name );
            }

            this.log( "Creating", name );
            this.pendingCreations[ name ] = new Deferred();
            
            var params = { reason: "Widget creation" }
            var d = this.ready().enlist( new Deferred(), params );
            this.bubbleMessage( { topic: "widget/form/busy", body: { deferred: d } });
            this.startFormTask( params );

            this.createWidget( content ).then( lang.hitch( this, function(){
                this.endFormTask( params );
                this.log( "Created", name );
                this.pendingCreations[ name ].resolve();
                delete this.pendingCreations[ name ];
                d.resolve();
            }));

            return d;
        }
        
        ,performWidgetRemoval: function( name ) {
            var d = new Deferred();
            
            if( name in this.pendingCreations ) {
                this.log( "Deferring removal of", name );

                this.pendingCreations[ name ].then( lang.hitch( this, function(){
                    this.log( "Deferred removing", name );
                    this.destroyWidget( this.widget( name ) ).then( function(){
                        d.resolve();
                    });
                }))
            } else {
                if( this.widget( name ) == undefined ) {
                    throw Error( "Cannot delete missing widget: " + name );
                }
                this.log( "Removing", name );
                this.destroyWidget( this.widget( name ) ).then( function(){
                    d.resolve();
                });
            }
            
            return d;
        }
        
        ,createWidget: function( html ) {
            var d = new Deferred();
            var node = domConstruct.toDom( "<div>" + html + "</div>" );
            domConstruct.place( node, this.domNode );
            var widgets = parser.parse({ rootNode: node }).then( lang.hitch( this, function( widgets ){
                //console.log( "Parsed ", widgets );
                //var widget = registry.byNode( node.firstChild );
                var widget = widgets[ 0 ];
            
                //TODO: ricevere un'indicazione di posizionamento
                domConstruct.place( node.firstChild, this.domNode );
                this.registerWidget( widget );
                domConstruct.destroy( node );
                d.resolve();
            }));
            
            return d;
        }
        
        ,destroyWidget: function( widget ) {
            var d = new Deferred();
            
            this.unregisterWidget( widget );
            if( lang.isFunction( widget[ "destroy" ] ) ) {
                widget.destroy();
            } else if( lang.isFunction( widget[ "destroyRecursive" ] ) ) {
                widget.destroyRecursive();
            }
            this.removeChild( widget );
            d.resolve();
            
            return d;
            
        }
        
        ,redrawForm: function( state ) {
            var parent = this.getParent();
            var self = this;
            if( parent != undefined && parent.isInstanceOf( ContentPane ) ) {
                this.unregisterWidgets();
                this.log( "Form redraw requested, forwarding to parent" );

                this.startFormTask({ reason: "Form redraw" });
                setTimeout( function(){
                    parent.load( { content: state.content }).then( function(){
                        delete state.content;
                        var redrawnForm = parent.getDescendants()[0];
                        redrawnForm.ready().then( function(){
                            redrawnForm.log( "Redraw completed" );
                            redrawnForm.inheritForm( self );
                            redrawnForm.setWidgetState( state );
                            redrawnForm.endFormTask({ reason: "Form redraw" });
                        });
                    });
                }, 0 );
                
            } else {
                console.error( "Form redraw requested, but parent is not a ContentPane"  );
                
            }
        }
        
        ,inheritForm: function( form ) {

        }
        
        ,scheduleNextTick: function() {
            this.startFormTask({ reason: "Next tick schedule" });

            var d = new Deferred();
            this.contentReady().then( lang.hitch( this, function(){
                this.log( "Content ready on next tick" );
                d.resolve();
            }));
            
            this.ready().then( lang.hitch( this, function(){
                this.log( "Next tick" );
                this.emit( "nexttick", { widget: this, state: {}, bubbles: false, wait: d } );
                this.endFormTask({ reason: "Next tick schedule" });
            }));

        }
        
        ,getWidgetState: function() {
            return {
                state: this.gatherFormState()
            }
        }
        
        ,setWidgetState: function( value ) {
            //this.log(  "Setting state", value );

            if( value.content ) {
                this.redrawForm( value );
            } else {

                if( value.state !== undefined ) {
                    this.setFormState( value.state );
                }

                if( value.title !== undefined ) {
                    this.set( "title", value.title );
                }

                if( value.loader !== undefined ) {
                    this.set( "loader", value.loader );
                }

                if( value.visible !== undefined ) {
                    this.set( "visible", value.visible );
                }
                
                if( value.activeChild !== undefined ) {
                    this.setActiveChild( value.activeChild );
                }
            
                if( value.nextTick == true ) {
                    this.scheduleNextTick();
                }
            }
        }

    });

});
