define([
    "dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/promise/all"
    ,"dojo/query"
    ,"dojo/aspect"
    ,"dojo/Deferred"
    ,"works/lang/strings"
    ,"works/registry"
    ,"dijit/form/_FormWidget"
    ,"works/form/_FormManagerWidget"
    ,"works/form/_SupportingWidget"
    ,"works/form/_WidgetLocator"
    ,"dijit/_DialogMixin"
    
], function( declare, array, lang, all, query, aspect, Deferred, strings, registry, _FormWidget, _FormManagerWidget, _SupportingWidget, _WidgetLocator, _DialogMixin ) {
    
    var WidgetRegistry = declare( [ _WidgetLocator ], {
        
        startup: function() {
            this.inherited( arguments );

            //this.log( "Registry", this.isMainForm() );
            this.formWidgets = {};
            if( this.isMainForm() ) {
                this.registerWidgets();
            }
        }
        
        ,registerWidgets: function() {
            //this.log( "Registering widgets" );
            
            if( ! this.isLoaded ) {
                //this.log( "Deferring registration" );
                this.startFormTask({ reason: "Deferred registration" });
                this.registrationDeferred = new Deferred();
                this.ready().enlist( this.registrationDeferred, { reason: "Deferred registration" } );
                this.registrationTrigger = aspect.after( this, "_onLoadHandler", lang.hitch( this, this.registerWidgets ) )
                
            } else {

                var closeTask = false;
                if( this.registrationTrigger ) {
                    //this.log( "Recovering registration" );
                    this.registrationTrigger.remove();
                    delete this.registrationDeferral;
                    closeTask = true;
                } else {
                    //this.log( "Performing registration" );
                }

                array.forEach( this.getDescendants(), function( widget ){
                    //this.log( "try", widget.get( "name" ), widget.declaredClass );
                    this.registerWidget( widget );
                }, this );
                
                if( closeTask ) {
                    this.endFormTask({ reason: "Deferred registration" });
                    this.registrationDeferred.resolve();
                    delete this.registrationDeferred;
                }
            }
        }
        
        ,registerWidget: function( widget ) {
            //console.log( "Register", widget.get( "name" ), widget.declaredClass );
            //this.log( widget.get( "name" ), this.approveWidget( widget ) );

            var approved = widget.isInstanceOf( _FormWidget )
                && ! widget.isInstanceOf( _SupportingWidget )
                && this.getWidgetParentForm( widget ) === this
                && strings.hasText( widget.get( "name" ) );

            if( approved ) {
                var name = widget.get( "name" );
                if( !( name in this.formWidgets ) ){
                    this.formWidgets[ name ] = { widget: widget };

                    if( lang.isFunction( widget[ "joinForm" ] ) ) {
                        //this.log( "join", widget.get( "name" ) );
                        var waitForWidget = lang.isFunction( widget.ready );
                        if( waitForWidget ) {
                            var d = new Deferred();
                            this.ready().enlist( d, { reason: "Child widget ready" } );
                        }

                        widget.joinForm( this );

                        if( waitForWidget ) {
                            widget.ready().then( lang.hitch( d, d.resolve ) );
                        }
                    }
                    
                } else if( this.formWidgets[ name ].widget !== widget ){
                    throw new Error( "Cannot register, duplicate widget name: " + name );
                }
            }
        }
        
        ,unregisterWidgets: function() {
            for( var name in this.formWidgets ) {
                this.unregisterWidget( this.formWidgets[ name ].widget );
            }
        }
        
        ,unregisterWidget: function( widget ) {
            //console.log( "Unregistering", widget, this.containsWidget( widget ) );
            if( this.containsWidget( widget ) ) {
                var name = widget.get( "name" );
                if( name in this.formWidgets ) {
                    if( this.formWidgets[ name ].widget === widget ) {
                        if( lang.isFunction( widget[ "leaveForm" ] ) ) {
                            widget.leaveForm( this );
                        }
                    
                        delete this.formWidgets[ name ];
                    } else {
                        throw new Error( "Cannot unregister, another widget exists under the same name: " + name );
                    }
                } else {
                    throw new Error( "Cannot unregister, widget is not registered: " + name );
                }
            } 
        }
        
        ,containsWidget: function( widget ) {
            return strings.hasText( widget.get( "name" ) ) 
                && this.formWidgets[ widget.get( "name" ) ] != undefined
                && this.formWidgets[ widget.get( "name" ) ].widget === widget;
        }
        
        ,findRelativeChild: function( name ) {
            var found;
            if( name in this.formWidgets ) {
                found = this.formWidgets[ name ].widget;
            }
            if( found == undefined ) {
                var descendants = this.getDescendants();
                for( var i = 0; i < descendants.length; i++ ) {
                    var item = descendants[ i ];
                    if( item.getParent() === this && ( item.get( "name" ) == name || item.get( "canonicalName" ) == name ) ) {
                        found = item;
                        break;
                    }
                }
            }
            return found;
        }
        
        ,widget: function( path ) {
            var names = path.split( "." );
            var widget = this;
            for( var i = 0; i < names.length; i++ ) {
                if( widget.isInstanceOf( _WidgetLocator ) ) {
                    widget = widget.findRelativeChild( names[ i ] );
                } else {
                    throw new Error( "Cannot locate relative child, " + widget.get( "declaredClass" ) + " is not a _WidgetLocator"  );
                }
                if( widget == undefined ) {
                    return undefined;
                }
            }
            return widget;

            
            // var index = name.indexOf( "." );
            //
            // if( index > 0 ) {
            //     var first = name.substring( 0, index );
            //     if( first in this.formWidgets ){
            //         return this.widget( first ).widget( name.substring( index + 1 ) );
            //     } else {
            //         return undefined;
            //     }
            // } else if( name in this.formWidgets ) {
            //     return this.formWidgets[ name ].widget;
            // } else {
            //     return undefined;
            // }
        }
        
        ,inspectWidgets: function( callback ) {
            var result = {};
            for( var name in this.formWidgets ) {
                result[ name ] = callback( this.formWidgets[ name ].widget, name );
            };
            return result;
            
        }
        
        ,getWidgets: function() {
            var result = [];
            for( var name in this.formWidgets ) {
                result.push( this.formWidgets[ name ].widget );
            };
            return result;
        }
        
        ,getWidgetParentForm: function( widget ) {
            widget = widget || this;
            // TODO: cerca prima un formManager, poi una dialog e gli chiede il parent
            if( widget[ "$parentform" ] !== undefined ) {
                return widget[ "$parentform" ];
            } else if( registry.getMainNode( widget ) ){
                return registry.findParent( widget, _FormManagerWidget );
            } else {
                return undefined;
            }
        }
        
        
        ,getWidgetParentForms: function( widget ) {
            widget = widget || this;
            var forms = [];
            
            do {
                forms = forms.concat( registry.findParents( widget, _FormManagerWidget ) );
                //console.log( "forms for", widget.id, registry.findParents( widget, _FormManagerWidget ) );
                if( forms.length > 0 ) {
                    var lastForm = forms[ forms.length - 1 ];
                    if( lastForm[ "$parentform" ] != undefined ) {
                        widget = lastForm[ "$parentform" ];
                        forms.push( widget );
                    } else {
                        widget = undefined;
                    }
                } else {
                    if( widget[ "$parentform" ] != undefined ) {
                        widget = widget[ "$parentform" ];
                        forms.push( widget );
                    } else {
                        widget = undefined
                    }
                }
                
            } while( widget != undefined );  
            
            return forms;
        }
        
        ,getWidgetQualifiedName: function( widget ) {
            if( widget.isInstanceOf( _FormManagerWidget ) && widget.isMainForm() ) {
                return "/";
            } else {
                var qName = widget[ ".qName" ];
                if( qName === undefined ) {
                    var widgetName = widget.get( "name" );
                    if( widgetName.hasText() ) {
                        var forms = this.getWidgetParentForms( widget );
                        if( forms.length == 0 ) {
                            throw new Error( "Orphan widget: " + widget );
                        } else if( forms.length == 1 ) {
                            qName = widget.get( "name" );
                        } else {
                            var names = array.map( forms.slice( 0, -1 ).reverse(), function( item ){ return item.get( "name" ) } );
                            names.push( widget.get( "name" ) );
                            qName = names.join( "." );
                        }
                    }
                    widget[ ".qName" ] = qName;
                }
                return qName;
            }
        }
        
        ,isMainForm: function() {
            //TODO: prevedere un parametro rootForm da popolarsi con il produttore
            //e lasciare la deduzione solo in mancanza del parametro
            if( this[ "_isMainForm" ] === undefined ) {
                // this.log( "parents", this.getWidgetParentForms( this ) );
                // var node = this.domNode;
                // do {
                //     console.log( ">", node );
                //     node = node.parentNode;
                // } while( node );
                
                if( this.getWidgetParentForm( this ) === undefined && registry.findParent( this, _DialogMixin ) === undefined ) {
                    this._isMainForm = true;
                } else {
                    this._isMainForm = false;
                }
            }
            return this._isMainForm;
        }
        

        ,getMainForm: function() {
            //TODO: prevedere un parametro rootForm da popolarsi con il produttore
            // e lasciare la ricerca solo in mancanza del parametro
            if( this.isMainForm() ) {
                return this;
            } else {
                var parents = this.getWidgetParentForms( this );
                return parents[ parents.length - 1 ];
            }
        }
        
        ,isWidgetContained: function( widget ) {
            var forms = this.getWidgetParentForms( widget );
            if( forms.length > 0 && forms.indexOf( this ) >= 0 ) {
                return true;
            } else {
                return false;
            }
        }

        ,isWidgetAnonymous: function( widget ) {
            var name = widget.get( "name" );
            if( name.indexOf( "@@" ) > 0 || !( name.hasText() ) ) {
                return true;
            } else {
                return false;
            }
            
        }
        
        ,getWidgetHierarchyToForm: function( widget, form ) {
            var names = [];
            while( widget !== form ) {
                if( this.isWidgetAnonymous( widget ) ) {
                    if( widget.get( "canonicalName" ) ) {
                        names.push( widget.get( "canonicalName" ) )
                    }
                    widget = widget.getParent();
                } else {
                    names.push( widget.get( "name" ) );
                    break;
                }
            }
            
            return names;
            
        }
        
        ,getWidgetUniquePath: function( widget ) {
            if( typeof widget == "string" ) {
                widget = this.widget( widget );
            }
            var widgets = this.getWidgetParentForms( widget );
            widgets.unshift( widget );
            var path = [];
            for( var i = 0; i < widgets.length - 1; i++ ) {
                var widget = widgets[ i ];
                if( this.isWidgetAnonymous( widget ) ) {
                    var names = this.getWidgetHierarchyToForm( widget, widgets[ i + 1 ] );
                    for( var n = 0; n < names.length; n++ ) {
                        path.push( names[ n ] );
                    }
                } else {
                    path.push( widget.get( "name" ) );
                }
            }
            return path.reverse().join( "." );
        }

    });

    return WidgetRegistry;
})
