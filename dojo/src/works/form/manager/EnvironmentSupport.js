define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/query"
    ,"works/lang/Expando"
    ,"works/layout/ContentPane"
    ], function( _require, declare, query, Expando, ContentPane ){
    
    return declare( [], {
        
        _setTitleAttr: function( value ) {
            this.ready().then( lang.hitch( this, function(){
                if( this.isMainForm() ) {
                    query( "title" )[ 0 ].innerHTML = value;
                } else {
                    var parent = this.getParent();
            
                    if( parent != undefined && parent.isInstanceOf( ContentPane ) ) {
                        parent.set( "title", value );
                    }
                }
                this.title = value;
            }));
        }
        
        ,_setLoaderAttr: function( value ) {
            this.get( "wrapper" ).set( "loader", value );
        }
        
        ,_getVisibleAttr: function() {
            return this.get( "wrapper" ).get( "visible" );
        }

        ,_setVisibleAttr: function( value ) {
            this.get( "wrapper" ).set( "visible", value );
        }
        
        ,_getWrapperAttr: function() {
            var parent = this.getParent();
    
            if( parent != undefined && parent.isInstanceOf( ContentPane ) ) {
                return parent;
            } else {
                if( this._readyProperties === undefined ) {
                    this._readyProperties = new Expando();
                    this.ready().then( lang.hitch( this, function(){
                        this._readyProperties.forEach( function( name, value ){
                            this.set( name, value );
                        }, this );
                        delete this._readyProperties;
                    }));
                }
                return this._readyProperties;
            }
        }
        
    });
    
});