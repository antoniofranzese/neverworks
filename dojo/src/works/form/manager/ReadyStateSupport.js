define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/topic"
    ,"dojo/promise/all"
    ,"dojo/Deferred"
    ,"works/_LogMixin"
    ,"works/_WidgetUtils"

], function( _require, declare, lang, array, topic, all, Deferred, _LogMixin, _WidgetUtils ){
    
    var logging = false;
    
    var ReadyState = declare([ _LogMixin ],{

        declaredClass: _WidgetUtils.inferDeclaredClass( _require ) + ".ReadyState"
        
        ,constructor: function( name, form ) {
            this.name = name;
            this.form = form;
            this.deferred = new Deferred();
            this.enlisteds = [];
            this.children = [];
            this.iterations = 50;
            this.resolving = false;
        }
        
        ,enlist: function( deferred, params ) {
            if( this.deferred.isResolved() ) {
                this.deferred = new Deferred();
            	if( logging ) this.log( "New deferred for " + this.name + ", " + ( this.deferred.isResolved() ? "resolved" : "unresolved" ));
            }

            if( deferred != undefined ) {
                if( deferred instanceof ReadyState ) {
                    if( logging ) this.log( "Enlisting child " + deferred.name + " into " + this.name + ( params && params.reason ? ": " + params.reason : "" ) );
                    this.enlisteds.push( deferred.deferred );
                    this.children.push( deferred );

                } else {
                    if( logging ) this.log( "Enlisting deferred into " + this.name + ( params && params.reason ? ": " + params.reason : "" ) );
                    this.enlisteds.push( deferred );
                }
            }
            this.resolve();
            return deferred;
        }
        
        ,then: function( callback ) {
        	if( logging ) this.log( "Subscribing to " + this.name + ", " + ( this.deferred.isResolved() ? "resolved" : "unresolved" ) );
            this.deferred.then( callback );
        }
        
        ,resolve: function( out ) {
            if( !( this.resolving ) ) {
            	if( logging ) this.log( "Resolving " + this.name );

                this.resolving = true;
                this.iterations = 50;

                this.deferred.then( lang.hitch( this, function(){
                	if( logging ) this.log( this.name + " resolved" );
                    this.resolving = false;
                }));
                
                this._resolve( out );
            }
        }
        
        ,_resolve: function( out ) {
            if( this.enlisteds.length == 0 ) {
                if( logging ) this.log( "No deferreds, direct resolve for " + this.name );
                this.deferred.resolve( out );

            } else {
            	var enlisteds = this.enlisteds;
            	var children = this.children;
            	this.enlisteds = [];
            	this.children = [];

                array.forEach( children, function( child ){
                    if( logging ) this.log( "Resolving " + child.name + " child for " + this.name );
                    child.resolve( out );
                });
                if( logging ) this.log( "Join point for " + this.name );

                all( enlisteds ).then( lang.hitch( this, function(){
                	this.iterations -= 1;
                	if( logging ) this.log( "After join point for " + this.name );

                	if( this.enlisteds.length == 0 ) {
                        if( logging ) this.log( "Final resolve for " + this.name );
                        this.deferred.resolve( out );

                	} else {
                		if( this.iterations > 0 ) {
                    		if( logging ) this.log( "New iteration, " + this.iterations + " remaining iterations for " + this.name );
                    		this._resolve( out );

                		} else {
                			console.error( "No more iterations allowed for " + this.name );
                			this.deferred.resolve( out );
                		}
                	}
                }));
            }
            
        }
        
        ,log: function( msg ) {
            //console.log( "ReadyState " + this.form.id + ": " + msg );
        } 
        
    });
    
    var ReadyStateSupport = declare( [], {
        
        declaredClass: _WidgetUtils.inferDeclaredClass( _require )
        
        ,ready: function( name, parent ) {
            if( this._readyStates == undefined ) {
                this._readyStates = {} 
            }

            name = name || "<MAIN>";
            parent = parent || "<MAIN>";
            
            if( !( name in this._readyStates ) ) {
                this._readyStates[ name ] = new ReadyState( name, this );
                if( name != parent ) {
                    this.ready( parent ).enlist( this._readyStates[ name ], { reason: name + " child ready state" } )
                }
            }
            
            return this._readyStates[ name ];
        }

        ,startup: function() {
            if( logging ) this.log( "Starting ready state management" );
            var d = this.ready().enlist( new Deferred(), { reason: "Ready state management" } );
            this.inherited( arguments );
            topic.publish( "form/startup", this );
            setTimeout( lang.hitch( this, function(){
                d.resolve();
                //this.ready().resolve( this );
            }), 0 );
        }
        
    });
    
    ReadyStateSupport.ReadyState = ReadyState;
    
    return ReadyStateSupport;
})