define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/query"
    ,"works/layout/ContentPane"
    ], function( _require, declare, query, ContentPane ){
    
    return declare( [], {
        
        startFormTask: function( params ) {
            params = params || {};
            if( params.widget == undefined ) {
                params.widget = this;
            }
            if( this.isMainForm() ) {
                this.notifyMessage( { topic: "widget/task/start", body: params } );
            } else {
                this.bubbleMessage( { topic: "widget/task/start", body: params } );
            }
        }

        ,endFormTask: function( params ) {
            params = params || {};
            if( params.widget == undefined ) {
                params.widget = this;
            }
            if( this.isMainForm() ) {
                this.notifyMessage( { topic: "widget/task/end", body: params } );
            } else {
                this.bubbleMessage( { topic: "widget/task/end", body: params } );
            }
        }
        
    });
    
});