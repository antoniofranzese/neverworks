define([
    "dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"

], function( declare, array, lang ) {
    
    return declare( [], {
        
        constructor: function() {
            this.messageObserverRegistry = {};
        }
        
        ,bubbleMessage: function( message ) {
            var form = this.getWidgetParentForm( this );
            //this.log( "Message bubble", message, form );
            if( form !== undefined ) {
                form.bubbleMessage( message );
            } else {
                if( message.topic ) {
                    this.notifyMessage( message );
                } else {
                    throw new Error( "Malformed message: " + message );
                }
            }
        }
        
        ,notifyMessage: function( message ) {
            if( message.topic in this.messageObserverRegistry ) {
                array.forEach( this.messageObserverRegistry[ message.topic ], function( callback ){
                    callback( message.body );
                }); 
            }
        }
        
        ,spreadMessage: function( message ) {
            this.notifyMessage( message );

            array.forEach( this.getWidgets(), function( widget ){
                if( lang.isFunction( widget.spreadMessage ) ) {
                    widget.spreadMessage( message );
                }
            });
        }
        
        ,subscribeMessage: function( messageTopic, callback ) {
            if( !( messageTopic in this.messageObserverRegistry ) ) {
                this.messageObserverRegistry[ messageTopic ] = []
            }

            var observers = this.messageObserverRegistry[ messageTopic ];
            observers.push( callback );

            return {
                remove: lang.hitch( this, function() {
                    this.unsubscribeMessage( messageTopic, callback );
                })
            }
        }
        
        ,unsubscribeMessage: function( messageTopic, callback ) {
            if( messageTopic in this.messageObserverRegistry ) {
                this.messageObserverRegistry[ messageTopic ] = array.filter( 
                    this.messageObserverRegistry[ messageTopic ]
                    ,function( cb ) {
                        return cb !== callback;
                    }
                , this );
            }
        }
        
    });

})
