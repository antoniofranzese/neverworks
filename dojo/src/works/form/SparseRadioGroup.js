define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/on"
    ,"dijit/layout/ContentPane"
    ,"works/form/RadioButton"
    ,"./_WidgetMixin"
    ,"./_FormWidgetMixin"
    ,"./_RadioGroupMixin"
], function( _require, declare, lang, array, on, ContentPane, RadioButton, _WidgetMixin, _FormWidgetMixin, _RadioGroupMixin ){
    
    var RadioGroup = declare( [ ContentPane, _WidgetMixin, _FormWidgetMixin, _RadioGroupMixin ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
 
        ,lastValue: null
        ,columns: 1
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.form = form;
            this.announceEvent( "changed", { rename: "change" } );

            this.buttons = array.filter( this.form.getDescendants(), lang.hitch( this, function( widget ){ 
                return widget.isInstanceOf( RadioButton) && widget.get( "name" ) == this.get( "name" ) 
            }));

            this.own(
                array.map( this.buttons, function( item ){
                    return on( item, "change", lang.hitch( this, this._changed ) )
                }, this )
            )
        }
        
        ,leaveForm: function() {
            this.retireEvent( "changed" );
            this.form = null;
            this.buttons = null;
            this.inherited( arguments );
        }
        
    });
    
    
    RadioGroup.markupFactory = _WidgetMixin.markupFactory;
    
    return RadioGroup;
});