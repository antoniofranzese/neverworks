define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-attr"
    ,"./TextBox"
    ,"./_WidgetMixin"
], function( _require, declare, domAttr, TextBox, _WidgetMixin ){
    
    var PasswordTextBox = declare( [ TextBox ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,type: "password"
        ,revealed: false
        
        ,_setRevealedAttr: function( value ) {
            domAttr.set( this.focusNode, "type", value == true ? "text" : "password" );
        }
    
    });
    
    PasswordTextBox.markupFactory = TextBox.markupFactory;
    
    return PasswordTextBox;
});