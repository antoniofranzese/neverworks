define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-class"
    ,"dojo/dom"
    ,"dijit/form/RadioButton"
    ,"./_SupportingWidget"
    ,"./_WidgetMixin"
], function( _require, declare, domClass, dom, RadioButton, _SupportingWidget, _WidgetMixin ){
    
    return declare( [ RadioButton, _SupportingWidget ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

        ,itemValue: ""
        ,readonly: false
        
        ,buildRendering: function() {
            if( this.labelId ) {
                this.labelNode = dom.byId( this.labelId );
            }
            this.inherited( arguments );
        }
        
		,_onClick: function(){
            if( ! this.readonly ) {
                return this.inherited( arguments );
            }
		}

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setReadonlyAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
            if( this.labelNode ) {
                domClass[ value == true ? "add" : "remove" ]( this.labelNode, "readonly" );
            }
            this._set( "readonly", value );
        }
        
        ,_setCheckedAttr: function( value ) {
            this.inherited( arguments );
            if( this.labelNode ) {
                domClass[ value == true ? "add" : "remove" ]( this.labelNode, "checked" );
            }
        }
    });
    
});