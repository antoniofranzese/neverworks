define([
	'dojo/_base/declare'
	,'dojo/_base/lang'
	,'dojo/_base/array'
	,'dojo/query'
    ,"dojo/on"
	,'works/registry'
	,'dojo/dom-attr'
    ,"dojo/dom-style"
	,'dojo/dom-geometry'
	,"dojo/Deferred"
	,"dojo/promise/all"
	,"dojo/when"
    ,"../layout/_ViewportMixin"
    ,"../style!./resources/Popup.less"

], function( declare, lang, array, query, on, registry, domAttr, domStyle, domGeometry, Deferred, all, when, _ViewportMixin ){
	
    //TODO: Nella trasformazione al nuovo framework, il meccanismo dei references puo' essere eliminato
    
	return declare( [ _ViewportMixin ], {
        
        startupPopup: function() {
            // console.log( "Popup startup" );
            
            this.formPane = this.getDescendants()[ 0 ];
            // console.log( "Formpane", this.formPane.domNode.style.width, this.formPane.domNode.style.height );
            this.own(
                on( this.formPane, "loaded", lang.hitch( this, this.reloadForm ) )
            );
        
            this.form = this.formPane.getDescendants()[ 0 ];
            // console.log( "Form", this.form.domNode.style.width, this.form.domNode.style.height );
            // console.log( "Popup form", this.form );
            // this.inherited( arguments );

            // console.log( "Popup startup", domGeometry.getMarginBox( this.containerNode ) );
        }
        
        ,joinForm: function( form ) {
            //console.log( "Popup join", form.get( "name" ) );
            this.parentForm = form
            this.form.$parentform = this.parentForm;
            this.form.ready().then( lang.hitch( this, this.resizeChildForm ) );
            this.form.joinForm( form );
        }
        
        ,leaveForm: function( form ) {
            this.form.leaveForm( form );
        }
        
        ,reloadForm: function() {
            // console.log( "Popup form reloaded" );
            this.form = this.formPane.getDescendants()[ 0 ];
            this.form.$parentform = this.parentForm;
            this.form.ready().then( lang.hitch( this, this.resizeChildForm ) );
            this.form.joinForm( this.parentForm );
        }
        
        ,getRequestedSize: function() {
            if( this.width !== undefined || this.height !== undefined ) {
                return { 
                    w: typeof this.width == "string" ? domStyle.toPixelValue( this.domNode, this.width ) : this.width
                    ,h: typeof this.height  == "string" ? domStyle.toPixelValue( this.domNode, this.height ) : this.height
                }
            } else {
                return undefined;
            }
        }
        
        ,resizeChildForm: function() {
            if( this.form && this.form.domNode ) {
                if( this.width !== undefined || this.height !== undefined ) {
                    this.resize( this.getRequestedSize() );
                } else {
                    // this.domNode.style.height = this.height !== undefined ? this.height: null;
                    // this.domNode.style.width = this.width !== undefined ? this.width : null;
                    this.domNode.style.height = null;
                    this.domNode.style.width = null;
                }

                if( this.form ) {
                    this.form.domNode.style.width = null;
                    this.form.domNode.style.height = null;
                }

                this._position();
            }
        }
        
        ,_setWidthAttr: function( value ) {
            this._set( "width", value );
            this.resizeChildForm();
        }

        ,_setHeightAttr: function( value ) {
            this._set( "height", value );
            this.resizeChildForm();
        }
        
        ,spreadMessage: function( message ) {
            this.notifyMessage( message );
            if( this.form ) {
                this.form.spreadMessage( message );
            }
        }

        ,notifyMessage: function( message ) {

        }
        
        ,destroy: function() {
			if(this._fadeInDeferred){
                this._fadeInDeferred.then( function(){}, function(){});
                this._fadeInDeferred.cancel( "destroy", false );
                delete this._fadeInDeferred;
			}
			if(this._fadeOutDeferred){
                this._fadeOutDeferred.then( function(){}, function(){});
                this._fadeOutDeferred.cancel( "destroy", false );
                delete this._fadeOutDeferred;
			}
            this.inherited( arguments );
            
        }
        
        
	});

});
