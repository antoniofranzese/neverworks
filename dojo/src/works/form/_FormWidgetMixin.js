define([ 
    "dojo/_base/declare"
    ,"dijit/form/_FormWidget"
    ,"../_WidgetUtils"
], function( declare, _FormWidget, _WidgetUtils ) {
	
    var FormWidgetMixin = declare( null, {
        name: ""
        
        ,constructor: function() {
            // Trick to fool isInstanceOf
            this.constructor._meta.bases.push( _FormWidget );
        }
        
        // Old style accessors
        ,_getNameAttr: function() {
            return this.name;
        }
        
        ,_setNameAttr: function( value ) {
            this.name = value;
        }

        // New style accessors
        ,_nameGetter: function() {
            return this.name;
        }
        
        ,_nameSetter: function( value ) {
            this.name = value;
        }

    });
    
    FormWidgetMixin.inferDeclaredClass = _WidgetUtils.inferDeclaredClass;
    
    return FormWidgetMixin
	
})
