define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/on"
    ,"dojo/aspect"
    ,"dojo/query"
    ,"dojo/dom-attr"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"dojo/dom-geometry"
    ,"dojox/html/entities"
    ,"works/lang/strings"
    ,"dijit/Editor"
    ,"dijit/_editor/plugins/EnterKeyHandling"
    ,"dijit/_WidgetBase"
    ,"./_FormWidgetMixin"
    ,"./_WidgetMixin"
    ,"../layout/_FixedSizeMixin"
    ,"dojo/text!./resources/HTMLEditorContent.css"

    ,"works/style!./resources/HTMLEditor.css"
], function( req, declare, lang, array, on, aspect, query, domAttr, domStyle, domClass, domConstruct, domGeometry, htmlEntities, strings
    , dijitEditor, EnterKeyHandling
    , _WidgetBase, _FormWidgetMixin, _WidgetMixin, _FixedSizeMixin, contentCSS ) {
    
    var CLASS_REGEX = new RegExp( "(nw_|dj_).*" );

    var HTMLEditor = declare( declare.className( req ), [ _WidgetBase, _WidgetMixin, _FormWidgetMixin, _FixedSizeMixin ], {
        
        spelling: false
        ,required: false
        ,visible: true
        ,disabled: false
        ,readonly: false
        ,dirtyDelay: 500

        ,_dirty: false

        ,constructor: function() {
            this.operations = [];
            this.features = {
                undo: true
                ,format: true
                ,lists: true
                ,indent: true
                ,alignment: true
            }
        }
        
        ,buildRendering: function() {
            this.inherited( arguments );
            domClass.add( this.domNode, "worksEditor" );
            this.editorNode = domConstruct.create( "div", {}, this.domNode );
        }
        
        ,startup: function() {
            this.inherited( arguments );
            if( this.isShown() ) {
                this.createEditor();
            }
        }
        
        ,destroy: function() {
            if( this.editorObserver ) {
                this.editorObserver.disconnect();
                delete this.editorObserver;
            }

            if( this.editor ) {
                this.editor.destroy();
                delete this.editor;
            }

            this.inherited( arguments );
        }
    
        ,setDirty: function() {
            this._dirty = true;
            delete this._currentValue;
            if( this._dirtyTimeout ) {
                clearTimeout( this._dirtyTimeout );
            }
            this._dirtyTimeout = setTimeout( lang.hitch( this, this.notifyDirty ), this.dirtyDelay );
        }

        ,resetDirty: function() {
            this._dirty = false;
            delete this._currentValue;
        }

        ,notifyDirty: function() {
            //console.log( "DIRTY" );
            delete this._dirtyTimeout;
        }

        ,createEditor: function() {
            var plugins = this.createPlugins();

            var editor = new dijitEditor({
                plugins: plugins
                ,contentPostFilters: [
                    function( txt ) {
                        return htmlEntities.applyEncoding( txt, htmlEntities.latin );
                    }
                    ,function( txt ) {
                        return strings.safe( txt ).replaceAll( "<br\\s*/?>", "<br/>" );
                    }
                ]
            }, this.editorNode );

            editor.startup();
            
            editor.onLoadDeferred.then( lang.hitch( this, function(){
                this.editor = editor;
                var editorDocument = this.get( "editorDocument" );
                var editorBody = this.get( "editorBody" );

                if( editorDocument ) {

                    domClass.forEach( document.documentElement, function( cls ){
                        if( CLASS_REGEX.test( cls ) ) {
                            domClass.add( editorDocument.documentElement, cls );
                        }
                    }, this );

                    this.setCustomCSS( "__builtin__", contentCSS );

                }
                
                if( editorBody ) {

                    if( this.spelling == false ) {
                        domAttr.set( editorBody, "spellcheck", "false" );
                    }

                    this.own(
                        on( editorBody, "paste", lang.hitch( this, function( evt ){
							this.editor.beginEditing( "paste" );
                            setTimeout( lang.hitch( this.editor, this.editor.endEditing ), 1 );
                        }))
                    );
                    
                }
                
                this.dequeueOperations();

                if( editorBody ) {
                    
                    this._ignoreFirstMutation = true;
                    this.editorObserver = new MutationObserver( lang.hitch( this, function( m ){
                        this.setDirty();
                    }));
                
                    this.editorObserver.observe( editorBody, {
                        subtree: true,
                        childList: true,
                        characterData: true,
                        attributes: false
                    });
                
                }
                
                this.resetDirty();

                setTimeout( lang.hitch( this, this.refresh ), 0 );
            }));
            
        }
        
        ,createPlugins: function() {
            var plugins = [];

            if( this.features.undo ) {
                plugins.push( "undo", "redo", "|" );
            }

            if( this.features.format ) {
                plugins.push( "bold", "italic", "underline", "strikethrough", "|" )
            }

            if( this.features.lists ) {
                plugins.push( "insertOrderedList", "insertUnorderedList" );
            }

            if( this.features.indent ) {
                plugins.push( "indent", "outdent" );
            }

            if( this.features.lists || this.features.indent ) {
                plugins.push( "|" );
            }

            if( this.features.alignment ) {
                plugins.push( "justifyLeft", "justifyRight", "justifyCenter", "justifyFull", "|" );
            }

            if( plugins[ plugins.length - 1 ] == "|" ) {
                plugins = plugins.slice( 0, plugins.length - 1 );
            }

            plugins.push( EnterKeyHandling );

            return plugins;
        }

        ,setCustomCSS: function( id, text, position ) {
            position = position || "last";
            var doc = this.get( "editorDocument" );
            if( doc ) {
                var cssNode = query( "style#" + id, doc.head )[0];
                if( strings.hasText( text ) ) {
                    if( cssNode === undefined ) {
                        cssNode = domConstruct.create( "style", { id: id }, doc.head, position );
                    }
                    
                    cssNode.innerHTML = "\n" + text + "\n";

                } else {
                    if( cssNode !== undefined ) {
                        domConstruct.destroy( cssNode );
                    }
                }
            }
        }

        ,dequeueOperations: function() {
            var ops = this.operations;
            this.operations = [];
            while( ops.length > 0 ) {
                var operation = ops.shift();
                if( operation.feature == "set" ) {
                    this.set( operation.property, operation.value );
                } 
            }
            
            if( this._pendingSize ) {
                this.resize( this._pendingSize );
            }
        }
    
        ,isShown: function() {
            return domStyle.isShown( this.domNode );
        }
        
        ,isActive: function() {
            return this.isShown() && this.editor;
        }

        ,resize: function( size ) {
            if( size ) {
                if( this.isShown() ) {
                    delete this._pendingSize;
                    domGeometry.setMarginBox( this.domNode, this.processFixedSize( size ) );
                    
                    var box = domGeometry.getContentBox( this.domNode );
                    domGeometry.setMarginBox( this.editorNode, box );
                    
                    if( this.editor ) {
                        this.editor.resize( box );
                        var mb = domGeometry.getMarginBox( this.editor.iframe.parentNode );
                        domStyle.set( this.editor.iframe, "height", ( mb.h - 1 ) + "px" );
                    }
                } else {
                    this._pendingSize = size;
                }
            }
        }
    
        ,refresh: function() {
            this.resize( domGeometry.getMarginBox( this.domNode ) );
        }

        ,cellResizing: function( parent ) {
            if( this.fixedWidth === undefined ) {
                this.domNode.style.width = "1px";
            }
            return this.domNode;
        }

        ,cellResize: function( size ) {
            this.resize( this.processElasticSize( size ) );
        }
    
        ,notifyMessage: function( message ) {
            if( message.topic == "widget/container/show" ) {
                delete this._savedValue;
                if( this.editor === undefined ) {
                    this.createEditor();
                }
                this.dequeueOperations();
            
            } else if( message.topic == "widget/container/hide" ) {
                if( this.editor && this._currentValue === undefined ) {
                    this._currentValue = this.editor.get( "value" );
                }
            }
        }

        ,getWidgetState: function() {
            if( this._dirty ) {
                var value = this.get( "value" );
                this.resetDirty();
                return {
                    value: value
                };
            } 
        }
        
        ,setWidgetState: function( state ) {
            var wasActive = this.isActive();
            
            if( this.editor ) {
                return this.editor.get( "value" );
            }

            if( ! wasActive && this.isActive() ) {
                this.dequeueOperations();
            }
        }
        
        ,_getEditorBodyAttr: function() {
            if( this.editor ) {
                return query( "#dijitEditorBody", this.editor.iframe.contentDocument.body )[0];
            }
        }

        ,_getEditorDocumentAttr: function() {
            if( this.editor ) {
                return this.editor.iframe.contentDocument;
            }
        }

        ,_getValueAttr: function( value ) {
            if( this.isActive() ) {
                return this.editor.get( "value" );
            } else {
                return this._currentValue;
            }
        }

        ,_setValueAttr: function( value ) {
            value = value || "";
            delete this._currentValue;
            if( this.isActive() ) {
                this.editor.set( "value", value );
                setTimeout( lang.hitch( this, function(){
                    this.resetDirty();
                }), 1 );
            } else {
                this._queueSet( "value", value )
            }
        }

        ,_setCustomStylesAttr: function( value ) {
            if( this.isActive() ) {
                if( value && lang.isObject( value ) ) {
                    array.forEach( Object.keys( value ), function( name ){
 
                        var text = strings.safe( value[ name ] ).trim();

                        if( text.hasText() ) {

                            if( text.indexOf( "##" ) < 0 ) {
                                text = "## " + text;
                            }

                            text = text.replaceAll( "##", "#dijitEditorBody" );
                        }

                        this.setCustomCSS( name, text );

                    }, this );
               }
            } else {
                this._queueSet( "customStyles", value );
            }
        }

        ,_setCustomFeaturesAttr: function( value ) {
            if( value && lang.isObject( value ) && this.editor === undefined ) {
                array.forEach( Object.keys( value ), function( name ){
                    this.features[ name ] = ( value[ name ] == true );
                }, this );
           }
        }
        
        ,_setReadonlyAttr: function( value ) {
            if( this.isActive() ) {
                var editorBody = this.get( "editorBody" );
                if( editorBody ) {
                    domAttr.set( editorBody, "contentEditable", value == true ? "false" : "true" );
                    domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
                }
                this._set( "readonly", value );
                this.refresh();
            } else {
                this._queueSet( "readonly", value )
            }
        }

        ,_setDisabledAttr: function( value ) {
            if( this.isActive() ) {
                domClass[ value == true ? "add" : "remove" ]( this.domNode, "worksDisabled" );
                this.editor.set( "disabled", value );
                this._set( "disabled", value );
            } else {
                this._queueSet( "disabled", value );
            }
        }

        ,_setRequiredAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "required" );
            this._set( "required", value );
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == false ? "add" : "remove" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_queueSet: function( name, value ) {
            this.operations.push({
                feature: "set"
                ,property: name
                ,value: value 
            });
        }
        
    });
    
    HTMLEditor.markupFactory = _WidgetMixin.markupFactory;
    
    return HTMLEditor;
    
});
