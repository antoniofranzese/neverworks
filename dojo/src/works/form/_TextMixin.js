define([
    "dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/has"
    ,"dojo/query"
    ,"dojo/keys"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-construct"
    ,"dojox/html/entities"
    ,"works/lang/strings"
    ,"works/_WidgetUtils"
    ,"./_FocusingWidget"
    ,"./_ProblemMixin"
    ,"./_PickMixin"
    ,"../layout/_FixedSizeMixin"
], function( declare, lang, on, topic, has, query, keys, domStyle, domAttr, domClass, domGeometry, domConstruct, htmlEntities, strings, utils, _FocusingWidget, _ProblemMixin, _PickMixin, _FixedSizeMixin ){
	
	 return declare( [ _FocusingWidget, _ProblemMixin, _PickMixin, _FixedSizeMixin ], {
         sealed: false
         ,readonly: false
         ,visible: true
         
         ,acquireFocus: function() {
             this.focus();
         }
         
         ,resignFocus: function() {
             //this.set( "focused", false );
         }
         
         ,buildRendering: function() {
             this.inherited( arguments );
             this._updateDisplayStrategy();
         }
        
         ,joinForm: function( form ) {
             this.inherited( arguments );
             this.announceEvent( "changed", { rename: "change" } );
             this.announceEvent( "enterpressed", { rename: "enterPress" } );
             this.announceEvent( "tabpressed", { rename: "tabPress" } );
         }
         
         ,leaveForm: function( form ) {
             this.abandonForm();
             // this.retireEvent( "changed" );
             // this.retireEvent( "enterpressed" );
             // this.retireEvent( "tabpressed" );
             this.inherited( arguments );
         }
         
         ,startup: function() {
             this.inherited( arguments );
             this.own(
                 on( this, "change", lang.hitch( this, this.handleChangeEvent ) )
                 ,on( this, "select", lang.hitch( this, this.handleSelectEvent ) )
                 ,on( this, "keypress", lang.hitch( this, this.handleKeyPressEvent ) )
             );
             this._lastValue = this.get( "value" );
         }
         
         ,handleChangeEvent: function( val ) {
             //console.log( "Change Value", this.get( "name" ), this.get( "value" ), "Last", this._lastValue );
             if( this.get( "value" ) != this._lastValue ) {
                 if( this.eventActive( "changed" ) ) {
                     this.emitEvent( "changed", { state: {} } );
                     this._lastValue = this.get( "value" );
                     this._lastValidValue = this._lastValue;
                 }
             }
         }
         
         ,_processInput: function() {
             this.inherited( arguments );
             if( this.isReallyValid( this.get( "value" ) ) ) {
                 this._lastValidValue = this.get( "value" );
             } 
         }

         ,handleKeyPressEvent: function( evt ) {
             //TODO: Verifica Explorer :(
             if( evt.keyCode == keys.ENTER && this.eventActive( "enterpressed" ) ) {
                 evt.stopPropagation();
                 evt.preventDefault();
                 this.handleEnterPressEvent( evt );
             } else if( evt.keyCode == keys.TAB && this.eventActive( "tabpressed" ) ) {
                 evt.stopPropagation();
                 evt.preventDefault();
                 this.handleTabPressEvent( evt );
             }
         }
         
         ,handleEnterPressEvent: function( evt ) {
             this.emitEvent( "enterpressed", { state: {} } );
         }
         
         ,handleTabPressEvent: function( evt ) {
             this.emitEvent( "tabpressed", { state: {} } );
         }
         
         ,handleSelectEvent: function( evt ) {
             evt.stopPropagation();
         }
         
         ,setWidgetState: function( state ) {
             // console.log( this.get( "name" ), "Set state: ", state );
             if( "value" in state ) {
                 this._setValueAttr( state.value, /* priority */ false );
                 this._lastValue = state.value;
                 this._lastValidValue = this._lastValue;
                 delete state.value;
             }
             this.inherited( arguments );
         }
         
         ,getWidgetState:function(){
             // console.log( "State Value", this.get( "name" ), " = ", this.get( "value" ), "Last", this._lastValue );
             var value = this.get( "value" );
             if( this.get( "readonly" ) != true && this.get( "disabled" ) != true && value != this._lastValue ) {
                 this._lastValue = value;
                 this._lastValidValue = this._lastValue;
    	         return {
    				 value: value 
    		     }
             }
		 }
         
         ,_setReadonlyAttr: function( value ) {
             if( value == true ) {
                 domAttr.set( this.focusNode, "readonly", "readonly" );
                 domClass.add( this.domNode, "readonly" )
             } else {
                 domAttr.remove( this.focusNode, "readonly" );
                 domClass.remove( this.domNode, "readonly" )
             }
             this._set( "readonly", value );
             this._updateDisplayStrategy();
         }
		 
         ,_setRequiredAttr: function( value ) {
             if( value == true ) {
                 domClass.add( this.domNode, "required" )
             } else {
                 domClass.remove( this.domNode, "required" )
             }
             this._set( "required", false );
         }

         ,_setVisibleAttr: function( value ) {
             if( value == true ) {
                 domClass.remove( this.domNode, "hidden" );
             } else {
                 domClass.add( this.domNode, "hidden" );
             }

             this._set( "visible", value );
         }

         ,_setHorizontalAlignmentAttr: function( value ) {
             domStyle.set( this.textbox, "textAlign", value );
             this._set( "horizontalAlignment", value );
             this._updateDisplayNode();
         }
         
         ,_setColorAttr: function( value ) {
             domStyle.set( this.textbox, "color", value );
         }
         
         ,_setBackgroundColorAttr: function( value ) {
             domStyle.set( this.domNode, "backgroundColor", value );
         }
		 
         ,_setFontAttr: function( value ) {
             domStyle.set( this.domNode, value );
         }

         ,_setBorderAttr: function( value ) {
             domStyle.set( this.domNode, value );
         }

         ,_getDevelopmentDescriptionAttr: function() {
             return lang.mixin( this.inherited( arguments),{
                 properties: [ "value", "readonly", "disabled" ]
                 ,events: [ "change", "enterPress", "tabPress" ] 
             });
         }
         
         ,autoCellWidth: false

         ,_setAutoCellWidthAttr: function( value ) {
             this._set( "autoCellWidth", this.fixedWidth === undefined ? value : false );
         }
         
         ,cellResizing: function( parent ) {
             var accept = false;
             if( this.autoCellWidth || this.elasticWidth ) {
                 this.domNode.style.width = "1px";
                 accept = true;
             }
             if( this.elasticHeight ) {
                 this.domNode.style.height = "1px";
                 accept = true;
             }
             return accept ? this.domNode : undefined;
         }

         ,cellResize: function( size ) {
             if( this.elasticHeight === undefined ) {
                 delete size.h;
             }
             domGeometry.setMarginBox( this.domNode, this.processElasticSize( size ) );
         }
         
         ,_setSealedAttr: function( value ) {
             this._set( "sealed", value );
             domClass[ value == true ? "add" : "remove" ]( this.domNode, "sealed" );
             this._updateDisplayStrategy();
         }

         ,_setRoundingAttr: function( value ) {
             domStyle.set( this.domNode, value );
         }

         ,_setShadowAttr: function( value ) {
             domStyle.set( this.domNode, value );
         }

         ,_setDisabledAttr: function( value ) {
             this.inherited( arguments );
             this._updateDisplayStrategy();
         }

         ,_setDisplayedValueAttr: function( value ) {
             this.inherited( arguments );
             this._updateDisplayNode();
             this._updateCaptionNode();
         }

         ,_setValueAttr: function( value ) {
             this.inherited( arguments );
             this._updateDisplayNode();
             this._updateCaptionNode();
         }
         
         ,_setTitleAttr: function( value ) {
             this._set( "title", value );
             this._updateCaptionNode();
             if( ! this.focused ) {
                 this._updateDisplayStrategy();
             }
         }

         ,_updateDisplayStrategy: function() {
             if( this.get( "readonly" ) == true || this.get( "disabled" ) == true || this.get( "sealed" ) == true ) {
                 if( this.displayNode === undefined ) {

                     this.displayNode = domConstruct.create( "div", {
                         "class": "worksTextBoxDisplay"
                         ,style: {
                             "textAlign": this.get( "horizontalAlignment" )
                         }
                         ,"innerHTML": htmlEntities.encode( this.get( "displayedValue" ) )
                     }, query( ".dijitInputContainer", this.domNode )[0], "last" );

                     this.displayNode.onclick = lang.hitch( this, function(){
                         this.emit( "screen-click", { bubbles: false } );
                     });

                 } 

                 domStyle.set( this.displayNode, "display", "block" );
                 domStyle.set( this.focusNode, "display", "none" );
                 if( this.captionNode !== undefined ) {
                     domStyle.set( this.captionNode, "display", "none" );
                 }

             } else {
                 if( this.displayNode !== undefined ) {
                     domStyle.set( this.displayNode, "display", "none" );
                 }
                 
                 if( strings.hasText( this.get( "title" ) ) && ! strings.hasText( this.get( "displayedValue" ) ) ) {

                     if( this.captionNode === undefined ) {
                         this.captionNode = domConstruct.create( "div", {
                             "class": "worksTextBoxCaption"
                             ,style: {
                                 "textAlign": this.get( "horizontalAlignment" )
                             }
                             ,"innerHTML": this.get( "title" )
                         }, query( ".dijitInputContainer", this.domNode )[0], "last" );

                         this.captionNode.onclick = lang.hitch( this, function(){
                             this.emit( "caption-click", { bubbles: false } );
                             domStyle.set( this.captionNode, "display", "none" );
                         });
                         
                         this.own(
                             on( this.focusNode, "focus", lang.hitch( this, function(){
                                 domStyle.set( this.captionNode, "display", "none" );
                             }))
                             ,on( this.focusNode, "blur", lang.hitch( this, this._updateDisplayStrategy ) )
                         );

                     }

                     domStyle.set( this.captionNode, "display", "block" );
                     
                 } else {
                     
                     domStyle.set( this.focusNode, "display", "block" );
                     if( this.captionNode !== undefined ) {
                         domStyle.set( this.captionNode, "display", "none" );
                     }
                     
                 }
             }
         }

         ,_updateCaptionNode: function() {
             if( this.captionNode !== undefined ) {
                 domAttr.set( this.captionNode, "innerHTML", this.get( "title" ) );
                 if( this.get( "horizontalAlignment" ) !== undefined ) {
                     domStyle.set( this.captionNode, "textAlign", this.get( "horizontalAlignment" ) );
                 }
                 domStyle.set( this.captionNode, "display", strings.hasText( this.get( "displayedValue" ) ) ? "none" : "block" );
             }
         }
         
         ,_updateDisplayNode: function() {
             if( this.displayNode !== undefined ) {
                 domAttr.set( this.displayNode, "innerHTML", htmlEntities.encode( this.get( "displayedValue" ) ) );
                 if( this.get( "horizontalAlignment" ) !== undefined ) {
                     domStyle.set( this.displayNode, "textAlign", this.get( "horizontalAlignment" ) );
                 }
             }
         }
         
	 });
	
});