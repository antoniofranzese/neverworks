define([
    "require"
    ,"dojo/_base/declare"
    ,"dijit/form/SimpleTextarea"
    ,"./_WidgetMixin"
    ,"./_TextAreaMixin"
], function( _require, declare, SimpleTextarea, _WidgetMixin, _TextAreaMixin ){
    
    var widgetClass = declare( [ SimpleTextarea, _WidgetMixin, _TextAreaMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
    });
    
    //widgetClass.markupFactory = _WidgetMixin.markupFactory;
    
    return widgetClass;
});