define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"dijit/layout/ContentPane"
    ,"works/form/RadioButton"
    ,"./_WidgetMixin"
    ,"./_FormWidgetMixin"
], function( _require, declare, lang, array, on, domClass, ContentPane, RadioButton, _WidgetMixin, _FormWidgetMixin ){
    
    var RadioGroup = declare( [ ContentPane, _WidgetMixin, _FormWidgetMixin ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

        ,lastValue: null
        ,fractions: 1
        ,sparse: false
        ,arrangeByRows: false
        ,horizontal: true
        ,readonly: false
        
        ,buildRendering: function() {
            this.inherited( arguments );
            domClass.add( this.domNode, "worksRadioGroup" );
            this.labelTargetNode = this.domNode;
        }
        
        ,startup: function() {
            this.inherited( arguments );
            this._setValueAttr( this.initialValue, /*priorityChange*/ false );
            this.lastValue = this.initialValue;
            if( this.initialDisabled !== undefined ) {
                this.set( "disabled", this.initialDisabled )
            }
        }

        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
        
            this.buttons = array.filter( form.getDescendants(), lang.hitch( this, function( widget ){ 
                return widget.isInstanceOf( RadioButton) && widget.get( "name" ) == this.get( "name" ) 
            }));
            
            this._connectButtons();
        }
        
        ,leaveForm: function() {
            this._disconnectButtons();
            this.retireEvent( "changed" );
            this.inherited( arguments );
        }
        
        ,_connectButtons: function() {
            this._buttonConnects = []
            this._buttonConnects = array.map( this.buttons, function( item ){
                return on( item, "change", lang.hitch( this, this._changed ) )
            }, this );
        }

        ,_disconnectButtons: function() {
            array.forEach( this._buttonConnects, function( c ){ c.remove() } );
        }

        ,_setItemsAttr: function( items ) {
            if( ! this.sparse ) {

                var extension = Math.ceil( items.length / this.fractions );

                if( this.arrangeByRows ) {
                    rows = this.fractions;
                    cols = extension;
                } else {
                    rows = extension;
                    cols = this.fractions;
                }
            
                var matrix = [];
                for( var r = 0; r < rows; r++ ) {
                    if( matrix[ r ] == undefined ) {
                        matrix[ r ] = [];
                    }
                    for( var c = 0; c < cols; c++ ) {
                        matrix[ r ][ c ] = items[ this.horizontal ? r * cols + c : r + c * rows ];
                    }
                }
            
                var content = [ '<table class="radioGroup">' ];
                for( var r = 0; r < rows; r++ ) {
                    content.push( "<tr>" );

                    for( var c = 0; c < cols; c++ ) {
                        var item = matrix[ r ][ c ];
                        
                        if( item != undefined ) {
                            var itemID = ( typeof item.id == "number" ) ? item.id : ( "'" + item.id + "'" );
                            var radioID = this.id + "-radio-" + item.id;
                            var labelID = this.id + "-label-" + item.id;
                            content.push( '<td class="radioButton"><div id="' + radioID 
                                +'" data-dojo-type="works/form/RadioButton" data-dojo-props="itemValue:' + itemID 
                                + ', name:\'' + this.name 
                                + '\', labelId: \'' + labelID 
                                + '\', readonly: \'' + this.readonly 
                                + '\'"></div></td>' );
                            content.push( '<td class="radioLabel"><label class="radioLabel" id="' + labelID + '" for="' + radioID + '">' + item.cap + '</label></td>')
                        } else {
                            content.push( '<td colspan="2">&nbsp;</td>' );
                        }

                    }

                    content.push( "</tr>" );
                }

                content.push( "</table>" );

                this.set( "content", content.join( "" ) );
                this.buttons = array.filter( this.getDescendants(), function( item ){ return item.isInstanceOf( RadioButton ) } );
                this._connectButtons();
                
                this.set( "readonly", this.readonly );
                this.set( "disabled", this.disabled );

            } else {
                console.error( "Cannot set items on sparse RadioGroup" );
            }
        }

        ,_changed: function( value ) {
            if( value ) {
                var newValue = this.get( "value" );
                // Evita l'emissione del change dopo una variazione tecnica
                // ed elimina le emissioni ripetute a fronte dello stesso valore
                if( this._lastChange == undefined && newValue != this.lastValue ) {
                    this.emitAlways( "changed", {
                        state: {
                            old: this.lastValue
                            ,"new": newValue
                        }
                    });
                } else {
                    //Skip event
                    delete this._lastChange;
                }
                this.lastValue = newValue;
            }
        }
        
        ,_getValueAttr: function() {
            var checkeds = array.filter( this.buttons, function( item ){ return item.get( "checked" ) } );
            if( checkeds.length > 0 ) {
                return checkeds[ 0 ].get( "itemValue" );
            } else {
                return null;
            }
        }
        
        ,_setValueAttr: function( value, priorityChange ) {
            var itemValue = ( typeof value == "number" ) ? value : ( "" + value );
            
            if( priorityChange === false ) {
                this._lastChange = value;
            }

            array.forEach( this.buttons, function( item ){
                item.set( "checked", item.get( "itemValue" ) == itemValue );
            });
            
        }
        
		,getWidgetState: function(){
			return {
				value: this.get( "value" )
			}
		}

        ,setWidgetState: function( state ) {
            if( !("value" in state) ) {
                var savedValue = this.get( "value" );
            }
            this._setValueAttr( null, /*priorityChange*/ false );
            
            if( "items" in state ) {
                this.set( "items", state[ "items" ] );
                delete state.items;
            }
            
            if( "value" in state ) {
                this._setValueAttr( state[ "value" ], /*priorityChange*/ false );
                delete state.value;
            } else {
                this._setValueAttr( savedValue, /*priorityChange*/ false );
            }
            
            this.inherited( arguments );
            
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setReadonlyAttr: function( value ) {
            if( this.buttons ) {
                for( var i = 0; i < this.buttons.length; i++ ) {
                    this.buttons[ i ].set( "readonly", value );
                }
            }
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
            this._set( "readonly", value );
        }
        
        ,_setDisabledAttr: function( value ) {
            if( this.buttons ) {
                for( var i = 0; i < this.buttons.length; i++ ) {
                    this.buttons[ i ].set( "disabled", value );
                }
            }
            domClass[ value ? "add" : "remove" ]( this.domNode, "disabled" );
            this._set( "disabled", value );
        }
    });
    
    
    RadioGroup.markupFactory = _WidgetMixin.markupFactory;
    
    return RadioGroup;
});