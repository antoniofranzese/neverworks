define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-style"
    ,"dojox/html/entities"
    ,"works/registry"
    ,"dijit/_WidgetBase"
    ,"./_FormWidgetMixin"
    ,"./_WidgetMixin"
    ,"./_WidgetLocator"
    ,"./Manager"
    
], function( _require, declare, array, lang, on, domStyle, htmlEntities, registry, _WidgetBase, _FormWidgetMixin, _WidgetMixin, _WidgetLocator, Manager ){
    
    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin, _WidgetLocator ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,name: ""
        ,popup: ""
        ,startupOpened: false
        ,_opened: false
        ,_closing: false
        ,_hiding: false
        
        ,startup: function() {
            // console.log( this.popup, "PopupProxy startup" );
            
            this.dialog = registry.byId( this.popup );

            if( this.dialog == undefined ) {
                throw Exception( "Missing popup: " + this.popup );
            }

            this.inherited( arguments );

            this.own( 
                on( this.dialog, "hidden", lang.hitch( this, this._onHide ) )
            );
            
        }
        
        ,doResize: function() {
            this.dialog.resize();
        }

        ,joinForm: function( form ) {
            // console.log( "Join Popup", this.name );
            this.inherited( arguments );
            this.announceEvent( "close" );
            this.dialog.joinForm( form );
            this.form = form;
            this.set( "opened", this.startupOpened );
        }
        
        ,leaveForm: function( form ) {
            // console.log( this.name, "PopupProxy leave" );
            this.dialog.leaveForm( form );
            this.retireEvent( "close" );
            delete this.form;
            this.inherited( arguments );
        }

        ,destroy: function(){
            this.dialog.destroyRecursive();
            delete this.dialog;
            this.inherited( arguments );
        }
        
        ,destroyRecursive: function() {
            this.dialog.destroyRecursive();
            this.inherited( arguments );
        }
        
        ,spreadMessage: function( message ) {
            this.dialog.spreadMessage( message );
        }

        ,_onHide: function() {
            this._opened = false;
            if( !this._hiding ) {
                this._closing = true;
                this.emitAlways( "close", { state: {} } );
                this._closing = false;
            }
            this._hiding = false;
        }
        
        ,_fireCloseEvent: function() {
            this.startWidgetTask( { reason: "Synthetic Popup close" } );
            on.once( this, "close", lang.hitch( this, function(){
                this.endWidgetTask( { reason: "Synthetic Popup close" } );
            }));
            this.dialog.hide();
        }
        
        ,ready: function() {
            return this.dialog.form.ready();
        }
        
        ,widget: function( name ) {
            return this.dialog.form.widget( name );
        }
        
        ,findRelativeChild: function( name ) {
            return this.dialog.form.findRelativeChild( name );
        }
        
        ,getWidgetState: function() {
            return {
                state: this.dialog.form.gatherFormState()
                ,opened: this.get( "opened" )
                
                ,top: this.get( "top" )
                ,left: this.get( "left" )
                ,width: this.get( "resizable" ) ? this.get( "width" ) : undefined
                ,height: this.get( "resizable" ) ? this.get( "height" ) : undefined
            };
        }
        
        ,setWidgetState: function( value ) {
            if( "top" in value ) {
                this.set( "top", value.top );
                delete value[ "top" ];
            }
            
            if( "left" in value ) {
                this.set( "left", value.left );
                delete value[ "left" ];
            }
            
            if( "width" in value ) {
                this.set( "width", value.width );
                delete value[ "width" ];
            }
            
            if( "height" in value ) {
                this.set( "height", value.height );
                delete value[ "height" ];
            }

            if( "title" in value ) {
                this.set( "title", value.title );
                delete value[ "title" ];
            }
            
            if( "anchor" in value ) {
                this.set( "anchor", value.anchor );
                delete value[ "anchor" ];
            }

            if( "opened" in value ) {
                this.set( "opened", value.opened );
                delete value[ "opened" ];
            }

            if( "resizable" in value ) {
                this.set( "resizable", value.opened );
                delete value[ "resizable" ];
            }

            this.dialog.form.setWidgetState( value );
        
        }
        
        ,inspect: function() {
            if( this.dialog && lang.isFunction( this.dialog.inspect ) ) {
                this.dialog.inspect();
            }
        }

        ,release: function() {
            if( this.dialog && lang.isFunction( this.dialog.release ) ) {
                this.dialog.release();
            }
        }
        
        ,_getOpenedAttr: function( value ) {
            return this._opened;
        }

        ,_setOpenedAttr: function( value ) {
            this._opened = value;

            if( value ) {
                this._hiding = false;
                this.dialog.show();
            
            } else {
                this._hiding = true;
                this.dialog.hide();
            }
            
        }

        , _getTopAttr: function() {
            if( this._opened || this._closing ) {
                return domStyle.get( this.dialog.domNode, "top" );
            } else {
                return undefined;
            }    
        }
        , _setTopAttr: function( value ) {
            domStyle.set( this.dialog.domNode, "top", value != null ? value + "px" : value );
        }
            
        , _getLeftAttr: function() {
            if( this._opened || this._closing ) {
                return domStyle.get( this.dialog.domNode, "left" );
            } else {
                return undefined;
            }    
        }
        , _setLeftAttr: function( value ) {
            domStyle.set( this.dialog.domNode, "left", value != null ? value + "px" : value );
        }
            
        , _getWidthAttr: function() {
            if( this._opened || this._closing ) {
                return domStyle.get( this.dialog.domNode, "width" );
            } else {
                return undefined;
            }    
        }
        , _setWidthAttr: function( value ) {
            domStyle.set( this.dialog.domNode, "width", value != null ? value + "px" : value );
        }
            
        , _getHeightAttr: function() {
            if( this._opened || this._closing ) {
                return domStyle.get( this.dialog.domNode, "height" );
            } else {
                return undefined;
            }    
        }
        , _setHeightAttr: function( value ) {
            domStyle.set( this.dialog.domNode, "height", value != null ? value + "px" : value );
        }
        
        , _getTitleAttr: function( value ) {
            return this.dialog.get( "title" );
        }

        , _setTitleAttr: function( value ) {
            this.dialog.set( "title", value );
        }

        , _getAnchorAttr: function( value ) {
            return this.dialog.get( "anchor" );
        }

        , _setAnchorAttr: function( value ) {
            this.dialog.set( "anchor", value );
        }

        , _getResizableAttr: function( value ) {
            return this.dialog.get( "resizable" );
        }

        , _setResizableAttr: function( value ) {
            this.dialog.set( "resizable", value );
        }

        , _setAutoCloseAttr: function( value ) {
            this.dialog.set( "autoClose", value );
        }
        
        ,_getTitleProperty: function() {
            return htmlEntities.decode( this.get( "title" ) || "" ).replace( new RegExp( "\u00A0", "g" ), " " ); // HTML + nbsp->space
        }
        
            
    })
});