define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/query"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/store/Memory"
    ,"dijit/form/FilteringSelect"
    ,"./_WidgetMixin"
    ,"./_ProblemMixin"
    ,"./_PickMixin"
], function( _require, declare, lang, on, query, domStyle, domAttr, domClass, domGeometry, Memory, FilteringSelect, _WidgetMixin, _ProblemMixin, _PickMixin ){
    
    var FilteringSelect = declare( [ FilteringSelect, _WidgetMixin, _ProblemMixin, _PickMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

		,searchAttr: "cap"
        ,required: false
        ,initialValue: null
        ,blankValue: "__BLANK__"
        ,readonly: false
        ,visible: true
        
        ,startup: function() {
            this.inherited( arguments );
            
            this.own(
                on( this, "change", lang.hitch( this, this._change ) )
                ,on( this, "blur", lang.hitch( this, this._blur ) )
            );
            this._setValueAttr( this.startupValue, /*priorityChange*/ false );
            this._lastValue = this.get( "value" );
            
            // Height fix
            var domBox = domGeometry.getContentBox( this.domNode );
            var buttonPads = domGeometry.getPadBorderExtents( this._buttonNode );
            domStyle.set( this._buttonNode, "height", ( domBox.h - buttonPads.h ) + "px" );
            
            var textPads = domGeometry.getPadBorderExtents( query( ".dijitInputContainer", this.domNode )[0] );
            domStyle.set( this.textbox, "height", ( domBox.h - textPads.h ) + "px" );
        }

        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
        }
        
        ,leaveForm: function( form ) {
            this.retireEvent( "changed" );
            this.inherited( arguments );
        }
        
        ,_change: function( value ) {
            var current = this.get( "value" );
            if( current != this._lastValue ) {
                this.emitAlways( "changed", {
                    state: {
                        old: this._lastValue
                        ,"new": this.get( "value" )
                    }
                });
                this._lastValue = current;
            }
        }
        
        ,_blur: function() {
            if( this.get( "value" ) != this._lastValue ) {
                this._change();
            }
        }

        ,_setHorizontalAlignmentAttr: function( value ) {
            domStyle.set( this.textbox, "textAlign", value );
        }
        
        ,_setReadonlyAttr: function( value ) {
            domAttr[ value == true ? "set" : "remove" ]( this.focusNode, "readonly", "readonly" );
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
            this._set( "readonly", value );
            this.set( "readOnly", value );
        }

        ,_setRequiredAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "required" );
            this._set( "required", false );
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_getValueAttr: function() {
            var value = this.inherited( arguments ) || null;
            if( value == this.blankValue ) {
                return null;
            } else {
                return value;
            }
        }
        
        ,_setItemsAttr: function( value ) {
            var store = new Memory({ data: value });
            this.set( "store", store );
        }   
        
		,getWidgetState: function(){
            if( ! this.isReallyValid() ) {
                this._setValueAttr( this._lastValue, /*priorityChange*/ false );
            }
            if( ! this.readonly && ! this.disabled && this.visible ) {
    			return {
    				value: this.get( "value" )
    			}
            }
		}

        ,setWidgetState: function( state ) {
            if( !("value" in state) ) {
                var savedValue = this.get( "value" );
            }
            this._setValueAttr( null, /*priorityChange*/ false );
            
            if( "items" in state ) {
                this.set( "items", state[ "items" ] );
                delete state.items;
            }
            
            if( "value" in state ) {
                this._setValueAttr( state[ "value" ], /*priorityChange*/ false );
                delete state.value;
            } else {
                this._setValueAttr( savedValue, /*priorityChange*/ false );
            }
           
            this._lastValue = this.get( "value" );
            
            this.inherited( arguments );
        }

		,isValid: function(){
            if( this._validationProblem != null ) {
                return false;
            } else {
                return this.inherited( arguments );
            }
		}
        
    });
    
    FilteringSelect.markupFactory = _WidgetMixin.markupFactory;
    
    return FilteringSelect;
});