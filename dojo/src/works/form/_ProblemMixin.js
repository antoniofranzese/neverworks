define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"works/lang/strings"
    ,"dijit/Tooltip"
    ,"./_HintMixin"
], function( _require, declare, lang, on, domClass, strings, Tooltip, _HintMixin ){
    
    return declare( [ _HintMixin ], {
        _validationProblem: null 
        
        ,startup: function() {
            this.inherited( arguments );
            this.own(
                this.watch( "hovering", lang.hitch( this, this._validationOnHover ) )
            );
        }
        
        ,_validationOnHover: function( name, oldValue, value ) {
            if( this._validationProblem != null ) {
    			if( value ){
    				Tooltip.show( this._validationProblem, this.domNode, this.tooltipPosition, !this.isLeftToRight() );
    			}else{
    				Tooltip.hide( this.domNode );
    			}
            }
        }
        
        ,validator: function( value, constraints ) {
        	if( this._validationProblem != null ) {
        		return false;
        	} else {
        		return this.inherited(arguments);
        	}
        }

        ,getErrorMessage: function() {
        	if( this._validationProblem != null ) {
        		return this._validationProblem;
        	} else {
        		return this.inherited(arguments);
        	}
        }
        
        ,isReallyValid: function( focused ) {
            var savedProblem = this._validationProblem;
            this._validationProblem = null;
            var valid = this.isValid( focused );
            this._validationProblem = savedProblem;
            return valid;
        }

        ,_setProblemAttr: function( problem ) {
            if( this._validationFlavor !== undefined ) {
                domClass.remove( this.domNode, this._validationFlavor );
            }
            
            var message, flavor;
            if( problem !== undefined && problem != null && lang.isObject( problem ) ) {
                message = problem.message;
                flavor = problem.flavor;
            } else {
                message = problem;
            }
            
            if( strings.hasText( message ) ) {
                this.disableHint();
            } else {
                message = undefined;
                this.enableHint();
            }
            
        	this._validationProblem = message;
        	this._validationFlavor = flavor;

            if( this._validationFlavor !== undefined ) {
                domClass.add( this.domNode, this._validationFlavor );
            }
            
            domClass[ this._validationProblem != null ? "add" : "remove" ]( this.domNode, "problem" );
            
        	this._hasBeenBlurred = true;
        	this.validate( false );
        }
        
    });
    
});