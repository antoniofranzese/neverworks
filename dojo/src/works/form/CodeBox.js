define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dijit/layout/ContentPane"
    ,"dijit/_WidgetBase"
    ,"dijit/_TemplatedMixin"
    ,"./_WidgetMixin"
    ,"./_FormWidgetMixin"
    ,"dojo/text!./codebox/CodeBox.html"
    ,"./codebox/XMLFormatter"
    
    ,"works/external!/ext/highlight/highlight.pack.js"
    ,"works/style!./codebox/CodeBox.css"

], function( _require, declare, domClass, domGeometry, ContentPane, _WidgetBase, _TemplatedMixin, _WidgetMixin, _FormWidgetMixin, template, XMLFormatter  ){
    
    var formatters = {
        "xml": XMLFormatter
    }
    
    hljs.configure({ tabReplace: '<span class="indent">&nbsp;</span>' });
    
    var CodeBox = declare( [ ContentPane, _TemplatedMixin, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,templateString: template

        ,value: ""
        ,language: ""
        
        ,_setLanguageAttr: function( value ) {
            if( this.language.hasText() ) {
                domClass.remove( this.containerNode, this.language )
            }
            this.language = value ? ( "" + value ).toLowerCase() : "";
            domClass.add( this.containerNode, this.language );
        }
        
        ,_setValueAttr: function( value ) {
            var text = value;
            if( this.language.hasText() ) {
                if( this.language in formatters ) {
                    text = formatters[ this.language ]( value );
                }
            }
            
            this.containerNode.innerHTML = text;
            if( this.language.hasText() ) {
                hljs.highlightBlock( this.containerNode );
            }
            this._set( "value", text );
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }
        
        ,setWidgetState: function( state ) {
            if( state.language != undefined ) {
                this.set( "language", state.language );
                delete state.language;
            }
            this.inherited( arguments );
        }
    
        // ,resize: function( size ) {
        //     if( size ) {
        //         if( size.w ) {
        //             size.w = size.w - 2;
        //         }
        //         domGeometry.setMarginBox( this.domNode, size );
        //     }
        // }
    });
   
    CodeBox.markupFactory = _WidgetMixin.markupFactory;
    
    return CodeBox;
    
});