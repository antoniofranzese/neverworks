define([
    "require"
    ,"dojo/_base/declare"
    ,"dijit/form/Textarea"
    ,"./_WidgetMixin"
    ,"./_TextAreaMixin"
], function( _require, declare, Textarea, _WidgetMixin, _TextAreaMixin ){
    
    var widgetClass = declare( [ Textarea, _WidgetMixin, _TextAreaMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
    });
    
    //widgetClass.markupFactory = _WidgetMixin.markupFactory;
    
    return widgetClass;
});