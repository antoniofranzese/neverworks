define([
    "require"
    ,"dojo/_base/declare"
    ,"dijit/_WidgetBase"
    ,"works/form/_WidgetMixin"
    ,"works/form/_FormWidgetMixin"
], function( _require, declare, _WidgetBase, _WidgetMixin, _FormWidgetMixin ){
    
    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {

        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,url: ""

        ,joinForm: function() {
            window.location.href = this.url;
        }
	
    });

})
