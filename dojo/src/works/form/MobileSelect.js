define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/query"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"works/lang/strings"
    ,"dijit/_WidgetBase"
    ,"dijit/_TemplatedMixin"
    ,"dijit/_Contained"
    ,"works/dnd/Source"
    ,"works/form/_FormWidgetMixin"
    ,"works/form/_WidgetMixin"
    
    ,"dojo/text!./resources/MobileSelect.html"
    ,"works/style!./resources/MobileSelect.css"
], function( req, declare, lang, array, on, has, query, domStyle, domAttr, domClass, domConstruct, strings, _WidgetBase, _TemplatedMixin, _Contained, Source, _FormWidgetMixin, _WidgetMixin, template ){
    
    var Select = declare( declare.className( req ), [ _WidgetBase, _TemplatedMixin, _WidgetMixin, _FormWidgetMixin ], {

        templateString: template
        ,baseClass: "worksMobileSelect"
        ,visible: true
        ,disabled: false
        ,readonly: false
        ,changed: false
        ,blankable: false
	
        ,buildRendering: function() {
            this.inherited( arguments );
        }

        ,startup: function() {
            this.inherited( arguments );
            this.lastValue = this.get( "value" );
            if( this.startupValue !== undefined ) {
                this.set( "value", this.startupValue );
                delete this.startupValue;
            } else {
                this.set( "value", null );
            }

        }

        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
        }
        
        ,leaveForm: function( form ) {
            this.retireEvent( "changed" );
            this.inherited( arguments );
        }
        
        ,onChange: function( evt ) {
            var value = this.get( "value" );
            if( value != null || this.blankable ) {
                
                if( ! this.blankable ) {
                    query( "option[value=__BLANK__]", this.inputNode ).orphan();
                }
                
                this.changed = true;
                this.emitAlways( "changed", {state:{
                    "old": this.lastValue
                    ,"new": value
                }});
                this.lastValue = value;
            }
        }
        
        ,onIconClick: function() {
            this.inputNode.focus();
        }

        ,getWidgetState: function() {
            if( this.changed ) {
                this.changed = false;
                return {
                    value: this.get( "value" )
                };
            }
        }

        ,setWidgetState: function( state ) {
            if( state.items !== undefined ) {
                this.set( "items", state.items );
                delete state.items;
            }
            this.inherited( arguments );
        }
        
        ,_getValueAttr: function() {
            var value = this.inputNode.value;
            return value == "__BLANK__" ? null : value;
        }
        
        ,_setValueAttr: function( value ) {
            if( ( value == null || value === undefined ) && ! this.blankable ) {
                if( query( "option[value=__BLANK__]", this.inputNode )[0] === undefined ) {
                    domConstruct.create( "option",{
                        value: "__BLANK__"
                        ,innerHTML: ""
                    }, this.inputNode, "first" );
                }
            }
            this.inputNode.value = ( value != null && value !== undefined ) ? value : "__BLANK__";

            this.updateDisplay();
        }

        ,_setItemsAttr: function( value ) {
            query( "option", this.inputNode ).orphan();

            array.forEach( value, function( item ){
                domConstruct.create( "option",{
                    value: item.id
                    ,innerHTML: item.txt !== undefined ? item.txt : item.cap
                }, this.inputNode );
            }, this );

        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }
        
        ,_setDisabledAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "disabled" );
            domAttr[ value == true ? "set" : "remove"]( this.inputNode, "disabled", "true" );
            this._set( "disabled", value );
            this.updateDisplay();
        }

        ,_setReadonlyAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
            this._set( "readonly", value );
            this.updateDisplay();
        }
    
        ,updateDisplay: function() {
            var selected = this.inputNode.options[ this.inputNode.options.selectedIndex ];

            domAttr.set( this.displayNode, "innerHTML", ( selected !== undefined ? selected.innerHTML  : "" ) + "&nbsp;");
            
            if( this.readonly || this.disabled ) {
                domStyle.set( this.inputNode, "display", "none" );
                domStyle.set( this.displayNode, "display", "block" );
            } else {
                domStyle.set( this.inputNode, "display", "block" );
                domStyle.set( this.displayNode, "display", "none" );
            }
        }

    });

    Select.markupFactory = _WidgetMixin.markupFactory;
    
    return Select;

});
