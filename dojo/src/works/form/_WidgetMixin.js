define([
    "dojo/_base/declare"
	,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/dom-attr"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/aspect"
    ,"dojo/has"
    ,"../config"
	,"../lang/Set"
	,"../lang/strings"
	,"../registry"
    ,"../_LogMixin"
    ,"../_WidgetUtils"
    ,"./_FormManagerWidget"
    
    
], function( declare, lang, array, domAttr, domGeom, domClass, aspect, has, config, Set, strings, registry, _LogMixin, _WidgetUtils, _FormManagerWidget ){
    
    var STANDALONE = config( "standalone", false );

    var WidgetMixin = declare( [ _LogMixin ], {
    	
        _joined: false
        
        ,joinForm: function( form ) {
            this._joined = true;
            this._widgetParentForm = form;
            this.inherited( arguments );
        }
        
        ,leaveForm: function() {
            this._joined = false;
            delete this._widgetParentForm;
            this.inherited( arguments );
        }
        
        ,destroy: function() {
            if( this._joined ) {
                console.error( "Destroying joined widget: ", this );
            }
            this.inherited( arguments );
        }
        
        ,initEvents: function() {
            if( this._events == undefined ) {
                this._events = {
                    active: new Set()
                    ,renamed: {}
                }             
            }
        }
        
        ,announceEvent: function( event, params ) {
            this.initEvents();
            
            this.bubbleMessage( { topic: "widget/event/announce", body: lang.mixin({
                widget: this
                ,event: event
            }, params || {} )});
            
            if( params != undefined && params.rename != undefined ) {
                //console.log( "Storing real name for", event, ":", params.rename );
                this._events.renamed[ event ] = params.rename;
            }
            
            this.switchEvent( event );
        }

        ,announceEvents: function( events ) {
            this.initEvents();
            
            this.bubbleMessage( { topic: "widget/event/announce-all", body: {
                widget: this
                ,events: events
            }});
            
            array.forEach( events, function( event ){
                if( event.rename != undefined ) {
                    //console.log( "Storing real name for", event, ":", params.rename );
                    this._events.renamed[ event.name ] = event.rename;
                }
                this.switchEvent( event.name );
            }, this );
        }
        
        ,retireEvent: function( event, params ) {
            this.bubbleMessage( { topic: "widget/event/retire", body: lang.mixin({
                widget: this
                ,event: event
            }, params || {} ) } );
        }

        ,abandonForm: function() {
            this.bubbleMessage( { topic: "widget/form/abandon", body: {
                widget: this
            }});
        }
        
        ,emitEvent: function( event, mixin ) {
            this.initEvents();
            mixin = mixin || {};
            
            if( this.eventActive( event ) ) {
                if( mixin.state == undefined ) {
                    mixin = { state: mixin }
                }
                mixin.widget = this;
                if( mixin.bubbles == undefined ) {
                    mixin.bubbles = false;
                }
                this.emit( event, mixin );
            // } else {
            //     console.log( "Event", event, "is not active" );
            }
        }

        ,emitAlways: function( event, mixin ) {
            mixin = mixin || {};
            mixin.widget = this;
            if( mixin.bubbles == undefined ) {
                mixin.bubbles = false;
            }
            this.emit( event, mixin );
        }
        
        ,eventActive: function( event ) {
            if( STANDALONE ) {
                return true;
            } else {
                if( this._events ) {
                    if( this._events.active.contains( this.eventRealName( event ) ) ) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    //console.log( "Event without configuration:", event );
                    return false;
                }
            }
        }

        ,eventRealName: function( event ) {
            return event in this._events.renamed ? this._events.renamed[ event ] : event
        }

        ,switchEvent: function( name, value ) {
            this.initEvents();
            
            if( name.endsWith( "$event" ) ) {
                name = name.substr( 0, name.length - 6 );
            }

            var realName = this.eventRealName( name )
            if( value == undefined ) {
                value = this[ realName + "$event" ];
                if( value == undefined ) {
                    value = false;
                }
            }

            // console.log( this.name, "Switching event", name, value, "real", this.eventRealName( name ) );

            if( value ) {
                this._events.active.add( realName );
            } else {
                this._events.active.remove( realName );
            }
            
            this.updateEvent( { name: name, realName: realName, active: value });
        }
    	
        ,switchEvents: function( params ) {
            for( var name in params ) {
                this.switchEvent( name, params[ name ] );
            }
        }
        
        ,updateEvent: function( evt ) {}
        
        ,startWidgetTask: function( params ) {
            params = params || {};
            if( params.widget == undefined ) {
                params.widget = this;
            }
            this.bubbleMessage( { topic: "widget/task/start", body: params });
        }

        ,endWidgetTask: function( params ) {
            params = params || {};
            if( params.widget == undefined ) {
                params.widget = this;
            }
            this.bubbleMessage( { topic: "widget/task/end", body: params });
        }
        
        ,queryMainForm: function( callback ) {
            this.bubbleMessage( { topic: "widget/form/query", body: lang.mixin({
                widget: this
                ,callback: callback
            }, {} ) });
        }
        
        ,bubbleMessage: function( message ) {
            var form = this.getParentForm();
            if( form !== undefined ) {
                form.bubbleMessage( message );
            }
        }
        
        ,spreadMessage: function( message ) {
            this.notifyMessage( message );
            this.spreadChildren( message );
            
        }
        
        ,spreadChildren: function( message ) {
            if( lang.isFunction( this.getChildren ) ) {
                var children = this.getChildren();
                if( lang.isArray( children ) ) {
                    array.forEach( children, function( child ) {
                        if( child && lang.isFunction( child.spreadMessage ) ) {
                            child.spreadMessage( message );
                        }
                    });
                }
            }
        }

        ,notifyMessage: function( message ) {

        }
        
        ,getParentForm: function() {
            return registry.findParent( this, _FormManagerWidget );
    	}

        ,startWidgetTask: function( params ) {
            params = params || {};
            if( params.widget == undefined ) {
                params.widget = this;
            }
            this.bubbleMessage( { topic: "widget/task/start", body: params } );
        }

        ,endWidgetTask: function( params ) {
            params = params || {};
            if( params.widget == undefined ) {
                params.widget = this;
            }
            this.bubbleMessage( { topic: "widget/task/end", body: params } );
        }

        ,_getNameAttr: function() {
            return this.name;
        }
        
    	,setWidgetState:function(params){
    		for( var property in params){
                if( property.endsWith( "$event" ) ) {
                    this.switchEvent( property, params[ property ] );
                } else {
                    this.set( property, params[ property ] );
                }
    		}
    	}
        
        // ,toString: function() {
        //     return this.declaredClass + ": " + this.name;
        // }
        
        ,_getDevelopmentDescriptionAttr: function() {
            return {
                name: this._widgetParentForm != undefined ? this._widgetParentForm.getWidgetUniquePath( this ) : "<" + ( this.get( "name" ) ? "name:" + this.get( "name" ) : "id:" + this.id ) + ">"
                ,"class": this.declaredClass
            }
        }
        
        ,parseEventOrigin: function( evt ) {
            if( has( "ie" ) ) {
                clickX = evt.clientX + this.domNode.scrollLeft;
                clickY = evt.clientY + this.domNode.scrollTop;
            } else {
                clickX = evt.pageX;
                clickY = evt.pageY;
            }
            return {
                clickX: Math.floor( clickX )
                ,clickY: Math.floor( clickY )
            }
        }
        
        ,_setFlavorAttr: function( value ) {
            var node = this.flavorNode !== undefined ? this.flavorNode : this.domNode;
            if( node ) {
                domClass.removeAll( node, /flavor-.*/ );
                domClass.addAll( node, value );
            }
        }

        ,child: function( property, value ) {
            var children = this.getChildren();
            if( children ) {
                if( value ) {
                    return children.filter( function( child ) { return child.get( property ) == value } )[ 0 ];
                } else {
                    return children.filter( function( child ) { return child.get( "id" ) == property || child.get( "name" ) == property } )[ 0 ];
                }
            }
        }
        
    });
    
    WidgetMixin.markupFactory = function( params, node, ctor ) {
        var options = _WidgetUtils.parseOptions( node );
        dojo.mixin( params, options );
        return new ctor( params, node );
    }
    
    WidgetMixin.inferDeclaredClass = _WidgetUtils.inferDeclaredClass;
    WidgetMixin.registerWidget = _WidgetUtils.registerWidget;
    WidgetMixin.unregisterWidget = _WidgetUtils.unregisterWidget;

    return WidgetMixin;
})
