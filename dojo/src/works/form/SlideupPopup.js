define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/fx"
    ,"dojo/fx/easing"
    ,"dojo/query"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"dojo/dom-attr"
    ,"dijit/popup"
    ,"./TooltipPopup"
], function( req, declare, lang, fx, easing, query, domGeometry, domStyle, domClass, domConstruct, domAttr, popup, TooltipPopup ){
    
    return declare( declare.className( req ), [ TooltipPopup ], {

        _getAnchorWidgetAttr: function() {
            return { x: 0, y: 0 };
        }
        
        ,openAround: function( target ) {
            if( this._opened != true ) {
                this._stopAnimations();

                this._opened = true;
                this.popupWrapper = popup.moveOffScreen( this );
            
                if( ! domAttr.has( this.popupWrapper, "data-works-enhanced" ) ) {

                    var header = domConstruct.create( "div", { 
                        "class": "worksSlideupPopupHeader" 
                        ,"innerHTML": '<span class="closeButton">X</span>'
                    }, this.popupWrapper, "first" );
                
                    query( ".closeButton", header ).on( "click", lang.hitch( this, this.hide ) );
                
                    domAttr.set( this.popupWrapper, "data-works-enhanced", "true" );
                }
            
                domStyle.set( this.domNode, {
                    "padding-left": "0px"
                    ,"padding-right": "0px"
                });
 
                var po = popup.open({
                    x: 0
                    ,y: Math.floor( window.screen.height  )
                    ,popup: this
                    ,visibility: false
                });

                var wrapperHeight = domGeometry.getMarginBox( this.popupWrapper ).h;

                var domWidth = domGeometry.getMarginBox( query( ".dijitTooltipContents", this.domNode )[0] ).w;
                //console.log( "dom width", domWidth );

                domClass.add( this.popupWrapper, "worksSlideupPopupWrapper" );
            
                domStyle.set( this.popupWrapper, {
                    top: window.innerHeight + "px"
                    ,overflow: "hidden"
                    ,height: "0px"
                    ,left: "0px"
                    ,width: "100%"
                    ,visibility: "visible"
                });
            
                var padding = ( domWidth <= window.innerWidth ? Math.floor( ( window.innerWidth - domWidth ) / 2 ) : 0 ) + "px";
                domStyle.set( this.domNode, {
                    "padding-left": padding
                    ,"padding-right": padding
                });
                
                //console.log( "Padding:", padding );

                this._showAnimation = fx.animateProperty({
                    node: this.popupWrapper
                    ,properties: { top: window.innerHeight - wrapperHeight, height: wrapperHeight }
                    ,duration: 500
                    ,easing: easing.cubicOut
                }).play();
            }
        }
    
        ,resize: function( size ) {
            //console.log( "slideup resize" );
            this.inherited( arguments );
        }
    
        ,hide: function() {
            if( this._opened ) {
                this._opened = false;
                this.emit( "hidden", { bubbles: false } );

                this._stopAnimations();
            
                this._hideAnimation = fx.animateProperty({
                    node: this.popupWrapper
                    ,properties: { top: window.innerHeight, height: 0 }
                    ,duration: 500
                    ,easing: easing.cubicOut
                    ,onEnd: lang.hitch( this, function(){
                        domStyle.set( this.popupWrapper, "width", null );
                        domStyle.set( this.domNode, {
                            "padding-left": null 
                            ,"padding-right": null
                        });
                    })
                }).play();

                this.cleanup();
            }
            
            this.inherited( arguments );
        }
        
        ,_stopAnimations: function() {

            if( this._hideAnimation ){
                if( this._hideAnimation.status() == "playing" ) {
                    this._hideAnimation.stop();
                    this._hideAnimation.onEnd();
                }  
                delete this._hideAnimation;
            } 
            
            if( this._showAnimation ) {
                if( this._showAnimation.status() == "playing" ) {
                    this._showAnimation.stop();
                }
                delete this._showAnimation;
            }
            
        }
    
    });
    
});