define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-attr"
    ,"works/lang/strings"
    ,"dijit/Tooltip"
], function( _require, declare, domAttr, strings, Tooltip ){
    
    return declare( [], {
        _hintEnabled: false
         
        ,startup: function( value ) {
            this.inherited( arguments );
            this.enableHint();
            this._hintStarted = true;
        }

        ,destroy: function() {
            if( this._hintTooltip !== undefined ) {
                this._hintTooltip.destroy();
            }
            this.inherited( arguments );
        }
        
        ,_setHintAttr: function( value ) {
            if( strings.hasText( value ) ) {
                
                if( this._hintTooltip === undefined ) {
                    this._hintTooltip = new Tooltip({
                        position: this.get( "hintPosition" )
                    });
                }
                this._hintTooltip.set( "label", value );
                
                if( this._hintStarted ) {
                    this.enableHint();
                }

            } else {
                if( this._hintTooltip !== undefined ) {
                    this.disableHint();
                    this._hintTooltip.destroy();
                    delete this._hintTooltip;
                }
            }
        }
        
        ,_getHintPositionAttr: function() {
            return [ 'after', 'before' ];
        }
       
        ,_getHintTargetAttr: function() {
            return domAttr.get( this.domNode, "id" );
        }
        
        ,enableHint: function() {
            var target = this.get( "hintTarget" );
            if( target && this._hintTooltip != undefined && ! this._hintEnabled ) {
                this._hintEnabled = true;
                this._hintTooltip.addTarget( target );
            }
        }
        
        ,disableHint: function() {
            var target = this.get( "hintTarget" );
            if( target && this._hintTooltip != undefined && this._hintEnabled ) {
                this._hintEnabled = false;
                this._hintTooltip.removeTarget( target );
            }
        }
        
        ,closeHint: function() {
            if( this._hintTooltip != undefined ) {
                this._hintTooltip.close();
            }
        }

    });
    
})