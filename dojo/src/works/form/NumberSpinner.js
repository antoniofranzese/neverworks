define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dijit/form/NumberSpinner"
    ,"./_TextMixin"
    ,"./_WidgetMixin"
    
    ,"dojo/has!mobile?dojo/text!./resources/MobileSpinner.html"
    ,"dojo/has!mobile?works/style!./resources/MobileTextBox.css"
    
], function( req, declare, lang, has, on, topic, NumberSpinner, _TextMixin, _WidgetMixin, mobileTemplate ){
    
    var HOLD_DELAY = 200;
    var BOOST_DELAY = 4000;
    
    var NumberSpinner = declare( declare.className( req ), [ NumberSpinner, _WidgetMixin, _TextMixin ], {
        
        rolling: false
        ,deltaBoost: 10
        
        ,postCreate: function(){
            this.inherited( arguments );
            this.largeDelta = this.smallDelta * this.deltaBoost;
        }

        ,getWidgetState: function() {
            if( !( this.isReallyValid( /* focused */ false ) ) ) {
                this._setValueAttr( null, /* priority */ false );
            }
            return this.inherited( arguments );
        }
		
        ,_setValueAttr: function(/*Number*/ value, /*Boolean?*/ priorityChange, /*String?*/ formattedValue){
            this.inherited( arguments, [ value, priorityChange, value == 0 ? "0" : formattedValue ]);
        }
        
        ,adjust: function(/*Object*/ val, /*Number*/ delta){
            var cs = this.constraints || {};
            var rolling = this.rolling && cs.min !== undefined && cs.max !== undefined;
            
            var decimals = undefined; 
            if( ! isNaN( val ) ) {
                var s = "" + val;
                var dot = s.indexOf( "." );
                if( dot > 0 ) {
                    decimals = s.substring( dot + 1, s.length )
                }
            }

            var result;
            if( rolling && delta > 0 && ( val + delta ) > cs.max ) {
                result = cs.min;
            } else if( rolling && delta < 0 && ( val + delta ) < cs.min ) {
                result = cs.max - ( decimals === undefined  ? 0 : 1 );
            } else {
                result = this.inherited( arguments );
            }
            
            if( decimals !== undefined ) {
                result = parseFloat( "" + Math.floor( result ) + "." + decimals );
            }
            
            return result;
        }
    });
    
    
    if( has( "mobile" ) ) {
        NumberSpinner = declare( declare.className( req ), [ NumberSpinner ], {
            templateString: mobileTemplate
            
            ,postCreate: function(){
                this.inherited( arguments );
                this.own(
                    on( this.upArrowNode, "touchstart", lang.hitch( this, this.onUpArrowTouchStart ) )
                    ,on( this.upArrowNode, "touchend", lang.hitch( this, this.onUpArrowTouchEnd ) )
                    ,on( this.downArrowNode, "touchstart", lang.hitch( this, this.onDownArrowTouchStart ) )
                    ,on( this.downArrowNode, "touchend", lang.hitch( this, this.onDownArrowTouchEnd ) )
                    ,on( this.upArrowContainer, "touchstart", lang.hitch( this, this.onUpArrowTouchStart ) )
                    ,on( this.upArrowContainer, "touchend", lang.hitch( this, this.onUpArrowTouchEnd ) )
                    ,on( this.downArrowContainer, "touchstart", lang.hitch( this, this.onDownArrowTouchStart ) )
                    ,on( this.downArrowContainer, "touchend", lang.hitch( this, this.onDownArrowTouchEnd ) )
                );
            }
            
            ,onUpArrowTouchStart: function( evt ) {
                this._setValueAttr( this.adjust( this.get('value'), this.smallDelta ), /* priority */ false );
                this.arrowTouching = true;
                this.arrowHoldTimeout = setTimeout( lang.hitch( this, this.onArrowHold ), HOLD_DELAY );
                this.arrowLongHoldTimeout = setTimeout( lang.hitch( this, this.onArrowLongHold ), BOOST_DELAY );
                this.arrowHoldDelta = this.smallDelta;
                this.arrowHoldDirection = 1;
                evt.preventDefault();
                evt.stopPropagation();
            }
            
            ,onUpArrowTouchEnd: function() {
                this.arrowTouching = false;
                clearTimeout( this.arrowHoldTimeout );
                delete this.arrowHoldTimeout;
                clearTimeout( this.arrowLongHoldTimeout );
                delete this.arrowLongHoldTimeout;
            }
            
            ,onDownArrowTouchStart: function( evt ) {
                this._setValueAttr( this.adjust( this.get('value'), this.smallDelta * -1 ), /* priority */ false );
                this.arrowTouching = true;
                this.arrowHoldTimeout = setTimeout( lang.hitch( this, this.onArrowHold ), HOLD_DELAY );
                this.arrowLongHoldTimeout = setTimeout( lang.hitch( this, this.onArrowLongHold ), BOOST_DELAY );
                this.arrowHoldDelta = this.smallDelta;
                this.arrowHoldDirection = -1;
                evt.preventDefault();
                evt.stopPropagation();
            }
            
            ,onDownArrowTouchEnd: function() {
                this.arrowTouching = false;
                clearTimeout( this.arrowHoldTimeout );
                delete this.arrowHoldTimeout;
                clearTimeout( this.arrowLongHoldTimeout );
                delete this.arrowLongHoldTimeout;
            }

            ,onArrowHold: function() {
                if( this.arrowTouching ) {
                    this._setValueAttr( this.adjust( this.get('value'), this.arrowHoldDirection * this.arrowHoldDelta ), /* priority */ false )
                    this.upArrowHoldTimeout = setTimeout( lang.hitch( this, this.onArrowHold ), 50 );
                }
            }
            
            ,onArrowLongHold: function() {
                var max = this.constraints !== undefined && this.constraints.max !== undefined ? this.constraints.max : 999999;
                if( ( this.arrowHoldDelta * this.deltaBoost ) < max ) {
                    this.arrowHoldDelta = this.arrowHoldDelta * this.deltaBoost;
                    this.arrowLongHoldTimeout = setTimeout( lang.hitch( this, this.onArrowLongHold ), BOOST_DELAY );
                }
            }

        });
        
    } else {

        NumberSpinner = declare( declare.className( req ), [ NumberSpinner ], {
            
            _scrollLock: false

            ,postCreate: function() {
                this.inherited( arguments );

                this._unlockScroll = lang.hitch( this, function() {
                    this._scrollLock = false;
                    delete this._scrollLockTimeout;
                });
            }
            
            ,startup: function() {
                this.inherited( arguments );

                this.own(
                    topic.subscribe( "mouse/scroll", lang.hitch( this, this._windowMouseWheeled ) )
                    ,on( this, "focus", this._unlockScroll )
                    ,on( this, "click", this._unlockScroll )
                );
            }
            
            ,_formatter: function( value ){ 
                return value; 
            }
            
            ,_mouseWheeled: function( evt ) {
                if( this.get( "readonly" ) !== true && this._scrollLock !== true ) {
                    this.inherited( arguments );
                }
            }
            
            ,_windowMouseWheeled: function() {
                if( this._scrollLockTimeout !== undefined ) {
                    clearTimeout( this._scrollLockTimeout );
                } else {
                    this._scrollLock = true;
                }
                
                this._scrollLockTimeout = setTimeout( this._unlockScroll, 1000 );
            }
            
        });
    
    }

    NumberSpinner.markupFactory = _WidgetMixin.markupFactory;

    return NumberSpinner;

});