define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-class"
    ,"dijit/ProgressBar"
    ,"./_FormWidgetMixin"
    ,"./_WidgetMixin"
], function( _require, declare, domClass, ProgressBar, _FormWidgetMixin, _WidgetMixin ){
    
    return declare( [ ProgressBar, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setValueAttr: function( value ) {
            this.inherited( arguments, [ value == "Infinity" ? Infinity : value ] );
        }
        
    });
    
});