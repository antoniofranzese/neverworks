define([
    "require"
    ,"dojo/_base/declare"
    ,"works/config"
    ,"works/lang/strings"
    ,"dojo/on"
    ,"dojo/query"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-attr"
    ,"dojo/dom-geometry"
    ,"dojox/html/entities"
    ,"dijit/_WidgetBase"
    ,"dojo/Stateful"
    ,"dijit/_TemplatedMixin"
    ,"dijit/Tooltip"
    ,"dojo/text!./templates/Label.html"
    ,"works/_WidgetUtils"
    ,"works/_BlinkMixin"
    ,"./_WidgetMixin"
    ,"./_FormWidgetMixin"
    ,"./_PickMixin"
    ,"./_HintMixin"
    ,"./_ProblemMixin"
    ,"../layout/_FixedSizeMixin"
    ,"../layout/_ParentQualifyMixin"
], function( _require, declare, config, strings, on, query, domStyle, domClass, domAttr, domGeometry, htmlEntities, _WidgetBase, Stateful, _TemplatedMixin, Tooltip
    ,template, utils, _BlinkMixin, _WidgetMixin, _FormWidgetMixin, _PickMixin, _HintMixin, _ProblemMixin, _FixedSizeMixin, _ParentQualifyMixin ){
    
    var DRAG_DELAY = config( "dragDelay", 500 );
    
    var Label = declare( [ _WidgetBase, _TemplatedMixin, _WidgetMixin, _FormWidgetMixin, _HintMixin, _PickMixin, _FixedSizeMixin, _ParentQualifyMixin, _BlinkMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
 
    	,templateString : template
        
    	,value : ""
        ,autoWidth: false
        ,baseClass: "worksLabel"
        ,blinkingClass: "worksLabelBlink"
        ,visible: true
        ,disabled: false
        
    	,constructor: function( params, node ) {
    	    this.nodeContent = node.innerHTML;
    	}

    	,buildRendering: function() {
    	    this.inherited( arguments );
            if( this.get( "value" ) == "" ){
                this.set( "value", this.nodeContent );
            }
            this.domNode.oncontextmenu = function(){ return false; }
    	}
    	
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvents([
                { name: "clicked", rename: "click" }
                ,{ name: "inspect" }
                ,{ name: "link" }
            ])
            this._updateEvents();
            if( this.startupTarget ) {
                form.ready().then( lang.hitch( this, function(){
                    this.set( "target", this.startupTarget );
                }))
            }
        }
        
        ,leaveForm: function() {
            this.abandonForm();
            this.inherited( arguments );
        }
        
        ,link: function( name, evt ) {
            var origin = this.parseEventOrigin( evt );
            this.emitEvent( "link", { state:{ link: name, x: origin.clickX, y: origin.clickY } } );
            return false;
        }

        ,_onmousedown: function( evt ) {
            this.closeHint();
            
            if( evt.button == 2 ) {
                this.rightClicking = true;
                setTimeout( lang.hitch( this, function(){
                    delete this.rightClicking;
                }), DRAG_DELAY );
                evt.preventDefault();
            } else {
                this.leftClicking = true;
                setTimeout( lang.hitch( this, function(){
                    delete this.leftClicking;
                }), DRAG_DELAY );
            }
        }

        ,_onmouseup: function( evt ) {
            if( evt.button == 0 && this.leftClicking ) {
                if( this.visible && !this.disabled ) {
                    var origin = this.parseEventOrigin( evt );
                    this.emitEvent( "clicked", { state:{ x: origin.clickX, y: origin.clickY } } );
                }
            } else if( evt.button == 2 && this.rightClicking ) {
                if( this.visible && !this.disabled ) {
                    var origin = this.parseEventOrigin( evt );
                    this.emitEvent( "inspect", { state:{ x: origin.clickX, y: origin.clickY } } );
                }
            }
        }

        ,_updateEvents: function() {
            if( strings.hasText( this.value ) ){
                if( this.eventActive( "click" ) || this.eventActive( "inspect" ) )  {
                    if( this._mouseUpHandler === undefined ) {
                        this._mouseUpHandler = on( this.domNode, "mouseup", lang.hitch( this, this._onmouseup ) )
                    }
                    if( this._mouseDownHandler === undefined ) {
                        this._mouseDownHandler = on( this.domNode, "mousedown", lang.hitch( this, this._onmousedown ) )
                    }
                } else {
                    if( this._mouseUpHandler !== undefined ) {
                        this._mouseUpHandler.remove();
                        delete this._mouseUpHandler;
                    }
                    if( this._mouseDownHandler !== undefined ) {
                        this._mouseDownHandler.remove();
                        delete this._mouseDownHandler;
                    }
                }   
                //this.enableHint();
                domClass[ this.eventActive( "click" ) ? "add" : "remove"]( this.domNode, "worksClickable" );
                domClass[ ! this.eventActive( "click" ) && this.eventActive( "inspect" ) ? "add" : "remove"]( this.domNode, "worksInspectable" );
            } else {
                //this.disableHint();
                domClass.remove( this.domNode, "worksClickable" );
                domClass.remove( this.domNode, "worksInspectable" );
            }
            
        }

        ,_updateLinks: function() {
            if( this.id ) {
                query( "a", this.domNode ).forEach( function( node ){
                    if( node.href && node.href.startsWith( "link:" ) ) {
                        var widgetID = this.id;
                        var link = node.href.substring( 5 );
                        node.onclick = function( evt ) {
                            return widget( widgetID ).link( link, this );
                        }
                        //node.onclick = "return widget('" + this.id + "').link('" + node.href.substring( 5 ) + "', event);"
                        node.href = "#";
                    }
                }, this );
            }
        }

    	,_setValueAttr : function(value){
			this.value = value; 
    		this.labelNode.innerHTML = strings.hasText( this.value ) ? this.value : "&nbsp;";
            this._updateEvents();
            this._updateLinks();
		}
		
		,_getValueAttr : function(){
			return this.value;
    	}
    	
        ,_getValueProperty: function() {
            return htmlEntities.decode( this.value || "" ).replace( new RegExp( "\u00A0", "g" ), " " ); // HTML + nbsp->space
        }
    	
        ,getWidgetState:function(){
            return undefined;
    	}
        
        ,setWidgetState: function() {
            this.inherited( arguments );
            this._updateEvents();
        }
        
        ,_setTargetAttr: function( target ) {
            if( typeof target == "string" ) {
                target = widget( target );
            }
            
            var targetNode = undefined;
            if( target !== undefined && target != null ) {
                if( target.get( "labelTargetNode" ) !== undefined ) {
                    targetNode = target.get( "labelTargetNode" );
                } else if( target.get( "focusNode" ) !== undefined ) {
                    targetNode = target.get( "focusNode" );
                }
            }
            
            if( targetNode ) {
                domAttr.set( this.domNode, "for", targetNode.id );
            } else {
                domAttr.remove( this.domNode, "for" );
            }
            
            this._set( "target", targetNode );
        }

        ,_getHintPositionAttr: function() {
            return ['above-centered', 'below-centered' ];
        }

        ,_setColorAttr: function( value ) {
            domStyle.set( this.domNode, "color", value );
        }

        ,_setBackgroundColorAttr: function( value ) {
            domStyle.set( this.domNode, "backgroundColor", value );
        }

        ,_setFontAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setBorderAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setRoundingAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setShadowAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setNowrapAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "nowrap" );
            this._set( "nowrap", value );
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setReadonlyAttr: function( value ) {
            domClass[ value ? "add" : "remove" ]( this.domNode, "readonly" );
            this._set( "readonly", value );
        }
	 
        ,_setRequiredAttr: function( value ) {
            domClass[ value ? "add" : "remove" ]( this.domNode, "required" );
            this._set( "required", value );
        }

        ,_setDisabledAttr: function( value ) {
            domClass[ value ? "add" : "remove" ]( this.domNode, "disabled" );
            this._set( "disabled", value );
        }
        
        ,_setProblemAttr: function( value ) {
            _ProblemMixin.prototype._setProblemAttr.apply( this, arguments );
        }

        ,validate: function() {
            if( this._validationProblem ) {
                if( this._problemTooltip === undefined ) {
                    this._problemTooltip = new Tooltip({
                        position: this.get( "hintPosition" )
                    });
                }

                this._problemTooltip.set( "label", this._validationProblem );
                this._problemTooltip.addTarget( this.get( "hintTarget" ) );
                
            } else {

                if( this._problemTooltip !== undefined ) {
                    this._problemTooltip.destroy();
                    delete this._problemTooltip;
                }
                
            }

        }

        ,resize: function( size ) {
            if( size ) {
                domGeometry.setMarginBox( this.domNode, this.processFixedSize( size ) );
            }
        }
        
        ,autoCellWidth: false
        
        ,_setAutoCellWidthAttr: function( value ) {
            this._set( "autoCellWidth", this.fixedWidth === undefined ? value : false );
        }

        ,cellResizing: function( parent ) {
            var accept = false;
            if( this.autoWidth || this.autoCellWidth || this.elasticWidth ) {
                this.domNode.style.width = "1px";
                accept = true;
            }
            if( this.elasticHeight ) {
                this.domNode.style.height = "1px";
                accept = true;
            }
            return accept ? this.domNode : undefined;
        }

        ,cellResize: function( size ) {
            if( this.elasticHeight === undefined ) {
                delete size.h;
            }
            this.resize( this.processElasticSize( size ) );
        }
        
    });
    
    Label.markupFactory = _WidgetMixin.markupFactory;
    
    return Label;
});