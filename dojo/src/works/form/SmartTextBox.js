define([
    "require"
    ,"works/lang/declare"
    ,"dojo/on"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"./TextBox"
    ,"dojo/text!./templates/SmartTextBox.html"

    ,"works/style!./resources/SmartTextBox.css"

], function( req, declare, on, domAttr, domClass, TextBox, template ){
    
    return declare( declare.className( req ), [ TextBox ], {
        templateString: template
        
        ,startup: function() {
            this.inherited( arguments );
            this.own(
                on( this, "screen-click", lang.hitch( this, this._onDropButtonClick ) )
            );
        }
        
        ,joinForm: function() {
            this.inherited( arguments );
            this.announceEvent( "clicked", { rename: "click" } ); 
        }
        
        ,leaveForm: function() {
            this.retireEvent( "clicked" );
            this.inherited( arguments );
            
        }
        
        ,_onDropButtonClick: function( evt ) {
            this.emitEvent( "clicked", { state: {} } );
        }
       
    });
    
});