define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/store/Memory"
    ,"dijit/form/ComboBox"
    ,"./_WidgetMixin"
    ,"./_ProblemMixin"
    ,"./_PickMixin"
], function( _require, declare, lang, on, domStyle, domAttr, domClass, Memory, ComboBox, _WidgetMixin, _ProblemMixin, _PickMixin ){
    
    var ComboBox = declare( [ ComboBox, _WidgetMixin, _ProblemMixin, _PickMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,searchAttr: "cap"
        
        ,startup: function() {
            this.inherited( arguments );
            this.own(
                on( this, "change", lang.hitch( this, this._change ) )
                ,on( this, "blur", lang.hitch( this, this._blur ) )
            );
            this._lastValue = this.get( "value" );
        }

        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
        }
        
        ,leaveForm: function( form ) {
            this.retireEvent( "changed" );
            this.inherited( arguments );
        }

        ,_change: function( value ) {
            var current = this.get( "value" );
            if( current != this._lastValue ) {
                this.emitAlways( "changed", {
                    state: {
                        old: this._lastValue
                        ,"new": this.get( "value" )
                    }
                });
                this._lastValue = current;
            }
        }

        ,_blur: function() {
            if( this.get( "value" ) != this._lastValue ) {
                this._change();
            }
        }

		,getWidgetState: function(){
			return {
				value: this.get( "value" )
			}
		}

        ,_setItemsAttr: function( value ) {
            var store = new Memory({ data: value });
            this.set( "store", store );
        }
        
        ,_setReadonlyAttr: function( value ) {
            domAttr[ value == true ? "set" : "remove" ]( this.focusNode, "readonly", "readonly" );
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
        }

        ,_setRequiredAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "required" );
            this._set( "required", false );
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setHorizontalAlignmentAttr: function( value ) {
            domStyle.set( this.textbox, "textAlign", value );
        }
        
        ,_setColorAttr: function( value ) {
            domStyle.set( this.textbox, "color", value );
        }
        
        ,_setBackgroundColorAttr: function( value ) {
            domStyle.set( this.domNode, "backgroundColor", value );
        }
	 
        ,_getDevelopmentDescriptionAttr: function() {
            return lang.mixin( this.inherited( arguments),{
                properties: [ "value", "readonly", "disabled" ]
                ,events: [ "change", "enterPress", "tabPress" ] 
            });
        }
        
    });
    
    ComboBox.markupFactory = _WidgetMixin.markupFactory;
    
    return ComboBox;
    
})