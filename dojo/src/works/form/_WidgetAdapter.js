define([
    "dojo/_base/declare"
    ,"works/registry"
    ,"dijit/_WidgetBase"
    ,"./_FormWidgetMixin"
    ,"./_WidgetMixin"
    ,"./_WidgetLocator"
    
    
], function( declare, registry, _WidgetBase, _FormWidgetMixin, _WidgetMixin, _WidgetLocator ){
    
    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin, _WidgetLocator ], {
        getDescendants: function() {
            return registry.findWidgets( this.domNode );
        }
    });
});