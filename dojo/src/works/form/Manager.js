define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dijit/layout/ContentPane"
    ,"./_WidgetMixin"
    ,"./_FormWidgetMixin"
    ,"./_FormManagerWidget"
    ,"../_LogMixin"
    ,"../_ContentReadyMixin"
    ,"./manager/WidgetRegistry"
    ,"./manager/WidgetLifeCycle"
    ,"./manager/ServerSideLifeCycle"
    ,"./manager/MessageSupport"
    ,"./manager/ReadyStateSupport"
    ,"./manager/EnvironmentSupport"
    ,"./manager/TaskSupport"
    ,"./manager/ErrorManagement"
    
    ,"../style!./resources/FormManager.less"

], function( 
    _require
    ,declare
    ,array
    ,ContentPane
    ,_WidgetMixin
    ,_FormWidgetMixin
    ,_FormManagerWidget
    ,_LogMixin
    ,_ContentReadyMixin
    ,WidgetRegistry
    ,WidgetLifeCycle
    ,ServerSideLifeCycle
    ,MessageSupport
    ,ReadyStateSupport 
    ,EnvironmentSupport 
    ,TaskSupport
    ,ErrorManagement
) {
    
    var Manager = declare([ 
        ContentPane
        ,WidgetRegistry
        ,WidgetLifeCycle
        ,ServerSideLifeCycle
        ,ErrorManagement
        ,MessageSupport
        ,TaskSupport
        ,ReadyStateSupport
        ,EnvironmentSupport
        ,_ContentReadyMixin
        ,_LogMixin
        ,_FormWidgetMixin
        ,_FormManagerWidget
    ], {

        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,autoSize: false
        ,isFormManager: true
        
        ,resize: function( size ){
            this.inherited( arguments );
            this.domNode.style.width = null;
            this.domNode.style.height = null;
        }
        
        ,_layoutChildren: function(){
            this.inherited( arguments );
            if( ! ( this._singleChild && this._singleChild.resize ) ) {
                // Propaga il resize evitando ai widget di contenere un resize()
                // per non falsare il _singleChild del ContentPane
                array.forEach( this.getChildren(), function( widget ){
                    if( lang.isFunction( widget.doResize ) ) {
                        widget.doResize();
                    }
                })
            }
        }
        
        ,getActualForm: function() {
            return this;
        }
    });

    Manager.markupFactory = _WidgetMixin.markupFactory;
    
    return Manager;
})