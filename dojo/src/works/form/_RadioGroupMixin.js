define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/on"
    ,"dijit/layout/ContentPane"
    ,"works/form/RadioButton"
    ,"./_WidgetMixin"
    ,"./_FormWidgetMixin"
], function( _require, declare, lang, array, on, ContentPane, RadioButton, _WidgetMixin, _FormWidgetMixin ){
    
    return declare( [ ContentPane, _WidgetMixin, _FormWidgetMixin ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

        ,_changed: function( value ) {
            if( value ) {
                var newValue = this.get( "value" );
                this.emitAlways( "changed", {
                    state: {
                        old: this.lastValue
                        ,"new": newValue
                    }
                })
            }
            this.lastValue = newValue;
        }
        
        ,_getValueAttr: function() {
            var checkeds = array.filter( this.buttons, function( item ){ return item.get( "checked" ) } );
            if( checkeds.length > 0 ) {
                return checkeds[ 0 ].get( "itemValue" );
            } else {
                return null;
            }
        }
        
        ,_setValueAttr: function( value ) {
            array.forEach( this.buttons, function( item ){
                item.set( "checked", item.get( "itemValue" ) == value );
            });
        }
        
		,getWidgetState: function(){
			return {
				value: this.get( "value" )
			}
		}

    });
    
});