define([
    'require'
	,'dojo/_base/declare'
    ,"dojo/_base/window"
    ,"dojo/_base/lang"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/dom"
    ,"dojo/has"
    ,"dojo/query"
    ,"dojo/keys"
    ,"works/registry"
    ,"dijit/popup"
	,'dijit/TooltipDialog'
    ,'works/_ContentReadyMixin'
    ,'./_PopupMixin'

    ,"../style!./resources/Popup.less"
    ,"dojo/domReady!"

], function( _require, declare, win, lang, domGeometry, domStyle, domConstruct, on, topic, dom, has, query, keys, registry, popup, TooltipDialog, _ContentReadyMixin, _PopupMixin ){
	
	return declare( [ TooltipDialog, _PopupMixin, _ContentReadyMixin ], {
        hideRefusalDelay: 500
        
        ,autoClose: true
        ,_acceptHide: true
        
        ,startup: function() {
            // Come per la Dialog, il nodo va fuori dalla form
            win.body().appendChild( this.domNode );
            
            this.startupPopup();
            this.inherited( arguments );
            this.own( 
                topic.subscribe( "form/resize/global", lang.hitch( this, this.resize ) )
            );
        }
        
        ,openAround: function( target ) {
            
            if( target != null ) {
                this._opened = true;
                
                var params = { 
                    popup: this
                };
                
                if( lang.isFunction( target.focus ) ) {
                    params.around = target;
                } else if( target.x !== undefined && target.y !== undefined ) {
                    params.x = target.x;
                    params.y = target.y;
                } else {
                    throw new Error( "Unknown target: " + target );
                }

                if( popup.getTopPopup() !== undefined ) {
                    params.parent = popup.getTopPopup().widget;
                }
                    
                popup.open( params );
                
                if( this.hideRefusalDelay > 0 ) {
                    this._acceptHide = false;
                    setTimeout( lang.hitch( this, function(){
                        this._acceptHide = true;
                    }), this.hideRefusalDelay );

                } else {
                    this._acceptHide = true;
                }

                this.focus();
                this.handlers = [
                    // Replaced by underlay:
                    // on( this, "blur", lang.hitch( this, this.onPopupBlur ) ),
                    
                    on( this, "hide", lang.hitch( this, this.onPopupHide ) )
                    ,on( win.body(), "keydown", lang.hitch( this, this.onKeyDown ) )
                    ,topic.subscribe( "mouse/scroll", lang.hitch( this, this.onMouseScroll ) )
                ];
            }
            
        }
        
        ,destroy: function() {
            domConstruct.destroy( this.underlay() );
            this.cleanup();
            this.inherited( arguments );
            
        }
        ,_position: function() {
            return domGeometry.position( this.domNode );
        }
        
        ,onKeyDown: function( evt ) {
            if( evt.keyCode == keys.ESCAPE && this._acceptHide && this.autoClose == true ) {
                this.hide();
            }
        }
        
        ,onMouseScroll: function( evt ) {
            if( this._acceptHide && this.autoClose == true && this._inspecting != true ) {
                var pos = domGeometry.position( this.domNode );

                if( evt.x < pos.x 
                    || evt.x > pos.x + pos.w 
                    || evt.y < pos.y 
                    || evt.y > pos.y + pos.h ) {
                    
                    this.hide();
                }
            }
        }
        
        ,onPopupBlur: function( evt ) {
            if( this._acceptHide ) {
                if( this.autoClose == true && this._inspecting != true ) {
                    this.hide();
                }
            } else {
                this.domNode.focus();
            }
        }

        ,onPopupHide: function() {
            if( this._opened ) {
                this._opened = false;
                setTimeout( lang.hitch( this, function(){
                    if( this._destroyed != true ) {
                        this.emit( "hidden", { bubbles: false } );
                        this.cleanup();
                    }
                }), 0 );
            }

            domStyle.set( this.underlay(), {
                display: "none"
            });
        }
        
        ,hide: function() {
            if( this._opened ) {
                this._opened = false;
                this.emit( "hidden", { bubbles: false } );
                popup.close( this );
                this.cleanup();
            }

            domStyle.set( this.underlay(), {
                display: "none"
            });
        }
        
        ,cleanup: function() {
            if( this.handlers !== undefined ) {
                for( var i = 0; i < this.handlers.length; i++ ) {
                    this.handlers[ i ].remove();
                }
                delete this.handlers;
            }
            delete this._inspecting;
        }
        
        ,_getAnchorWidgetAttr: function() {
            return lang.isString( this.anchor ) ? this.parentForm.getMainForm().widget( this.anchor ) : this.anchor;
        }
        
        ,show: function() {
            var anchorWidget = this.get( "anchorWidget" );

            if( anchorWidget !== undefined ) {
                var anchorNode = undefined;

                if( lang.isFunction( anchorWidget.isInstanceOf ) ) {
                    if( lang.isFunction( widget.getAnchorNode ) ) {
                        anchorNode = widget.getAnchorNode();
                    } else if( anchorWidget.domNode !== undefined ) {
                        anchorNode = anchorWidget.domNode;
                    }
                } else {
                    // domGeometry.setMarginBox( fakeAnchor, { l: anchorWidget.x, t: anchorWidget.y } );
                    // anchorNode = fakeAnchor;
                    anchorNode = anchorWidget;
                }
        
                if( anchorNode !== undefined ) {
                    this.openAround( anchorNode );
                    
                    var zIndex = parseInt( this.domNode.parentNode.style.zIndex );

                    domStyle.set( this.underlay(), {
                        display: "block"
                        ,zIndex: zIndex - 1
                    });
                    
                    this.resize();
                    
                } else {
                    throw new Error( "Undefined anchor node for path: " + this.anchor );
                }
    
            } else {
                throw new Error( "Invalid anchor path: " + this.anchor );
            }
            
        }
        
        ,inspect: function() {
            this._inspecting = true;
            this.show();
        }
        
        ,release: function() {
            this._inspecting = false;
            this.hide();
        }

        ,_setPaddingAttr: function( value ) {
            domStyle.set( this.containerNode.parentNode, "padding", value );
        }

        ,underlay: function() {
            var id = this.id + "_TooltipUnderlay";
            var underlay = dom.byId( id );

            if( underlay == null || underlay === undefined ) {
                underlay = domConstruct.create( "div", { 
                    id: id
                    ,style: {
                        width: "100%"
                        ,height: "100%"
                        ,position: "absolute"
                        ,top: "0px"
                        ,left: "0px"
                        ,backgroundColor: "white"
                        ,opacity: "0.01"
                    }
                }, document.body );
                
                underlay[ has( "mobile" ) ? "ontouchstart" : "onclick" ] = lang.hitch( this, function( evt ) {
                    evt.preventDefault();
                    this.onPopupBlur();
                });
            }
            
            return underlay;
        }

        ,resize: function( size ) {
            size = size || this.getRequestedSize();
            
            if( size !== undefined ) {
                var borders = domGeometry.getPadBorderExtents( query( ".dijitTooltipContainer", this.domNode )[0] );
                query( ".dijitTooltipContents", this.domNode ).style({ height: ( size.h - borders.h ) + "px", width: ( size.w - borders.w ) + "px" });
            }
            
            this.inherited( arguments, size );
        }
	});

});
