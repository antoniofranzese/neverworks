define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-class"
    ,"dijit/form/CheckBox"
    ,"./_WidgetMixin"
], function( _require, declare, lang, on, has, domClass, CheckBox, _WidgetMixin ){
    
    return declare( [ CheckBox, _WidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,readonly: false
        ,visible: true
        ,disabled: false
        
        ,startup: function(){
            this.inherited( arguments );

            this.own(
                on( this, "change", lang.hitch( this, this._changed ) )
            );

            if( has( "mobile" ) ) {
                this.own(
                    on( this.domNode, "touchstart", lang.hitch( this, this.onTouchStart ) )
                    ,on( this.domNode, "touchend", lang.hitch( this, this.onTouchEnd ) )
                );
            }
        }

        ,onTouchStart: function( evt ) {
            this.touchStartX = evt.clientX;
        }

        ,onTouchEnd: function( evt ) {
            var delta = evt.clientX - this.touchStartX;
            if( Math.abs( delta ) > 5 ) {
                this.set( "value", delta > 0 );
            }
            delete this.touchStartX;
        }

        ,joinForm: function() {
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
        }
        
        ,leaveForm: function() {
            this.retireEvent( "changed" );
            this.inherited( arguments );
        }

		,_onClick: function(){
            if( ! this.readonly ) {
                return this.inherited( arguments );
            }
		}

        ,_changed: function( value ) {
            this.emitAlways( "changed", { state: { "new": value, "old": !value } } );
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setReadonlyAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "readonly" );
            this._set( "readonly", value );
        }

        ,getWidgetState: function() {
            if( this.visible && ! this.readonly && ! this.disabled ) {
                return {
                    value: this.get( "checked" )
                }
            }
        }
        
        ,_getValueAttr: function() {
            return this.get( "checked" );
        }

        ,_setValueAttr: function( value ) {
            if( value != undefined && value != null ) {
                this.set( "checked", value );
            } else {
                this.set( "checked", false );
            }
        }
    });
    
});