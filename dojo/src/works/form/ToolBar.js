define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/dom-class"
    ,"dojo/dom-attr"
    ,"dojo/dom-geometry"
    ,"dojo/query"
    ,"works/registry"
    ,"dijit/_WidgetBase"
    ,"dijit/form/Button"
    ,"./_FormWidgetMixin"
    ,"./_WidgetMixin"
    ,"./_WidgetLocator"
    ,"../layout/_ChildrenVisibilityMixin"
    ,"../layout/_ParentQualifyMixin"
    ,"../layout/_ViewportMixin"
    ,"../_ContentReadyMixin"
    
], function( _require, declare, array, domClass, domAttr, domGeometry, query, registry, _WidgetBase, Button, 
        _FormWidgetMixin, _WidgetMixin, _WidgetLocator, _ChildrenVisibilityMixin, _ParentQualifyMixin, _ViewportMixin, _ContentReadyMixin ){
    
    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin, _WidgetLocator, _ChildrenVisibilityMixin, _ContentReadyMixin, _ParentQualifyMixin, _ViewportMixin ], {

        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,justify: "none"
        
        ,startup: function() {
            this.inherited( arguments );
            this.contentReady().then( lang.hitch( this, function(){
                this.registerChildrenVisibilityChanges( function( widget, value ){
                    var widgetNode = widget.domNode;
                    if( widgetNode && widgetNode.parentNode && widgetNode.parentNode.tagName == "TD" ) {
                        domClass[ value == true ? "remove" : "add" ]( widgetNode.parentNode, "hidden" );
                    }
                });
            }));
        }
        
        ,destroy: function() {
            this.unregisterChildrenVisibilityChanges();
            this.inherited( arguments );
        }
        
        ,resize: function() {
            if( this.justify != "none" ) {
                var size = { w: 0, h: 0 };
                var buttons = this.getButtons();
                array.forEach( buttons, function( button ){
                    var mb = domGeometry.getMarginBox( button.domNode );
                    if( mb.w > size.w ) {
                        size.w = mb.w;
                    }
                    if( mb.h > size.h ) {
                        size.h = mb.h;
                    }
                });
                
                if( this.justify == "width" || size.h == 0 ) {
                    delete size.h;
                } else if( this.justify == "height" || size.w == 0) {
                    delete size.w;
                }
                
                if( size.h > 0 || size.w > 0 ) {
                    array.forEach( buttons, function( button ){
                        domGeometry.setMarginBox( button.domNode, size );
                    });
                }
            }
            this.inherited( arguments );
        }

        ,getDescendants: function() {
            return registry.findWidgets( this.domNode );
        }
        
        ,getButtons: function() {
            return array.filter( this.getDescendants(), function( widget ){
                return widget.isInstanceOf( Button );
            });
        }

        ,getContainerCell: function() {
            return query( ".worksToolBarContent", this.domNode )[0].parentNode;
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setCollapsibleAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "collapsible" );
        }

        ,_setHalignAttr: function( value ) {
            if( this.domNode ) {
                domClass.removeAll( this.domNode, /halign-.*/ );
                domClass.add( this.domNode, "halign-" + value );
                if( value ) {
                    domAttr.set( this.getContainerCell(), "align", value );
                } else {
                    domAttr.remove( this.getContainerCell(), "align" );
                }
            }
            this._set( "halign", value );
        }
        
        ,_setValignAttr: function( value ) {
            if( this.domNode ) {
                domClass.removeAll( this.domNode, /valign-.*/ );
                domClass.add( this.domNode, "valign-" + value );
                if( value ) {
                    domAttr.set( this.getContainerCell(), "valign", value );
                } else {
                    domAttr.remove( this.getContainerCell(), "valign" );
                }
            }
            this._set( "halign", value );
        }
        
        ,_setBorderAttr: function( value ) {
            if( this.domNode ) {
                domStyle.set( this.domNode, value );
            }
        }

        ,_setBackgroundAttr: function( value ) {
            if( this.domNode ) {
                domStyle.set( this.domNode, value );
            }
        }

    });
});