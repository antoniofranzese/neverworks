define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"works/_base/click"
    ,"works/lang/strings"
    ,"dojo/Deferred"
    ,"dijit/_WidgetBase"
    ,"dijit/_TemplatedMixin"
    ,"dijit/_Contained"
    ,"works/dnd/Source"
    ,"works/form/_FormWidgetMixin"
    ,"works/form/_WidgetMixin"
    ,"works/layout/_FixedSizeMixin"
    
    ,"dojo/text!./templates/Image.html"
], function( req, declare, lang, on, has, domStyle, domGeometry, domClass, click, strings, Deferred, _WidgetBase, _TemplatedMixin, _Contained, Source, _FormWidgetMixin, _WidgetMixin, _FixedSizeMixin, template ){
    
    var BLANK = req.toUrl( req.toAbsMid( "./resources/blank.gif" ) );
    var DRAG_DELAY = lang.getObject( "worksConfig.dragDelay" );
    var DOUBLE_CLICK_DELAY = lang.getObject( "worksConfig.doubleClickDelay" );
    var TOUCH_INSPECT_DELAY = lang.getObject( "worksConfig.touchInspectDelay" );
    var TOUCH_DRAG_TRESHOLD = lang.getObject( "worksConfig.touchDragTreshold" );
    
    return declare( declare.className( req ), [ _WidgetBase, _TemplatedMixin, _WidgetMixin, _FormWidgetMixin, _FixedSizeMixin ], {

        baseClass: "worksImage"
        ,templateString: template
        ,value: "null:"
        ,visible: true
        ,disabled: false
        ,completed: false
        ,blank: false
	
        ,buildRendering: function() {
            this.inherited( arguments );
            this.domNode.oncontextmenu = function(){ return false; };
            this.imageNode.addEventListener( 'load', lang.hitch( this, function(){
                this._completeImage( false );
            }));
            this.imageNode.addEventListener( 'error', lang.hitch( this, function(){
                this._completeImage( true );
            }));

            if( has( "mobile" ) ) {
                this.domNode.ontouchstart = lang.hitch( this, this.onTouchStart );
                this.domNode.ontouchend = lang.hitch( this, this.onTouchEnd );
                this.domNode.ontouchmove = lang.hitch( this, this.onTouchMove );
            } else {
                this.domNode.onmousedown = lang.hitch( this, this.onMouseDown );
                this.domNode.onmouseup = lang.hitch( this, this.onMouseUp );
                this.domNode.onclick = lang.hitch( this, this.onMouseClick );
            }     

        }
    
        ,postMixInProperties: function() {
            this.inherited( arguments );
            this.parseSource( this.value );
        }

        ,parseSource: function( source ) {
            this.blank = false;
            if( lang.isObject( source ) ) {
                source = ( source.type ? source.type : "null" ) + ":" + ( source.content ? source.content : "" );
            }
            if( source.startsWith( "file:" ) ) {
                this.sourceFile = source.substring( 5 );
                this.sourceClass = "";
            } else if( source.startsWith( "class:" ) ) {
                this.sourceFile = BLANK;
                this.sourceClass = source.substring( 6 );
            } else {
                this.sourceFile = BLANK;
                this.blank = true;
            }
        }

        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.form = form;
            this.announceEvents([
                { name: has( "mobile" ) ? "touch" : "clicked", rename: "click" }
                ,{ name: "inspect" }
                ,{ name: "launch" }
            ]);

            //console.log( this.id, "completed on startup", this.imageNode.complete );
            if( this.imageNode.complete != true ) {
                this.completeDeferred = new Deferred();
                this.form.ready().enlist( this.completeDeferred, { reason: "Image completion" } );
            } else {
                this._completeImage( false );
            }
        }
        
        ,leaveForm: function() {
            this.abandonForm();
            delete this.form;
            this.inherited( arguments );
        }

        ,onMouseDown: function( evt ) {
            if( evt.button == 2 ) {
                this.rightClicking = true;
                setTimeout( lang.hitch( this, function(){
                    delete this.rightClicking;
                }), 500 );
            } else {
                this.clicking = true;
            }
        }

        ,onMouseUp: function( evt ) {
            if( evt.button == 2 && this.rightClicking ) {
                if( this.visible && !this.disabled ) {
                    var origin = this.parseEventOrigin( evt );
                    this.emitEvent( "inspect", { state:{ x: origin.clickX, y: origin.clickY } } );
                }
            }
        }
        
        ,onMouseClick: function( evt ) {
            evt.stopPropagation();

            if( this.clicking ) {
                this.clicking = false;

                if( this.visible && !this.disabled ) {
                    var origin = this.parseEventOrigin( evt );
                    this.emitEvent( "clicked", { state:{ x: origin.clickX, y: origin.clickY } } );
                }
            }
        }

        ,onTouchStart: function( evt ) {
            this.touching = true;
            evt.preventDefault();                
            
            this.touchStartPoint = click.point( evt );
            this.inspectTimeout = setTimeout( lang.hitch( this, function(){
                delete this.inspectTimeout;
                
                if( this.touching ) {
                    this.touching = false;
                    this.emitEvent( "inspect", {
                        state: this.touchStartPoint
                        ,changedTouches: evt.changedTouches
                        ,widget: this }
                    );
                }

            }), TOUCH_INSPECT_DELAY );
        }
        
        ,onTouchMove: function( evt ) {
            var distance = click.distance( this.touchStartPoint, evt );
            if( distance >= TOUCH_DRAG_TRESHOLD ) {
                this.touching = false;
            }
        }

        ,onTouchEnd: function( evt ) {
            if( this.inspectTimeout ) {
                clearTimeout( this.inspectTimeout );
                delete this.inspectTimeout;
            }

            if( this.touching ) {
                this.touching = false;
                //console.log( "TOUCH" );
                this.emitEvent( "touch", {
                    state: this.touchStartPoint
                    ,changedTouches: evt.changedTouches
                    ,widget: this }
                );
            }
            
        }
        
        ,updateEvent: function( evt ) {
            if( evt.name == "clicked" ) {
                if( evt.active ) {
                    domClass[ evt.active ? "add" : "remove"]( this.domNode, "worksClickable" );
                }
            }
        }
        
        ,resize: function( size ) {
            domGeometry.setMarginBox( this.domNode, this.processFixedSize( size ) );
            this.set( "zoom", this.zoom );
        }

        ,getWidgetState: function() {
            return undefined;
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setValueAttr: function( value ) {
            this.completed = false;
            domStyle.set( this.domNode, "visibility", "hidden" );
            domClass.remove( this.domNode, "worksImageError" );

            if( this.sourceClass != "" ) {
                domClass.remove( this.imageNode, this.sourceClass )
            }
            this.value = value;
            this.parseSource( value );

            this.imageNode.src = this.sourceFile;
            this.imageNode.style.display = "block";
            domClass.add( this.imageNode, this.sourceClass );
            
            if( this.imageNode.complete ) {
                this._completeImage( false );
            }
            
        }
        
        ,_completeImage: function( error ) {
            this.completed = true;
            if( error ) {
                domClass.add( this.domNode, "worksImageError" );
            } else {
                this.set( "zoom", this.zoom );
            }
            domStyle.set( this.domNode, "visibility", has( "ie11" ) ? "" : null );

            if( this.completeDeferred ) {
                //console.log( this.id, "deferred complete" );
                this.completeDeferred.resolve();
                delete this.completeDeferred;
            }
        }
        
        ,_setDisabledAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "disabled" );
            this._set( "disabled", value );
        }
        
        ,_setProposalAttr: function( value ) {
            if( value ) {
                if( this._dndSource === undefined ) {

                    this._dndSource = new Source({
                        avatar: lang.hitch( this, function(){ 
                            return typeof this.avatar == "string" ? widget( this.avatar ) : this.avatar
                        })
                        ,proposal: lang.hitch( this, function(){ 
                            return {
                                source: this.form.getWidgetQualifiedName( this )
                                ,content: this.proposal 
                            }
                        }) 
                        ,timeout: DRAG_DELAY
                        ,onDragStart: lang.hitch( this, function() {
                            this.clicking = false;
                        })
                    }, this.imageNode );
                }
                
            } else if( this._dndSource ) {
                this._dndSource.destroy();
                delete this._dndSource;
            }
            
            this._set( "proposal", value )
        }
        
        ,_setMaxWidthAttr: function( value ) {
            domStyle.set( this.imageNode, "max-width", value );
        }

        ,_setMaxHeightAttr: function( value ) {
            domStyle.set( this.imageNode, "max-height", value );
        }

        ,_setWidthAttr: function( value ) {
            domStyle.set( this.domNode, "width", value );
        }

        ,_setHeightAttr: function( value ) {
            domStyle.set( this.domNode, "height", value );
        }

        ,_setScrollableAttr: function( value ) {
            domClass.removeAll( this.domNode, /scroll-.*/ );
            if( value ) {
                domClass.add( this.domNode, "scroll-" + value )
            }
            this._set( "scrollable", value );
        }

        ,_setZoomAttr: function( value ) {
            if( this.completed && ! this.blank && value != null && value !== undefined ) {
                domStyle.set( this.imageNode, {
                    "max-width": null 
                    ,"max-height": null 
                    ,"width": null 
                    ,"height": null 
                });
            
                var imageBox = domGeometry.getMarginBox( this.imageNode );
                
                if( this.blank ) {
                    
                } else if( value == "fit" ) {
                    var domBox = domGeometry.getMarginBox( this.domNode );
                    var hRatio = domBox.w / imageBox.w;
                    var vRatio = domBox.h / imageBox.h;

                    if( hRatio < vRatio ) {
                        domStyle.set( this.imageNode, "max-width", "100%" );
                    } else {
                        domStyle.set( this.imageNode, "max-height", "100%" );
                    }

                } else {
                    domStyle.set( this.imageNode, "width", ( imageBox.w * value / 100 ) + "px" );
                }
            }
            
            this._set( "zoom", value );
        }
        
        ,_setBackgroundAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setRoundingAttr: function( value ) {
            domStyle.set( this.imageNode, value );
        }

        ,_setShadowAttr: function( value ) {
            domStyle.set( this.imageNode, value );
        }

    });

});
