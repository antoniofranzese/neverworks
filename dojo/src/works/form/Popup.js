define([
    'require'
	,'dojo/_base/declare'
    ,"dojo/_base/lang"
    ,"dojo/window"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"works/lang/strings"
	,'dijit/Dialog'
    ,"dojox/layout/ResizeHandle"
    ,'works/_ContentReadyMixin'
    ,"works/dev/PickTarget"
    ,'./_PopupMixin'

    ,"../style!./resources/Popup.less"
    ,"../style!works/layout/resources/ResizeHandle.css"

], function( _require, declare, lang, win, on, domClass, domConstruct, domStyle, domGeometry, strings, Dialog, ResizeHandle, _ContentReadyMixin, PickTarget, _PopupMixin ){
	
	return declare( [ Dialog, _PopupMixin, _ContentReadyMixin ], {
        
        resizable: false
        ,minWidth: 300
        ,minHeight: 200
        ,maxWidth: null
        ,maxHeight: null
        ,referenceWidth: null
        ,referenceHeight: null
        ,width: null
        ,height: null
        ,_firstResize: true
        ,title: "&nbsp;"
        
        ,buildRendering: function() {
            this.inherited( arguments );

            this.setMaximumSize();

            if( this.resizable ) {
                this.resizeNode = domConstruct.create( "span", { "class": "dojoxPopupResizeHandle" }, this.domNode );
            }

        }
        
        ,setMaximumSize: function() {
            var box = win.getBox();
            
            if( this.referenceWidth == null ) {
                this.referenceWidth = this.maxWidth != null ? this.maxWidth : this.width;
            }
            if( this.referenceHeight == null ) {
                this.referenceHeight = this.maxHeight != null ? this.maxHeight : this.height;
            }

            if( typeof this.referenceWidth == "string" ) {
                this.referenceWidth = domStyle.toPixelValue( this.domNode, this.referenceWidth );
            }
            if( typeof this.referenceHeight == "string" ) {
                this.referenceHeight = domStyle.toPixelValue( this.domNode, this.referenceHeight );
            }

            if( this.referenceWidth ) {
                this.domNode.style.width = Math.min( box.w - 100, this.referenceWidth ) + "px";
            } else if( this.width ) {
                this.domNode.style.width = this.width;
            }
            
            if( this.referenceHeight ) {
                this.domNode.style.height = Math.min( box.h - 100, this.referenceHeight ) + "px";
            } else if( this.height ) {
                this.domNode.style.height = this.height;
            }
        }
        
        ,startup: function() {
            this.startupPopup();
            this.inherited( arguments );
            this.own(
                on( this.closeButtonNode, "mousedown", lang.hitch( this, function(){
                    domClass.remove( this.closeButtonNode, "dijitDialogCloseIconHover" )
                    domClass.add( this.closeButtonNode, "dijitDialogCloseIconActive" )
                }))
                ,on( this.closeButtonNode, "mouseup", lang.hitch( this, function(){
                    domClass.remove( this.closeButtonNode, "dijitDialogCloseIconActive" )
                    domClass.add( this.closeButtonNode, "dijitDialogCloseIconHover" )
                }))
                ,on( this, "show", lang.hitch( this, function(){
                    if( this._firstResize == true ) {
                        this._firstResize = false;
                        if( this.domNode ) {
                            this.resize();
                        }
                    }
                }))
                ,on( this, "hide", lang.hitch( this, function(){
                    this.emit( "hidden", { bubbles: false });
                }))
            );
			
            if( this.resizable ) {
                this._resizeHandle = new ResizeHandle({
                    targetId: this.id,
                    resizeAxis: "xy"
                }, this.resizeNode );
                
                var borders = domGeometry.getPadBorderExtents( this.domNode );
                this._resizeHandle.minSize = { 
                    w: this.minWidth - borders.w, 
                    h: this.minHeight - borders.h 
                };
                
            }
            
        }
        
        ,notifyMessage: function( message ) {
            if( message.topic = "works/pick/init" ) {
                if( this._pickTarget === undefined ) {
                    this._pickTarget = new PickTarget( this, this.titleBar );
                    this.own({
                        destroy: lang.hitch( this, function() {
                            this._pickTarget.destroy();
                            delete this._pickTarget;
                        })
                    });
                }
            } else {
                this.inherited( arguments );
            }
        }
        
        ,resize: function( size ) {
            if( this.domNode ) {
                if( size !== undefined && this.resizable ) {
                    this.width = Math.round( size.w ) + "px";
                    this.height = Math.round( size.h ) + "px";
                    this.domNode.style.width = this.width;
                    this.domNode.style.height = this.height;
                } else if( size === undefined ) {
                    this.setMaximumSize();
                }

                if( strings.hasText( this.domNode.style.height ) ) {
                    var dialogHeight = domStyle.toPixelValue( this.domNode, this.domNode.style.height );
                    var titleHeight = domGeometry.getMarginBox( this.titleBar ).h;
            
                    var contentBox = domGeometry.getMarginBox( this.containerNode );

                    var pads = domGeometry.getPadBorderExtents( this.containerNode );
                    var height = dialogHeight - titleHeight - pads.h;
                    domGeometry.setMarginBox( this.containerNode, { h: height } );

                    var width = contentBox.w - pads.w;

                    if( this.form ) {
                        this.form.resize({
                            h: height 
                            ,w: width
                        });
                    }
                }

                this.inherited( arguments );
            }
            
        }
        
        ,_setMinWidthAttr: function( w ) {
            if( typeof w == "string" ) {
                w = domStyle.toPixelValue( this.domNode, w );
            }
            this._set( "minWidth", w );
            if( this._resizeHandle !== undefined ) {
                this._resizeHandle.minWidth = w;
            }
        }
        
        ,_setMinHeightAttr: function( h ) {
            if( typeof h == "string" ) {
                h = domStyle.toPixelValue( this.domNode, h );
            }
            this._set( "minHeight", h );
            if( this._resizeHandle !== undefined ) {
                this._resizeHandle.minHeight = h;
            }
        }

        ,_setPaddingAttr: function( value ) {
            domStyle.set( this.containerNode.parentNode, "padding", value );
        }
        
	});

});