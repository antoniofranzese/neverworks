define([
    "require"
    ,"works/lang/declare"
    ,"works/form/TextBox"

    ,"dojo/text!./resources/MobileTextBox.html"
    ,"works/style!./resources/MobileTextBox.css"
], function( req, declare, TextBox, template ){
    
    return declare( declare.className( req ), [ TextBox ], {
        type: "text"
        ,iconClass: "hidden"
        ,templateString: template
        
        ,onIconClick: function() {
            this.focusNode.focus();
        }
        
    });
    
});