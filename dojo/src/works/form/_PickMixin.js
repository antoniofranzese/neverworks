define([
    "require"
    ,"dojo/_base/declare"
    ,"works/dev/PickManager"
    ,"works/dev/PickTarget"
], function( _require, declare, PickManager, PickTarget ){
    
    return declare( [], {
        
        notifyMessage: function( message ) {
            if( message.topic == "works/pick/init" ) {
                if( this._pickTarget === undefined ) {
                    this._pickTarget = new PickTarget( this, this.domNode );
                    this.own({
                        destroy: lang.hitch( this, function() {
                            this._pickTarget.destroy();
                            delete this._pickTarget;
                        })
                    });
                }
            } else {
                this.inherited( arguments );
            }
        }
    });
    
})