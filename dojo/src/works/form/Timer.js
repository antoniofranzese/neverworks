define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dijit/_WidgetBase"
    ,"./_FormWidgetMixin"
    ,"./_WidgetMixin"
    
], function( _require, declare, array, lang, _WidgetBase, _FormWidgetMixin, _WidgetMixin ){
    
    return declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

        ,name: ""
        ,interval: 0
        ,startupRunning: false
        ,concurrent: false
        ,_running: false
        ,_ticking: false
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "tick" );
            form.ready().then( lang.hitch( this, function(){
                this.set( "running", this.startupRunning ); 
            }));
        }
        
        ,leaveForm: function() {
            this.retireEvent( "tick" );
            this.clearSchedule();
            this.inherited( arguments );
        }
        
        ,clearSchedule: function() {
            if( this._timeout !== undefined ) {
                clearTimeout( this._timeout );
                delete this._timeout;
            }
        }
        
        ,scheduleNext: function() {
            this.clearSchedule();
            if( this._running ) {
                //console.log( "Scheduling" );
                this._timeout = setTimeout( lang.hitch( this, this.tick ), this.interval * 1000 );
            }
        }

        ,tick: function() {
            this.set( "running", false );
            this.emitAlways( "tick", { state: {} } );
        }
        
        ,start: function() {
            if( !this._running ) {
                this._running = true;
                this.clearSchedule();
                //console.log( "Starting" )
                this.scheduleNext();
            }
        }
        
        ,stop: function() {
            if( this._running ) {
                this._running = false;
                this._ticking = false;
                this.clearSchedule();
            }
        }
        
        ,getWidgetState: function() {
            return undefined;
        }
        
        ,_getRunningAttr: function() {
            return this._running;
        }
        
        ,_setRunningAttr: function( value ) {
            if( value == true ) {
                this.start();
            } else {
                this.stop();
            }
        }
        
        ,setWidgetState: function( state ) {
            var running;
            if( "running" in state ) {
                running = state.running;
                delete state.running;
            }
            
            this.inherited( arguments );
            
            if( running !== undefined ) {
                this.set( "running", running );
            }
        }
        
    });

});