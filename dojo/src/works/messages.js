define([
    "dojo/_base/lang"
    ,"dojo/aspect"
    ,"dojo/has"
    ,"works/lang/Enrollment"

    ,"works/_base/os"
], function( lang, aspect, has, Enrollment ) {

    var electron = has( "electron" );

    var here = function() {
        return "[" + location.href + "]";
    }
    
    var RegisteredListeners = {};
    
    var MessageListener = function( evt ) {
        var msg = evt.data;
        //console.log( here(), "message:", msg );
        
        if( msg !== undefined 
            && msg.sender == "NW3" 
            && msg.action !== undefined 
            && msg.action in RegisteredListeners ) {
            
            RegisteredListeners[ msg.action ].forEach( function( listener ){
                listener( msg, { source: evt.source } );
            });
        }
    }

    if( electron ) {
        electron.ipc.on( 'NW3::IPC', function( evt, args ) {
            MessageListener({
                source: evt.sender
                ,data: lang.mixin( { sender: "NW3" }, args )
            });
        });

    } else {
        if( window.addEventListener ){
            window.addEventListener( "message", MessageListener, false );
        } else {
            window.attachEvent( "onmessage", MessageListener );
        }

    }

    return {
        on: function( action, listener ) {
            if( lang.isFunction( listener ) ) {
                if( !( action in RegisteredListeners ) ) {
                    RegisteredListeners[ action ] = new Enrollment();
                }
                return RegisteredListeners[ action ].add( listener );
            } 
        }
        
        ,bubble: function( action, message ) {
            
            var msg = lang.mixin( {}, message, { 
                sender: "NW3"
                ,action: action 
            });
            
            //console.log( here(), "bubbling:", msg );

            if( electron ) {
                electron.ipc.sendToHost( "NW3::IPC", msg );

            } else if( window.parent !== window ) {
                
                window.parent.postMessage( msg, "*" );
            }
        }
        
        ,send: function( target, action, message ) {
            var msg = lang.mixin( {}, message, { 
                sender: "NW3"
                ,action: action 
            });

            //console.log( here(), "sending:", msg );

            if( lang.isFunction( target.postMessage ) ) {
                target.postMessage( msg, "*" );
            
            } else if( lang.isFunction( target.send ) ) {
                target.send( msg );
            }
        }
    }

});