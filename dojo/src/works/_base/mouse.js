define([
    "require"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"dojo/topic"
    ,"dojo/aspect"
], function( _require, lang, has, topic, aspect  ){
    
	var publishScrollEvent = function( evt ){

		var parameters = { x: Math.floor( evt.clientX ), y: Math.floor( evt.clientY ) };
		topic.publish( "mouse/scroll", parameters );
	}

	/** IE/Opera. */
	if (window.addEventListener){
	    /** DOMMouseScroll is for mozilla. */
	    window.addEventListener('DOMMouseScroll', publishScrollEvent, false);
    
		/** mousewheel is for safari, google chrome. */
		window.addEventListener('mousewheel', publishScrollEvent, false);
        
   	} else if( has( "ie" ) ){
		aspect.after( window, "onmousewheel", publishScrollEvent );
		aspect.after( document, "onmousewheel", publishScrollEvent );

	}

    return {}
    
});