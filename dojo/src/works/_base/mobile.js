define([
    "dojo/_base/window",
    "dojo/has",
    "dojo/topic",
    "dojo/on",
    "dojo/dom-class",
    "dojo/ready",
    "dojo/aspect",
    "dojo/query",
    "works/config",
    
    "./mobile-defaults"
], function( win, has, topic, on, domClass, ready, aspect, query, config ){
    
    var isIpad = false;
    var isIphone = false;
    
    var isPhonoid = false;
    var isTabloid = false;
    
    var isWide = false;
    var isNarrow = false;

    // Su alcuni dispositivi gli orientamenti considerati portrait non sono gli stessi
    var agent = {
        portraits: [ 0, 180 ]
    }
	
    var html = win.doc.documentElement;
	var ua = navigator.userAgent;

    /* Sniff dispositivi Ios/Android */
	if( ua.indexOf( "iPad;" ) >= 0 ) {
		isIpad = true; 

 	} else if( ua.indexOf( "iPhone;" ) >= 0 ) {
		isIphone = true; 

    } else if( ua.indexOf( "Android" ) >= 0 ) {
        var width = screen.width;
        var height = screen.height;
        if( height > width ) {
            var tmp = height;
            height = width;
            width = tmp;
        }
        if( width < 1000 ) {
            isNarrow = true;
        } else if( ( width / height ) > 1.34 ) { // 4:3
            isWide = true;
        }
            
        
        if( ua.indexOf( "Mobile" ) >= 0 ) {
            isPhonoid = true;
        } else {
            isTabloid = true;
        }
 	}
        
    /* Classi distintive dei dispositivi */
    
    if( isIpad || isTabloid) { 
		domClass.add( html, "nw_tablet" );
    }		

    if( isIphone || isPhonoid ) { 
		domClass.add( html, "nw_phone" );
    }		
    
    if( isIpad || isIphone ) {
		domClass.add( html, "nw_ios" );
    }

    if( isIpad ) {
		domClass.add( html, "nw_ipad" );
    }

    if( isIphone ) {
		domClass.add( html, "nw_iphone" );
    }

    if( isTabloid|| isPhonoid ) {
		domClass.add( html, "nw_android" );
    }

    if( isTabloid ) {
		domClass.add( html, "nw_tabloid" );
    }

    if( isPhonoid ) {
		domClass.add( html, "nw_phonoid" );
    }
    
    if( isNarrow ) {
		domClass.add( html, "nw_narrow" );
    } else if( isWide ) {
        domClass.add( html, "nw_wide" );
    }
    
    has.add( "tablet", function( global, document, anElement ){
        return isIpad || isTabloid; /* aggiungere tablet android */
    });

    has.add( "phone", function( global, document, anElement ){
        return isIphone || isPhonoid; /* aggiungere telefoni android */
    });

    has.add( "ios", function( global, document, anElement ){
        return isIpad || isIphone;
    });

    has.add( "ipad", function( global, document, anElement ){
        return isIpad;
    });

    has.add( "iphone", function( global, document, anElement ){
        return isIphone;
    });

    has.add( "android", function( global, document, anElement ){
        return isTabloid || isPhonoid;
    });

    has.add( "tabloid", function( global, document, anElement ){
        return isTabloid;
    });

    has.add( "phonoid", function( global, document, anElement ){
        return isPhonoid;
    });

    config.add( "orientation", function( global, document, anElement ){
        if( has( "tablet" ) || has( "phone" ) ) {

            if( agent.portraits.indexOf( global.orientation ) >= 0 ) {
                return "portrait";
            } else {
                return "landscape";
            }
            
            // if( global.matchMedia( "(orientation: portrait)" ).matches ) {
            //     return "portrait";
            // } else {
            //     return "landscape";
            // }

        } else {
            return undefined;
        }
    });

    has.add( "mobile", function( global, document, anElement ){
        return has( "tablet" ) || has( "phone" );
    });

    has.add( "mobile-app", function( global, document, anElement ){
        return has( "mobile" ) && ( navigator.standalone == true || global.matchMedia( '(display-mode: standalone)' ).matches );
    });
    
    if( has( "mobile-app" ) ) {
		domClass.add( html, "nw_mobile_app" );
    }

    if( has( "mobile" ) ) {
		domClass.add( html, "nw_mobile" );

        on( document.body, "touchmove", function( evt ) {
            evt.preventDefault();
        });

        var orientationHandler = function() {
            var params = { orientation: config( "orientation" ), rotation: win.global.orientation };
        	topic.publish( "viewport/orientation/before", params );
            
            if( config( "orientation" ) == "portrait" ) {
        		domClass.add( html, "nw_portrait" );
        		domClass.remove( html, "nw_landscape" );

        		topic.publish( "viewport/orientation/portrait" );
        
            } else {
        		domClass.add( html, "nw_landscape" );
        		domClass.remove( html, "nw_portrait" );

        		topic.publish( "viewport/orientation/landscape" );
        
            }

        	topic.publish( "viewport/orientation/after", params );
        }
        
        topic.subscribe( "form/task/end", function() {
    		topic.publish( "viewport/orientation/" + config( "orientation" ) );
        });
        
        ready( function(){
            
            var orientation =  win.global.matchMedia( "(orientation: portrait)" ).matches ? "portrait" : "landscape";
            // Tecnica alternativa
            // var orientation = screen.width < screen.height ? "portrait" : "landscape";
            
            // Determina gli orientamenti portrait del dispositivo in uso
            if( orientation != config( "orientation" ) ) {
                agent.portraits = [ 90, -90, 270 ];
            }
            
            orientationHandler();
        });
    
        aspect.after( win.global, "onorientationchange", function(){
            orientationHandler();
        });     
        
        if( ! has( "mobile-app" ) ) {
            window.addEventListener( "scroll", function( evt ) {

                if( window.worksMobileBodyScrollTimeout !== undefined ) {
                    clearTimeout( window.worksMobileBodyScrollTimeout );
                }

                window.worksMobileBodyScrollTimeout = setTimeout( function(){
                    delete window.worksMobileBodyScrollTimeout;
                    document.body.scrollIntoViewIfNeeded( true );
                    form.resize();
                }, 50 );
            });
        }
        
        topic.subscribe( "app/ready", function(){
            if( window.form ) {
                setTimeout( function(){
                    window.form.resize();
                }, 500 );
            }
        });
        
    }
 
});
