define([
    "require"
    ,"dojo/dom-style"
], function( req, domStyle ){
    
    domStyle.isShown = function( node ) {
        if( node ) {
            while( document.body !== node ) {
                var cs = domStyle.getComputedStyle( node );
                if( cs.display == "none" || cs.visibility == "hidden" ) {
                    return false;
                }
                if( node.parentNode ) {
                    node = node.parentNode;
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }
     
    return domStyle;
    
});
