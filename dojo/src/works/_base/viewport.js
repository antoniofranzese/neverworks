define([
    "dojo/_base/kernel"
    ,"dojo/_base/array"
    ,"dojo/topic"
    ,"dojo/has"
    ,"works/lang/types"
    ,"works/_base/dom-class"

], function( kernel, array, topic, has, types, domClass ){

    var VP = {
        registry: {}
        ,index : []
        ,current: null
    };

    var cfg = kernel.global.worksConfig;

    // Default viewports
    if( cfg.viewports === undefined && has( "mobile" ) ) {
        cfg.viewports = [
            { name: "portrait", media: "(orientation: portrait)" }
            ,{ name: "landscape", media: "(orientation: landscape)" }
        ];
    }

    var ViewportChange = function( name, matcher ) {
        if( matcher.matches ) {
            VP.current = VP.registry[ name ];
            domClass.removeAll( document.body, /viewport-.*/ );
            domClass.add( document.body, "viewport-" + name );
            topic.publish( "viewport/change", name );
        }
    }

    if( cfg && types.isArray( cfg.viewports ) ) {

        array.forEach( cfg.viewports, function( vp ){
            if( vp.name !== undefined && vp.media !== undefined ) {
                if( VP.registry[ vp.name ] === undefined ) {
                    var viewport = {
                        name: vp.name
                        ,media: vp.media
                        ,matcher: window.matchMedia( vp.media )
                        ,listener: lang.partial( ViewportChange, vp.name )
                    }

                    viewport.matcher.addListener( viewport.listener );

                    VP.registry[ vp.name ] = viewport;
                    viewport.level = VP.index.length;
                    VP.index.push( viewport );

                } else {
                    throw new Error( "Duplicate viewport '" + vp.name + "'" );
                }
            }    
        });

        if( VP.index.length > 0 ) {
            topic.subscribe( "app/startup", function(){
                array.forEach( VP.index, function( viewport ){
                    viewport.listener( viewport.matcher );
                });
            });
        }

    }
    
    VP.match = function( candidates, view ) {
        if( view === undefined && VP.current ) {
            view = VP.current.name;
        }

        if( view !== undefined && view in VP.registry ) {
            var reference = VP.registry[ view ].level;
            var choices = [];
            array.forEach( candidates, function( candidate ){
                var name;
                var op = "=";
                if( candidate.startsWith( "<=") ) {
                    op = "<=";
                    name = candidate.substring( 2 ).trim();
                } else if( candidate.startsWith( ">=" ) ) {
                    op = ">=";
                    name = candidate.substring( 2 ).trim();
                } else if( candidate.startsWith( "<" ) ) {
                    op = "<";
                    name = candidate.substring( 1 ).trim();
                } else if( candidate.startsWith( ">" ) ) {
                    op = ">";
                    name = candidate.substring( 1 ).trim();
                } else if( candidate.startsWith( "!=" ) || candidate.startsWith( "<>" ) ) {
                    op = "!=";
                    name = candidate.substring( 2 ).trim();
                } else if( candidate.startsWith( "=" ) ) {
                    name = candidate.substring( 1 ).trim();
                } else {
                    name = candidate;
                }

                if( name in VP.registry ) {
                    var level = VP.registry[ name ].level;
                    if( op == "=" &&  reference == level 
                        || op == "!=" && reference != level
                        || op == "<=" && reference <= level
                        || op == ">=" && reference >= level
                        || op == "<" && reference < level
                        || op == ">" && reference > level ) {
                        choices.push({
                            name: candidate
                            ,distance: Math.abs( reference - level )
                        })
                    }
                }

            });

            if( choices.length == 0 ) {
                return undefined;
            } else if( choices.length == 1 ) {
                return choices[ 0 ].name;
            } else {
                var choice;
                for( var c = 0; c < choices.length; c++ ) {
                    if( choice == undefined || choice.distance > choices[ c ].distance ) {
                        choice = choices[ c ];
                    }
                }
                return choice.name;
            }

        } else {
            return undefined;
        }
    }

    return VP;
});

