define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
], function( _require, declare, lang ){
    
    var cfg = lang.getObject( "worksConfig" );
    var def = function( key, value ) {
        if( cfg[ key ] === undefined ) {
            cfg[ key ] = value;
        }
    }
    
    def( "dragDelay", 300 );
    def( "doubleClickDelay", 200 );
    def( "blinkDelay", 1000 );
    def( "touchInspectDelay", 500 );
    def( "touchDragTreshold", 10 );
    def( "blurRefusalDelay", 500 );
    
    return cfg;
})