define([
	"dojo/_base/kernel"
	,"works/registry"
    ,"works/channels"
	,"dojo/dom"
	,"dojo/_base/lang"
    ,"dojo/request"
    ,"dojo/query"
    ,"dojo/topic"
    ,"dojo/has"
    ,"dojo/Deferred"

], function( kernel, registry, channels, dom, lang, request, query, topic, has, Deferred ){
    
    var defined = function( obj ) {
        return obj !== undefined && obj != null;
    }
    
    var mixin = function( module, name ) {
        // Infer attribute name
        if( name == undefined ){
            name = module.substring( module.lastIndexOf( "/" ) + 1 ).replace( /-/g, "_" );
        }

        // Avoid overwrite
        while( name in kernel.global ){
            name = "_" + name;
        }
        
        // Load module and place as global attribute
        require( [ module ], function( mod ){
            kernel.global[ name ] = mod;
        })

        // Display used name in console
        return name;
    }
	
    var widget = function( name ) {
        if( name ) {
            var result = registry.byId( name );
            return result ? result : ( window.form !== undefined ? form.widget( name ) : undefined );
        } else {
            return undefined;
        }
    }
    
    var setLocalTimeout = function( callback, timeout ) {
        var win = window;
        return setTimeout( function(){
            if( window === win ) {
                callback();
            }
        }, timeout || 0 );
    }
    
    var pick = function() {
        form.spreadMessage({ topic: "works/pick/init" });
        
        topic.publish( "works/pick/stop" );
        var d = new Deferred();
        topic.publish( "works/pick/start", d );
        d.then( function( result ){
            console.log( "Pick:", result.name );
        }, function(){
            console.log( "Closing Pick" );
        });
        return d;
    }
    
    var channel = function( name ) {
        return channels.channel( name );
    }
    
    // Utility shortcuts for common modules and functions
    lang.mixin( kernel.global , {
		widget: widget
		,dom: dom.byId
		,registry: registry
		,lang: lang
		,hitch: lang.hitch
        ,request: request
        ,query: query
        ,topic: topic
        ,defined: defined
        ,mixin: mixin
        ,setLocalTimeout: setLocalTimeout
        ,pick: pick
        ,channel: channel
	});
    
    return {};
})