define([

], function() {

    var point = function( src ) {
        var x, y;
        if( src.x !== undefined && src.y !== undefined ) {
            return src;

        } else if( src.pageX !== undefined && src.pageY !== undefined ) {
            return { 
                x: Math.floor( src.pageX )
                , y: Math.floor( src.pageY ) 
            };

        } else if( src.clientX !== undefined && src.clientY !== undefined ) {
            return { 
                x: Math.floor( src.clientX )
                , y: Math.floor( src.clientY ) 
            };

        } else if( src.touches !== undefined && src.touches[ 0 ] !== undefined ) {
            return { 
                x: Math.floor( src.touches[ 0 ].pageX )
                , y: Math.floor( src.touches[ 0 ].pageY ) 
            };

        } else {
            return { x: null, y: null }

        }
        
    };
    
    var box = function( p1, p2 ) {
        p1 = point( p1 );
        p2 = point( p2 );

        if( p1.x > p2.x ) {
            var x = p2.x;
            p2.x = p1.x;
            p1.x = x;
        }

        if( p1.y > p2.y ) {
            var y = p2.y;
            p2.y = p1.y;
            p1.y = y;
        }
        
        return {
            x1: p1.x
            ,y1: p1.y
            ,x2: p2.x
            ,y2: p2.y
            ,from: p1
            ,to: p2
        }
    };

    var distance = function( p1, p2 ) {
        var b = box( p1, p2 );
        return Math.sqrt( Math.pow( b.x2 - b.x1, 2 ) + Math.pow( b.y2 - b.y1, 2 ) );
    };
    
    return {
        point: point
        ,distance: distance
        ,box: box
    };

});