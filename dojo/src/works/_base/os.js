define([
    "dojo/_base/window",
    "dojo/_base/lang",
    "dojo/has",
    "dojo/dom-class"

], function( win, lang, has, domClass ){

    var html = win.doc.documentElement;
    var ua = navigator.userAgent;

    var isLinux = false;
    var isMac = false;
    var isWindows = false;

    domClass.add( html, "nw" );

    if( ua.indexOf( "Linux" ) >= 0 ) {
        isLinux = true;
        domClass.add( html, "nw_linux" );
    
    } else if( ua.indexOf( "Windows" ) >= 0 ) {
        isWindows = true;
        domClass.add( html, "nw_win" );
    
    } else if( ua.indexOf( "Mac" ) >= 0 && ua.indexOf( "Mobile" ) < 0  ) {
        isMac = true;
        domClass.add( html, "nw_mac" );
    }

    if( ua.indexOf( "Electron/" ) >= 0 ) {
        domClass.add( html, "nw_electron" );
    }

    if( isMac || isLinux || isWindows ) {
        domClass.add( html, "nw_desktop" );
    }

    has.add( "linux", function( global, document, anElement ){
        return isLinux;
    });

    has.add( "windows", function( global, document, anElement ){
        return isWindows;
    });

    has.add( "mac", function( global, document, anElement ){
        return isMac;
    });

    has.add( "desktop", function( global, document, anElement ){
        return isMac || isLinux || isWindows;
    });
    
    has.add( "electron", function( global, document, anElement ){
        return lang.getObject( "neverworks.electron" );
    });

    has.add( "ie11", function( global, document, anElement ){
        if( navigator.appName == 'Netscape' ) {
            var re = new RegExp( "Trident/.*rv:([0-9]{1,}[\.0-9]{0,})" );
            if( re.exec( ua ) != null ) {
                return parseFloat( RegExp.$1 );
            }
        }
    });

    if( has( "ie11" ) ) {
        domClass.add( html, "nw_ie11" );
    }
    
});

