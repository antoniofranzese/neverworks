define([
	"dojo/_base/json"
    ,"dojo/date/stamp"
    ,"dojo/request/handlers"

], function(  json, stamp, handlers  ){

    Date.prototype.toJSON = function( key ) { 
        return stamp.toISOString( this, { zulu: false } ); 
    };

    handlers.register( "json", function( response ) { 
        return json.fromJson( response.text || null ); } 
    );

    return {};
})
