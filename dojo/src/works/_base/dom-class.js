define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/dom"
    ,"dojo/dom-class"
], function( _require, declare, array, lang, dom, domClass ){
    
    var className = "className";
    var spaces = /\s+/;
    
	function str2array( s ){
        if( s && s.hasText() ) {
            return s.trim().split( spaces );
        } else {
            return [];
        }
	}
    
    domClass.forEach = forEach = function( node, callback, context ) {
        if( lang.isFunction( callback ) ) {
            var classes = str2array( dom.byId(node)[ className ] );
            array.forEach( classes, callback, context );
        }
    }
    
    domClass.removeAll = function( node, regex ) {
        if( lang.isFunction( regex.test ) ) {
            forEach( node, function( cls ){
                if( regex.test( cls ) ) {
                    domClass.remove( node, cls );
                }
            });
        }
    }

    domClass.addAll = function( node, classes ) {
        var all;
        if( typeof classes == "string" ) {
            all = str2array( classes );
        } else {
            all = classes;
        }
        
        array.forEach( all, function( cls ){
            domClass.add( node, cls );
        });
    }
    
    return domClass;
    
});