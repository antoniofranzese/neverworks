define([

], function(){
    // Object.keys polyfill
    if (!Object.keys) {
        Object.keys = (function () {
            'use strict';
            var hasOwnProperty = Object.prototype.hasOwnProperty,
            hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
            dontEnums = [
                'toString',
                'toLocaleString',
                'valueOf',
                'hasOwnProperty',
                'isPrototypeOf',
                'propertyIsEnumerable',
                'constructor'
            ],
            dontEnumsLength = dontEnums.length;

            return function (obj) {
                if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                    throw new TypeError('Object.keys called on non-object');
                }

                var result = [], prop, i;

                for (prop in obj) {
                    if (hasOwnProperty.call(obj, prop)) {
                        result.push(prop);
                    }
                }

                if (hasDontEnumBug) {
                    for (i = 0; i < dontEnumsLength; i++) {
                        if (hasOwnProperty.call(obj, dontEnums[i])) {
                            result.push(dontEnums[i]);
                        }
                    }
                }
                return result;
            };
        }());
    }

    // Object.create polyfill
    if (typeof Object.create != 'function') {
        (function () {
            var F = function () {};
            Object.create = function (o) {
                if (arguments.length > 1) { 
                  throw Error('Second argument not supported');
                }
                if (o === null) { 
                  throw Error('Cannot set a null [[Prototype]]');
                }
                if (typeof o != 'object') { 
                  throw TypeError('Argument must be an object');
                }
                F.prototype = o;
                return new F();
            };
        })();
    }
    
    
    // Function bind polyfill
    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5 internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }

            var aArgs = Array.prototype.slice.call(arguments, 1), 
            fToBind = this, 
            fNOP = function () {},
            fBound = function () {
                return fToBind.apply(
                    this instanceof fNOP && oThis ? this : oThis,
                    aArgs.concat(Array.prototype.slice.call(arguments))
                );
            };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
        };
    }
    
    // Array.isArray
    if(!Array.isArray) {
        Array.isArray = function(arg) {
            return Object.prototype.toString.call(arg) === '[object Array]';
        };
    }
    
    // String.trim
    if (!String.prototype.trim) {
      String.prototype.trim = function () {
        return this.replace(/^\s+|\s+$/g, '');
      };
    }
    
    // Date.toISOString
    if (!Date.prototype.toISOString) {
        Date.prototype.toISOString = function() {
            function pad(n) { return n < 10 ? '0' + n : n }
            return this.getUTCFullYear() + '-'
                + pad(this.getUTCMonth() + 1) + '-'
                + pad(this.getUTCDate()) + 'T'
                + pad(this.getUTCHours()) + ':'
                + pad(this.getUTCMinutes()) + ':'
                + pad(this.getUTCSeconds()) + '.'
                + pad(this.getUTCMilliseconds()) + 'Z';
        };
    }
    
    // Array.forEach
    if (!Array.prototype.forEach) {
      Array.prototype.forEach = function(fn, scope) {
        for(var i = 0, len = this.length; i < len; ++i) {
          fn.call(scope, this[i], i, this);
        }
      }
    }
    
    // Array.indexOf
    if (!Array.prototype.indexOf) {
      Array.prototype.indexOf = function (searchElement, fromIndex) {

        var k;

        // 1. Let O be the result of calling ToObject passing
        //    the this value as the argument.
        if (this == null) {
          throw new TypeError('"this" is null or not defined');
        }

        var O = Object(this);

        // 2. Let lenValue be the result of calling the Get
        //    internal method of O with the argument "length".
        // 3. Let len be ToUint32(lenValue).
        var len = O.length >>> 0;

        // 4. If len is 0, return -1.
        if (len === 0) {
          return -1;
        }

        // 5. If argument fromIndex was passed let n be
        //    ToInteger(fromIndex); else let n be 0.
        var n = +fromIndex || 0;

        if (Math.abs(n) === Infinity) {
          n = 0;
        }

        // 6. If n >= len, return -1.
        if (n >= len) {
          return -1;
        }

        // 7. If n >= 0, then Let k be n.
        // 8. Else, n<0, Let k be len - abs(n).
        //    If k is less than 0, then let k be 0.
        k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

        // 9. Repeat, while k < len
        while (k < len) {
          var kValue;
          // a. Let Pk be ToString(k).
          //   This is implicit for LHS operands of the in operator
          // b. Let kPresent be the result of calling the
          //    HasProperty internal method of O with argument Pk.
          //   This step can be combined with c
          // c. If kPresent is true, then
          //    i.  Let elementK be the result of calling the Get
          //        internal method of O with the argument ToString(k).
          //   ii.  Let same be the result of applying the
          //        Strict Equality Comparison Algorithm to
          //        searchElement and elementK.
          //  iii.  If same is true, return k.
          if (k in O && O[k] === searchElement) {
            return k;
          }
          k++;
        }
        return -1;
      };
    }
    
    // Node.contains
    if( window.Node && Node.prototype && !Node.prototype.contains ){
    	Node.prototype.contains = function( arg ) {
            // compareDocumentPosition bitmask result:
            // 1 = Position disconnected
            // 2 = Precedes
            // 4 = Follows
            // 8 = Contains
            // 16 = Is contained by
    		return !!( this.compareDocumentPosition( arg ) & 16 );
    	}
    }
    

    // SVGElement.getTransformToElement
    if( ! SVGElement.prototype.getTransformToElement ){
        SVGElement.prototype.getTransformToElement = function( elem ) { 
            return elem.getScreenCTM().inverse().multiply( this.getScreenCTM() ); 
        };
    }

    // Object properties
    if( !Object.defineProperty ) {
        (function () {
            "use strict";
        	var ObjectProto = Object.prototype,
        	defineGetter = ObjectProto.__defineGetter__,
        	defineSetter = ObjectProto.__defineSetter__,
        	lookupGetter = ObjectProto.__lookupGetter__,
        	lookupSetter = ObjectProto.__lookupSetter__,
        	hasOwnProp = ObjectProto.hasOwnProperty;
	
        	if (defineGetter && defineSetter && lookupGetter && lookupSetter) {

        		if (!Object.defineProperty) {
        			Object.defineProperty = function (obj, prop, descriptor) {
        				if (arguments.length < 3) { // all arguments required
        					throw new TypeError("Arguments not optional");
        				}
				
                        descriptor.writable = descriptor.writable || true;
                        descriptor.enumerable = descriptor.enumerable || true;
                        descriptor.configurable = descriptor.configurable || true;

        				prop += ""; // convert prop to string

        				if (hasOwnProp.call(descriptor, "value")) {
        					if (!lookupGetter.call(obj, prop) && !lookupSetter.call(obj, prop)) {
        						// data property defined and no pre-existing accessors
        						obj[prop] = descriptor.value;
        					}

        					if ((hasOwnProp.call(descriptor, "get") ||
        					     hasOwnProp.call(descriptor, "set"))) 
        					{
        						// descriptor has a value prop but accessor already exists
        						throw new TypeError("Cannot specify an accessor and a value");
        					}
        				}

        				// can't switch off these features in ECMAScript 3
        				// so throw a TypeError if any are false
        				if (!(descriptor.writable && 
                            descriptor.enumerable && descriptor.configurable))
        				{
        					throw new TypeError(
        						"This implementation of Object.defineProperty does not support" +
        						" false for configurable, enumerable, or writable."
        					);
        				}
				
        				if (descriptor.get) {
        					defineGetter.call(obj, prop, descriptor.get);
        				}
        				if (descriptor.set) {
        					defineSetter.call(obj, prop, descriptor.set);
        				}
			
        				return obj;
        			};
        		}

        		if (!Object.getOwnPropertyDescriptor) {
        			Object.getOwnPropertyDescriptor = function (obj, prop) {
        				if (arguments.length < 2) { // all arguments required
        					throw new TypeError("Arguments not optional.");
        				}
				
        				prop += ""; // convert prop to string

        				var descriptor = {
        					configurable: true,
        					enumerable  : true,
        					writable    : true
        				},
        				getter = lookupGetter.call(obj, prop),
        				setter = lookupSetter.call(obj, prop);

        				if (!hasOwnProp.call(obj, prop)) {
        					// property doesn't exist or is inherited
        					return descriptor;
        				}
        				if (!getter && !setter) { // not an accessor so return prop
        					descriptor.value = obj[prop];
        					return descriptor;
        				}

        				// there is an accessor, remove descriptor.writable;
        				// populate descriptor.get and descriptor.set (IE's behavior)
        				delete descriptor.writable;
        				descriptor.get = descriptor.set = undefined;
				
        				if (getter) {
        					descriptor.get = getter;
        				}
        				if (setter) {
        					descriptor.set = setter;
        				}
				
        				return descriptor;
        			};
        		}

        		if (!Object.defineProperties) {
        			Object.defineProperties = function (obj, props) {
                        var prop;
        				for (prop in props) {
        					if (hasOwnProp.call(props, prop)) {
        						Object.defineProperty(obj, prop, props[prop]);
        					}
        				}
        			};
        		}

        	}
        }());
    }
    
    return {};    
});