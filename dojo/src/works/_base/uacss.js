define([
    "dojo/_base/window"
    ,"dojo/has"
    ,"works/_base/dom-class"

    ,"dojo/uacss"
], function( win, has, domClass ){

    var CLASS_REGEX = new RegExp( "dj_.*" );

    var html = win.doc.documentElement;
    var ua = navigator.userAgent;
    var ie11 = undefined;

    if( navigator.appName == 'Netscape' ) {
        var re = new RegExp( "Trident/.*rv:([0-9]{1,}[\.0-9]{0,})" );
        if( re.exec( ua ) != null ) {
            ie11 = parseFloat( RegExp.$1 );
            domClass.add( html, "nw_ie11" );
        }
    }

    has.add( "ie11", function( global, document, anElement ){
        return ie11;
    });

    // Fix missing class
    if( has( "ff" ) ) {
        domClass.add( html, "dj_ff" );
    }
    
    domClass.forEach( document.documentElement, function( cls ){
        if( CLASS_REGEX.test( cls ) ) {
            //console.log( "CLASS", cls );
            domClass.add( html, cls.replace( "dj_", "nw_" ) );
        }
    }, this );

});

