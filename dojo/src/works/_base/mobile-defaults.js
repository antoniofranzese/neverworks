define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
], function( _require, declare, lang ){
    
    var cfg = lang.getObject( "worksConfig" );
    
    if( cfg.mobile === undefined ) {
        cfg.mobile = {}
    }
    
    if( cfg.mobile.rotationEvent === undefined ) {
        cfg.mobile.rotationEvent = false;
    }

    return cfg;
})