define([
    "require"
    ,"works/lang/declare"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"dijit/Tooltip"
    ,"./SyncManager"

    ,"../style!./resources/List.less"
], function( req, declare, on, domClass, Tooltip, SyncManager ){
    
    return declare( declare.className( req ), [ SyncManager ], {
        baseClass: "worksListBox"
        ,header: false
        ,readonly: false
        ,tooltipPosition: []
        
        ,startup: function() {
            this.inherited( arguments );
            this.own(
                on( this, "reloaded", lang.hitch( this, function(){
                    if( this.value ) {
                        this.set( "value", this.value );
                    }
                }))
                ,this.watch( "hovering", lang.hitch( this, this._showProblemOnHover ) )
            );
            if( this.initialValue ) {
                this.set( "value", this.initialValue );
            }
        }
        
        ,_showProblemOnHover: function( name, old, value ) {
            if( this._validationProblem != null ) {
    			if( value ){
    				Tooltip.show( this._validationProblem, this.domNode, this.tooltipPosition, false );
    			}else{
    				Tooltip.hide( this.domNode );
    			}
            }
        }

        ,handleRowClick: function( params ) {
            this.emitEvent( "clicked", { state: params } );
        }
        
        ,handleRowInspect: function( params ) {
            this.emitEvent( "inspect", { state: params } );
        }
        
        ,handleRowSelection: function( params ) {
            if( ! this.readonly ) {
                this.keepSelectedRow( params["new"] )
                this.set( "selectedKey", params["new"] );

                var index = this.grid.model.idToIndex( params["new"] );
                var selectedRow = this.grid.row( index );
                this.value = selectedRow ? selectedRow.item()[ this.idAttribute ] : null;
                this.emitAlways( "changed", { state: params } );
            }
        }
        
        ,keepSelectedRow: function( id ) {
            var ids = this.grid.select.row.getSelected();
            for( var i = 0; i < ids.length; i++ ) {
                if( ids[ i ] != id ) {
                    this.grid.select.row.deselectById( ids[ i ] );
                }
            }
        }
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
            form.ready().then( lang.hitch( this, function(){
                if( !( this.getParent() ) || this.getParent().isStaticLayoutWidget ) {
                    //console.log( "Resize because parent is static: " + this.getParent() );
                    this.resizeGrid();
                }
            }));
        }
        
        ,leaveForm: function() {
            this.retireEvent( "changed" );
            this.inherited( arguments );
        }
        
        ,processElasticSize: function() {
            var size = this.inherited( arguments );
            if( size !== undefined && size != null && this.autoHeight ) {
                delete size.h;
            }
            return size;
        }
            
        ,gatherGridState: function() {
            var state = this.inherited( arguments );
            state[ "value" ] = this.value;
            return state;
        }

        ,_getScrollAttr: function() {
            return undefined;
        }

        ,setGridState: function( state ) {
            if( "items" in state ) {
                this.set( "items", state.items );
                delete state.items;
            }
            if( "readonly" in state ) {
                this.set( "readonly", state.readonly );
            }
            if( "required" in state ) {
                this.set( "required", state.required );
            }
            if( "problem" in state ) {
                this.set( "problem", state.problem );
            }
            if( "value" in state ) {
                this.set( "value", state.value );
            }
            this.inherited( arguments );
        }
        
        ,_setValueAttr: function( value ) {
            this.keepSelectedRow( value );
            var index = this.grid.model.idToIndex( value );
            if( index >= 0 ) {
                this.grid.select.row.selectById( value );
                this.value = value;

                setTimeout( lang.hitch( this, function(){
                    this.grid.vScroller.scrollToRow( index );
                }));
            } else {
                this.value = null;
            }
        }

        ,_setReadonlyAttr: function( value ) {
            this.readonly = ( value == true ) ;
            domClass[ this.readonly ? "add" : "remove" ]( this.domNode, "readonly" );
        }

        ,_setRequiredAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode, "required" );
        }
        
        ,_setProblemAttr: function( value ) {
            this._validationProblem = value;
            domClass[ value != null ? "add" : "remove" ]( this.domNode, "problem" );
        }
        
    });
    
});