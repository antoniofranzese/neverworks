define([
    "require"
    ,"dojo/_base/declare"
    ,"./manager/Builder"
    ,"./manager/Properties"
    ,"../form/_WidgetMixin"
    ,"../form/_FormWidgetMixin"
    ,"../_WidgetUtils"
    
    ,"../style!../../gridx/resources/claro/Gridx.css"
    ,"../style!./resources/Grid.less"
     
], function( _require, declare, Builder, Properties, _WidgetMixin, _FormWidgetMixin, _WidgetUtils ){
    
    return declare( [ _WidgetMixin, _FormWidgetMixin, Builder, Properties ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,baseClass: "worksGrid"
        
        ,constructor: function( params, node ) {
            var options = _WidgetUtils.parseOptions( node );
            declare.safeMixin( this, options );
            
            this.fragments = _WidgetUtils.parseFragments( node );
            this.modules = [];
        }
        
        ,getWidgetState: function() {
            return this.gatherGridState();
        }
        
        ,gatherGridState: function() {
            return {};
        }
        
        ,setWidgetState: function( state ) {
            this.setBuilderGridState( state );
            this.setGridState( state );
        }
        
        ,setGridState: function( state ) {}
        
    });
    
});