define([
    "require"
    ,"dojo/_base/declare"
    ,"./BaseManager"
    ,"./manager/AsyncStorage"      
    ,"../_WidgetUtils"
], function( _require, declare, BaseManager, AsyncStorage, _WidgetUtils ){
    
    return declare( [ BaseManager, AsyncStorage ], {
        declaredClass: _WidgetUtils.inferDeclaredClass( _require )
    });
    
});