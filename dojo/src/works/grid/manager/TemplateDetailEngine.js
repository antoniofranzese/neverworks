define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/parser"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/query"
    ,"dojo/Deferred"
    ,"works/registry"

], function( _require, declare, lang, array, parser, domClass, domStyle, query, Deferred, registry  ){
    
    var Engine = declare( [], {
        constructor: function( template ) {
            this.template = template;
        }
        
        ,draw: function( params ) {
            var d = new Deferred();
            setTimeout( lang.hitch( this, function(){
                var html = lang.replace( this.template.body, params.grid.row( params.id ).item() );
                //TODO: destroy;
                params.node.innerHTML = html;
                parser.parse( params.node ).then( function() {
                    d.resolve();
                });
            }), 0 );
            return d;
        }
        
        ,clean: function( params ) {
            var d = new Deferred();
            array.forEach( registry.findWidgets( params.node ), function( widget ){
                widget.destroyRecursive( false );
            });
            setTimeout( function(){ d.resolve(); }, 0 );
            return d;
        }
    });
    
    return declare( [], {

        postCreateGrid: function() {
            this.inherited( arguments );
            
            var cfg = this.getConfiguration( "details" );
            var template = this.getFragment( cfg.get( "template", "<no template>" ) );
            if( this.details != undefined ) {
                this.details.engine = new Engine( template );
            } else {
                console.error( "Missing details module" );
            }
        }
        
        
    });
    
});