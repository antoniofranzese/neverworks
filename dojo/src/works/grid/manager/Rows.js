define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/fx"
    ,"dojo/fx/easing"
    ,"dojo/has"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/query"
    ,"dojo/dom-attr"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-construct"
    ,"works/_base/click"
    ,"works/local-style"
    ,"dojo/has!mobile?works/layout/MomentumScroller"
	,'gridx/modules/RowHeader'
    ,"gridx/modules/select/Row"
	,'gridx/modules/TouchScroll'

], function( _require, declare, lang, fx, easing, has, on, topic, query, domAttr, domStyle, domClass, domGeometry, domConstruct, click, localStyle, MomentumScroller, RowHeader, SelectRow, TouchScroll ){
    
    var ROW = ".r";
    var INTERIM = "in";
    var BEFORE = "bf";
    var AFTER = "af";

    var TOUCH_INSPECT_DELAY = lang.getObject( "worksConfig.touchInspectDelay" );
    var TOUCH_DRAG_TRESHOLD = lang.getObject( "worksConfig.touchDragTreshold" );

    return declare( [], {
        
        postMixInProperties: function() {
            this.inherited( arguments );
            var cfg = this.getConfiguration( "rows" );

            this.rows = {
                selector: cfg.get( "selector", false )
                ,selectorWidth: cfg.get( "selectorWidth", "20px" )
                ,scrollable: cfg.get( "scrollable", "vertical" )
                ,selectMultiple: cfg.get( "selectMultiple", false )
                ,selectOnClick: cfg.get( "selectOnClick", true )
            }
            
        }
        
        ,preCreateGrid: function() {
            this.inherited( arguments );

            this.modules.push({             
                moduleClass: SelectRow
                ,multiple: this.rows.selectMultiple
                ,triggerOnCell: false //this.rows.selectOnClick
            })             

            if( has( "mobile" ) ) {
                this.modules.push({
                    moduleClass: TouchScroll
                });
            }

            if( this.rows.selector ) {
                this.modules.push({ 
                    moduleClass: RowHeader 
                    ,width: this.rows.selectorWidth
                });
            }
            
            this.set( "scrollable", this.rows.scrollable );
        }
        
        ,postCreateGrid: function() {
            this.inherited( arguments );

            if( this.bodyDividers !== undefined ) {
                this.set( "bodyDividers", this.bodyDividers );
            }
            
            this.own(
                on( this, "row-clicking", lang.hitch( this, function( evt ){
                    var oldID = this.selectedRow ? this.selectedRow.item()[ this.idAttribute ] : null;
                    var newID = evt.row.item()[ this.idAttribute ];
                    if( newID != oldID ) {
                        this.handleRowSelection({ 
                            "old": oldID
                            ,"new": newID 
                            ,rowIndex: evt.row.index()
                            ,columnIndex: evt.column !== undefined ? evt.column.index() : null
                            ,x: evt.x
                            ,y: evt.y
                            ,attributes: evt.attributes
                        });
                    
                    } else {
                        this.handleRowClick({
                            id: evt.row.item()[ this.idAttribute ]
                            ,rowIndex: evt.row.index()
                            ,columnIndex: evt.column !== undefined ? evt.column.index() : null
                            ,x: evt.x
                            ,y: evt.y
                            ,attributes: evt.attributes
                        });                                    
                    }

                }))

                ,on( this, "row-context", lang.hitch( this, function( evt ){
                    this.handleRowInspect({ 
                        id: evt.row.item()[ this.idAttribute ]
                        ,rowIndex: evt.row.index()
                        ,columnIndex: evt.column.index() 
                        ,x: evt.x
                        ,y: evt.y
                        ,attributes: evt.attributes
                    });                                    
                }))
                
                ,on( this, "column-sorting", lang.hitch( this, function(){
                    this.deselectRows();
                }))
                
                ,on( this, "reloading", lang.hitch( this, function(){
                    this.deselectRows();
                    this.grid.sort.clear( /* skipBodyUpdate */ true );
                    //this.grid.vScroller.scrollToRow( 0 );
                }))
                
                ,this.grid.connect( this.grid.bodyNode, "mouseover", lang.hitch( this, function( evt ){
                    // console.log( "GridMouse",  );
                    this.rows.mouse = { x: Math.floor( evt.clientX ), y: Math.floor( evt.clientY ) };
                }))
                
                ,this.grid.connect( this.grid.bodyNode, "onscroll", lang.hitch( this, function( evt ){
                    //console.log( "GridScroll", evt.clientX, evt.clientY );
                    if( this.rows.mouse ) {
                        topic.publish( "mouse/scroll", this.rows.mouse );
                    }
                }))
                
    			,this.grid.connect( this.grid.body, 'onAfterRow', lang.hitch( this, function( row ){
                    this.renderBanners( row );
    			}))

                ,on( this, "grid-touch-start", lang.hitch( this, function(){
                    this.deselectRows();
                }))

    			,this.grid.connect( this.grid.columnWidth, 'onUpdate', lang.hitch( this, function(){
                    this.updateBanners();
    			}))
                
                //,this.wire( this.grid.body, "onAfterRow", lang.hitch( this, function( row ) {
                ,this.grid.connect( this.grid.body, "onAfterRow", lang.hitch( this, function( row ) {
                    this.emit( "after-row", {
                        row: row 
                        ,bubbles: false
                    });
                })) 

                // ,this.grid.connect( this.grid.body, 'onUnrender', lang.hitch( this, function( rowId ){
                //                     var row = this.grid.row( rowId );
                //                     if( row ) {
                //                         this.onRowUnrender( row );
                //                     }
                // }))
                
            );
            
            if( has( "mobile" ) ) {
                
                this.rows.scroll = new MomentumScroller({ 
                    grid: this.grid 
                    ,mainNode: this.grid.domNode
                    ,headerNode: this.grid.rowHeader ? this.grid.rowHeader.bodyNode : undefined
                    ,bodyNode: this.grid.bodyNode
                    ,scrollerNode: this.grid.vScrollerNode
                    
                    ,onStart: function() {
                        this.stopOffsetAnimation();
                        if( this.offset != 0 ) {
                            this.adjustOffset( this.offset * -1 );
                            this.offset = 0;
                        }
                        this.maxScroll = domGeometry.position( this.scrollerNode.firstChild ).h - domGeometry.position( this.scrollerNode ).h;
                    }
                    
                    ,onScroll: function( value ){
                        //console.log( "scroll", value );
                        this.stopOffsetAnimation();
                        if( this.scrollerNode.scrollTop > 0 && this.scrollerNode.scrollTop < this.maxScroll ) {
                            this.scrollerNode.scrollTop += value;
                            this.offset = 0;
                        } else if( value < 0 && this.offset < 150 ){
                            this.adjustOffset( value * -1 );
                            if( Math.abs( value ) < 10 ) {
                                return false;
                            }
                        } else if( value > 0 && this.offset > -150 ){
                            this.adjustOffset( value * -1 );
                            if( Math.abs( value ) < 10 ) {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                    
                    ,onTrack: function( value ) {
                        //console.log( "track", value );
                        this.stopOffsetAnimation();
                        var brake = 1;
                        if( this.offset != 0 ) {
                            brake = ( 1000 - Math.abs( this.offset ) ) / 3000;
                        }
                        if( this.scrollerNode.scrollTop <= 0 && value < 0 && this.offset < 150 ){
                            this.adjustOffset( value * -1 * brake );
                        } else if( this.scrollerNode.scrollTop >= this.maxScroll && value > 0 && this.offset > -150 ){
                            this.adjustOffset( value * -1 * brake );
                        }
                    }
                    
                    ,onEnd: function() {
                        //console.log( "end" );
                        if( this.offset != 0 ) {
                            this.animateOffset( 0 );
                        }
                    }

                    ,stopOffsetAnimation: function() {
                        if( this.offsetAnimation ) {
                            this.offsetAnimation.stop();
                            delete this.offsetAnimation;
                        }
                    }

                    ,adjustOffset: function( value ) {
                        var wasZero = this.offset == 0;
                        this.offset += Math.floor( value );
                        if( wasZero ) {
                            if( this.offset > 0 ) {
                                domClass.add( this.mainNode, "gridxBounceDown" );
                            } else if( this.offset < 0 ) {
                                domClass.add( this.mainNode, "gridxBounceUp" );
                            }
                        }
                        domStyle.set( this.bodyNode, "top", this.offset + "px" );
                        if( this.headerNode ) {
                            domStyle.set( this.headerNode, "top", this.offset + "px" );
                        }
                    }

                    ,animateOffset: function( destination ) {
                        if( this.offset > 0 ) {
                            domClass.add( this.mainNode, "gridxBounceDown" );
                        } else if( this.offset < 0 ) {
                            domClass.add( this.mainNode, "gridxBounceUp" );
                        }

                        this.offsetAnimation = fx.animateProperty({
                            node: this.bodyNode
                            ,headerNode: this.headerNode
                            ,mainNode: this.mainNode
                            ,properties: { top: destination }
                            ,duration: 750
                            ,easing: easing.expoOut
                            ,onAnimate: function( value ) {
                                if( this.headerNode ) {
                                    domStyle.set( this.headerNode, value );
                                }
                            }
                            ,onEnd: function() {
                                domClass.remove( this.mainNode, "gridxBounceDown" );
                                domClass.remove( this.mainNode, "gridxBounceUp" );
                                this.offset = Math.floor( domGeometry.position( this.node ).y );
                            }
                            ,onStop: function() {
                                this.onEnd();
                            }
                        }).play();
                    }
                    
                });

                this.own(
                    on( this, "row-touch-start-managed", lang.hitch( this, function( evt ){
                        // console.log( this.name, "rowtouchmanaged " + ( evt.grid != this.grid ) );
                        if( evt.grid != this.grid ) {
                            this.rows.touch = { 
                                passive: true 
                            };
                        }
                    }))
                );
                
                this.wire( this.grid, "onRowTouchStart", lang.hitch( this, function( evt ) { 
                    // console.log( this.name, "touch start" );
                    this.emit( "row-touch-start-managed", { grid: this.grid } );
                    
                    evt.preventDefault();
                    
                    if( this.rows.scroll.active() ) {
                        this.rows.scroll.restart( evt.clientY );
 
                    } else {
                        
                        this.rows.touch = {
                            tapping: true
                            ,inspecting: false
                            ,rowId: evt.rowId
                            ,startPoint: click.point( evt )
                            ,inspectTimeout: setTimeout( lang.hitch( this, function(){
                                if( this.rows.touch.tapping ){
                                    this.rows.scroll.stop();
                                    this.rows.touch.inspecting = true;
                                
                                    //BUGFIX
                                    if( evt.rowId === undefined ) {
                                        evt.rowId = this.rows.touch.rowId;
                                    }
                                
                                    //BUGFIX
                                    if( evt.columnId === undefined ) {
                                        evt.columnId = domAttr.get( evt.target, "colid" );
                                    }
                                
                                    this.grid.onCellContextMenu( evt );
                                }
                            }), TOUCH_INSPECT_DELAY )
                        };
                        
                        this.rows.scroll.start( evt.clientY );
                    }

                }));

                this.wire( this.grid, "onRowTouchEnd", lang.hitch( this, function( evt ) {
                    //console.log( this.name, "touch end"  );
                    var touch = this.rows.touch;
                    
                    if( touch ) {
                        if( touch.inspectTimeout ) {
                            clearTimeout( touch.inspectTimeout ); 
                        }

                        if( touch.tapping == true ) {
                            this.rows.scroll.stop();
                            if( ! touch.inspecting ) {
                                this.grid.onRowClick( evt );
                            }
                        } else {
                            this.rows.scroll.finish();
                        }
                    }
                    delete this.rows.touch;
                }));

                this.wire( this.grid.touchScroll, "_start", lang.hitch( this, function( evt ) {
                    // console.log( this.name, "start" );
                    if( this.rows.touch === undefined ) {
                        this.rows.touch = {
                            tapping: false
                            ,inspecting: false
                        };
                        this.rows.scroll.start( evt.clientY );
                        
                    } else if( this.rows.touch.passive ) {
                        delete this.grid.touchScroll._last;
                    }
                }));
                
                this.wire( this.grid.touchScroll, "_end", lang.hitch( this, function( evt ) {
                    //console.log( this.name, "end" );
                    delete this.rows.touch;
                    this.rows.scroll.finish();
                }));

                this.wire( this.grid.touchScroll, "_scroll", lang.hitch( this, function( evt ) { 
                    // console.log( this.name, "_scroll" );
                    var touch = this.rows.touch;
                    if( touch && touch.passive != true ) {
                        if( touch.startPoint && click.distance( touch.startPoint, evt ) > TOUCH_DRAG_TRESHOLD ) {
                            touch.tapping = false;
                        }
                    
                        if( touch.passive === undefined ) {
                            this.rows.scroll.track( evt.clientY );
                        }
                    }
                }));
                
            }

            setTimeout( lang.hitch( this, function(){
                if( this.pendingDisplacement ) {
                    this.set( "displacement", this.pendingDisplacement );
                    delete this.pendingDisplacement;
                }
            }));
        }
        

        ,handleRowClick: function( params ) {
            this.emitAlways( "clicked", { state: params } );
        }
        
        ,handleRowInspect: function( params ) {
            this.emitAlways( "inspect", { state: params } );
        }
        
        ,handleRowSelection: function( params ) {
            params.index = this.grid.model.idToIndex( params["new"] );
            this.emitAlways( "select", { state: params } );
        }
        
        ,selectRow: function( row ) {
            //console.log( "select", row, "selected", this.selectedRow, this.selectedRow == row );
            if( this.selectedRow == undefined || ( row && this.selectedRow.id != row.id ) ) {
                if( this.selectedRow ) {
                    this.selectedRow.deselect();
                    this.emit( "row-deselect", { row: this.selectedRow, bubbles: false });
                }
                //console.log( "selected", row );
    
                this.selectedRow = row;
        
                if( row ) {
                    row.select();
                    this.emit( "row-select", { row: row, bubbles: false } );
                }
            }
        }
        
        ,clearSelection: function() {
            var oldID = this.selectedRow ? this.selectedRow.item()[ this.idAttribute ] : null;
            this.handleRowSelection({ 
                "old": oldID
                ,"new": null 
                ,rowIndex: null
                ,columnIndex: null
                ,x: null
                ,y: null
            });
        }
        
        ,deselectRows: function() {
            var rows = this.grid.select.row.getSelected();
            for( var i = 0; i < rows.length; i++ ) {
                this.grid.select.row.deselectById( rows[ i ] );
                // console.log( "deselecting", rows[ i ] );
                this.emit( "row-deselect", { row: this.grid.row( rows[ i ] ), bubbles: false });
            }
            //this.emit( "row-deselect-all", { bubbles: false });
            delete this.selectedRow;
        }
        
        ,_setSelectedKeyAttr: function( value ) {
            if( value != null ) {
                var index = this.grid.model.idToIndex( value );
                if( index >= 0 ) {
                    this.set( "selectedIndex", index );
                } else {
                    console.error( "Cannot locate row key '" + value + "'" );
                }
            } else {
                this.deselectRows();
            }
        }

        ,_setSelectedIndexAttr: function( index ) {
            //console.log( "selecting", index );
            if( index != null && index >= 0 ) {
                if( this.grid.vScroller ) {
                    this.grid.vScroller.scrollToRow( index ).then( lang.hitch( this, function(){
                        this.selectRow( this.grid.row( index ) );
                    }));
                } else {
                    this.selectRow( this.grid.row( index ) );
                }
            } else {
                this.deselectRows();
            }
        }
        
        ,_getScrollAttr: function() {
            return this.grid.vScroller.position();
        }
        
        ,_setScrollAttr: function( value ) {
            if( this.grid.vScroller ) {
                if( typeof value == "string" ) {

                    if( value.endsWith( "%" ) ) {
                        var percent = parseInt( value.substr( 0, value.length - 1 ) );
                        var scrollMax = domGeometry.getMarginBox( this.grid.vScroller.stubNode ).h;
                        this._setScrollAttr( Math.ceil( scrollMax * percent / 100 ) );

                    } else if( value.endsWith( "px" ) ) {
                        this._setScrollAttr( parseInt( value.substr( 0, value.length - 2 ) ) );
                    
                    }
                } else if( value !== undefined && value != null ) {
                    this.grid.model.when( 0, lang.hitch( this, function(){
                        this.grid.vScroller.scroll( value );
                    }));
                }
            }
        }

        ,_setDisplacementAttr: function( value ) {
            if( this.grid ) {
                if( "scroll" in value ) {
                    this.set( "scroll", value.scroll );
                }
                if( "selectedIndex" in value ) {
                    //console.log( "selecting from displacement", value.selectedIndex );
                    this.set( "selectedIndex", value.selectedIndex );
                } else if( "selectedKey" in value ) {
                    this.set( "selectedKey", value.selectedKey );
                }
            } else {
                this.pendingDisplacement = value;
            }
        }
        
        ,_setScrollableAttr: function( value ) {
            domClass.removeAll( this.domNode, /scroll-.*/ );
            if( value ) {
                domClass.add( this.domNode, "scroll-" + value )
            }
            
            this.rows.scrollable = value;
            
            if( this.grid ) {
                this.grid.resize();
            }
        }

        ,joinForm: function() {
            this.inherited( arguments );
            this.announceEvent( "select" );
            this.announceEvent( "inspect" );
            this.announceEvent( "clicked", { rename: "click" } );
        }
        
        ,leaveForm: function() {
            this.retireEvent( "clicked" );
            this.retireEvent( "inspect" );
            this.retireEvent( "select" );
            this.inherited( arguments );
        }
        
        ,gatherGridState: function() {
            var state = this.inherited( arguments );
            if( this.grid.vScroller ) {
                state[ "clientScroll" ] = this.get( "scroll" );
            }
            return state;
        }
        
        ,setGridState: function( state ) {
            if( "scroll" in state && state.scroll == "0px" ) {
                this.grid.vScroller.scrollToRow( 0 );
            }
            
            this.inherited( arguments );
            
            if( "scroll" in state && state.scroll != "0px" ) {
                this.set( "scroll", state.scroll );
            }
            
            if( "scrollable" in state ) {
                this.set( "scrollable", state.scrollable );
            }
            
            if( "bodyDividers" in state ) {
                this.set( "bodyDividers", state.bodyDividers );
            }

            if( "selectedIndex" in state ) {
                this.deselectRows();
                var index = state.selectedIndex;
                // this.grid.model.when( index, lang.hitch( this, function(){
                //     this.set( "selectedIndex", index );
                // }));
                setTimeout( lang.hitch( this, function(){
                    this.set( "selectedIndex", index );
                }), 0 );
                
            } else if( "selectedKey" in state ) {
                this.set( "selectedKey", state.selectedKey );

            } else if( "selectingIndex" in state ) {
                var idx = state.selectingIndex;
                on.once( "reloaded", lang.hitch( this, function(){
                    this.grid.model.when( idx, lang.hitch( this, function(){
                        //console.log( "SELECTING INDEX", idx  );  
                        this.set( "selectedIndex", idx );
                    }));
                }));
            }
        }

        ,updateRows: function( rows ) {
            if( rows && rows.length ) {
                for( var r = 0; r < rows.length; r++ ) {
                    var data = rows[ r ];
                    if( data && data.id ) {
                        var id = data.id;
                        delete data.id;
                        var row = this.grid.row( id );
                        if( row ) {
                            row.setRawData( data );
                        }
                    }
                }
            }
        }   
        
        ,repaintRow: function( id, data ) {
            var lockCount = ( this.grid.columnLock && this.grid.columnLock.count ) || 0;

            var row = this.grid.row( id );
            row.setRawData( data ).then( lang.hitch( this, function(){
                var vi = row.visualIndex();

                if( row.node() != null && this.rowDecoration ) {
                    this.removeBanners( row );
                    
                    var cells = row.cells();
                    for( i = 0; i < cells.length; i++ ) {
                        this.grid.body.refreshCell( vi, i );
                    }
                    
                    this.renderBanners( row );
                }

                this.emit( "after-row", {
                    row: row 
                    ,bubbles: false
                });

                if( lockCount > 0 ) {
                    this.grid.resize();
                }

                if( this.grid.rowHeader ) {
                    setTimeout( lang.hitch( this.grid.rowHeader, this.grid.rowHeader._onResize), 100 );
                }
            }))
            
        }
        
        ,renderBanner: function( banner, cls, colspan ) {
            return '<td'
                + ( cls ? ( ' class="' + cls + '"' ) : "" )
                + ( colspan ? ( ' colspan="' + colspan + '"' ) : "" )
                + ( banner.halign ? ( ' align="' + banner.halign + '"' ) : "" )
                + ( banner.valign ? ( ' valign="' + banner.valign + '"' ) : "" )
                + ( banner.style ? ( ' style="' + banner.style + '"' ) : "" )
                + '>' + banner.content + '</td>';
        }

        ,removeBanners: function( row ) {
            query( ".gridxRowBanner", row.node() ).orphan();
        }
        
        ,renderBanners: function( row ) {
            
            if( this.rowDecoration ) {
                if( this.rows.lastRowWidth === undefined ) {
                    this.rows.lastRowWidth = this.calculateRowWidth();
                } 
                var decoration = row.item()[ ROW ];
                if( decoration ) {
                    var interim = decoration[ INTERIM ];
                    if( interim ) {
                        domConstruct.create( "table", {
                                "class": "gridxRowBanner"
                                ,"style": { "width": this.rows.lastRowWidth + "px" }
                                ,"innerHTML": "<tr>" + this.renderBanner( interim, "gridxRowBannerInterim" ) + "</tr>"
                            }
                            ,row.node()
                            ,"first"
                        );
                        query( ".gridxRowTable", row.node() ).style( "display", "none" );
                    } else {
                        
                        var before = decoration[ BEFORE ];
                        var after = decoration[ AFTER ];
                        if( before || after ) {
                            var colspan = "" + this.grid.columnCount();
                            var tableBody = query( ".gridxRowTable > tbody", row.node() )[ 0 ];
                            
                            if( before ) {
                                domConstruct.create( "tr", {
                                        "class": "gridxRowBanner"
                                        ,"innerHTML": this.renderBanner( before, "gridxRowBannerBefore", colspan )
                                    }
                                    ,tableBody
                                    ,"first"
                                );
                            }

                            if( after ) {
                                domConstruct.create( "tr", {
                                        "class": "gridxRowBanner"
                                        ,"innerHTML": this.renderBanner( after, "gridxRowBannerAfter", colspan )
                                    }
                                    ,tableBody
                                    ,"last"
                                );
                            }
                        }
                        
                        
                    }
                }
            }
            
        }
        
        ,updateBanners: function() {
            this.rows.lastRowWidth = this.calculateRowWidth();
            query( "table.gridxRowBanner", this.grid.bodyNode ).style( "width", this.rows.lastRowWidth + "px" );
        }
        
        ,calculateRowWidth: function() {
            var columns = this.grid.columns();
            var total = 0;
            for( var i = 0; i < columns.length; i++ ) {
                total += domGeometry.getMarginBox( columns[ i ].headerNode() ).w;
            }
            return total;
        }
        
        ,rowHeaderNode: function( row ) {
			if( this.grid.rowHeader){
				return query( '[rowid="' + this.grid._escapeId( row.id ) + '"].gridxRowHeaderRow', this.grid.rowHeader.bodyNode )[ 0 ];
			} else {
			    return undefined;
			}
        }

        ,_setBodyDividersAttr: function( value ) {
            if( this.grid !== undefined ) {
                domClass.removeAll( this.domNode, /divider-.*/ );
                if( value ) {
                    domClass.add( this.domNode, "divider-" + value );
                }
            }
            this._set( "bodyDividers", value );
        }

    });
    
});
