define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/Deferred"
], function( _require, declare, lang, Deferred  ){
    
    return declare( [], {
        maxCachedPages: 20
        ,pageSize: 100
        ,localCache: true
        
        ,constructor: function( params ) {
            //TODO: richiedere con bubbleMessage
            this.form = params.form.getMainForm();
            this.manager = params.manager;
            if( params.pageSize != undefined ) {
                this.pageSize = params.pageSize;
            }

            if( this.form == undefined ) {
                throw new Error( "Missing form in query engine" );
            } else if( this.manager == undefined ) {
                throw new Error( "Missing manager in query engine" );

            }
        }

        ,query: function( params ) {
            var d = new Deferred();
            var sort = null;

            // Verifica la richiesta di ordinamento
            if( params.sort != undefined ) {
                if( params.sort.length == 1 ) {
                    sort = {
                        attribute: params.sort[0].attribute
                        ,direction: params.sort[0].descending ? -1 : 1
                    } 
                }
            }

            var count = params.count;
            var start = params.start;
            var end = params.start + params.count - 1;
            //console.log( "Requesting from", start, "to", end, ", ", count, "element" );

            var commandParams = {
                start: start
                ,count: count
            };
            
            if( sort != null ) {
                commandParams.sort = sort;
            }
            
            this.form.sendServerCommand({
                target: this.manager
                ,command: "getSlice"
                ,params: commandParams

            }).then( lang.hitch( this, function( items ) {
                // console.log( "Received" );
                if( items.error != undefined ) {
                    // if( items.error.stack != undefined ) {
                    //     console.error( items.error.stack );
                    // } else {
                    //     console.error( items.error.message );
                    // }
                    d.reject( items.error );
                } else {
                    var results = items.data;
                    results.total = items.total;
                    d.resolve( results ); 
                }
            }));                          

            return d;
        }
        
    });
    
});