define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/store/Memory"
    ,"gridx/core/model/cache/Sync"
    ,"./BaseStorage"

], function( _require, declare, Memory, Sync, BaseStorage ){
    
    return declare( [ BaseStorage ], {
        
        preCreateGrid: function() {
            this.inherited( arguments );
            
            // Initial store
            this.data = this.data || [];
            this.store = new Memory({
                data: this.data
            });
            this.cacheClass = Sync;
            delete this.data;
        }
        
        ,_setItemsAttr: function( items ) {
            this.emit( "reloading", { bubbles: false } );
            this.grid.setStore( new Memory({
                data: items
            }));
            this.selectedRow = null;
            this.emit( "reloaded", { bubbles: false } );
        }
        
        ,setGridState: function( state ) {
            if( "items" in state ) {
                this.set( "items", state.items );
            }
            this.inherited( arguments );
        }

    });
    
});