define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/parser"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/query"
    ,"works/registry"
    ,"works/lang/Set"
    ,"gridx/modules/Dod"

], function( _require, declare, lang, array, parser, domClass, domStyle, query, registry, Set, Dod ){
    
    return declare( [], {

        postMixInProperties: function() {
            this.inherited( arguments );

            var cfg = this.getConfiguration( "details" );
            
            this.details = {
                expandMultiple: cfg.get( "expandMultiple", true )
                ,expandOnClick: cfg.get( "expandOnClick", false )
                ,autoDestroy: cfg.get( "autoDestroy", false )
                ,animation: cfg.get( "animation", true )
                ,duration: cfg.get( "duration", 100 )
                ,asyncResizeTreshold: cfg.get( "asyncResizeTreshold", 1 )
                ,template: this.getFragment( cfg.get( "template", "<no template>" ) )
                ,shown: new Set()
                ,resizables: new Set()
            }
        }

        ,preCreateGrid: function() {
            this.inherited( arguments );
                
            this.modules.push({
                moduleClass: Dod
				,defaultShow: false
				,useAnimation: this.details.animation
                ,duration: this.details.duration
				,showExpando: true
				,detailProvider: lang.hitch( this, this.details.template.type == "dojo" ? this.formatAndParseDetailTemplate : this.formatDetailTemplate )
            });
        }
        
        ,formatDetailTemplate: function( grid, rowId, detailNode, rendered ) {
            detailNode.innerHTML = lang.replace( this.details.template.body, grid.row( rowId ).item() );
            rendered.callback();
        }

        ,formatAndParseDetailTemplate: function( grid, rowId, detailNode, rendered ) {
            this.closePreviousDetails();
            setTimeout( lang.hitch( this, function(){
                var html = lang.replace( this.details.template.body, grid.row( rowId ).item() );
                detailNode.innerHTML = html;
                parser.parse( detailNode ).then( lang.hitch( this, function( widgets ) {
                    rendered.callback();
                }));
            }), 2000 );
        }
        
        ,registerOpenedDetail: function( id ) {
            this.details.lastExpanded = id;
        }
        
        ,closePreviousDetails: function() {
            if( !this.details.expandMultiple ) {
                if( this.details.lastExpanded != undefined ) {
                    this.grid.dod.hide( this.grid.row( this.details.lastExpanded ) );
                }
            }
        }
        
        ,postCreateGrid: function() {
            this.inherited( arguments );
            
            // Show
            this.wire( this.grid.dod, "onShow", function( row ){
                //console.log( "Show", row.id, "last", this.details.lastExpanded );
                if( this.details.resizables.contains( row.id ) ) {
                    this.details.resizables.remove( row.id );
                    lang.hitch( this.grid.dod._row( row.id ), this.resizeDetailRow )();
                }
                
                this.registerOpenedDetail( row.id );
                this.details.shown.add( row.id );
            });
            
            // AutoDestroy
            if( this.details.autoDestroy ) {
                this.wire( this.grid.dod, "onHide", function( row ) {
                    setTimeout( lang.hitch( this.grid.dod._row( row.id ), function(){
                        array.forEach( registry.findWidgets( this.dodNode ), function( widget ){
                            widget.destroyRecursive( false );
                        });
                    
                        this.dodNode.parentNode.removeChild( this.dodNode );
                        this.dodLoadingNode.parentNode.removeChild( this.dodLoadingNode );
                        delete this.dodNode;
                        delete this.dodLoadingNode;
                        this.dodLoaded = false;
                    }), this.details.duration + 200 ); //distanzia la distruzione dalla possibile creazione in atto 
                });
            }
            
            // Resize
            this.wire( this.grid, "resize", function( ){
               this.details.resizables.clear();
               this.details.shown.forEach( function( rowId ){
                   // var node = this.grid.row( rowId ).node();
                   // if( domClass.contains( node, "gridxDodShown" ) ) {
                   var rowsToResize = [];    
                   var dodRow = this.grid.dod._row( rowId );
                   if( dodRow.dodShown ) {
                       rowsToResize.push( dodRow );
                   } else {
                       this.details.resizables.add( rowId );
                   }
                   
                   // Ridimensiona in sincrono solo se le righe da trattare sono al di sotto della soglia stabilita
                   if( rowsToResize.length > 0 ) {
                       if( rowsToResize.length > this.details.asyncResizeTreshold ) {
                           for( var i in rowsToResize ) {
                               setTimeout( lang.hitch( rowsToResize[ i ], this.resizeDetailRow ), 0 )
                           }
                       } else {
                           for( var i in rowsToResize ) {
                               lang.hitch( rowsToResize[ i ], this.resizeDetailRow )();
                           }
                       }
                   }
                   
               }, this );
            });
            
            // ExpandOnClick
            if( this.details.expandOnClick ) {
                this.wire( this.grid, "onRowClick", function( evt ){
                    var row = this.grid.row( evt.rowId );
                    this.grid.dod.toggle( row );
                });
            }
        }
        
        ,resizeDetailRow: function() {
            //console.log( "resize>", this.dodNode, this );
            this.dodNode.style.width = domStyle.get( this.dodNode.parentNode, "width" ) + "px";
            array.forEach( registry.findWidgets( this.dodNode ), function( widget ){
                if( lang.isFunction( widget[ "resize" ] ) ) {
                    widget.resize();
                }
            });
        }
        
    });
    
});