define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/topic"
    ,"dojo/on"
    ,"dojo/parser"
    ,"dojo/has"
    ,"dojo/dom-construct"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/query"
    ,"dojo/Deferred"
    ,"dojo/promise/all"
    ,"works/registry"
    ,"works/layout/ContentPane"

], function( _require, declare, lang, array, topic, on, parser, has, domConstruct, domClass, domStyle, domGeometry, query, Deferred, all, registry, ContentPane  ){
    
    var Engine = declare( [], {
        constructor: function( manager ) {
            this.manager = manager;
            this.depotNode = manager.detailDepotNode;
            this.detailNode = this.depotNode.firstChild;
            this.parseDeferred = new Deferred();
            this.parseDeferred.resolve();
            this.drawn = false;
            this.blank = ( this.detailNode == undefined );
            //this.resizeSubscription = on( this.manager, "grid-resize", lang.hitch( this, this.resize ) );
            this.hoverSubscription = on( this.detailNode, "mouseover", lang.hitch( this, function(){
                this.manager.emit( "detail-hover", { bubbles: false });
            }));
            
        }
        
        ,destroy: function() {
            // if( this.resizeSubscription ) {
            //     this.resizeSubscription.remove();
            //     delete this.resizeSubscription;
            // }
            if( this.hoverSubscription ) {
                this.hoverSubscription.remove();
                delete this.hoverSubscription;
            }
        }
        
        ,resize: function( params ) {
            //console.log( "Detail resize", this.manager.grid.id );
            if( this.drawn ) {
                if( this.detailNode.parentNode != this.depotNode ) {
                    //console.log( "engine resize", this.drawn );
                    var dodNode = this.detailNode.parentNode;
                
                    if( dodNode && dodNode.parentNode && domClass.contains( dodNode.parentNode, "gridxDodShown" ) ) {
                        //console.log( "dod parent", dodNode.parentNode );
                        var parentBox = domGeometry.getMarginBox( dodNode.parentNode );
                        //console.log( "dod mb", parentBox );
                        if( parentBox.w && parentBox.w > 0 ) {
                            //dodNode.style.width = parentBox.w + "px";
                            domStyle.set( dodNode, "width", parentBox.w + "px" );

                            var leftPadding = domGeometry.getPadExtents( dodNode.parentNode ).l;
                            var horizontalScroll = ( this.manager.grid.hScrollerNode && this.manager.grid.hScrollerNode.scrollLeft ) || 0;
                            var marginLeft = horizontalScroll - leftPadding;
                            domStyle.set( dodNode, "margin-left", marginLeft != 0 ? ( marginLeft + "px" ) : null );
                            
                            
                            //console.log( "detail first", this.detailNode.firstChild );
                            var firstWidget = registry.byNode( this.detailNode.firstChild );
                            // console.log( "dod resize on resize" );
                            if( firstWidget && lang.isFunction( firstWidget.resize ) ) {
                                firstWidget.resize();
                            }
                        }
                    }
                
                
                }
            }
            
        }
        
        ,draw: function( params ) {
            //console.log( "draw", params );
            var deferred = new Deferred();
            if( this.detailNode.firstChild ) {
                this.parseDeferred.then( lang.hitch( this, function(){
                    params.node.appendChild( this.detailNode );
                    var panel = registry.byNode( this.detailNode.firstChild );
                    var form = panel.getDescendants()[ 0 ];
                    form.domNode.style.height = null;
                    form.domNode.style.width = null;

                    // In caso di scroll veloce, la form di dettaglio 
                    // potrebbe perdere il genitore
                    form.$parentform = this.manager.form;
                    
                    //console.log( "dod resize on draw" );
                    form.resize();
                    this.drawn = true;
                    deferred.resolve();
                }));
                
            } else {
                this.drawn = true;
                deferred.resolve();
            }
            return deferred;
        }
        
        ,clean: function( params ) {
            var deferred = new Deferred();
            if( params.node.firstChild == this.detailNode ) {
                this.depotNode.appendChild( this.detailNode );
            }
            setTimeout( function(){ deferred.resolve(); }, 0 );
            return deferred;
        }

        ,store: function( detail ) {
            //console.log( "storing", detail );
            this.drawn = false;
            this.erase();
            if( detail ) {
                this.parse( detail );
            }
        }

        ,erase: function() {
            if( this.detailNode.firstChild ) {
                var panel = registry.byNode( this.detailNode.firstChild );
            
                //ricerca parent
                if( panel ) {
                    if( panel.isInstanceOf( ContentPane ) ) {
                        panel.unregisterContent();
                        panel.destroyRecursive();
                
                    } else {
                        throw new Error( "Cannot unregister existing detail content" );
                    }
                }
                this.detailNode.innerHTML = "";
            }
            this.blank = true;
        }

        ,parse: function( source ) {
            this.parseDeferred = new Deferred();
            this.blank = false;
            
            // Riporta il nodo temporaneamente in una posizione in cui possa
            // ricostruire correttamente la gerarchia di form
            this.depotNode.appendChild( this.detailNode );
            
            this.detailNode.innerHTML = source;
            delete this.detailSource;
        
            parser.parse( this.detailNode ).then( lang.hitch( this, function( widgets ) {
                var panel = widgets[ 0 ];

                if( panel != undefined ) {

                    this.manager.form.registerWidget( panel );

                    if( panel.isInstanceOf( ContentPane ) ) {
                        panel.registerContent();
                    } else {
                        throw new Error( "Cannot register new detail content" );
                    }

                    /* Aspetta la form nel caso ci siano moduli non caricati */
                    var form = panel.getDescendants()[ 0 ];
                    var descendantDeferreds = array.map( array.filter( panel.getDescendants(), function( widget ){
                        return "onLoadDeferred" in widget;
                    }), function( w ){
                        return w.onLoadDeferred;
                    });

                    all( descendantDeferreds ).then( lang.hitch( this, function(){
                        form.domNode.style.height = null;
                        this.parseDeferred.resolve();
                    }));


                } else {
                    this.parseDeferred.resolve();
                }
            }));
            
        }
        
    });
    
    return declare( [], {
        
        preCreateGrid: function() {
            this.detailDepotNode = query( ".detailDepotNode", this.domNode )[ 0 ];
            this.inherited( arguments );
        }
        
        ,postCreateGrid: function() {
            this.inherited( arguments );
            if( this.details != undefined ) {
                this.details.engine = new Engine( this );
            } else {
                console.error( "Missing details module" );
            }
        }
        
        ,joinForm: function() {
            this.inherited( arguments );
            if( this.startupDetail ) {
                this.details.engine.store( this.startupDetail );
                delete this.startupDetail;
            }
        }
        
        ,leaveForm: function() {
            this.details.engine.destroy();
            this.inherited( arguments );
        }
        
        ,_setServerDetailAttr: function( value ) {
            this.details.engine.store( value );
        }
        
        ,setGridState: function( state ) {
            if( "serverDetail" in state ) {
                this.set( "serverDetail", state.serverDetail );
            }
            this.inherited( arguments );
        }
        
    });
    
});
