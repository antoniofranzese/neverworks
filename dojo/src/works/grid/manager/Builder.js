define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/aspect"
    ,"dojo/on"
    ,"dojo/dom-construct"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/Deferred"
    ,"dijit/layout/ContentPane"
    ,"gridx/Grid"
    ,"../decorator/DecoratorRegistry"
    ,"works/layout/_FixedSizeMixin"
    ,"works/layout/_ParentQualifyMixin"
     
], function( _require, declare, lang, array, aspect, on, domConstruct, domStyle, domGeometry, domClass, Deferred, ContentPane, Grid, DecoratorRegistry, _FixedSizeMixin, _ParentQualifyMixin ){
    
    var Config = declare( [], {
        constructor: function( cfg ) {
            this.config = cfg || {};
        }
        
        ,get: function( key, default_ ) {
            if( key in this.config && this.config[ key ] !== undefined && this.config[ key ] != null ) {
                return this.config[ key ];
            } else {
                return default_;
            }
        }
    })
    
    return declare([ ContentPane, _FixedSizeMixin, _ParentQualifyMixin ], {
        
        configuration: {}
        ,columns: {}
        ,selectedRow: null
        ,idAttribute: "id"
        ,pageSize: 100
        ,header: true
        ,autoHeight: false
        ,autoDeselect: false
        ,rowDecoration: false
        ,decorationPriority: [ "cell", "row", "column" ]

        ,constructor: function() {
            this.creationDeferred = new Deferred();
        }
        
        ,contentReady: function() {
            return this.creationDeferred;
        }
        
        ,buildRendering: function() {
            this.inherited( arguments );
            // Patch per errore in _WidgetBase
            this.focusNode = this.domNode;
        }

        ,getConfiguration: function( name ) {
            return new Config( this.configuration[ name ] );
        }

        ,getFragment: function( name ) {
            if( name in this.fragments ) {
                return this.fragments[ name ];
            } else {
                throw new Error( "Missing fragment '" + name + "'" );
            }
        }
        
        ,postCreate: function() {
            this.inherited( arguments );

            if( this.autoHeight ) {
                domClass.add( this.domNode, "autoHeight" );
            }

            this.preCreateGrid();
            
            // Decorator factory
            for( var c = 0; c < this.columns.length; c++ ) {
                var column = this.columns[ c ];
                if( column.decoratorParams === undefined && ( this.rowDecoration || column.titleStyle ) ) {
                    column.decoratorParams = { "$type": "base" };
                }
                if( column.decoratorParams != undefined  ) {
                    var params = column.decoratorParams;
                    if( params.$type !== undefined ) {
                        var type = params.$type;
                        delete params.$type;
 
                        if( DecoratorRegistry.contains( type ) ) {
                            var decoratorClass = DecoratorRegistry.decorator( type );
                            var decorator = new decoratorClass( params );

                            (function( col, dec ) {
                                col.decorator = function( data, rowId, visualIndex, cell ) {
                                    return dec.decorate( data );
                                }
                                delete col.decoratorParams;
                                col.decoratorInstance = dec;
                                
                            })( column, decorator );

                        } else {
                            console.error( "Unknown decorator:", type );
                        }

                    } else {
                        console.error( "Untyped decorator factory for column:", column );
                    }
                }
            }
            

            this.gridNode = domConstruct.create( "div", { id: this.id + "_gridNode" }, this.containerNode );
            
            this.grid = new Grid({
                cacheClass: this.cacheClass,
                store: this.store,
                structure: this.columns,
                pageSize: this.pageSize,
                modules: this.modules,
                headerHidden: ( this.header == false ),
                autoHeight: ( this.autoHeight == true )
            }, this.gridNode );
            
            // Decorators references
            var decs = []
            for( var c = 0; c < this.grid.columnCount(); c++ ) {
                var dec = this.grid.column( c ).decoratorInstance;
                if( dec != undefined ) {
                    dec.set( "columnIndex", c );
                    dec.set( "manager", this );
                    decs.push( dec );
                }
            }
            
            // Decorators startup
            for( var d = 0; d < decs.length; d++ ) {
                decs[ d ].startup();
            }
            
            var describeCellClick = function( grid, evt ) {
                var column = evt.columnId !== undefined ? grid.column( evt.columnId ) : undefined;
                var attributes = undefined;
                if( column && column.def().clickAttributes == true ) {
                    var node = document.elementFromPoint( evt.clientX, evt.clientY );
                    if( node ) {
                        attributes = {};
                        for( var i = 0; i < node.attributes.length; i++ ) {
                            if( node.attributes[ i ].name.startsWith( "data-attribute-" ) ) {
                                attributes[ node.attributes[ i ].name.substring( 15 ) ] = node.attributes[ i ].value;
                            }
                        }
                    }
                }
                return {
                    column: column
                    ,attributes: attributes
                }
            }
            
            this.wire( this.grid, "onRowClick", lang.hitch( this, function( evt ) { 
                var desc = describeCellClick( this.grid, evt );
                this.emit( "row-clicking", { 
                    row: this.grid.row( evt.rowId )
                    ,column: desc.column
                    ,attributes: desc.attributes
                    ,x: Math.floor( evt.clientX )
                    ,y: Math.floor( evt.clientY )
                    ,bubbles: false
                });
            }));

            this.wire( this.grid, "onCellContextMenu", lang.hitch( this, function( evt ) { 
                var desc = describeCellClick( this.grid, evt );
                evt.preventDefault();
                this.emit( "row-context", {
                    row: this.grid.row( evt.rowId )
                    ,column: desc.column
                    ,attributes: desc.attributes
                    ,x: Math.floor( evt.clientX )
                    ,y: Math.floor( evt.clientY )
                    ,bubbles: false
                });
            }));

            this.wire( this.grid, "onHeaderCellContextMenu", lang.hitch( this, function( evt ) { 
                evt.preventDefault();
                //console.log( "Header", evt.columnId );
                this.emit( "header-context", {
                    column: this.grid.column( evt.columnId )
                    ,x: Math.floor( evt.clientX )
                    ,y: Math.floor( evt.clientY )
                    ,bubbles: false
                });
            }));
            
            this.wire( this.grid.columnWidth, "onUpdate", lang.hitch( this, function() { 
                this.emit( "column-resize", {
                    bubbles: false
                });
            }));

            this.wire( this.grid, "onBlur", lang.hitch( this, function() { 
                this.emit( "grid-blur", {
                    bubbles: false
                });
            }));

            if( this.grid.sort !== undefined ) {
                this.own(
                    aspect.before( this.grid.sort, "sort", lang.hitch( this, function(){
                        this.emit( "column-sorting", { bubbles: false } );
                    }))
                )
            }
            
            this.connect( this.grid.hScrollerNode, "onscroll", function( evt ){
                this.emit( "scroll-horizontal", { bubbles: false } );
            });

            // if( ! this.horizontalScroller ) {
            //     query( ".gridxHScroller", this.domNode ).style( "display", "none" )
            // }

            this.postCreateGrid();
            
            this.creationDeferred.resolve();
        }

        ,preCreateGrid: function() {
        }
        
        ,postCreateGrid: function() {

        }
        
        ,setBuilderGridState: function( state ) {
            if( state.eventQueue !== undefined ) {
                array.forEach( state.eventQueue, function( event ){

                    if( event.state.rowIndex !== undefined && event.state.rowIndex !== null ) {
                        event.state.row = this.grid.row( event.state.rowIndex );
                        event.state.rowId = event.state.row.id;
                    }

                    if( event.state.columnIndex !== undefined && event.state.columnIndex !== null ) {
                        event.state.column = this.grid.column( event.state.columnIndex );
                        event.state.columnId = event.state.column.id;
                    }

                    event.state.bubbles = false;
                    this.emit( event.name, event.state );

                }, this );
                delete state.eventQueue;
            }
        }
        
        ,startup: function() {
            this.inherited( arguments );
            this.own(
                on( this.domNode, "mouseenter", lang.hitch( this, function(){
                    this._set( "hovering", true );
                }))
                ,on( this.domNode, "mouseleave", lang.hitch( this, function(){
                    this._set( "hovering", false );
                }))
                
            );
        }
        
        ,wire: function( source, event, callback ) {
            if( source ) {
                this.own( 
                    aspect.after( source, event, lang.hitch( this, callback ), /*receiveArguments*/ true )
                );
            }
        }
        
        ,connect: function( source, event, callback ) {
            if( this.grid && source ) {
                this.own(
                    this.grid.connect( source, event, lang.hitch( this, callback ) )
                );
            }
        }
        
        ,joinForm: function( form ) {
            var self = this;
            form.ready().then( function(){ 
                setTimeout( function(){ 
                    self.resize();
                }, 0 ) 
            } );

            this.inherited( arguments );

            this.form = form;
            this.grid.startup();
        }
        
        ,resize: function( size ) {
            if( ! this.cellresizing ) {
                var processed = this.processFixedSize( size );
                domGeometry.setMarginBox( this.domNode, processed );
                this.resizeGrid( processed );
            }
        }
        
        ,resizeGrid: function( size ) {
            if( size == undefined ) {
                size = domGeometry.getMarginBox( this.domNode );
            }
            
            var borders = domGeometry.getPadBorderExtents( this.domNode );

            this.grid.resize({
                w: size.w - borders.w
                ,h: size.h - borders.h 
            });

            this.emit( "grid-resize", { bubbles: false } );
        }
        
        ,cellResizing: function( parent ) {
            if( this.elasticWidth === undefined && this.fixedWidth === undefined ) {
                this.elasticWidth = 100;
            }
            this.cellresizing = true;
            if( this.elasticWidth ) {
                this.domNode.style.width = "1px";
            }
            if( this.elasticHeight ) {
                this.domNode.style.height = "1px";
            }
            return this.domNode;
        }

        ,cellResize: function( size ) {
            this.cellresizing = false;
            this.resize( this.processElasticSize( size ) );
        }

        ,leaveForm: function() {
            delete this.form;
            this.inherited( arguments );
        }
        
        ,inspect: function() {
            this.emit( "grid-inspect", { bubbles: false } );
        }
        
    });
    
});
