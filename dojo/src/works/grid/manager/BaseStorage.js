define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
], function( _require, declare, array ){
    
    var ROW = ".r";
    var FORBIDDEN = [ "id" ];
    
    return declare( [], {
        _setChangedItemsAttr: function( items ) {
            this.updateItems( items, true );
        }

        ,updateItems: function( items, cleanup ) {
            array.forEach( items, function( item ) {
                var row = this.grid.row( item.id );
                if( row ) {
                    var rawData = row.rawData();
                    var dotted = [];
                    for( var name in item ) {
                        if( FORBIDDEN.indexOf( name ) < 0 ) {
                            rawData[ name ] = item[ name ];
                        }
                        if( name.startsWith( "." ) ) {
                            dotted.push( name );
                        }
                    }
                    
                    if( cleanup ) {
                        for( var name in rawData ) {
                            if( name.startsWith( "." ) && dotted.indexOf( name ) < 0 ) {
                                delete rawData[ name ];
                            }
                        }
                    }

                    this.repaintRow( item.id, rawData );
                } 
            }, this );
        }

        ,setGridState: function( state ) {
            if( "changedItems" in state ) {
                this.set( "changedItems", state.changedItems );
            }
            this.inherited( arguments );
        }

    });
    
});