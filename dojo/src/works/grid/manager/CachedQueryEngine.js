define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/Deferred"
], function( _require, declare, lang, Deferred  ){
    
    return declare( [], {
        maxCachedPages: 20
        ,pageSize: 100
        ,currentSort: { attribute: "", direction: 0 }
        ,localCache: true
        
        ,constructor: function( params ) {
            //TODO: richiedere con bubbleMessage
            this.form = params.form.getMainForm();
            this.manager = params.manager;
            this.pageCache = [];
            if( params.pageSize != undefined ) {
                this.pageSize = params.pageSize;
            }

            if( this.form == undefined ) {
                throw new Error( "Missing form in query engine" );
            } else if( this.manager == undefined ) {
                throw new Error( "Missing manager in query engine" );

            }
        }

        ,query: function( params ) {
            var d = new Deferred();
            
            // Verifica la richiesta di ordinamento
            if( params.sort != undefined ) {
                if( params.sort.length == 1 ) {
                    var sort = {
                        attribute: params.sort[0].attribute
                        ,direction: params.sort[0].descending ? -1 : 1
                    } 
                    
                    // Se è cambiato l'ordinamento, elimina la cache
                    if( sort.attribute != this.currentSort.attribute || sort.direction != this.currentSort.direction ) {
                        //console.log( "Cleaning cache" );
                        this.pageCache = [];
                    }
                    
                    this.currentSort = sort;
                }
            }

            var count = params.count;
            var start = params.start;
            var end = params.start + params.count - 1;
            //console.log( "Cached requesting from", start, "to", end, ", ", count, "element" );

            // Verifica se c'è una pagina in cache che contiene una sottolista
            // finale della query richiesta
            var endingChunk = this.findBestMatchingChunk( start, end ); 
            if( endingChunk != undefined ) {
                //console.log( "Narrowing request from", start, "for", endingChunk.start - start, "items" );
                count = endingChunk.start - start;
            }

            // Se count <= 0 il chunk contiene l'intero intervallo richiesto
            // si evita la request
            if( count > 0 ) {

                // Se non e' stata richiesta una pagina intera, calcola le righe da
                // richiedere in piu' per completare la pagina
                var surplus = count < this.pageSize ? this.pageSize - count : 0;
                if( surplus > start ) {
                    surplus = start;
                }
                count += surplus;
                start = start - surplus;
                //console.log( "Using start surplus of", surplus, "items, actual start", start );
                
                var commandParams = {
                    start: start
                    ,count: count
                };
                
                if( this.currentSort.attribute != "" ) {
                    commandParams.sort = this.currentSort;
                }
                
                this.form.sendServerCommand({
                    target: this.manager
                    ,command: "getSlice"
                    ,params: commandParams

                }).then( lang.hitch( this, function( items ) {
                    // console.log( "Received", items );
                    if( items.error != undefined ) {
                        // if( items.error.stack != undefined ) {
                        //     console.error( items.error.stack );
                        // } else {
                        //     console.error( items.error.message );
                        // }
                        d.reject( items.error );
                    } else {
                        var results = items.data;
                
                        if( endingChunk != undefined ) {
                            //console.log( "Appending", endingChunk.data.length, "items from cache" );
                            results = results.concat( endingChunk.data );
                        }

                        // In cache va la pagina completa del surplus iniziale e delle aggiunte finali
                        this.pageCache.push({
                            start: start
                            ,end: start + results.length - 1
                            ,data: results
                            ,total: items.total
                        });
                
                        // Elimina le prima pagine se sono troppe
                        if( this.pageCache.length > this.maxCachedPages ) {
                            this.pageCache = this.pageCache.slice( this.pageCache.length - this.maxCachedPages );
                        }
                    
                        // Toglie in testa le righe di surplus per restituire il risultato
                        // corretto secondo lo start originariamente richiesto
                        if( surplus > 0 ) {
                            results = results.slice( surplus );
                        }
                        if( results.length != params.count ) {
                            console.warn( "Result length mismatch, requested", params.count, ", actual", results.length );
                        }
                        results.total = items.total;
                        d.resolve( results ); 
                    }
                }));                          

            } else {
                // L'intervallo e' completamente disponibile in cache
                //console.log( "Full cache response", endingChunk.data.length, params.count );
                var results = endingChunk.data;
                if( results.length != params.count ) {
                    console.warn( "Full cache result length mismatch, requested", params.count, ", actual", results.length );
                }
                results.total = endingChunk.total;
                setTimeout( function(){
                    d.resolve( results );
                }, 0 );
            }
            

            return d;
        }
        
        ,findBestMatchingChunk: function( start, end ) {
            var lastCount = 0;
            var selectedPage = undefined;
            for( var i = 0; i < this.pageCache.length; i++ ) {
                var page = this.pageCache[ i ]
                // La pagina e' utilizzabile solo se contiene la fine della richiesta
                if( end >= page.start && end <= page.end ) {
                    
                    // Calcola il massimo contributo che questa pagina puo'
                    // dare alla request
                    var pageStart = Math.max( page.start, start );
                    var pageEnd = Math.min( page.end, end );
                    var pageCount = pageEnd - pageStart + 1;
                    
                    // Candida la pagina se ha un contributo maggiore di quello
                    // raggiunto fino a questo momento
                    if( pageCount > lastCount ) {
                        //console.log( "Candidating page", page.start, "-", page.end, "from", pageStart, "to", pageEnd, "using", pageCount, "items" );
                        selectedPage = {
                            page: page
                            ,start: pageStart
                            ,end: pageEnd
                        };
                        lastCount = pageCount;
                    } 
                }
            }
            
            if( selectedPage != undefined ) {
                //console.log( "Acquiring page", selectedPage.page.start, "-", selectedPage.page.end, "from", selectedPage.start, "to", selectedPage.end, "using", pageCount, "items" );
                var chunk = [];
                
                // Estrae la sottolista inclusa nei margini richiesti
                var startOffset = selectedPage.start - selectedPage.page.start;
                for( var i = 0; i < lastCount; i++ ) {
                    chunk.push( selectedPage.page.data[ i + startOffset ] );
                }
                return {
                    data: chunk
                    ,start: selectedPage.start
                    ,end: selectedPage.end
                    ,total: selectedPage.page.total
                }
            }
            
            return selectedPage;
        }
    })
    
})