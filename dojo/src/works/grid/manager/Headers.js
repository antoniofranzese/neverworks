define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"gridx/modules/SingleSort"
    ,"gridx/modules/ColumnResizer"
    ,"gridx/modules/ColumnLock"
    ,"works/local-style"

], function( _require, declare, on, domClass, SingleSort, ColumnResizer, ColumnLock, localStyle ){
    
    return declare( [], {
        sortable: true
        ,resizable: true
        ,scrollOnSort: true
        ,headerVisible: true
        ,lockable: false
        
        ,preCreateGrid: function() {
            this.inherited( arguments );

            if( this.sortable ) {
                this.modules.push( SingleSort );
            }
            
            if( this.resizable ) {
                this.modules.push( ColumnResizer );
            }

            if( this.lockable ) {
                this.modules.push( ColumnLock );
            }
            
        }
        
        ,postCreateGrid: function() {
            this.inherited( arguments );

            this.updateColumnLock();

            this.set( "headerVisible", this.headerVisible );
            if( this.headerColor !== undefined ) {
                this.set( "headerColor", this.headerColor );
            }
            if( this.headerBackground !== undefined ) {
                this.set( "headerBackground", this.headerBackground );
            }
            if( this.headerFont !== undefined ) {
                this.set( "headerFont", this.headerFont );
            }
            if( this.headerHalign !== undefined ) {
                this.set( "headerHalign", this.headerHalign );
            }
            if( this.headerHeight !== undefined ) {
                this.set( "headerHeight", this.headerHeight );
            }
            if( this.headerDividers !== undefined ) {
                this.set( "headerDividers", this.headerDividers );
            }

            if( this.scrollOnSort ) {
                var g = this.grid;
                g.sort.__$originalSort = this.grid.sort.sort;
                g.sort.sort = function( colId, isDescending, skipUpdateBody ) {
                    g.vScroller.scrollToRow( 0 ).then( lang.hitch( this, function(){
                        this.__$originalSort( colId, isDescending, skipUpdateBody );
                    }));
                }
            }
            
            this.own(
                on( this, "header-context", lang.hitch( this, function( evt ){
                    this.emitAlways( "inspect", { 
                        expr: "columns"
                        ,state:{
                            x: evt.x
                            ,y: evt.y 
                            ,columnIndex: evt.column.index()
                        }
                    })
                }))
            )
            
        }
        
        ,updateColumnLock: function() {
            if( this.grid.columnLock ) {
                var lock = 0;
                for( var c = 0; c < this.columns.length; c++ ) {
                    if( this.columns[ c ].lock == true ) {
                        lock++;
                    } else {
                        break;
                    }
                }

                if( lock > 0 ) {
                    this.grid.columnLock.lock( lock );
                } else {
                    this.grid.columnLock.unlock();
                }
            }
        }
        
        ,_setHeaderVisibleAttr: function( value ) {
            if( this.grid !== undefined ) {
                domClass[ value ? "remove" : "add" ]( this.grid.domNode, "worksGridNoHeader")
            }
            this._set( "headerVisible", value );
        }
        
        ,_setHeaderColorAttr: function( value ) {
            if( this.grid !== undefined ) {
                localStyle( this, "header-color" )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow", { "color": value } )
                    .write();
            }
            this._set( "headerColor", value );
        }

        ,_setHeaderBackgroundAttr: function( value ) {
            if( this.grid !== undefined ) {
                localStyle( this, "header-background" )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow", { "background-color": value } )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow .gridxLockedCell ", { "background-color": value } )
                    .write();
            }
            this._set( "headerBackground", value );
        }

        ,_setHeaderFontAttr: function( value ) {
            if( this.grid !== undefined ) {
                localStyle( this, "header-font" )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow", value )
                    .write();
            }
            this._set( "headerFont", value );
        }

        ,_setHeaderHalignAttr: function( value ) {
            if( this.grid !== undefined ) {
                localStyle( this, "header-halign" )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow td.gridxCell", value )
                    .write();
            }
            this._set( "headerHalign", value );
        }

        ,_setHeaderHeightAttr: function( value ) {
            if( this.grid !== undefined ) {
                localStyle( this, "header-height" )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow", value )
                    .write();
            }
            this._set( "headerHeight", value );
        }
        
        ,_setHeaderBorderAttr: function( value ) {
            if( this.grid !== undefined ) {
                localStyle( this, "header-border" )
                    .set( "## > .gridx > .gridxHeader > .gridxHeaderRow > .gridxHeaderRowInner .gridxCell", value )
                    .write();
            }
            this._set( "headerBorder", value );
        }

        ,_setHeaderDividersAttr: function( value ) {
            if( this.grid !== undefined ) {
                domClass.removeAll( this.grid.header.domNode, /divider-.*/ );
                if( value ) {
                    domClass.add( this.grid.header.domNode, "divider-" + value );
                }
            }
            this._set( "headerDividers", value );
        }

        ,setGridState: function( state ) {
            this.inherited( arguments );
            if( "headerVisible" in state ) {
                this.set( "headerVisible", state.headerVisible );
            } 
            if( "headerColor" in state ) {
                this.set( "headerColor", state.headerColor );
            } 
            if( "headerBackground" in state ) {
                this.set( "headerBackground", state.headerBackground );
            }
            if( "headerFont" in state ) {
                this.set( "headerFont", state.headerFont );
            }
            if( "headerHalign" in state ) {
                this.set( "headerHalign", state.headerHalign );
            }
            if( "headerDividers" in state ) {
                this.set( "headerDividers", state.headerDividers );
            }

        }
        
    });
    
});