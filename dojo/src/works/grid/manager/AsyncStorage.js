define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/aspect"
    ,"dojo/has"
    ,"gridx/modules/VirtualVScroller"
    ,"gridx/core/model/cache/Async"
    ,"./AsyncStore"
    //,"./PlainQueryEngine"
    // ,"./CachedQueryEngine"
    ,"./QueueingCachedQueryEngine"
    ,"./BaseStorage"

], function( _require, declare, aspect, has, VirtualVScroller, Async, Store, QueryEngine, BaseStorage ){
    
    return declare( [ BaseStorage ], {
        
        preCreateGrid: function() {
            this.inherited( arguments );
 
            this.modules.push({
                moduleClass: VirtualVScroller
                
                // Lazy per i browser meno prestanti 
                ,lazy: ( has( "ff" ) <= 4 )
                //,lazy: ( has( "ff" ) != undefined )
                //,lazy: true
            });
            
            // Initial store
            this.store = new Store();
            this.cacheClass = Async;
            
            // First page
            if( this.data != undefined && this.total != undefined ) {
                this.data.total = this.total;
                this.store.firstResult = this.data;
            }
        }

        ,postCreateGrid: function() {
            this.inherited( arguments );

            // Patch per impedire a Firefox di ripristinare lo stato di scroll precedente
            if( has( "ff" ) >= 4 ) {
                var scroller = this.grid.vScroller;
                aspect.before( scroller, "_init", function(){
                   var syncHeightPatch = aspect.after( scroller, "_syncHeight", function(){
                       scroller.domNode.scrollTop = 0;
                       syncHeightPatch.remove();
                   });
                });
            }
            
        }

        ,joinForm: function( form ) {
            // console.log( "Async join" );
            this.store.engine = new QueryEngine({ manager: this, form: form, pageSize: this.pageSize });
            this.inherited( arguments );
        }
        
        ,_setItemsAttr: function( items ) {
            //console.log( "Items", items );
            this.emit( "reloading", { bubbles: false } );
            var store = new Store();
            store.firstResult = items.data;
            store.firstResult.total = items.total;
            store.engine = new QueryEngine({ manager: this, form: form, pageSize: this.pageSize });
            this.grid.setStore( store );
            this.selectedRow = null;
            this.emit( "reloaded", { bubbles: false } );
        }
        
        ,setGridState: function( state ) {
            if( "items" in state ) {
                this.set( "items", state.items );
            }
            this.inherited( arguments );
        }
        
    });
    
});