define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/Deferred"
    ,"dojo/store/util/QueryResults"
    ,"works/wait"
], function( _require, declare, Deferred, QueryResults, wait ){
    
    return declare( [], {
        idProperty: "id"
        
        ,getIdentity: function( item ) {
            return item[ this.idProperty ];
        }
    	
        ,query: function( query, options ) {
            //console.log( "query", query, "options", options );
        
            if( options.start == 0 && options.sort == undefined && this.firstResult ) {
                //console.log( "Providing static first result" );
                return this.firstResult;
                // return QueryResults( this.firstResult );
            
            } else if( this.engine ) {
                //console.log( "Querying engine" );
                var results = this.engine.query( options );
                results.total = results.then( function( data ){
                    return data.total;
                });
                return results;
                // return QueryResults( results );

            } else {
                // Engine mancante, aspetta che venga impostato
                //console.log( "Missing engine" );
                var self = this;

                var results = new Deferred();
                results.total = results.then( function( data ){
                    return data.total;
                });

                wait( function(){ return self.engine != undefined; }, { delay: 100 } ).then( function(){
                   self.engine.query( options ).then( function( data ){
                       //console.log( "Recovering", options );
                       results.resolve( data ); 
                   });
                });

                return results;
                // return QueryResults( results );
            }
        }

    })
    
})