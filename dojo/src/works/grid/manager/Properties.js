define([
    "require"
    ,"dojo/_base/declare"

], function( _require, declare, SingleSort, ColumnResizer ){

    var ColumnAccessor = declare( [], {
        constructor: function( grid, column ) {
            this.grid = grid;
            this.column = column;
        }
        
        ,_getTitleProperty: function() {
            return this.column.name();
        }

        ,_getNameProperty: function() {
            return this.column.field();
        }
        
        ,_getRowProperty: function() {
            return new ColumnRowAccessor( this.grid, this.column );
        }
    });

    var ColumnCatalog = declare( [],{
        
        constructor: function( grid ) {
            this.grid = grid;
        }
        
        ,get: function( index ) {
            var i = parseInt( index );
            if( ! isNaN( i ) ) {
                var column = this.grid.column( i - 1 );
                if( column ) {
                    return new ColumnAccessor( this.grid, column );
                } else {
                    throw new Error( "Column index out of range: " + i );
                }
            } else {
                throw new Error( "Invalid column index: " + index );
            }
        }
        
        ,_getSizeProperty: function() {
            return this.grid.columns().length;
        }

        ,_getCountProperty: function() {
            return this._getSizeProperty();
        }

    });
    
    var ColumnRowAccessor = declare( [], {

        constructor: function( grid, column ) {
            this.grid = grid;
            this.column = column;
        }

        ,get: function( index ) {
            var i = parseInt( index );
            if( ! isNaN( i ) ) {
                var row = this.grid.row( i - 1 );
                if( row ) {
                    return row.item()[ this.column.field() ];
                } else {
                    throw new Error( "Row index out of range: " + i );
                }
            } else {
                throw new Error( "Invalid row index: " + index );
            }
        }
        ,_getColumnProperty: function() {
            return this;
        }
    });

    var RowAccessor = declare( [], {

        constructor: function( grid, row ) {
            this.grid = grid;
            this.row = row;
        }

        ,get: function( index ) {
            var i = parseInt( index );
            if( ! isNaN( i ) ) {
                var column = this.grid.column( i - 1 );
                if( column ) {
                    return this.row.item()[ column.field() ];
                } else {
                    throw new Error( "Column index out of range: " + i );
                }
            } else {
                throw new Error( "Invalid column index: " + index );
            }
        }
        ,_getColumnProperty: function() {
            return this;
        }
    });
    
    var RowCatalog = declare( [], {
        constructor: function( grid ) {
            this.grid = grid;
        }

        ,get: function( index ) {
            var i = parseInt( index );
            if( ! isNaN( i ) ) {
                var row = this.grid.row( i - 1 );
                if( row ) {
                    return new RowAccessor( this.grid, row );
                } else {
                    throw new Error( "Row index out of range: " + i );
                }
            } else {
                throw new Error( "Invalid row index: " + index );
            }
            
        }
        
        ,_getSizeProperty: function() {
            return this.grid.rowCount();
        }

        ,_getCountProperty: function() {
            return this._getSizeProperty();
        }
    
    });
    
    var CELL_RE = new RegExp( "\([a-zA-Z]+\)\([0-9]+\)" );
    
    var stringToIndex = function( str ) {
        if( str && str.length == 1 ) {
            return str.toUpperCase().charCodeAt( 0 ) - 64;
        } else {
            throw new Error( "Column reference not supported: " + str );
        }
    }
    
    var CellAccessor = declare( [], {

        constructor: function( grid ) {
            this.grid = grid;
        }

        ,get: function( ref ) {
            var match = CELL_RE.exec( "" + ref.replace( new RegExp( " ", "g" ), "" ).trim() );
            if( match ) {
                var columnRef = match[ 1 ];
                var rowRef = match[ 2 ];
                
                var columnIndex = stringToIndex( columnRef ) - 1;
                var rowIndex = parseInt( rowRef ) - 1;
                //TODO: grid.cell
                var column = this.grid.column( columnIndex );
                if( column ) {
                    var row = this.grid.row( rowIndex );
                    if( row ) {
                        return row.item()[ column.field() ];
                    } else {
                        throw new Error( "Row reference out of range: " + rowRef );
                    }
                } else {
                    throw new Error( "Column reference out of range: " + columnRef );
                }
            } else {
                throw new Error( "Invalid cell reference: " + ref );
            }
        }
    });
    
    return declare( [], {
        _getColumnProperty: function() {
            return new ColumnCatalog( this.grid );
        }
        ,_getColumnsProperty: function() {
            return this._getColumnProperty();
        }
        ,_getCellProperty: function() {
            return new CellAccessor( this.grid );
        }
        ,_getRowsProperty: function() {
            return this._getRowProperty();
        }
        ,_getRowProperty: function() {
            return new RowCatalog( this.grid );
        }
        
        ,_setSortProperty: function( value ) {
            var order = parseInt( value );
            if( ! isNaN( order ) && order != 0 ) {
                var descending = false;
                if( order < 0 ) {
                    order = Math.abs( order );
                    descending = true;
                }
                
                var column = this.grid.column( order - 1 );

                if( column ) {
                    this.grid.sort.sort( column.id, descending );
                } else {
                    throw new Error( "Column index out of range: " + order );
                }
            } else {
                throw new Error( "Invalid sort column: " + value );
            }
            
        }
    });
    
});