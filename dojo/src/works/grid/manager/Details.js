define([
    "require"
    ,"works/registry"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/has"
    ,"dojo/query"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/aspect"
    ,"dojo/parser"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"dojo/Deferred"
    ,"works/lang/Set"
    ,"gridx/modules/Dod"

], function( _require, registry, declare, lang, array, has, query, on, topic, aspect, parser, domClass, domStyle, domConstruct, Deferred, Set, Dod ){
    
    return declare( [], {

        postMixInProperties: function() {
            this.inherited( arguments );

            var cfg = this.getConfiguration( "details" );
            
            this.details = {
                expandOnClick: cfg.get( "expandOnClick", true )
                ,animation: cfg.get( "animation", true )
                ,duration: cfg.get( "duration", 200 )
                ,loader: cfg.get( "loader", false )
                ,editor: cfg.get( "editor", false )
            }
            
        }

        ,preCreateGrid: function() {
            this.inherited( arguments );
                
            this.modules.push({
                moduleClass: Dod
                ,showLoader: this.details.loader
				,defaultShow: false
				,useAnimation: this.details.animation
                ,duration: this.details.duration
				,showExpando: false
				,detailProvider: lang.hitch( this, this.formatDetailTemplate )
            });
        }
        
        ,formatDetailTemplate: function( grid, rowId, detailNode, rendered ) {
            this.details.engine.draw({
                grid: grid
                ,id: rowId
                ,node: detailNode
            }).then( function(){
                //console.log( "Drawn", rowId );
                rendered.callback();
            });
            
        }

        ,registerOpenedDetail: function( id ) {
            this.details.lastExpanded = id;
        }
        
        ,destroyDetail: function( rowId ) {
            var d = new Deferred();
            var dodRow = this.grid.dod._row( rowId ); 

            if( dodRow.dodNode ) {
                if( this.details.engine != undefined ) {
                    this.details.engine.clean({
                        id: rowId
                        ,node: dodRow.dodNode
                    }).then( function(){

                        if( dodRow.dodNode.parentNode ) {
                            dodRow.dodNode.parentNode.removeChild( dodRow.dodNode );
                        }

                        if( dodRow.dodLoadingNode && dodRow.dodLoadingNode.parentNode ) {
                            dodRow.dodLoadingNode.parentNode.removeChild( dodRow.dodLoadingNode );
                        }

                        delete dodRow.dodNode;
                        delete dodRow.dodLoadingNode;

                        dodRow.dodLoaded = false;
                        //console.log( "Destroyed", rowId );
                        d.resolve();
                    });

                } else {
                    console.error( "Missing detail engine" );
                    dodRow.dodLoaded = false;
                    d.resolve();
                }
            }
            
            return d;
        }

        ,closeLastExpanded: function() {
            if( this.details.lastExpanded ) {
                var id = this.details.lastExpanded.id;

                this.hideRow( this.details.lastExpanded );

                //console.log( "Destroying", id );
                this.destroyDetail( id );
                                        // .then( lang.hitch( this, function(){
//                     var row = this.grid.row( id );
//                     if( row ) {
//                         var rowNode = row.node();
//                         if( rowNode ) {
//                             dodNode = query( "div.gridxDodNode", rowNode )[0];
//                             if( dodNode ) {
//                                 console.log( "DIRTY", id, rowNode );
//                                 console.log( "DOD", dodNode );
//                             }
//                         }
//                     } else {
//                         console.error( "Cannot get row", id );
//                     }
//                 }))
                delete this.details.lastExpanded;
            }
        }
        
        ,showRow: function( row ) {
            // console.log( "Showing", row.node() );
            this.cleanupNode( row.id );
            this.grid.dod.show( row );
        }

        ,hideRow: function( row ) {
            // console.log( "Hiding", row.node() );
            var dodRow = this.grid.dod._row( row.id );
            try {
                this.grid.dod.hide( row );
            } catch( ex ) {
                dodRow.dodShown = false;
                dodRow.inLoading = false;
                dodRow.dodLoaded = false;
                dodRow.inAnim = false;
                this.cleanupRow( row.id );
            }
            
        }
        
        ,toggleRow: function( row ) {
            this.cleanupNode( row.id );
            var shouldShow = false;
            var row = this.grid.row( row.id );
            if( row ) {
                var rowNode = row.node();
                if( rowNode ) {
                    dodNode = query( "div.gridxDodNode", rowNode )[0];
                    if( ! dodNode || dodNode.firstChild == null ) {
                        shouldShow = true;
                    }
                }
            }

            try {
                this.grid.dod.toggle( row );
            } catch( ex ) {
                console.error( "Error on toggle", mustShow );
                //this.cleanupNode( row.id );
                this.hideRow( row );
                if( shouldShow ) {
                    // this.showRow( row );
                    this.grid.dod.show( row );
                }
            }
            
        }
        
        ,cleanupNode: function( id ) {
            var dodRow = this.grid.dod._row( id );
            if( dodRow.dodNode !== undefined && ! dodRow.dodNode.firstChild ) {
                //console.log( "CLEANING", id );
                domConstruct.destroy( dodRow.dodNode );
                delete dodRow.dodNode;
                if( dodRow.dodLoadingNode ) {
                    domConstruct.destroy( dodRow.dodLoadingNode );
                    delete dodRow.dodLoadingNode;
                }
            }

            if( dodRow.dodNode === undefined ) {
                dodRow.dodLoaded = false;
                dodRow.dodShown = false;
                dodRow.inAnim = false;
                dodRow.inLoading = false;
                this.cleanupRow( id );
            }
        }
        
        ,cleanupRow: function( id ) {
            var row = this.grid.row( id );
            if( row ) {
                var rowNode = row.node();
                if( rowNode ) {
                    domClass.remove( rowNode, "gridxDodShown" );
                }
            }
        }

        ,cleanupBody: function() {
            
            query( ".gridxDodNode", this.grid.bodyNode ).forEach( function( node ){
                if( node.firstChild == null ) {
                    //console.log( "DISCARDING", node );
                    domConstruct.destroy( node );
                }
            });
            
            query( ".gridxRow", this.grid.bodyNode ).removeClass( "gridxDodShown" );
            if( this.grid.rowHeader ) {
                query( '.gridxRowHeaderRow', this.grid.rowHeader.bodyNode ).removeClass( "gridxDodShown" );
            }            
        }
        
        ,postCreateGrid: function() {

            this.inherited( arguments );
            
            this.set( "editor", this.details.editor );
            
            // Resize
            // this.wire( this.grid, "resize", function( evt ){
            //     console.log( "dod resize", this.id );
            //     //this.details.engine.resize();
            // });
                

            
            this.own( 
                on( this, "detail-hover", lang.hitch( this, function(){
                    //console.log( "Detail hover" );
                    query( ".gridxRowOver", this.grid.bodyNode ).removeClass( 'gridxRowOver' );
                }))
                ,on( this, "grid-resize", lang.hitch( this, function(){
                    //console.log( "dod grid resize", this.id );
                    this.details.engine.resize();
                }))
                ,on( this, "column-resize", lang.hitch( this, function(){
                    setTimeout( lang.hitch( this.details.engine, this.details.engine.resize ), 0 );
                }))
                ,on( this, "scroll-horizontal", lang.hitch( this, function(){
                    setTimeout( lang.hitch( this.details.engine, this.details.engine.resize ), 0 );
                }))
                ,on( this, "grid-inspect", lang.hitch( this, function(){
                    this.details.inspecting = true;
                }))
                ,topic.subscribe( "form/task/start", lang.hitch( this, function(){
                    this.grid.dod.useAnimation = false;
                }))
                ,topic.subscribe( "form/task/end", lang.hitch( this, function(){
                    this.grid.dod.useAnimation = this.details.animation;
                }))
            );

            this.wire( this.grid.dod, "onShow", function( row ){
                //console.log( "Show", row.id, "last", this.details.lastExpanded );
                this.details.engine.resize();
                if( this.grid.rowHeader ) {
                    var headerNode = this.rowHeaderNode( row );
                    if( headerNode ) {
                        domClass.add( headerNode, "gridxDodShown" );
                    }
                    
                    setTimeout( lang.hitch( this.grid.rowHeader, this.grid.rowHeader._onResize), 100 );
                }
            });

            this.wire( this.grid.dod, "onHide", function( row ){
                if( this.grid.rowHeader ) {
                    var headerNode = this.rowHeaderNode( row );
                    if( headerNode ) {
                        domClass.remove( headerNode, "gridxDodShown" );
                    }

                    setTimeout( lang.hitch( this.grid.rowHeader, this.grid.rowHeader._onResize), 100 );
                }
            });

            // ExpandOnClick
            if( this.details.expandOnClick ) {
                this.own(
                    on( this, "row-select", lang.hitch( this, function( evt ){
                        //console.log( "dod-row-select", evt.row );
                        
                        if( evt.row ) {
                            if( ! this.details.engine.blank ) {
                                this.showRow( evt.row );
                            }
                            this.details.lastExpanded = evt.row;
                            var focusRequested = false;
                            if( this.get( "editor" ) == true && this.details.lastClick !== undefined ) {

                                var click = this.details.lastClick;
                                if( click.y !== undefined && click.y !== undefined ) {
                                    var node = document.elementFromPoint( click.x, click.y );
                                    //console.log( "Found node:", node );
                                    if( node ) {
                                        var widget = registry.getEnclosingWidget( node );
                                        //console.log( "Found widget:", widget );
                                        if( widget && lang.isFunction( widget.focus ) ) {
                                            focusRequested = true;
                                            setTimeout( lang.hitch( widget, widget.focus ), 300 );
                                        } else if( lang.isFunction( node.focus ) ) {
                                            focusRequested = true;
                                            setTimeout( lang.hitch( node, node.focus ), 300 );
                                        }    
                                    }
                                }
                            }
                            
                            if( ! focusRequested ) {
                                this.grid.domNode.focus();
                            }

                            delete this.details.lastClick;
                        }
                    }))
                    
                    // ,on( this, "row-reselect", lang.hitch( this, function( evt ){
                   //      console.log( "dod-row-reselect", evt );
                   //      this.grid.dod.toggle( evt.row );
                   //  }))

                    ,on( this, "row-deselect", lang.hitch( this, function( evt ){
                        // console.log( "dod-row-deselect", evt );
                        // console.log( "last", this.details.lastExpanded );
                        // console.log( "row", evt.row );
                        if( evt.row && this.details.lastExpanded && evt.row.id == this.details.lastExpanded.id ) {
                            //console.log( "hide", this.details.lastExpanded );
                            this.closeLastExpanded();
                        }
                    }))

                    ,on( this, "row-click", lang.hitch( this, function( evt ){
                        //console.log( "dod-row-click", evt.row.index() );
                        
                        // Fix per Chrome che fa perdere il focus alla griglia anche se lo si guarda male
                        // NOTA: il timeout di blocco deve essere superiore al timeout di focus
                        this.details.blurLock = true;
                        setTimeout( lang.hitch( this, function(){
                            delete this.details.blurLock;
                        }), 500 );
                        
                        if( this.get( "editor" ) == true ) {
                            this.details.lastClick = { x: evt.x, y: evt.y }
                        }
                        
                        // Se non c'e' una precedente espansione, il click non e' interessante
                        if( evt.row && this.details.lastExpanded ) {
                            // Se il click e' su una riga diversa da quella corrente, elimina il dod corrente
                            if( evt.row.id != this.details.lastExpanded.id ) {
                                // console.log( "hide", this.details.lastExpanded.id );
                                this.closeLastExpanded();

                            // Se il click e' sulla riga corrente, apre o chiude
                            } else {
                                this.toggleRow( evt.row );
                            }   
                        }
                    }))
                    
                    ,on( this, "grid-blur", lang.hitch( this, function( evt ){
                        if( this.details.blurLock == true ) {
                            this.grid.domNode.focus();
                        } else
                        if( this.get( "editor" ) == true && this.details.lastExpanded ) {
                            if( this.autoDeselect && this.details.inspecting != true ) {
                                this.clearSelection();
                            }
                        }
                    }))
                    
                    ,aspect.after( this.grid.dod, "onShow", lang.hitch( this, function( row ){
                        setTimeout( lang.hitch( this, function(){
                            this.grid.vScroller.scrollToRow( row.index() ); 
                        }), 200 );
                    }), true )
                )
                
            }
            
            // Items reload
            this.own(
                on( this, "reloading", lang.hitch( this, function( evt ){
                    if( evt.detail.widget === this ) {
                        //console.log( "dod reloading", evt );
                         if( this.details.lastExpanded ) {
                             this.grid.dod.hide( this.details.lastExpanded );
                             delete this.details.lastExpanded;
                         }
                         
                    }
                }))
                
                ,on( this, "reloaded", lang.hitch( this, function( evt ){
                    //console.log( "reloaded", evt.detail.widget );
                    if( evt.detail.widget === this ) {
                        //console.log( "dod reloaded", evt );
                        this.grid.dod._rowMap = {};
                    }
                    this.cleanupBody();
                }))


                ,on( this, "column-sorting", lang.hitch( this, function( evt ){
                    this.grid.dod._rowMap = {};
                }))
            );
        }
        
        ,_setEditorAttr: function( value ) {
            domClass[ value ? "add" : "remove" ]( this.grid.domNode, "gridxEditor" );
            this.grid.dod.useAnimation = value ? false : this.details.animation;
            //this.grid.dod.duration = value ? 10 : this.details.duration;
            this._set( "editor", value );
        }
    });
    
});
