define([
    "require"
    ,"works/lang/declare"
    ,"works/registry"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/query"
    ,"works/layout/ContentPane"
    ,"gridx/Grid"
], function( req, declare, registry, array, lang, domStyle, domGeometry, query, ContentPane, Grid ){
    
    return declare( declare.className( req ), [ ContentPane ], {
        baseClass: "gridxRowLayout"
        ,neverResized: true
        
        //TODO: calcolare in base allo stile dell'intestazione, al primo utilizzo
        ,widthFix: 3
        
        ,startup: function() {
            this.inherited( arguments );
            array.forEach( this.getChildren(), function( child ){
                if( lang.isFunction( child.cellResizing ) ) {
                    child.set( "autoCellWidth", true );
                }
            });
        }

        ,resize: function() {
            this.inherited( arguments );
            this.domNode.style.height = null;
            if( this.neverResized ) {
                //this.neverResized = false;
                this.resizeChildren();
            } else {
                if( this.resizeChildrenTimeout ) {
                    clearTimeout( this.resizeChildrenTimeout );
                }
                this.resizeChildrenTimeout = setTimeout( lang.hitch( this, this.resizeChildren ), 100 );
            }
        }

        ,resizeChildren: function() {
            var grid = registry.findParent( this, Grid );
            if( grid ) {
                var columns = grid.columns();
                var cells = query( "table > tbody > tr > td.gridxRowLayoutCell", this.containerNode );
                var resizables = [];
                array.forEach( cells, function( cell ){
                    var child = registry.findWidgets( cell )[ 0 ];
                    if( child && lang.isFunction( child.cellResizing ) ) {
                        var node = child.cellResizing( this );
                        if( node !== undefined && node != null ) {
                            resizables.push({
                                widget: child
                                ,cell: cell
                            });
                        }
                    }
                }, this );

                for( var i = 0; i < cells.length; i++ ) {
                    if( i < columns.length ) {
                        var width = ( parseFloat( columns[ i ].getWidth().replace( "px", "" ) ) + this.widthFix ) + "px";
                        domStyle.set( cells[ i ], {
                            width: width
                            ,minWidth: width
                            ,maxWidth: width
                        });
                    } else {
                        domStyle.set( cells[ i ], "display", "none" );
                    }
                }


                array.forEach( resizables, function( resizable ){
                    resizable.size = domGeometry.getContentBox( resizable.cell );
                }, this );

                array.forEach( resizables, function( resizable ){
                    if( lang.isFunction( resizable.widget.cellResize ) ) {
                        resizable.widget.cellResize( resizable.size );
                    }
                }, this );
            }   
        }
        
        
    });
    
});