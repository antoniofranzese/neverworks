define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-class"
    ,"./AbstractManager"
    ,"./manager/Headers"
    ,"./manager/Rows"
    
    ,"./decorator/StandardDecorators"
    
], function( _require, declare, domClass, AbstractManager, Headers, Rows ){
    
    return declare( [ AbstractManager, Headers, Rows ], {
        setGridState: function( state ) {
            this.inherited( arguments );
            if( "visible" in state ) {
                this.set( "visible", state.visible );
            }
        }

        ,_setVisibleAttr: function( value ) {
            domClass[ value != true ? "add" : "remove" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }
        
    });
    
});