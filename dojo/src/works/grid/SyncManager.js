define([
    "require"
    ,"dojo/_base/declare"
    ,"./BaseManager"
    ,"./manager/SyncStorage"      
], function( _require, declare, BaseManager, SyncStorage ){
    
    return declare( [ BaseManager, SyncStorage ], {
        
    });
    
});