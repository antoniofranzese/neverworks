define([
    "require"
    ,"dojo/_base/declare"
], function( _require, declare ){
    
    return declare( [], {
        decorators: {}
        
        ,register: function( name, decorator ) {
            this.decorators[ name ] = decorator;
        }
        
        ,decorator: function( name ) {
            return this.decorators[ name ]; // dc
        }
        
        ,contains: function( name ) {
            return name in this.decorators;
        }
    })();
    
})