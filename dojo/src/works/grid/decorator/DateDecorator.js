define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/date/stamp"
    ,"dojo/date/locale"
    ,"./BaseDecorator" 
], function( _require, declare, stamp, locale, BaseDecorator ){
    
    return declare( [ BaseDecorator ], {
        format: "dd/MM/yyyy"
        ,selector: "date"
        
        ,decorate: function( data ) {
            var date = typeof data == "string" ? stamp.fromISOString( data ) : data;
            
            return this.wrap( date != null ? locale.format( date, { selector: this.selector, datePattern: this.format }) : ( "#ERROR:" + date ) );
        }
    })
    
})