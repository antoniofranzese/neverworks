define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/dom-attr"
    ,"works/canvas/CanvasController"
    ,"./BaseDecorator" 
], function( _require, declare, lang, domAttr, CanvasController, BaseDecorator ){
    
    return declare( [ BaseDecorator ], {
        
        startup: function() {
            this.inherited( arguments );
            var field = this.manager.grid.column( this.columnIndex ).field();
            this.manager.on( "after-row", lang.hitch( this, function( evt ) { 
                var row = evt.row;
                var canvas = row.cell( this.columnIndex ).node().firstChild;
                if( canvas && domAttr.get( canvas, "data-works-rendered" ) == "false" ) {
                    var scene = row.item()[ field ];
                    new CanvasController( canvas ).plot( scene );
                    domAttr.set( canvas, "data-works-rendered", "true" );
                }
            }));
        }

        ,decorate: function( data ) {
            return '<canvas data-works-rendered="false" height="' + this.height + '" width="' + this.width + '"></canvas>';
        }

    });
    
});