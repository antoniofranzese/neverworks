define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/Stateful"
], function( _require, declare, Stateful ){
    
    return declare( [ Stateful ], {
        
        manager: null
        ,column: null
        
        ,constructor: function( params ) {
            declare.safeMixin( this, params );
        }
        
        ,decorate: function( data ){
            return data;
        }
        
        ,startup: function() {
            
        }
        
        ,_columnGetter: function() {
            if( this.columnIndex != undefined && this.manager != undefined ) {
                return this.manager.grid.column( this.columnIndex );
            }
        }
        
        ,_managerSetter: function( value ) {
            this.manager = value;
        }
        
    })
    
})