define([
    "require"
    ,"dojo/_base/declare"
    ,"./DecoratorRegistry"
    ,"./BaseDecorator"
    ,"./DateDecorator"
    ,"./CanvasDecorator"
], function( _require, declare, registry, BaseDecorator, DateDecorator, CanvasDecorator ){
    
    registry.register( "base", BaseDecorator );
    registry.register( "date", DateDecorator );
    registry.register( "canvas", CanvasDecorator );
    
    return registry;
})