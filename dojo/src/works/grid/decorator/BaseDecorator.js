define([
    "require"
    ,"dojo/_base/declare"
    ,"works/lang/StringBuilder"
    ,"works/lang/Model"
    ,"works/local-style"

    ,"./Decorator" 
], function( _require, declare, StringBuilder, Model, localStyle, Decorator ){
    
    var CELL = ".c.";
    var ROW = ".r";
    var COLOR = "cl";
    var BACKGROUND = "bk";
    var CLASS = "cs";
    var FONT = "ft";

    var Style = declare( [ Model ], {
        
        merge: function( other ) {
            var merged = new Style();
            merged[ FONT ] = other[ FONT ] || this[ FONT ];
            merged[ COLOR ] = other[ COLOR ] || this[ COLOR ];
            merged[ BACKGROUND ] = other[ BACKGROUND ] || this[ BACKGROUND ];
            return merged;
        }
        
        ,toCSS: function() {
            return ( this[ FONT ] ? ( "font:" + this[ FONT ] + ";" ) : "" )
                + ( this[ COLOR ] ? ( "color:" + this[ COLOR ] + ";" ) : "" )
                + ( this[ BACKGROUND ] ? ( "background-color:" + this[ BACKGROUND ] + ";" ) : "" );
        }
        
        ,isEmpty: function() {
            if( this[ COLOR ] ) return false;
            if( this[ BACKGROUND ] ) return false;
            if( this[ FONT ] ) return false;
            return true;
        }
    
    });
    
    var RowClassMerger = function( attr, cell ) {
        var rowStyle = cell.row.item()[ ROW ];
        if( rowStyle !== undefined && rowStyle != null ) {
            return attr + " " + ( rowStyle[ CLASS ] || "" ) ;
        } 
    }

    var RowStyleMerger = function( style, cell ) {
        var rowStyle = cell.row.item()[ ROW ];
        if( rowStyle !== undefined && rowStyle != null ) {
            return style.merge( rowStyle );
        } else {
            return style;
        }
    }

    var CellClassMerger = function( attr, cell ) {
        var cellStyle = cell.row.item()[ CELL + cell.column.field() ];
        if( cellStyle !== undefined && cellStyle != null ) {
            return attr + " " + ( cellStyle[ CLASS ] || "" ) ;
        } 
    }

    var CellStyleMerger = function( style, cell ) {
        var cellStyle = cell.row.item()[ CELL + cell.column.field() ];
        if( cellStyle !== undefined && cellStyle != null ) {
            return style.merge( cellStyle );
        } else {
            return style;
        }
    }

    return declare( [ Decorator ], {
        halign: "left"
        ,valign: "middle"
        ,height: null
        ,lwrap: null
        ,font: null
        ,color: null
        ,background: null
        ,cell: false
        
        ,decorate: function( data ) {
            return this.wrap( data );
        }

        ,wrap: function( data ) {
            var css = {};
            var wrap = false;
            if( this.height != null ) {
                css[ "display" ] = "table-cell";
                css[ "vertical-align" ] = this.valign;
                css[ "height" ] = this.height;
                wrap = true;
            }
            
            if( wrap ) {
                console.log( "WRAPPING", data );
                var style = new StringBuilder( 128 );
                for( prop in css ) {
                    style.append( prop ).append( ":" ).append( css[ prop ] ).append( ";" );
                }

                return '<div style="' + style.toString() + '">' + data + '</div>';
            } else {
                return data;
            }
        }
        
        ,startup: function() {
            this.inherited( arguments );
            
            var columnClass = "";
            var colDef = this.get( "column" ).def();
            
            if( colDef.titleStyle ) {
                var colId = this.get( "column" ).id;
                this.titleLocalStyle = "column-title-" + colId;
                localStyle( this.get( "manager" ), this.titleLocalStyle )
                    .set( '## > .gridx > .gridxHeader .gridxCell[colid="' + colId + '"]', colDef.titleStyle )
                    .write();
            }

            
            if( this.halign != "left" ) {
                columnClass += " " + this.halign;
            }
            
            if( this.valign != "middle" ) {
                columnClass += " " + this.valign;
            }
            
            if( this.lwrap == true ) {
                columnClass += " wrap";
            }
            
            var columnFont = this.font != null ? ( "font:" + this.font + ";" ) : "";
            var columnColor = this.color != null ? ( "color:" + this.color + ";" ) : "";
            var columnBackground = this.background != null ? ( "background-color:" + this.background + ";" ) : "";
            
            var columnStyle = new Style();
            columnStyle[ FONT ] = this.font;
            columnStyle[ COLOR ] = this.color;
            columnStyle[ BACKGROUND ] = this.background;
            
            var styleMergeChain = [];
            var classMergeChain = [];
            var complexMerge = false;

            for( var i = 0; i < this.manager.decorationPriority.length; i++ ){
                var step = this.manager.decorationPriority[ i ];
                if( step == "column" && ! columnStyle.isEmpty() ) {
                    styleMergeChain.unshift( function( style ) {
                        return style.merge( columnStyle );
                    });

                } else if( step == "row" && this.manager.rowDecoration ) {
                    styleMergeChain.unshift( RowStyleMerger );
                    classMergeChain.unshift( RowClassMerger );
                    complexMerge = true;

                } else if( step == "cell" && this.cell ) {
                    styleMergeChain.unshift( CellStyleMerger );
                    classMergeChain.unshift( CellClassMerger );
                    complexMerge = true;

                }
            }

            if( complexMerge ) {
                colDef[ "class" ] = function( cell ) {
                    var cls = columnClass;
                    for( var c = 0; c < classMergeChain.length; c++ ) {
                        cls = classMergeChain[ c ]( cls, cell );
                    }
                    return cls;    
                }

                colDef.style = function( cell ) {
                    var style = new Style();
                    for( var s = 0; s < styleMergeChain.length; s++ ) {
                        style = styleMergeChain[ s ]( style, cell );
                    }
                    return style.toCSS();
                }
                
            } else {
                if( columnClass != "" ) {
                    colDef[ "class" ] = columnClass;
                }
                
                if( ! columnStyle.isEmpty() ) {
                    colDef.style = columnStyle.toCSS();
                }
                
            }
            
        }

    });
    
});