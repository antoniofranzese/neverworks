var profile = (function(){
	var testResourceRe = /^dojo\/tests\//,

		copyOnly = function(filename, mid){
			var list = {
				"works/browser":1,
				"works/web/resources/default-gateway":1
			};
			return (mid in list) ||
            (/.*\/resources\/.*/.test( filename ) );
		};

	return {
		resourceTags:{
			copyOnly: function(filename, mid){
				return copyOnly(filename, mid);
			},

			amd: function(filename, mid){
				return !copyOnly(filename, mid) && /\.js$/.test(filename);
			}
		}
	};
})();