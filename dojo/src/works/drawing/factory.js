define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
], function( _require, declare, lang ){
    
    var JOINT_NS = lang.getObject( "joint.shapes" );
    if( JOINT_NS === undefined ) {
        console.error( "Joint is not configured!" );
    }
    
    if( JOINT_NS.__works__ === undefined ) {
        JOINT_NS.__works__ = {}
    }
    var WORKS_NS = lang.getObject( "joint.shapes.__works__" );

    var inherited = function( name, args ) {
        
        if( typeof name != "string" ) {
            args = name;
            name = args.callee.__name__;
            if( ! name ) {
                throw new Error( "Cannot infer callee name on inherited" );
            }
        }

        var proto = this.__proto__;
        var mine = proto[ name ];

        do {
            proto = proto.__proto__;
        } while( proto !== undefined && proto[ name ] === mine );

        var func = proto !== undefined ? proto[ name ] : function(){};
        return func.apply( this, args );

    }
    
    var instrument = function( cls ) {
        var proto = cls.prototype;
        for( var key in proto ) {
            if( lang.isFunction( proto[ key ] ) ) {
                proto[ key ].__name__ = key;
            }
        }
        
        proto.inherited = inherited;
        return cls;
    }
    
    var retrieveClass = function( name ) {
        var worksName = name.replace( new RegExp( "\\.", "g" ), "$" );
        var cls = lang.getObject( worksName, /* create */ false, WORKS_NS );
        if( cls === undefined ) {
            cls = lang.getObject( name, /* create */ false, JOINT_NS );

            if( cls === undefined ) {
                cls = lang.getObject( name, /* create */ false );
            }
        }
        return cls;
    }

    var storeClass = function( name, cls ) {
        var worksName = name.replace( new RegExp( "\\.", "g" ), "$" );
        lang.setObject( worksName, cls, WORKS_NS );
    }
    
    return declare( [], {
        
        declare: function( name, base, params ) {
            var baseClass = ( typeof base == "string" ) ? retrieveClass( base ) : base;
            
            if( baseClass && lang.isFunction( baseClass.extend ) ) {
                params = params || {};
                if( params.defaults == undefined ) {
                    params.defaults = joint.util.deepSupplement(
                        {
                            type: "__works__." + name.replace( new RegExp( "\\.", "g" ), "$" )

                        }, 
                        baseClass.prototype.defaults 
                    );
                }
                var declared = instrument( baseClass.extend( params ) );
                storeClass( name, declared );
                
                return declared;
            } else {
                throw new Error( "Unknown or invalid stencil base class: " + base );
            }
        }
        
        ,require: function( name, callback ) {
            var cls = retrieveClass( name );

            if( cls ) {
                callback( cls );
            } else {
                require( [ name.replace( new RegExp( "\\.", "g" ), "/" ) ], function(){
                    var _cls = retrieveClass( name );
                    if( _cls ) {
                        callback( _cls );
                    } else {
                        throw new Error( "Unknown stencil class: " + name );
                    }
                });
            }
        }

    })();
    
});