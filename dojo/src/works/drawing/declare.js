define([
    "./factory"
], function( factory ){
    
    return factory.declare;
    
})