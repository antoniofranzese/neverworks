define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"works/config"
    ,"dojo/dom-construct"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/on"
    ,"dojo/Evented"
    ,"dojo/Deferred"
    ,"dojo/promise/all"
    ,"dijit/_WidgetBase"
    ,"works/form/_WidgetMixin"
    ,"works/form/_FormWidgetMixin"
    ,"works/dnd/_DropMixin"
    ,"works/_BlinkMixin"
    ,"./factory"
    
    ,"works/style!./resources/Paper.css"
    
], function( _require, declare, lang, array, config, domConstruct, domGeometry, domClass, domStyle, on, Evented, Deferred, all, _WidgetBase, _WidgetMixin, _FormWidgetMixin, _DropMixin, _BlinkMixin, factory ){
    
    var DRAG_DELAY = config( "dragDelay", 300 );
    var DOUBLE_CLICK_DELAY = config( "doubleClickDelay", 150 );
    var ID = ".id";
    
    var Paper = declare( [ _WidgetBase, Evented, _WidgetMixin, _FormWidgetMixin, _DropMixin, _BlinkMixin ], {
        blinkingClass: "worksPaperBlink"

        ,readonly: false
        ,minWidth: 0
        ,minHeight: 0
        
        ,buildRendering: function() {
            this.inherited( arguments );
            domClass.add( this.domNode, "worksPaper" );

            this.scrollerNode = domConstruct.create( "div", { 
                "class": "worksPaperScroller"
            }, this.domNode );
            
            this.containerNode = domConstruct.create( "div", { 
                "class": "worksPaperContainer"
            }, this.scrollerNode );
    
            this.domNode.oncontextmenu = function(){ return false; };
        }
        
        ,startup: function() {
            this.inherited( arguments );
            this.document = window.document;
            this.graph = new joint.dia.Graph();

            this.paper = new joint.dia.Paper({
                el: $( this.containerNode )
                ,width: 100
                ,height: 100
                ,model: this.graph
                ,gridSize: 1
                ,async: true
            
            });
            
            this.own(
                this.paper.on( "blank:pointerdown",     lang.hitch( this, this.onBlankPointerDown ) )
                ,this.paper.on( "blank:pointerup",       lang.hitch( this, this.onBlankPointerUp ) )
                ,this.paper.on( "blank:pointerclick",    lang.hitch( this, this.onBlankPointerClick ) )
                ,this.paper.on( "blank:pointerdblclick", lang.hitch( this, this.onBlankPointerDoubleClick ) )
                ,this.paper.on( "cell:pointerdown",      lang.hitch( this, this.onCellPointerDown ) )
                ,this.paper.on( "cell:pointermove",      lang.hitch( this, this.onCellPointerMove ) )
                ,this.paper.on( "cell:pointerup",        lang.hitch( this, this.onCellPointerUp ) )
                ,this.paper.on( "element:remove",        lang.hitch( this, this.onElementRemove ) )
                ,this.paper.on( "view:rendering",        lang.hitch( this, this.onViewRendering ) )
            );
            
            if( this.startupElements ) {
                array.forEach( this.startupElements, this.addElement, this );
            }
            
        }
        
        ,joinForm: function() {
            this.inherited( arguments );
            this.announceEvent( "remove" );
            this.announceEvent( "cell.click" );
            this.announceEvent( "cell.inspect" );
            this.announceEvent( "cell.launch" );
            this.announceEvent( "paper.click" );
            this.announceEvent( "paper.inspect" );
        }

        ,leaveForm: function() {
            this.retireEvent( "remove" );
            this.retireEvent( "paper.inspect" );
            this.retireEvent( "paper.click" );
            this.retireEvent( "cell.launch" );
            this.retireEvent( "cell.inspect" );
            this.retireEvent( "cell.click" );
            this.inherited( arguments );
        }
        
        ,onBlankPointerDown: function( evt, x, y ) {
            if( evt.target === this.paper.svg ) {
                //console.log( "paper down", evt.button, evt.target );
                if( evt.button == 0 ) {
                    this.blankMoveHandler = on( this.document, "mousemove", lang.hitch( this, function( evt ){ 
                        this.onBlankPointerMove( evt, Math.floor( evt.clientX ), Math.floor( evt.clientY ) );
                    }));
                    this.blankUpHandler = on( this.document, "mouseup", lang.hitch( this, function( evt ){
                        this.onBlankPointerUp( evt, Math.floor( evt.clientX ), Math.floor( evt.clientY ) );
                    }));
                    this.emit( "paper-down", { bubbles: false, x: x, y: y });

                } else if( evt.button == 2 ) {
                    this.paperRightClicking = true;
                    setTimeout( lang.hitch( this, function(){
                        delete this.paperRightClicking;
                    }), DRAG_DELAY );
                }
            }
        }

        ,onBlankPointerMove: function( evt, x, y ) {
            //this.emit( "paper-move", { bubbles: false, x: x, y: y });
        }
        
        ,onBlankPointerUp: function( evt, x, y ) {
            if( evt.target === this.paper.svg ) {
                //console.log( "paper up", evt.button, evt.target );
                if( evt.button == 0 ) {
                
                    if( this.blankMoveHandler !== undefined ) {
                        this.blankMoveHandler.remove();
                        delete this.blankMoveHandler;
                    }
                    
                    if( this.blankUpHandler !== undefined ) {
                        this.blankUpHandler.remove();
                        delete this.blankUpHandler;
                    }

                    //this.emit( "paper-up", { bubbles: false, x: x, y: y });

                } else if( evt.button == 2 && this.paperRightClicking ) {
                    var origin = this.parseEventOrigin( evt );
                    this.emitAlways( "paper.inspect", { state: { x: origin.clickX, y: origin.clickY } } );
                }
            }
        }
        
        ,onBlankPointerClick: function( evt, x, y ) {
            if( evt.target === this.paper.svg ) {
                //console.log( "paper click", evt.button );
                var origin = this.parseEventOrigin( evt );
                this.emitAlways( "paper.click", { state: { x: origin.clickX, y: origin.clickY } } );
            }
        }
        
        ,onBlankPointerDoubleClick: function( evt, x, y ) {
            //this.emit( "paper-dblclick", { bubbles: false, x: x, y: y });
        }

        ,onCellPointerDown: function( cellView, evt, x, y ) {
            if( evt.button == 0 ) {
                this.cellLeftClicking = true;
                this.cellLeftClicks = this.cellLeftClicks !== undefined && this.cellLeftClicks > 0 ? this.cellLeftClicks + 1 : 1;
                
                //console.log( "click", this.cellLeftClicks );
                if( this.cellLeftClicks == 1 ) {
                    this.cellLeftDragTimeout = setTimeout( lang.hitch( this, function(){
                        //console.log( "drag timeout" );
                        delete this.cellLeftClicking;
                        delete this.cellLeftClicks;
                    }), DRAG_DELAY );
                }

            } else if( evt.button == 2 ) {
                this.cellRightClicking = true;
                setTimeout( lang.hitch( this, function(){
                    delete this.cellRightClicking;
                }), DRAG_DELAY );
            }
        }
        
        ,onCellPointerUp: function( cellView, evt, x, y ) {
            //console.log( "up", this.cellLeftClicking );
            if( evt.button == 0 && this.cellLeftClicking ) {
                if( this.cellLeftDragTimeout ) {
                    clearTimeout( this.cellLeftDragTimeout );
                    delete this.cellLeftDragTimeout;
                }
                if( this.cellLeftClicks == 1 ) {
                    setTimeout( lang.hitch( this, function(){
                        //console.log( "click timeout" );
                        if( this.cellLeftClicking ) {
                            //console.log( "click", this.cellLeftClicks );
                            this[ this.cellLeftClicks == 2 ? "onCellPointerDoubleClick" : "onCellPointerClick" ]( cellView, evt, x, y );
                            delete this.cellLeftClicking;
                            delete this.cellLeftClicks;
                        }
                    }), DOUBLE_CLICK_DELAY );
                }
            } else if( evt.button == 2 && this.cellRightClicking ) {
                this.onCellPointerRightClick( cellView, evt, x, y );
            }
        }
        
        ,onCellPointerMove: function( cellView, evt, x, y ) {
            delete this.cellRightClicking;
        }
        
        ,onCellPointerClick: function( cellView, evt, x, y ) {
            if( cellView.model !== undefined && cellView.model.cid !== undefined ) {
                var origin = this.parseEventOrigin( evt );
                this.emitAlways( "cell.click", { state: { x: origin.clickX, y: origin.clickY, id: this.elementByCid( cellView.model.cid )[ ID ] } } );
            }
        }
        
        ,onCellPointerDoubleClick: function( cellView,evt, x, y ) {
            if( cellView.model !== undefined && cellView.model.cid !== undefined ) {
                var origin = this.parseEventOrigin( evt );
                this.emitAlways( "cell.launch", { state: { x: origin.clickX, y: origin.clickY, id: this.elementByCid( cellView.model.cid )[ ID ] } } );
            }
        }

        ,onCellPointerRightClick: function( cellView,evt, x, y ) {
            if( cellView.model !== undefined && cellView.model.cid !== undefined ) {
                var origin = this.parseEventOrigin( evt );
                this.emitAlways( "cell.inspect", { state: { x: origin.clickX, y: origin.clickY, id: this.elementByCid( cellView.model.cid )[ ID ] } } );
            }
        }

        ,onRenderDone: function() {
            // this.emit( "render-done", { bubbles: false });
        }

        ,onDropOffer: function( evt ) {
            if( this.readonly ) {
                evt.reject();
            } else {
                this.inherited( arguments );
            }
        }

        ,resize: function( size ) {
            //this.inherited( arguments );
            if( size === undefined && this.lastSize !== undefined ) {
                size = this.lastSize;
            }
            
            if( size !== undefined ) {
                this.domNode.style.width = size.w + "px";
                this.domNode.style.height = size.h + "px";
                // this.shieldNode.style.width = size.w + "px";
                // this.shieldNode.style.height = size.h + "px";
                var width = size.w < this.minWidth ? this.minWidth : size.w;
                var height = size.h < this.minHeight ? this.minHeight : size.h;
                
                this.containerNode.style.width = width + "px";
                this.containerNode.style.height = height + "px";
                
                if( this.resizeTimeout !== undefined ) {
                    clearTimeout( this.resizeTimeout );
                }
                this.resizeTimeout = setTimeout( lang.hitch( this, this.resizePaper ), 1500 );
                this.lastSize = size;
            }
        }
        
        ,resizePaper: function() {
            delete this.resizeTimeout;
            var size = domGeometry.getContentBox( this.containerNode );

            array.forEach( this.get( "elements" ), function( element ){
                var s = element.get( "size" );
                var p = element.get( "position" );
                size.w = Math.max( size.w, p.x + s.width );
                size.h = Math.max( size.h, p.y + s.height );
                var w = element.get( "size" ).w
            }, this );

            this.paper.setDimensions( size.w, size.h );
        }
        
        ,addElement: function( info ) {

            if( info[ "class" ] ) {
                factory.require( info[ "class" ], lang.hitch( this, function( cls ){
                    var element = new cls( info.model );
                    element[ ID ] = info.id;
                    this.graph.addCell( element );
                }));
            } else {
                this.graph.addCell( info );
            };
        }
        
        ,removeElement: function( id ) {
            var element = this.elementById( id );
            
            if( element !== undefined ) {
                element.remove();
            } else {
                throw new Error( "Cannot remove element for id: " + id );
            }
        }
        
        ,clear: function() {
            var elements = this.get( "elements" );
            for( var i = elements.length - 1; i >= 0; i-- ) {
                elements[ i ].remove();
            }
        }
        
        ,onElementRemove: function( element ) {
            var cid = element.cid;
            if( cid !== undefined ) {
                this.emitAlways( "remove", { state: { id: this.elementByCid( cid )[ ID ] } } );
            }
        }
        
        ,onViewRendering: function( view ) {
            if( lang.isFunction( view.setReadonly ) ) {
                view.setReadonly( this.get( "readonly" ) );
            }
        }
        
        ,elementByCid: function( cid ) {
            var elements = this.get( "elements" );
            var result = undefined;
            for( var index = 0; index < elements.length; index++ ) {
                if( elements[ index ].cid == cid ) {
                    result = elements[ index ];
                    break;
                }
            }
            if( result !== undefined ) {
                return result;
            } else {
                throw new Error( "Cannot locate Cid: " + cid );
            }
            
        }
 
        ,elementById: function( id ) {
            var elements = this.get( "elements" );
            var result = undefined;
            for( var index = 0; index < elements.length; index++ ) {
                if( elements[ index ][ ID ] == id ) {
                    result = elements[ index ];
                    break;
                }
            }
            if( result !== undefined ) {
                return result;
            } else {
                throw new Error( "Cannot locate id: " + id );
            }
        }

        ,getWidgetState: function() {
            var elementStates = [];
            array.forEach( this.get( "elements" ), function( element ){
                var state = lang.isFunction( element.getState ) ? element.getState() : undefined;
                if( state !== undefined ) {
                    elementStates.push({ id: element[ ID ], state: state });
                }
            }, this ); 
            
            return {
                elements: elementStates
            };
        }
        
        ,setWidgetState: function( state ) {
            if( state.operations !== undefined ) {
                this.performOperations( state.operations );
                delete state.operations;
            }
            this.inherited( arguments );
        }
        
        ,requireAll: function( classes ) {
            var deferreds = [];
            array.forEach( classes, function( cls ){
                var d = new Deferred();
                deferreds.push( d );
                factory.require( cls, d.resolve );
            }, this );
            
            return all( deferreds );
        }

        ,_getViewsAttr: function() {
            return array.map( this.get( "elements" ), function( element ){
                return element.findView( this.paper );
            }, this );
        }

        ,_getElementsAttr: function() {
            return this.graph.getElements();
        }
        
        ,_setElementsAttr: function( elements ) {
            this.clear();

            this.requireAll( array.map( elements, function( e ){ return e["class"]; } ) ).then( lang.hitch( this, function(){
                array.forEach( elements, this.addElement, this );
                this.resizePaper();
            }))
        }
        
        ,_setChangedElementsAttr: function( changes ) {
            var elements = this.get( "elements" );
            array.forEach( changes, function( change ){
                if( change[ ID ] !== undefined ) {
                    var element = this.elementById( change[ ID ] );
                    if( element !== undefined ) {
                        delete change[ ID ];
                        delete change[ "id" ];
                        for( var name in change ) {
                            element.set( name, change[ name ] );
                        }
                    } else {
                        throw new Error( "Missing element for id: " + change[ ID ] );
                    }
                }
            }, this );
        }
        
        ,_setReadonlyAttr: function( value ) {
            this._set( "readonly", value == true );
            
            if( this.readonly ) {
                domClass.add( this.domNode, "worksPaperReadonly" );
                domClass.remove( this.domNode, "worksPaperWritable" );
                this.restartBlinking();
            } else {
                domClass.remove( this.domNode, "worksPaperReadonly" );
                domClass.add( this.domNode, "worksPaperWritable" );
                this.suspendBlinking();
            }
            
            if( this.graph !== undefined ) {
                array.forEach( this.get( "views" ), function( view ){
                    if( lang.isFunction( view.setReadonly ) ) {
                        view.setReadonly( this.readonly );
                    }
                }, this );
            }
        }
        
        ,_setMinWidthAttr: function( value ) {
            this._set( "minWidth", value );
            if( this._started ) {
                this.resize();
            }
        }

        ,_setMinHeightAttr: function( value ) {
            this._set( "minHeight", value );
            if( this._started ) {
                this.resize();
            }
        }
        
        ,performOperations: function( value ) {
            this.requireAll( 
                array.map( 
                    array.filter( value, function( opr ){ return opr.add !== undefined; } )
                    , function( op ){ return op["class"]; } ) 
                ).then( lang.hitch( this, function(){
                
                    array.forEach( value, function( operation ){
                
                        if( operation.add !== undefined ) {
                            operation.id = operation.add;
                            this.addElement( operation );
            
                        } else if( operation.remove !== undefined ) {
                            this.removeElement( operation.remove );
                
                        }
            
                    }, this );
            
                    this.resizePaper();
            }));

        }
        
        ,_setBlinkingAttr: function( value ) {
            this._set( "blinking", value );
            if( this.readonly ) {
                this.inherited( arguments );
            }
        }
        

    });
    
    Paper.markupFactory = _WidgetMixin.markupFactory;
    
    return Paper;
    
})
