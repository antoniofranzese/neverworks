define([
    "require"
    ,"dojo/query"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"works/drawing/factory"

    ,"dojo/NodeList-manipulate"
], function( _require, query, domStyle, domGeometry, factory ){
    
    return factory.declare( "works.drawing.html.Rect", "basic.Rect", {
        defaults: joint.util.deepSupplement({
            type: 'works.html.Rect',
            attrs: {
            rect: { stroke: 'none', 'fill-opacity': 0 }
            }
            ,selected: false

        }, joint.shapes.basic.Rect.prototype.defaults )

        ,constructor: function() {
            joint.shapes.basic.Rect.apply( this, arguments );
            this.setup();
        }

        ,setup: function() {}

        ,initialize: function() {
            this.inherited( arguments );

            this.on( "change:position", lang.hitch( this, this.onPositionChange ) );
            this.on( "change:size", lang.hitch( this, this.onSizeChange ) );
        }

        ,bindPosition: function( xProp, yProp ) {
            if( this._positionBinding === undefined ) {
                this._positionBinding = { x: xProp, y: yProp };
                this.on( "change:" + xProp, lang.hitch( this, this.refreshPosition ) )
                this.on( "change:" + yProp, lang.hitch( this, this.refreshPosition ) )
                this.refreshPosition();
            }
        }

        ,refreshPosition: function() {
            if( this._positionBinding !== undefined ) {
                this.set( "position", {
                    x: this.get( this._positionBinding.x )
                    ,y: this.get( this._positionBinding.y )
                });
            }
        }

        ,onPositionChange: function() {
            if( this._positionBinding !== undefined ) {
                var position = this.get( "position" );
                this.attributes[ this._positionBinding.x ] = position.x;
                this.attributes[ this._positionBinding.y ] = position.y;
            }
        }

        ,bindSize: function( wProp, hProp ) {
            if( this._sizeBinding === undefined ) {
                this._sizeBinding = { w: wProp, h: hProp };
                this.on( "change:" + wProp, lang.hitch( this, this.refreshSize ) )
                this.on( "change:" + hProp, lang.hitch( this, this.refreshSize ) )
                this.refreshSize();
            }
        }

        ,refreshSize: function() {
            if( this._sizeBinding !== undefined ) {
                this.set( "size", {
                    width: this.get( this._sizeBinding.w )
                    ,height: this.get( this._sizeBinding.h )
                });
            }
        }

        ,onSizeChange: function() {
            if( this._sizeBinding !== undefined ) {
                var size = this.get( "size" );
                this.attributes[ this._sizeBinding.w ] = size.width;
                this.attributes[ this._sizeBinding.h ] = size.height;
            }
        }

    });
   
});