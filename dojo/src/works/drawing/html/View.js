define([
    "require"
    ,"dojo/query"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"works/drawing/factory"

    ,"dojo/NodeList-manipulate"
], function( _require, query, domStyle, domGeometry, factory ){
    
    return factory.declare( "works.drawing.html.View", "joint.dia.ElementView", {

       template: null
       ,readonly: false
       
       ,initialize: function() {
           _.bindAll( this, 'updateBox' );
           
           // Super
           joint.dia.ElementView.prototype.initialize.apply(this, arguments);

           // Produzione del nodo
           this.$box = $( _.template( this.template )() );
           
           // Riferimento classico
           this.domNode = this.$box[ 0 ];
           
           // Evita il pointerdown della paper
           this.$box.find( 'input,select' ).on( 'mousedown click', function( evt ) { evt.stopPropagation(); });
           
           // Pulsante di cancellazione
           this.$box.find( '.delete' ).on( 'click', _.bind( this.notifyRemove, this ) );

           // Aggiornamento sui cambiamenti del modello
           this.model.on( 'change', this.updateBox, this );
           
           // Rimozione dalla view alla rimozione dal modello
           this.model.on( 'remove', this.removeBox, this );

           this.updateBox();
       }

       ,pointermove: function() {
           if( ! this.readonly ) {
               this.inherited( arguments );
           }
       }
       
       ,render: function() {
           this.paper.trigger( "view:rendering", this );
           joint.dia.ElementView.prototype.render.apply( this, arguments );
           this.paper.$el.prepend( this.$box );
           this.updateBox();
           return this;
       }

       ,updateView: function() {}
       
       ,updateBox: function() {
           
           this.updateView();

           var bbox = this.model.getBBox();

           domGeometry.setMarginBox( this.domNode, {
               w: bbox.width
               ,h: bbox.height
               ,l: bbox.x
               ,t: bbox.y
           });

           domStyle.set( this.domNode, {
               transform: 'rotate(' + ( this.model.get( 'angle' ) || 0 ) + 'deg)'
           });
           
       }
       
       ,notifyRemove: function( evt ) {
           this.paper.trigger( "element:remove", this.model );
       }
       
       ,removeBox: function( evt ) {
           this.$box.remove();
       }
   
       ,query: function( selector ) {
           return query( selector, this.domNode );
       }
       
       ,setReadonly: function( value ) {
           this.readonly = value;
       }
       
   });
    
    
    
});