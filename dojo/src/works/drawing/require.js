define([
    "./factory"
], function( factory ){
    
    var req = factory.require;
    return {
        load: function( id, require, callback ) {
            var className = id.replace( new RegExp( "\\/", "g" ), "." );
            //console.log( "Requiring", className );
            try {

                factory.require( className, function( cls ){
                    callback( cls );
                })
                
            } catch( e ) {
                console.error( "Error loading", id, "stencil:", e );
            }
        }
        
    }
    
})