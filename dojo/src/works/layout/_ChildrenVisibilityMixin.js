define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/dom-class"
], function( _require, declare, lang, array, domClass ){
    
    return declare( [], {
        registerChildrenVisibilityChanges: function( callback ) {
            this.visibilityWatches = [];
            array.forEach( this.getDescendants(), function( widget ) {
                if( widget.getParent() === this ) {
                    if( lang.isFunction( widget.watch ) ) {
                        this.visibilityWatches.push( widget.watch( "visible", function( name, oldValue, newValue ){
                            callback( widget, newValue ); 
                        }));
                    }
                }
            }, this );
        }
        
        ,unregisterChildrenVisibilityChanges: function() {
            if( this.visibilityWatches ) {
                array.forEach( this.visibilityWatches, function( watch ){
                    watch.remove();
                });
            }
            delete this.visibilityWatches;
        }

    });
    
})