define([
    "require"
    ,"dojo/_base/declare"
], function( _require, declare  ){
    
    return declare( [], {
        needsResizePropagation: true
    })
    
})