define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/Color"
    ,"dojo/dom-style"
    ,"dijit/layout/ContentPane"
    ,"dijit/_TemplatedMixin"
    ,"dijit/_Contained"
    ,"works/form/_WidgetMixin"
    ,"dojox/dtl/filter/strings"
    ,"dojo/text!./resources/RibbonPane.html"

    ,"works/style!./resources/RibbonPane.css"
], function( req, declare, Color, domStyle, ContentPane, _TemplatedMixin, _Contained, _WidgetMixin, strings, template ){
    
    return declare( declare.className( req ), [ ContentPane, _TemplatedMixin, _Contained ], {

        templateString: template,
        
    	baseClass: "ribbonpane",
	
    	align: "",
	
    	imageUrl: "",
	
    	contentValign: "top",
	
    	topTitleCorrection: 2,
	
    	sideTitleCorrection: 5,
	
    	postMixInProperties: function() {
    		this.inherited( arguments );
			if( this.align == "" ) {
				this.align = "center";
			}
    	},
	
    	startup: function() {
    		if( this._started ){ return; }

    		if( this.getParent() == null ) {
    			var divStyle = domStyle.getComputedStyle( this.domNode );
    			var hborders = this._px( divStyle.borderLeftWidth ) + this._px( divStyle.borderRightWidth );
    			var vborders = this._px( divStyle.borderTopWidth ) + this._px( divStyle.borderBottomWidth );
    			// Rettifica per bordi
    			if( hborders > 0 && domStyle.get( this.domNode, "width") > hborders ) {
    				domStyle.set( this.domNode, "width", domStyle.get( this.domNode, "width") - hborders );
    			}	
    			if( vborders > 0 && domStyle.get( this.domNode, "height" ) > vborders ) {
    				 domStyle.set( this.domNode, "height", domStyle.get( this.domNode, "height") - vborders );
    			}	
			
    			domStyle.set( this.tableNode, "width", domStyle.get( this.domNode, "width") );
    			domStyle.set( this.tableNode, "height", domStyle.get( this.domNode, "height") );
	
    		}

    		var cellStyle = domStyle.getComputedStyle( this.titleCellNode );
    		var titleSize = cellStyle.fontSize.replace( "px", "" );
    		this._titleCellSize = parseInt( titleSize ) + this.topTitleCorrection;

    		this._calculateContentBox();
    		this.inherited( arguments );
    	} ,
	
    	resize: function( changeSize, resultSize ) {
    		this._resizeInProgress = true;
		
    		this.inherited( arguments );
    		this._calculateContentBox();

    		domStyle.set( this.tableNode, "width", domStyle.get( this.domNode, "width") );
    		domStyle.set( this.tableNode, "height", domStyle.get( this.domNode, "height") );
	
    		this._resizeInProgress = false;
    		this._layoutChildren();

    	},
	
    	_layoutChildren: function() {
    		if( !(this._resizeInProgress) ) {
    			this.inherited( arguments );
    		}
    	},
	
    	_calculateContentBox: function() {
    		var box = {
    			h: domStyle.get( this.domNode, "height"),
    			w: domStyle.get( this.domNode, "width" )
    		};
    		//console.dir( box );
    		//var titleBox = dojo.contentBox( this.titleCellNode );
			this._contentBox = {
				t: 0,
				l: 0,
				w: box.w,
				h: box.h - this._titleCellSize
			}
		
    	},
	
    	_px: function( size ) {
    		return parseInt( size.replace( "px", "" ) )
    	}
	
    });

})
