define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/fx"
    ,"dojo/fx/easing"
], function( req, declare, fx, easing ){
    
    return declare( declare.className( req ), [], {

        constructor: function( params ) {
            this.speeds = [];
            declare.safeMixin( this, params );
        }
        
        ,finish: function() {
            if( this.speeds.length > 0 ) {
                // Average speed
                var speed = this.speeds.reduce( function( p, v ) { return p + v }, 0 ) / this.speeds.length;
     
                if( Math.abs( speed ) > 0.05 ) {
                    var momentum = Math.min( Math.floor( Math.abs( speed ) * 40 ), 300 );
                    var direction = speed > 0 ? 1 : -1;

                    this.stop();

                    this.scrollAnimation = new fx.Animation({
                        curve: [ momentum, 0 ]
                        ,easing: easing.quartOut
                        ,duration: 1750 + Math.floor( momentum * 5 )
                        ,onAnimate: lang.hitch( this, function( value ) {
                            if( value > 1 ) {
                                if( this.onScroll( Math.floor( value ) * direction ) == false ) {
                                    this.stop();
                                };
                            } else {
                                this.stop();
                            }
                        })
                        ,onStop: lang.hitch( this, this.onEnd )
                    }).play();
                    
                }
            } else {
                this.onEnd();
            }
            this.reset();
        }
        
        ,track: function( value ) {
            if( this.lastValue === undefined ) {
                this.lastValue = value;
                this.lastTime = new Date().getTime();
            } else {
                // Autovelox
                if( this.speeds.length == 3 ) {
                    this.speeds = this.speeds.slice( 1 );
                }
                var now = new Date().getTime();
                var time = now - this.lastTime;
                var distance = this.lastValue - value;
                this.speeds.push( distance / time );
                this.lastValue = value;
                this.lastTime = now;
                this.onTrack( distance );
            }
            return this;
        }

        ,tracking: function() {
            return this.speeds.length > 0;
        }

        ,start: function( value ) {
            this.reset();
            this.onStart();
            this.track( value );
        }
        
        ,restart: function( value ) {
            this.stop();
            this.onStart();
            this.track( value );
        }

        ,active: function() {
            return this.scrollAnimation !== undefined;
        }
        
        ,stop: function() {
            if( this.scrollAnimation ) {
                this.scrollAnimation.stop();
                delete this.scrollAnimation;
            }
            return this;
        }
        
        ,reset: function() {
            delete this.lastValue;
            delete this.lastTime;
            this.speeds = [];
            return this;
        }
        
        ,onScroll: function( value ){
            
        }

        ,onStart: function(){
            
        }

        ,onEnd: function(){
            
        }
        
        ,onTrack: function( value ) {
            
        }

    });
    
});