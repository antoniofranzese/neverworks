define([
    "require"
    ,"works/lang/declare"
    ,"dojo/on"
    ,"dojo/topic"
    ,"./Layer"
], function( req, declare, on, topic, Layer ){
    
    var Underlays = declare( [], {
       
        references: 0
        
        ,show: function() {
            this.references += 1;
            if( this.references == 1 ) {
                topic.publish( "widget/layout/underlay/show" );
            }
        }
        
        ,hide: function() {
            this.references -= 1;
            if( this.references == 0 ) {
                topic.publish( "widget/layout/underlay/hide" );
            }
        }
    
    })();
    
    return declare( declare.className( req ), [ Layer ], {
        baseClass: "worksUnderlay"
        
        ,constructor: function( params ) {
            on( this, "show", function() {
                Underlays.show();
            });
            
            on( this, "hide", function() {
                Underlays.hide();
            });
        }
        
    });
    
});