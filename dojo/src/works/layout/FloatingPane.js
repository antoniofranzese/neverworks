define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/connect"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/aspect"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-construct"
    ,"dojo/Deferred"
    ,"dojox/layout/FloatingPane"
    ,"../form/_WidgetMixin"
    ,"../form/_SupportingWidget"
    ,"./_ContentWidget"
], function( _require, declare, connectUtil, lang, on, aspect, domStyle, domClass, domGeometry, domConstruct, Deferred, dojoxFloatingPane, _WidgetMixin, _SupportingWidget, _ContentWidget ){
    
    var BodyMovingConstraint = declare([],{

        constructor: function( panel ) {
            this.panel = panel;
        }
        
        ,registerHost: function( movingHost ) {
            this.aspects = [];
            this.aspects.push( aspect.after( movingHost, "onMoveStart", lang.hitch( this, this.onMoveStart ), /*receiveArguments*/ true ) );
            this.aspects.push( aspect.after( movingHost, "onMoveStop", lang.hitch( this, this.onMoveStop ), /*receiveArguments*/ true ) );
            this.aspects.push( aspect.after( movingHost, "onMoving", lang.hitch( this, this.onMoving ), /*receiveArguments*/ true ) );
            return this;
        }
        
        ,remove: function() {
            if( this.aspects !== undefined ) {
                for( var i = 0; i < this.aspects.length; i++ ) {
                    this.aspects[ i ].remove();
                }
            }
        }
        
        ,onMoveStart: function( mover ) {
            var containerPos = domGeometry.position( this.panel.getParent().domNode );
            this.lastContainerSize = { w: containerPos.w, h: containerPos.h };
            var pos = domGeometry.position( this.panel.domNode );
            this.lastOwnSize = { w: pos.w, h: pos.h };
        }
        
        ,onMoveStop: function( mover ) {
            delete this.lastContainerSize;
            delete this.lastOwnSize;
            this.panel.set( "origin", {
                x: domStyle.toPixelValue( this.panel.domNode, this.panel.domNode.style.left )
                ,y: domStyle.toPixelValue( this.panel.domNode, this.panel.domNode.style.top )
            });
        }
        
        ,onMoving: function( mover, origin ) {
            if( origin.l + this.lastOwnSize.w > this.lastContainerSize.w ) {
                origin.l = this.lastContainerSize.w - this.lastOwnSize.w - 2; // TODO: calcolare dimensione bordi
            }

            if( origin.t + this.lastOwnSize.h > this.lastContainerSize.h ) {
                origin.t = this.lastContainerSize.h - this.lastOwnSize.h - 2; // TODO: calcolare dimensione bordi
            }

            if( origin.l < 0 ) {
                origin.l = 0;
            }
            
            if( origin.t < 0 ) {
                origin.t = 0;
            }
        }
    });
    
    
    var FloatingPane = declare( [ dojoxFloatingPane, _SupportingWidget ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,dockable: false
        ,maxable: false
        ,working: false
        ,minWidth: 300
        ,minHeight: 200
        
        ,buildRendering: function() {
            this.inherited( arguments );
            domClass.add( this.domNode, "worksFloatingPane" );
            this.overlayNode = domConstruct.create( "div", { "class": "worksFloatingPaneOverlay loading" }, this.canvas );

            if( this.resizable ) {
                this.resizeNode = domConstruct.create( "span", { 
                    "class": "worksFloatingResizeIcon" 
                    ,innerHTML: "&nbsp;"
                });
                
                domConstruct.place( this.resizeNode, this.restoreNode, "after" );
                
                this.own(
                    on( this.resizeNode, "click", lang.hitch( this, this.onSizerClick ) )
                );
            }
            
        }
        
        ,startup: function() {
            this.inherited( arguments );
            
            var borders = domGeometry.getPadBorderExtents( this.domNode );
            if( this._resizeHandle !== undefined ) {
                this._resizeHandle.minSize = { 
                    w: this.minWidth - borders.w, 
                    h: this.minHeight - borders.h + domGeometry.position( this.focusNode ).h 
                };
            }

            this.own(
        		topic.subscribe( "/dnd/move/start", lang.hitch( this, this.registerMovingHost ) )
            );

            domStyle.set( this.containerNode, { overflow: "hidden" } );
            var firstChild = this.getDescendants()[ 0 ];
            if( firstChild && firstChild.isInstanceOf( _ContentWidget ) ) {
                this.contentWidget = firstChild;
                
                this.contentWidget.watch( "title", lang.hitch( this, function( name, oldValue, newValue ){
                    this.set( "title", newValue );
                }));

                this.contentWidget.watch( "origin", lang.hitch( this, function( name, oldValue, newValue ){
                    this.set( "origin", newValue );
                }));

                this.contentWidget.contentReady().then( lang.hitch( this, function(){
                    //domStyle.set( this.overlayNode, { visibility: "hidden" });
                    domClass.remove( this.overlayNode, "loading" );
                }));

            } else {
                throw new Error( "First child must be a _ContentWidget" );
            }
         
        }
        
        ,onSizerClick: function( evt ) {
            if( this.sizerWidget == undefined ) {
                this.sizerWidget = this.getParent().get( "sizer" );
                this.sizerWidget.openAround( this.resizeNode ).then( lang.hitch( this, function( size ){
                    if( size !== undefined ) {
                        var mb = domGeometry.getMarginBox( this.domNode );
                        var cb = domGeometry.getContentBox( this.domNode );
                    
                        // Correzione content box
                        size.h = size.h - ( mb.h - cb.h ); 
                        size.w = size.w - ( mb.w - cb.w );
                     
                        this.resize( size );
                    }
                    delete this.sizerWidget;
                }));
            } else {
                this.sizerWidget.hide();
                delete this.sizerWidget;
            }
        }
        
        ,resize: function( size ) {
            if( this.working ) {
                if( size ) {
                    if( size.x !== undefined && size.y !== undefined ) {
                        this.set( "origin", { x: size.x, y: size.y });
                    } else if( size.l !== undefined && size.t !== undefined ) {
                        this.set( "origin", { x: size.l, y: size.t });
                    }
                }
            
                if( this.resizable ) {
                    
                    if( size === undefined ) {
                        if( this.contentWidget ) {

                            if( this.width !== undefined && this.height !== undefined ) {
                                this.resizeCanvas();

                            } else {
                                this.contentWidget.resize();

                                var contentPos = domGeometry.position( this.contentWidget.domNode );
                                var width = this.get( "width" ) !== undefined ? this.get( "width" ) : contentPos.w;
                                var height = this.get( "height" ) !== undefined ? this.get( "height" ) : contentPos.h;
                    
                                this.canvas.style.width = width + "px";
                                this.canvas.style.height = height + "px";
                    
                                this.set( "width", width );
                                this.set( "height", height );                          
                            }
                        }
                    
                    } else {
                        if( size.w !== undefined && size.h !== undefined ) {
                    
                            var parentPos = domGeometry.position( this.getParent().domNode );
                            var titlePos = domGeometry.position( this.focusNode );
                            var origin = this.get( "origin" );
                            var borders = domGeometry.getPadBorderExtents( this.domNode );
                            
                            if( size.w < this.minWidth ) {
                                size.w = this.minWidth;
                            }

                            if( size.h < this.minHeight ) {
                                size.h = this.minHeight;
                            }

                            if( ( origin.x + size.w ) > parentPos.w ) {
                                size.w = parentPos.w - origin.x - borders.w - 2;
                            }

                            if( ( origin.y + size.h + titlePos.h ) > parentPos.h ) {
                                size.h = parentPos.h - origin.y - borders.h - 2;
                            }

                            //this.inherited( arguments, size );
 
                            this.set( "width", size.w );
                            this.set( "height", size.h - titlePos.h );
                            this.resizeCanvas();
                        }
                    }
                } else {
                    if( this.width !== undefined && this.height !== undefined ) {
                        this.resizeCanvas();
                    }
                }
                
            }
            
        }
        
        ,resizeCanvas: function() {
            this.domNode.style.width = null;
            this.domNode.style.height = null;
            this.canvas.style.width = ( this.width < this.minWidth ? this.minWidth : this.width ) + "px";
            this.canvas.style.height = ( this.height < this.minHeight ? this.minHeight : this.height ) + "px";
            
            var canvasPos = domGeometry.position( this.canvas );
            this.contentWidget.resize({ w: canvasPos.w, h: canvasPos.h });
        }
        
        ,_getOriginAttr: function() {
            return this._origin;
        }
        
        ,_setOriginAttr: function( value ) {
            this.domNode.style.left = ( typeof value.x == "number" ? value.x + "px" : value );
            this.domNode.style.top = ( typeof value.y == "number" ? value.y + "px" : value );
            this._origin = value;
        }
        
        ,_setWidthAttr: function( w ) {
            if( typeof w == "string" ) {
                w = domStyle.toPixelValue( this.domNode, w );
            }
            this._set( "width", w );
        }
        
        ,_setHeightAttr: function( h ) {
            if( typeof h == "string" ) {
                h = domStyle.toPixelValue( this.domNode, h );
            }
            this._set( "height", h );
        }

        ,_setMinWidthAttr: function( w ) {
            if( typeof w == "string" ) {
                w = domStyle.toPixelValue( this.domNode, w );
            }
            this._set( "minWidth", w );
            if( this._resizeHandle !== undefined ) {
                this._resizeHandle.minWidth = w;
            }
        }
        
        ,_setMinHeightAttr: function( h ) {
            if( typeof h == "string" ) {
                h = domStyle.toPixelValue( this.domNode, h );
            }
            this._set( "minHeight", h );
            if( this._resizeHandle !== undefined ) {
                this._resizeHandle.minHeight = h;
            }
        }
        
        ,joinForm: function( form ) {
            this.parentForm = form;
            this.contentWidget.joinForm( form );
        }
        
        ,leaveForm: function( form ) {
            this.contentWidget.leaveForm( form );
            delete this.parentForm;
        }
        
        ,load: function( params ) {
            return this.contentWidget.load( params );
        }
        
        ,registerContent: function() {
            return this.contentWidget.registerContent();
        }
        
        ,unregisterContent: function() {
            return this.contentWidget.unregisterContent();
        }
        
        ,registerMovingHost: function( mover ) {
            if( this.movingHost == undefined && mover.host.node == this.domNode ) {
                this.movingHost = mover.host;
                this.movingConstraint = new BodyMovingConstraint( this );
                this.own(
                    this.movingConstraint.registerHost( this.movingHost )
                )
                // Recupera la prima volta
                this.movingConstraint.onMoveStart( mover );
            }
        }
        
        ,maximize: function() {
            this.emit( "maximized", { bubbles: false } );
        }
        
        ,maximizeWindow: function( size ) {
    		if(this._maximized){ return; }
            var mb = domGeometry.getMarginBox( this.domNode );
            var cb = domGeometry.getContentBox( this.domNode );
    		this._naturalState = {
    		    x: mb.l
                ,y: mb.t
                ,w: cb.w
                ,h: cb.h
    		};

    		if( this._isDocked ){
    			this.show();
    			setTimeout( lang.hitch( this, "maximizeSize" ), this.duration );
    		}
    		domClass.add( this.domNode, "floatingPaneMaximized" );
    		domClass.add( this.focusNode, "floatingPaneMaximized" );
    		this.resize( size );
    		this._maximized = true;
            
            domStyle.set( this.resizeNode, { visibility: "hidden" } );
        }
        
        ,_restore: function(){
            this.emit( "restored", { bubbles: false } );
        }

        ,restoreWindow: function() {
    		if( this._maximized ){
    			this.resize( this._naturalState );

    			domClass.remove( this.domNode, "floatingPaneMaximized");
    			domClass.remove( this.focusNode, "floatingPaneMaximized");

    			this._maximized = false;
                
                if( this.resizable ) {
                    domStyle.set( this.resizeNode, { visibility: "visible" } );
                }
    		}
            
        }
        
        ,_getSizeAttr: function( node ) {
            var mb = domGeometry.getMarginBox( node );
            return { x: mb.l, y: mb.t, w: mb.w - 2, h: mb.h - 2 };
        }
        
        ,close: function() {
    		if( !this.closable ){ return; }
            this.emit( "closed", { bubbles: false } );
        }

    	,closeWindow: function(){
            this.closeDeferred = new Deferred();
    		connectUtil.unsubscribe( this._listener );
    		this.hide( lang.hitch( this, function(){
    			this.destroyRecursive();
                this.closeDeferred.resolve();
    		}));
            return this.closeDeferred;
    	}

        ,bringToTop: function() {
            if( ! this.get( "selected" ) ) {
                this.emit( "selected", { bubbles: false } );
            }
        }

        ,_getSelectedAttr: function() {
            return domClass.contains( this.domNode, "dojoxFloatingPaneFg" );
        }
        
        
        ,_getNameAttr: function() {
            return this.contentWidget.get( "name" );
        }

    });
    
    var base = {
        bringToTop: dojoxFloatingPane.prototype.bringToTop
    };
    
    FloatingPane.prototype.selectWindow = function() {
        base.bringToTop.apply( this );
    }
    
    return FloatingPane;
})