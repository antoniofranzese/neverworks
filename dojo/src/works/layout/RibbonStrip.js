define([
    "require"
    ,"works/lang/declare"
    ,"dojo/has!mobile?./MobileScrollPane:dojox/layout/ScrollPane"

], function( req, declare, basePane ){
    
    return declare( declare.className( req ), [ basePane ], {

    });
    
});