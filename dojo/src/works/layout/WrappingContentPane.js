define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/topic"
    ,"works/layout/ContentPane"
    ,"works/form/_WidgetMixin"
    ,"works/form/_SupportingWidget"
    
], function( _require, declare, lang, on, topic, ContentPane, _WidgetMixin, _SupportingWidget ){
    
    return declare( [ ContentPane, _SupportingWidget ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,_getNameAttr: function() {
            var wrapped = this.get( "wrapped" );
            if( wrapped ) {
                return wrapped.get( "name" );
            } else {
                return undefined;
            }
            
        }

        ,_getCanonicalNameAttr: function() {
            var wrapped = this.get( "wrapped" );
            if( wrapped ) {
                return wrapped.get( "canonicalName" );
            } else {
                return undefined;
            }
            
        }

        ,_getWrappedAttr: function() {
            return this.getDescendants()[0];
        }
        
        ,setContentSize: function() {
            this.domNode.style.width = null;
            this.domNode.style.height = null;
        }
        
    });
    
})