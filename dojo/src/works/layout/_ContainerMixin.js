define([
    "dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/dom-construct"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/parser"
    ,"dojo/on"
    ,"dojo/aspect"
    ,"dojo/topic"
    ,"dojo/Deferred"
    ,"dojo/promise/all"
    ,"../layout/ContentPane"
    ,"../form/_WidgetMixin"
    ,"../form/_FormWidgetMixin"
    ,"./_FixedSizeMixin"
    ,"./_ParentQualifyMixin"
    ,"./_ViewportMixin"
    ,"./Loader"
        
], function( declare, array, lang, domConstruct, domClass, domStyle, parser, on, aspect, topic, Deferred, all, 
    ContentPane, _WidgetMixin, _FormWidgetMixin, _FixedSizeMixin, _ParentQualifyMixin, _ViewportMixin, Loader ){
    
    var ContainerMixin = declare( [ _WidgetMixin, _FormWidgetMixin, _FixedSizeMixin, _ParentQualifyMixin, _ViewportMixin ], {
        startupSelected: 0
        ,loader: false
        
        ,startup: function() {
            this.inherited( arguments );
            array.forEach( this.getChildren(), this.updateChildVisibility, this );
            array.forEach( this.getChildren(), this.subscribeChild, this );
            this.set( "selectedIndex", this.startupSelected );
            this.updateSelectedChild();
            this._contentReadyDeferred = new Deferred();
            this._contentReadyDeferred.resolve();
        }
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.form = form;
            this.announceEvent( "selecting" );
            this.announceEvent( "selected", { rename: "select" } );
            this.announceEvent( "closing" );
            this.announceEvent( "closed", { rename: "close" } );
        }
        
        ,leaveForm: function( form ) {
            this.retireEvent( "selecting" );
            this.retireEvent( "selected" );
            this.retireEvent( "closing" );
            this.retireEvent( "closed" )
            this.form = null;
            this.inherited( arguments );
        }
        
		,selectChild: function( page ){
            if( lang.isFunction( page.resize ) && page._containerMixinResized === undefined ) {
                page._containerMixinResized = true;
                setTimeout( function(){
                    page.resize();
                }, 0 );
            }

            if( this._updating ) {
                this.inherited( arguments );
            } else {
                if( this.selectedChildWidget != page ) {
                    var state = { state:{
                        "old": array.indexOf( this.getChildren(), this.selectedChildWidget )
                        ,"new": array.indexOf( this.getChildren(), page )
                    }}
                
                    if( this.eventActive( "selecting" ) ) {
                        this.emitEvent( "selecting", state );
                    } else {
                        this.inherited( arguments );
                        if( !this._updating ) {
                            this.emitEvent( "selected", state );
                        }
                    }
                }
            }
        }
        
        ,closeChild: function( page ) {
            var index = array.indexOf( this.getChildren(), page );
            
            if( this.eventActive( "closing" ) ) {
                //console.log( "Closing", page );
                if( this.expectedRemoval != undefined ) {
                    delete this.expectedRemoval;
                }
                this.emitEvent( "closing", { state: { index: index } } );

            } else {
                //console.log( "Closed", page );
                this._updating = true;
			    this.form.unregisterWidget( page );
                if( lang.isFunction( page.unregisterContent ) ) {
                    page.unregisterContent();
                }
                this.inherited( arguments );
                delete this._updating;
            
                
                // L'evento close va sempre emesso
                this.expectedRemoval = index;
                this.emitAlways( "closed", { state: { index: index } } );
            }
        }
        
        ,_getSelectedIndexAttr: function() {
            return array.indexOf( this.getChildren(), this.selectedChildWidget );
        }

        ,_setSelectedIndexAttr: function( value ) {
            if( value != null ) {
                var children = this.getChildren();
                if( children.length > 0 ) {
                    if( value < 0 ) {
                        value = 0;
                    } else if( value > children.length -1 ) {
                        value = children.length -1;
                    }
                    this._updating = true;
                    this._performChildSelection( this.getChildren()[ parseInt( value ) ] );
                    delete this._updating;
                } 
            }
        }

        ,_performChildSelection: function( child ) {
            this.selectChild( child );
        }
		
        ,contentReady: function() {
            //console.log( "ContentReady requested on ", this.declaredClass, this.name );
            return this._contentReadyDeferred;
        }
        
        ,getWidgetState:function(){
			return {
				selectedIndex: this.get( "selectedIndex" )
			}
		}
        
        ,setWidgetState: function( state ) {
            if( state.loader != undefined ) {
                this.set( "loader", loader );
                delete state.loader;
            }
            
            if( state.operations != undefined ) {
                var operations = state.operations;
                delete state.operations;
                this._updating = true;
                this._contentReadyDeferred = new Deferred();
                //console.log( "Performing operations on", this.name );
                this.performOperations( operations ).then( lang.hitch( this, function(){
                    delete this._updating;
                    this.setWidgetState( state );
                    this.onOperationsPerformed(); 
                    //console.log( "Operations performed on", this.name ); 
                    if( ! this._contentReadyDeferred.isResolved() ) {
                        this._contentReadyDeferred.resolve();
                    }
                }));
            } else {
                this.inherited( arguments );
            }

        }
    
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }
        
        ,onOperationsPerformed: function() {
            
        }

        ,performOperations: function( operations ) {
            var deferreds = [];
            for( var i = 0; i < operations.length; i++ ) {
                if( operations[ i ].type == "redraw" ) {
                    deferreds.push( this.performChildRedraw( operations[ i ] ) );
                } else if( operations[ i ].type == "insert" ) {
                    deferreds.push( this.performChildInsert( operations[ i ] ) );
                } else if( operations[ i ].type == "remove" ) {
                    deferreds.push( this.performChildRemove( operations[ i ] ) );
                }
            }
            return all( deferreds );
        }
        
        ,performChildRedraw: function( params ) {
            var d = new Deferred();
            var child = this.getChildren()[ params.index ];
            var visible = params.visible != false;
            if( child.isInstanceOf( ContentPane ) ) {
                if( visible && child === this.selectedChildWidget && child.get( "loader" ) == true ) {
                    this.showLoader( child.domNode.parentNode );
                }
                topic.publish( "works/widget/change" );
                this.startWidgetTask({ reason: "Child redraw" });
                child.load({ content: params.content }).then( lang.hitch( this, function(){
                    this.updateChildVisibility( child );
                    this.endWidgetTask({ reason: "Child redraw" });
                    if( child === this.selectedChildWidget ) {
                        this.hideLoader();
                    }
                    d.resolve();
                }));
            } else {
                throw new Error( "Child #" + params.index + " is not redrawable" );
            }
        }
        
        ,performChildInsert: function( params ) {
            var d = new Deferred();
            topic.publish( "works/widget/change" );
            
            var visible = params.visible != false;
            //console.log( "Visible", visible, params.visible );
            if( visible && this.loader ) {
                this.stickLoader();
            }

            this.startWidgetTask({ reason: "Child insert" });
            
            var node = domConstruct.toDom( "<div>" + params.content + "</div>" );
            domConstruct.place( node, this.domNode );
            
            var widgets = parser.parse({ rootNode: node }).then( lang.hitch( this, function(){
                var widget = registry.byNode( node.firstChild );
            
                domConstruct.place( node.firstChild, this.domNode );
                this.addChild( widget, params.index + 1 );
                
                //console.log( "Widget", widget.id, widget.get( "visible" ) );
                this.updateChildVisibility( widget );
                this.form.registerWidget( widget );
                
                if( lang.isFunction( widget.registerContent ) ) {
                    widget.registerContent().then( lang.hitch( this, function(){
                        domConstruct.destroy( node );
                        this.subscribeChild( widget );
                        this.endWidgetTask({ reason: "Child insert" });
                        this.hideLoader();
                        d.resolve();
                    }));
                } else {
                    throw new Error( "Cannot register new page content" );
                }
                
            }));
            return d;
        }
        
        ,performChildRemove: function( params ) {
            var d = new Deferred();
            if( this.expectedRemoval != undefined && params.index == this.expectedRemoval ) {
                //console.log( "Expected removal:", params.index );
                delete this.expectedRemoval;
            } else {
                topic.publish( "works/widget/change" );
                var page = this.getChildren()[ params.index ];
                //console.log( "Removing:", page );
				
			    this.form.unregisterWidget( page );
                if( lang.isFunction( page.unregisterContent ) ) {
                    page.unregisterContent();
                }
                
                // Non passa per il closeChild
                this.removeChild( page );
				page.destroyRecursive();
                
            }
            d.resolve();
            return d;
        }
        
        ,subscribeChild: function( child ) {
            if( lang.isFunction( child.emit ) && lang.isFunction( child.own ) ) {
                child.own(
                    on( child, "redrawing", lang.hitch( this, lang.partial( this.onChildRedrawing, child ) ) )
                    ,on( child, "redrawn", lang.hitch( this, lang.partial( this.onChildRedrawn, child ) ) )
                    ,child.watch( "visible", lang.hitch( this, lang.hitch( this, lang.partial( this.onChildVisibility, child ) ) ) ) 
                );
            }
        }

        ,onChildVisibility: function( child ) {
            this.updateChildVisibility( child );
            this.updateSelectedChild();
        }
        
        ,updateSelectedChild: function() {
            if( this.selectedChildWidget ) {
                if( this.selectedChildWidget.get( "visible" ) == false ) {
                    var children = this.getChildren();
                    var i = 0;
                    do {
                        this.selectChild( children[ i++ ] );
                    } while( i < children.length && this.selectedChildWidget.get( "visible" ) != true );
                } else {
                    this._showChild( this.selectedChildWidget );
                }
            }
        }
        
        ,updateChildVisibility: function( child ) {
            
        }
        
        ,onChildRedrawing: function( child ) {
            //console.log( "Child redrawing for " + this );
            if( child === this.selectedChildWidget && child.get( "loader" ) == true ) {
                this.showLoader( child.domNode.parentNode );
            }
        }
        
        ,onChildRedrawn: function( child ) {
            //console.log( "Child redrawn for " + this );
            if( lang.isFunction( child.resize ) ) {
                child.resize();
            }
            this.hideLoader();
        }
        
        ,appendChild: function( params ) {
            var title       = "title"       in params ? params.title : "<notitle>";
            var name        = "name"        in params ? params.name : null;
            var styleClass  = "styleClass"  in params ? params.styleClass : undefined;
            var widgetClass = "widgetClass" in params ? params.widgetClass : ContentPane;
            var closable    = "closable"    in params ? params.closable : true;
            
            var attrs = styleClass ? { "class": styleClass } : {};
            var node = domConstruct.create( "div", attrs, this.domNode );
            var widget = new widgetClass({ 
                title: title
                ,name: name
                ,closable: closable 
            }, node );
            this.addChild( widget );
            return widget; 
        }
        
        ,findChildren: function( params ) {
            params = lang.mixin( {}, params );
            delete params[ "styleClass" ];
            delete params[ "widgetClass" ];
            
            var children = this.getChildren();
            var result = [];
            for( var c in children ) {
                var matches = true;
                for( var prop in params ) {
                    if( params[ prop ] != children[ c ].get( prop ) ) {
                        matches = false;
                    }
                }
                if( matches ) {
                    result.push( children[ c ] );
                }
            }
            return result;
        }
        
        ,load: function( params ) {
            var href = params.href;
            var content = params.content;
            delete params[ "href" ];
            delete params[ "content" ];

            //console.log( "find", params );
            var children = this.findChildren( params );
            if( children.length <= 1 ) {
                var widget = children.length == 0 ? this.appendChild( params ) : children[ 0 ];

                if( lang.isFunction( widget[ "load" ] ) ) {
                    widget.load({ href: href, content: content });
                    this.selectChild( widget );
                } else {
                    throw new Error( "Missing 'loadContent' method on " + widget );
                }

            } else {
                throw new Error( "" + children.length + " children found for " + params );
            }
        }
        
        ,stickLoader: function( node ) {
            if( this.loaderWidget === undefined ) {
                this.loaderWidget = new Loader();
            }
            this.loaderWidget.stick( this.containerNode );
        }

        ,showLoader: function( node ) {
            if( this.loaderWidget === undefined ) {
                this.loaderWidget = new Loader();
            }
            this.loaderWidget.show( node ? node : this.containerNode );
        }
        
        ,hideLoader: function() {
            if( this.loaderWidget !== undefined ) {
                this.loaderWidget.hide();
            }
        }
		
        ,resizeContainer: function() {
            if( this.width === undefined ) {
                domStyle.set( this.domNode, "width", null );
            }
            
            if( this.height === undefined ) {
                domStyle.set( this.domNode, "height", null );
            }
            
            if( this.width !== undefined || this.height !== undefined ) {
                this.resize({ 
                    w: typeof this.width == "string" ? domStyle.toPixelValue( this.domNode, this.width ) : this.width
                    ,h: typeof this.height  == "string" ? domStyle.toPixelValue( this.domNode, this.height ) : this.height
                });
            } else {
                this.resize();
            }
        }
        
        ,_setWidthAttr: function( value ) {
            this._set( "width", value );
            this.resizeContainer();
        }

        ,_setHeightAttr: function( value ) {
            this._set( "height", value );
            this.resizeContainer();
        }
        
		,_showChild: function( page ){
            //console.log( "Showing", page.declaredClass, page.title );
			var result = this.inherited( arguments );
            if( result != false ) {
                lang.isFunction( page.spreadMessage ) && page.spreadMessage( { topic: "widget/container/show" } );
            }
            return result;
		}

		,_hideChild: function( page ){
            //console.log( "Hiding", page.declaredClass, page.title );
            lang.isFunction( page.spreadMessage ) && page.spreadMessage( { topic: "widget/container/hide" } );
            this.inherited( arguments );
        }
        
        ,spreadChildren: function( message ) {
            if( message.topic == "widget/container/show" ) {
                this.selectedChildWidget 
                && lang.isFunction( this.selectedChildWidget.spreadMessage )
                && this.selectedChildWidget.spreadMessage( message );

            } else {
                this.inherited( arguments );
            }
        }
        
    
    });
    
    ContainerMixin.markupFactory = _WidgetMixin.markupFactory;
    ContainerMixin.inferDeclaredClass = _WidgetMixin.inferDeclaredClass;
    
    return ContainerMixin;
});	
