define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/topic"
    ,"dojo/on"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dijit/layout/BorderContainer"
    ,"works/layout/ContentPane"
    ,"works/layout/_StaticLayoutWidget"
    ,"works/layout/_ChildrenVisibilityMixin"
    ,"works/layout/_ParentQualifyMixin"
    ,"works/form/_WidgetMixin"
    ,"works/_WidgetUtils"
    ,"works/local-style"

    ,"works/style!./resources/PinboardLayout.css"
    
], function( _require, declare, lang, array, topic, on, domStyle, domGeometry, domClass, BorderContainer, ContentPane, _StaticLayoutWidget, _ChildrenVisibilityMixin, _ParentQualifyMixin, _WidgetMixin, utils, localStyle ){
    
    return declare( [ ContentPane, _ChildrenVisibilityMixin, _StaticLayoutWidget, _ParentQualifyMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,baseClass: "worksPinboardLayoutPane"
        
        ,startup: function() {
            this.inherited( arguments );
            this.contentReady().then( lang.hitch( this, function(){
                this.registerChildrenVisibilityChanges( function( widget, value ){
                    var widgetNode = widget.domNode;
                    if( widgetNode && widgetNode.parentNode && domClass.contains( widgetNode.parentNode, "pinboardCell" ) ) {
                        if( value == true ) {
                            domClass.remove( widgetNode.parentNode, "hidden" );
                        } else {
                            domClass.add( widgetNode.parentNode, "hidden" );
                        }
                    }
                });
            }));
        }
        
        ,destroy: function() {
            this.unregisterChildrenVisibilityChanges();
            this.inherited( arguments );
        }
        
        ,parentIsLayoutContainer: function() {
            return this.getParent() && ( 
                this.getParent().isLayoutContainer == true 
                && this.getParent().isFormManager != true 

                // //TODO: correggere il resize del LabelPane
                // && this.getParent().isLabelPane != true
            )
        }
        
        ,_layoutChildren: function() {
            var resizables = [];
            array.forEach( this.getChildren(), function( child ){
                if( lang.isFunction( child.cellResizing ) ) {
                    //console.log( "child resizing", child );
                    var node = child.cellResizing( this );
                    if( node !== undefined && node != null ) {
                        resizables.push({ 
                            widget: child
                            ,node: node
                        });
                    }
                }
            }, this );
            
            array.forEach( resizables, function( resizable ){
                var node = resizable.node.parentNode;
                var size = domGeometry.getContentBox( node );
                var pads = domGeometry.getPadExtents( node );
                size.w = size.w - pads.w;
                size.h = size.h - pads.h;
                resizable.size = size;
            }, this );
            
            array.forEach( resizables, function( resizable ){
                if( lang.isFunction( resizable.widget.cellResize ) ) {
                    //console.log( "child resize", resizable.widget, resizable.size );
                    resizable.widget.cellResize( resizable.size );
                }
            }, this );
            
        }
        
        ,getChildren: function() {
            return array.filter( this.getDescendants(), function( child ){
                return lang.isFunction( child.getParent ) && child.getParent() == this;
            }, this );
        }
        
        ,setWidgetState: function( state ) {
            for( var name in state ) {
                this.set( name, state[ name ] );
            }
        }

        ,_setScrollableAttr: function( value ) {
            domClass.removeAll( this.domNode, /scroll-.*/ );
            if( value ) {
                domClass.add( this.domNode, "scroll-" + value )
            }
            this._set( "scrollable", value );
        }
        
        ,_setBorderAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setColumnsAttr: function( columns ) {
            var style = localStyle( this, "columns" );
            if( columns ) {
                for( var i = 0; i < columns.length; i++ ) {
                    style.set( "## div.pinboardCell.col-" + i, { width: columns[ i ].width } );
                }
                style.write();
            } else {
                style.remove();
            }
        }

        ,_setSpacingAttr: function( size ) {
            var style = localStyle( this, "spacing" );
            if( size ) {
                style
                .set( "## div.pinboardCell.col", { 
                    "margin-bottom": size 
                    ,"margin-right": size 
                })
                .write();
            } else {
                style.remove();
            }
        }
    });
    
})
