define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/dom-attr"
    ,"works/lang/strings"
    ,"./ContentPane"
    ,"dijit/_TemplatedMixin"
    ,"dijit/_Contained"
    ,"works/form/_WidgetMixin"
    ,"./_FixedSizeMixin"
    ,"./_ParentQualifyMixin"
    ,"./_ContentWidget"
    ,"dojo/text!./resources/LabelPane.html"

    ,"works/style!./resources/LabelPane.css"
], function( req, declare, lang, domStyle, domGeometry, domClass, domAttr, strings, ContentPane, _TemplatedMixin, _Contained, _WidgetMixin, _FixedSizeMixin, _ParentQualifyMixin, _ContentWidget, template ){
    
    return declare( declare.className( req ), [ ContentPane, _TemplatedMixin, _FixedSizeMixin, _ParentQualifyMixin ], {

    	baseClass: "labelpane"
        ,templateString: template
        ,isLabelPane: true
        
    	,position: "top"
    	,align: "center"
        ,title: ""
        
    	,postMixInProperties: function() {
    		this.inherited( arguments );
            if( this.position == "side" ) {
                this.position = "left";
            }
            this.title = this.title.replace( new RegExp( " ", "g" ), "&nbsp;" );
    	}
	
    	,startup: function() {
            this.inherited( arguments );
            this.startupContentManagement();
            domAttr.set( this.domNode, "tabindex", "-1" );
            setTimeout( lang.hitch( this, this.resize ), 0 );
        }
        
        ,resizeDomNode: function() {
			var divBox = domGeometry.getContentBox( this.domNode );
            var titleBox = domGeometry.getMarginBox( this.titleNode );
            var containerBox = domGeometry.getMarginBox( this.containerNode );
            
            var box = { w: divBox.w, h: divBox.h };
            if( !strings.hasText( this.domNode.style.width ) ) {
                if( this.position == "top" || this.position == "bottom" ) {
                    box.w = Math.max( titleBox.w, containerBox.w );
                } else {
                    box.w = containerBox.w + titleBox.h;
                }
            }

            if( !strings.hasText( this.domNode.style.height ) ) {
                if( this.position == "top" || this.position == "bottom" ) {
                    box.h = containerBox.h + titleBox.h
                } else {
                    box.h = Math.max( containerBox.h, titleBox.w );
                }
            }
                
            domGeometry.setContentSize( this.domNode, box );
        }
        
        ,resizeContent: function() {
			var divBox = domGeometry.getContentBox( this.domNode );
			var containerBorder = domGeometry.getBorderExtents( this.containerNode );
            var titleBox = domGeometry.getMarginBox( this.titleNode );
            var titleBorder = domGeometry.getBorderExtents( this.titleNode );
            //console.log( this.id, "D", divBox );
            //console.log( this.id, "B", containerBorder );
            
            var box = {};
            if( this.position == "top" || this.position == "bottom" ) {
                box.w = divBox.w;
                box.h = divBox.h - titleBox.h; 
                var titleWidth = box.w;
            } else {
                box.w = divBox.w - titleBox.h;
                box.h = divBox.h;
                var titleWidth = box.h; 
            }
            
            box.t = 0;
            box.l = 0;
            if( this.position == "top" ) {
                box.t = titleBox.h;
            } else if( this.position == "left" ) {
                box.l = titleBox.h;
            }
            
            // console.log( this.id, "MB", box );
            domGeometry.setMarginBox( this.containerNode, box );
            domGeometry.setMarginBox( this.titleNode, { w: titleWidth } );
            

        }
        
    	,resize: function( size ) {
            if( size ) {
                domGeometry.setMarginBox( this.domNode, this.processFixedSize( size ) );

                this.resizeContent();
            } else {
                this.resizeDomNode();
            }

			this._contentBox = domGeometry.getContentBox( this.containerNode );
            this._layoutChildren();
    	}
	
        ,cellResizing: function( parent ) {
            if( this.elasticWidth ) {
                this.domNode.style.width = "1px";
            }
            if( this.elasticHeight ) {
                this.domNode.style.height = "1px";
            }
            return this.domNode;
        }

        ,cellResize: function( size ) {
            this.resize( this.processElasticSize( size ) );
        }

        ,_setTitleAttr: function( value ) {
            value = value || "";
            this.titleContentNode.innerHTML = ("" + value).replace( new RegExp( " ", "g" ), "&nbsp;" )
            this.title = value;
        }
        
        ,_setAlignAttr: function( value ) {
            value = value || "center";
            if( ! this.align != value ) {
                this.align = value;
                domClass.remove( this.titleNode, "left center right" );
                domClass.add( this.titleNode, value );
            }
        }

        ,_setPositionAttr: function( value ) {
            value = value || "top";
            if( this.position != value ) {
                this.position = value;
                domClass.remove( this.domNode, "top bottom left right" );
                domClass.add( this.domNode, value );
                this.resize();
            }
        }

    	,_px: function( size ) {
    		return parseInt( size.replace( "px", "" ) )
    	}
	
    });

})
