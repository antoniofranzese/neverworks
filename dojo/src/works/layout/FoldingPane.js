define([
    "require"
    ,"works/lang/declare"
    ,"works/lang/strings"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"./ExpandoPane"
    ,"dojo/text!./resources/FoldingPane-top.html"

], function( req, declare, strings, domClass, domStyle, ExpandoPane, topTemplate ){
    
    return declare( declare.className( req ), [ ExpandoPane ], {
        closedSize: 0
    	
        ,_getClosedSize: function() {
            return this._getTitleHeight() + this.closedSize;
        }
        
    	,_hideWrapper: function(){
    		domClass.add(this.domNode, "dojoxExpandoClosed");
    	}
        
    
        ,buildRendering: function() {
            var region = strings.safe( this.region ).toLowerCase().trim();
			if( region == "top" ) {
                this.templateString = topTemplate;
			}
    		this.inherited( arguments );
    	}
        
        ,_setClosedSizeAttr: function( value ) {
            this._set( "closedSize", Math.round( typeof value == "number" ? value : domStyle.toPixelValue( this.domNode, value ) ) );
        }
        
    });
    
});