define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/fx"
    ,"dojo/fx/easing"
    ,"dojo/on"
    ,"dojo/dom-geometry"
    ,"dijit/_Templated"
    ,"./ContentPane"
    ,"./MomentumScroller"
    ,"dojo/text!./resources/MobileScrollPane.html"
    
    ,"works/style!./resources/MobileScrollPane.css"
    
], function( req, declare, lang, fx, easing, on, domGeometry, Templated, ContentPane, MomentumScroller, template ){
    
    return declare( declare.className( req ), [ ContentPane, Templated ], {
        templateString: template
        
        ,offset: 0
        
        ,postCreate: function() {
            this.inherited( arguments );
            this.scroller = new MomentumScroller({
                panel: this
                ,onScroll: function( value ) {
                    var offset = this.panel.get( "offset" );
                    var slow = Math.abs( value ) < 70;
                    
                    //TODO: se offset e fuori limite, la velocità viene abbattuta fortemente, quando va al di sotto di una certa soglia, si chiude
                    if( offset > 150 || ( offset > 0 && slow ) ) {
                        return false;
                    } else {
                        var max = this.panel.get( "maxOffset" );
                        if( offset < max - 150 || ( offset < max && slow ) ) {
                            return false;
                        } else {
                            this.panel.adjustOffset( value * -1 );
                            return true;
                        }
                    }
                }
                ,onEnd: function( value ) {
                    this.panel.fixOffset();
                }
            })
        }
        
        ,onTouchStart: function( evt ) {
            evt.stopPropagation();
            evt.preventDefault();
            if( this.offsetAnimation ) {
                this.offsetAnimation.stop();
            }
            this.lastX = evt.clientX;
            this.scroller.start( this.lastX );
        }
        
        ,onTouchMove: function( evt ) {
            evt.stopPropagation();
            evt.preventDefault();
            this.adjustOffset( evt.clientX - this.lastX );
            this.lastX = evt.clientX;
            this.scroller.track( this.lastX );
        }
        
        ,onTouchEnd: function( evt ) {
            evt.stopPropagation();
            evt.preventDefault();
            if( this.offsetAnimation ) {
                this.offsetAnimation.stop();
            }
            if( this.fixOffset() ) {
                this.scroller.finish();
            }
        }

        ,fixOffset: function() {
            //console.log( "fix", this.get( "offset" ), this.get( "maxOffset" ) );
            var offset = this.get( "offset" );
            if( offset > 0 ) {
                //console.log( "fix min" );
                this.animateOffset( 0 );
                return false;
            } else {
                var max = this.get( "maxOffset" );
                if( offset < max ) {
                    //console.log( "fix max" );
                    this.animateOffset( max );
                    return false;
                } else {
                    return true;
                }
            }
        }

        ,adjustOffset: function( delta ) {
            var offset = this.get( "offset" );
            var brake = 1;
            if( offset > 0 ) {
                brake = ( 1000 - offset ) / 3000;
            } else {
                var max = this.get( "maxOffset" );
                if( offset < max ) {
                    brake = ( 1000 - Math.abs( offset - max ) ) / 3000;
                }
            }
            this.set( "offset", offset + delta * brake );
        }
        
        ,_setOffsetAttr: function( value ) {
            this._set( "offset", Math.floor( value ) );
            this.containerNode.style.left = this.offset + "px";
        }
        
        ,_getMaxOffsetAttr: function() {
            var excess = domGeometry.position( this.containerNode ).w - domGeometry.position( this.domNode ).w;
            return excess > 0 ? excess * -1 : 0;
        }
        
        ,animateOffset: function( destination ) {
            this.offsetAnimation = fx.animateProperty({
                node: this.containerNode
                ,properties: { left: destination }
                ,duration: 750
                ,easing: easing.expoOut
                ,onEnd: lang.hitch( this, function(){
                    this._set( "offset", domGeometry.position( this.containerNode ).x );
                })
                ,onStop: function() {
                    this.onEnd();
                }
            }).play();
        }
        
    });
    
});