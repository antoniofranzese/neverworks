define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/dom-attr"
    ,"dojo/dom-construct"
    ,"works/lang/Model"

], function( req, declare, lang, on, domStyle, domGeometry, domClass, domAttr, domConstruct, Model ){
    
    return declare( declare.className( req ), [ Model ], {

        constructor: function( node ) {
            this.nodes = {
                top: domConstruct.create( "div", { 
                    "class": "worksScreen" 
                    ,style: {
                        
                    }
                }, document.body )
                
                ,left: domConstruct.create( "div", { 
                    "class": "worksScreen" 
                    ,style: {
                        
                    }
                }, document.body )
                
                ,right: domConstruct.create( "div", { 
                    "class": "worksScreen" 
                    ,style: {
                        
                    }
                }, document.body )
                
                ,bottom: domConstruct.create( "div", { 
                    "class": "worksScreen" 
                    ,style: {
                        
                    }
                }, document.body )
            };
            
            var onPointerDown = lang.hitch( this, this.onPointerDown );
            
            this.own(
                on( this.nodes.top, "mousedown", onPointerDown )
                ,on( this.nodes.left, "mousedown", onPointerDown )
                ,on( this.nodes.right, "mousedown", onPointerDown )
                ,on( this.nodes.bottom, "mousedown", onPointerDown )
            );
        }
        
        ,show: function( node ) {
            if( node ) {
                var body = domGeometry.position( document.body );
                var pos = domGeometry.position( node );
                
                var margin = {
                    top: pos.y
                    ,bottom: body.h - pos.h - pos.y
                    ,left: pos.x
                    ,right: body.w - pos.w - pos.x
                };
                var side = body.h - margin.top - margin.bottom;
                
                domStyle.set( this.nodes.top, {
                    display: "block"
                    ,top: "0px"
                    ,left: "0px"
                    ,height: margin.top + "px"
                    ,width: "100%"
                });

                domStyle.set( this.nodes.bottom, {
                    display: "block"
                    ,bottom: "0px"
                    ,left: "0px"
                    ,height: margin.bottom + "px"
                    ,width: "100%"
                });

                domStyle.set( this.nodes.left, {
                    display: "block"
                    ,top: margin.top + "px"
                    ,left: "0px"
                    ,height: side + "px"
                    ,width: margin.left + "px"
                });

                domStyle.set( this.nodes.right, {
                    display: "block"
                    ,top: margin.top + "px"
                    ,right: "0px"
                    ,width: margin.right + "px"
                    ,height: side + "px"
                });

            }
        }
        
        ,hide: function( node ) {
            domStyle.set( this.nodes.top, { display: "none" } );
            domStyle.set( this.nodes.bottom, { display: "none" } );
            domStyle.set( this.nodes.left, { display: "none" } );
            domStyle.set( this.nodes.right, { display: "none" } );
        }
        
        ,onPointerDown: function() {
            this.emit( "click", { bubbles: false } );
        }
    });

})
