define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"works/lang/strings"
    ,"dijit/_WidgetBase"
    ,"works/form/_FormWidgetMixin"
    ,"works/form/_WidgetMixin"
    
], function( req, declare, lang, on, has, domStyle, domAttr, domClass, domConstruct, strings, _WidgetBase, _FormWidgetMixin, _WidgetMixin ){
    
    return declare( declare.className( req ), [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {

        visible: true
        ,width: "0px"
        ,height: "0px"
	
        ,getWidgetState: function() {
            return undefined;
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_setWidthAttr: function( value ) {
            this._setDimension( "width", value );
        }

        ,_setHeightAttr: function( value ) {
            this._setDimension( "height", value );
        }

        ,_setDimension: function( dimension, value ) {
            if( typeof value == "string" ) {
                if( value.endsWith( "%" ) && this.domNode.parentNode && this.domNode.parentNode.nodeName == "TD" ) {
                    domStyle.set( this.domNode.parentNode, dimension, value );
                    domStyle.set( this.domNode.parentNode, "min-" + dimension, value );
                    domStyle.set( this.domNode.parentNode, "max-" + dimension, value );   
                    domStyle.set( this.domNode, dimension, "100%" );
                } else {
                    domStyle.set( this.domNode, dimension, value );
                }
            } else if( value ){
                domStyle.set( this.domNode, dimension, value + "px" );
            }
            this._set( dimension, value );
            
        }

        ,_setBackgroundAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

    });

});
