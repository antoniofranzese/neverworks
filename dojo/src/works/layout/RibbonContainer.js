define([
    "require"
    ,"works/lang/declare"
    ,"dojo/query"
    ,"./TabContainer"
    
], function( req, declare, query, TabContainer ){
    
    return declare( declare.className( req ), [ TabContainer ], {
        
        buildRendering: function() {
            this.inherited( arguments );
            query( ".dijitTabContainerTop-tabs", this.domNode ).addClass( "ribbon-tabs" );
        }
        
    });
    
});
