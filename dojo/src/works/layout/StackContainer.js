define([
		"require"
        ,"works/lang/declare"
        ,"dijit/layout/StackContainer"
		,"./_ContainerMixin"
        
], function( req, declare, StackContainer, _ContainerMixin ){
    var StackContainer = declare( declare.className( req ), [ StackContainer, _ContainerMixin ], {
 
  	});
    
    StackContainer.markupFactory = _ContainerMixin.markupFactory;
    
    return StackContainer;
});	
