define([
		"require"
        ,"works/lang/declare"
        ,"dojo/_base/lang"
        ,"dojo/_base/array"
        ,"dojo/aspect"
        ,"dojo/dom-style"
        ,"dojo/dom-class"
        ,"dojo/dom-geometry"
        ,"dijit/layout/TabContainer"
		,"./_ContainerMixin"
        
], function( req, declare, lang, array, aspect, domStyle, domClass, domGeometry, TabContainer, _ContainerMixin ){
    
    var TabContainer = declare( declare.className( req ), [ TabContainer, _ContainerMixin ], {
        loader: true
        ,stripVisible: true
        
        ,_setBorderTopAttr: function( value ) {
            if( this.tabPosition != "top" ) {
                domStyle.set( this.containerNode, "borderTop", value );
            }
            //TODO: impostazione del bordo sulle linguette
        }
        
        ,_setBorderBottomAttr: function( value ) {
            if( this.tabPosition == "top" ) {
                domStyle.set( this.containerNode, "borderBottom", value );
            }
            //TODO: impostazione del bordo sulle linguette
        }

        ,_setBorderLeftAttr: function( value ) {
            domStyle.set( this.containerNode, "borderLeft", value );
        }

        ,_setBorderRightAttr: function( value ) {
            domStyle.set( this.containerNode, "borderRight", value );
        }
        
        ,_setStripFontAttr: function( value ) {
            // localStyle( this, "strip-font" )
            //     .set( "## > table > tbody > tr > td", value )
            // .write();
        }
        
        ,_setStripPaddingAttr: function( value ) {
            domStyle.set( this.tablist.domNode, "padding", value );
        }

        ,_setStripBackgroundAttr: function( value ) {
            domStyle.set( this.tablist.domNode, "backgroundColor", value );
        }

        ,_setStripColorAttr: function( value ) {
            domStyle.set( this.tablist.domNode, "color", value );
        }

        ,_setStripVisibleAttr: function( value ) {
            domClass[ value ? "remove" : "add" ]( this.domNode, "hiddenStrip" );
            this.resize();
        }

        ,_setContentFontAttr: function( value ) {
            // localStyle( this, "content-font" )
            //     .set( "## > table > tbody > tr > td", value )
            // .write();
        }
        
        ,_setContentPaddingAttr: function( value ) {
            domStyle.set( this.containerNode, "padding", value );
        }

        ,_setContentBackgroundAttr: function( value ) {
            domStyle.set( this.containerNode, "backgroundColor", value );
        }

        ,_setContentColorAttr: function( value ) {
            domStyle.set( this.containerNode, "color", value );
        }


        ,updateChildVisibility: function( child ) {
            var button = this.tablist.pane2button( child.id );
            if( button ) {
                domStyle.set( button.domNode, "display", child.get( "visible" ) != false ? "" : "none" );
            }
            this.inherited( arguments );
            if( lang.isFunction( this.tablist.resize ) ) {
                this.tablist.resize({ w: domGeometry.getMarginBox( this.domNode ).w });
            }
        }

        ,subscribeChild: function( child ) {
            if( lang.isFunction( child.own ) ) {
                var button = this.tablist.pane2button( child.id );
                if( button ) {
                    child.own(
                        button.watch( "checked", lang.hitch( this, lang.hitch( this, lang.partial( this.updateButton, button ) ) ) ) 
                    );
                }
            }
            this.inherited( arguments );
        }
        
        ,updateButton: function( button, a, b, c ) {
            if( button ) {
                domClass[ button.get( "checked" ) == true ? "remove" : "add" ]( button.domNode, "dijitTabUnchecked" );
            }
        }
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "clicked", { rename: "click" } );
        }
        
        ,leaveForm: function( form ) {
            this.retireEvent( "clicked" );
            this.inherited( arguments );
        }

        ,startup: function() {
            this.inherited( arguments );

            array.forEach( this.getChildren(), function( child ){
                this.updateButton( this.tablist.pane2button( child.id ) );
            }, this );

            this.own(
                aspect.before( this.tablist, "onButtonClick", lang.hitch( this, function( page ){
                    if( page === this.selectedChildWidget || ! this.eventActive( "selected" ) ) {
                        this.emitEvent( "clicked", { state: {
                            "new": array.indexOf( this.getChildren(), this.selectedChildWidget )
                        }});
                    }
                }))
            );
        }
        
 	});
    
    TabContainer.markupFactory = _ContainerMixin.markupFactory;
    
    return TabContainer;
});	
