define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/topic"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/query"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/dom-attr"
    ,"dijit/layout/BorderContainer"
    ,"works/layout/ContentPane"
    ,"works/layout/_StaticLayoutWidget"
    ,"works/layout/_ChildrenVisibilityMixin"
    ,"works/layout/_ParentQualifyMixin"
    ,"works/form/_WidgetMixin"
    ,"works/local-style"
    
    ,"works/style!./resources/GridLayout.less"
    
], function( _require, declare, lang, array, topic, on, has, query, domStyle, domGeometry, domClass, domAttr, BorderContainer, ContentPane, _StaticLayoutWidget, _ChildrenVisibilityMixin, _ParentQualifyMixin, _WidgetMixin, localStyle ){
    
    return declare( [ ContentPane, _ChildrenVisibilityMixin, _StaticLayoutWidget, _ParentQualifyMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        ,collapsible: false
        ,baseClass: "worksGridLayoutPane"
        
        ,startup: function() {
            this.inherited( arguments );
            this.updateReferences();
            this.contentReady().then( lang.hitch( this, this.processTableNode ) );

            on( this, "loaded", lang.hitch( this, function(){
                this.updateReferences();
                this.contentReady().then( lang.hitch( this, function(){ 
                    this.processTableNode();
                    this.set( "collapsible", this.get( "collapsible" ) );
                    this.set( "halign", this.get( "halign" ) );
                }));
            }));
            
            if( has( "mobile" ) ) {
                on( this.domNode, "touchmove", lang.hitch( this, function( evt ) {
                    if( this.scrollable != null && this.scrollable !== undefined ) {
                        evt.stopPropagation();
                    }
                }));
            }
        }
        
        ,destroy: function() {
            this.unregisterChildrenVisibilityChanges();
            this.inherited( arguments );
        }
        
        ,updateReferences: function() {
            this.boxNode = query( ">div.worksGridLayoutBox", this.domNode )[ 0 ];
            this.tableNode = query( ">table.worksGridLayout", this.domNode )[ 0 ];
        }

        ,processTableNode: function() {

            this.registerChildrenVisibilityChanges( lang.hitch( this, function( widget, value ){
                var widgetNode = widget.domNode;
                if( widgetNode && widgetNode.parentNode && widgetNode.parentNode.tagName == "TD" ) {
                    if( value == true ) {
                        domClass.remove( widgetNode.parentNode, "hidden" );
                    } else {
                        domClass.add( widgetNode.parentNode, "hidden" );
                    }
                    
                    this.updateRow( widgetNode.parentNode.parentNode );
                    topic.publish( "works/widget/change" );
                }
            }));
            
            query( ">table>tbody>tr", this.domNode ).forEach( lang.hitch( this, function( tr ){
                this.updateRow( tr );
            }));
            
        }
        
        ,updateRow: function( tr ) {
            domClass[ query( ">td:not(.hidden)", tr ).length > 0 ? "remove" : "add" ]( tr, "hidden" );
        }
        
        ,parentIsLayoutContainer: function() {
            var parent = this.getParent();
            return parent && ( 
                parent.isLayoutContainer == true 
                && parent.isFormManager != true 

                //TODO: correggere il resize del LabelPane
                && parent.isLabelPane != true 
                && parent.isRealPane != true 
            )
        }
        
        ,resize: function( size ) {
            var resizables = [];
            array.forEach( this.getChildren(), function( child ){
                if( lang.isFunction( child.cellResizing ) ) {
                    var node = child.cellResizing( this );
                    if( node !== undefined && node != null ) {
                        resizables.push({ 
                            widget: child
                            ,node: node
                        });
                    }
                }
            }, this );

            if( this.parentIsLayoutContainer() || this.scrollable ) {
                this.inherited( arguments );
            } else {
                this.domNode.style.width = null;
                this.domNode.style.height = null;
            }

            array.forEach( resizables, function( resizable ){
                resizable.size = domGeometry.getContentBox( resizable.node.parentNode );
            }, this );
            
            array.forEach( resizables, function( resizable ){
                if( lang.isFunction( resizable.widget.cellResize ) ) {
                    resizable.widget.cellResize( resizable.size );
                }
            }, this );
        }
        
        ,setWidgetState: function( state ) {
            for( var name in state ) {
                this.set( name, state[ name ] );
            }
        }

        ,_setCollapsibleAttr: function( value ) {
            domClass[ value == true ? "add" : "remove" ]( this.domNode.firstChild, "collapsible" );
            this._set( "collapsible", value );
        }
        
        ,_setHalignAttr: function( value ) {
            if( this.domNode ) {
                if( value ) {
                    domAttr.set( this.domNode.firstChild, "align", value );
                } else {
                    domAttr.remove( this.domNode.firstChild, "align" );
                }
            }
            this._set( "halign", value );
        }
        
        ,_setScrollableAttr: function( value ) {
            domClass.removeAll( this.domNode, /scroll-.*/ );
            if( value ) {
                domClass.add( this.domNode, "scroll-" + value )
            }
            this._set( "scrollable", value );
        }
        
        ,_setOuterBorderAttr: function( value ) {
            domStyle.set( this.domNode.firstChild, value );
        }

        ,_setInnerBorderAttr: function( value ) {
            // localStyle( this, "inner-border" )
            //     .set( "## > table > tbody > tr > td", value )
            //     .set( "## > table > tbody > tr:first-child > td", value ? "border-top: none" : null )
            //     .set( "## > table > tbody > tr > td:first-child", value ? "border-left: none" : null )
            //     .write();
            
            var borderTop = value[ "border-top" ] || "none";
            var borderLeft = value[ "border-left" ] || "none";
            query( ">table>tbody>tr", this.domNode ).forEach( function( tr ) {
                query( ">td", tr ).forEach( function( td ) {
                    if( ! domClass.contains( td, "bordered" ) ) {
                        if( tr.rowIndex > 0 ) {
                            domStyle.set( td, "border-top", borderTop );
                        }
                        if( td.cellIndex > 0 ) {
                            domStyle.set( td, "border-left", borderLeft );
                        }
                    }
                });
            });
        }
        
        ,_setBackgroundAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }
        
        ,_setColorAttr: function( value ) {
            domStyle.set( this.domNode, "color", value );
        }
        
        ,_setRoundingAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setShadowAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

    });
    
})
