define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/aspect"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"dijit/TitlePane"
    ,"works/form/_WidgetMixin"
    ,"./BorderContainer"
    ,"./_FixedSizeMixin"
    ,"./_ParentQualifyMixin"
    ,"./_ContentWidget"
    
    ,"works/style!./resources/TitlePane.css"

], function( req, declare, lang, aspect, domClass, domGeometry, domStyle, TitlePane, _WidgetMixin, BorderContainer, _FixedSizeMixin, _ParentQualifyMixin, _ContentWidget ){
    
    var TitlePane = declare( declare.className( req ), [ TitlePane, _WidgetMixin, _ContentWidget, _FixedSizeMixin, _ParentQualifyMixin ], {
        startExpanded: true
        ,doLayout: true
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "toggle" );
            form.ready().then( lang.hitch( this, function(){
                this._toggled = false;
            }));
        }
        
        ,leaveForm: function() {
            this.retireEvent( "toggle" );
            this.inherited( arguments );
        }
        
        ,startup: function() {
            this.inherited( arguments );
            this.startupContentManagement();
            if( this.startExpanded == false ) {
                this.toggle();
            }
            
            if( this.getParent() && this.getParent().isInstanceOf( BorderContainer ) ) {
                this.set( "toggleable", false );
            }
            
            this.own(
                aspect.before( this, "onShow", lang.hitch( this, function(){
                    if( this.savedHeight ) {
                        domStyle.set( this.domNode, "height", this.savedHeight );
                        delete this.savedHeight;
                    }
                    this._toggled = true;
                }))
                ,aspect.after( this, "onHide", lang.hitch( this, function(){
                    this.savedHeight = this.domNode.style.height;
                    this.domNode.style.height = null;
                    this._toggled = true;
                }))
            );
            
        }
        
        ,resize: function( size ) {
            var processed = this.processFixedSize( size );
            // console.log( this.name, "w", this.fixedWidth, size ? size.w : "-", processed.w, " / h", this.fixedHeight, size ? size.h : "-", processed.h );
            
            if( processed.w || processed.h ) {
                var borders = domGeometry.getPadBorderExtents( this.domNode );
                var title = domGeometry.getMarginBox( this.titleBarNode );
                if( processed.w ) {
                    domGeometry.setMarginBox( this.containerNode, { w: processed.w - borders.w } );
                }
                if( processed.h ) {
                    domGeometry.setMarginBox( this.containerNode, { h: processed.h - borders.h - title.h - 1 } );
                }
            }

            this.inherited( arguments );
        }
        
        ,cellResizing: function( parent ) {
            if( this.elasticWidth ) {
                this.domNode.style.width = "1px";
            }
            if( this.elasticHeight ) {
                this.domNode.style.height = "1px";
            }
            return this.domNode;
        }

        ,cellResize: function( size ) {
            if( this.elasticWidth || this.elasticHeight ) {
                this.resize( this.processElasticSize( size ) );
            }
        }

        ,getWidgetState: function() {
            if( this._toggled ) {
                this._toggled = false;
                return {
                    opened: this.get( "open" )
                };
            } else {
                return undefined;
            }
        }
        
        ,setWidgetState: function( state ) {
            if( state[ "collapsible" ] !== undefined ) {
                this.set( "collapsible", state.collapsible );
            }
            this.inherited( arguments );
        }
        
        ,_setContentAttr: function( content ) {
            // console.log( "Panel content", this.id );
            this.unregisterDescendants();
            this.inherited( arguments );
        }

        ,_setCollapsibleAttr: function( value ) {
            if( this.getParent() && this.getParent().isInstanceOf( BorderContainer ) ) {
                this.set( "toggleable", false );
            } else {
                this.set( "toggleable", value );
            }
        }
        
    });
    
    TitlePane.markupFactory = _WidgetMixin.markupFactory;
    
    return TitlePane;
});