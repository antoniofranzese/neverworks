define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/window"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"dojo/dom-geometry"
    ,"dojo/dom-attr"
    
    ,"works/style!./resources/Loader.css"
], function( _require, declare, lang, win, domStyle, domConstruct, domGeometry, domAttr ){
    
    return declare( [], {
        constructor: function() {
            this.loaderNode = domConstruct.create( "div", { 
                "class": "worksLoaderOverlay"
                ,style: {
                    visibility: "hidden"
                }
            }, win.body() );
        }
        
        ,stick: function( target ) {
            if( this.stickTimeout === undefined ) {
                this.show( target );
            } else {
                this.place( target );
            }

            this.stickTimeout = setTimeout( lang.hitch( this, function(){
                this.stick( target );
            }), 200 );
        }
        
        ,show: function( target, size ) {
            this.place( target, size );
            var targetID = domAttr.get( target, "id" );
            if( targetID ) {
                domAttr.set( this.loaderNode, "data-works-target", targetID );
            } else {
                domAttr.remove( this.loaderNode, "data-works-target" );
            }
        }
        
        ,place: function( target, size ) {
            if( size === undefined ) {
                size = domGeometry.position( target );
            }
            
            if( size.z === undefined ) {
                size.z = this.computeZIndex( target );
            }
            
            domStyle.set( this.loaderNode, { 
                visibility: "visible" 
                ,top: size.y + "px"
                ,left: size.x + "px"
                ,width: size.w + "px"
                ,height: size.h + "px"
                ,zIndex: size.z
            } );
            
        }
                
        ,hide: function() {
            if( this.stickTimeout ) {
                clearTimeout( this.stickTimeout );
                delete this.stickTimeout;
            }
            domStyle.set( this.loaderNode, { visibility: "hidden" } );
        }
        
        ,destroy: function() {
            this.hide();
            domConstruct.destroy( this.loaderNode );
        }

        ,computeZIndex: function( node ) {
            try {
                if( node ) {
                    var zIndex = node.style.zIndex;
                    while( ( zIndex == "" || zIndex == null || zIndex == undefined ) && node.parentNode ) {
                        node = node.parentNode;
                        zIndex = node.style.zIndex;
                    }
                    var result = parseInt( zIndex ) + 1;
                    return isNaN( result ) ? 5000 : result;
                } else {
                    return 5000;
                }
            } catch( ex ) {
                return 5000;
            }
        }
    });
    
});
