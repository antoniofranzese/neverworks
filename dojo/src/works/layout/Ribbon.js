define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"dojox/layout/ScrollPane"
    ,"./ContentPane"
    
    ,"works/style!./resources/ribbon.less"
    
], function( req, declare, array, ScrollPane, ContentPane ){
    
    return declare( declare.className( req ), [ ContentPane ], {
        
        startup: function() {
            this.inherited( arguments );
            array.forEach( array.filter( this.getDescendants(), function( w ){ 
                return w.isInstanceOf( ScrollPane ) 
            }), function( pane ){
                setTimeout( function(){
                    pane.wrapper.scrollLeft = 0;
                }, 0 );
            });

            this.containerWidget = widget( query( ">.ribbonContainer", this.domNode )[ 0 ].id );
        }
        
        ,getWidgetState: function() {
            return this.containerWidget && this.containerWidget.getWidgetState();
        }
        
        ,setWidgetState: function( state ) {
            this.containerWidget && this.containerWidget.setWidgetState( state );
        }
    });
    
});