define([
    "require"
    ,"works/lang/declare"
    ,"works/lang/strings"
    ,"dojo/_base/array"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"dojo/dom-attr"
    ,"dijit/_TemplatedMixin"
    ,"./_FixedSizeMixin"
    ,"./_ParentQualifyMixin"
    ,"./_DisplacedNodeMixin"
    ,"./ContentPane"
    ,"dojo/text!./resources/PlainPane.html"
], function( req, declare, strings, array, domGeometry, domStyle, domClass, domConstruct, domAttr, _TemplatedMixin, _FixedSizeMixin, _ParentQualifyMixin, _DisplacedNodeMixin, ContentPane, template ){
    
    return declare( declare.className( req ), [ ContentPane, _TemplatedMixin, _FixedSizeMixin, _ParentQualifyMixin, _DisplacedNodeMixin ], {
        templateString: template
        ,baseClass: "worksPlainPane"
        ,doLayout: true
        ,isRealPane: true
        
        ,setContentSize: function() {
            // if( this.contentHeight !== undefined || this.contentWidth !== undefined ) {
            //     var contentBorder = domGeometry.getPadBorderExtents( this.containerNode );
            //     var domBox = domGeometry.getMarginBox( this.domNode );
            //
            //     if( this.contentHeight !== undefined && !this.contentHeight.endsWith( "%" ) ) {
            //         domStyle.set( this.containerNode, { height: ( domStyle.toPixelValue( this.containerNode, this.contentHeight ) - contentBorder.h ) + "px" });
            //     }
            //
            //     if( this.contentWidth !== undefined && !this.contentWidth.endsWith( "%" ) ) {
            //         domStyle.set( this.containerNode, { width: ( domStyle.toPixelValue( this.containerNode, this.contentWidth ) - contentBorder.w ) + "px" });
            //     }
            // }
            // var contentBorder = domGeometry.getPadBorderExtents( this.containerNode );
            // var domBox = domGeometry.getMarginBox( this.domNode );
            
            // var height;
            // if( this.contentHeight !== undefined && !this.contentHeight.endsWith( "%" ) ) {
            //     height = domStyle.toPixelValue( this.containerNode, this.contentHeight );
            // } else {
            //     height = domBox.h;
            // }
            //
            // var width;
            // if( this.contentWidth !== undefined && !this.contentWidth.endsWith( "%" ) ) {
            //     width = domStyle.toPixelValue( this.containerNode, this.contentWidth );
            // } else {
            //     width = domBox.w;
            // }
            // domGeometry.setMarginBox( this.containerNode, {
            //     h: domBox.h - contentBorder.h
            //     ,w: domBox.w - contentBorder.w
            // });
            
        }

        ,buildRendering: function() {
            this.inherited( arguments );
            if( this.displaced ) {
                domClass.add( this.containerNode, "worksDisplacedPane" );
                domAttr.set( this.containerNode, "data-works-parent", this.id );
                this.containerNode[ ".parentNode" ] = this.domNode;
                this.displacedNode = this.containerNode;
                this.displacedContainerNode = this.domNode;
                this.flavorNode = this.containerNode;
                this.styleNode = this.containerNode;
            }
        }

        ,startup: function() {
            this.inherited( arguments );
            if( this.displaced ) {
                domConstruct.place( this.containerNode, document.body, "last" );
                this.placeDisplacedNode();
            }
        }
        
        ,joinForm: function( form ) {
            this.form = form;
            this.inherited( arguments );
            if( this.displaced ) {
                this.registerDescendants();
            }
        }
        
        ,leaveForm: function() {
            if( this.displaced ) {
                domConstruct.place( this.containerNode, this.domNode );
                this.unregisterDescendants();
            }
            this.inherited( arguments );
            delete this.form;
        }
        
        ,resize: function( size ) {
            var processed = this.processFixedSize( size );
            //console.log( this.name, "w", this.fixedWidth, size ? size.w : "-", processed.w, " / h", this.fixedHeight, size ? size.h : "-", processed.h );
            
            domGeometry.setMarginBox( this.domNode, processed );
            
            //var contentBorder = domGeometry.getBorderExtents( this.containerNode );
            var domBox = domGeometry.getContentBox( this.domNode );
            var containerBox = {};
            if( strings.hasText( this.domNode.style.width ) ) {
                containerBox.w = domBox.w;
            }
            if( strings.hasText( this.domNode.style.height ) ) {
                containerBox.h = domBox.h;
            }
            if( this.displaced ) {
                if( this.contentWidth || this.contentHeight ) {
                    domGeometry.setMarginBox( this.containerNode, containerBox );
                }
                this.placeDisplacedNode();
            } else {
                domGeometry.setMarginBox( this.containerNode, containerBox );
            }

            this._contentBox = domGeometry.getContentBox( this.containerNode );
            this._layoutChildren();
        }
        
        ,cellResizing: function( parent ) {
           if( this.elasticHeight ) {
                domStyle.set( this.domNode, { height: "1px" });
            }
            
            if( this.elasticWidth ) {
                domStyle.set( this.domNode, { width: "1px" });
            }
            return this.domNode;
        }

        ,cellResize: function( size ) {
            if( this.fixedHeight === undefined ) {
                delete size.h;
            }
            this.resize( this.processElasticSize( size ) );
        }
        
        ,setWidgetState: function( state ) {
            array.forEach( [ "verticalScroll", "horizontalScroll" ], lang.hitch( this, function( scrollProperty ){
                if( state[ scrollProperty ] !== undefined ) {
                    var scrollValue = state[ scrollProperty ];
                    if( state[ "content" ] !== undefined ) {
                        on.once( this, "loaded", lang.hitch( this, function(){
                            this.contentReady().then( lang.hitch( this, function(){
                                this.set( scrollProperty, scrollValue );
                            }))
                        }));
                    } else {
                        form.ready().then( lang.hitch( this, function(){
                            setTimeout( lang.hitch( this, function(){
                                this.set( scrollProperty, scrollValue );
                            }), 0 );
                        }));
                    }
                }
            }))
            this.inherited( arguments );
        }
        
        ,_setVisibleAttr: function( value ) {
            this.inherited( arguments );
            this.placeDisplacedNode();
        }

        ,_setScrollableAttr: function( value ) {
            var node = this.styleNode || this.domNode;
            domClass.removeAll( node, /scroll-.*/ );
            if( value ) {
                domClass.add( node, "scroll-" + value )
            }
            this._set( "scrollable", value );
        }
        
        ,_setVerticalScrollAttr: function( value ) {
            var node = this.styleNode || this.domNode;
            if( typeof value == "string" ) {

                if( value.endsWith( "%" ) ) {
                    var percent = parseInt( value.substr( 0, value.length - 1 ) );
                    var scrollMax = node.scrollHeight;
                    if( scrollMax > domGeometry.getContentBox( node ).h ) {
                        this._setVerticalScrollAttr( Math.ceil( scrollMax * percent / 100 ) );
                    } else {
                        this._setVerticalScrollAttr( 0 );
                    }

                } else if( value.endsWith( "px" ) ) {
                    this._setVerticalScrollAttr( parseInt( value.substr( 0, value.length - 2 ) ) );
                
                }

            } else {
                node.scrollTop = value;
            }
            
        }

        ,_setHorizontalScrollAttr: function( value ) {
            var node = this.styleNode || this.domNode;
            if( typeof value == "string" ) {

                if( value.endsWith( "%" ) ) {
                    var percent = parseInt( value.substr( 0, value.length - 1 ) );
                    var scrollMax = node.scrollWidth;
                    if( scrollMax > domGeometry.getContentBox( node ).w ) {
                        this._setHorizontalScrollAttr( Math.ceil( scrollMax * percent / 100 ) );
                    } else {
                        this._setHorizontalScrollAttr( 0 );
                    }

                } else if( value.endsWith( "px" ) ) {
                    this._setHorizontalScrollAttr( parseInt( value.substr( 0, value.length - 2 ) ) );
                
                }

            } else {
                node.scrollLeft = value;
            }
            
        }
        
    });
    
});