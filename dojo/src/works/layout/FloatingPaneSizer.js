define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/window"
    ,"dojo/has"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/query"
    ,"dijit/popup"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"dojo/dom-construct"
    ,"dojo/dom-geometry"
    ,"dojo/Deferred"
    ,"dijit/TooltipDialog"
    ,"../form/_SupportingWidget"
], function( _require, declare, win, has, on, topic, query, popup, domStyle, domClass, domConstruct, domGeometry, Deferred, TooltipDialog, _SupportingWidget ){
    
    return declare( [ TooltipDialog, _SupportingWidget ], {
        border: 1
        ,width: 150
        ,height: 100
        ,rows: 4
        ,cols: 6
        
        ,postCreate: function() {
            this.inherited( arguments );
            domStyle.set( this.containerNode, { width: this.width + "px", height: this.height + "px" });
            domClass.add( this.domNode, "worksFloatingPaneSizer" );
         
            var cellWidth = Math.floor( ( this.width ) / this.cols );
            var cellHeight = Math.floor( ( this.height ) / this.rows );
            
            this.cells = [];
            for( var r = 0; r < this.rows; r++ ) {
                //var row = [];
                //this.cells.push( row );
                for( var c = 0; c < this.cols; c++ ) {
                    var fix = ( c + this.cols * r ) * this.border;
                    var cell = domConstruct.create( "div",{
                        style: {
                            left: ( c * cellWidth ) + "px"
                            ,top: ( ( ( c + r * ( this.cols - 1 ) ) * cellHeight * -1 ) - fix ) + "px"
                            ,width: ( cellWidth - this.border ) + "px"
                            ,height: ( cellHeight - this.border ) + "px"
                        }
                        ,"class": "cell"
                    }, this.containerNode );
                    this.cells.push({ node: cell, row: r, col: c });
                }
            }
            
            for( var i = 0; i < this.cells.length; i++ ) {
                this.own(
                    on( this.cells[ i ].node, "mousedown",  lang.hitch( this, lang.partial( this.onCellMouseDown, this.cells[ i ] ) ) )
                    ,on( this.cells[ i ].node, "mouseup",   lang.hitch( this, lang.partial( this.onCellMouseUp, this.cells[ i ] ) ) )
                    ,on( this.cells[ i ].node, has( "ff" ) < 4 || has( "chrome" ) ? "mouseout"  : "mouseleave", lang.hitch( this, lang.partial( this.onCellMouseLeave, this.cells[ i ] ) ) )
                    ,on( this.cells[ i ].node, has( "ff" ) < 4 || has( "chrome" ) ? "mouseover" : "mouseenter", lang.hitch( this, lang.partial( this.onCellMouseEnter, this.cells[ i ] ) ) )
                )
                
            }
            
            this.previewNode = this.previewNode || win.body();
            this.boxNode = query( "div.worksFloatingPaneSizerBox", this.previewNode )[ 0 ];
            if( this.boxNode == undefined ) {
                this.boxNode = domConstruct.create( "div", { "class": "worksFloatingPaneSizerBox" }, this.previewNode );
            }
            
            this.own(
                on( this.containerNode, "mouseleave",  lang.hitch( this, this.onContainerMouseLeave ) )
            );
            
        }
        
        ,onCellMouseDown: function( cell, evt ) {
            this.mouseUpHandler = on( win.body(), "mouseup", lang.hitch( this, this.onCellMouseUp ) );
            
            evt.preventDefault ? evt.preventDefault() : evt.returnValue = false;
            this.startingCell = cell;
            this.endingCell = cell;
            this.updateView();
        }

        ,onCellMouseEnter: function( cell ) {
            domClass.add( cell.node, "hover" );

            if( this.startingCell != undefined /* && this.startingCell !== cell */ ) {
                this.endingCell = cell;
                this.updateView();
            } else {
                this.updateView( cell );
            }
        }

        ,onCellMouseLeave: function( cell ) {
            domClass.remove( cell.node, "hover" );
        }

        ,onContainerMouseLeave: function( cell ) {
            if( this.startingCell == undefined ) {
                this.resetView();
            }
        }
            
        ,onCellMouseUp: function() {
            if( this.startingCell != undefined ) {
                this.emitSize();
            }
            
        }
        
        ,onKeyDown: function( evt ) {
            if( evt.keyCode == 27 ) {
                this.cancelSize();
            }
        }
        
        
        ,updateView: function( cell ) {
            var startingCell = cell || this.startingCell;
            var endingCell = cell || this.endingCell;

            var start = {
                row: Math.min( startingCell.row, endingCell.row )
                ,col: Math.min( startingCell.col, endingCell.col )
            }

            var end = {
                row: Math.max( startingCell.row, endingCell.row )
                ,col: Math.max( startingCell.col, endingCell.col )
            }

            if( cell == undefined ) {
                for( var i = 0; i < this.cells.length; i++ ) {
                    var cl = this.cells[ i ];
                    if( cl.row >= start.row && cl.row <= end.row && cl.col >= start.col && cl.col <= end.col ) {
                        domClass.add( cl.node, "highlight" );
                    } else {
                        domClass.remove( cl.node, "highlight" );
                    }
                }
            }
            
            var pos = domGeometry.getContentBox( this.previewNode );
            var widthUnit = Math.floor( pos.w / this.cols );
            var heightUnit = Math.floor( pos.h / this.rows );

            domStyle.set( this.boxNode, {
                visibility: "visible"
                // ,top: Math.round( start.row * heightUnit ) + "px"
                // ,left: Math.round( start.col * widthUnit ) + "px"
                // ,height: Math.round( ( end.row - start.row + 1 ) * heightUnit ) + "px"
                // ,width: Math.round( ( end.col - start.col + 1 ) * widthUnit ) + "px"
            });
            
            domGeometry.setMarginBox( this.boxNode, {
                t: Math.round( start.row * heightUnit )
                ,l: Math.round( start.col * widthUnit )
                ,h: Math.round( ( end.row - start.row + 1 ) * heightUnit )
                ,w: Math.round( ( end.col - start.col + 1 ) * widthUnit )
            });
            
        }
        
        ,resetView: function() {
            for( var i = 0; i < this.cells.length; i++ ) {
                domClass.remove( this.cells[ i ].node, "highlight" );
            }
            
            domStyle.set( this.boxNode, { visibility: "hidden" });
            
            delete this.startingCell;
            delete this.endingCell;
            
            if( this.mouseUpHandler ) {
                this.mouseUpHandler.remove();
                delete this.mouseUpHandler;
            }
            
            if( this.keyDownHandler ) {
                this.keyDownHandler.remove();
                delete this.keyDownHandler;
            }
        }

        ,emitSize: function() {
            this.sizeDeferred.resolve( domGeometry.getMarginBox( this.boxNode ) );
            this.resetView();
            this.hide();
        }
        
        ,cancelSize: function() {
            this.sizeDeferred.resolve( undefined );
            this.resetView();
            this.hide();
        }
        
        ,hide: function() {
            popup.hide( this );
            if( this.keyDownHandler !== undefined ) {
                this.keyDownHandler.remove();
                delete this.keyDownHandler;
            }
        }
        
        ,openAround: function( target ) {
            this.sizeDeferred = new Deferred();
            popup.open({
                popup: this,
                around: target
            });
            this.focus();
            this.keyDownHandler = on( win.body(), "keydown", lang.hitch( this, this.onKeyDown ) );
            return this.sizeDeferred;
        }
        
        ,startup: function() {
            this.inherited( arguments );
            var cancel = lang.hitch( this, this.cancelSize );
            this.own(
                topic.subscribe( "mouse/scroll", cancel )
                ,on( this, "blur", cancel )
            )
        }
    });
    
})