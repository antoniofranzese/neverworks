define([
    "require"
    ,"works/lang/declare"
    ,"dojo/has"
    ,"dojo/on"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"dojo/Evented"
], function( req, declare, has, on, domStyle, domConstruct, Evented ){
    
    return declare( declare.className( req ), [ Evented ], {
        
        baseClass: "worksOverlay"
        
        ,constructor: function( params ) {
            declare.safeMixin( this, params );
        }
        
        ,initDomNode: function( params ) {
            params = params || {};
            
            if( this.domNode === undefined ) {
                
                this.domNode = domConstruct.create( "div", { 
                    "class": this.baseClass
                    ,style: {
                        width: "100%"
                        ,height: "100%"
                        ,position: "absolute"
                        ,top: "0px"
                        ,left: "0px"
                        ,backgroundColor: "white"
                        ,opacity: "0.01"
                        ,display: "none"
                    }
                }, document.body );
                
                if( params.after ) {
                    domConstruct.place( this.domNode, params.after, "after" );
                }
                
                on( this.domNode, has( "mobile" ) ? "touchstart" : "click", lang.hitch( this, function( evt ){
                    var self = this;
                    setTimeout( function(){
                        self.emit( "touch", { bubbles: false } );
                    }, 0 );
                    evt.stopPropagation();
                }));
                
                if( has( "desktop" ) ) {
                    on( this.domNode, "contextmenu", lang.hitch( this, function( evt ){
                        var self = this;
                        setTimeout( function(){
                            self.emit( "touch", { bubbles: false } );
                        }, 0 );
                        evt.preventDefault();
                        evt.stopPropagation();
                    }));

                    on( this.domNode, "mouseover", lang.hitch( this, function( evt ){
                        evt.preventDefault();
                        this.emit( "over", { bubbles: false });
                    }));
                }
                
            }
            
        }

        ,show: function( params ) {
            this.initDomNode( params );
            
            if( params.zIndex ) {
                domStyle.set( this.domNode, "zIndex", params.zIndex );
            }

            if( ! this.isShown() ) {
                domStyle.set( this.domNode, "display", "block" );
                this.emit( "show", { bubbles: false });
            }
        }
        
        ,hide: function() {
            if( this.domNode && this.isShown() ) {
                domStyle.set( this.domNode, "display", "none" );
                this.emit( "hide", { bubbles: false });
            }
        }
        
        ,isShown: function() {
            return domStyle.get( this.domNode, "display" ) == "block";
        }
        
        ,destroy: function() {
            if( this.domNode ) {
                domConstruct.destroy( this.domNode );
            }
        }
    });
    
});