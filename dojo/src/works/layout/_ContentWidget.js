define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/aspect"
    ,"dojo/topic"
    ,"dojo/request"
    ,"dojo/promise/all"
    ,"dojo/Deferred"
    ,"dojo/dom-style"
    ,"dojo/dom-construct"
    ,"works/registry"
    ,"dijit/layout/ContentPane"
    ,"../form/_FormWidgetMixin"
    ,"../form/_FormManagerWidget"
    ,"../form/_WidgetLocator"
    ,"../_ContentReadyMixin"
    
    ,"works/style!./resources/ContentPane.less"
    
], function( _require, declare, array, lang, on, aspect, topic, request, all, Deferred, domStyle, domConstruct, registry, ContentPane, _FormWidgetMixin, _FormManagerWidget, _WidgetLocator, _ContentReadyMixin ){

    return declare( [ ContentPane, _ContentReadyMixin, _FormWidgetMixin, _WidgetLocator ], {

        joinForm: function( form ) {
            this._parentForm = form;
        }
        
        ,leaveForm: function( ){
            delete this._parentForm;
        }

        ,startupContentManagement: function() {
            this.registered = false;

            this.own( 
                aspect.after( this, "_onLoadHandler", lang.hitch( this, function() {
                    this.registerContent();
                }))
                ,on( this, "loading", lang.hitch( this, function(){
                    topic.publish( "works/widget/change" );
                    
                    if( this._parentForm ) {
                         this._parentForm.bubbleMessage( { topic: "widget/task/start", body: { widget: this, reason: "Panel load" } } );
                    }
                }))
                ,on( this, "loaded", lang.hitch( this, function(){
                    if( this._parentForm ) {
                         this._parentForm.bubbleMessage( { topic: "widget/task/end", body: { widget: this, reason: "Panel load" } } );
                    }
                }))
            );
            
        }
        
        ,registerContent: function() {
            var d = new Deferred()
            //console.log( "Panel loading", this.id );
            this.emit( "loading", { bubbles: false } );
            
            this.contentReady().then( lang.hitch( this, function(){
                //console.log( "Panel loaded", this.id );
                this.registerDescendants();

                if( this.formDeferred ) {
                    // console.log( "Resolving panel form wait" );
                    this.formDeferred.resolve();
                    delete this.formDeferred;
                }
                
                this.emit( "loaded", { bubbles: false } );
                d.resolve();
            }));
            
            return d;
        }
        
        ,unregisterContent: function() {
            this.unregisterDescendants();
        }
        
        ,registerDescendants: function() {
            //console.log( "Panel register", this.id );
            var form = this.getParentForm( this );
                        
            if( form != undefined ) {
                //console.log( this.id, "registro per", form );
                //console.log( this.id, "descendants", this.getDirectDescendants() );
                // Registra sulla form contenitrice gli eventuali discendenti diretti
                array.map( this.getDirectDescendants(), function( widget ){
                                
                    //Deregistra solo se i discendenti fanno parte dello stesso FormManager
                    if( this.getParentForm( widget ) === form ) {
                        //console.log( this.id, "registro", widget );
                        form.registerWidget( widget );
                    } else {
                        //console.log( this.id, "salto", widget, self.getParentForm( widget ) );
                    }
                }, this );
                this.registered = true;
            } else {
                //console.log( this.id, "non registro" );
                
            }
    
        }
        
        ,unregisterDescendants: function(){
            var form = this.getParentForm( this );
            // console.log( this.id, "unregistering, form", form );
            if( form != undefined ) {
                // Deregistra sulla form contenitrice gli eventuali discendenti diretti
                // console.log( this.id, "deregistro", this.getDirectDescendants() );
                array.map( this.getDirectDescendants(), function( widget ){
                    
                    // Deregistra solo se i discendenti fanno parte dello stesso FormManager
                    if( this.getParentForm( widget ) === form ) {
                        // console.log( this.id, "deregistro", widget );
                        form.unregisterWidget( widget );
                    } else {
                        // console.log( this.id, "desalto", widget, this.getParentForm( widget ) );
                    }                    
                    
                }, this );
                this.registered = false;
            } else {
                // console.log( this.id, "non deregistro" )
            }
        }
        
        ,getDirectDescendants: function() {
            if( this.getDescendants()[0] != undefined ) {
                var firstWidget = this.getDescendants()[0];
                // Se e' un FormManager basta gestire un solo widget
                if( firstWidget.isInstanceOf( _FormManagerWidget ) ) {
                    //console.log( this.id, "ho una form");
                    return [ firstWidget ];
                } else {
                    // Altrimenti bisogna gestire tutti i widget
                    //console.log( this.id, "ho altro", firstWidget );
                    return this.getDescendants();
                }   
            } else {
                //console.log( this.id, "nulla" );
                return [];
            }
            
        }
        
        ,getParentForm: function( widget ) {
            if( this._parentForm ) {
                return this._parentForm;
            } else {
                return registry.findParent( this, _FormManagerWidget );
            }
        }
    });
})
