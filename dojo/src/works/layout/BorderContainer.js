define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"works/lang/strings"
    ,"works/config"
    ,"dojo/parser"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-construct"
    ,"dojo/topic"
    ,"dojo/Deferred"
    ,"dijit/layout/BorderContainer"
    ,"../form/_WidgetMixin"
    ,"../form/_FormWidgetMixin"
    ,"../form/_WidgetLocator"
    ,"./_LayoutWidget"
    ,"./_FixedSizeMixin"
    ,"./_ParentQualifyMixin"
    ,"./_ViewportMixin"
    ,"../_ContentReadyMixin"
    ,"./Loader"
    
], function( req, declare, lang, array, strings, config, parser, on, has, domClass, domStyle, domGeometry, domConstruct, topic, Deferred
    , BorderContainer, _WidgetMixin, _FormWidgetMixin, _WidgetLocator, _LayoutWidget, _FixedSizeMixin, _ParentQualifyMixin, _ViewportMixin, _ContentReadyMixin, Loader ){
    
    var blankPanel = function( region ) {
        return '<div data-dojo-type="dijit/layout/ContentPane" data-dojo-props="region: \'' + region + '\', blankRegion: true">&nbsp;</div>';
    } 
    
    var DOUBLE_CLICK_DELAY = config( "doubleClickDelay", 250 );

    var Splitter = declare( [ BorderContainer._Splitter ], {
        
		buildRendering: function() {
			this.inherited(arguments);
            try {
                this.childSizeAttr = this.horizontal ? "height" : "width";
                this.childSizeValue = strings.hasText( this.child.domNode.style[ this.childSizeAttr ] ) ? this.child.domNode.style[ this.childSizeAttr ] : undefined;
            } catch( ex ) {
                delete this.childSizeAttr;
                delete this.childSizeValue;
            }
        }
        
		,_startDrag: function(e){
            this.container.spreadMessage( { topic: "widget/layout/drag/start" } );

            if( this.doubleClickTimeout ) {
                clearTimeout( this.doubleClickTimeout );
            }
            
            if( this.clicked ) {
                //console.log( "Double" );
                if( this.childSizeAttr !== undefined && this.childSizeValue !== undefined ) {
                    this.child.domNode.style[ this.childSizeAttr ] = this.childSizeValue;
                    this.container.resize();
                }
            } else {
                this.clicked = true;
                setTimeout( lang.hitch( this, function(){
                    delete this.clicked;
                }), DOUBLE_CLICK_DELAY );

                this.inherited( arguments );
            }
            
        }
        
        ,_stopDrag: function(e){
            this.inherited( arguments );
            this.container.spreadMessage( { topic: "widget/layout/drag/stop" } );
        }

        ,saveState: function() {
            return {
                attr: this.childSizeAttr
                ,size: this.child.domNode.style[ this.childSizeAttr ]
            }
        }
        
        ,restoreState: function( state ) {
            if( state.size && state.size.hasText() ) {
                this.child.domNode.style[ state.attr ] = state.size;
                this.container.resize();
            }
        }
        
    });
    
    var BorderContainer = declare( declare.className( req ), [ BorderContainer, _WidgetMixin, _FormWidgetMixin, _LayoutWidget, _WidgetLocator , _FixedSizeMixin, _ParentQualifyMixin, _ContentReadyMixin, _ViewportMixin ], {
        
        _splitterClass: Splitter
        ,loader: true
        
        ,joinForm: function( form ) {
            this.form = form;
            this.inherited( arguments );
        }
        
        ,leaveForm: function() {
            this.inherited( arguments );
            delete this.form;
        }
        
        ,startup: function() {
            this.inherited( arguments );
            array.forEach( this.getChildren(), this.subscribeChild, this );
        }
    	
        ,_setDesignAttr: function( value ) {
            this.design = value;
            this.layout();
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }

        ,_getWidthAttr: function() {
            if( this.domNode.style.width ) {
                return domStyle.toPixelValue( this.domNode, this.domNode.style.width );
            } else {
                return undefined;
            }
        }

        ,_getHeightAttr: function() {
            if( this.domNode.style.height ) {
                return domStyle.toPixelValue( this.domNode, this.domNode.style.height );
            } else {
                return undefined;
            }
        }

        ,_getOrderedChildren: function() {
            var children = this.inherited( arguments );
            return array.filter( children, function( child ) {
                return child.get( "visible" ) != false;
            });
        }
        
        ,getRegionChild: function( region ) {
            var children = this.getChildren();
            for( var i = 0; i < children.length; i++ ) {
                if( children[ i ].region == region ) {
                    return children[ i ];
                }
            }
            return undefined;
        }
        
        ,findRelativeChild: function( name ) {
            var child = this.getRegionChild( name );
            if( child.regionWrapper ) {
                return child.getDescendants()[ 0 ];
            } else {
                return child;
            }
        }
        
        ,subscribeChild: function( child ) {
            if( lang.isFunction( child.emit ) && lang.isFunction( child.own ) ) {
                child.own(
                    on( child, "redrawing", lang.hitch( this, lang.partial( this.onChildRedrawing, child ) ) )
                    ,on( child, "redrawn", lang.hitch( this, lang.partial( this.onChildRedrawn, child ) ) )
                    ,child.watch( "visible", lang.hitch( this, function( name, oldValue, newValue ){
                        this.updateChildLayout( child ); 
                        this.resize();
                    }))
                    ,child.watch( "region", lang.hitch( this, function( name, oldValue, newValue ){
                        if( child._splitterWidget ) {
                            child._splitterWidget.destroy();
                            delete child._splitterWidget;
                        }
                        this._setupChild( child );
                        this.resize();
                    }))
                );
            }
        }
        
        ,onChildRedrawing: function( child ) {
            this.showLoader( child );
        }
        
        ,onChildRedrawn: function( child ) {
            this.hideLoader();
        }
        
        ,showLoader: function( child ) {
            if( this.loader == true ) {
                if( this.loaderWidget === undefined ) {
                    this.loaderWidget = new Loader();
                }
                
                var node;
                if( child.loaderNode !== undefined ) {
                    node = child.loaderNode;
                } else if( child.containerNode !== undefined ) {
                    node = child.containerNode;
                } else {
                    node = child.domNode;
                }
                
                if( this.gutters ) {
                    var size = domGeometry.position( node );
                    var borders = domGeometry.getBorderExtents( node );

                    size.w = size.w - borders.w;
                    size.h = size.h - borders.h;
                    size.x = size.x + borders.l;
                    size.y = size.y + borders.t;
                    
                    this.loaderWidget.show( node, size );
                } else {
                    this.loaderWidget.show( node );
                }
            }
        }
        
        ,hideLoader: function() {
            if( this.loaderWidget !== undefined ) {
                this.loaderWidget.hide();
            }
        }
        
        ,cellResizing: function( parent ) {
            if( this.elasticHeight ) {
                domStyle.set( this.domNode, { height: "1px" });
            }
            
            if( this.elasticWidth ) {
                domStyle.set( this.domNode, { width: "1px" });
            }
            return this.domNode;
        }

        ,cellResize: function( size ) {
            this.resize( this.processElasticSize( size ) );
        }
        
        ,updateChildLayout: function( child ) {
            if( child._splitterWidget && child._splitterWidget.domNode ) {
                domStyle.set( child._splitterWidget.domNode, "display", child.get( "visible" ) ? "" : "none" );
            }
        }

        ,_setColorAttr: function( value ) {
            domStyle.set( this.domNode, "color", value );
        }
        
        ,_setBackgroundAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }
	 
        ,_setFontAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }

        ,_setBorderAttr: function( value ) {
            domStyle.set( this.domNode, value );
        }
        
        ,_setTopAttr: function( value ) {
            this.replaceRegion( "top", value );
        }

        ,_setBottomAttr: function( value ) {
            this.replaceRegion( "bottom", value );
        }
        
        ,_setLeftAttr: function( value ) {
            this.replaceRegion( "left", value );
        }
        
        ,_setRightAttr: function( value ) {
            this.replaceRegion( "right", value );
        }

        ,_setCenterAttr: function( value ) {
            this.replaceRegion( "center", [ value ? value : blankPanel( "center" ) ] );
        }

        ,set: function( name, value ) {
            if( name.startsWith( "child:" ) ) {
                this.replaceChild( name.substring( 6 ), value );
            } else {
                this.inherited( arguments );
            }
        }
        
        ,replaceRegion: function( name, value ) {
            array.forEach( array.filter( this.getChildren(), function( child ){
                return name == child.get( "region" );
            }), function( child ){
                this.performChildRemove( child );
            }, this );

            if( value ) {
                array.map( value, function( fragment ){ 
                    var node = domConstruct.toDom( "<div>" + fragment + "</div>" );
                    domConstruct.place( node, this.domNode, "last" );
                    return this.performChildInsert( node );
                }, this );
            }
        }
        
        ,replaceChild: function( name, fragment ) {
            //console.log( "Must replace", name );
            var child = array.filter( this.getChildren(), function( child ){ 
                return ( child.get( "regionWrapper" ) == true ? child._singleChild : child ).get( "name" ) == name;  
            })[ 0 ];
            if( child && child.domNode ) {
                var splitterState;
                if( child._splitterWidget && child._splitterWidget.saveState ) {
                    splitterState = child._splitterWidget.saveState();
                }
                var node = domConstruct.toDom( "<div>" + fragment + "</div>" );
                domConstruct.place( node, child.domNode, "after" );
                this.performChildRemove( child );
                this.performChildInsert( node ).then( function( widget ){
                    if( splitterState && widget._splitterWidget && widget._splitterWidget.restoreState ) {
                        widget._splitterWidget.restoreState( splitterState );
                    }
                });
            }
        }
        
        ,performChildInsert: function( node ) {
            var d = new Deferred();
            topic.publish( "works/widget/change" );

            this.startWidgetTask({ reason: "Child insert" });

            var widgets = parser.parse({ rootNode: node }).then( lang.hitch( this, function( nodes ){
                var widget = nodes[ 0 ];
                
                //console.log( "Adding", widget.id );
                domConstruct.place( node.firstChild, node, "after" );
                this.form.registerWidget( widget );

                if( lang.isFunction( widget.registerContent ) ) {
                    widget.registerContent().then( lang.hitch( this, function(){
                        domConstruct.destroy( node );
                        this._setupChild( widget );
                        this.updateChildLayout( widget );
                        this.subscribeChild( widget );
                        this.endWidgetTask({ reason: "Child insert" });
                        //this.hideLoader();
                        d.resolve( widget );
                    }));
                } else {
                    domConstruct.destroy( node );
                    this._setupChild( widget );
                    this.updateChildLayout( widget );
                    if( widget.blankRegion != true ) {
                        this.subscribeChild( widget );
                    }
                    this.endWidgetTask({ reason: "Child insert" });
                    //this.hideLoader();
                    d.resolve( widget );
                }
                
            }));
            return d;
        }
        
        ,performChildRemove: function( child ) {
            //console.log( "Destroying", child.id );
            topic.publish( "works/widget/change" );
			
		    this.form.unregisterWidget( child );
            if( lang.isFunction( child.unregisterContent ) ) {
                child.unregisterContent();
            }
            
            if( child._splitterWidget ) {
                child._splitterWidget.destroy();
            }
			
            child.destroyRecursive();
        }

    });
    
    //BorderContainer.markupFactory = _WidgetMixin.markupFactory;
    return BorderContainer;
})
