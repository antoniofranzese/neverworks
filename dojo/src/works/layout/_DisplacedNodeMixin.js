define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/dom-attr"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
], function( req, declare, lang, domAttr, domStyle, domGeometry ){
    
    return declare( declare.className( req ), [], {
        
        displaced: false
        
        ,getDisplacedNodeStyle: function( nodeType ) {
            return {
                position: "absolute"
                ,top: "-10000px"
                ,left: "0px"
            };
        }
        
        ,placeDisplacedNode: function( container ) {
            container = container || this.domNode;

            if( this.displaced && this.displacedNode ) {
            
                if( domStyle.isShown( container ) ) {
                    var pos = domGeometry.position( container );
                    domStyle.set( this.displacedNode, {
                        top: pos.y + "px"
                        ,left: pos.x + "px"
                    });
                    domAttr.set( this.displacedNode, "data-works-visible", "true" );
                } else {
                    domStyle.set( this.displacedNode, {
                        top: "-10000px"
                    });
                    domAttr.set( this.displacedNode, "data-works-visible", "false" );
                }
            }
        }

        ,evaluateDisplacedLayout: function() {
            if( this._dragInProgress == true  ) {
                domStyle.set( this.displacedNode, "pointer-events", "none" );
            } else {
                domStyle.set( this.displacedNode, "pointer-events", null );
            }
        }
        
        ,notifyMessage: function( message ) {
            if( this.displaced ) {
                if( message.topic == "widget/container/show" ) {
                    this.placeDisplacedNode( this.displacedContainerNode );
                
                } else if( message.topic == "widget/container/hide" ) {
                    setTimeout( lang.hitch( this, function(){
                        this.placeDisplacedNode( this.displacedContainerNode );
                    }), 0 );
                
                } else if( message.topic == "widget/layout/drag/start" ) {
                    this._dragInProgress = true;
                    this.evaluateDisplacedLayout( this.displacedContainerNode );
                
                } else if( message.topic == "widget/layout/drag/stop" ) {
                    this._dragInProgress = false;
                    this.evaluateDisplacedLayout( this.displacedContainerNode );

                }
            }
        }
        
    });
    
});