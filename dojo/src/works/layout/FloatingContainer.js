define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/parser"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-construct"
    ,"dojo/on"
    ,"dojo/topic"
    ,"dojo/Deferred"
    ,"dojo/promise/all"
    ,"./ContentPane"
    ,"./FloatingPane"
    ,"./FloatingPaneSizer"
    ,"../form/_WidgetMixin"

    ,"works/style!./resources/FloatingContainer.less"
    ,"works/style!works/layout/resources/ResizeHandle.css"
    
], function( _require, declare, lang, array, parser, domStyle, domAttr, domClass, domGeometry, domConstruct, on, topic, Deferred, all, ContentPane, FloatingPane, FloatingPaneSizer, _WidgetMixin ){
    
    var size = function( node ) {
        var mb = domGeometry.getMarginBox( node );
        return { x: mb.l, y: mb.t, w: mb.w - 2, h: mb.h - 2 };
    }
    
    var StairsPlacingStrategy = declare([],{

        constructor: function( container ) {
            this.container = container;
            this.x = 0;
            this.y = 0;
            this.restart = 100;
            this.minTitleHeight = lang.getObject( "worksConfig.layout.FloatingContainer.minumumTitleHeight" ) || 22;
        }
        
        ,placeChild: function( child ) {
            var titleHeight = domGeometry.position( child.focusNode ).h;
            if( titleHeight < this.minTitleHeight ) {
                titleHeight = this.minTitleHeight;
            }
            var childHeight = domGeometry.position( child.domNode ).h;
            var ownHeight = domGeometry.position( this.container.domNode ).h;
            // Calculate new origin
            this.y += titleHeight;
            this.x += titleHeight;
            if( this.y + childHeight > ownHeight ) {
                this.restart = this.restart < titleHeight ? titleHeight : Math.floor( titleHeight / 2 );
                this.y = this.restart;
                
            }

            return { x: this.x, y: this.y };
        }
        
    })
    
    return declare( [ ContentPane, _WidgetMixin ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )

        ,startupSelected: 0
        
        ,buildRendering: function() {
            this.inherited( arguments );
            domClass.add( this.domNode, "worksFloatingContainer" );
        }
        
        ,startup: function() {
            this.placingStrategy = new StairsPlacingStrategy( this );
            this.children = [];
            this.inherited( arguments );
            array.forEach( this.getChildren(), function( child ){
                child.set( "working", true );
                // if( child.get( "origin" ) === undefined ) {
                //     child.set( "origin", this.placeChild( child ) );
                // }
                this.subscribeChild( child );
            }, this );
            this.set( "selectedIndex", this.startupSelected );
        }

        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.form = form;
            this.announceEvent( "selecting" );
            this.announceEvent( "selected", { rename: "select" } );
            this.announceEvent( "closing" );
            this.announceEvent( "closed", { rename: "close" } );
            array.forEach( this.getChildren(), function( child ){
                child.joinForm( form );
            });
            
        }
        
        ,leaveForm: function( form ) {
            array.forEach( this.getChildren(), function( child ){
                child.leaveForm( form );
            });
            this.retireEvent( "selecting" );
            this.retireEvent( "selected" );
            this.retireEvent( "closing" );
            this.retireEvent( "closed" )
            this.form = null;
            this.inherited( arguments );
        }
        
        ,_checkIfSingleChild: function(){
            this._singleChild = null;
        }

        ,_layoutChildren: function() {
            var pos = domGeometry.position( this.domNode );

            if( this._maximizedChild ) {
                this._maximizedChild.resize( { t: 0, l: 0, w: pos.w, h: pos.h } );
            } else {
                array.forEach( this.getChildren(), function( child ){
                    child.resize();
                    if( child.get( "origin" ) === undefined ) {
                        child.set( "origin", this.placeChild( child ) );
                    }
                }, this );
            }

            // array.forEach( this.getChildren(), function( child ){
            //     var childPos = size( child.domNode );
            //     var resize = true;
            //     console.log( "pos", childPos.x + childPos.w,  pos.w );
            //     if( childPos.x + childPos.w + 2 > pos.w ){
            //         childPos.x = pos.w - childPos.w;
            //         resize = true;
            //     }
            //     if( childPos.y + childPos.h + 2 > pos.h ){
            //         childPos.y = pos.h - childPos.h - 2;
            //         resize = true;
            //     }
            //     console.log( "resize", child.id, resize );
            //     if( resize ) {
            //         //child.resize( { x: childPos.x - 8, y: childPos.y - 8, w: childPos.w, h: childPos.h } );
            //         //child.resize( childPos );
            //         child.resize( childPos );
            //     }
            // });
            
        }    

        ,addChild: function( child ) {
            if( child ){
                if( child.isInstanceOf == undefined || !( child.isInstanceOf( FloatingPane ) ) ) {
                    
                    var floatingNode = domConstruct.create( "div" );
                    domConstruct.place( floatingNode, this.domNode );
                    
                    var isContentWidget = lang.isFunction( widget.registerContent );
                    if( ! isContentWidget ) {
                        var contentNode = domConstruct.create( "div" );
                        domConstruct.place( contentNode, floatingNode );
                    
                        var contentPane = new ContentPane({}, contentNode );
                        contentPane.startup();
                    }

                    var title = ( child.get ? child.get( "title" ) : domAttr.get( child, "title" ) ) || "Untitled";
                    var floatingPane = new FloatingPane({
                        title: title,
                        dockable: child.get( "dockable" ) !== undefined ? child.get( "dockable" ) : false,
                        maxable: child.get( "maxable" ) !== undefined ? child.get( "maxable" ) : true,
                        closable: child.get( "closable" ) !== undefined ? child.get( "closable" ) : true,
                        resizable: child.get( "resizable" ) !== undefined ? child.get( "resizable" ) : true
                    }, floatingNode );

                    floatingPane.startup();
                    
                    if( isContentWidget)
                    
                    var titleHeight = domGeometry.position( floatingPane.focusNode ).h;
                    var ownHeight = domGeometry.position( this.domNode ).h;
                    
                    var position = {};
                    
                    // Calculate size
                    if( lang.isFunction( child.placeAt ) ) {
                        child.placeAt( floatingPane.containerNode );
                        var cs = domStyle.getComputedStyle( child.domNode );
                        position.w = child.get( "width" ) || cs.width;
                        position.h = child.get( "height" ) || cs.height;
                    } else {
                        floatingPane.contentWidget.containerNode.appendChild( child ); 
                        var cs = domStyle.getComputedStyle( child );
                        position.w = domStyle.toPixelValue( child, cs.width );
                        position.h = domStyle.toPixelValue( child, cs.height ) + titleHeight;
                    }
                    
                    var origin = this.placeChild( floatingPane );
                    position.x = origin.x;
                    position.y = origin.y;
                    
                    floatingPane.resize( position ); 
 
                    child = floatingPane;
                
                } else {
                    child.placeAt( this );
                    if( child.get( "x" ) === undefined || child.get( "y" ) === undefined ) {
                        child.resize( this.placeChild( child ) );
                    }
                }
                
                this.subscribeChild( child );
                child.registerContent();
                child.selectWindow();
                
            } else {
                throw new Error( "Invalid child: " + child );
            }
        }

        ,placeChild: function( child ) {
            var titleHeight = domGeometry.position( child.focusNode ).h;
            return this.placingStrategy.placeChild( child );
        }

        ,subscribeChild: function( child ) {
            child.containerHandlers = [
                on( child, "selected", lang.hitch( this, lang.partial( this.selectingChild, child ) ) )
                ,on( child, "maximized", lang.hitch( this, lang.partial( this.maximizingChild, child ) ) )
                ,on( child, "restored", lang.hitch( this, lang.partial( this.restoringChild, child ) ) )
                ,on( child, "closed", lang.hitch( this, lang.partial( this.closingChild, child ) ) )
            ];
        }
        
        ,unsubscribeChild: function( child ) {
            if( lang.isArray( child.containerHandlers ) ) {
                array.forEach( child.containerHandlers, function( handler ){
                    handler.remove();
                });
            }
        }
        
        ,getChildren: function() {
            return array.filter( this.getDescendants(), function( widget ){
                return widget.isInstanceOf( FloatingPane )
            });
        }
        
        ,selectingChild: function( child ) {
            if( this.eventActive( "selecting" ) ) {
                var state = { state:{
                    "old": this.get( "selectedIndex" )
                    ,"new": array.indexOf( this.getChildren(), child )
                }}
                this.emitEvent( "selecting", state );
                
            } else {
                this.selectChild( child );
            }
        }

        ,selectChild: function( child ) {
            var old = this.get( "selectedIndex" );
            if( child ) {
                child.selectWindow();
            }
            if( this.eventActive( "selected" ) ){
                var state = { state:{
                    "old": old
                    ,"new": array.indexOf( this.getChildren(), child )
                }}
                this.emitAlways( "selected", state );
            }
        }
        
        ,maximizingChild: function( child ) {
            if( this._maximizedChild != undefined ) {
                this._maximizedChild.restoreWindow();
            }
            this._maximizedChild = child;
            var pos = domGeometry.position( this.domNode );
            child.maximizeWindow( { t: 0, l: 0, w: pos.w, h: pos.h } );
            domStyle.set( this.domNode, { overflow: "hidden" } );
        }

        ,restoringChild: function( child ) {
            if( child == this._maximizedChild ) {
                delete this._maximizedChild;
            }
            child.restoreWindow();
            domStyle.set( this.domNode, { overflow: "hidden" } );
        }

        ,closingChild: function( child ) {
            var index = array.indexOf( this.getChildren(), child );

            if( this.eventActive( "closing" ) ) {
                this.emitEvent( "closing", { state: { index: index } } );
            } else {
                this.closeChild( child );
                this.expectedRemoval = index;
                this.emitAlways( "closed", { state: { index: index } } );
            }
        }
        
        ,closeChild: function( child ) {
            var d = new Deferred();
            var index = array.indexOf( this.getChildren(), child );
            
            if( child == this._maximizedChild ) {
                delete this._maximizedChild;
                domStyle.set( this.domNode, { overflow: "hidden" } );
            }
            this.unsubscribeChild( child );
            child.unregisterContent();
            child.closeWindow().then( function(){
                d.resolve();
            });
            return d;
        }

        ,_getSelectedIndexAttr: function() {
            var children = this.getChildren();
            for( var i = 0; i < children.length; i++ ) {
                if( children[ i ].get( "selected" ) ) {
                    return i;
                } 
            }
            return undefined;
        }
        
        ,_setSelectedIndexAttr: function( value ) {
            if( value !== undefined && value >= 0 ) {
                var child = this.getChildren()[ value ];
                if( child ) {
                    child.selectWindow();
                }
            }
        }

        ,getWidgetState: function() {
            return {
                selectedIndex: this.get( "selectedIndex" )
            }
        }
        
        ,setWidgetState: function( state ) {
            if( state.operations !== undefined ) {
                this.performOperations( state.operations ).then( lang.hitch( this, function(){
                    delete state.operations;
                    this.setWidgetState( state );
                }));
            } else {
                for( var name in state ) {
                    this.set( name, state[ name ] );
                }
            }
        }
    
        ,_getSizerAttr: function() {
            if( this.sizerWidget === undefined ) {
                this.sizerWidget = new FloatingPaneSizer( { previewNode: this.containerNode }, domConstruct.create( "div", {}, this.containerNode ) );
                this.sizerWidget.startup();
            }
            return this.sizerWidget;
        }
    
        ,performOperations: function( operations ) {
            var deferreds = [];
            for( var i = 0; i < operations.length; i++ ) {
                if( operations[ i ].type == "redraw" ) {
                    deferreds.push( this.performChildRedraw( operations[ i ] ) );
                } else if( operations[ i ].type == "insert" ) {
                    deferreds.push( this.performChildInsert( operations[ i ] ) );
                } else if( operations[ i ].type == "remove" ) {
                    deferreds.push( this.performChildRemove( operations[ i ] ) );
                }
            }
            return all( deferreds );
        }
        
        ,performChildInsert: function( params ) {
            if( this._maximizedChild != undefined ) {
                this._maximizedChild.restoreWindow();
                delete this._maximizedChild;
            }

            var d = new Deferred();
            topic.publish( "works/widget/change" );
            this.startWidgetTask({ reason: "Floating child insert" });
            var node = domConstruct.toDom( "<div>" + params.content + "</div>" );
            domStyle.set( node, { visibility: "hidden" });
            domConstruct.place( node, this.domNode );
            
            parser.parse({ rootNode: node }).then( lang.hitch( this, function( widgets ){
                domStyle.set( node, { visibility: "visible" });
                var child = widgets[ 0 ];
                domConstruct.place( child.domNode, this.domNode, params.index + 1 );

                if( child.get( "origin" ) === undefined  ) {
                    child.set( "origin", this.placeChild( child ) );
                }
                child.selectWindow();
                this.subscribeChild( child );
                
                child.registerContent().then( lang.hitch( this, function(){
                    child.set( "working", true );
                    child.resize();
                    domConstruct.destroy( node );
                    this.endWidgetTask({ reason: "Child insert" });
                    d.resolve();
                }));
 
            }));
            return d;
        }
        
        ,performChildRedraw: function( params ) {
            var d = new Deferred();
            var child = this.getChildren()[ params.index ];
            topic.publish( "works/widget/change" );
            this.startWidgetTask({ reason: "Floating child redraw" });
            child.load({ content: params.content }).then( lang.hitch( this, function(){
                this.endWidgetTask({ reason: "Floating child redraw" });
                d.resolve();
            }));
            return d;
        }
        
        ,performChildRemove: function( params ) {
            var d = new Deferred();
            //console.log( "perform remove", params.index, "expected", this.expectedRemoval )
            if( this.expectedRemoval !== undefined ) {
                if( this.expectedRemoval != params.index ) {
                    throw new Error( "Expected removal for index " + this.expectedRemoval + ", got " + params.index );
                } else {
                    delete this.expectedRemoval;
                    var child = this.getChildren()[ params.index ];
                    if( child && child.closeDeferred !== undefined ) {
                        child.closeDeferred.then( function(){
                            d.resolve();
                        })
                    } else {
                        d.resolve();
                    }
                }
            } else {
                this.closeChild( this.getChildren()[ params.index ]).then( function(){
                    d.resolve();
                });
            }
            return d;
        }
    });
    
})