define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-class"
    ,"works/lang/strings"
], function( _require, declare, domClass, strings ){
    
    return declare( [], {
        buildRendering: function() {
            this.inherited( arguments );
            this._qualifyParentBase( this.srcNodeRef );
            this._qualifyParentFlavors( this.srcNodeRef );
        }
        
        ,_setFlavorAttr: function( value ) {
            this.inherited( arguments );
            this._qualifyParentFlavors( this.domNode );
        }
        
        ,_qualifyParentBase: function( node ) {
            if( strings.hasText( this.baseClass ) && node && node.parentNode ) {
                domClass.removeAll( node.parentNode, /parentOf.*/ );
                
                var baseParentClass = "parentOf-" + this.baseClass; 
                domClass.add( node.parentNode, baseParentClass );
                
            }
        }

        ,_qualifyParentFlavors: function( node ) {
            if( node && node.parentNode ) {
                domClass.removeAll( node.parentNode, /parentOf-flavor-.*/ );
                
                domClass.forEach( node, function( cls ){
                    if( cls.match( /flavor-.*/ ) ) {
                        domClass.add( node.parentNode, "parentOf-" + cls );
                    }
                }, this );
            }
        }


    });
    
})

