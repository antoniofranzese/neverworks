define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/aspect"
    ,"dojo/topic"
    ,"dojo/request"
    ,"dojo/promise/all"
    ,"dojo/Deferred"
    ,"dojo/dom-style"
    ,"dojo/dom-class"
    ,"works/registry"
    ,"works/lang/strings"
    ,"dijit/layout/ContentPane"
    ,"../form/_WidgetMixin"
    ,"../form/_FormWidgetMixin"
    ,"../form/_FormManagerWidget"
    ,"../form/_WidgetLocator"
    ,"../dnd/_DropMixin"
    ,"./_ContentWidget"
    ,"./_ViewportMixin"
    ,"./Loader"
    
    ,"works/style!./resources/ContentPane.less"
    
], function( _require, declare, array, lang, on, aspect, topic, request, all, Deferred, domStyle, domClass, registry, strings, 
            ContentPane, _WidgetMixin, _FormWidgetMixin, _FormManagerWidget, _WidgetLocator, _DropMixin, _ContentWidget, _ViewportMixin, Loader ){

    return declare( [ ContentPane, _WidgetMixin, _FormWidgetMixin, _ContentWidget, _WidgetLocator, _DropMixin, _ViewportMixin ], {

        declaredClass: _FormWidgetMixin.inferDeclaredClass( _require )

        ,name: ""
        ,loader: false

        ,minSize: 0
        ,maxSize: Infinity
        ,splitter: false
        ,layoutPriority: 0
        ,visible: true

        ,buildRendering: function() {
            this.inherited( arguments );
            // Patch per errore in _WidgetBase
            this.focusNode = this.domNode;

        }

        ,joinForm: function( form ) {
            //console.log( "Panel join", this.id, this.registered );
            this.own(
                form.subscribeMessage( "form/startup", lang.hitch( this, function( message ){
                    if( this.get( "href" ).hasText() && this.get( "preload" ) ) {
                        //console.log( "Enlisting panel form wait" );
                        this.formDeferred = new Deferred();
                        form.ready().enlist( this.formDeferred );
                    }
                }))
            );
            this.inherited( arguments );
        }
        
        ,startup: function() {
            // console.log( "Panel start", this.id );
            this.setContentSize();
            this.inherited( arguments );
            this.startupContentManagement();
        }

        ,_setContent: function() {
            if( this._started ) {
                //console.log( "Redrawing", this.id, this._started );
                this.emit( "redrawing", { bubbles: false } );
                on.once( this, "loaded", lang.hitch( this, function(){
                    this.contentReady().then( lang.hitch( this, function(){
                        //console.log( "Redrawn", this.id, this._started );
                        this.emit( "redrawn", { bubbles: false } );
                    }))
                }));
            }
            this.inherited( arguments );
        }
        
        ,_setContentAttr: function( content ) {
            // console.log( "Panel content", this.id );
            this.unregisterDescendants();
            this.inherited( arguments );
        }

        ,_setHrefAttr: function( href ) {
            this.unregisterDescendants();
            this.inherited( arguments );
        }
        
        ,setWidgetState: function( state ) {
            this._setWidgetStateProperty( state, "content" );
            this._setWidgetStateProperty( state, "title" );
            this._setWidgetStateProperty( state, "visible" );
            this._setWidgetStateProperty( state, "scrollable" );
            this._setWidgetStateProperty( state, "font" );
            this._setWidgetStateProperty( state, "color" );
            this._setWidgetStateProperty( state, "background" );
            this._setWidgetStateProperty( state, "border" );
        }
        
        ,_setWidgetStateProperty: function( state, prop ) {
            if( state[ prop ] !== undefined ) {
                this.set( prop, state[ prop ] );
            }
        }
        
        ,_setOriginAttr: function( origin ) {
            this._origin = origin;
            if( origin != null ) {
                domStyle.set( this.domNode, "position", "absolute" );
                domStyle.set( this.domNode, origin.x >= 0 ? "left" : "right", Math.abs( origin.x ) + "px" );
                domStyle.set( this.domNode, origin.y >= 0 ? "top" : "bottom", Math.abs( origin.y ) + "px" );
            } else {
                domStyle.set( this.domNode, "position", "static" );
            }
        }
        
        ,load: function( params ) {
            var d = new Deferred();
            
            on.once( this, "loaded", function(){
                d.resolve();
            });
            
            if( "href" in params ) {
                request( params.href ).then( lang.hitch( this, function( data ) {
                    this.set( "content", data );
                }));
                
            } else if( "content" in params ) {

                this.set( "content", params.content )
            }
 
            return d;
        }
        
        ,findRelativeChild: function( name ) {
            if( this._singleChild ) {
                if( this._singleChild.get( "name" ) == name || this._singleChild.get( "canonicalName" ) == name ) {
                    return this._singleChild;
                }
            } else {
                return this.inherited( arguments );
            }
        }
        
        ,isShown: function() {
            return domStyle.isShown( this.domNode );
        }

        ,_getEmptyAttr: function() {
            return this.getDescendants().length == 0;
        }
        
        ,setContentSize: function() {
            if( this.contentHeight != undefined ) {
                domStyle.set( this.containerNode, { height: this.contentHeight });
            }
            if( this.contentWidth != undefined ) {
                domStyle.set( this.containerNode, { width: this.contentWidth });
            }
        }
        
        ,resize: function( size ) {
            if( this._globalResizeHandler ) {
                this._globalResizeHandler.remove();
                delete this._globalResizeHandler;
            }

            if( size === undefined ) {
                this.setContentSize();
            } 
            this.inherited( arguments );
            this.setContentSize();
        }

        ,_setVisibleAttr: function( value ) {
            var current = this._get( "visible" );
            var wasShown = this.isShown();

            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );

            if( current != value ) {
                if( value && this.isShown() ) {
                    this.spreadChildren( { topic: "widget/container/show"  }, /* priority */ true );
                    //this._globalResizeHandler = topic.subscribe( "form/resize", lang.hitch( this, this.resize ) );
                } else if( ! value && wasShown ) {
                    this.spreadChildren( { topic: "widget/container/hide"  }, /* priority */ true );
                }

                if( value ) {
                    topic.publish( "works/widget/change" );
                }
            }
        }
        
        ,_setTitleAttr: function( value ) {
            this.inherited( arguments );
            this._set( "showTitle", strings.hasText( value ) );
            this._set( "title", value );
        }
 
        ,_setPaddingAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, "padding", value );
        }
       
        ,_setColorAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, "color", value );
        }
        
        ,_setBackgroundAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, value );
        }
	 
        ,_setFontAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, value );
        }
        
        ,_setBorderAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, value );
        }

        ,_setRoundingAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, value );
        }

        ,_setShadowAttr: function( value ) {
            domStyle.set( this.styleNode || this.domNode, value );
        }

        ,destroy: function() {
            if( this.loaderWidget !== undefined ) {
                this.loaderWidget.destroy();
            }
            this.inherited( arguments );
        }

        ,spreadChildren: function( message, priority ) {
            if( priority || !( "" + message.topic ).startsWith( "widget/container/" ) || this.isShown() ) {
                this.inherited( arguments );
            }
        }

    });
})
