define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/has"
    ,"dojo/dom-style"
    ,"works/_base/viewport"
    ,"works/lang/types"
    ,"works/lang/strings"
    ,"works/config"
], function( req, declare, array, lang, has, domStyle, viewportManager, types, strings, config ){
    
    var BOOLEANS = [ "visible" ];
    var NULLABLES = [ "width", "height" ];
    var REQUIREDS = [ "region" ];
    var ALL = [ "visible", "width", "height", "region" ];

    return declare( [], {
        postCreate: function() {
            this.inherited( arguments );

            if( this.viewport !== undefined ) {
                this.initViewport();

                this.own( 
                    topic.subscribe( "viewport/change", lang.hitch( this, this.changeViewport ) )
                );

                var updateProperty = lang.hitch( this, this.updateViewportProperty );
                array.forEach( ALL, function( name ){
                    this.own( this.watch( name, updateProperty ) );
                }, this );
            }
            
        }
        
        ,updateViewportProperty: function( name, oldValue, newValue ){
            if( this.viewport._updating === undefined ) {
                this.viewport._base[ name ] = newValue;
                this.changeViewport();
            }
        }

        ,changeViewport: function( name ) {
            var matching = viewportManager.match( this.viewport._names, name );
           //console.log( "Matching view", name, "with", matching );
            var mixed = ( matching && this.viewport[ matching ] )
                ? lang.mixin( 
                    {}, 
                    this.viewport._base, 
                    this.viewport[ matching ], 
                    { visible: this.viewport._base.visible && this.viewport[ matching ].visible } 
                )
                : this.viewport._base;

            this.updateViewport( mixed );
        }

        ,initViewport: function() {
            var vp = this.viewport;
            vp._names = Object.keys( this.viewport );
            delete this.viewport._updating;

            if( vp.landscape !== undefined || vp.portrait !== undefined ) {

               if( vp.landscape === undefined ) {
                    vp.landscape = {};
                    vp._names.push( "landscape" );
                }
            
                if( vp.portrait === undefined ) {
                    vp.portrait = {};
                    vp._names.push( "portrait" );
                }
                
                if( vp.portrait.visible !== undefined && vp.landscape.visible === undefined ) {
                    vp.landscape.visible = ! vp.portrait.visible;
                
                } else if( vp.landscape.visible !== undefined && vp.portrait.visible === undefined ) {
                    vp.portrait.visible =  ! vp.landscape.visible;

                }
            }

            var base = vp._base = {};
            array.forEach( ALL, function( prop ){
                base[ prop ] = this.get( prop );
            }, this );

            array.forEach( vp._names, function( name ){
                console.log( this.id, name, "=>", vp[ name ].visible );
                if( vp[ name ].visible === undefined ) {
                    vp[ name ].visible = true;
                }
            }, this );

        }
        
        ,updateViewport: function( viewport, properties ) {
            this.viewport._updating = true;
            
            try {
                if( typeof viewport == "string" ) {
                    viewport = this.viewport[ viewport ];
                }

                if( viewport ) {
                    array.forEach( properties !== undefined ? properties : ALL, function( prop ){
                        var value = viewport[ prop ];
                        if( value !== undefined ) {
                            var current = this.get( prop );
                            if( current != value ) {
                                this.set( prop, value );
                            }
                        }
                    }, this );
                }
            } finally {
                delete this.viewport._updating;
            }
            
            return this;
        }

        ,_getWidthAttr: function() {
            if( this.domNode.style.width ) {
                return domStyle.toPixelValue( this.domNode, this.domNode.style.width );
            } else {
                return undefined;
            }
        }

        ,_getHeightAttr: function() {
            if( this.domNode.style.height ) {
                return domStyle.toPixelValue( this.domNode, this.domNode.style.height );
            } else {
                return undefined;
            }
        }

        ,_setWidthAttr: function( value ) {
            var size = null;
            if( value ) {
                size = types.isNumberLike( value ) ? parseInt( value ) + "px" : strings.safe( value );
            }
            if( size != this.domNode.style.width ) {
                this.domNode.style.width = size;
                this._set( "width", this._getWidthAttr() );
            }
        }

        ,_setHeightAttr: function( value ) {
            var size = null;
            if( value ) {
                size = types.isNumberLike( value ) ? parseInt( value ) + "px" : strings.safe( value );
            }
            if( size != this.domNode.style.height ) {
                this.domNode.style.height = size;
                this._set( "height", this._getHeightAttr() );
            }
        }

    });

});

