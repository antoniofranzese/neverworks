define([
    "require"
    ,"dojo/_base/declare"
	,"dojo/_base/connect"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojox/layout/ExpandoPane"
    ,"../form/_WidgetMixin"
    ,"../form/_FormWidgetMixin"
    ,"../form/_WidgetLocator"
    ,"./_ContentWidget"
    
    ,"works/style!./resources/ExpandoPane.css"
    
], function( _require, declare, connectUtil, lang, on, has, domClass, domGeometry, ExpandoPane, _WidgetMixin, _FormWidgetMixin, _WidgetLocator, _ContentWidget ){

    var ExpandoPane = declare( [ ExpandoPane, _ContentWidget, _WidgetMixin, _FormWidgetMixin  ], {
        
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,closedHeight: 0
        ,duration: 320
        ,previewOpacity: 1
        
        ,toggleRequested: false
        
        ,postMixInProperties: function() {
            this.inherited( arguments );
            if( this.startExpanded == false ) {
                this.toggleRequested = true;
            }
            if( this.visible == false ) {
                this.hiddenOnStartup = true;
            }
        }
    
        ,startup: function() {
            this.inherited( arguments );
            
            this.own(
                on( this.titleWrapper, "click", lang.hitch( this, function( evt ){
                    if( evt.target !== this.iconNode ) {
                        this.toggle();
                    }
                }))
                ,on( this, "animated", lang.hitch( this, this._afterAnimate ) )
            );

            if( has( "mobile" ) ) {
                this.own(
                    on( this.domNode, "touchstart", lang.hitch( this, function( evt ){
                        if( evt.target == this.domNode && this.get( "expanded" ) != true ) {
                            this.expand();
                        }
                    }))
                );
            }
            
            this.startupContentManagement();
        }

        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "toggle" );
        }
        
        ,leaveForm: function() {
            this.retireEvent( "toggle" );
            this.inherited( arguments );
        }
    
        ,preview: function() {
            domClass.add( this.domNode, "preview" );
            this.inherited( arguments );
        }
    
        ,_getClosedSize: function() {
            return this._getTitleHeight() + this.closedHeight;
        }
    
    	,_setupAnims: function(){
            this.inherited( arguments );
            this._animConnects.push( connectUtil.connect( this._showAnim, "beforeBegin", this, "_showStart" ) );
            this._animConnects.push( connectUtil.connect( this._hideAnim, "beforeBegin", this, "_hideStart" ) );
        }
        
        ,_onShowEnd: function() {
            //console.log( "Show end" );
        }
        
        ,_showStart: function() {
            if( ! this.toggleRequested ) {
                this.emitEvent( "toggle", { state: { expanding: true } } );
            }
            this.toggleRequested = false;
        }

        ,_hideStart: function() {
            domClass.remove( this.domNode, "preview" );
            if( ! this.toggleRequested ) {
                this.emitEvent( "toggle", { state: { expanding: false } } );
            }
            this.toggleRequested = false;
        }
    
        ,_showEnd: function() {
            this.inherited( arguments );
            this.emit( "animated", { bubbles: false } );
        }
        
        ,_hideEnd: function() {
            this.inherited( arguments );
            this.emit( "animated", { bubbles: false } );
        }
        
        ,expand: function() {
            if( ! this.get( "expanded" ) ) {
                this.toggle();
            }
        }

        ,collapse: function() {
            if( this.get( "expanded" ) ) {
                this.toggle();
            }
        }
        
        ,resize: function( size ) {

            if( this._frozenSize === undefined ) {
                if( this._isHorizontal ) {
                    this._frozenSize = { "h": this._getTitleHeight() + domGeometry.getBorderExtents( this.titleWrapper ).h + domGeometry.getMarginBox( this.cwrapper ).h };
                } else {
                    this._frozenSize = { "w": domGeometry.getMarginBox( this.domNode ).w };
                }
                this.inherited( arguments, [ this._frozenSize ] );
            }
            
            this.inherited( arguments );
        }

        ,getWidgetState: function() {
            var expanded = this.get( "expanded" );
            if( expanded !== this._lastExpanded ) {
                return {
                    expanded: this.get( "expanded" )
                }
            }
        }
        
        ,setWidgetState: function( state ) {
            if( state.visible !== undefined ) {
                this.set( "visible", state.visible );
                if( this.hiddenOnStartup && state.visible ) {
                    this.showRecovery = connectUtil.connect( this._showAnim, "onEnd", this, "_recoverFirstShow" )
                }
                delete state.visible;
            }
            if( state.expanded !== undefined ) {
                if( this.get( "expanded" ) != state.expanded ) {
                    this.startWidgetTask({ reason: "Changing expando state" });
                    this.closeTask = true;
                    setTimeout( lang.hitch( this, this._closeTask ), 5000 ); //Recovery
                } else {
                    delete state.expanded;
                }
            }
            this.inherited( arguments );
        }

        ,_closeTask: function() {
            if( this.closeTask ) {
                delete this.closeTask;
                this.endWidgetTask({ reason: "Changing expando state" });
            }
        }
        
        ,_afterAnimate: function() {
            this._closeTask();
        }
        
        ,_recoverFirstShow: function() {
            // console.log( "First show" );
            this.showRecovery.remove();
            delete this.showRecovery;
            delete this.hiddenOnStartup;
            setTimeout( lang.hitch( this, this._showEnd ), 0 );
        }

        ,_getExpandedAttr: function() {
            // return this._showing == true ;
            return ! domClass.contains( this.domNode, "dojoxExpandoClosed" );
        }
        
        ,_setExpandedAttr: function( value ) {
            this.toggleRequested = true;
            if( this._showAnimationEnd ) {
                this._showAnimationEnd.remove();
            }
            if( this._hideAnimationEnd ) {
                this._hideAnimationEnd.remove();
            }
            this._showAnimationEnd = connectUtil.connect( this._showAnim, "onEnd", lang.hitch( this, function(){ this.emit( "animated", { bubbles: false } ) } ) );
            this._hideAnimationEnd = connectUtil.connect( this._hideAnim, "onEnd", lang.hitch( this, function(){ this.emit( "animated", { bubbles: false } ) } ) );
            
            this[ value ? "expand" : "collapse" ]();
        }
    	
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }
        
    });
    
    //ExpandoPane.markupFactory = _WidgetMixin.markupFactory;
    
    return ExpandoPane;
})
