define([
		"require"
        ,"works/lang/declare"
        ,"dojo/_base/lang"
        ,"dojo/_base/array"
        ,"dojo/query"
        ,"dojo/dom-style"
        ,"dijit/layout/AccordionContainer"
		,"./_ContainerMixin"
        
], function( req, declare, lang, array, query, domStyle, AccordionContainer, _ContainerMixin ){
    var AccordionContainer = declare( declare.className( req ), [ AccordionContainer, _ContainerMixin ], {
        
        startup: function() {
            this.inherited( arguments );
        }
        
        ,_performChildSelection: function( child ) {
            this.selectChild( child, /* animate */ true );
        }
        
        ,onOperationsPerformed: function() {
            //TODO: trovare cosa attendere prima di fare il resize 
            setTimeout( lang.hitch( this, this.resize ), 500 );
        }
                
        ,updateChildVisibility: function( child ) {
            var wrapperNode = query( ".dijitAccordionInnerContainer[id=" + child.id + "_wrapper]", this.domNode )[ 0 ];
            if( wrapperNode ) {
                domStyle.set( wrapperNode, "display", child.get( "visible" ) != false ? "" : "none" );
            }
            this.inherited( arguments );
        }

        ,onChildVisibility: function( child ) {
            this.inherited( arguments );
            this.resize();
        }

 	});
    
    AccordionContainer.markupFactory = _ContainerMixin.markupFactory;
    
    return AccordionContainer;
});	
