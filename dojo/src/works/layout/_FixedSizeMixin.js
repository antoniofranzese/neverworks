define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"works/lang/strings"
    ,"dijit/layout/BorderContainer"
], function( _require, declare, domStyle, domGeometry, strings, BorderContainer ){
    
    return declare( [], {
        buildRendering: function() {
            this.inherited( arguments );
            if( this.srcNodeRef.style.height ) {
                if( strings.endsWith( this.srcNodeRef.style.height, "%" ) ) {
                    this.elasticHeight = parseInt( this.srcNodeRef.style.height.substring( 0, this.srcNodeRef.style.height.length ) );
                } else {
                    this.fixedHeight = domStyle.toPixelValue( this.srcNodeRef, this.srcNodeRef.style.height );
                }
            }
            if( this.srcNodeRef.style.width ) {
                if( strings.endsWith( this.srcNodeRef.style.width, "%" ) ) {
                    this.elasticWidth = parseInt( this.srcNodeRef.style.width.substring( 0, this.srcNodeRef.style.width.length ) );
                } else {
                    this.fixedWidth = domStyle.toPixelValue( this.srcNodeRef, this.srcNodeRef.style.width );
                }
            }
            
            //console.log( this.declaredClass, "width", this.srcNodeRef.style.width, this.elasticWidth, "height", this.srcNodeRef.style.height, this.elasticHeight );
        }

        ,isChildOfLayoutContainer: function() {
            var parent = this.getParent();
            if( parent != null && parent !== undefined ) {
                return parent.isLayoutContainer == true 
                    && parent.isStaticLayoutWidget != true
                    && parent.isFormManager != true
                    && parent.isRealPane != true;
            } else {
                return false;
            }
        }

        ,autoProcessFixedSize: true

        ,resize: function( size ) {
            if( this.autoProcessFixedSize ) {
                this.processFixedSize( size );
            }
            this.inherited( arguments );
        }

        ,processFixedSize: function( size ) {
            size = ( size !== undefined ? size : {} );
            if( ! this.isChildOfLayoutContainer() ) {
                if( this.fixedWidth ) {
                    size.w = this.fixedWidth;
                }

                if( this.fixedHeight ) {
                    size.h = this.fixedHeight;
                }
                
            }

            return this.processMarginSize( size );
        }
        
        ,processElasticSize: function( size ) {
            var result = {};
            
            if( this.elasticWidth && size !== undefined && size.w !== undefined ) {
                result.w = size.w * this.elasticWidth / 100;
            } else {
                result.w = size && size.w;
            }
            
            if( this.elasticHeight && size !== undefined && size.h !== undefined ) {
                result.h = size.h * this.elasticHeight / 100;
            } else {
                result.h = size && size.h;
            }
            
            return this.processMarginSize( result );
        }
        
        ,processMarginSize: function( size ) {
            if( size )  {
                var domMargins = domGeometry.getMarginExtents( this.domNode );
                if( size.w !== undefined ) {
                    size.w = size.w - domMargins.w;
                }
                if( size.h !== undefined ) {
                    size.h = size.h - domMargins.h;
                }
            }
            return size;
        }
        
    });
    
})

