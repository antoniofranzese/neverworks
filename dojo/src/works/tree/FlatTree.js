define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/aspect"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/dom-construct"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/dom-attr"
    ,"dijit/Tree"
    ,"dojo/store/Memory"
    ,"dojo/store/Observable"
    ,"dijit/tree/ObjectStoreModel"
    ,"works/form/_FormWidgetMixin"
    ,"works/form/_WidgetMixin"
    ,"works/_WidgetUtils"
    ,"works/dnd/Source"
    
    ,"works/style!./resources/Tree.css"
], function( _require, declare, lang, aspect, on, has, domConstruct, domClass, domStyle, domAttr, dijitTree, Memory, Observable, ObjectStoreModel, _FormWidgetMixin, _WidgetMixin, _WidgetUtils, Source ){
    
    var DRAG_DELAY = lang.getObject( "worksConfig.dragDelay" );
    var DOUBLE_CLICK_DELAY = lang.getObject( "worksConfig.doubleClickDelay" );
    var TOUCH_INSPECT_DELAY = lang.getObject( "worksConfig.touchInspectDelay" );

    var TreeNode = declare( [ dijitTree._TreeNode ], {
        _updateLayout: function() {
            this.inherited( arguments );
            
            /* Row style */
            var row = this.item.row || {};
            
            if( this.lastRowClass !== undefined ) {
                domClass.remove( this.rowNode, this.lastRowClass );
                delete this.lastRowClass;
            }
            
            if( row.cs !== undefined ) {
                domClass.add( this.rowNode, row.cs );
                this.lastRowClass = row.cs;
            }
            
            if( row.bk !== undefined ) {
                domStyle.set( this.rowNode, "background-color", row.bk );
                domStyle.set( this.rowNode, "background-image", domStyle.get( this.rowNode.parentNode, "background-image" ) );
                domStyle.set( this.rowNode, "background-repeat", domStyle.get( this.rowNode.parentNode, "background-repeat" ) );
                domStyle.set( this.rowNode, "background-position", domStyle.get( this.rowNode.parentNode, "background-position" ) );
                domClass.add( this.rowNode, "background" );
            } else {
                domStyle.set( this.rowNode, "background-color", null );
                domStyle.set( this.rowNode, "background-image", null );
                domStyle.set( this.rowNode, "background-repeat", null );
                domStyle.set( this.rowNode, "background-position", null );
                domClass.remove( this.rowNode, "background" );
            }
            
            domStyle.set( this.rowNode, "color", row.cl !== undefined ? row.cl : null );
            domStyle.set( this.rowNode, row.ft ? row.ft : { font: null } );
            
            /* Drag & Drop */
            var tree = this.tree;
            var tn = this;
            
            if( tree.drags ) {
                var item = tn.item;

                if( tn.item.prp !== undefined && tn._dndSource === undefined ) {
                    tn._dndSource = new Source({
                        avatar: function(){ 
                            var av = tn.item.avt !== undefined ? tn.item.avt : tree.avatar; 
                            return typeof av == "string" ? widget( av ) : av;
                        }
                        ,proposal: function(){ 
                            if( tn.item.prp !== undefined ) {
                                return {
                                    source: tree.form.getWidgetQualifiedName( tree )
                                    ,key: tn.item.id
                                    ,content: tn.item.prp || {} 
                                }
                            } else {
                                return undefined;
                            }
                        } 
                        ,timeout: DRAG_DELAY
                    }, tn.contentNode );
                    domClass.add( this.rowNode, "drag" );

                } else if( tn.item.prp === undefined && tn._dndSource !== undefined ){
                    tn._dndSource.destroy();
                    delete tn._dndSource;
                    domClass.remove( this.rowNode, "drag" );
                }

            } else {
                if( tn._dndSource !== undefined ) {
                    tn._dndSource.destroy();
                    delete tn._dndSource;
                    domClass.remove( this.rowNode, "drag" );
                }
            }
            
        }
        
        ,destroy: function() {
            //console.log( "Destroying", this );
            if( this._dndSource !== undefined ) {
                this._dndSource.destroy();
                delete this._dndSource;
            }
            this.inherited( arguments );
        }
    });
    
    var Tree = declare( [ dijitTree, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetUtils.inferDeclaredClass( _require )
        ,avatar: null
        ,ROOT: "__ROOT__"
        ,drags: false
        ,persist: false
                
        ,postMixInProperties: function() {
            this.inherited( arguments );
            
            if( this.startupItems ) {
                
                var store = new Memory({
                    data: this.startupItems
                    ,getChildren: function( object ){
                        return this.query({ par: object.id });
                    }
                });
                
                var model = new ObjectStoreModel({
                    labelAttr: "cap",
                    store: new Observable( store ),
                    query: { id: this.ROOT },
                    labelType: "html"
                });

                this.set( "model", model );                                
                
                delete this.startupItems;
            }
            
        }
        
        ,buildRendering: function() {
            this.inherited( arguments );
            this.shieldNode = domConstruct.create( "div", { "style": { 
                visibility: "hidden"
                ,position: "absolute"
                ,top: "0px"
                ,left: "0px"
                ,width: "100%"
                ,height: "100%"
                ,backgroundColor: "white"
                ,opacity: 0.7
                ,pointerEvents: "none"
            } }, this.domNode );
            
        }
        
        ,startup: function() {
            this.inherited( arguments );
            var self = this;

            if( has( "mobile" ) ) {
                var self = this;
                this.own(
                    on( this.containerNode, on.selector( ".dijitTreeNode", "touchstart" ), function( evt ){
                        evt.preventDefault();
                        var item = registry.byNode( this ).get( "item" );

                        self._touch = evt;
                        self._inspectTimeout = setTimeout( function(){
                            delete self._inspectTimeout;
                            
                            if( self._touch !== undefined ) {
                                var evt = self._touch;
                                delete self._touch;

                                self._onNodeRightClick( item, evt );
                            }
                        }, TOUCH_INSPECT_DELAY );
                    })

                    ,on( this.containerNode, on.selector( ".dijitTreeExpando", "touchstart" ), function( evt ){
                        // Ignore expando click
                        delete self._touch;
                    })

                    ,on( this, "click", function( item ){
                        if( self._inspectTimeout ) {
                            clearTimeout( self._inspectTimeout );
                            delete self._inspectTimeout;
                        }
                        
                        if( self._touch !== undefined ) {
                            var evt = self._touch;
                            delete self._touch;
                            self._onNodeLeftClick( item, evt );
                        }
                    })
                
                )
                
            } else {
                this.own(
    				on( this.containerNode, on.selector(".dijitTreeNode", "contextmenu" ), function( evt ){
                        evt.preventDefault();
    					self._onNodeRightClick( registry.byNode( this ).get( "item" ), evt );
    				})

                    ,on( this.containerNode, on.selector(".dijitTreeNode", "click" ), function( evt ){
                        if( self._itemClicked ) {
                            delete self._itemClicked;
                            self._onNodeLeftClick( registry.byNode( this ).get( "item" ), evt );
                        }
                    })

                    // L'evento arriva prima del click sul TreeNode, e solo per la parte etichetta
                    ,on( this, "click", function( item ){
                        self._itemClicked = true;
                    })
                
                )
            }
        }
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "clicked", { rename: "click" } );
            this.announceEvent( "inspect" );
            this.form = form;
        }
        
        ,leaveForm: function() {
            delete this.form;
            this.retireEvent( "inspect" );
            this.retireEvent( "clicked" );
            this.inherited( arguments );
        }

        ,_onNodeRightClick: function( item, evt ) {
            //console.log( "Right", evt, item );
            var origin = this.parseEventOrigin( evt );
            var id = item.id;
            this.emitAlways( "inspect", { state: { 
                "key" : id == this.ROOT ? null : id 
                ,"button": "right"
                ,"x": origin.clickX
                ,"y": origin.clickY
            }});
        }
        
        ,_onNodeLeftClick: function( item, evt ) {
            //console.log( "Left", evt, item );
            var origin = this.parseEventOrigin( evt );
            var id = item.id;
            this.emitAlways( "clicked", { state: {
                "key" : id == this.ROOT ? null : id
                ,"button": "left"
                ,"x": origin.clickX
                ,"y": origin.clickY
            }});
        }
        
        ,getIconClass: function( node, opened ) {
            if( this.icons ) {
                if( node.icn in this.icons ) {
                    var icon = this.icons[ node.icn ];
                    if( typeof icon == "string" ) {
                        return icon;
                    } else {
                        return icon[ opened ? "on" : "off" ];
                    }
                } else {
                    return this.inherited( arguments );
                }
            } else {
                return this.inherited( arguments );
            }
            
        }
        
        ,_setSelectedKeyAttr: function( value ) {
            if( value != null && value !== undefined ) {
                var store = this.model.store;
                var path = [];
                var id = value;
                do {
                    path.unshift( "" + id );
                    var result = store.query( { "id": id });
                    if( result && result.length == 1 ) {
                        id = result[0].par;
                    } else {
                        id = undefined;
                    }
                } while( id );
                this.set( "path", path );
                
            } else {
                this.set( "selectedNodes", undefined );
            }
        }
        
        ,_setItemsAttr: function( value ) {
            var store = this.model.store;

            store.query().forEach( function( item ){
                store.remove( item.id );
            });
            
            for( var i in value ) {
                store.add( value[ i ] );
            }
            
        }
        
        ,setWidgetState: function( state ) {
            if( state.items ) {
                this.set( "items", state.items );
                delete state.items;
            }
            this.inherited( arguments );
        }

        ,_setDisabledAttr: function( value ) {
            domClass[ value ? "add" : "remove" ]( this.domNode, "disabled" );
            domStyle.set( this.shieldNode, { visibility: value ? "visible" : "hidden" });
        }
        
        ,_createTreeNode: function( /* Object */ args ){
            return new TreeNode( args );
        }
        
    });
    
    Tree.markupFactory = _WidgetUtils.markupFactory;
    Tree._TreeNode = TreeNode;
    
    return Tree
})