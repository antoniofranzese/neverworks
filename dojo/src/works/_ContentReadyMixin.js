define([
    "dojo/_base/declare"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"dojo/promise/all"
], function( declare, array, lang, all ) {
    
    return declare( [], {
        
        contentReady: function() {
            //console.log( "Content ready on", this.id );
            var deferreds = array.filter( array.map( array.filter( this.contentReadyDescendants() , function( widget ){
                //console.log( "Content descendant", widget.id );
                return widget.onLoadDeferred !== undefined || lang.isFunction( widget.contentReady );
            }), function( w ){
                return lang.isFunction( w.contentReady ) ? w.contentReady() : w.onLoadDeferred;
            }), function( d ){
                return ! d.isResolved();
            });

            if( this.onLoadDeferred !== undefined && ! this.onLoadDeferred.isResolved() ) {
                deferreds.push( this.onLoadDeferred );
            }
            //console.log( "End Content ready on", this.id );

            //console.log( this.id, "Deferreds", array.map( deferreds, function(d){ return d.isResolved() } ) );
            return all( deferreds );
        }
        
        ,contentReadyDescendants: function() {
            return lang.isFunction( this.getChildren ) ? this.getChildren() : this.getDescendants();
        }
        
    });

})
