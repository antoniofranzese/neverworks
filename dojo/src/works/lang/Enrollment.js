define([
    "dojo/_base/lang"
    ,"./strings"
], function( lang, strings ) {

    return function() {
        this._list = [];
        this.length = 0;
        
        this.add = function( value ) {
            var key;
            do {
                key = strings.generateHexID( 64 );
            } while( this.indexOf( key ) !== undefined );
            
            this._list.push({ 
                key: key
                ,value: value 
            });
            
            var self = this;
            this.length = this._list.length;
            
            return {
                remove: function() {
                    self.remove( key );
                }
            }
        }

        this.indexOf = function( key ) {
            for( var i = 0; i < this._list.length; i++ ) {
                if( this._list[ i ].key == key ) {
                    return i;
                }
            }
            return undefined;
        }
        
        this.remove = function( key ) {
            var i = this.indexOf( key );
            if( i !== undefined ) {
                this._list.splice( i, 1 );
            } else {
                for( var i = this._list.length - 1; i >= 0; i-- ) {
                    if( this._list[ i ].value == key ) {
                        this._list.splice( i, 1 );
                    }
                }
            }
            this.length = this._list.length;
        }

        this.contains = function( key ) {
            var i = this.indexOf( key );
            if( i !== undefined ) {
                return true;
            } else {
                for( var i = this._list.length - 1; i >= 0; i-- ) {
                    if( this._list[ i ].value == key ) {
                        return true;
                    }
                }
                return false;
            }
        }

        this.clear = function() {
            this._list = [];
            this.length = 0;
        }
        
        this.forEach = function( callback, context ) {
            if( context !== undefined ) {
                callback = lang.hitch( context, callback );
            }
            for( var i = 0; i < this._list.length; i++ ) {
                callback( this._list[ i ].value );
            }
        }

        this.map = function( callback, context ) {
            if( context !== undefined ) {
                callback = lang.hitch( context, callback );
            }
            var result = [];
            for( var i = 0; i < this._list.length; i++ ) {
                result.push( callback( this._list[ i ].value ) );
            }
            return result;
        }

        this.filter = function( callback, context ) {
            if( context !== undefined ) {
                callback = lang.hitch( context, callback );
            }
            var result = [];
            for( var i = 0; i < this._list.length; i++ ) {
                var value = this._list[ i ].value;
                if( callback( value ) == true ) {
                    result.push( value );
                }
            }
            return result;
        }

        this.values = function() {
            var result = [];
            for( var i = 0; i < this._list.length; i++ ) {
                result.push( this._list[ i ].value );
            }
            return result;
        }
    }
    
});