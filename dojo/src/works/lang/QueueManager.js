define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/when"
    ,"dojox/collections/Queue"
], function( _require, declare, lang, when, Queue ){
    
    return declare( [], {

        constructor: function( params ) {
            params = params || {};
            this.counter = 0;
            this.working = 0;
            this.max = params.max || 1;
            this.onError = params.onError;
            this.name = params.name || "<anonymous>";
    		this.queue = [];
    		this.deferreds = {};
        }
        
		/*
		 * Parametri richiesti
		 * job, la funzione da eseguire; puo' ritornare un dojo.Deferred
		 * 
		 * Parametri opzionali
		 * id (string), l'identificativo del job; se presente tra i differiti, sostituisce il job esistente
		 * defer (int), il tempo da attendere prima di inserire il job in coda; durante questo tempo, il job puo' essere sostituito o annullato
		 * hold (int), il tempo da attendere, in caso di dojo.Deferred, prima di ricominciare il pick
		 * 
		 * Ritorna un handler che rappresenta il job, che contiene il metodo remove() per annullarlo
		 * 
		 */

		,schedule: function( params ) {
			var handler = dojo.mixin( {}, params );

			if( !handler.id ) {
				handler.id = "queue_handler_" + this.counter++ ;
			}

            handler.defer = isFinite( handler.defer ) ? Math.abs( parseInt( handler.defer ) ) : undefined;
            handler.hold = isFinite( handler.hold ) ? Math.abs( parseInt( handler.hold ) ) : undefined;

			if( handler.defer ) {
				return this.defer( handler, params.defer );
			} else {
				return this.enqueue( handler );
			}
		}
	
		,remove: function( handler ) {
			if( this._deferreds[ handler.id ] ) {
				//console.log( "cancelling " + handler.id)
				clearTimeout( this.deferreds[ handler.id ].timeoutHandler );
				delete this.deferreds[ handler.id ];
			}
		}
		
		,enqueue: function( handler ) {
			//console.log( this.name, "enqueueing " + handler.id + " as " + (handler.first ? "first" : "last" ) );
            if( handler.first ) {
                this.queue.unshift( handler );
            } else {
    			this.queue.push( handler );
            }
			this.pick();
			handler.remove = function(){};
			return handler;
		}

		,defer: function( handler, time ) {
			this.remove( handler );
            
			//console.log( "scheduling " + handler.id );
			handler.timeoutHandler = setTimeout( lang.hitch( this, function(){
				this.enqueue( handler );
				delete this.deferreds[ handler.id ];
			}), time );
	
            var self = this;
			this.deferreds[ handler.id ] = handler;
			handler.remove = function(){ self.remove( handler ) };
			return handler;
		}
		
		,pick: function() {
            //console.log( "PICK", this.working );
            if( this.working < this.max ) {
    			if( this.queue.length > 0 ) {
                    this.working += 1;
    				var handler = this.queue.shift();
				    var hold = handler.hold;
                    
    				if( lang.isFunction( handler.job ) ) {
    					try {
                            var afterJob =       
            				//console.log( this.name, "picking " + handler.id + ", " + this.working );
    						
                            when( lang.hitch( handler, handler.job )() 
                                ,lang.hitch( this, function(){
        							//console.log( this.name, "scheduling next pick" );
                                    this.working -= 1;
        							this.nextPick( hold );
        						})
                                ,lang.hitch( this, function( error ){
        							//console.log( "handler error" );
                                    this.working -= 1;
                                    if( lang.isFunction( this.onError ) ) {
                                        setTimeout( lang.hitch( this, function(){
                                            this.onError( error );
                                        }), 0 );
                                    } else {
                                        this.nextPick( hold );
                                    }
                                }) 
                            );
    					} catch( e ) {
    						//console.error( e );
                            this.working -= 1;
                            this.nextPick( hold );
    					}
                    } else {
                        this.working -= 1;
                        this.nextPick( hold );
                    }

    			}
            }
		}
		
        ,reset: function() {
            this.working = 0;
    		this.queue = [];
    		this.deferreds = {};
			clearTimeout( this.pickTimeout );
        }

		,nextPick: function( timeout ) {
			timeout = timeout || 0;
			clearTimeout( this.pickTimeout );
			this.pickTimeout = setTimeout( lang.hitch( this, this.pick ), timeout );
		}
		
    });
    
});

