define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/Stateful"
    ,"dojo/Evented"
], function( req, declare, lang, array, Stateful, Evented ){
    
    return declare( declare.className( req ), [ Stateful, Evented ], {
        
        postscript: function(){
            // override Stateful behavior
    	}

        ,own: function() {
            array.forEach( arguments, function( handler ){
                if( handler && lang.isFunction( handler.remove ) ) {
                    if( this._subscriptions === undefined ) {
                        this._subscriptions = [];
                    }
                    this._subscriptions.push( handler );
                }
            }, this );
        }
        
        ,destroy: function() {
            if( this._subscriptions ) {
                array.forEach( this._subscriptions, function( subscription ){
                    subscription.remove();
                });
                delete this._subscriptions;
            }
            this.inherited( arguments );
        }
    
    });
    
});