define([
    "dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
], function( declare, lang, array ){

    declare.className = function( source ) {
        var name;
        if( source.module !== undefined && source.module.mid !== undefined ) {
            name = source.module.mid;
        } else {
            name = "" + source;
        }
        return name.replace( new RegExp( "\\/", "g" ), "." );
    }
    
    declare.hitch = function( owner, method ) {
        var originalMethod = owner[ method ];
        owner[ method ] = lang.hitch( owner, originalMethod );
    }

    declare.statics = function( statics, cls ) {
        if( cls !== undefined ) {
            if( typeof statics == "object" ) {
                array.forEach( Object.keys( statics ), function( key ){
                    if( !( key in cls ) ) {
                        cls[ key ] = statics[ key ];
                    }
                })
            }
            return cls;
        } else {
            return statics
        }
    }
    
    return declare;
    
});