define([
    "works/lang/declare"
    ,"dojo/_base/lang"

], function( declare, lang ){

    return declare( [], {
        ttl: 5000

        ,constructor: function( params ) {
            declare.safeMixin( this, params );

            this._cleanup = lang.hitch( this, function() {
                delete this._instance;
                delete this._timeout;
            });

            this.startup();
        }

        ,locator: function() {}

        ,startup: function() {}

        ,touch: function() {
            if( this._timeout ) {
                clearTimeout( this._timeout );
            }
            this._timeout = setTimeout( this._cleanup, this.ttl );
        }

        ,instance: function() {
            if( this._instance === undefined ) {
                //console.log( "Locating" );
                this._instance = this.locator();
            }
            if( this._instance ) {
                this.touch();
            }
            return this._instance;
        }

        ,get: function( name ) {
            var instance = this.instance();
            if( instance ) {
                this.touch();
                return instance.get( name );
            }
        }

        ,set: function( name, value ) {
            var instance = this.instance();
            if( instance ) {
                this.touch();
                instance.set( name, value );
            }
        }

    });

})