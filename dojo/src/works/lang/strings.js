define([
    "dojo/_base/lang"
	,"dojo/string"
    ,"./StringBuilder"
    ,"ext/lz-string"
], function( lang, dojoString, StringBuilder, lz ){

	String.prototype.startsWith = function( text ) { 
		return this.indexOf( text ) == 0;
	}

	String.prototype.endsWith = function( text ) { 
        if( text.length <= this.length ) {
    		return this.lastIndexOf( text ) == this.length - text.length;
        } else {
            return false;
        }
	}

	String.prototype.hasText = function() {
		var result = false;
		if( this.trim().length > 0 ){
			result = true;
		}
		return result;
	}
    
    String.prototype.replaceAll = function( from, to ) {
        return this.replace( new RegExp( from, "g" ), to );
    }

    String.prototype.zip = function() {
        return lz.compressToBase64( this );
    }
    
    String.prototype.unzip = function() {
        return lz.decompressFromBase64( this );
    }

	var strings = lang.mixin( {
		safe: function( obj ) {
		    if( obj === undefined || obj == null ) {
		        return "";
		    } else {
		        return strings.toString( obj );
		    }
		}
		
        ,toString: function( obj ) {
            if( obj != undefined && obj != null ) {
    			if( typeof obj != "string" ) {
    				if( lang.isFunction( obj.toString() ) ) {
    					return obj.toString();
    				} else {
    					return "" + obj;
    				}
    			} else {
    				return obj;
    			}
            } else {
                return "";
            }
			
		}
		
		,startsWith: function( str, text ) {
			return this.toString( str ).startsWith( text );
		}

		,endsWith: function( str, text ) {
			return this.toString( str ).endsWith( text );
		}

		,hasText: function( str ) {
			return this.toString( str ).hasText();
		}
		
		,normalizeCamelCase: function( str, separator) {
			separator = separator || "-";
			regexp = /^[A-Z]/; //regular expression per maiuscole
			var ret_str = "";
			for(i=0; i<str.length; i++){
				var currChar = str.charAt(i); 
				if(regexp.test( currChar )){
					ret_str += separator + currChar.toLowerCase();
				}else{
					ret_str += currChar;
				}
			}
			return ret_str;
            
		}
        
        ,generateHexID: function( len ) {
            len = len || 16;
            var result = "";
            while( result.length < len ) {
                result += Math.floor( Math.random() * 10000000000 ).toString( 16 );
            }
            
            return result.substring( 0, len );
        }
        
        ,builder: function() {
            return new StringBuilder();
        }
        
        ,zip: function( str ) {
            return lz.compressToBase64( str );
        }
        
        ,unzip: function( str ) {
            return lz.decompressFromBase64( str );
        }
        
	}, dojoString );
	
	return strings;

});