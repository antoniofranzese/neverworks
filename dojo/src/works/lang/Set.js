define([
    "dojo/_base/lang"
], function( lang ) {
    
    return function() {
        this._hash = {};
        this.add = function( value ) {
            this._hash[ value ] = true;
        }

        this.remove = function( value ) {
            delete this._hash[ value ];
        }
        
        this.contains = function( value ) {
            return value in this._hash;
        }

        this.clear = function() {
            this._hash = {};
        }
        
        this.forEach = function( callback, context ) {
            if( context != undefined ) {
                callback = lang.hitch( context, callback );
            }
            for( var value in this._hash ) {
                callback( value );
            }
        }

        this.map = function( callback, context ) {
            if( context !== undefined ) {
                callback = lang.hitch( context, callback );
            }
            var result = [];
            for( var value in this._hash ) {
                result.push( callback( value ) );
            }
            return result;
        }

        this.values = function() {
            var result = [];
            for( var value in this._hash ) {
                result.push( value );
            }
            return result;
        }
    }
    
});