define([
    "require"
], function( _require ){
    
    return {
        method: function( name ) {
            return function() {
                console.error( "Abstract method '" + name + "' in " + ( this.declaredClass ? this.declaredClass : this ) );
            }
        }
    }
    
});