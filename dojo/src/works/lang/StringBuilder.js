define([
    "dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojox/string/Builder"
], function( declare, lang, dojoxStringBuilder ){
    
    return declare( [], {
        constructor: function( startup ) {
            if( typeof startup == "number" ) {
                this.buffer = new Array( startup );
                this.pointer = 0;
            } else if( startup != undefined ){
                this.buffer = lang._toArray( lang.isString( startup ) ? startup : "" + startup );
                this.pointer = this.buffer.length;
            } else {
                this.buffer = new Array( 16 );
                this.pointer = 0;
            }
        }
        
        ,append: function( value ) {
            if( arguments.length > 1 ) {
                var params = lang._toArray( arguments ).slice( 1 );
                return this.appendString( lang.replace( value, params ) );
            } else {
                if( lang.isString( value ) ) {
                    return this.appendString( value );
                } else {
                    return this.appendString( "" + value );
                }
            }
        }
        
        ,appendString: function( string ) {
            this.reserve( string.length );

            var sarray = lang._toArray( string );
            for( var i = 0; i < sarray.length; i++ ) {
                this.buffer[ this.pointer ] = sarray[ i ];
                this.pointer++;
            }
            return this;
        }

        ,reserve: function( minLenght ) {
            if( minLenght > this.buffer.length - this.pointer ) {
                var newBuffer = new Array( this.buffer.length + Math.max( this.buffer.length, minLenght ) );
                for( var i = 0; i < this.buffer.length; i++ ) {
                    newBuffer[ i ] = this.buffer[ i ];
                }
                this.buffer = newBuffer;
            }
            
        }

        ,cut: function( number ) {
            if( typeof number == "number" ) {
                this.pointer -= number;
            }
            return this;
        }
        
        ,toString: function() {
            return this.buffer.slice( 0, this.pointer ).join( "" );
        }

    });


});