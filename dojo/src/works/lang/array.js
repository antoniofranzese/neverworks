define([
    "dojo/_base/array"
    ,"dojo/_base/lang"
], function( array, lang ){

    return lang.mixin({
        contains: function( arr, element ) {
            return( this.indexOf( arr, element ) >= 0 );
        },
        toArray: function( obj ) {
            result = [];
            for( var i in obj ) {
                result.push( obj[i] );
            }
            return result;
        }
    }, array );
    
})