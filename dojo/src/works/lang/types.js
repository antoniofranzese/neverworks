define([
], function(){
    
    var opts = Object.prototype.toString;
    
    var types = {

        isNumber: function( it ) {
            return typeof it == "number";
        }
        
        ,isNumberLike: function( it ) {
          return types.isNumber( it ) || ( !isNaN( parseFloat( it ) ) && isFinite( it ) );
        }

		,isString: function( it ){
			return (typeof it == "string" || it instanceof String); // Boolean
		}

		,isFunction: function( it ){
			return opts.call(it) === "[object Function]";
		}

		,isObject: function( it ){
			return it !== undefined &&
				(it === null || typeof it == "object" || types.isArray(it) || types.isFunction(it)); // Boolean
		}

		,isArray: function( it ){
			return it && (it instanceof Array || typeof it == "array"); // Boolean
		}

		,isArrayLike: function( it ){
			return it && it !== undefined && // Boolean
				// keep out built-in constructors (Number, String, ...) which have length
				// properties
				!types.isString(it) && !types.isFunction(it) &&
				!(it.tagName && it.tagName.toLowerCase() == 'form') &&
				(types.isArray(it) || isFinite(it.length));
		}
		
        ,isAlien: function( it ){
			// summary:
			//		Returns true if it is a built-in function or some other kind of
			//		oddball that *should* report as a function but doesn't
			return it && !types.isFunction(it) && /\{\s*\[native code\]\s*\}/.test(String(it)); // Boolean
		}

        ,toString: function( it ) {
            // Predisposizione per implementare meccanismi piu' complessi
            if( types.isString( it ) ) {
                return it
            } else if( it && it.toString() && types.isFunction( it.toString() ) ) {
                return it.toString()
            } else {
                return "" + it;
            }
        }
        
        ,toNumber: function( it ) {
            if( types.isNumber( it ) ) {
                return it;
            } else {
                if( types.isNumberLike( it ) ) {
                    return parseFloat( it );
                } else {
                    throw Exception( "Not a number: " + it );
                }
            } 
        }
    }
    
    return types;

});