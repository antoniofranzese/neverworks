define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"dojo/Stateful"
], function( req, declare, array, Stateful ){
    
    return declare( declare.className( req ), [ Stateful ], {
        
        constructor: function() {
            this.properties = [];
        }
        
        ,set: function( name, value ) {
            if( this.properties.indexOf( name ) < 0 ) {
                this.properties.push( name );
            }
            return this.inherited( arguments );
        }
        
        ,forEach: function( callback, context ) {
            var func = context ? lang.hitch( context, callback ) : callback;
            var self = this;
            array.forEach( this.properties, function( name ){
                func( name, self.get( name ) );
            }, context );
        }
        
    });
    
});