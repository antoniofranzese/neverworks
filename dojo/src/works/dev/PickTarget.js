define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/topic"
    ,"dojo/on"
    ,"dojo/query"
    ,"dojo/dom-construct"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
    ,"works/dev/PickManager"
], function( _require, declare, topic, on, query, domConstruct, domClass, domGeometry, domStyle, PickManager ){
    
    return declare( [], {
        constructor: function( widget, node ) {
            this.startSubscription = topic.subscribe( "works/pick/start", lang.hitch( this, this.startPicking ) );
            this.node = node;
            this.widget = widget;
        }

        ,destroy: function() {
            this.startSubscription.remove();
            delete this.startSubscription;
        }
        
        ,startPicking: function() {
            this.over = false;
            var box = PickManager.box( this.node );
            this.handlers = [
                topic.subscribe( "works/pick/fly", lang.hitch( this, function( evt ){

                    if( evt.x >= box.x1 && evt.x <= box.x2
                        && evt.y >= box.y1 && evt.y <= box.y2 ) {
                
                        if( ! this.over ) {
                            this.over = true;
                            PickManager.focus( this );
                        }
                    } else {
                        if( this.over ) {
                            this.over = false
                            PickManager.blur( this );
                        }
                    }
                }))
                ,topic.subscribe( "works/pick/stop", lang.hitch( this, this.stopPicking ) )
            ];
        }
        
        ,stopPicking: function() {
            PickManager.blur( this );

            if( this.handlers !== undefined ) {
                for( var i = 0; i < this.handlers.length; i++ ) {
                    this.handlers[ i ].remove();
                }
                delete this.handlers;
            }
        }
        
    });
    
});