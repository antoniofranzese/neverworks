define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/topic"
    ,"dojo/on"
    ,"dojo/query"
    ,"dojo/dom-construct"
    ,"dojo/dom-class"
    ,"dojo/dom-geometry"
    ,"dojo/dom-style"
], function( _require, declare, lang, topic, on, query, domConstruct, domClass, domGeometry, domStyle ){

    return declare([],{
    
        constructor: function( doc ) {
            topic.subscribe( "works/pick/start", lang.hitch( this, this.startPicking ) )
            topic.subscribe( "works/pick/stop", lang.hitch( this, this.stopPicking ) )
            this.document = doc;
        }

        ,startPicking: function( deferred ) {
            domClass.add( this.document.body, "worksPicking" );
            
            if( this.pickingNode === undefined ) {
                this.pickingNode = query( ".worksPickingBox" )[0];
                if( this.pickingNode === undefined ) {
                    this.pickingNode = domConstruct.create( "div", { "class": "worksPickingBox" } );
                    this.document.body.appendChild( this.pickingNode );
                }
            }

            if( this.closeNode === undefined ) {
                this.closeNode = query( ".worksPickingCloseBox" )[0];
                if( this.closeNode === undefined ) {
                    this.closeNode = domConstruct.create( "div", { "class": "worksPickingCloseBox", innerHTML: "X" } );
                    this.document.body.appendChild( this.closeNode );
                }
            }
            
            domStyle.set( this.closeNode, { visibility: "visible" } );

            this.handlers = [
                on( this.document, "mousemove", lang.hitch( this, function( evt ){ 
                    topic.publish( "works/pick/fly", { x: Math.floor( evt.clientX ), y: Math.floor( evt.clientY ) });
                }))
                ,on( this.closeNode, "click", lang.hitch( this, function( evt ){
                    deferred.reject({ message: "Stop" });
                    topic.publish( "works/pick/stop" );
                }))
                ,on( this.pickingNode, "click", lang.hitch( this, function( evt ){
                    evt.preventDefault();
                    deferred.resolve( this.focusTarget.widget.get( "developmentDescription" ) );
                    topic.publish( "works/pick/stop" );
                }))
            ];
            
        }
    
        ,stopPicking: function() {
            domClass.remove( this.document.body, "worksPicking" );

            if( this.closeNode ) {
                domStyle.set( this.closeNode, { visibility: "hidden" } );
            }
            
            if( this.handlers !== undefined ) {
                for( var i = 0; i < this.handlers.length; i++ ) {
                    this.handlers[ i ].remove();
                }
                delete this.handlers;
            }
        }
    
        ,focus: function( target ) {
            domStyle.set( this.pickingNode, { visibility: "visible" });
            var pos = domGeometry.position( target.node );
            var mb = domGeometry.getMarginBox( target.node );
        
            domGeometry.setMarginBox( this.pickingNode, {
                t: Math.floor( pos.y )
                ,l: Math.floor( pos.x )
                ,w: Math.ceil( mb.w )
                ,h: Math.ceil( mb.h )
            });
            
            this.focusTarget = target;
            this.focusNode = target.node;
        }
    
        ,blur: function( target ) {
            if( this.focusNode === target.node ) {
                delete this.focusNode;
                delete this.focusTarget;
                setTimeout( lang.hitch( this, function(){
                    if( this.focusNode === undefined ) {
                        domStyle.set( this.pickingNode, { visibility: "hidden" });
                    }
                }), 200 );
            }
        }
    
        ,candidate: function( widget ) {
        
        }
    
        ,box: function( node ) {
            var position = domGeometry.position( node );
            return {
                x1: position.x
                ,y1: position.y
                ,x2: position.x + position.w - 1
                ,y2: position.y + position.h - 1
            }
        }
    
    })( window.document );

});
