define([
    "dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/fx"
    ,"dojo/fx/easing"
    ,"dojo/on"
    ,"dojo/Deferred"
    ,"dojo/dom-construct"
    ,"dojo/dom-style"
    ,"works/form/Manager"
    ,"works/lang/strings"
    ,"works/app"
    
    ,"dojo/domReady!"
], function( declare, lang, fx, easing, on, Deferred, domConstruct, domStyle, FormManager, strings, app ){
    
    var Console = declare([],{
        constructor: function() {
            this.messages = [];
        }
        ,log: function() {
            this.messages.push( this._join( arguments ) );
            this._native( "log", arguments );
        }
        ,error: function() {
            this.messages.push( "[E] " + this._join( arguments ) );
            this._native( "error", arguments );
        }
        ,debug: function() {
            this.messages.push( "[D] " + this._join( arguments ) );
            this._native( "debug", arguments );
        }
        ,info: function() {
            this.messages.push( "[I] " + this._join( arguments ) );
            this._native( "info", arguments );
        }
        ,warn: function() {
            this.messages.push( "[W] " + this._join( arguments ) );
            this._native( "warn", arguments );
        }
        ,flush: function() {
            var m = this.messages;
            this.messages = [];
            return m;
        }
        ,_native: function( level, args ) {
            if( this._nativeConsole ) {
                this._nativeConsole[ level ].apply( this._nativeConsole, args );
            }
        }
        ,_join: function( args ) {
            var arr = [];
            for( var i = 0; i < args.length; i++ ) {
                arr.push( args[ i ] );
            }
            return arr.join( " " );
        }
    })
    
    var _registerWidget = FormManager.prototype.registerWidget;
    var _unregisterWidget = FormManager.prototype.unregisterWidget;
    
    lang.extend( FormManager, {
        
        __reindexWidgets: function() {
            if( this.__widgetQueue == undefined ) {
                this.__widgetQueue = [];
                this.inspectWidgets( lang.hitch( this, this.__indexWidget ) );
            }
        }
        
        ,__indexWidget: function( widget ) {
            if( widget.getParent() == this ) {
                this.__widgetQueue.push( widget.get( "name" ) );
                if( strings.hasText( widget.get( "canonicalName" ) ) ) {
                    this.__widgetQueue.push( widget.get( "canonicalName" ) );
                }
            }
        }
        
        ,registerWidget: function( widget ) {
            _registerWidget.apply( this, arguments );

            if( this.containsWidget( widget ) && widget.getParent() == this ) {
                this.__reindexWidgets();
                this.__indexWidget( widget );
            }
        }

        ,unregisterWidget: function( widget ) {
            var unregister = this.containsWidget( widget ) && widget.getParent() == this;
                
            _unregisterWidget.apply( this, arguments );
    
            if( unregister && this.__widgetQueue ) {
                var index = this.__widgetQueue.indexOf( widget.get( "name" ) );
                if( index >= 0 ) {
                    this.__widgetQueue = this.__widgetQueue.slice( 0, index ).concat( this.__widgetQueue.slice( index + 1 ) )
                }
                if( strings.hasText( widget.get( "canonicalName" ) ) ) {
                    index = this.__widgetQueue.indexOf( widget.get( "canonicalName" ) );
                    if( index >= 0 ) {
                        this.__widgetQueue = this.__widgetQueue.slice( 0, index ).concat( this.__widgetQueue.slice( index + 1 ) )
                    }
                }
            }
        }
        
    });
    
    var Environment = declare([], {
        
        constructor: function( params ) {
            declare.safeMixin( this, params );
            if( this.console ) {
                this.console._nativeConsole = window.console;
                window.console = this.console;
            }
            this.shotNode = domConstruct.create( "div", { 
                style: { 
                    position: "absolute"
                    ,height: "100%"
                    ,width: "100%"
                    ,top: "0"
                    ,left: "0"
                    ,backgroundColor: "white"
                    ,opacity: "0"
                    ,visibility: "hidden" 
                } 
            });
            window.document.body.appendChild( this.shotNode );
        }
        
        ,form: function( name ) {
            var frm = name ? widget( name ) : window.form;
            if( frm && frm.getActualForm ) {
                frm = frm.getActualForm();
            }
            if( frm ) {
                return { id: frm.get( "id" ), type: frm.get( "declaredClass" ) };
            } else {
                return { error: "Cannot locate " + ( name != null ? name : "root" ) + " form" };
            }
        }
        
        ,widget: function( id, name ) {
            var frm = widget( id );
            if( frm && frm.getActualForm ) {
                frm = frm.getActualForm();
            }
            if( frm ) {
                if( name.startsWith( "last:" ) ) {
                    var pattern = name.substring( 5 );
                    return this.last( frm, pattern );
                } else {
                    var wdg = frm.widget( name );
                    if( wdg ) {
                        return { id: wdg.get( "id" ), type: wdg.get( "declaredClass" ) };
                    } else {
                        return { error: "Cannot locate " + id + "." + name + " widget" };
                    }
                }
            } else {
                return { error: "Cannot locate " + id + " form" };
            }
        }
        
        ,last: function( frm, pattern ) {
            if( typeof frm == "string" ) {
                frm = widget( frm );
            }
            if( frm ) {
                frm.__reindexWidgets();
                var names = frm.__widgetQueue;

                for( var i = names.length - 1; i >= 0; i-- ) {
                    if( names[ i ].match( pattern ) != null ) {
                        var wdg = frm.widget( names[ i ] );
                        return { id: wdg.get( "id" ), type: wdg.get( "declaredClass" ) };
                    }
                }
                return { error: "Cannot find last widget for expression '" + pattern + "'" };
                
            } else {
                return { error: "Cannot locate " + id + " form" };
            }
        }
        
        ,contains: function( id, other ) {
            var frm = widget( id );
            if( frm && frm.getActualForm ) {
                frm = frm.getActualForm();
            }
            if( frm ) {
                var otherWidget = widget( other );
                if( otherWidget == undefined ) {
                    otherWidget = frm.widget( other );
                }
                if( otherWidget != undefined ) {
                    return { contained: frm.containsWidget( otherWidget ) }
                } else {
                    return { contained: false }
                }
                    
            } else {
                return { error: "Cannot locate " + id + " form" };
            }
        }
        
        ,exists: function( id ) {
            return { exists: ( widget( id ) != undefined ) }
        }
        
        ,get: function( id, path ) {
            path = ( lang.isArray( path ) ? path : [ path ] ).reverse();
            var wdg = widget( id );
            if( wdg ) {
                var value, type;
                while( wdg && path.length > 0 ) {
                    var name = path.pop();
                    var accessor = typeof name == "number" ? "_getIndexedItem" : "_get" + name.substr( 0, 1 ).toUpperCase() + name.substr( 1 ) + "Property";
                    value = lang.isFunction( wdg[ accessor ] ) ? wdg[ accessor ]() : wdg.get( name );
                    
                    if( path.length > 0 ) {
                        wdg = value;
                    }
                    
                }
                var result = { value: value };

                if( wdg && wdg[ name + "$type" ] ) {
                    result.type = wdg[ name + "$type" ];
                }
                return result;

            } else {
                return { error: "Cannot locate " + id + " widget" };
            }
        }

        ,set: function( id, name, value ) {
            var d = new Deferred();
            this.async = false;
            
            var start = on.once( form.parentPane, "task-start", lang.hitch( this, function(){
                this.async = true;
            }));
            var end = on.once( form.parentPane, "task-end", lang.hitch( this, function(){
                delete this.async;
                d.resolve({ status: "ok", async: true });
            }));
            
            var wdg = widget( id );
            if( wdg != null ) {
                setTimeout( lang.hitch( this, function(){
                    var accessor = typeof name == "number" ? "_setIndexedItem" : "_set" + name.substr( 0, 1 ).toUpperCase() + name.substr( 1 ) + "Property";
                    if( lang.isFunction( wdg[ accessor ] ) ) {
                        wdg[ accessor ]( value );
                    } else {
                        wdg.set( name, value );
                    }
                    setTimeout( lang.hitch( this, function(){
                        if( !( this.async ) ) {
                            start.remove();
                            end.remove();
                            delete this.async;
                            d.resolve({ status: "ok", async: false });
                        }
                    }), 0 );
                }), 0 );
            } else {
                start.remove();
                end.remove();
                delete this.async;
                d.resolve({ error: "Cannot locate " + id + " widget" });
            }

            return d;
        }
        
        ,fire: function( id, name, args ) {
            var d = new Deferred();
            this.async = false;
            var start = on.once( form.parentPane, "task-start", lang.hitch( this, function(){
                this.async = true;
            }));
            var end = on.once( form.parentPane, "task-end", lang.hitch( this, function(){
                delete this.async;
                d.resolve({ status: "ok", async: true });
            }));
            
            var wdg = widget( id );
            if( wdg != null ) {
                setTimeout( lang.hitch( this, function(){
                    var accessor = "_fire" + name.substr( 0, 1 ).toUpperCase() + name.substr( 1 ) + "Event";
                    if( lang.isFunction( wdg[ accessor ] ) ) {
                        wdg[ accessor ]( args );
                    } else {
                        wdg.emit( name, { state: args, widget: wdg, bubbles: false } );
                    }
                    
                    setTimeout( lang.hitch( this, function(){
                        if( !( this.async ) ) {
                            start.remove();
                            end.remove();
                            delete this.async;
                            d.resolve({ status: "ok", async: false });
                        }
                    }))

                }));
            } else {
                start.remove();
                end.remove();
                delete this.async;
                d.resolve({ error: "Cannot locate " + id + " widget" });
            }
            
            return d;
        }
        
        ,sync: function() {
            return {
                messages: this.console.flush()
            }
        }
        
        ,size: function() {
            return {
                width: document.body.offsetWidth
                ,height: document.body.offsetHeight
            }
        }
        
        ,shoot: function() {
            var d = new Deferred();
            var node = this.shotNode;
            domStyle.set( node, { opacity: 1, visibility: "visible" } );
            
			new fx.Animation({
				duration: 1000
				,curve: new fx._Line( 100, 0 )
				,easing: easing.sineOut

				,onAnimate: function( val ) {
                    domStyle.set( node, { opacity: val / 100 } );
				}

				,onEnd: function() {
                    domStyle.set( node, { visibility: "hidden" } );
                    d.resolve();
				}

			}).play();
            return d;
        }

    });
    
    var console = new Console();
    
    var test = new Environment({
        app: app
        ,console: console
    })
    
    app.then( function(){
        //TODO: cercare via DOM
        test.mainform = window.form;
    });
    
    return test;
})