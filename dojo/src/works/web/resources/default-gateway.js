{
    startup: function() {
    
    }

    ,get: function( name ) {
        if( this.properties && this.properties[ name ] && this.properties[ name ].get ) {
            return this.properties[ name ].get( name );
        } else {
            return undefined;
        }
    }

    ,set: function( name, value ) {
        if( this.properties && this.properties[ name ] && this.properties[ name ].set ) {
            var setter = this.properties[ name ].set;
            if( setter.length < 2 ) {
                return setter( value );
            } else {
                return setter( name, value );
            }
        } else {
            return undefined;
        }
    }

    ,callback: function() {
        
    }

    ,errback: function() {
        
    }

    ,emit: function( evt, params ) {
        
    }

    ,register: function( params ) {
        params = params || {};
        if( params.property ) {
            if( this.properties == undefined ) {
                this.properties = {};
            }
        
            if( this.properties[ params.property ] === undefined ) {
                this.properties[ params.property ] = {}
            }
            
            if( params.get ) {
                if( typeof params.get == "function" ) {
                    this.properties[ params.property ].get = params.get;

                } else {
                    this.properties[ params.property ].get = new Function( "" + params.get );
                }
            }

            if( params.set ) {
                if( typeof params.set == "function" ) {
                    this.properties[ params.property ].set = params.set;

                } else {
                    this.properties[ params.property ].set = new Function( "" + params.set );
                }
            }
            
        } else if( params.event ) {
            if( this.events === undefined ) {
                this.events = {};
            }
            
            if( this.events[ params.event ] === undefined ) {
                this.events[ params.event ] = [] 
            }

            if( params.handler !== undefined && typeof params.handler == "function" ) {
                this.events[ params.event ].push( params.handler );

            } else if( params.code !== undefined ){
                this.events[ params.event ].push( new Function( "" + params.code ) );
            }

        } else if( params.request ) {
            if( this.requests === undefined ) {
                this.requests = {};
            }
            
            if( params.handler !== undefined && typeof params.handler == "function" ) {
                this.requests[ params.request ] = params.handler;

            } else if( params.code !== undefined ){
                this.requests[ params.request ] = new Function( "" + params.code );
            }
        }
    }
    
    ,notify: function( name, state ) {
        if( this.events !== undefined && this.events[ name ] !== undefined ) {
            this.events[ name ].forEach( function( handler ){
                handler( state );
            });
        }
    }

    ,request: function( name, state, id ) {
        if( this.requests !== undefined && this.requests[ name ] !== undefined ) {
            var self = this;
            var callback = function( result ) {
                self.callback( id, result );
            }
            this.requests[ name ]( callback, state );
        }
    }
    
    ,wait: function() {
        if( this.ready === undefined ) {
            this.ready = {
                listeners: []
                ,resolved: false
                ,then: function( listener ) {
                    if( this.resolved ) {
                        listener();
                    } else {
                        this.listeners.push( listener );
                    }
                }
                ,resolve: function() {
                    if( ! this.resolved ) {
                        this.resolved = true;
                        for( var i = 0; i < this.listeners.length; i++ ) {
                            this.listeners[ i ]();
                        }
                    }
                }
            }
        }
    }
}