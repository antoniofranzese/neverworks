define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/dom-class"
    ,"dojox/form/Uploader"
    ,"works/form/_WidgetMixin"
], function( _require, declare, lang, on, domClass, Uploader, _WidgetMixin ){
    
    var widgetClass = declare( [ Uploader, _WidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
		,uploadOnSelect: true
        
        ,joinForm: function(){
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
            this.announceEvent( "begun",{ rename: "begin" } );
        }

        ,leaveForm: function() {
            this.abandonForm();
            this.inherited( arguments );
        }

        ,startup: function() {
            this.inherited( arguments );
            this.own(
                on( this, "begin", lang.hitch( this, this.handleBegin ) )
                ,on( this, "complete", lang.hitch( this, this.handleUploaded ) )
            );
        }

        ,handleBegin: function( data ) {
            //console.log( "Begin", data );
            this.emitEvent( "begun", { state: {} } );
        }
        
        ,handleUploaded: function( evt ) {
            if( "error" in evt ) {
                delete this.files
                alert( evt.error );
            } else {
                //console.log( "Uploaded", evt.uploaded );
                this.emitEvent( "changed", { state: {
                    files: evt.uploaded ? evt.uploaded : null
                }});
            }
        }
        
        ,getWidgetState: function() {
            return undefined;
        }
        
        ,_setVisibleAttr: function( value ) {
            domClass[ value == true ? "remove" : "add" ]( this.domNode, "hidden" );
            this._set( "visible", value );
        }
        
    });
    
    return widgetClass;
});