define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"dojo/on"
    ,"dojo/has"
    ,"dojo/aspect"
    ,"dojo/topic"
    ,"dojo/query"
    ,"dojo/dom-construct"
    ,"dojo/dom-style"
    ,"dojo/dom-geometry"
    ,"dojo/dom-attr"
    ,"works/wait"
    ,"works/channels"
    ,"works/messages"
    ,"works/lang/strings"
    ,"dojo/Deferred"
    ,"dijit/layout/ContentPane"
    ,"works/form/_FormWidgetMixin"
    ,"works/form/_WidgetMixin"
    ,"works/layout/_FixedSizeMixin"
    ,"works/layout/_ParentQualifyMixin"
    ,"works/layout/_DisplacedNodeMixin"
    ,"dojo/text!./resources/default-gateway.js"

], function( req, declare, array, on, has, aspect, topic, query, domConstruct, domStyle, domGeometry, domAttr, wait, channels, messages, strings, 
    Deferred, ContentPane, _FormWidgetMixin, _WidgetMixin, _FixedSizeMixin, _ParentQualifyMixin, _DisplacedNodeMixin, DefaultGateway ) {
    
    var RESERVED_PROPERTIES = [ "location" ];
    var RESERVED_EVENTS = [ "init", "startup", "ready", "show", "hide", "resize" ];
    var LOCAL_PROPERTIES = [ "view.origin", "view.size" ];
        
    var WebView = declare( declare.className( req ), [ ContentPane, _WidgetMixin, _FormWidgetMixin, _FixedSizeMixin, _ParentQualifyMixin, _DisplacedNodeMixin ], {

        baseClass: "worksWebView"
        ,loader: false
        ,avoidFirstLoad: false
        ,accessible: true
        ,familiar: false
        ,waitForReady: false
        ,wasShown: false
        ,log: false
        
        ,handleChannelMessage: function( topic, value, channel ) {
            messages.send( this.get( "window" ), "neverworks.channel.Send", {
                channel: channel
                ,topic: topic
                ,value: value
            });
        }
        
        ,joinForm: function( form ) {
            this.inherited( arguments );
            this.announceEvent( "loaded", { rename: "load" } );
            this.announceEvent( "event" );
            this.announceEvent( "callback" );
            this.announceEvent( "errback" );
            this.form = form;
        }

        ,leaveForm: function() {
            this.abandonForm();
            delete this.form;
            if( this.displaced ) {
                domConstruct.destroy( this.frame );
                delete this.frame;
                delete this.displacedNode;
            }
            this.inherited( arguments );
        }
        
        ,startup: function() {
            this.inherited( arguments );

            this.own(
                channels.attach( this )
                ,messages.on( "neverworks.app.Loading", lang.hitch( this, this.onFamiliarAppLoading ) )
                ,messages.on( "neverworks.app.Ready", lang.hitch( this, this.onFamiliarAppReady ) )
            );

            this.displacedNodeContainer = this.domNode;
            
            if( this.frame == undefined ) {
                this.displacedNode = this.frame = domConstruct.create( "iframe", { 
                    id: this.id + "_frame" 
                    ,style: this.displaced ? this.getDisplacedNodeStyle( "iframe" ) : undefined
                    //, onload: "parent.widget('" + this.id + "').notifyLoad( this )"
                    //, onunload: "parent.widget('" + this.id + "').notifyUnload( this )"
                    //, src:"about:blank" 
                }, this.displaced ? document.body : this.domNode );
                
                domStyle.set( this.frame, {
                    borderWidth: 0 
                });
                
                if( this.loader ) {
                    this.loaderNode = domConstruct.create( "div", { 
                        id: this.id + "_frame" 
                        ,"class": "worksLoader"
                    }, this.containerNode );
                    
                    domStyle.set( this.containerNode, { position: "relative" } );
                }
                
                this.resize();
                
                if( ! has( "safari" ) ) {
                    this.onLoadSubscription = on( this.frame, "load", lang.hitch( this, this.onFrameLoad ) );
                }
                
                if( this.startupLocation ) {
                    if( this.loader ) {
                        this.showLoader();
                    }
                    this.set( "location", this.startupLocation + ( this.startupBookmark ? "#" + this.startupBookmark : "" ) );
                } else {
                    if( has( "ff" ) && this.startupContent === undefined ){
                        this.avoidFirstLoad = true;
                    }
                    
                    if( this.startupContent ) {
                        this.set( "bodyContent", this.startupContent );
                        delete this.startupContent;
                    }       
                    
                    if( this.startupBookmark ) {
                        this.set( "bookmark", this.startupBookmark );
                    }
                }
                
    
            } else {
                this.displacedNode = this.frame;
            }
        }
        
        ,onFamiliarAppLoading: function( msg, evt ) {
            if( evt.source === this.get( "window" ) ) {
                this.waitForReady = true;
                this.familiar = true;
                if( this.log ) console.log( "Familiar client app is loading" );
            }
        }
        
        ,onFamiliarAppReady: function( msg, evt ) {
            if( evt.source === this.get( "window" ) && this.waitForReady ) {
                this.waitForReady = false;
                if( this.log ) console.log( "Familiar client app is ready" );
                this.onLoad();
            }
        }

        ,onFrameLoad: function() {
            if( this.avoidFirstLoad ) {
                this.avoidFirstLoad = false;
            } else {
                var win = this.get( "window" );
                var href = "";
                try {
                    href = win.location.href;
                    this.accessible = true;
                } catch( ex ) {
                    this.accessible = false;
                }

                if( href != "about:blank" ) {
                    if( this.accessible ) {
                        if( this.log ) console.log( "Accessible page loaded" );
                        setTimeout( lang.hitch( this, function(){
                            this.ensureGateway( win );
                            this.connectGateway( win ).then( lang.hitch( this, this.onLoad ) );
                        }), 0 );
                    } else {
                        delete this.gateway;
                        if( this.log ) console.log( "Unaccessible page loaded" );
                        setTimeout( lang.hitch( this, this.onLoad ), 0 );
                    }
            
                    if( has( "safari" ) ) {
                        win.addEventListener( 'beforeunload', lang.hitch( this, function(){
                            var gateway = this.gateway;
                            this.onFrameUnload();
                            if( gateway && gateway.__LOAD_REQUESTED__ == false ) {
                                this.monitorLoadState().then( lang.hitch( this, this.onFrameLoad ) );
                            }
                        }));

                    } else {
                        if( this.onUnloadSubscription ) {
                            if( this.accessible ) {
                                this.onUnloadSubscription.remove();
                            }
                            delete this.onUnloadSubscription;
                        }
                        if( this.accessible ) {
                            this.onUnloadSubscription = on( this.get( "window" ), "unload", lang.hitch( this, this.onFrameUnload ) );
                        }
                    }
            
                    this.hideLoader();
                } else {
                    setTimeout( lang.hitch( this, this.reload ), 0 );
                }
            }
        }
        
        ,onLoad: function( ) {
            if( ! this.waitForReady ) {
                messages.send( this.get( "window" ), "neverworks.channel.Attach" );

                if( this.log ) console.log( "Signaling " + ( this.familiar ? "familiar" : "custom" ) + " app load" );
                this.emitAlways( "loaded", { state: {
                    location: this.accessible ? this.get( "location" ) : null
                    ,accessible: this.accessible
                    ,familiar: this.familiar
                }});
            } else {
                if( this.log ) console.log( "Waiting for familiar app ready" );
            }
        }
        
        ,ensureGateway: function( win ) {
            if( win[ "neverworks" ] == undefined ) {
                win.eval( "window.neverworks = {}" );
            }
            
            if( win.neverworks.gateway == undefined ) {
                win.eval( "window.neverworks.gateway = " + DefaultGateway );
            }
            
           // console.log( "Connecting", win.location.href, "with gateway", win.neverworks );
            
        }
        
        ,onFrameUnload: function() {
            if( this.log ) console.log( "Unloaded" );
            this.disconnectGateway();
            this.waitForReady = false;
            this.familiar = false;
        }
        
        ,connectGateway: function( win ) {
            var self = this;
            var d = new Deferred();
            d.then( function() {
                self.gateway.notify( "ready" );
            });
            
            this.gateway = win.neverworks.gateway;
            this.gateway.__LOAD_REQUESTED__ = false;
            this.gatewayEvents = [];
            this.gatewayProperties = [];

            this.gatewayHandlers = [
                 aspect.after( this.gateway, "emit",     hitch( this, this.handleEvent ),     /* receiveArguments */ true )
                ,aspect.after( this.gateway, "callback", hitch( this, this.handleCallback ),  /* receiveArguments */ true )
                ,aspect.after( this.gateway, "errback",  hitch( this, this.handleErrback ),   /* receiveArguments */ true )
                ,aspect.after( this.gateway, "register", hitch( this, this.registerFeature ), /* receiveArguments */ true )
            ]
            
            this.populateGateway();

            var commonPreamble = function( node ) {
                var preamble = "\n";
                
                var gatewaySpec = domAttr.get( node, "gateway" ) || domAttr.get( node, "gw" );
                if( strings.hasText( gatewaySpec ) ) {
                    preamble += "var " + gatewaySpec.replaceAll( " ", "" ) + " = window.neverworks.gateway;\n";
                }
                
                return preamble;
            }

            query( "script[type='neverworks/get']", win.document ).forEach( function( node ){
                var name = domAttr.get( node, "property" );
                
                if( strings.hasText( name ) ) {
                    var code = node.innerHTML;
                    var preamble = commonPreamble( node );

                    if( strings.hasText( code ) ) {
                        self.gateway.register({
                            property: name
                            ,get: preamble + code
                         });
                    }
                }
            });
            
            query( "script[type='neverworks/set']", win.document ).forEach( function( node ){
                var name = domAttr.get( node, "property" );
                if( strings.hasText( name ) ) {
                    var code = node.innerHTML;
                    var preamble = commonPreamble( node );
                    
                    var valueSpec = domAttr.get( node, "value" ) ;
                    if( strings.hasText( valueSpec ) ) {
                        preamble += "var " + valueSpec.replaceAll( " ", "" ) + " = arguments[ 0 ];\n";
                    }
                    
                    if( strings.hasText( code ) ) {
                        self.gateway.register({
                            property: name
                            ,set: preamble + code
                         });
                    }
                }
            });

            query( "script[type='neverworks/connect']", win.document ).forEach( function( node ){
                var name, type;
                if( domAttr.has( node, "request" ) ) {
                    name = domAttr.get( node, "request" );
                    type = "request";
                } else if( domAttr.has( node, "event" ) ) {
                    name = domAttr.get( node, "event" );
                    type = "event";
                }

                if( strings.hasText( name ) ) {
                    var code = node.innerHTML;
                    var preamble = commonPreamble( node );
                    
                    var argsSpec = domAttr.get( node, "arguments" ) || domAttr.get( node, "args" );
                    if( strings.hasText( argsSpec ) ) {
                        var args = argsSpec.replaceAll( " ", "" ).split( "," );
                        for( var a = 0; a < args.length; a++ ) {
                            preamble += "var " + args[ a ] + " = arguments[" + a + "];\n";
                        }
                    }
                    
                    if( strings.hasText( code ) ) {
                        var registration = {
                            code: preamble + code
                        }
                        registration[ type ] = name;
                        self.gateway.register( registration );
                    }
                }
            });
            
            var init = win.document.body.getAttribute( "data-works-init" ) || win.document.body.getAttribute( "data-works-onload" );
            if( init ) {
                if( this.log ) console.log( "Init by body attribute" );
            
                win.eval( init );
        
            } else if( lang.isFunction( this.gateway.init ) ) {
                if( this.log ) console.log( "Init by gateway method" );
                this.gateway.init();
                
            }

            this.gateway.notify( "init" );

            if( this.gateway.ready ) {
                if( lang.isFunction( this.gateway.ready.then ) || lang.isFunction( this.gateway.ready.done ) ) {
                    if( this.log ) console.log( "Deferred gateway startup" );
                    var method = lang.isFunction( this.gateway.ready.then ) ? "then" : "done";
                    this.gateway.ready[ method ]( lang.hitch( this, function(){ 
                        this.startupGateway();
                        d.resolve();
                    }));
                } else {
                    console.error( "Invalid gateway ready deferred:", this.gateway.ready );
                    this.startupGateway();
                    d.resolve();
                }
            } else {
                if( this.log ) console.log( "Immediate gateway startup" );
                this.startupGateway();
                d.resolve();
            }

            return d;
        }

        ,populateGateway: function() {
            var self = this;
            
            this.gateway.register({ 
                property: "view.origin"
                ,local: true
                ,get: function() {
                    var pos = domGeometry.position( self.domNode );
                    return { x: pos.x, y: pos.y } ;
                } 
            });

            this.gateway.register({ 
                property: "view.size"
                ,local: true
                ,get: function() {
                    var pos = domGeometry.position( self.frame );
                    return { w: pos.w, h: pos.h } ;
                } 
            });
        }
        
        ,startupGateway: function() {
            
            if( lang.isObject( this.initProperties ) ) {
                array.forEach( Object.keys( this.initProperties ), function( name ) {
                    this.gateway.set( name, this.initProperties[ name ] );
                }, this );
                delete this.initProperties;
            }
            
            if( lang.isFunction( this.gateway.startup ) ) {
                this.gateway.startup();
            }

            this.gateway.notify( "startup" );
            
            if( this.startupQueue !== undefined ) {
                this.set( "executionQueue", this.startupQueue );
                delete this.startupQueue;
            }
            
            if( this.log ) console.log( "Gateway started" );
            
            if( this.isShown() ) {
                this.gateway.notify( "resize" );
                this.notifyShow( /* shown */ true );
            }
            
        }
        
        ,registerFeature: function( params ) {
            if( params.property ) {
                var propertyName = "" + params.property;
                if( RESERVED_PROPERTIES.indexOf( propertyName ) < 0 ) {
                    this.gatewayProperties.push( propertyName );
                } else {
                    throw new Error( "Reserved property: " + propertyName );
                }
            }
        }

        ,disconnectGateway: function( gateway ) {
           // console.log( "Disconnecting", this.get( "location" ), "gateway" );
            if( this.gateway != undefined ) {
                array.forEach( this.gatewayEvents, function( event ){
                    this.retireEvent( event );
                }, this );
                
                array.forEach( this.gatewayHandlers, function( handler){
                    handler.remove();
                })

                delete this.gateway;
                delete this.gatewayEvents;
                delete this.gatewayHandlers;
            }
        }
        
        ,handleEvent: function( name, result ) {
           //console.log( "Catch", name );
            this.emitAlways( "event", { state: { name: name, state: result } });
        }
        
        ,handleCallback: function( id, result ) {
           //console.log( "Callback", id );
            this.emitAlways( "callback", { state: { id: id, result: result } });
        }

        ,handleErrback: function( id, result ) {
           //console.log( "Errback", id );
            this.emitAlways( "errback", { state: { id: id, result: result } });
        }
        
        ,showLoader: function() {
            if( this.loaderNode ) {
                domStyle.set( this.loaderNode, { visibility: "visible" } );
            }
        }

        ,hideLoader: function() {
            if( this.loaderNode ) {
                domStyle.set( this.loaderNode, { visibility: "hidden" } );
            }
        }

        ,cellResizing: function() {
            return this.containerNode;
        }

        ,cellResize: function( size ) {
            this.resize( size );
        }
        
        ,resize: function( size ) {
            var fw = domStyle.get( this.frame, "width" ), 
                fh = domStyle.get( this.frame, "height" );
            
            if( size === undefined ) {
                //console.log( "FW", this.fixedWidth, "FH", this.fixedHeight );
                domStyle.set( this.containerNode,{
                    height: this.fixedHeight || "100%"
                    ,width: this.fixedWidth || "100%"
                } );
            }
        
            this.inherited( arguments );

            this.placeDisplacedNode();
        
            var cs = domStyle.getComputedStyle( this.containerNode );

            domStyle.set( this.frame,{
                height: cs.height
                ,width: cs.width
            } );

            if( fw != domStyle.get( this.frame, "width" ) 
                || fh != domStyle.get( this.frame, "height" ) ) {
                    
                if( this.isShown() && this.gateway ) {
                    this.gateway.notify( "resize" );
                    this.notifyShow( /* shown */ true );
                }
            
            }
        }
        
        ,isShown: function() {
            return domStyle.isShown( this.domNode );
        }

        ,notifyMessage: function( message ) {
            this.inherited( arguments );
            if( message.topic == "widget/container/show" ) {
                this.notifyShow();
            } else if( message.topic == "widget/container/hide" ) {
                this.notifyHide();
            }
        }

        ,notifyShow: function( shown ) {
            if( ( shown || this.isShown() ) && this.gateway && ! this.wasShown ) {
                this.wasShown = true;
                this.gateway.notify( "show" );
            }
        }

        ,notifyHide: function() {
            if( this.gateway && this.wasShown ) {
                this.wasShown = false;
                this.gateway.notify( "hide" );
            }
        }

        ,load: function( params ) {
            if( params.href ) {
                if( this.gateway ) {
                    this.gateway.__LOAD_REQUESTED__ = true;
                }

                this.get( "window" ).location.href = params.href;
                if( params.href != "about:blank" ) {
                    this.lastLoadParams = params;
                } else {
                    delete this.lastLoadParams;
                }

                if( has( "safari" ) ) {
                    this.monitorLoadState().then( lang.hitch( this, this.onFrameLoad ) );
                }
            }
        }
        
        ,reload: function() {
            if( this.lastLoadParams ) {
                this.load( this.lastLoadParams );
            }
        }

        ,monitorLoadState: function() {
            var d = new Deferred();
            var win = this.frame;
            var old = win.contentDocument || win.contentWindow;
            wait( function(){
                var doc = win.contentDocument || win.contentWindow;
                //console.log( "doc", doc.readyState, doc === old );
                return doc !== old && doc.readyState == 'complete';
            }).then( lang.hitch( this, function(){
                d.resolve();
            }));
            return d;
        }

        ,_setBodyContentAttr: function( value ) {
            if( this.accessible ) {
                var doc = this.get( "window" ).document;
                doc.open();
                doc.write( value );
                doc.close();
            }
        }
        
        ,_getWindowAttr: function() {
            return this.frame.contentWindow;
        }
        
        ,_getLocationAttr: function() {
            try {
                var loc = this.get( "window" ).location;
                return loc.protocol + "//" + loc.host + loc.pathname;
            } catch( ex ) {
                return undefined;
            }
        }
        
        ,_setLocationAttr: function( value ) {
            if( this.frame != null ) {
                this.load({ href: value });
            } else {
                this.startupLocation = value;
            }
        }
        
        ,_setBookmarkAttr: function( value ) {
            var win = this.get( "window" );
            win.location.hash = null;
            win.location.hash = value;
        }
        
        ,_getHistoryLength: function() {
            if( this.frame != null ) {
                return this.get( "window" ).history.length;
            } else {
                return 0;
            }
        }
        
        ,_setExecutionQueueAttr: function( value ) {
            var win = this.get( "window" );
            array.forEach( value, function( stmt ){
                if( stmt.exec == "event" ) {
                    if( RESERVED_EVENTS.indexOf( stmt.name ) < 0 ) {
                        this.gateway && this.gateway.notify( stmt.name, stmt.state );
                    } else {
                        console.error( "Cannot fire reserved event: " + stmt.name );
                    }
                    
                } else if( stmt.exec == "request" ) {
                    if( this.gateway ) {
                        var self = this;
                        setTimeout( function() {
                            self.gateway.request( stmt.name, stmt.state, stmt.id );
                        }, 0 );
                    }
                
                } else if( stmt.exec == "async" ) {
                   //console.log( "Schedule", stmt );
                    try {
                        var code = [
                            "(function(){ ",
                            "var callback = function( result ){ neverworks.gateway.callback( '" + stmt.id + "', result ); }; ",                            
                            "var errback = function( result ){ neverworks.gateway.errback( '" + stmt.id + "', result ); }; ",                            
                            stmt.js + ";",
                            "})();"
                        ].join( "" );
                            
                        win.eval( code );
                    } catch( ex ) {
                        this.handleErrback( stmt.id, { message: ex.message } );
                    }
                    
                } else if( stmt.exec == "sync" ){
                    win.eval( stmt.js );
                }
            }, this );
        }
        
        ,getWidgetState: function() {
            var pos = domGeometry.position( this.containerNode );
            
            var state = { 
                location: this.get( "location" ) 
                ,accessible: this.accessible
                ,familiar: this.familiar
            }

            if( this.gateway ) {
                for( var i = 0; i < this.gatewayProperties.length; i++ ) {
                    try {
                        var property = this.gatewayProperties[ i ];
                        if( LOCAL_PROPERTIES.indexOf( property ) < 0 ) {
                            state[ property ] = this.gateway.get( property );
                        }
                    } catch( ex ){
                        console.error( "Error getting " + this.gatewayProperties[ i ] + ": " + ex );
                    }
                }
            }
            
            return state;
        }
        
        ,setWidgetState: function( value ) {
            if( value.executionQueue ) {
                this.set( "executionQueue", value.executionQueue );
                delete value.executionQueue;
            }

            if( value.location ) {
                this.set( "location", value.location + ( value.bookmark ? "#" + value.bookmark : "" ) );
                delete value.location;
                on.once( this, "loaded", lang.hitch( this, function() {
                    this.setGatewayState( value );
                }));
            } else if( value.bodyContent ) {
                this.set( "bodyContent", value.bodyContent );
                delete value.bodyContent;
                on.once( this, "loaded", lang.hitch( this, function() {
                    this.setGatewayState( value );
                }));
            } else {
                if( value.bookmark ) {
                    this.set( "bookmark", value.bookmark );
                    delete value.bookmark;
                }
                this.setGatewayState( value )
            }
        }
        
        ,setGatewayState: function( state ) {
            if( this.gateway ) {
                for( var name in state ) {
                    this.gateway.set( name, state[ name ] );
                }
            }
        }
    
    });
    
    WebView.markupFactory = _WidgetMixin.markupFactory;
    return WebView;
    
});
