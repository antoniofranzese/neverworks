define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/dom-construct"
    ,"dojo/on"
    ,"dojo/has"
    ,"works/lang/strings"
    ,"dijit/_WidgetBase"
    ,"works/form/_WidgetMixin"
    ,"works/form/_FormWidgetMixin"
], function( _require, declare, lang, domConstruct, on, has, strings, _WidgetBase, _WidgetMixin, _FormWidgetMixin ){
    
    var widgetClass = declare( [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {
        declaredClass: _WidgetMixin.inferDeclaredClass( _require )
        
        ,protocol: null
        
        ,buildRendering: function() {
            this.inherited( arguments );
            this.frame = domConstruct.create( "iframe", { 
                id: this.id + "_frame"
            }, this.domNode );
        }
        
        ,joinForm: function( form ){
            this.inherited( arguments );
            this.announceEvent( "send" );
            this.form = form;
        }

        ,leaveForm: function() {
            this.retireEvent( "send" );
            this.form = undefined;
            this.inherited( arguments );
        }

        ,download: function( url ) {
            if( url ) {
                var win = this.frame.contentWindow;
                var protocol = "";
                if( strings.hasText( this.protocol ) ) {
                    protocol = this.protocol + ":";
                } else if( has( "mobile" ) ) {
                    protocol = "download:";
                }
                win.location.replace( protocol + url );
            }
        }
        
        ,getWidgetState: function() {
            return undefined;
        }
        
        ,setWidgetState: function( value ) {
            if( value.protocol !== undefined ) {
                this.protocol = value.protocol;
            }
            if( value.source !== undefined ) {
                this.download( value.source );
            }
        }
    });
    
    return widgetClass;
});