define([
    "require"
    ,"works/lang/declare"
	,"dojo/string"
    ,"dojo/has"
    ,"./WebView"
    ,"dojo/text!./resources/PDFViewer.html"
], function( req, declare, string, has, WebView, template ){
    
    return declare( declare.className( req ), [ WebView ], {

        joinForm: function( form ) {
            this.inherited( arguments );
            form.ready().then( lang.hitch( this, function(){
                if( this.startupSource ) {
                    this.set( "source", this.startupSource );
                    delete this.startupSource;
                }
            }));
        }
        
        ,getWidgetState: function() {
            return undefined;
        }
        
        ,setWidgetState: function( value ) {
            if( value.source !== undefined ) {
                this.set( "source", value.source );
            }
        }
        
        ,_setSourceAttr: function( value ) {
            if( has( "ff" ) ) {
                this.set( "bodyContent", value ? string.substitute( template, { source: value } ) : "");
            } else {
                this.set( "location", value ? value : "about:blank" );
            }
        }

        ,onLoad: function( state ) {

        }

    });
    
});