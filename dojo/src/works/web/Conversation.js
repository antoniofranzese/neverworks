define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/array"
    ,"works/json"
    ,"works/channels"
    ,"dijit/_WidgetBase"
    ,"works/form/_WidgetMixin"
    ,"works/form/_FormWidgetMixin"
], function( req, declare, array, json, channels, _WidgetBase, _WidgetMixin, _FormWidgetMixin ){
    
    return declare( declare.className( req ), [ _WidgetBase, _WidgetMixin, _FormWidgetMixin ], {
        
        changed: false
        
        ,joinForm: function( form ){
            this.inherited( arguments );
            this.announceEvent( "changed", { rename: "change" } );
            this.announceEvent( "message" );
            this.form = form;
            
            if( this.startupChannel ) {
                this.joinChannel( this.startupChannel );
                delete this.startupChannel;
            }
            
            if( this.startupState ) {
                var state = json.parse( this.startupState );
                if( state && this.channel ) {
                    channels.channel( this.channel ).write( state, /* priority */ true );
                }
                delete this.startupState;
            }
        }

        ,leaveForm: function() {
            this.joinChannel( undefined );
            this.retireEvent( "message" );
            this.retireEvent( "changed" );
            this.form = undefined;
            this.inherited( arguments );
        }

        ,getWidgetState: function() {
            if( this.pendingChange ) {
                this.pendingChange = false;
                return {
                    value: json.stringify( this.value )
                }
            } else {
                return undefined;
            }
        }
        
        ,setWidgetState: function( state ) {
            
            if( state.channel !== undefined ) {
                this.joinChannel( state.channel );
            }

            if( state.value !== undefined ) {
                this.notifyChange( json.parse( state.value ) );
            }
            
            if( state.messages !== undefined && this.channel ) {
                array.forEach( state.messages, function( msg ){
                    channels.channel( this.channel ).notify( msg.topic, json.parse( msg.value ) );
                }, this );
            }
        
        }
        
        ,joinChannel: function( channel ) {
            if( this.channelSubscription ) {
                this.channelSubscription.remove();
                delete this.channelSubscription;
            }
            
            this.channel = channel;
            
            if( this.channel ) {
                this.channelSubscription = channels.channel( this.channel ).join( this );
            }
        }

        ,notifyChange: function( value ) {
            this.pendingChange = false;
            this.value = value;

            if( this.channel ) {
                 //console.log( "Conversation change sending", this.value );
                 channels.channel( this.channel ).notify( "change", this.value );
            }
        }
        
        ,getChannelValue: function() {
            return this.value;
        }
        
        ,setChannelValue: function( value ) {
            this.value = value;
            if( this.startupState === undefined ) {
                if( this.eventActive( "changed" ) ) {
                    this.pendingChange = false;
                    this.emitEvent( "changed", { state: { value: json.stringify( this.value ) } } );
                } else {
                    this.pendingChange = true;
                }
            }
        }
        
        ,sendChannelMessage: function( topic, value ) {
            //console.log( "Conversation", topic, value );
            this.emitAlways( "message", { state: { topic: topic, value: json.stringify( value ) } } );
        }
    
    });
    
});