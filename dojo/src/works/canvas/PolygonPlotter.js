define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    return declare( [ Base ], {
        plot: function( shape ) {
            var ctx = this.context;

            this.setTranslation( shape.pt );

            ctx.rotate( -0.5 * Math.PI );
            this.setRotation( shape.ro );

            this.setScale( shape.sl );

            ctx.beginPath();

            var sides = parseFloat( shape.sd ); 
            var size = parseFloat( shape.r );

            //ctx.moveTo( shape.w * Math.cos( 0 ), shape.w *  Math.sin( 0 ) );          
            for( var i = 1; i <= shape.sd; i++ ) {
                ctx.lineTo( 
                    size * Math.cos( i * 2.0 * Math.PI / sides )
                    ,size * Math.sin( i * 2.0 * Math.PI / sides ) 
                );
            }
           
            this.resetTransformations();

            this.fillAndStroke( shape );
   
        }
    });

});