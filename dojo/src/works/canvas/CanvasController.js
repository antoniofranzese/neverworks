define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"works/lang/Model"

    ,"./Circle"
    ,"./Path"
    ,"./Polygon"
    ,"./Rectangle"
    ,"./Ring"
    ,"./Star"
    ,"./Text"
    ,"./Triangle"

    ,"./CirclePlotter"
    ,"./PathPlotter"
    ,"./PolygonPlotter"
    ,"./RectanglePlotter"
    ,"./RingPlotter"
    ,"./StarPlotter"
    ,"./TextPlotter"
    ,"./TrianglePlotter"

], function( declare, types, array, lang, Model
    ,Circle
    ,Path
    ,Polygon
    ,Rectangle
    ,Ring
    ,Star
    ,Text
    ,Triangle

    ,CirclePlotter
    ,PathPlotter
    ,PolygonPlotter
    ,RectanglePlotter
    ,RingPlotter
    ,StarPlotter
    ,TextPlotter
    ,TrianglePlotter

    ){

        // "ln": PathPlotter
        // ,"rc": RectanglePlotter
        // ,"cr": CirclePlotter
        // ,"rg": RingPlotter
        // ,"tx": TextPlotter
        // ,"tr": TrianglePlotter
        // ,"py": PolygonPlotter

    var PlotterMappings = {};
    PlotterMappings[ Path.TYPE ]      = PathPlotter;
    PlotterMappings[ Rectangle.TYPE ] = RectanglePlotter;
    PlotterMappings[ Circle.TYPE ]    = CirclePlotter;
    PlotterMappings[ Polygon.TYPE ]   = PolygonPlotter;
    PlotterMappings[ Ring.TYPE ]      = RingPlotter;
    PlotterMappings[ Star.TYPE ]      = StarPlotter;
    PlotterMappings[ Text.TYPE ]      = TextPlotter;
    PlotterMappings[ Triangle.TYPE ]  = TrianglePlotter;

    return declare( [ Model ],{
        reset: true

        ,constructor: function( canvas ) {
            this.canvas = canvas;
            this.context = canvas.getContext( "2d" );
            this.plotters = {};
        }

        ,plot: function( shapes ) {
            array.forEach( shapes, function( shape ) {

                if( shape ) {
                    var type = shape._;
                    if( type && type in PlotterMappings ) {

                        var plotter = this.plotters[ type ];
                        if( plotter === undefined ) {
                            plotter = new PlotterMappings[ type ]( this.canvas );
                            this.plotters[ type ] = plotter;
                        }

                        plotter.plot( shape );

                    }    
                }

            }, this );
        }

        ,clear: function() {
            this.context.clearRect( 0, 0, this.canvas.width, this.canvas.height );
        }
   
    });

});
