define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    return declare( [ Base ], {
        plot: function( shape ) {
            var ctx = this.context;
 
            var ZERO = shape.st !== undefined ? 0.5 : 0

            this.setTranslation( shape.or );
            this.setScale( shape.sc );
            this.setRotation( shape.ro );

            ctx.beginPath();
            ctx.moveTo( shape.pt[ 0 ], shape.pt[ 1 ] );
            for( var p = 0; p < shape.pts.length; p++ ) {
                var pt = shape.pts[ p ];

                if( pt.length == 2 ) {
                    ctx.lineTo( pt[ 0 ] + ZERO, pt[ 1 ] + ZERO );
                } else if( pt.length == 3 ) {
                    if( pt[ 0 ] == "B" ) {
                        // bezier
                    } else if( pt[ 0 ] == "Q" ) {
                        // quadratic
                    }
                }
            }
            if( shape.fl ) {
                ctx.closePath();
            }
            this.resetTransformations();

            this.fillAndStroke( shape );

        }
    });

});