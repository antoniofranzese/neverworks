define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    return declare( [ Base ], {
        plot: function( shape ) {
            var ctx = this.context;
            var ZERO = shape.st !== undefined || shape.fl === undefined ? 0.5 : 0;

            var r = shape.r
                , w = shape.w + ZERO
                , h = shape.h !== undefined ? shape.h + ZERO : shape.w;

            this.setTranslation( shape.pt );
            this.setRotation( shape.ro );
            this.setScale( shape.sc );

            if( r ) {
                // rounded rect

                ctx.beginPath();
                ctx.moveTo( r, ZERO );
                ctx.lineTo( w - r, ZERO);
                ctx.arcTo( w, ZERO, w, r, r );
                ctx.lineTo( w, h - r );
                ctx.arcTo( w, h, w - r, h, r );
                ctx.lineTo( r, h );
                ctx.arcTo( ZERO, h, ZERO, h - r, r );
                ctx.lineTo( ZERO, r );
                ctx.arcTo( ZERO, ZERO, r, ZERO, r );

            } else {
                ctx.beginPath();
                ctx.moveTo( ZERO, ZERO );
                ctx.lineTo( w, ZERO );
                ctx.lineTo( w, h );
                ctx.lineTo( ZERO, h );
                ctx.lineTo( ZERO, ZERO );
            }

            this.resetTransformations();

            this.fillAndStroke( shape );

        }
    });

});