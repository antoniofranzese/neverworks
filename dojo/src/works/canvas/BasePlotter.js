define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"works/lang/Model"

], function( declare, types, array, lang, Model ){

    var LINE_CAP = {
        "B": "butt"
        ,"R": "round"
        ,"S": "square"
    }

    var LINE_JOIN = {
        "R": "round"
        ,"B": "bevel"
        ,"M": "miter"
    }

    var BLACK = {
        cl: "black"
    }

    var COMPOSITES = {
      "SA": "source-atop"
      ,"SI": "source-in"
      ,"SO": "source-out"
      ,"SR": "source-over"
      ,"DA": "destination-atop"
      ,"DI": "destination-in"
      ,"DO": "destination-out"
      ,"DR": "destination-over"
      ,"L": "lighter"
      ,"D": "darker"
      ,"X": "xor"
      ,"C": "copy"
    }

    var radians = function( degrees ) {
        return degrees * Math.PI / 180;
    }

    return lang.mixin( declare( [ Model ], {
 
        constructor: function( canvas ) {
            this.canvas = canvas;
            this.context = canvas.getContext( "2d" );
        }

        ,setStroke: function( stroke ) {
            this.context.lineWidth = stroke.w !== undefined ? stroke.w : 1 ;
            this.context.strokeStyle = stroke.cl !== undefined ? stroke.cl : "black";
            this.context.lineCap = stroke.cp in LINE_CAP ? LINE_CAP[ stroke.cp ] : "butt";
            this.context.lineJoin = stroke.jn in LINE_JOIN ? LINE_JOIN[ stroke.jn ] : "miter";
        }

        ,setFill: function( fill ) {
            if( fill.cl ) {
                this.context.fillStyle = fill.cl;
            }
        }

        ,setShadow: function( shadow ) {
            if( shadow ) {
                var ctx = this.context;
                ctx.shadowColor = shadow.cl;
                ctx.shadowBlur = shadow.bl !== undefined ? shadow.bl : 1;
                ctx.shadowOffsetX = shadow.x !== undefined ? shadow.x : 1;
                ctx.shadowOffsetY = shadow.y !== undefined ? shadow.y : 1;
            }
        }

        ,resetShadow: function( shadow ) {
            if( shadow ) {
                var ctx = this.context;
                ctx.shadowColor = "rgba(0, 0, 0, 0)";
                ctx.shadowBlur = 0;
                ctx.shadowOffsetX = 0;
                ctx.shadowOffsetY = 0;
            }
        }

        ,setTranslation: function( origin ) {
            if( origin ) {
                this.context.translate( origin[ 0 ], origin[ 1 ] );
            }
        }
        
        ,setRotation: function( rotation ) {
            if( rotation !== undefined ) {
                this.context.rotate( radians( rotation ) );
            }
        }

        ,setScale: function( scale ) {
            if( scale !== undefined ) {
                this.context.scale(
                    scale.x !== undefined ? scale.x / 100 : 1
                    ,scale.y !== undefined ? scale.y / 100 : 1
                );
            }
        }

        ,setEffects: function( fx ) {
            if( fx && fx in COMPOSITES ) {
                this.context.globalCompositeOperation = COMPOSITES[ fx ];
            }
        }

        ,resetEffects: function( fx ) {
            if( fx ) {
                this.context.globalCompositeOperation = "source-over";
            }
        }

        ,resetTransformations: function() {
            this.context.setTransform( 1, 0, 0, 1, 0, 0 );
        }

        ,fillAndStroke: function( shape ) {
            this.setEffects( shape.fx );

            if( shape.fl !== undefined ) {
                this.setShadow( shape.sh );
                this.setFill( shape.fl );
                this.context.fill();
                this.resetShadow( shape.sh );
            }
      
            if( shape.fl === undefined ) {
                this.setShadow( shape.sh );
            }

            if( shape.st !== undefined ) {
                this.setStroke( shape.st );
                this.context.stroke();
            } else if( shape.fl === undefined ) {
                this.setStroke( BLACK );
                this.context.stroke();
            }

            if( shape.fl === undefined ) {
                this.resetShadow( shape.sh );
            }

            this.resetEffects( shape.fx );
       }
 
    }), { 
        
        radians: radians
        ,BLACK: BLACK
    });

});