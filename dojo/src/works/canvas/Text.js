define([
    "works/lang/declare"
    ,"./Shape"
], function( declare, Shape ){

    var TYPE = "tx";

    var TEXT_HALIGN = {
        "start" : "S"
        ,"end" : "E"
        ,"left" : "L"
        ,"right" : "R"
        ,"center" : "C"
        ,"middle": "C"
    }
    
    var TEXT_VALIGN = {
        "top" : "T"
        ,"middle" : "M"
        ,"center": "M"
        ,"bottom" : "B"
    }

    var TEXT_BASELINE = {
        "alphabetic" : "A"
        ,"top" : "T"
        ,"hanging" : "H"
        ,"middle" : "M"
        ,"ideographic" : "I"
        ,"bottom" : "B"
    }

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Shape ], {
        _: TYPE
        
        ,value: function( text ) {
            this.tx = text;
            return this;
        }

        ,height: function( height ) {
            this.h = height;
            return this;
        }

        ,width: function( width ) {
            this.w = width;
            return this;
        }

        ,halign: function( halign ) {
            if( halign in TEXT_HALIGN ) {
                this.ha = TEXT_HALIGN[ halign ];
            }
            return this;
        }

        ,valign: function( valign ) {
            if( valign in TEXT_VALIGN ) {
                this.va = TEXT_VALIGN[ valign ];
            }
            return this;
        }

        ,baseline: function( baseline ) {
            if( baseline in TEXT_BASELINE ) {
                this.bl = TEXT_BASELINE[ baseline ];
            }
            return this;
        }

        ,font: function( font ) {
            this.ft = font;
            return this;
       }

    }));

});