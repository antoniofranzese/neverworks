define([
    "works/lang/declare"
    ,"./Shape"
], function( declare, Shape ){

    var TYPE = "rc";

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Shape ], {
        _: TYPE
        
        ,width: function( width ) {
            this.w = width;
            return this;
        }    

        ,height: function( height ) {
            this.h = height;
            return this;
        }    
    
    }));

});