define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    var HALF_PI = 0.5 * Math.PI;
    var DOUBLE_PI = 2 * Math.PI;

    return declare( [ Base ], {
        plot: function( shape ) {
            var ctx = this.context;

            ctx.beginPath();
            
            var from, to;
            if( shape.fr !== undefined && shape.to !== undefined ) {
                from = Base.radians( shape.fr ) - HALF_PI;
                to = Base.radians( shape.to ) - HALF_PI;
                
                ctx.save();

                this.setTranslation( shape.pt );
                this.setRotation( shape.fr );
                
                if( shape.fl ) {
                    ctx.moveTo( 0, 0 );
                    ctx.lineTo( 0, -1 * shape.r );
                } else {
                    ctx.moveTo( 0, -1 * shape.r );
                }
                
                ctx.arc( 0, 0, shape.r, - HALF_PI, Base.radians( shape.to - shape.fr ) - HALF_PI );
                
                if( shape.fl ) {
                    ctx.lineTo( 0, 0 );            
                }

                ctx.restore();

            } else {
                ctx.arc( 
                    shape.pt[ 0 ]
                    ,shape.pt[ 1 ]
                    ,shape.r
                    ,0
                    ,DOUBLE_PI
                    ,false
                );
            }
            



            this.resetTransformations();

            this.fillAndStroke( shape );

        }
    });


});