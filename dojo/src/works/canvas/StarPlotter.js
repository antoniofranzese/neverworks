define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    return declare( [ Base ], {
        plot: function( shape ) {
            var ctx = this.context;

            this.setTranslation( shape.pt );

            ctx.rotate( -0.5 * Math.PI );
            this.setRotation( shape.ro );

            this.setScale( shape.sl );

            ctx.beginPath();

            var sides = shape.sp !== undefined ? ( parseFloat( shape.sp ) * 2 ) : 10; 
            var outer = parseFloat( shape.r );
            var inner = outer * ( shape.rt !== undefined ? parseFloat( shape.rt ) : 0.5 );

            for( var i = 1; i <= sides; i++ ) {
                var size = ( i % 2 == 0 ) ? outer : inner
                ctx.lineTo( 
                    size * Math.cos( i * 2.0 * Math.PI / sides )
                    ,size * Math.sin( i * 2.0 * Math.PI / sides ) 
                );
            }
           
            this.resetTransformations();

            this.fillAndStroke( shape );
   
        }
    });

});