define([
    "works/lang/declare"
    ,"./Shape"
], function( declare, Shape ){

    var TYPE = "st";

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Shape ], {
        _: TYPE
    
        ,radius: function( radius ) {
            this.r = radius;
            return this;
        }

        ,ratio: function( ratio ) {
            this.rt = ratio;
            return this;
        }

        ,spikes: function( spikes ) {
            this.sp = spikes;
            return this;
        }

    }));

});