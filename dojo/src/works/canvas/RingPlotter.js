define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    var CCW_HALF_PI = -0.5 * Math.PI;
    var DOUBLE_PI = 2 * Math.PI;

    return declare( [ Base ], {
        plotArc: function( shape ) {
            var ctx = this.context;
            var extent = Base.radians( shape.to - shape.fr );
            var inner = shape.r - shape.w;

            ctx.save();
            
            ctx.translate( shape.pt[ 0 ], shape.pt[ 1 ] );
            ctx.beginPath();

            ctx.rotate( Base.radians( shape.fr ) );
            
            ctx.moveTo( 0, -1 * inner );
            ctx.lineTo( 0, -1 * shape.r );
            ctx.arc( 0, 0, shape.r, CCW_HALF_PI, extent + CCW_HALF_PI, false );
            ctx.rotate( extent );
            ctx.lineTo( 0, -1 * inner );
            ctx.arc( 0, 0, inner, CCW_HALF_PI, CCW_HALF_PI - extent, true );
            ctx.closePath();
 
            ctx.restore();

            this.fillAndStroke( shape );
            
        }

        ,plot: function( shape ) {

            if( shape.fr !== undefined && shape.to !== undefined ) {

                this.plotArc( shape );

            } else {

                if( shape.fl ) {
                    this.plotArc( lang.mixin( {}, shape, { fr: 0, to: 360, st: undefined } ) );
                }

                if( shape.st !== undefined || shape.fl === undefined ) {

                    this.setStroke( shape.st !== undefined ? shape.st : Base.BLACK );

                    var ctx = this.context;
        
                    ctx.beginPath();
                    ctx.arc( 
                        shape.pt[ 0 ]
                        ,shape.pt[ 1 ]
                        ,shape.r
                        ,0
                        ,DOUBLE_PI
                        ,false
                    );
                    ctx.stroke();
                    
                    ctx.beginPath();
                    ctx.arc( 
                        shape.pt[ 0 ]
                        ,shape.pt[ 1 ]
                        ,shape.r - shape.w
                        ,0
                        ,DOUBLE_PI
                        ,false
                    );

                    ctx.stroke();

                }

            }

        }
    });

    // var StrokeRingPlotter = declare( [ Baser ], {
    //     plot: function( shape ) {
    //         this.setTransformations( shape );
    //         var ctx = this.context;
    //         ctx.beginPath();

    //         var from, to;
    //         if( shape.fr !== undefined && shape.to !== undefined ) {
    //             from = radians( shape.fr ) - Base.HALF_PI;
    //             to = radians( shape.to ) - Base.HALF_PI;
    //         } else {
    //             from = 0;
    //             to = 2 * Math.PI;
    //         }
            
    //         var radius = shape.r - ( shape.w / 2 );
    //         ctx.arc( 
    //             shape.pt[ 0 ]
    //             ,shape.pt[ 1 ]
    //             ,radius
    //             ,from
    //             ,to
    //             ,false
    //         );

    //         if( shape.fl ) {
    //             this.setStroke( { cl: shape.fl.cl, w: shape.w })
    //         } else {
    //             this.setStroke( { cl: "black", w: shape.w });
    //         }
    //         ctx.stroke();

    //     }
    // });


});