define([
    "works/lang/declare"
    ,"./Circle"
], function( declare, Circle ){

    var TYPE = "rg";

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Circle ], {
        _: TYPE
        
        ,width: function( width ) {
            this.w = width;
            return this;
        }    

    }));

});