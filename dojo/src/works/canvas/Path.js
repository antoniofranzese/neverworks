define([
    "works/lang/declare"
    ,"./Shape"
], function( declare, Shape ){

    var TYPE = "ln";

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Shape ], {
        _: TYPE
        
        ,constructor: function() {
            this.pts = [];
        }

        ,from: function( x, y ) {
            return this.at( x, y );
        }

        ,to: function( x, y ) {
            this.pts.push( [ x, y ] );
        }

    }));

});