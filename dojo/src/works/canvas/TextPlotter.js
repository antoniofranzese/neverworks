define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    var TEXT_HALIGN = {
        "S": "start" 
        ,"E": "end" 
        ,"L": "left" 
        ,"R": "right" 
        ,"C": "center"
    }

    var TEXT_BASELINE = {
        "A": "alphabetic"
        ,"T": "top"
        ,"H": "hanging"
        ,"M": "middle"
        ,"I": "ideographic"
        ,"B": "bottom"
    }

    return declare( [ Base ], {
        plot: function( shape ) {
            if( shape.tx !== undefined ) {
                this.setTranslation( shape.pt );
                this.setRotation( shape.ro );
                this.setScale( shape.sc );

                var ctx = this.context;

                ctx.font = "normal 10px Helvetica";
                if( shape.ft ){
                    ctx.font = shape.ft;
                }
                ctx.textAlign = shape.ha !== undefined ? TEXT_HALIGN[ shape.ha ] : "start";

                var x = 0,
                    y = 0;

                if( shape.va !== undefined && shape.h !== undefined ) {
                    ctx.textBaseline = "top";

                    if( shape.va == "M" ) {
                        var metrics = context.measureText( shape.tx );
                        y += ( shape.h - metrics.height ) / 2;
                    } else if( shape.va == "B" ) {
                        var metrics = context.measureText( shape.tx );
                        y += shape.h - metrics.height;
                    }

                } else if( shape.bl !== undefined ) {
                    ctx.textBaseline = shape.bl !== undefined ? TEXT_BASELINE[ shape.bl ] : "top";
                }

                this.setEffects( shape.fx );

                if( shape.fl || shape.st === undefined ) {
                    this.setShadow( shape.sh );
                    this.setFill( shape.fl !== undefined ? shape.fl : Base.BLACK );
                    ctx.fillText( shape.tx, x, y );
                    this.resetShadow( shape.sh );
                }

                if( shape.fl === undefined ) {
                    this.setShadow( shape.sh );
                }
                if( shape.st ) {
                    this.setStroke( shape.st );
                    ctx.strokeText( shape.tx, x, y );
                }
                if( shape.fl === undefined ) {
                    this.resetShadow( shape.sh );
                }

                this.resetEffects( shape.fx );

                this.resetTransformations();
            }
        }
    });


});