define([
    "works/lang/declare"
], function( declare ){

    var COMPOSITES = {
       "source-atop": "SA" 
      ,"source-in": "SI" 
      ,"source-out": "SO" 
      ,"source-over": "SR" 
      ,"destination-atop": "DA" 
      ,"destination-in": "DI" 
      ,"destination-out": "DO" 
      ,"destination-over": "DR" 
      ,"lighter": "L" 
      ,"darker": "D" 
      ,"xor": "X" 
      ,"copy": "C" 
    }

    var LINE_CAP = {
        "butt": "B"
        ,"round": "R"
        ,"square": "S"
    }

    var LINE_JOIN = {
        "round": "R"
        ,"bevel": "B"
        ,"miter": "M"
    }

    return declare( [], {
        
        at: function( x, y ) {
            this.pt = [ x, y ];
            return this;
        }

        ,stroke: function( stroke, width, join, cap ) {
            if( typeof stroke == "string" ) {
                this.st = { 
                    cl: stroke 
                    ,w: width
                    ,cp: cap in LINE_CAP ? LINE_CAP[ cap ] : cap
                    ,jn: join in LINE_JOIN ? LINE_JOIN[ join ] : join
                }
            } else {
                this.st = {
                    w: stroke.width
                    ,cl: stroke.color
                    ,cp: stroke.cap in LINE_CAP ? LINE_CAP[ stroke.cap ] : stroke.cap
                    ,js: stroke.join in LINE_JOIN ? LINE_JOIN[ stroke.join ] : stroke.join
                }
            }
            return this;
        }    

        ,fill: function( fill ) {
            if( typeof fill == "string" ) {
                this.fl = { 
                    cl: fill 
                }
            } else {
                this.fl = {
                    cl: fill.color
                }
            }
            return this;
        }    

        ,shadow: function( shadow, blur, x, y ) {
            if( typeof shadow == "string" ) {
                this.sh = { 
                    cl: shadow
                    ,bl: blur
                    ,x: y
                    ,y: y 
                };
            } else {
                this.sh = {
                    cl: shadow.color
                    ,bl: shadow.shadow
                    ,x: shadow.x
                    ,y: shadow.y
                }
            }
            return this;
        }

        ,rotate: function( rotation ) {
            this.ro = rotation
        }

        ,scale: function( scale, y ) {
            if( typeof scale == "number" ) {
                this.sc = { 
                    x: scale
                    ,y: y 
                }
            } else {
                this.sc = {
                    x: scale.x
                    ,y: scale.y
                }
            }
        }

        ,effect: function( effect ) {
            this.fx = effect in COMPOSITES ? COMPOSITES[ effect ] : effect;
            return this;
        }   
    
    });
});