define([
    "works/lang/declare"
    ,"./Shape"
], function( declare, Shape ){

    var TYPE = "py";

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Shape ], {
        _: TYPE
    
        ,radius: function( radius ) {
            this.r = radius;
            return this;
        }

        ,sides: function( sides ) {
            this.sd = sides;
            return this;
        }

    }));

});