define([
    "works/lang/declare"
    ,"works/lang/types"
    ,"dojo/_base/array"
    ,"dojo/_base/lang"
    ,"./BasePlotter"

], function( declare, types, array, lang, Base ){

    return declare( [ Base ], {
        plot: function( shape ) {
            var ctx = this.context;

            this.setTranslation( shape.pt );
            this.setRotation( shape.ro );
            this.setScale( shape.sc );


            ctx.beginPath();
            ctx.moveTo( 0, shape.h / -2 );
            ctx.lineTo( shape.w / 2, shape.h / 2 );
            ctx.lineTo( shape.w / -2, shape.h / 2 );
            ctx.closePath();

            this.resetTransformations()

            this.fillAndStroke( shape );

        }
    });

});