define([
    "works/lang/declare"
    ,"./Shape"
], function( declare, Shape ){

    var TYPE = "cr";

    return declare.statics({ 
        TYPE: TYPE
    
    }, declare( [ Shape ], {
        _: TYPE
        
        ,radius: function( radius ) {
            this.r = radius;
            return this;
        }

        ,from: function( from ) {
            this.fr = from;
            return this;
        }    

        ,to: function( to ) {
            this.to = to;
            return this;
        }    
    
    }));

});