define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/dom-construct"
    ,"dojo/dom-geometry"
    ,"dojo/dom-class"
    ,"dojo/dom-style"
    ,"dojo/on"
    ,"dojo/Evented"
    ,"dijit/_WidgetBase"
    ,"works/form/_WidgetMixin"
    ,"works/form/_FormWidgetMixin"
    ,"works/dnd/_DropMixin"
    ,"./CanvasController"
    
], function( req, declare, lang, array, domConstruct, domGeometry, domClass, domStyle, on, 
    Evented, _WidgetBase, _WidgetMixin, _FormWidgetMixin, _DropMixin, CanvasController ){
    
    var Canvas = declare( declare.className( req ), [ _WidgetBase, Evented, _WidgetMixin, _FormWidgetMixin /*, _DropMixin */ ], {

        baseClass: "worksCanvas"
        ,height: "50px"
        ,width: "50px"

        ,buildRendering: function() {
            this.inherited( arguments );

            this.canvasNode = domConstruct.create( "canvas", {
                width: this.width
                ,height: this.height
            }, this.domNode );

            this.controller = new CanvasController( this.canvasNode );

            if( this.initialScene ) {
                this.controller.plot( this.initialScene );
                delete this.initialScene;
            }
        }

        ,_setSceneAttr: function( scene ) {
            this.controller.clear();
            this.controller.plot( scene );
        }

    });

    Canvas.markupFactory = _WidgetMixin.markupFactory;

    return Canvas;
});