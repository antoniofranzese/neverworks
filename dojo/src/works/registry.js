define([ 
    "dijit/registry" 
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dijit/_WidgetBase"
], function( registry, lang, array, _WidgetBase ){
    
    return lang.mixin( registry, {
        getMainNode: function( widget ) {
            if( widget !== undefined ) {
                if( widget.srcNodeRef ) {
                    return widget.srcNodeRef;
                } else {
                    return widget.domNode;
                }
            } else {
                throw Error( "Cannot get node for undefined widget" );
            }
        }
        
        ,exists: function( widget ) {
            if( widget !== undefined && widget != null ) {
                var node = this.getMainNode( widget );
                return node !== undefined && node != null;
            } else {
                return false;
            }
        }

        ,findParents: function( widget, requestedClass ) {
            var result = [];
            while( widget != null && widget !== undefined ) {
                var parent = this.findParent( widget, requestedClass );
                if( parent !== undefined ) {
                    result.push( parent );
                }
                widget = parent;
            }
            return result;
        }


        ,findParentsLegacy: function( widget, requestedClass ) {
            requestedClass = requestedClass || _WidgetBase;
            var result = [];
            var node = this.getMainNode( widget ).parentNode;
            while( node != null && node.nodeName != "BODY" ) {
                parentWidget = this.getEnclosingWidget( node );
                if( parentWidget !== undefined && parentWidget !== null ) {
                    if( parentWidget.isInstanceOf( requestedClass ) ) {
                        result.push( parentWidget );
                    }
                    node = this.getMainNode( parentWidget ).parentNode;
                } else {
                    node = node.parentNode;
                }
            }
            return result;
        }

        ,findParent: function( widget, requestedClass ) {
            requestedClass = requestedClass || _WidgetBase;
            do {
                var parent = widget.getParent();
                if( parent == null || parent === undefined ) {
                    var mainNode = this.getMainNode( widget );
                    if( mainNode ) {
                        var node = mainNode[ ".parentNode" ] || mainNode.parentNode;
                        while( node && ( parent == null || parent === undefined ) && node.nodeName != "BODY" ) {
                            parent = node.id ? registry.byId( node.id ) : registry.getEnclosingWidget( node );

                            if( parent == null || parent === undefined ) {
                                node = node[ ".parentNode" ] || node.parentNode;
                            }
                        }
                    }
                }
                widget = parent;

            } while( widget != null && widget !== undefined && ! widget.isInstanceOf( requestedClass ) );

            return widget != null ? widget : undefined;
        }
    
        ,findParentLegacy: function( widget, requestedClass ) {
            requestedClass = requestedClass || _WidgetBase;
            var result = undefined;
            var mainNode = this.getMainNode( widget );
            if( mainNode ) {
                var node = mainNode.parentNode;
                while( result === undefined && node != null && node.nodeName != "BODY" ) {
                    // In alcune situazione l'attributo id e' popolato ma widgetid no, per cui getEnclosingWidget
                    // fornisce un risultato errato
                    // parentWidget = this.getEnclosingWidget( node );
                    parentWidget = node.id ? registry.byId( node.id ) : registry.getEnclosingWidget( node );
                    
                    if( parentWidget !== undefined && parentWidget !== null && parentWidget.isInstanceOf( requestedClass ) ) {
                        result = parentWidget;
                    } else {
                        node = node.parentNode;
                    }
                }
                return result;
            } else {
                console.error( "Missing main node:", widget );
                throw new Error( "Missing main node for " + widget.id );
            }
        }
        
        ,getInventory: function() {
            var result = {};
            array.forEach( this.toArray(), function( widget ){
                var cls = widget.declaredClass || "no class";
                if( result[ cls ] !== undefined ) {
                    result[ cls ] += 1;
                } else {
                    result[ cls ] = 1;
                }
            });
            return result;
        }
    
        ,findByClass: function( cls ) {
            return array.filter( this.toArray(), function( widget ){ return widget.isInstanceOf ? widget.isInstanceOf( cls ) : widget.declaredClass == cls });
        }
    
        ,filter: function( flt ) {
            return array.filter( this.toArray(), flt );
        }

    });
    
});