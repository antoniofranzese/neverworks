define([
    "dojo/has"
    ,"dojo/_base/lang"
    ,"dojo/_base/window"
    ,"dojo/_base/array"
], function( has, lang, win, array ){

    var mainConfig = lang.getObject( "worksConfig" ) || {};
    
    var plugins = {};
    
    var defined = function( value ) {
        return value !== undefined && value != null;
    }
    var safe = function( value, deflt ) {
        if( ! defined( value ) && defined( deflt ) ) {
            return deflt;
        } else {
            return value;
        }
    }

    var get = function( name ) {
        if( name in plugins ) {
            if( lang.isFunction( plugins[ name ] ) ) {
                var _global = win.global;
                var _document = win.doc;
                var _element = win.doc.documentElement; 

                return plugins[ name ]( _global, _document, _element );
            } else {
                return plugins[ name ];
            } 

        } else {
            return lang.getObject( name, false, mainConfig );
        }
    }
    
    var config = function( names, deflt ){
        if( lang.isArrayLike( names ) ) {
            var value;
            array.forEach( names, function( name ){
                if( ! defined( value ) ) {
                    value = get( name );
                }
            })
            return safe( value, deflt );
        } else {
            return safe( get( names ), deflt );
        }
    }
    
    config.get = get;
    
    config.add = function( name, value, force ) {
        force = force || false;
        if( name in plugins && force ) {
            delete plugins[ name ];
        }

        if( !( name in plugins ) ) {
            plugins[ name ] = value;
        } else {
            throw Exception( "Duplicated config plugin: " + name );
        }
    }
    
    return config;
    
})