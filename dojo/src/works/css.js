define([
    "require"
	,"dojo/_base/kernel"
    ,"dojo/_base/window"
    ,"dojo/_base/lang"
    ,"dojo/_base/url"
    ,"dojo/dom"
    ,"dojo/dom-attr"
    ,"dojo/dom-class"
    ,"dojo/query"
    ,"dojo/dom-construct"
    ,"dojo/has"
    ,"dojo/request"
    ,"dojo/Deferred"
    
    ,"works/lang/StringBuilder"
    ,"works/lang/strings"
    ,"works/wait"
    
    ,"works/_base/uacss"
    ,"ext/less"

], function( _require, kernel, win, lang, URL, dom, domAttr, domClass, query, domConstruct, has, request, Deferred, StringBuilder, strings, wait ){
    
    var html = win.doc.documentElement;
    
    var head = query( "head" )[0];
    var marker = dom.byId( _require.module.mid );

    domClass.remove( win.body(), "nw_theme" );

    if( "less" in kernel.global ) {
        var parser = less.Parser();
        
        var currentTheme = domAttr.get( win.body(), "class" ).trim();
        var commons = (new StringBuilder( 128 )) 
            .append( "@dojoTheme: {0};\n", currentTheme )
            .append( "@basePath: '{0}';\n", dojoConfig.baseUrl )
            .toString();

    } else {
        var parser = undefined;
    }

    domClass.add( win.body(), "nw_theme" );
    
    var findStyleInRegistry = function( s ) {
        var found = false;
        for( var i = 0; i < document.styleSheets.length; i++ ) {
            if( s == document.styleSheets[ i ].ownerNode ) {
                found = true;
            }
        }
        return found;
    } 

    var waitForStyle = function( style, deferred ) {
 
        // Firefox
        if( has( "ff" ) ) {
            wait( function(){
                try {
                    var rules = style.sheet.cssRules;
                    return true;
                } catch( ex ) {
                    return false;
                }
            }, {
                delay: 200
                ,timeout: 20000
                //,log: "FF style"
            
            }).then( function(){
                //console.log( "FF Loaded", style );
                deferred.resolve();
            }, function(){
                console.log( "FF Timeout", style );
            });
 
        // WebKit
        } else if( has( "webkit" ) ) {
            wait( function(){
                return findStyleInRegistry( style );
            }, {
                delay: 100
                ,timeout: 20000
                //,log: "WK style"
            }).then( function(){
                //console.log( "WK Loaded", style );
                deferred.resolve();
            }, function(){
                console.log( "WK Timeout", style );
            });
 

        } else {
            setTimeout( function(){ 
                //console.log( "Loaded", style );
                deferred.resolve();
            }, 0 );
            
        }
    }
    
    

    var loadCSS = function( url ) {
        var path = ( url.indexOf( "?" ) > 0 ? url.substr( 0, url.indexOf( "?" ) ) : url ).trim();

        var d = new Deferred();
        // Parsed CSS
        if( parser && path.endsWith( ".less" ) ) {
            request( url, { describeAs: "text/css" } ).then( function( data ){
                var path = new URL( url ).path;
                var locals = (new StringBuilder( 128 )) 
                    .append( '@localPath: "{0}";\n', url.slice( 0, url.lastIndexOf( "/" ) ) )
                    .append( '@localName: "{0}";\n', url.slice( url.lastIndexOf( "/" ) + 1, -5 ) )
                    .toString();

                //console.log( "LOCALS", locals );
                parser.parse( data + commons + locals, function( err, tree ){ 
                    if( err ) {
                        console.error( "Error parsing " + url + ", " + err.message + " on line " + err.line + ":\n" + err.extract.join( "\n" ) );
                    } else {
                        //console.log( tree.toCSS() );
                        var style = domConstruct.create( "style", { type: "text/css", "data-works-source": url } );
                        var head = query( "head" )[0];
                        if( marker ) {
                            domConstruct.place( style, marker, "before" );
                        } else {
                            domConstruct.place( style, head );
                        }
                        style.innerHTML = tree.toCSS()  + "\n.worksCssParserInfo { content: \"source: " + url + "\"; }\n" ;
                        waitForStyle( style, d );
                    } 
                });
                
            }, function( err ){
                console.error( err );
            });
            
        
        // Plain CSS load
        } else {
            if( has( "ie" ) ) {
                var link = document.createElement( "link" );
                link.rel = "stylesheet";
                link.type = "text/css";
                link.href = url;
                if( marker ) {
                    marker.parentNode.insertBefore( link, marker );
                } else {
                    document.getElementsByTagName( "head" )[0].appendChild( link );
                }
                setTimeout( function(){ d.resolve() }, 0 );

            } else {
                //console.log( "Loading", url );
                var style = domConstruct.create( "style", { type: "text/css", "data-works-ref": url } );
                if( marker ) {
                    domConstruct.place( style, marker, "before" );
                } else {
                    domConstruct.place( style, head );
                }

                //style.innerHTML = "@import url('" + url + "');"
                style.textContent = "@import url('" + url + "');"
                waitForStyle( style, d );
            }
        }
        
        return d;
        
    }
    
    return {
        load: function( id, require, callback ) {
            try {
                //console.log( "Loading", require.toUrl( id ) );
                loadCSS( require.toUrl( id ) ).then( function(){
                    callback();
                });
                
            } catch( e ) {
                console.error( "Error loading", id, "style:", e );
            }
        }
        
    }
})