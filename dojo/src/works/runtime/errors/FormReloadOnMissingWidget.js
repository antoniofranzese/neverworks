define([
    "dojo/_base/declare"
    ,"dojo/i18n!./nls/ErrorMessages"
    ,"./FormReload"
], function( declare, messages, FormReload ){

    return declare([ FormReload ], {
        constructor: function() {
            this.message = messages.missingWidget;
        }
    });
    
});