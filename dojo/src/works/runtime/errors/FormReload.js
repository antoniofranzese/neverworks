define([
    "dojo/_base/declare"
    ,"dojo/i18n!./nls/ErrorMessages"
    ,"works/runtime/dialogs/MessageBox"
], function( declare, messages, MessageBox ){

    var FormReload = declare([], {
        constructor: function( params ) {
            params = params || {};
            this.message = params.message;
        }
        
        ,handleError: function( error ) {
            MessageBox.show({
                title: messages.errorTitle
                ,message: this.message
            }).then( function(){
                location.reload( /* bypassCache */ true );
            });
        }
    });
    
    FormReload.messages = messages;
    
    return FormReload;
});