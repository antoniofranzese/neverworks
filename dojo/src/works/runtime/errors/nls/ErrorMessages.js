define({
    root: {
        errorTitle: "Error"
        ,missingWidget: "The current form is not available anymore, click OK to reload."
        ,serverError: "The server is not available, click OK to try to reload."
        ,serverTimeout: "The request has timed out, click OK to try to reload."
        ,logonExpired: "The authentication session has expired, click OK to authenticate."
    }
    ,it: true
});