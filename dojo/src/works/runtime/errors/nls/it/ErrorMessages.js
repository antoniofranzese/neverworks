define({
    errorTitle: "Errore"
    ,missingWidget: "La maschera corrente non è più disponibile, premere OK per ricaricare."
    ,serverError: "Il server non è disponibile, premere OK per provare a ricaricare."
    ,serverTimeout: "La richiesta è scaduta, premere OK per provare a ricaricare."
    ,logonExpired: "La sessione di autenticazione è scaduta, premere OK per autenticarsi."
});