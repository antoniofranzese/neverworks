define([
	'dojo/_base/declare'
	,'dojo/_base/lang'
	,'dojo/_base/array'
	,'dojo/query'
	,'works/registry'
	,'dijit/Dialog'
	,'dojo/dom-attr'
	,"dojo/Deferred"
	,"dojo/promise/all"
	,"dojo/when"
], function( declare, lang, array, query, registry, Dialog, domAttr, Deferred, all, when ){
	
    //TODO: Nella trasformazione al nuovo framework, il meccanismo dei references puo' essere eliminato
    
	return declare( [Dialog], {
		referenceWith: "dialog"
		,waitForDescendants: true

	    ,postMixInProperties: function() {
			this.inherited( arguments );
			this.injectTo = 'data-' + this.referenceWith + '-inject';
			this.attachFrom = 'data-' + this.referenceWith + '-attach';
	    }
    
        ,postCreate: function() {
            this.updateReferences();
            this.inherited( arguments );
        }

		,show: function() {
 		    var super_show = lang.partial( lang.hitch( this, this.getInherited( arguments ) ), arguments );

		    var self = this;
		    var deferred = new Deferred();
		    var args = arguments;
            
		    if(self.onLoadDeferred) {
			    self.onLoadDeferred.then( function(){
                    // when( super_show.apply( self, args ), function(){
			    	when( super_show(), function(){
			            //console.log( "Shown" );
	                    
	                    if( self.waitForDescendants ) {
	            		    var descendantDeferreds = array.map( array.filter( self.getDescendants(), function( widget ){
	            				return "onLoadDeferred" in widget;
	            			}), function( w ){
	            			    return w.onLoadDeferred;
	            			});
	                    } else {
	                        var descendantDeferreds = []
	                    }
	
	                    all( descendantDeferreds ).then( function(){ 
	                        self.updateReferences();
	                        deferred.resolve( self );
	                    });
			    		
			    	});
			    
			    });
		    } else {
                // when( super_show.apply( self, args ), function(){
		    	when( super_show(), function(){
			    	deferred.resolve( self );		    		
		    	});
		    }

			return deferred;
		}
		
		,clearReferences: function() {
			if( lang.isArray( this._attachs ) ) {
				for( var a in this._attachs ) {
					delete this[this._attachs[a]];
				}
			}
			this._attachs = [];
		}
		
		,updateReferences: function() {
            // console.log( "Updating references" );
			this.clearReferences();

		  	query( "[" + this.injectTo + "]", this.containerNode ).forEach( function(node, index, arr) {
		  	    
		    	var widget = registry.byId( node.id );
				if( widget != undefined ) {
                    // console.log( "Inject", domAttr.get( node, this.injectTo ) );
					widget[ domAttr.get( node, this.injectTo ) ] = this;
				}
		  	}, this );

			query( "[" + this.attachFrom + "]", this.containerNode ).forEach( function(node, index, arr) {
				var name = domAttr.get( node, this.attachFrom );
				var attachPoint = undefined;

				if( node.id != undefined ) {
					var widget = registry.byId( node.id );
					if( widget != undefined ) {
						attachPoint = widget
					}
		    	}
				if( attachPoint == undefined ) {
					attachPoint = node;
				}
                // console.log( "Attach", name );
				this[ name ] = attachPoint;
				this._attachs.push( name );
		  	}, this );

            this.emit( "update-references", { bubbles: false });
		}
        
        ,destroy: function() {
			if(this._fadeInDeferred){
                this._fadeInDeferred.then( function(){}, function(){});
                this._fadeInDeferred.cancel( "destroy", false );
                delete this._fadeInDeferred;
			}
			if(this._fadeOutDeferred){
                this._fadeOutDeferred.then( function(){}, function(){});
                this._fadeOutDeferred.cancel( "destroy", false );
                delete this._fadeOutDeferred;
			}
            this.inherited( arguments );
            
        }
		
	});
})