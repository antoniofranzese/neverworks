define([
    "require"
    ,"dojo/_base/declare"
    ,"dojo/_base/lang"
    ,"dojo/on"
    ,"dojo/Deferred"
    ,"./Dialog"
    ,"dojo/text!./templates/MessageBox.html"

], function( _require, declare, lang, on, Deferred, Dialog, template ){
    
    var MessageBox = declare( "works.MessageBox", [ Dialog ], {
        content: template
        
        ,startup: function() {
            this.inherited( arguments );
            this.own(
                on( this, "update-references", lang.hitch( this, function(){
                    this.set( "message", this.get( "message" ) );
                    this._position();
                }))
                ,on( this, "cancel", lang.hitch( this, function(){
                    this.doConfirm();
                }))
            )
        }
        
		,_setMessageAttr: function( value ) {
            this._set( "message", value );
	    	if( this.messageNode != undefined ) {
	    		this.messageNode.innerHTML = value;
	    	}
	    }
        
        ,doConfirm: function() {
            this.hide();
            this.emit( "confirm", { bubbles: false });
            setTimeout( lang.hitch( this, this.destroy ), 0 );
        }
        
        ,show: function() {
            var super_show = this.getInherited( arguments );
            d = new Deferred();
            on.once( this, "confirm", function() {
                d.resolve();
            });
            super_show.apply( this );
            return d;
        }
        
    });
    
    MessageBox.show = function( params ) {
        params = params || {};
        if( params.closable === undefined ) {
            params.closable = false;
        }
        return new MessageBox( params ).show();
    }
    
    // Preload
    var m = new MessageBox();
    setTimeout( function(){
        m.destroy();
    }, 5000 );
    
    return MessageBox;
    
});