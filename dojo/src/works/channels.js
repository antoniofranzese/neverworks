define([
    "require"
    ,"works/lang/declare"
    ,"dojo/_base/lang"
    ,"dojo/_base/array"
    ,"dojo/Deferred"
    ,"works/lang/Enrollment"
    ,"works/lang/strings"
    ,"works/messages"
], function( req, declare, lang, array, Deferred, Enrollment, strings, messages ){
    
    var catchers = new Enrollment();

    var REQUEST = "request";
    var RESPONSE = "response";

    var Publisher = declare([], {
    
        constructor: function( channel ) {
            this.channel = channel;
        }

        ,sendChannelMessage: function( topic, value ) {
            this.channel.notify( topic, value );
        }

    });

    var Requestor = declare([], {
    
        constructor: function( channel ) {
            this.channel = channel;
            this.requests = {};
        }

        ,sendChannelMessage: function( topic, value ) {
            if( topic == RESPONSE ) {
                var request = this.requests[ value.id ];
                if( request ) {
                    request.deferred.resolve( value.response );
                    delete this.requests[ value.id ];
                }
            }
        }

        ,request: function( body ) {
            var id = strings.generateHexID( 64 );

            var deferred = new Deferred();
            this.requests[ id ] = {
                deferred: deferred
            }
            
            this.channel.send( REQUEST, { id: id, request: body } );
            
            return deferred;
        }

    });

    var Responder = declare([], {
    
        constructor: function( channel ) {
            this.channel = channel;
            this.responders = new Enrollment();
        }

        ,sendChannelMessage: function( topic, value ) {
            if( topic == REQUEST ) {
                
                var responders = this.responders.filter( function( r ){ 
                    return r.filter( value.request ) == true;
                });
                
                if( responders.length > 0 ) {
                    var status = { responded: false };
                    var channel = this.channel;
                    var reply = function( body ) {
                        if( ! status.responded ) {
                            status.responded = true;
                            channel.send( RESPONSE, { id: value.id, response: body });
                        }
                    }

                    array.forEach( responders, function( responder ){
                        var processed = responder.callback( value.request, reply );
                        if( processed !== undefined ) {
                            value.request = processed;
                        }
                    });
                }
            }
        }

        ,accept: function( filter, callback ) {
            if( callback === undefined ) {
                callback = filter;
                filter = function(){ return true; }
            }
            return this.responders.add({
                callback: callback
                ,filter: filter
            });
        }
    
    });

    var Channel = declare( [], {
        
        constructor: function( name ) {
            this.name = name;
            this.contributors = new Enrollment();
            this.listeners = {};
        }
        
        ,read: function() {
            if( this.contributors.length > 0 ) {
                var result = this.contributors.values().map( function( contributor ){
                    if( contributor && lang.isFunction( contributor.getChannelValue ) ) {
                        return contributor.getChannelValue();
                    } else {
                        return undefined;
                    }
                });
                return result.length > 1 ? result : result[ 0 ];
                
            } else {
                return this._value;
            }
        }
        
        ,write: function( value, priority ) {
            //this.log( "Writing... " );
            if( this.contributors.length > 0 ) {
                this.contributors.forEach( function( contributor ){
                    if( lang.isFunction( contributor.setChannelValue ) ) {
                    //console.log( "Writing value to " + contributor );
                        contributor.setChannelValue( value );
                    }
                });
            } else {
                this._value = value;
            }
            
            if( priority != true ) {
                this.notify( "change", value );
            }
        }
        
        ,active: function() {
            return this.contributors.length > 0;
        }
        
        ,send: function( topic, value ) {
            this.contributors.forEach( function( contributor ){
                if( contributor && lang.isFunction( contributor.sendChannelMessage ) ) {
                    contributor.sendChannelMessage( topic, value );
                }
            });
        }

        ,request: function( body, callback ) {
            if( this.requestor === undefined ) {
                this.requestor = new Requestor( this );
                this.join( this.requestor );
            }
            
            var req = this.requestor.request( body );
            
            if( callback ) {
                req.then( callback );
            }
            
            return req;
        }
        
        ,accept: function( filter, listener ) {
            if( this.responder === undefined ) {
                this.responder = new Responder( this );
                this.join( this.responder );
            }
            return this.responder.accept( filter, listener );
        }

        ,notify: function( topic, value ) {
            //this.log( "Notifying " +  evt + " => " + value );
            var name = this.name;
            var notify = function( listener ){
                if( lang.isFunction( listener.handleChannelMessage ) ) {
                    listener.handleChannelMessage( topic, value, name );
                } else {
                    listener( value );
                }
            }

            if( topic in this.listeners ) {
                this.listeners[ topic ].forEach( notify );
            }
            
            if( catchers.length > 0 ) {
                catchers.forEach( notify );
            }
        }
        
        ,publish: function() {
            if( this.publisher === undefined ) {
                this.publisher = this.join( new Publisher( this ) );
            }
            return this;
        }
        
        ,unpublish: function() {
            if( this.publisher ) {
                this.publisher.remove();
                delete this.publisher();
            }
            return this;
        }

        ,on: function( topic, listener ) {
            if( listener != null 
                && listener !== undefined 
                && ( lang.isFunction( listener ) || lang.isFunction( listener.handleChannelMessage ) ) ) {
                
                if( !( topic in this.listeners ) ) {
                    this.listeners[ topic ] = new Enrollment();
                }    
                
                return this.listeners[ topic ].add( listener );
            } else {
                return { remove: function() {} };
            }
        }
        
        ,remove: function( evt, listener ) {
            if( evt in this.listeners ) {
                this.listeners[ evt ].remove( listener );
            }    
        }
        
        ,join: function( contributor ) {
            if( ! this.contributors.contains( contributor ) ) {
                //this.log( "Joining " + contributor );
                return this.contributors.add( contributor );
            } else {
                return { remove: function() {} };
            }
        }
        
        ,leave: function( contributor ) {
            this.contributors.remove( contributor );
        }
        
        ,log: function( message ) {
            //console.log( "Channel " + this.name + ": "  + message );
            
        }
    
    });
    
    var ChannelManager = declare( declare.className( req ), [], {
        
        constructor: function() {
            this._channels = {};
            this._null = new Channel( "<null channel>" );
        }
        
        ,channel: function( name ) {
            if( strings.hasText( name ) ) {
                if( !( name in this._channels ) ) {
                    this._channels[ name ] = new Channel( name );
                }
                return this._channels[ name ];
            } else {
                return this._null;
            }
        }
        
        ,attach: function( listener ) {
            if( listener != null 
                && listener !== undefined 
                && ( lang.isFunction( listener ) || lang.isFunction( listener.handleChannelMessage ) ) ) {
                
                return catchers.add( listener );
            } else {
                return { remove: function() {} };
            }
            
        }
        
    })();
    
    var ForeignListener = declare( [],{
        
        constructor: function( channel ) {
            this.channel = channel
        }
        
        ,handleChannelMessage: function( topic, value ) {
            //console.log( "Foreign listener", topic, value );
            
            messages.bubble( "neverworks.channel.Notify", {
                channel: this.channel
                ,topic: topic
                ,value: value
            }, "*" );
            
        }
        
    });

    var ForeignCatcher = declare( [],{
        
        handleChannelMessage: function( topic, value, channel ) {
            //console.log( "Foreign catcher", topic, value );
            
            messages.bubble( "neverworks.channel.Send", {
                channel: channel
                ,topic: topic
                ,value: value
            }, "*" );
            
        }
        
    });

    messages.on( "neverworks.channel.Write", function( msg ) {
        if( msg.channel !== undefined ) {
            ChannelManager.channel( msg.channel ).write( msg.value, msg.priority );
        }
    });
    
    messages.on( "neverworks.channel.Subscribe", function( msg ){
        if( msg.channel !== undefined && msg.topic !== undefined ) {
            var subscription = ChannelManager.channel( msg.channel ).on( msg.topic, new ForeignListener( msg.channel ) );
        }
    });
    
    messages.on( "neverworks.channel.Attach", function( msg ){
        ChannelManager.attach( new ForeignCatcher( msg.channel ) );
    });
    
    messages.on( "neverworks.channel.Send", function( msg ){
        if( msg.channel !== undefined && msg.topic !== undefined ) {
            ChannelManager.channel( msg.channel ).send( msg.topic, msg.value );
        }
    });

    messages.on( "neverworks.channel.Read", function( msg ){
        if( msg.channel !== undefined ) {
            
            messages.bubble( "neverworks.channel.Value", {
                channel: msg.channel
                ,value:  ChannelManager.channel( msg.channel ).read()
            }, "*" );
            
        }
    });
    
    return ChannelManager;
    
});