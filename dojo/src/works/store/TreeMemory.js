define([ 
    "dojo/_base/declare"
    ,"dojo/store/Memory"
    ,"works/_WidgetUtils"
], function( declare, Memory, _WidgetUtils ){

	var TreeMemory = declare( [ Memory ], {
		childrenProperty: "children",
		rootName: "Root",
		rootId: 0,

        mayHaveChildren: function( object ){
	        return this.childrenProperty in object;
	    },

		getChildren: function( object, onComplete, onError ){
	       	onComplete( object[ this.childrenProperty ] );
	    },

	    getRoot: function( onItem, onError ){
			if( !this._root ){
				var root = {};
				root[ this.idProperty ] = this.rootId;
				root.name = this.rootName;
				root[ this.childrenProperty ] = this.data; 
				this._root = root;
			}
			onItem( this._root );
	    },

	    getLabel: function( object ){
	        return object.name;
	    },

		refresh: function( object ){
			object = object || this._root;
			this.onChildrenChange( object, object[ this.childrenProperty ] );
			this.onChange( object );
		},

		refreshAll: function( object ){
			object = object || this._root;
			this.refresh( object );
			if( this.childrenProperty in object ) {
				for( var i in object[ this.childrenProperty ] ) {
					this.refreshAll( object[ this.childrenProperty ][ i ] );
				}
			}
		},

		loadAll: function( data ) {
			this.data.splice( 0, this.data.length );
			for( var i in data ) {
				this.data.push( data[i] );
			}
			this.refreshAll();
		}
		
	});
    
    TreeMemory.markupFactory = _WidgetUtils.markupFactory;
    
    return TreeMemory;
});