define([
    "dojo/has"
    ,"dojo/json"
    ,"dojo/_base/json"
], function( has, json, baseJson ){
    var wrapper = {
        stringify: function( obj ){
            return json.stringify( obj );
        }
    }

    wrapper.parse = has( "ie" ) <= 7 
        ? function( str ) {
            return baseJson.fromJson( str );
        }
        : function( str ) {
            return json.parse( str );
        }
    
    return wrapper;
})