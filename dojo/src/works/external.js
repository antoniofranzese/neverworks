define([
    "dojo/io/script"
    ,"works/lang/strings"
], function( script ){
    
    return {
        load: function( id, require, callback ) {
            var base = require.baseUrl;
            var url = ( base.endsWith( "/" ) ? base.slice( 0, -1 ) : base ) + require.toUrl( id );
            //console.log( "External", url );
            script.get({ 
                url: url
            }).then( function(){
                //console.log( "External: " + id + " loaded" );
                callback();
            }) 
        }
    }
})
