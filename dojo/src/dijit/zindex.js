define([
    "dojo/_base/lang"
], function( lang  ){
    
    var associates = [];
    var beginZIndex = 1000;
    
    var ZIndex = {
        
        register: function( associate ) {
            if( associate !== null && associate !== undefined && lang.isFunction( associate.getTopZIndex ) ) {
                associates.push( associate );
            }
        }
        
        ,getNextZIndex: function() {
            var index = beginZIndex;
            for( var i = 0; i < associates.length; i++ ) {
                index = Math.max( index, associates[ i ].getTopZIndex() );
            }
            index += 2;
            
            //console.log( "Next index: " + index );
            return index;
        }
    }
    
    return ZIndex;
    
});