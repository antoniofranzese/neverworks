var profile = (function(){
    return {
        basePath: "."
        ,releaseDir: "release"
        ,releaseName: "dojo"
        ,action: "release"

        ,layerOptimize: "comments"
        ,optimize: "comments"
        ,cssOptimize: "comments"
        ,mini: true
        ,selectorEngine: 'lite'
        ,stripConsole: "none"

        ,staticHasFeatures: {
            "dojo-has-api": 1
        }

        ,packages:[
        {
            name: "works",
            location: "works"
        },{
            name: "dojo",
            location: "dojo"
        },{
            name: "dijit",
            location: "dijit"
        },{
            name: "dojox",
            location: "dojox"
        },{
            name: "gridx",
            location: "gridx"
        },{
            name: "ext",
            location: "ext"
        }
        
        ]

        ,layers: {
            "dojo/dojo" : {
                include: [
                    "dojo/main"
                    ,"dojo/parser"
                    ,"dojo/Stateful"
                    ,"dojo/Evented"
                    ,"dojo/Deferred"
                    ,"dojo/promise/all"
                    ,"dojo/i18n"
                    ,"dojo/window"
                    ,"dojo/text"
                ]
                ,exclude: []
                ,boot: true
                ,includeLocales: [ "en", "it" ]
            }
            
            ,"works/core" : {
                include:[
                    "works/app"
                    ,"works/registry"
                    ,"works/css"
                    ,"works/style"
                    ,"works/form/Manager"
                    ,"works/form/Label"
                    ,"works/form/Button" 
                    ,"works/form/CheckBox"
                    ,"works/form/RadioGroup"
                    ,"works/form/TextBox"
                    ,"works/form/DateTextBox"
                    ,"works/form/TimeTextBox"
                    ,"works/form/NumberTextBox"
                    ,"works/form/NumberSpinner"
                    ,"works/form/Image"
                    ,"works/form/Popup"
                    ,"works/form/TooltipPopup"
                    ,"works/form/ToolBar"
                    ,"works/form/manager/FormContentPane"
                    ,"works/menu/Menu"
                    ,"works/layout/BorderContainer"
                    ,"works/layout/GridLayout"
                    ,"works/layout/TabContainer"
                    ,"works/layout/Ribbon"
                    ,"works/layout/RibbonPane"
                    ,"works/layout/RibbonStrip"
                    ,"works/layout/RibbonContainer"
                    ,"dojox/layout/ScrollPane"
                    ,"works/layout/LabelPane"
                    ,"works/layout/PlainPane"
                    ,"works/layout/Screen"
                    ,"works/layout/Spacer"
                    ,"works/grid/AsyncManager"
                    ,"works/grid/SyncManager"
                    ,"works/web/Downloader"
                    ,"works/web/Uploader"
                    ,"works/runtime/errors/FormReload"
                    
                    ,"dijit/Calendar"
                    ,"dijit/form/TextBox"
                    ,"dijit/form/DateTextBox"
                    ,"dijit/form/TimeTextBox"
                    ,"dijit/form/NumberTextBox"
                ]
                ,exclude: []
                ,includeLocales: [ "en", "it" ]
            }
        }
    };
})();
