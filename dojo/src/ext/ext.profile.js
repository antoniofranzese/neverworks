var profile = (function(){
	var testResourceRe = /^dojo\/tests\//,

		amd = function(filename, mid){
			var list = {
				"ext/lz-string":1
				,"ext/jquery":1
				,"ext/lodash":1
				,"ext/backbone":1
				,"ext/less":1
			};
			return (mid in list);
        };

	return {
		resourceTags:{
			copyOnly: function(filename, mid){
				return !amd(filename, mid);
			},

			amd: function(filename, mid){
				return amd(filename, mid) && /\.js$/.test(filename);
			}
		}
	};
})();