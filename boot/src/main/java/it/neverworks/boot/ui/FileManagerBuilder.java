package it.neverworks.boot.ui;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import it.neverworks.lang.Strings;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileProvider;
import it.neverworks.ui.html.server.J2EEFileManager;
import it.neverworks.log.Logger;

public class FileManagerBuilder extends AbstractServletBuilder {

    private FileRepository fileRepository;
    private FileProvider fileProvider;
    private String filePath;
    private int loadPriority = 1;
    private String name;
    private Boolean downloads;
    private Boolean uploads;

    public FileManagerBuilder( FileRepository fileRepository ) {
        this.fileRepository = fileRepository;
        this.name = "file-manager-" + Strings.generateHexID( 8 );
    }

    public FileManagerBuilder mapFiles( String path ) {
        this.filePath = cleanupPath( path );
        return this;
    }

    public FileManagerBuilder loadPriority( int priority ) {
        this.loadPriority = priority;
        return this;
    }

    public FileManagerBuilder withName( String name ) {
        this.name = name;
        return this;
    }

    public FileManagerBuilder withFileProvider( FileProvider fileProvider ) {
        this.fileProvider = fileProvider;
        return this;
    }

    public FileManagerBuilder withDownloads() {
        this.downloads = true;
        return this;
    }

    public FileManagerBuilder withoutDownloads() {
        this.downloads = false;
        return this;
    }

    public FileManagerBuilder withUploads() {
        this.uploads = true;
        return this;
    }

    public FileManagerBuilder withoutUploads() {
        this.uploads = false;
        return this;
    }

    @Override
    public ServletRegistrationBean<HttpServlet> build() {
        ServletRegistrationBean<HttpServlet> registration = new ServletRegistrationBean<>();
        J2EEFileManager servlet = new J2EEFileManager();
        servlet.setFileRepository( this.fileRepository );

        if( this.fileProvider != null ) {
            servlet.setFileProvider( this.fileProvider );
        }

        if( this.downloads != null ) {
            servlet.setDownload( this.downloads );
        }

        if( this.uploads != null ) {
            servlet.setUpload( this.uploads );
        }

        List<String> urlMappings = new ArrayList<>();
        urlMappings.add( this.filePath );
        urlMappings.add( this.filePath + "/*" );

        log.info( "''{1}'' maps files to: {0}", urlMappings, this.name );

        registration.setName( this.name );
        registration.setUrlMappings( urlMappings );
        registration.setLoadOnStartup( this.loadPriority );
        registration.setServlet( servlet );

        return registration;
    }

}