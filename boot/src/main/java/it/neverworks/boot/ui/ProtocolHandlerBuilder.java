package it.neverworks.boot.ui;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import it.neverworks.lang.Strings;
import it.neverworks.ui.html.server.J2EEProtocolHandler;
import it.neverworks.ui.html.production.storage.MemoryStorage;
import it.neverworks.ui.html.production.ProtocolManager;
import it.neverworks.ui.html.production.HTMLPackageBasedMapper;
import it.neverworks.ui.html.production.HTMLProducerFactory;
import it.neverworks.ui.html.production.VelocityPageProducerFactory;
import it.neverworks.ui.html.production.Page;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.Factory;
import it.neverworks.log.Logger;

public class ProtocolHandlerBuilder extends AbstractServletBuilder {

    private ProtocolManager protocolManager;
    private Class<?> formClass;
    private String formPath;
    private int loadPriority = 1;
    private String name;

    public ProtocolHandlerBuilder( ProtocolManager protocolManager ) {
        this.protocolManager = protocolManager;
        this.name = "protocol-handler-" + Strings.generateHexID( 8 );
    }

    public ProtocolHandlerBuilder mapForm( Class<?> formClass, String path ) {
        this.formClass = formClass;
        this.formPath = cleanupPath( path );
        return this;
    }

    public ProtocolHandlerBuilder mapForms( String path ) {
        this.formClass = null;
        this.formPath = cleanupPath( path );
        return this;
    }

    public ProtocolHandlerBuilder loadPriority( int priority ) {
        this.loadPriority = priority;
        return this;
    }

    public ProtocolHandlerBuilder withName( String name ) {
        this.name = name;
        return this;
    }

    @Override
    public ServletRegistrationBean<HttpServlet> build() {
        ServletRegistrationBean<HttpServlet> registration = new ServletRegistrationBean<>();
        J2EEProtocolHandler servlet = new J2EEProtocolHandler();
        servlet.setProtocolManager( this.protocolManager );
        List<String> urlMappings = new ArrayList<>();

        if( this.formClass != null ) {
            servlet.setForm( this.formClass.getName() );
            urlMappings.add( this.formPath );
        }

        urlMappings.add( this.formPath + "/*" );

        log.info( "''{2}'' maps {0} to: {1}", this.formClass != null ? this.formClass.getName() : "all forms", urlMappings, this.name );

        registration.setName( this.name );
        registration.setUrlMappings( urlMappings );
        registration.setLoadOnStartup( this.loadPriority );
        registration.setServlet( servlet );

        return registration;
    }

}