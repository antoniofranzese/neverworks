package it.neverworks.boot.ui;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import it.neverworks.lang.Strings;
import it.neverworks.ui.html.server.J2EEProtocolHandler;
import it.neverworks.ui.html.production.storage.MemoryStorage;
import it.neverworks.ui.html.production.ProtocolManager;
import it.neverworks.ui.html.production.HTMLPackageBasedMapper;
import it.neverworks.ui.html.production.HTMLProducerFactory;
import it.neverworks.ui.html.production.VelocityPageProducerFactory;
import it.neverworks.ui.html.production.Page;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.Factory;
import it.neverworks.log.Logger;

public abstract class AbstractServletBuilder {

    @Logger.ForMe protected static Logger log;

    protected String cleanupPath( String path ) {
        //TODO: better regex
        return path.replaceAll( "/\\*", "" );
    }

    public abstract ServletRegistrationBean<HttpServlet> build();

}