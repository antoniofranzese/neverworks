package it.neverworks.boot.ui;

import java.util.Map;
import java.util.HashMap;
import java.util.function.Supplier;
import javax.servlet.http.HttpServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import it.neverworks.lang.Wrapper;
import it.neverworks.ui.html.server.J2EEProtocolHandler;
import it.neverworks.ui.html.production.storage.MemoryStorage;
import it.neverworks.ui.html.production.ProtocolManager;
import it.neverworks.ui.html.production.HTMLPackageBasedMapper;
import it.neverworks.ui.html.production.HTMLProducerFactory;
import it.neverworks.ui.html.production.VelocityPageProducerFactory;
import it.neverworks.ui.html.production.Page;
import it.neverworks.ui.production.WidgetStorage;
import it.neverworks.ui.production.Factory;
import it.neverworks.ui.production.Mapper;
import it.neverworks.log.Logger;

public class ProtocolManagerBuilder {

    @Logger.ForMe private static Logger log;

    private WidgetStorage storage;
    private String pageTemplate = "page-template.html";
    private Mapper mapper = new HTMLPackageBasedMapper();
    private Map<Class<?>, Factory> factories = new HashMap<>();

    public ProtocolManagerBuilder( WidgetStorage storage ) {
        this.storage = storage;
    }

    public ProtocolManagerBuilder produceWidget( Class<?> widgetClass, Factory factory ) {
        if( factory != null ) { 
            this.factories.put( widgetClass, factory ); 
        }
        return this;
    }

    public ProtocolManagerBuilder produceWidget( Class<?> widgetClass, Supplier<Factory> factoryWrapper ) {
        if( factoryWrapper != null ) { 
            this.factories.put( widgetClass, factoryWrapper.get() ); 
        }
        return this;
    }

    public ProtocolManagerBuilder withPageTemplate( String pageTemplate ) {
        this.pageTemplate = pageTemplate;
        return this;
    }

    public ProtocolManagerBuilder withMapper( Mapper mapper ) {
        this.mapper = mapper;
        return this;
    }

    public ProtocolManager build() {
        if( ! factories.containsKey( Page.class ) ) {
            produceWidget( Page.class, () -> {
                log.info( "Implicit Page producer using {}", pageTemplate );
                VelocityPageProducerFactory factory = new VelocityPageProducerFactory();
                factory.setTemplate( pageTemplate );
                factory.afterPropertiesSet();
                return factory;
            });
        }

        ProtocolManager protocolManager = new ProtocolManager();

        HTMLProducerFactory producerFactory = new HTMLProducerFactory();
        producerFactory.setMapper( this.mapper );
        producerFactory.setFactories( this.factories );
        protocolManager.setProducerFactory( producerFactory );

        protocolManager.setWidgetStorage( this.storage );

        return protocolManager;
    }

}
