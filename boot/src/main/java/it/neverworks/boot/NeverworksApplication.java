package it.neverworks.boot;

import java.util.Map;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationContext;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Framework;
import it.neverworks.lang.Application;
import it.neverworks.context.Context;
import it.neverworks.log.Logger;

public class NeverworksApplication implements BeanFactoryPostProcessor, ApplicationContextAware {
    
    private static Logger log = Logger.of( NeverworksApplication.class );

    @Override
    public void postProcessBeanFactory( ConfigurableListableBeanFactory beanFactory ) {
        for( Object app: beanFactory.getBeansWithAnnotation( SpringBootApplication.class ).values() ) {
            Package pkg = app.getClass().getPackage();
            log.info( "Registering package: {0.name}", pkg );
            Application.register( pkg );
        }
    }

    @Override
    public void setApplicationContext( ApplicationContext context ) {
        log.info( "Initializing application [NeverWorks/{}]", Framework.property( "revision" ) );
        Context.init( context );
    }

    public static void init( ApplicationContext context ) {
        NeverworksApplication app = new NeverworksApplication();
        app.setApplicationContext( context );
        app.postProcessBeanFactory( (ConfigurableListableBeanFactory) context );
    }

}