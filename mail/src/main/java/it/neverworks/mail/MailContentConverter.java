package it.neverworks.mail;

import java.util.Map;
import java.io.InputStream;
import it.neverworks.model.converters.ModelConverter;

public class MailContentConverter extends ModelConverter {
    
    public MailContentConverter() {
        super( MailContent.class );
    }
    
    public Object convert( Object value ) {
        if( value instanceof MailContent ) {
            return value;
        } else if( value instanceof Map ) {
            return super.convert( value );
        } else {
            return new MailContent().set( "content", value );
        }
    }
}