package it.neverworks.mail;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

import it.neverworks.model.converters.ModelConverter;

public class MailAddressConverter extends ModelConverter {
    
    private final static Pattern FULL_ADDRESS = Pattern.compile( "([^<>].*)\\s*(?:<([^>]*)>)" );
    
    public MailAddressConverter() {
        super( MailAddress.class );
    }
    
    public Object convert( Object value ) {
        if( value instanceof String ) {
            Matcher matcher = FULL_ADDRESS.matcher( (String) value );
            if( matcher.matches() ) {
                return new MailAddress()
                    .set( "title", matcher.group( 1 ).trim() )
                    .set( "mailbox", matcher.group( 2 ).trim() );
            } else {
                return new MailAddress()
                    .set( "mailbox", ((String) value).trim() );
            }
        } else {
            return super.convert( value );
        }
    }
}