package it.neverworks.mail;

import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.template.TemplateEngine;

public class MailComposer implements Model {
    
    @Property
    private TemplateEngine templates;
    
    @Property
    private MailSender sender;
    
    @Property
    private MessageParser parser;
    
    public MailProcess compose( String text ) {
        return new MailProcess()
            .set( "source", text )
            .set( "templates", this.templates )
            .set( "sender", this.sender )
            .set( "parser", this.parser );
    }

    public MessageParser getParser(){
        return this.parser;
    }
    
    public void setParser( MessageParser parser ){
        this.parser = parser;
    }
    
    public MailSender getSender(){
        return this.sender;
    }
    
    public void setSender( MailSender sender ){
        this.sender = sender;
    }
    
    public TemplateEngine getTemplates(){
        return this.templates;
    }
    
    public void setTemplates( TemplateEngine templates ){
        this.templates = templates;
    }
    
}