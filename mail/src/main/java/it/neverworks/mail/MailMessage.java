package it.neverworks.mail;

import java.util.Date;
import java.util.List;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.converters.AutoConvert;

public class MailMessage implements Model {
    
    @Property
    private MailAddress from;

    @Property
    private MailAddress replyTo;
    
    @Property @Collection( wrap = true )
    private List<MailAddress> to;
    
    @Property @Collection( wrap = true )
    private List<MailAddress> cc;
    
    @Property @Collection( wrap = true )
    private List<MailAddress> bcc;
    
    @Property @Collection( wrap = true )
    private List<MailContent> body;
    
    @Property @Collection( wrap = true )
    private List<MailContent> attachments;
    
    @Property @AutoConvert
    private Date sent;
    
    @Property
    private String subject;
    
    public MailMessage() {
        super();
    }
    
    public MailMessage( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public String getSubject(){
        return this.subject;
    }
    
    public void setSubject( String subject ){
        this.subject = subject;
    }

    public Date getSent(){
        return this.sent;
    }
    
    public void setSent( Date sent ){
        this.sent = sent;
    }

    public List<MailContent> getAttachments(){
        return this.attachments;
    }
    
    public void setAttachments( List<MailContent> attachments ){
        this.attachments = attachments;
    }
    
    public List<MailContent> getBody(){
        return this.body;
    }
    
    public void setBody( List<MailContent> body ){
        this.body = body;
    }
    
    public List<MailAddress> getBcc(){
        return this.bcc;
    }
    
    public void setBcc( List<MailAddress> bcc ){
        this.bcc = bcc;
    }
    
    public List<MailAddress> getCc(){
        return this.cc;
    }
    
    public void setCc( List<MailAddress> cc ){
        this.cc = cc;
    }
    
    public List<MailAddress> getTo(){
        return this.to;
    }
    
    public void setTo( List<MailAddress> to ){
        this.to = to;
    }
    
    public MailAddress getReplyTo(){
        return this.replyTo;
    }
    
    public void setReplyTo( MailAddress replyTo ){
        this.replyTo = replyTo;
    }
    
    public MailAddress getFrom(){
        return this.from;
    }
    
    public void setFrom( MailAddress from ){
        this.from = from;
    }

}