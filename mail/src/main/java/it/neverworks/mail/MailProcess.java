package it.neverworks.mail;

import static it.neverworks.language.*;

import java.io.InputStream;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.template.TemplateEngine;
import it.neverworks.template.TemplateContext;
import it.neverworks.io.Streams;

public class MailProcess implements Model {
    
    @Property
    private TemplateEngine templates;
    
    @Property
    private MailSender sender;
    
    @Property
    private MessageParser parser;
    
    @Property
    private Object source;
    
    private TemplateContext context;
    private Arguments properties = new Arguments();
    private MailMessage message;

    public MailProcess with( String name, Object value ) {
        return this.with( new Arguments().arg( name, value ) );
    }
    
    public MailProcess with( Arguments arguments ) {
        
        for( String key: arguments.keys() ) {

            if( ModelDescriptor.of( MailMessage.class ).has( key ) ) {
                if( this.properties == null ) {
                    this.properties = new Arguments();
                }
                this.properties.put( key, arguments.get( key ) );

            } else {
                if( this.context == null && this.templates != null ) {
                    this.context = this.templates.createContext();
                }
                this.context.put( key, arguments.get( key ) );
            }
        }

        return this;
    }

    public MailProcess from( Object from ) {
        return with( "from", from );
    }
    
    public MailProcess to( Object to ) {
        return with( "to", to );
    }

    public MailProcess cc( Object cc ) {
        return with( "cc", cc );
    }

    public MailProcess bcc( Object bcc ) {
        return with( "bcc", bcc );
    }
    
    public MailMessage message() {
        if( this.message == null ) {
            synchronized( this ) {
                if( this.message == null ) {

                    if( this.source instanceof MailMessage ) {
                        message = (MailMessage) source;

                    } else if( this.parser != null ) {
                        InputStream stream = null;

                        if( this.templates != null ) {
                            stream = this.templates.stream( source, this.context != null ? this.context : this.templates.createContext() );
                        } else if( this.source instanceof String ) {
                            stream = Streams.asStream( (String) source );
                        } else {
                            stream = type( InputStream.class ).process( source );
                        }
            
                        message = this.parser.parse( stream );
                        
                    } else if( this.source instanceof String ) {
                        message = new MailMessage()
                            .set( "body", this.source );
                    }

                    if( message != null ) {
                        if( this.properties != null ) {
                            message.set( this.properties );
                        }
                    } else {
                        throw new IllegalStateException( "Cannot process message" );
                    }
                    
                }
            }
        }
        
        return this.message;

    }
    
    public MailProcess using( MailSender sender ) {
        this.sender = sender;
        return this;
    }
    
    public MailProcess send() {
        if( this.sender != null ) {
            MailMessage message = this.message();
            if( message.value( "to" ).size() > 0 ) {
                this.sender.send( message );
            }
        }
        return this;
    }
    
    public Object getSource(){
        return this.source;
    }
    
    public void setSource( Object source ){
        this.source = source;
    }
    
    public MessageParser getParser(){
        return this.parser;
    }
    
    public void setParser( MessageParser parser ){
        this.parser = parser;
    }
    
    public MailSender getSender(){
        return this.sender;
    }
    
    public void setSender( MailSender sender ){
        this.sender = sender;
    }
    
    public TemplateEngine getTemplates(){
        return this.templates;
    }
    
    public void setTemplates( TemplateEngine templates ){
        this.templates = templates;
    }
    
}