package it.neverworks.mail;

import static it.neverworks.language.*;

import java.io.File;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import org.springframework.core.io.Resource;
import net.htmlparser.jericho.StreamedSource;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.EndTag;
import net.htmlparser.jericho.Attribute;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Dates;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.io.FileInfo;
import it.neverworks.encoding.html.HTMLParser;

public class HTMLMessageParser implements Model, MessageParser {

    protected final static Pattern CSS_URL = Pattern.compile( "url\\s*\\(\\s*[\\'\\\"]?([^\\)\\'\\\"]*)[\\'\\\"]?\\s*\\)" );

    @Property 
    protected Resource resources;

    @Property @Collection
    protected Map<String, MailResourcePlugin> plugins;
    
    public MailMessage parse( Object source ) {
        MailMessage message = new MailMessage();
        parse( message, HTMLParser.stream( source ) );
        return message;
    }
    
    protected void parse( MailMessage message, StreamedSource source ) {
        
        StringBuilder html = new StringBuilder();
        StringBuilder text = null;

        message.value( "body" ).add( new MailContent().set( "type", "text/html" ) );
 
		for( Segment segment : source ) {
            
            if( segment instanceof StartTag ) {
                StartTag tag = (StartTag)segment;
                String tagName = tag.getName();
                // System.out.println( "Start " + tagName );

                if( "meta".equals( tagName ) ) {
                    if( parseMeta( message, tag ) && text == null ) {
                        html.append( Strings.valueOf( segment ) );
                    }
                    
                } else if( "link".equals( tagName ) ) {
                    if( parseLink( message, tag ) && text == null ) {
                        html.append( Strings.valueOf( segment ) );
                    }
            
                } else if( "title".equals( tagName ) ) {
                    if( text == null ) {
                        text = new StringBuilder();
                    } else {
                        throw new IllegalStateException( "Unexpected <title> tag" );
                    }
                
                } else if( "body".equals( tagName ) ) {
                    String type = str( tag.getAttributeValue( "type" ) ).trim().toLowerCase();
                    if( empty( type ) ) {
                        type = "text/html";
                    }
  
                    if( type.equals( "text/html" ) ) {
                        Map<String,String> attributes = each( tag.getAttributes() ).dict( a -> pair( a.getName(), a.getValue() ) );
                        attributes.remove( "type" );
                        html.append( StartTag.generateHTML( "body", attributes, tag.isEmptyElementTag() ) );
  
                    } else {
                        if( text == null ) {
                            text = new StringBuilder();
                        } else {
                            throw new IllegalStateException( "Unexpected <body> tag" );
                        }
                        text = new StringBuilder();
                        message.value( "body" ).add( new MailContent().set( "type", type ) );
                    }

                } else if( "img".equals( tagName ) ) {
                    if( text == null ) {
                        html.append( parseImage( message, tag ) );
                    }

                } else {
                    if( ! "!--".equals( tagName ) ) {
                        ( text != null ? text : html ).append( Strings.valueOf( segment ) );
                    }
                }
                
            } else if( segment instanceof EndTag ) {
                EndTag tag = (EndTag)segment;
                String tagName = tag.getName();
                // System.out.println( "End " + tagName );
                
                if( "title".equals( tagName ) ) {
                    if( text != null ) {
                        message.set( "subject", Strings.trim( text ) );
                        text = null;
                    }
                } else if( "body".equals( tagName ) ) {
                    if( text != null ) {
                        MailContent body = message.query( "body" ).last().result();
                        body.set( "content", Strings.trim( text ) );
                        text = null;
                    } else {
                        html.append( Strings.valueOf( segment ) );
                    }
                    
                } else {
                    ( text != null ? text : html ).append( Strings.valueOf( segment ) );
                }
            
            } else {
                ( text != null ? text : html ).append( parseText( message, Strings.valueOf( segment ) ) );
            }

        }
        
        message.set( "body[0].content", Strings.trim( html ) );
    }

    protected String parseText( MailMessage message, String text ) {
        Matcher m = CSS_URL.matcher( text );
        while( m.find() ) {
            if( m.groupCount() > 0 ) {
                String original = m.group( 0 );
                String url = parseInlineContent( message, m.group( 1 ).trim(), /* type */ null );
                text = text.replace( original, msg( "url(\"{0}\")", url ) );
            }
        }
        return text;
    }
    
    protected String normalizeSlash( String str ) {
        return Strings.safe( str ).trim().replaceAll( "\\\\", "/" );
    }

    protected String parseImage( MailMessage message, StartTag tag ) {
 
        Map<String,String> attributes = each( tag.getAttributes() ).dict( a -> pair( a.getName(), a.getValue() ) );
        attributes.remove( "type" );
        attributes.put( "src", parseInlineContent( 
            message
            ,normalizeSlash( tag.getAttributeValue( "src" ) ) 
            ,normalizeSlash( tag.getAttributeValue( "type" ) ) 
        ));
        return StartTag.generateHTML( "img", attributes, tag.isEmptyElementTag() );
        
    }
    
    protected String parseInlineContent( MailMessage message, String href, String type ) {

        if( href.startsWith( "cid:" ) 
            || href.startsWith( "http:" )
            || href.startsWith( "https:" ) ) {
            return href;

        } else {
            if( ! empty( href ) ) {
                
                href = href.trim();
                
                MailContent content = message.query( "attachments" )
                    .by( "href" ).eq( href )
                    .and( "displacement" ).is( "inline" )
                    .result();
                    
                if( content == null ) {
            
                    content = parseContent( href )
                        .set( "displacement", "inline" );
            
                    if( ! empty( type ) ) {
                        content.set( "type", type );
                    }

                    String name = null;
                    do {
                        name = msg( "inline-{0}", Strings.generateHexID( 16 ) );
                    } while( message.query( "attachments" ).by( "name" ).eq( "name" ).any() );
                    
                    content.set( "name", name );
                    message.getAttachments().add( content );
                    
                }
            
                return msg( "cid:{0}", content.value( "name" ) );
            } else {
                return href;
            }
        }
        
    }
    
    protected boolean parseLink( MailMessage message, StartTag tag ) {
        String kind = Strings.hyphen2camel( tag.getAttributeValue( "rel" ) );
        boolean isAttachment = "attachment".equalsIgnoreCase( kind );
        boolean isInline = "inline".equalsIgnoreCase( kind );
        
        if( isAttachment || isInline ) {
            String href = normalizeSlash( tag.getAttributeValue( "href" ) );

            if( ! empty( href ) ) {
                
                String type = tag.getAttributeValue( "type" );
                String name = tag.getAttributeValue( "name" );
            
                MailContent content = parseContent( href )
                    .set( "displacement", isInline ? "inline" : "attachment" );
            
                if( ! empty( type ) ) {
                    content.set( "type", normalizeSlash( type ) );
                }
            
                if( ! empty( name ) ) {
                    content.set( "name", normalizeSlash( name ) );
                }
                
                if( isInline ) {
                    if( empty( content.get( "name" ) ) ) {
                        throw new IllegalArgumentException( "Inline content without name: " + Strings.valueOf( tag ) );
                    }
                    if( message.query( "attachments" ).by( "name" ).eq( "name" ).any() ) {
                        throw new IllegalArgumentException( "Duplicate inline content name: " + Strings.valueOf( tag ) );
                    }
                }
                
                message.getAttachments().add( content );
            }
            
            return false;
            
        } else {
            return true;
        }
        
    }
    
    protected MailContent parseContent( String href ) {
        MailContent content = null;
        if( href.startsWith( "file:" ) ) {
            String path = href.substring( 5 ).trim();
            if( ! empty( path ) ) {
                content = new MailContent()
                    .set( "href", href )
                    .set( "content", new File( path ) )
                    .set( "name", path.lastIndexOf( "/" ) > 0 ? path.substring( path.lastIndexOf( "/" ) + 1 ) : path );
            }

        } else if( href.startsWith( "classpath:" ) ) {
            String path = href.substring( 10 ).trim();
            if( ! empty( path ) ) {
                content = new MailContent()
                    .set( "href", href )
                    .set( "content", new ApplicationStreamProvider( path ) )
                    .set( "name", path.lastIndexOf( "/" ) > 0 ? path.substring( path.lastIndexOf( "/" ) + 1 ) : path );
            }
            
        } else if( href.startsWith( "resource:" ) && this.getResourcePath() != null ) {
            String path = href.substring( 9 ).trim();
            if( ! empty( path ) ) {
                while( path.startsWith( "/" ) ) {
                    path = path.substring( 1 );
                }
                content = new MailContent()
                    .set( "href", href )
                    .set( "content", new File( this.getResourcePath() + "/" + path ) )
                    .set( "name", path.lastIndexOf( "/" ) > 0 ? path.substring( path.lastIndexOf( "/" ) + 1 ) : path );
            }
        } else {
            int colon = href.indexOf( ":" );
            if( colon > 0 ) {
                String pluginName = href.substring( 0, colon );
                if( this.plugins != null && this.plugins.containsKey( pluginName ) ) {
                    String uri = href.substring( colon + 1 );
                    //System.out.println( "Resolving " + href + " with " + this.plugins.get( pluginName ) );
                    content = this.plugins.get( pluginName ).resolveMailResource( uri )
                        .set( "href", href );
                    //System.out.println( "Obtained " + content );
                }
            }
        }
        
        return content != null ? content : new MailContent().set( "href", href );
    }
    
    
    protected boolean parseMeta( MailMessage message, StartTag tag ) {
        String name = Strings.hyphen2camel( tag.getAttributeValue( "name" ) );
        String content = tag.getAttributeValue( "content" );
        
        if( "from".equalsIgnoreCase( name ) ) {
            MailAddress from = parseAddress( content );
            if( from != null ) {
                message.set( "from", from );
            }
            return false;
        
        } else if( "replyTo".equalsIgnoreCase( name ) ) {
            MailAddress from = parseAddress( content );
            if( from != null ) {
                message.set( "from", from );
            }
            return false;
    
        } else if( "to".equalsIgnoreCase( name ) ) {
            MailAddress to = parseAddress( content );
            if( to != null ) {
                message.getTo().add( to );
            }
            return false;
        
        } else if( "cc".equalsIgnoreCase( name ) ) {
            MailAddress cc = parseAddress( content );
            if( cc != null ) {
                message.getCc().add( cc );
            }
            return false;
            
        } else if( "bcc".equalsIgnoreCase( name ) ) {
            MailAddress bcc = parseAddress( content );
            if( bcc != null ) {
                message.getBcc().add( bcc );
            }
            return false;
        
        } else if( "subject".equalsIgnoreCase( name ) ) {
            if( ! empty( content ) ) {
                message.set( "subject", content.trim() );
            }
            return false;

        } else if( "sent".equalsIgnoreCase( name ) ) {
            if( Dates.isDate( content ) ) {
                message.set( "set", Dates.date( content ) );
            }
            return false;
        } else {
            return true;
        }
        
    }
    
    protected MailAddress parseAddress( String text ) {
        if( ! empty( text ) ) {
            int colon = text.indexOf( ":" );
            if( colon > 0 ) {
                return new MailAddress()
                    .set( "title", text.substring( 0, colon ).trim() )
                    .set( "mailbox", text.substring( colon + 1 ).trim() );
            } else {
                return type( MailAddress.class ).process( text );
            }
            
        } else {
            return null;
        }
    }

    protected String getResourcePath() {
        try {
            return ( this.resources != null && this.resources.getFile() != null ) ? this.resources.getFile().getAbsolutePath() : null;
        } catch( Exception ex ) {
            throw error( ex );
        }
    }

    public Resource getResources(){
        return this.resources;
    }
    
    public void setResources( Resource resources ){
        this.resources = resources;
    }
    
    public Map<String, MailResourcePlugin> getPlugins(){
        return this.plugins;
    }
    
    public void setPlugins( Map<String, MailResourcePlugin> plugins ){
        this.plugins = plugins;
    }

}