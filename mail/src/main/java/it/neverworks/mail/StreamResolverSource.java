package it.neverworks.mail;

import java.io.InputStream;
import java.io.IOException;
import org.springframework.core.io.InputStreamSource;
import it.neverworks.io.InputStreamResolver;

public class StreamResolverSource implements InputStreamSource {
    
    private InputStreamResolver resolver;
    private Object source;
    
    public StreamResolverSource( InputStreamResolver resolver, Object source ) {
        this.resolver = resolver;
    }
    
    public InputStream getInputStream() throws IOException {
        return this.resolver.resolveInputStream( this.source );
    }

}
