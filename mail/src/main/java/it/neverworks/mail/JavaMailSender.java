package it.neverworks.mail;

import static it.neverworks.language.*;

import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.ByteArrayResource;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.description.Required;
import it.neverworks.encoding.MIME;
import it.neverworks.io.Files;
import it.neverworks.io.Streams;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileRepository;
import it.neverworks.io.InputStreamProvider;

public class JavaMailSender implements Model, MailSender {
    
    @Property @Required
    private String host;
    
    @Property @AutoConvert @Required
    private Integer port = 25;
    
    @Property @Required
    private String protocol = "smtp"; 
    
    @Property
    private String username;
    
    @Property
    private String password;
    
    @Property
    private boolean debug = false; 
    
    @Property
    private FileRepository repository;
    
    public void send( MailMessage message ) {
        try {
            JavaMailSenderImpl javaSender = getJavaSender(); 
            MimeMessage mimeMessage = javaSender.createMimeMessage();

            boolean multipart = message.getBody().size() > 0 || message.getAttachments().size() > 0;
            MimeMessageHelper helper = new MimeMessageHelper( mimeMessage, multipart );
        
            helper.setFrom( convertAddress( message.get( "from" ) ) );
            
            if( message.get( "replyTo" ) != null ) {
                helper.setReplyTo( convertAddress( message.get( "replyTo" ) ) );
            }

            for( MailAddress to: message.getTo() ) {
                helper.addTo( convertAddress( to ) );
            }

            for( MailAddress cc: message.getCc() ) {
                helper.addCc( convertAddress( cc ) );
            }

            for( MailAddress bcc: message.getBcc() ) {
                helper.addBcc( convertAddress( bcc ) );
            }

            helper.setSubject( message.getSubject() );
            helper.setSentDate( message.get( "sent", new Date() ) );
        
            if( message.getBody().size() > 0 ) {
                String text = message.get( "body[0].content" ) instanceof String 
                    ? message.<String>get( "body[0].content" )
                    : Streams.asString( type( InputStream.class ).process( message.get( "body[0].content" ) ) );
                    
                helper.setText( 
                     text 
                    ,"text/html".equalsIgnoreCase( message.get( "body[0].type" ) )
                );
            }
        
            if( message.getAttachments().size() > 0 ) {
                for( MailContent attachment: message.getAttachments() ) {
                    if( attachment.getContent() != null ) {
                        MailContent processed = processAttachment( attachment );
                        
                        if( processed.value( "displacement" ).is( "inline" ) ) {
                            helper.addInline(
                                processed.get( "name" )
                                ,processed.get( "content" ) 
                                ,processed.get( "type")
                            );
                        } else {
                            helper.addAttachment(
                                processed.get( "name" )
                                ,processed.get( "content" ) 
                                ,processed.get( "type")
                            );
                        }
                    }
                }
            }
        
            javaSender.send( mimeMessage );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        
    }
    
    protected MailContent processAttachment( MailContent attachment ) throws IOException {
        String name = attachment.get( "name" );
        String type = attachment.get( "type" );
        Object content = attachment.get( "content" );
        InputStreamSource source = null;
        
        if( content instanceof File ) {
            source = new FileSystemResource( (File) content );
        
            if( empty( type ) ) {
                type = MIME.detect( (File) content );
            }
        
            if( empty( name ) ) {
                name = ((File) content).getName();
            }
        
        } else if( content instanceof FileInfo ) {
            FileInfo info = (FileInfo) content;
            FileInfo stored = null;
            
            if( info.isStatic() ) {
                stored = storeStream( info.getStream() );
                
            } else {
                stored = info;

            }
        
            source = new StreamProviderSource( new FileInfoStreamProvider( stored ) );
            
            if( empty( type ) && ! empty( info.get( "type" ) ) ) {
                type = info.get( "type" );
            }
            
            if( empty( name ) && ! empty( info.get( "name" ) ) ) {
                name = info.get( "name" );
            }
                
        } else if( content instanceof String ) {
            source = new ByteArrayResource( ((String) content).getBytes() );
            
            if( empty( type ) ) {
                type = "text/plain";
            }
        
            if( empty( name ) ) {
                name = msg( "attachment-{0}.txt", Strings.generateHexID( 8 ) );
            }
        
        } else if( content instanceof InputStreamProvider ) {
            source = new StreamProviderSource( (InputStreamProvider) content );
        
        } else if( content instanceof InputStream ) {
            FileInfo info = storeStream( (InputStream) content );
            source = new StreamProviderSource( new FileInfoStreamProvider( info ) );
 
        } else if( content instanceof InputStreamSource ) {
            source = (InputStreamSource) content;
            
        } else {
            throw new IllegalArgumentException( "Cannot process attachment: " + repr( content ) );
        }

        if( empty( type ) ) {
            type = MIME.detect( Streams.marking( source.getInputStream() ) );
        }

        if( empty( name ) ) {
            name = msg( "attachment-{0}{1}", Strings.generateHexID( 8 ), MIME.extension( type ) );
        }
 
        return new MailContent()
            .set( "name", name )
            .set( "type", type )
            .set( "content", source )
            .set( "displacement", attachment.get( "displacement" ) );
    }
    
    protected FileInfo storeStream( InputStream stream ) {
        FileRepository repo = this.repository != null ? this.repository : Files.temporary();
        return repo.create( arg( "stream", stream ) );
    }

    protected InternetAddress convertAddress( MailAddress address ) {
        if( address != null ) {
            try {
                if( empty( address.getTitle() ) ) {
                    return new InternetAddress( address.getMailbox() );
                } else {
                    return new InternetAddress( address.getMailbox(), address.getTitle() );
                }
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            return null;
        }
    }
    
    private final static ThreadLocal<JavaMailSenderImpl> javaMailSender = new ThreadLocal<JavaMailSenderImpl>();
    
    protected JavaMailSenderImpl getJavaSender() {
        if( this.javaMailSender.get() == null ) {
            String protocol = str( get( "protocol" ) ).toLowerCase();

            Properties props = new Properties();
            if( "smpts".equals( protocol ) || "ssl".equals( protocol ) ) {
                protocol = "smtps";
                props.put( msg( "mail.{0}.socketFactory.class", protocol ), "javax.net.ssl.SSLSocketFactory" );
                props.put( msg( "mail.{0}.socketFactory.fallback", protocol ), "false" );
            } else if( "tls".equals( protocol ) ) {
                protocol = "smtp";
                props.put( msg( "mail.{0}.starttls.enable", protocol ), "true" );
            }

            props.put( msg( "mail.{0}.quitwait", protocol ), "false" );
            
            if( ! empty( get( "username" ) ) || ! empty( get( "password" ) ) ) {
                props.put( msg( "mail.{0}.auth", protocol ), "true" );
            }
            
            //System.out.println( "DEBUG: " + this.debug );
            if( this.debug ) {
                props.put( "mail.debug", "true" );
            }
            
            JavaMailSenderImpl sender = new JavaMailSenderImpl();
            sender.setHost( get( "host" ) );
            sender.setPort( get( "port" ) );
            sender.setProtocol( protocol );
            sender.setJavaMailProperties( props );
            
            if( ! empty( get( "username" ) ) ) {
                sender.setUsername( get( "username" ) );
            }

            if( ! empty( get( "password" ) ) ) {
                sender.setPassword( get( "password" ) );
            }
            
            this.javaMailSender.set( sender );
        }
        
        return this.javaMailSender.get();
    }
    
    public String getProtocol(){
        return this.protocol;
    }
    
    public void setProtocol( String protocol ){
        this.protocol = protocol;
    }
    
    public Integer getPort(){
        return this.port;
    }
    
    public void setPort( Integer port ){
        this.port = port;
    }
    
    public String getHost(){
        return this.host;
    }
    
    public void setHost( String host ){
        this.host = host;
    }

    public String getPassword(){
        return this.password;
    }
    
    public void setPassword( String password ){
        this.password = password;
    }
    
    public String getUsername(){
        return this.username;
    }
    
    public void setUsername( String username ){
        this.username = username;
    }
    
    public boolean getDebug(){
        return this.debug;
    }
    
    public void setDebug( boolean debug ){
        this.debug = debug;
    }
    
    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }
    
}