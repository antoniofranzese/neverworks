package it.neverworks.mail;

import it.neverworks.model.converters.Converter;
import it.neverworks.io.InputStreamResolver;
import it.neverworks.io.FileProvider;

public class MailResourcePluginConverter implements Converter {
    public Object convert( Object value ) {
        if( value instanceof MailResourcePlugin ) {
            return value;
        } else if( value instanceof FileProvider ) {
            return new FileProviderPlugin( (FileProvider) value );
        } else if( value instanceof InputStreamResolver ) {
            return new InputStreamResolverPlugin( (InputStreamResolver) value );
        } else {
            return value;
        }
    }
}