package it.neverworks.mail;

import java.io.InputStream;
import it.neverworks.io.FileInfo;
import it.neverworks.io.InputStreamProvider;

public  class FileInfoStreamProvider implements InputStreamProvider {
    
    private FileInfo info;
    
    public FileInfoStreamProvider( FileInfo info ) {
        this.info = info;
    }
    
    public InputStream toStream() {
        return this.info.getStream();
    }

}
