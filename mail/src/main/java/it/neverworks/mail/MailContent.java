package it.neverworks.mail;

import static it.neverworks.language.*;

import java.util.List;
import java.io.InputStream;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.io.Streams;
import it.neverworks.io.NamedStream;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;

@Convert( MailContentConverter.class )
public class MailContent implements Model {

    @AutoConvert
    public enum Displacement {
        INLINE
        ,ATTACHMENT
    }

    @Property
    private String type;
    
    @Property
    private String name;
    
    @Property 
    private Object content;
    
    @Property
    private String href;
    
    @Property
    private Displacement displacement = Displacement.ATTACHMENT; 
    
    public Displacement getDisplacement(){
        return this.displacement;
    }
    
    public void setDisplacement( Displacement displacement ){
        this.displacement = displacement;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public Object getContent(){
        return this.content;
    }
    
    public void setContent( Object content ){
        this.content = content;
    }
    
    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }

    public String getHref(){
        return this.href;
    }
    
    public void setHref( String href ){
        this.href = href;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "!href" )
            .add( "!name" )
            .add( "!type" )
            .add( "!displacement" )
            .add( "content", Objects.className( this.content ) )
            .toString();
    }

}