package it.neverworks.mail;

public interface MailSender {

    public void send( MailMessage message ); 

}