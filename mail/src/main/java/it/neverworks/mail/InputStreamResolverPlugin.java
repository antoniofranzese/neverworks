package it.neverworks.mail;

import java.io.InputStream;
import it.neverworks.io.InputStreamResolver;

public class InputStreamResolverPlugin implements MailResourcePlugin {
    
    private InputStreamResolver streamResolver;
    
    public InputStreamResolverPlugin( InputStreamResolver streamResolver ) {
        this.streamResolver = streamResolver;
    }
    
    public MailContent resolveMailResource( String uri ) {
        return new MailContent()
            .set( "content", new StreamResolverSource( this.streamResolver, uri ) );
    }

}