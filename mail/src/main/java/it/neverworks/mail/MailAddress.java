package it.neverworks.mail;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.Identification;

@Convert( MailAddressConverter.class )
@Identification({ "!mailbox", "!title" })
public class MailAddress implements Model {
    
    @Property
    private String title;
    
    @Property
    private String mailbox;
    
    public MailAddress() {
        super();
    }
    
    public MailAddress( String title, String mailbox ) {
        this();
        this.title = title;
        this.mailbox = mailbox;
    }
    
    public MailAddress( String mailbox ) {
        this();
        this.mailbox = mailbox;
    }
    
    public MailAddress( Arguments arguments ) {
        this();
        set( arguments );
    }

    public String getMailbox(){
        return this.mailbox;
    }
    
    public void setMailbox( String mailbox ){
        this.mailbox = mailbox;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public void setTitle( String title ){
        this.title = title;
    }

}