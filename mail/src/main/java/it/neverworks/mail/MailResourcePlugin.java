package it.neverworks.mail;

import it.neverworks.model.converters.Convert;

@Convert( MailResourcePluginConverter.class )
public interface MailResourcePlugin {
    MailContent resolveMailResource( String uri );

}