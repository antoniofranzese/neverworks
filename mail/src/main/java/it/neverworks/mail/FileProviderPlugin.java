package it.neverworks.mail;

import it.neverworks.io.FileInfo;
import it.neverworks.io.FileProvider;
import it.neverworks.io.FileProviderStreamResolver;
import it.neverworks.mail.MailResourcePlugin;

public class FileProviderPlugin extends FileProviderStreamResolver implements MailResourcePlugin {
    
    public FileProviderPlugin() {
        super();
    }

    public FileProviderPlugin( FileProvider provider ) {
        super( provider );
    }

    public MailContent resolveMailResource( String uri ) {
        FileInfo info = super.resolveFileInfo( uri );
        return new MailContent()
            .set( "name", info.get( "name" ) )
            .set( "type", info.get( "type" ) )
            .set( "content", info );
    }
    
}