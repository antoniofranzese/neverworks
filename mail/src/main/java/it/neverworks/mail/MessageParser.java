package it.neverworks.mail;

public interface MessageParser {
    MailMessage parse( Object source );
}