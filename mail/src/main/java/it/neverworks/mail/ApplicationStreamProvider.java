package it.neverworks.mail;

import java.io.InputStream;
import it.neverworks.lang.Application;
import it.neverworks.io.InputStreamProvider;

public  class ApplicationStreamProvider implements InputStreamProvider {
    
    private String path;
    
    public ApplicationStreamProvider( String path ) {
        this.path = path;
    }
    
    public InputStream toStream() {
        return Application.resource( this.path );
    }

}
