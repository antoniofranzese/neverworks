package it.neverworks.mail;

import java.io.InputStream;
import java.io.IOException;
import org.springframework.core.io.InputStreamSource;
import it.neverworks.io.InputStreamProvider;

public  class StreamProviderSource implements InputStreamSource {
    
    private InputStreamProvider provider;
    
    public StreamProviderSource( InputStreamProvider provider ) {
        this.provider = provider;
    }
    
    public InputStream getInputStream() throws IOException {
        return this.provider.toStream();
    }

}
