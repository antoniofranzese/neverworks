package test.misc;

import org.apache.commons.beanutils.PropertyUtils;
import java.beans.PropertyDescriptor;
import test.it.neverworks.model.context.*;
import static it.neverworks.language.*;

public class Test {
    public static void main(String[] args) {
        Bean bean = new Bean();

        System.out.println( msg( "Class {0.class.name}", bean ) );

    }
}