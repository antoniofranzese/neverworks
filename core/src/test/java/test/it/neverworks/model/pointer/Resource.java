package test.it.neverworks.model.pointer;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.utils.Identification;

@Identification( "content" )
public class Resource extends BaseModel {
    
    @Property
    private String content;
    
    public String getContent(){
        return this.content;
    }
    
    public void setContent( String content ){
        this.content = content;
    }
    
}