package test.it.neverworks.model.undefined;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.ArrayList;

@RunWith( JUnit4.class )
public class UndefinedTest {

    @Test
    public void testPropertyUndefinedConditionTest() {
        UndefinedEntity e = new UndefinedEntity();
        
        assertFalse( e.model().defined( "name" ) );
        e.setName( "value" );
        assertTrue( e.model().defined( "name" ) );
        e.model().delete( "name" );
        assertFalse( e.model().defined( "name" ) );
        
        assertFalse( e.model().defined( "age" ) );
        e.setAge( 50 );
        assertTrue( e.model().defined( "age" ) );
        
        e.model().delete( "age" );
        assertFalse( e.model().defined( "age" ) );
        
        assertEquals( e.getAge(), 0 );
        
    }
}