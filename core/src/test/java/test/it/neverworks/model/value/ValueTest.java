package test.it.neverworks.model.value;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;

import it.neverworks.lang.Mapper;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Inspector;
import it.neverworks.lang.Calculator;
import it.neverworks.model.ModelException;
import it.neverworks.model.Value;

@RunWith( JUnit4.class )
public class ValueTest {

    @Test
    public void primitiveTypeValuesTest() {
        Entity e = new Entity()
            .set( "name", "MyName" )
            .set( "age", 30 )
            .set( "checked", true );

        assertEquals( e.value( "name" ), "MyName" );
        assertEquals( e.value( "age" ), 30 );
        assertEquals( e.value( "checked" ), true );

        assertTrue( e.value( "age" ).asInt() == 30 );
        assertTrue( e.value( "checked" ).asBool() );

        // Null/Empty
        assertFalse( e.value( "name" ).isEmpty() );
        Entity e2 = new Entity();

        assertTrue( e2.value( "name" ).isNull() );
        assertTrue( e2.value( "name" ).isEmpty() );

        e2.set( "name", "" );

        assertFalse( e2.value( "name" ).isNull() );
        assertTrue( e2.value( "name" ).isEmpty() );

        // Or
        Entity e3 = new Entity();
        assertEquals( e.value( "name" ).or( "nothing" ), "MyName" );
        assertEquals( e3.value( "name" ).or( "nothing" ), "nothing" );

    }

    @Test
    public void listTypeValuesManipulationTest() {
        Entity e = new Entity()
            .set( "list", new ArrayList() )
            .set( "intList", new ArrayList() )
            .set( "entityList", new ArrayList() );
        Entity e2 = new Entity()
            .set( "name", "MyName" );

        // Strings
        assertTrue( e.value( "list" ).isEmpty() );

        e.value( "list" )
            .add( "obj1" )
            .add( "obj2" )
            .add( e2.get( "name" ) );

        assertEquals( e.value( "list" ).size(), 3 );
        assertEquals( e.value( "list" ).get( 0 ), "obj1" );
        assertEquals( e.value( "list" ).get( 1 ), "obj2" );
        assertEquals( e.value( "list" ).get( 2 ), "MyName" );

        String names = "";
        for( Value obj: e.value( "list" ).each() ) {
            assertThat( obj, instanceOf( Value.class ) );
            names += obj.asString();
        }
        assertEquals( names, "obj1obj2MyName" );


        assertTrue( e.value( "list" ).contains( "obj2" ) );

        e.value( "list" ).remove( "obj2" ); // Remove by value

        assertFalse( e.value( "list" ).contains( "obj2" ) );

        assertEquals( e.value( "list" ).size(), 2 );
        assertEquals( e.value( "list" ).get( 0 ), "obj1" );
        assertEquals( e.value( "list" ).get( 1 ), "MyName" );

        e.value( "list" ).put( 1, "new" );
        assertEquals( e.value( "list" ).size(), 3 );
        assertEquals( e.value( "list" ).get( 0 ), "obj1" );
        assertEquals( e.value( "list" ).get( 1 ), "new" );
        assertEquals( e.value( "list" ).get( 2 ), "MyName" );

        e.value( "list" ).remove( 0 ); // Remove by key
        e.value( "list" ).remove( 0 ); // Remove by key

        assertEquals( e.value( "list" ).size(), 1 );
        assertEquals( e.value( "list" ).get( 0 ), "MyName" );

        assertFalse( e.value( "list" ).isEmpty() );

        // Integers
        assertTrue( e.value( "intList" ).isEmpty() );

        e.value( "intList" ).cat( 1, 2, 3, 4, 5 );

        assertEquals( e.value( "intList" ).size(), 5 );
        assertEquals( e.value( "intList" ).value( 0 ), 1 );

        // Entities
        e.value( "entityList" ).cat( list(
            new Entity().set( "name", "E1" )
            ,new Entity().set( "name", "E2" )
            ,new Entity().set( "name", "E3" )
        ));
        
        assertEquals( e.value( "entityList" ).size(), 3 );

        assertEquals( e.value( "entityList" ).value( 0 ).get( "name" ), "E1" );
        assertEquals( e.value( "entityList" ).value( 1 ).get( "name" ), "E2" );
        assertEquals( e.value( "entityList" ).value( 2 ).get( "name" ), "E3" );

        // Expression integration
        assertEquals( e.value( "entityList" ).get( "[0].name" ), "E1" );

        assertEquals( e.value( "entityList" ).value( 0 ).as( Entity.class ).getName(), "E1" );

        String entityNames = "";
        for( Entity ent: e.value( "entityList" ).each( Entity.class ) ) {
            entityNames += ent.getName();
        }
        assertEquals( entityNames, "E1E2E3" );

        entityNames = "";
        for( Value ev: e.value( "entityList" ).each() ) {
            entityNames += ev.value( "name" ).asString();
        }
        assertEquals( entityNames, "E1E2E3" );

    }

    @Test
    public void valueFunctionalElementsTest() {
        Value list = Value.of( list() ).cat( list(
            new Entity().set( "name", "MainEntity1" ).set( "age", 20 )
            ,new Entity().set( "name", "MainEntity2" ).set( "age", 10 )
            ,new Entity().set( "name", "MainEntity3" ).set( "age", 20 )
            ,new Entity().set( "name", "Entity4" ).set( "age", 30 )
            ,new Entity().set( "name", "Entity5" ).set( "age", 40 )
            ,new Entity().set( "name", "Entity6" ).set( "age", 50 )
            //,new Entity().set( "name", "Entity7" )
        ));


        // map
        List<String> names = list.each( Entity.class ).map( new Mapper<Entity,String>(){
            public String map( Entity value ) {
                return value.get( "name" );
            }
        }).list();

        assertThat( names, equalsAll( "MainEntity1", "MainEntity2", "MainEntity3", "Entity4", "Entity5", "Entity6" ) );

        // filter
        Value mains = Value.of( list.each().filter( new Filter<Value>(){
            public boolean filter( Value value ) {
                return value.value( "name" ).asString().startsWith( "Main" );
            }
        }));

        assertThat( mains.each(), equalsAll(
            list.get( 0 )
            ,list.get( 1 )
            ,list.get( 2 ) )
        );

        // filter + map
        List<String> mainNames = list.each()
            .filter( new Filter<Value>(){
                public boolean filter( Value value ) {
                    return value.value( "name" ).asString().startsWith( "Main" );
                }
            }).map( new Mapper<Value,String>(){
                public String map( Value value ) {
                    return value.value( "name" ).asString();
                }
            }).list();

        assertThat( mainNames, equalsAll( "MainEntity1", "MainEntity2", "MainEntity3" ) );
 
        int ageTotal = list.each().reduce( new Inspector<Value>(){
            int total = 0;
            public void inspect( Value item) {
                total += item.value( "age" ).asInt();
            }
        }).total;

        assertThat( ageTotal, is( 170 ) );

        Value ageGroup = list.each().reduce( new Inspector<Value>(){
            Value totals = Value.of( dict() );
            
            public void inspect( Value item ) {
                String group = item.value( "name" ).asString().startsWith( "Main" ) ? "main" : "other";

                if( !totals.contains( group ) ) {
                    totals.put( group, 0 );
                }

                totals.put( group, totals.value( group ).asInt() + item.value( "age" ).asInt() );
            }
        }).totals;

        assertThat( ageGroup.size(), is( 2 ) );
        assertThat( ageGroup.value( "main" ).asInt(), is( 50 ) );
        assertThat( ageGroup.value( "other" ).asInt(), is( 120 ) );
    }
    
    @Test
    public void valueInvokeTestTest() {
        Value value = Value.of( arg( "calc", new Calculator() ) );
        
        value.value( "calc.push" ).call( 10 );
        value.value( "calc.add" ).call( 5 );
        assertThat( value.value( "calc.toInt" ).call().asInt(), is( 15 ) );

        value.value( "calc.push" ).call( 3 );
        value.value( "calc.sub" ).call();
        assertThat( value.value( "calc.toInt" ).call().asInt(), is( 12 ) );

        Value calc = Value.of( new Calculator() );
        
        calc.value( "push" ).call( Value.of( 10 ) );
        calc.value( "add" ).call( Value.of( 5 ) );
        assertThat( calc.value( "toInt" ).call().asInt(), is( 15 ) );

        calc.value( "push" ).call( 3 );
        calc.value( "sub" ).call();
        assertThat( calc.value( "toInt" ).call().asInt(), is( 12 ) );

    }
    

}