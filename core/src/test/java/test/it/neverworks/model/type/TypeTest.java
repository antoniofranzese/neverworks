package test.it.neverworks.model.type;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import java.util.Date;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.converters.*;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.types.TypeDefinition;

@RunWith( JUnit4.class )
@SuppressWarnings( "deprecation" )
public class TypeTest {

    @Test
    public void objectPropertyTypeSpecificationTest() {
        Entity e = new Entity();
        
        e.setUncheckedInt( "10" );
        assertThat( e.getUncheckedInt(), instanceOf( Integer.class ) );

        e.setUncheckedInt( "A" );
        assertThat( e.getUncheckedInt(), instanceOf( String.class ) );
        
        e.setCheckedInt( "10" );
        assertThat( e.getCheckedInt(), instanceOf( Integer.class ) );

        e.setAutoCheckedInt( "20" );
        assertThat( e.getAutoCheckedInt(), instanceOf( Integer.class ) );
        
        e.setPerson( "MyName" );
        assertThat( e.getPerson(), instanceOf( Person.class ) );

    }
    
    @Test
    public void typeDefinitionTest() {
        TypeDefinition enumType = TypeFactory.build( MyEnum.class );
        assertThat( (MyEnum)enumType.process( "yes" ), is( MyEnum.YES ) );
        assertThat( enumType.process( null ), nullValue() );
        
        TypeDefinition autoEnumType = TypeFactory.build( AutoEnum.class );
        assertThat( autoEnumType.getConverter(), instanceOf( EnumConverter.class ) );
        assertThat( (AutoEnum)autoEnumType.process( "yes" ), is( AutoEnum.YES ) );
        assertThat( autoEnumType.process( null ), nullValue() );
        
    }
    
    @Test( expected = IllegalArgumentException.class ) 
    public void objectPropertyWrongTypeTest() {
        Entity e = new Entity();
        e.setCheckedInt( "A" );
    }

    @Test( expected = IllegalArgumentException.class ) 
    public void objectAutoPropertyWrongTypeTest() {
        Entity e = new Entity();
        e.setAutoCheckedInt( "A" );
    }

    @Test( expected = IllegalArgumentException.class ) 
    public void objectInheritedConverterPropertyWrongTypeTest() {
        Entity e = new Entity();
        e.setPerson( 10 );
    }

}