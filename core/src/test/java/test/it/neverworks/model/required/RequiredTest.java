package test.it.neverworks.model.required;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.RequiredPropertyException;

@RunWith( JUnit4.class )
public class RequiredTest {

    @Test
    public void propertySkipRequiredValueCheckTest() {
        Entity e = new Entity();
        assertThat( e.model().unchecked().get( "name" ), nullValue() ); //No error
        
        e.setName( "myname" );
        assertEquals( e.model().front().get( "name" ), "myname" ); //No error
        
    }

    @Test( expected = RequiredPropertyException.class )
    public void propertyRequiredValueErrorTest() {
        Entity e = new Entity();
        
        System.out.println( e.model().<String>get( "name" ) );
    }

}