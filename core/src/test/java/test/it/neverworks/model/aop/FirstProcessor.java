package test.it.neverworks.model.aop;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

public class FirstProcessor implements MethodProcessor {
    
    private String value;
    
    public FirstProcessor( AnnotationInfo<First> info ) {
        this.value = info.annotation().value();
    }
    
    public void processMethod( MethodDescriptor method ) {
        //System.out.println( "First processing " + method.getActual() );
        method.place( new FirstInterceptor( value ) ).before( SecondInterceptor.class, AnotherInterceptor.class );
    }
    
}