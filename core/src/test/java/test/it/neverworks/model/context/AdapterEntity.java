package test.it.neverworks.model.context;

import java.util.Map;
import java.util.List;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.converters.ClassConverter;

public class AdapterEntity extends BaseModel {
    
    @Property
    private Label label;

    @Property @Collection( key = ClassConverter.class )
    private Map<Class, Label> labelMap;
    
    @Property @Collection
    private List<Label> labelList;
    
    public List<Label> getLabelList(){
        return this.labelList;
    }
    
    public void setLabelList( List<Label> labelList ){
        this.labelList = labelList;
    }
    
    public Map<Class, Label> getLabelMap(){
        return this.labelMap;
    }
    
    public void setLabelMap( Map<Class, Label> labelMap ){
        this.labelMap = labelMap;
    }

    public Label getLabel(){
        return this.label;
    }
    
    public void setLabel( Label label ){
        this.label = label;
    }

}