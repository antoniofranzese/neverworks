package test.it.neverworks.model.io;

import java.util.Date;
import java.util.List;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.collections.Collection;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.DateConverter;

import it.neverworks.model.io.Internal;
import it.neverworks.model.io.Export;
import it.neverworks.model.io.Qualified;

public class Entity extends BaseModel {
    
    @Property         
    private String name;
     
    @Property @Convert( IntConverter.class ) 
    private int age;
    
    @Property @AutoConvert @Internal
    private int code;

    @Property @AutoConvert
    private String address;
    
    @Property @Convert( DateConverter.class )
    private Date birth;
    
    @Property
    private ModelChild child;
    
    @Property @Export
    private SimpleChild simpleChild1;
    
    @Property
    private SimpleChild simpleChild2;
    
    @Property @Collection @Qualified
    private List untypedChildren;
    
    @Property @Collection
    private List<ModelChild> typedChildren;
    
    @Property
    private City city;
    
    public List<ModelChild> getTypedChildren(){
        return this.typedChildren;
    }
    
    public void setTypedChildren( List<ModelChild> typedChildren ){
        this.typedChildren = typedChildren;
    }
    
    public List getUntypedChildren(){
        return this.untypedChildren;
    }
    
    public void setUntypedChildren( List children ){
        this.untypedChildren = children;
    }
    
    public SimpleChild getSimpleChild2(){
        return this.simpleChild2;
    }
    
    public void setSimpleChild2( SimpleChild simpleChild2 ){
        this.simpleChild2 = simpleChild2;
    }
    
    public SimpleChild getSimpleChild1(){
        return this.simpleChild1;
    }
    
    public void setSimpleChild1( SimpleChild simpleChild1 ){
        this.simpleChild1 = simpleChild1;
    }
    
    public ModelChild getChild(){
        return this.child;
    }
    
    public void setChild( ModelChild child ){
        this.child = child;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
       
    public int getCode(){
        return this.code;
    }
    
    public void setCode( int code ){
        this.code = code;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    public Date getBirth(){
        return this.birth;
    }
    
    public void setBirth( Date birth ){
        this.birth = birth;
    }
    
    public City getCity(){
        return this.city;
    }
    
    public void setCity( City city ){
        this.city = city;
    }
        
}
