package test.it.neverworks.model.type;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.EnumConverter;

@Convert( EnumConverter.class )
public enum MyEnum {
    YES,
    NO,
    PERHAPS
}