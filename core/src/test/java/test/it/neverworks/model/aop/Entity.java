package test.it.neverworks.model.aop;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class Entity extends BaseModel {
    
    @Property
    protected String name;
    
    public Object[] refs = new Object[]{ this };
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public String makeName( String prefix ) {
        return prefix + ">" + this.name;
    }
    
    @Loggable
    @First( "ein" )
    public String interceptedGetName() {
        return "$" + this.name;
    }

    @Loggable
    @Second( "zwei" )
    @First( "ein" )
    public String interceptedMakeName( Object prefix ) {
        return prefix.toString() + "#" + this.name;
    }

    @Loggable
    @First( "one" )
    @Another( "other" )
    @Second( "two" )
    public String interceptedMakeName( String prefix ) {
        return prefix + ">" + this.name;
    }

    @Loggable
    @Second( "due" )
    @First( "uno" )
    public String interceptedMakeName2( String prefix, String suffix ) {
        return prefix + ">" + this.name + "<" + suffix;
    }

    @Second( "B" )
    @Another( "A" )
    @Loggable
    public String interceptedMakeName3( String prefix ) {
        return prefix + ">" + this.name;
    }

    @Another( "C" )
    @Second( "D" )
    public String interceptedMakeName4( String prefix ) {
        return prefix + ">" + this.name;
    }
    
    @Checked( "MISSING" )
    public String checkedMakeName( String prefix ) {
        return uncheckedMakeName( prefix );
    }
    
    @Loggable
    public String uncheckedMakeName( String prefix ) {
        if( prefix != null ) {
            return prefix + ">" + this.name;
        } else {
            throw new MissingValueException();
        }
    }


}