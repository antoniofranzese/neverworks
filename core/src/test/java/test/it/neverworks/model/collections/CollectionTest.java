package test.it.neverworks.model.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static it.neverworks.language.*;
import static it.neverworks.test.TestUtils.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

@RunWith( JUnit4.class )
public class CollectionTest {

    @Test
    public void interceptedListTest() {
        Entity e = new Entity().set( "name", "Entity1" );
        
        e.value( "interceptedList" ).add( new SubEntity().set( "name", "Sub1" ) );
        
        assertThat( e.<List>get( "interceptedList" ).size(), is( 1 ) );
        assertThat( e.<Entity>get( "interceptedList[0].parent" ), sameInstance( e ) );
        
        SubEntity sub = e.get( "interceptedList[0]" );
        assertThat( sub.getParent(), sameInstance( e ) );
        
        e.<List>get( "interceptedList" ).remove( 0 );
        assertThat( e.<List>get( "interceptedList" ).size(), is( 0 ) );
        
        assertThat( sub.getParent(), nullValue() );
        
    }
    
    @Test
    public void listItemConversionTest() {
        Entity e = new Entity().set( "name", "Entity1" );

        e.value( "autoConvertedList" ).cat( "Child1", "Child2", "Child3" );
        e.value( "autoConvertedList" ).cat( new AutoSubEntity().set( "name", "Child4" ), "Child5", new AutoSubEntity().set( "name", "Child6" ) );
        
        StringBuilder names = new StringBuilder();
        for( Object c: e.<List>get( "autoConvertedList" ) ) {
            assertThat( c, instanceOf( AutoSubEntity.class ) );
            names.append( ((AutoSubEntity) c).getName() );
        }
        assertThat( names.toString(), is( "Child1Child2Child3Child4Child5Child6" ) );
    }

    @Test
    public void wholeListConversionTest() {
        List ls = list( 
            new SubEntity().set( "name", "Sub1" )
            ,new SubEntity().set( "name", "Sub2" )
            ,new SubEntity().set( "name", "Sub3" )
        );
        Entity e1 = new Entity();
        e1.set( "interceptedList", ls );
        
        assertThat( e1.value( "interceptedList" ).size(), is( 3 ) );
        for( SubEntity item: e1.<List<SubEntity>>get( "interceptedList" ) ) {
            assertThat( item.getParent(), sameInstance( e1 ) );
        }
        
        e1.value( "interceptedList" ).cat( 
            new SubEntity().set( "name", "Sub4" )
            ,new SubEntity().set( "name", "Sub5" )
            ,new SubEntity().set( "name", "Sub6" )
        );
        
        assertThat( e1.value( "interceptedList" ).size(), is( 6 ) );
        for( SubEntity item: e1.<List<SubEntity>>get( "interceptedList" ) ) {
            assertThat( item.getParent(), sameInstance( e1 ) );
        }
        
        // Reassignment
        Entity e2 = new Entity();
        e2.set( "interceptedList", e1.get( "interceptedList" ) );
        
        assertThat( e2.value( "interceptedList" ).size(), is( 6 ) );
        for( SubEntity item: e2.<List<SubEntity>>get( "interceptedList" ) ) {
            assertThat( item.getParent(), sameInstance( e2 ) );
        }
        
        // Array conversion
        e1.set( "untypedList", list( "one", "two", "three" ) );
        
        assertThat( e1.value( "untypedList" ).size(), is( 3 ) );
        
        StringBuilder names = new StringBuilder();
        for( String str: e1.<List<String>>get( "untypedList" ) ) {
            names.append( str );
        }
        assertThat( names.toString(), is( "onetwothree" ) );
        
        e2.set( "autoConvertedList", e1.get( "untypedList" ) );
        
        names = new StringBuilder();
        for( AutoSubEntity se: e2.<List<AutoSubEntity>>get( "autoConvertedList" ) ) {
            names.append( se.getName() );
        }
        assertThat( names.toString(), is( "onetwothree" ) );
        
    }
    
    
    @Test( expected = IllegalArgumentException.class ) 
    public void wrongListItemTypeTest(){
        Entity e = new Entity();
        
        e.value( "plainList" ).add( new ChildEntity().set( "name", "Child1" ) );
        
    }
    
    @Test( expected = IllegalArgumentException.class ) 
    public void cannotConvertItemTest(){
        Entity e = new Entity();
        
        e.value( "autoConvertedList" ).add( 0 );
        
    }
    
    @Test( expected = IllegalArgumentException.class ) 
    public void cannotConvertSingleItemTest(){
        Entity e = new Entity();
        
        e.set( "untypedList", list( "A", "b", 0 ) );
        
        e.set( "autoConvertedList", e.get( "untypedList" ) );
        
    }
    
    @Test
    public void interceptedMapTest() {
        Entity e = new Entity().set( "name", "E1" );
        
        e.value( "interceptedMap" )
            .put( "one", new SubEntity().set( "name", "S1" ) )
            .put( "two", new SubEntity().set( "name", "S2" ) )
            .put( "three", new SubEntity().set( "name", "S3" ) );
        
        assertThat( e.<Map>get( "interceptedMap" ).size(), is( 3 ) );
        
        for( SubEntity se: e.<Map<String, SubEntity>>get( "interceptedMap" ).values() ) {
            assertThat( se.getParent(), sameInstance( e ) );
        }
        
        SubEntity se1 = e.get( "interceptedMap[one]" );
        
        e.value( "interceptedMap" ).remove( "one" );
        assertThat( e.value( "interceptedMap" ).size(), is( 2 ) );
        assertThat( se1.getParent(), nullValue() );
        
    }
    
    @Test
    public void autoConvertedMapTest() {
        Entity e = new Entity();
        e.set( "autoConvertedMap", 
            arg( "one", "E1" )
            .arg( "two", new AutoSubEntity().set( "name", "E2" ) )
        );
        assertThat( e.get( "autoConvertedMap[ one ]" ), instanceOf( AutoSubEntity.class ) );
        assertThat( e.get( "autoConvertedMap[ two ]" ), instanceOf( AutoSubEntity.class ) );
        assertThat( e.<String>get( "autoConvertedMap[ one ].name" ), is( "E1" ) );
        assertThat( e.<String>get( "autoConvertedMap[ two ].name" ), is( "E2" ) );

        e.set( "autoBooleans",
            arg( "one", true )
            .arg( "two", "false" )
            .arg( "three", 0 )
        );

        assertThat( e.<Boolean>get( "autoBooleans[one]" ), is( true ) );
        assertThat( e.<Boolean>get( "autoBooleans[two]" ), is( false ) );
        assertThat( e.<Boolean>get( "autoBooleans[three]" ), is( false ) );

        e.set( "autoBooleans2",
            arg( "one", true )
            .arg( "two", "false" )
            .arg( "three", 0 )
        );

        assertThat( e.<Boolean>get( "autoBooleans2[one]" ), is( true ) );
        assertThat( e.<Boolean>get( "autoBooleans2[two]" ), is( false ) );
        assertThat( e.<Boolean>get( "autoBooleans2[three]" ), is( false ) );
    }
    
    @Test
    public void childPropagatingCollectionTest() {
        Entity e = new Entity().set( "name", "E1" );
        
        ChildEntity c = new ChildEntity().set( "name", "C1" );
        assertThat( c.getParent(), nullValue() );
        
        e.getChildList().add( c );
        assertThat( c.getParent(), is( e ) );

        e.getChildList().remove( 0 );
        assertThat( c.getParent(), nullValue() );
        
        Entity e2 = new Entity().set( "name", "E2" );
        
        e2.getChildMap().put( "c", c );
        assertThat( c.getParent(), is( e2 ) );
        
        e2.getChildMap().remove( "c" );
        assertThat( c.getParent(), nullValue() );

    }
    
    @Test
    public void explicitMapItemConversionTest() {
        Entity e = new Entity()
            .set( "booleans[one]", "true" )
            .set( "booleans[two]", "yes" )
            .set( "booleans[three]", "no" )
            .set( "booleans[four]", "false" );
            
            assertThat( e.<Boolean>get( "booleans[one]" ), is( Boolean.TRUE ) );
            assertThat( e.<Boolean>get( "booleans[two]" ), is( Boolean.TRUE ) );
            assertThat( e.<Boolean>get( "booleans[three]" ), is( Boolean.FALSE ) );
            assertThat( e.<Boolean>get( "booleans[four]" ), is( Boolean.FALSE ) );
        
    }
    
}