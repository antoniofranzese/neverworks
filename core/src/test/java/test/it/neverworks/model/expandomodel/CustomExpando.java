package test.it.neverworks.model.expandomodel;

import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.Property;

public class CustomExpando extends ExpandoModel {
    
    @Property
    private String meta;
    
    @Property @AutoConvert
    private Integer number;
    
    private String sealed;
    
    public String getSealed(){
        return this.sealed;
    }
    
    public void setSealed( String sealed ){
        this.sealed = sealed;
    }
    
    public String getMeta(){
        return this.meta;
    }
    
    public void setMeta( String meta ){
        this.meta = meta;
    }
    
}