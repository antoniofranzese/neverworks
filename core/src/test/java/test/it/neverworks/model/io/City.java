package test.it.neverworks.model.io;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

public class City extends BaseModel {
    
    @Property
    private String name;
    
    @Property
    private String province;
    
    public String getProvince(){
        return this.province;
    }
    
    public void setProvince( String province ){
        this.province = province;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
}