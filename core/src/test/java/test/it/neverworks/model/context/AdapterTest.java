package test.it.neverworks.model.context;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.context.Context;


@RunWith( JUnit4.class )
public class AdapterTest {

    @Test
    public void contextModelAdapterTest() {
        
        within( Context.init( "/model/context/adapt-context.xml" ), ctx -> {

            // Simple type conversion
            AdapterEntity text1 = Context.get( "text1" );
            assertThat( text1.getLabel(), instanceOf( Label.class ) );
            assertThat( text1.getLabel().getValue(), is( "Text 1" ) );

            // List conversion
            assertThat( text1.getLabelList().get( 0 ), instanceOf( Label.class ) );
            assertThat( text1.getLabelList().get( 0 ).getValue(), is( "String 1" ) );
            assertThat( text1.getLabelList().get( 1 ), instanceOf( Label.class ) );
            assertThat( text1.getLabelList().get( 1 ).getValue(), is( "Integer 1" ) );
            assertThat( text1.getLabelList().get( 2 ), instanceOf( Label.class ) );
            assertThat( text1.getLabelList().get( 2 ).getValue(), is( "Native 1" ) );

            // Map conversion
            assertTrue( text1.getLabelMap().containsKey( String.class ) );
            assertTrue( text1.getLabelMap().containsKey( Integer.class ) );
            assertThat( text1.getLabelMap().get( String.class ), instanceOf( Label.class ) );
            assertThat( text1.getLabelMap().get( String.class ).getValue(), is( "String 2" ) );
            assertThat( text1.getLabelMap().get( Integer.class ), instanceOf( Label.class ) );
            assertThat( text1.getLabelMap().get( Integer.class ).getValue(), is( "Integer 2" ) );
            
        });
    
    }
    
}