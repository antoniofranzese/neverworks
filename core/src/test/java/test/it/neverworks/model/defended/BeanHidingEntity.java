package test.it.neverworks.model.defended;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class BeanHidingEntity extends BaseEntity {

    private String simple;
    
    public String getSimple(){
        return this.simple;
    }
    
    public void setSimple( String simple ){
        this.simple = simple;
    }
}