package test.it.neverworks.model.converter;

import java.util.Date;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Booleans;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.LocalConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.DateConverter;
import it.neverworks.model.converters.ModelConverter;
import it.neverworks.model.converters.EnumConverter;

public class ConverterEntity extends BaseModel {
    
    public enum Gender{
        MALE, FEMALE
    }
    
    public static class Inner extends BaseModel {

        @Property @Convert( DateConverter.class )
        private Date date;
        
        public Date getDate(){
            return this.date;
        }
        
        public void setDate( Date date ){
            this.date = date;
        }
        
        @Property @AutoConvert
        private int integer;
        
        public int getInteger(){
            return this.integer;
        }
        
        public void setInteger( int integer ){
            this.integer = integer;
        }
    }
    
    @Property         
    private String name;
     
    @Property @Convert( IntConverter.class ) 
    private int age;
    
    @Property @AutoConvert
    private int code;
    
    @Property @AutoConvert
    private String address;
    
    @Property @Convert( DateConverter.class )
    private Date birth;
    
    @Property
    private Person person;
    
    @Property @AutoConvert
    private boolean active;
    
    @Property @AutoConvert
    private Kind kind;

    @Property @AutoConvert
    private Gender gender;
    
    @Property @Convert( ModelConverter.class )        
    private Entity sub;
    
    @Property
    private Inner inner;
    
    @Property
    private AutoKind autoKind;
    
    private Person classicPerson;
    
    @Property @LocalConvert
    private Boolean visible;
    
    protected static Object convertVisible( Object value ) {
        if( value instanceof String && "sure!".equalsIgnoreCase( (String) value ) ) {
            return true;
        } else if( value instanceof String && "nope!".equalsIgnoreCase( (String) value ) ) {
            return false;
        } else if( value != null ) {
            return Booleans.bool( value );
        } else {
            return value;
        }
    }
    
    @Property @LocalConvert
    private String index;
    
    protected Object convertIndex( Object value ) {
        return "<" + Strings.valueOf( value ) + ">";
    }
    
    public String getIndex(){
        return this.index;
    }
    
    public void setIndex( String index ){
        this.index = index;
    }
    
    private Boolean lockable;
    
    public Boolean getLockable(){
        return this.lockable;
    }
    
    @LocalConvert( "convertVisible" )
    public void setLockable( Boolean lockable ){
        this.lockable = lockable;
    }
    
    @Property @AutoConvert @LocalConvert
    private Boolean chainable;
    
    protected Object convertChainable( Object value ) {
        if( value instanceof String && "sure!".equalsIgnoreCase( (String) value ) ) {
            return true;
        } else if( value instanceof String && "nope!".equalsIgnoreCase( (String) value ) ) {
            return false;
        } else {
            return value;
        }
    }

    public Boolean getChainable(){
        return this.chainable;
    }
    
    public void setChainable( Boolean chainable ){
        this.chainable = chainable;
    }
    
    @Property @AutoConvert @LocalConvert( "convertChainable" )
    private boolean primitiveChainable;
    
    public boolean getPrimitiveChainable(){
        return this.primitiveChainable;
    }
    
    public void setPrimitiveChainable( boolean primitiveChainable ){
        this.primitiveChainable = primitiveChainable;
    }
    
    public Person getClassicPerson(){
        return this.classicPerson;
    }
    
    @Convert( PersonConverter.class )
    public void setClassicPerson( Person classicPerson ){
        this.classicPerson = classicPerson;
    }
    
    private Person classicAutoPerson;
    
    public Person getClassicAutoPerson(){
        return this.classicAutoPerson;
    }
    
    @AutoConvert
    public void setClassicAutoPerson( Person classicAutoPerson ){
        this.classicAutoPerson = classicAutoPerson;
    }
    
    private Kind classicKind;
    
    public Kind getClassicKind(){
        return this.classicKind;
    }
    
    @Convert( EnumConverter.class )
    public void setClassicKind( Kind classicKind ){
        this.classicKind = classicKind;
    }
    
    private Kind classicAutoKind;
    
    @Convert( EnumConverter.class )
    public Kind getClassicAutoKind(){
        return this.classicAutoKind;
    }
    
    public void setClassicAutoKind( Kind classicAutoKind ){
        this.classicAutoKind = classicAutoKind;
    }
    
    public AutoKind getAutoKind(){
        return this.autoKind;
    }
    
    public void setAutoKind( AutoKind autoKind ){
        this.autoKind = autoKind;
    }
    
    public Inner getInner(){
        return this.inner;
    }
    
    public void setInner( Inner inner ){
        this.inner = inner;
    }
    
    public Entity getSub(){
        return this.sub;
    }
    
    public void setSub( Entity sub ){
        this.sub = sub;
    }
    public Gender getGender(){
        return this.gender;
    }
    
    public void setGender( Gender gender ){
        this.gender = gender;
    }
    
    public Kind getKind(){
        return this.kind;
    }
    
    public void setKind( Kind kind ){
        this.kind = kind;
    }
    
    public boolean getActive(){
        return this.active;
    }
    
    public void setActive( boolean active ){
        this.active = active;
    }
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
       
    public int getCode(){
        return this.code;
    }
    
    public void setCode( int code ){
        this.code = code;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    public Date getBirth(){
        return this.birth;
    }
    
    public void setBirth( Date birth ){
        this.birth = birth;
    }
        
    public Person getPerson(){
        return this.person;
    }
    
    public void setPerson( Person person ){
        this.person = person;
    }

    public Boolean getVisible(){
        return this.visible;
    }
    
    public void setVisible( Boolean visible ){
        this.visible = visible;
    }
    

}
