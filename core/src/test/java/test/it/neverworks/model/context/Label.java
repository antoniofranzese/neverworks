package test.it.neverworks.model.context;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;

@Convert( Label.DefaultConverter.class )
public class Label extends BaseModel {
    
    @Property
    private String value;
    
    public String getValue(){
        return this.value;
    }
    
    public void setValue( String value ){
        this.value = value;
    }
    
    public static class DefaultConverter implements Converter {
        
        public Object convert( Object value ) {
            if( value instanceof String ) {
                return new Label().set( "value", value );
            } else {
                return value;
            }
        }
    }
    
    public String toString() {
        return "Label(" + value + ")";
    }

}