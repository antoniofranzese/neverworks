package test.it.neverworks.model.instance;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


import it.neverworks.model.description.ModelInstance;

@RunWith( JUnit4.class )
public class InstanceTest {

    @Test
    public void modelInstancePropertyReadTest() {
        Entity e = new Entity();
        e.setName( "MyName" );
        e.setAge( 15 );
        
        assertEquals( e.model().<String>get( "name" ), "MyName" );
        assertEquals( e.model().get( "name" ), "MyName" );
        
        assertThat( e.model().<Integer>get( "age" ), is( 15 ) );

    }

    @Test
    public void modelInstancePropertyWriteTest() {
        Entity e = new Entity();
        
        e.setName( "Start" );
        e.setAge( 0 );
        assertEquals( e.getName(), "Start" );
        assertEquals( e.getAge(), 0 );

        e.model().set( "name", "MyName" );
        
        assertEquals( e.model().get( "name" ), "MyName" );
        assertEquals( e.getName(), "MyName" );

        e.model().set( "age", 45 );

        assertThat( e.model().<Integer>get( "age" ), is( 45 ) );
        assertEquals( e.getAge(), 45 );
        
    }
    
    @Test
    public void modelInstanceCopyTest() {
        Entity e = new Entity();
        
        e.setName( "MyName" );
        e.setAge( 15 );

        Entity e2 = new Entity();
        e.model().copy().to( e2 );
        
        assertEquals( e2.getName(), "MyName" );
        assertEquals( e2.getAge(), 15 );
    }

    @Test
    public void persistentModelInstanceTest() {
        Entity e = new Entity();

        ModelInstance instance2 = ModelInstance.of( e );
        ModelInstance instance3 = e.model();
        
        assertTrue( instance2 == instance3 );
    }
}