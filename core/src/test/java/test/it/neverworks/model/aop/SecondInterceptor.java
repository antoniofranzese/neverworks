package test.it.neverworks.model.aop;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

class SecondInterceptor implements Interceptor, MethodProcessor {
    
    private String value;
    
    public SecondInterceptor( AnnotationInfo<Second> info ) {
        this.value = info.annotation().value();
    }
    
    public void processMethod( MethodDescriptor method ) {
        //System.out.println( "Second processing " + method.getActual() );
        method.place( this ).after( FirstInterceptor.class );
    }

    public Object invoke( Invocation invocation ) {
        //System.out.println( "Second invocation with " + invocation.arguments().get( 0 ) );
        invocation.setArgument( 0, "2=" + this.value + ":" + invocation.arguments().get( 0 ) );
        return invocation.invoke();
    }
    
}