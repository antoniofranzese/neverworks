package test.it.neverworks.model.io;

import java.util.Map;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.text.SimpleDateFormat;

import static it.neverworks.language.*;
import static it.neverworks.model.expressions.ExpressionEvaluator.evaluate;
import it.neverworks.lang.Objects;
import it.neverworks.encoding.JSON;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.io.*;

@RunWith( JUnit4.class )
public class IoTest {

    @Test
    public void exportModelStateTest() {
        Date birth = new Date();
        Entity e = new Entity()
            .set( "name", "MyName" )
            .set( "age", 15 )
            .set( "birth", birth )
            .set( "child", new ModelChild().set( "name", "MyChild" ).set( "code", 20 ) )
            .set( "simpleChild1", new SimpleChild( "MySimpleChild1", 30 ) )
            .set( "simpleChild2", new SimpleChild( "MySimpleChild2", 40 ) )
            .set( "untypedChildren", list(
                new ModelChild().set( "name", "Child1" )
                ,new ModelChild().set( "name", "Child2" )
                ,new ModelChild().set( "name", "Child3" )
                ,new SimpleChild( "SimpleChild1", 30 )
                ,new ExportChild( "ExportChild1", 40 )
            ));
        
        State state = StateIO.extract( e );
        assertThat( state, instanceOf( Map.class ) );
        assertThat( (String) state.get( "name" ), is( "MyName" ) );
        assertThat( (Integer) state.get( "age" ), is( 15 ) );
        assertThat( (Date) state.get( "birth" ), is( birth ) );
        
        // Internal
        assertFalse( state.has( "code" ) );
        
        assertThat( state.get( "child" ), instanceOf( State.class ) );
        assertThat( state.get( "simpleChild1" ), instanceOf( State.class ) );
        assertThat( state.get( "simpleChild2" ), instanceOf( SimpleChild.class ) );

        assertThat( (String) state.sub( "child" ).get( "name" ), is( "MyChild" ) );
        assertThat( (Integer) state.sub( "child" ).get( "code" ), is( 20 ) );
        assertThat( (String) state.sub( "simpleChild1" ).get( "name" ), is( "MySimpleChild1" ) );
        assertThat( (Integer) state.sub( "simpleChild1" ).get( "code" ), is( 30 ) );

        assertThat( state.get( "untypedChildren" ), instanceOf( ListState.class ) );

        List children = (List) state.get( "untypedChildren" );
        assertThat( children.get( 0 ), instanceOf( State.class ) );
        assertThat( children.get( 1 ), instanceOf( State.class ) );
        assertThat( children.get( 2 ), instanceOf( State.class ) );
        assertThat( children.get( 3 ), instanceOf( SimpleChild.class ) );
        assertThat( children.get( 4 ), instanceOf( State.class ) );

        // Expressions
        assertThat( (String) evaluate( state, "child.name" ), is( "MyChild" ) );
        assertThat( (String) evaluate( state, "simpleChild1.name" ), is( "MySimpleChild1" ) );
        assertThat( (String) evaluate( state, "simpleChild2.name" ), is( "MySimpleChild2" ) );
        
        // Map map = (Map)state;
        // for( Object key: map.keySet() ) {
        //     Object value = map.get( key );
        //     System.out.println( key + " = " + value + " >> " + ( value != null ? value.getClass().getName() : "null" ) );
        // }

    }

    @Test( expected = IllegalArgumentException.class )
    public void nonExportedSimpleClassTest() {
        Entity e = new Entity()
            .set( "simpleChild2", new SimpleChild( "MySimpleChild2", 40 ) );

        StateIO.extract( e ).sub( "simpleChild2" );
    
    }
    
    @Test
    public void importJsonIntoNewModelTest() {
        Date birth = new Date();
        Entity e = new Entity()
            .set( "name", "MyName" )
            .set( "age", 15 )
            .set( "birth", birth )
            .set( "typedChildren", list(
                new ModelChild().set( "name", "Child1" ).set( "code", 10 )
                ,new ModelChild().set( "name", "Child2" ).set( "code", 20 )
                ,new ModelChild().set( "name", "Child3" ).set( "code", 30 )
            ))
            .set( "untypedChildren", list(
                new ModelChild().set( "name", "UChild1" ).set( "code", 100 )
                ,new ModelChild().set( "name", "UChild2" ).set( "code", 101 )
                ,new SimpleChild( "SimpleChild1", 130 )
                ,new ExportChild( "ExportChild1", 240 )
            ));
        
        String json = JSON.encode( StateIO.extract( e ) ).toString();
        //System.out.println( json );
        
        Object state = JSON.decode( json );
        // System.out.println( state );
        
        Entity m = StateIO.merge( state, Entity.class );
        
        assertThat( m.<String>get( "name" ), is( "MyName" ) );
        assertThat( m.<Integer>get( "age" ), is( 15 ) );
        assertThat( m.<Date>get( "birth" ), is( birth ) );
        assertThat( m.<List>get( "typedChildren" ).size(), is( 3 ) );
        
        // Typed
        assertThat( m.<List>get( "typedChildren" ).size(), is( 3 ) );
        assertThat( m.get( "typedChildren[0]" ), instanceOf( ModelChild.class ) );
        assertThat( m.get( "typedChildren[1]" ), instanceOf( ModelChild.class ) );
        assertThat( m.get( "typedChildren[2]" ), instanceOf( ModelChild.class ) );

        assertThat( m.<String>get( "typedChildren[0].name" ), is( "Child1" ) );
        assertThat( m.<String>get( "typedChildren[1].name" ), is( "Child2" ) );
        assertThat( m.<String>get( "typedChildren[2].name" ), is( "Child3" ) );

        assertThat( m.<Integer>get( "typedChildren[0].code" ), is( 10 ) );
        assertThat( m.<Integer>get( "typedChildren[1].code" ), is( 20 ) );
        assertThat( m.<Integer>get( "typedChildren[2].code" ), is( 30 ) );
        
        // Unyped
        assertThat( m.<List>get( "untypedChildren" ).size(), is( 4 ) );
        assertThat( m.get( "untypedChildren[0]" ), instanceOf( ModelChild.class ) );
        assertThat( m.get( "untypedChildren[1]" ), instanceOf( ModelChild.class ) );
        assertThat( m.get( "untypedChildren[2]" ), instanceOf( SimpleChild.class ) );
        assertThat( m.get( "untypedChildren[3]" ), instanceOf( ExportChild.class ) );

        assertThat( m.<String>get( "untypedChildren[0].name" ), is( "UChild1" ) );
        assertThat( m.<String>get( "untypedChildren[1].name" ), is( "UChild2" ) );
        assertThat( m.<String>get( "untypedChildren[2].name" ), is( "SimpleChild1" ) );
        assertThat( m.<String>get( "untypedChildren[3].name" ), is( "ExportChild1" ) );

        assertThat( m.<Integer>get( "untypedChildren[0].code" ), is( 100 ) );
        assertThat( m.<Integer>get( "untypedChildren[1].code" ), is( 101 ) );
        assertThat( m.<Integer>get( "untypedChildren[2].code" ), is( 130 ) );
        assertThat( m.<Integer>get( "untypedChildren[3].code" ), is( 240 ) );

    }
    
    @Test
    public void importJsonIntoExistingModelTest() {
        Date birth = new Date();
        Entity e = new Entity()
            .set( "name", "MyName" )
            .set( "age", 15 )
            .set( "birth", birth )
            .set( "address", "MyAddress")
            .set( "typedChildren", list(
                new ModelChild().set( "name", "Child1" ).set( "code", 10 )
                ,new ModelChild().set( "name", "Child2" ).set( "code", 20 )
                ,new ModelChild().set( "name", "Child3" ).set( "code", 30 )
            ))
            .set( "untypedChildren", list(
                new ModelChild().set( "name", "UChild1" ).set( "code", 100 )
                ,new ModelChild().set( "name", "UChild2" ).set( "code", 101 )
                ,new SimpleChild( "SimpleChild1", 130 )
                ,new ExportChild( "ExportChild1", 240 )
            ));

        String json = "{\"birth\":null,\"typedChildren\":[{\"name\":\"NewChild1\",\"code\":110},{\"name\":\"NewChild2\",\"code\":120}],\"age\":25,\"name\":\"NewMyName\",\"untypedChildren\":[{\"name\":\"NewSimpleChild\",\"code\":220,\".class\":\"test.it.neverworks.model.io.SimpleChild\"}, {\"name\":\"NewEntity\",\"age\":10,\"birth\":\"2015-02-13\",\".class\":\"test.it.neverworks.model.io.Entity\"}] }";
        
        StateIO.merge( JSON.decode( json ), e );
        
        assertThat( e.<String>get( "name" ), is( "NewMyName" ) );
        assertThat( e.<String>get( "address" ), is( "MyAddress" ) );
        assertThat( e.<Integer>get( "age" ), is( 25 ) );
        assertThat( e.<Date>get( "birth" ), nullValue() );

        // Typed
        assertThat( e.<List>get( "typedChildren" ).size(), is( 2 ) );
        assertThat( e.get( "typedChildren[0]" ), instanceOf( ModelChild.class ) );
        assertThat( e.get( "typedChildren[1]" ), instanceOf( ModelChild.class ) );

        assertThat( e.<String>get( "typedChildren[0].name" ), is( "NewChild1" ) );
        assertThat( e.<String>get( "typedChildren[1].name" ), is( "NewChild2" ) );

        assertThat( e.<Integer>get( "typedChildren[0].code" ), is( 110 ) );
        assertThat( e.<Integer>get( "typedChildren[1].code" ), is( 120 ) );
        
        // Unyped
        assertThat( e.<List>get( "untypedChildren" ).size(), is( 2 ) );
        //System.out.println( e.getUntypedChildren() );
        assertThat( e.get( "untypedChildren[0]" ), instanceOf( SimpleChild.class ) );
        assertThat( e.get( "untypedChildren[1]" ), instanceOf( Entity.class ) );

        assertThat( e.<String>get( "untypedChildren[0].name" ), is( "NewSimpleChild" ) );
        assertThat( e.<String>get( "untypedChildren[1].name" ), is( "NewEntity" ) );

        assertThat( e.<Integer>get( "untypedChildren[0].code" ), is( 220 ) );
        assertThat( e.<Integer>get( "untypedChildren[1].age" ), is( 10 ) );

        Date d = e.get( "untypedChildren[1].birth" );
        assertThat( d.getDate(), is( 13 ) );
        assertThat( d.getMonth(), is( 1 ) );
        assertThat( d.getYear(), is( 115 ) );
        
    }

    // @Test
    // public void exportModelWithConvertersTest() {
    //     Date birth = new Date();
    //     Entity e = new Entity()
    //         .set( "name", "MyName" )
    //         .set( "age", 15 )
    //         .set( "birth", birth )
    //         .set( "child", new ModelChild().set( "name", "MyChild" ).set( "code", 20 ) )
    //         .set( "city", new City().set( "name", "Benevento" ).set( "province", "BN" ) )
    //         ;
    //
    //     StateContextFactory factory1 = new StateContextFactory();
    //     State state1 = StateIO.extract( e, factory1.buildContext() );
    //
    //     assertThat( state1.get( "birth"), instanceOf( Date.class ) );
    //     assertThat( state1.get( "child"), instanceOf( State.class ) );
    //     assertThat( state1.get( "city"), instanceOf( State.class ) );
    //
    //     StateContextFactory factory2 = new StateContextFactory(
    //         arg( "converters",
    //             map( (Class) City.class, (Object) new Converter(){
    //                 public Object convert( Object value ) {
    //                     City city = (City) value;
    //                     return city.getName() + " (" + city.getProvince() + ")";
    //                 }
    //             })
    //             .map( Date.class, new Converter(){
    //                 public Object convert( Object value ) {
    //                     return new SimpleDateFormat( "dd/MM/yyyy" ).format( (Date) value );
    //                 }
    //             })
    //         )
    //     );
    //     State state2 = StateIO.extract( e, factory2.buildContext() );
    //
    //     assertThat( state2.get( "birth"), instanceOf( String.class ) );
    //     assertThat( state2.get( "child"), instanceOf( State.class ) );
    //     assertThat( (String) state2.get( "city"), is( "Benevento (BN)" ) );
    //
    // }

}