package test.it.neverworks.model.proxymodel;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Classic;

public class Entity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Classic
    private String address;
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    //Sealed
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    @Virtual
    public String getCity(){
        return "within:" + address;
    }
    
}

