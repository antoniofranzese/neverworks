package test.it.neverworks.model.converter;

import it.neverworks.model.converters.Converter;

public class PersonConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof String ) {
            return new Person().set( "name", value );
        } else {
            return value;
        }
    }
    
}