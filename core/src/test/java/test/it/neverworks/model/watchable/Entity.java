package test.it.neverworks.model.watchable;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Watch;

@Watchable
public class Entity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }

    @Property @Ignore
    private String address;
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    @Property @Watch
    private Person father;
    
    public Person getFather(){
        return this.father;
    }
    
    public void setFather( Person father ){
        this.father = father;
    }
    
    @Property @Watch
    private Person mother;
    
    public Person getMother(){
        return this.mother;
    }
    
    public void setMother( Person mother ){
        this.mother = mother;
    }
    
    private String watchableSealed;
    
    public String getWatchableSealed(){
        return this.watchableSealed;
    }
    
    public void setWatchableSealed( String watchableSealed ){
        this.watchableSealed = watchableSealed;
    }
    
    private String ignoredSealed;
    
    @Ignore
    public String getIgnoredSealed(){
        return this.ignoredSealed;
    }
    
    public void setIgnoredSealed( String ignoredSealed ){
        this.ignoredSealed = ignoredSealed;
    }
}