package test.it.neverworks.model.converter;

import java.util.Date;

import it.neverworks.lang.Booleans;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.description.Classic;

import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.LocalConvert;
import it.neverworks.model.utils.ToStringBuilder;

public class Entity extends BaseModel {
    
    @Property         
    private String name;
     
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property @AutoConvert
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    public String toString() {
        return new ToStringBuilder( this ).add( "name" ).toString();
    }
    
    private Integer sealed;
    
    @AutoConvert
    public Integer getSealed(){
        return this.sealed;
    }
    
    public void setSealed( Integer sealed ){
        this.sealed = sealed;
    }
    
    private Integer sealed2;
    
    public Integer getSealed2(){
        return this.sealed2;
    }
    
    @AutoConvert
    public void setSealed2( Integer sealed2 ){
        this.sealed2 = sealed2;
    }
    
    private Boolean sealed3;
    
    public Boolean getSealed3(){
        return this.sealed3;
    }
    
    @LocalConvert
    public void setSealed3( Boolean sealed3 ){
        this.sealed3 = sealed3;
    }
    
    protected Object convertSealed3( Object value ) {
        if( value instanceof String && "sure!".equalsIgnoreCase( (String) value ) ) {
            return true;
        } else if( value instanceof String && "nope!".equalsIgnoreCase( (String) value ) ) {
            return false;
        } else if( value != null ) {
            return Booleans.bool( value );
        } else {
            return value;
        }
    }
    
    @Classic
    private Long classic;
    
    @AutoConvert
    public Long getClassic(){
        return this.classic;
    }
    
    public void setClassic( Long classic ){
        this.classic = classic;
    }
    
    @Classic @AutoConvert
    private Long classic2;
    
    public Long getClassic2(){
        return this.classic2;
    }
    
    public void setClassic2( Long classic2 ){
        this.classic2 = classic2;
    }
}
