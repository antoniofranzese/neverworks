package test.it.neverworks.model.collections;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;

class ChildEntity extends BaseModel implements ChildModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    private Entity parent;
    
    public Entity getParent(){
        return this.parent;
    }
    
    public void setParent( Entity parent ){
        this.parent = parent;
    }
    
    public void adoptModel( ModelInstance parent, PropertyDescriptor property ) {
        this.parent = parent.as( Entity.class );
    }
    
    public void orphanModel( ModelInstance parent, PropertyDescriptor property ) {
        this.parent = null;
    }
    
    public String toString() {
        return "ChildEntity:" + name;
    }
    
}

