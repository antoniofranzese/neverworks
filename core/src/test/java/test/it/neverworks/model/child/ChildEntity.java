package test.it.neverworks.model.child;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;

class ChildEntity extends BaseModel implements ChildModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    private Entity parent;
    
    public Entity getParent(){
        return this.parent;
    }
    
    public void setParent( Entity parent ){
        this.parent = parent;
    }
    
    private String lastProperty;
    
    public String getLastProperty(){
        return this.lastProperty;
    }
    
    public void adoptModel( ModelInstance parent, PropertyDescriptor property ) {
        if( this.parent == null ) {
            this.parent = parent.as( Entity.class );
            this.lastProperty = property.getName();
        } else {
            throw new IllegalStateException( "Duplicate adoption" );
        }
    }
    
    public void orphanModel( ModelInstance parent, PropertyDescriptor property ) {
        this.parent = null;
        this.lastProperty = null;
    }
}

