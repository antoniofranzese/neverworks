package test.it.neverworks.model.utils;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Calculator;
import it.neverworks.lang.Reflection;
import it.neverworks.model.features.Invoke;
import it.neverworks.model.description.InstanceMethod;
import it.neverworks.model.utils.ExpandoModel;

@RunWith( JUnit4.class )
public class InvokeTest {

    @Test
    public void instanceMethodTest() {
        Calculator calc = new Calculator();
        calc.push( 10 );
        new InstanceMethod( calc,  Reflection.findMethods( Calculator.class, "add" ) ).invokeInstance( 5 );
        assertThat( calc.toInt(), is( 15 ) );

        new InstanceMethod( calc,  Reflection.findMethods( Calculator.class, "push" ) ).invokeInstance( 3 );
        new InstanceMethod( calc,  Reflection.findMethods( Calculator.class, "sub" ) ).invokeInstance();
        assertThat( calc.toInt(), is( 12 ) );
    }

    @Test
    public void expressionInvokeTest() {
        ExpandoModel entity = new ExpandoModel( arg( "calc", new Calculator() ) );

        ((Invoke) eval( entity, "calc.push" )).invokeInstance( 10 );
        ((Invoke) eval( entity, "calc.add" )).invokeInstance( 5 );
        assertThat( (Integer) ((Invoke) eval( entity, "calc.toInt" )).invokeInstance(), is( 15 ) );

        ((Invoke) eval( entity, "calc.push" )).invokeInstance( 3 );
        ((Invoke) eval( entity, "calc.sub" )).invokeInstance();
        assertThat( (Integer) ((Invoke) eval( entity, "calc.toInt" )).invokeInstance(), is( 12 ) );

    }

    @Test
    public void baseModelInvokeTest() {
        Entity entity = new Entity()
            .set( "name", "Entity 1" )
            .set( "parent", new Entity().set( "name", "Parent 1" ));

        assertThat( (String) entity.call( "getName" ), is( "Entity 1") );
        assertThat( (String) entity.call( "parent.getName" ), is( "Parent 1") );

    }

    
}