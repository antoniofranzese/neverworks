package test.it.neverworks.model.bytecode;

import it.neverworks.model.Modelize;
import it.neverworks.model.Property;
import it.neverworks.model.description.Setter;
import it.neverworks.model.utils.Identification;

@Modelize
@Identification({ "!name" })
class Entity {

    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
       
       
}