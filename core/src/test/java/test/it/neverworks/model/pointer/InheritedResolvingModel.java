package test.it.neverworks.model.pointer;

import it.neverworks.model.Property;
import it.neverworks.model.features.Resolve;

public class InheritedResolvingModel extends ResolvingModel  {
    
    @Property @Resolve
    private Resource resource3;
    
    public Resource getResource3(){
        return this.resource3;
    }
    
    public void setResource3( Resource resource3 ){
        this.resource3 = resource3;
    }
    
}