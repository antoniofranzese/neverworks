package test.it.neverworks.model.process;

import it.neverworks.lang.Reflection;
import it.neverworks.aop.MethodCall;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;

public class TestProcessor2 implements PropertyProcessor {
    
    public void processProperty( MetaPropertyManager property ) {
        property.placeGetter( new MethodCall( this, Reflection.findMethod( this, "getter", Getter.class ) ) ).after();
        property.placeSetter( new MethodCall( this, Reflection.findMethod( this, "setter", Setter.class ) ) ).after();
    }
    
    public void setter( Setter<String> setter ) {
        setter.set( "processed2:" + setter.value() );
    }

    public String getter( Getter<String> getter ) {
        return "retrieved2:" + getter.value();
    }

}