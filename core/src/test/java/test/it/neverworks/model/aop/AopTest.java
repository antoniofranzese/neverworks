package test.it.neverworks.model.aop;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import it.neverworks.lang.Method;
import it.neverworks.lang.Reflection;
import it.neverworks.aop.MethodCall;
import it.neverworks.aop.Pointcut;
import it.neverworks.aop.MethodPointcut;
import it.neverworks.aop.Invocation;
import it.neverworks.aop.AopException;
import it.neverworks.aop.MissingInvocationException;
import it.neverworks.model.description.MethodDescriptor;


@RunWith( JUnit4.class )
public class AopTest {

    @Test
    public void invokeInterceptorChainTest() {
        Entity e = new Entity().set( "name", "Entity1" );
        
        String result = (String) new Invocation( e )
            .add( new BeforeInterceptor() )
            .add( new AfterInterceptor() )
            .add( new MethodCall( Reflection.getMethod( Entity.class, "makeName", String.class ) ) )
            .launch( "parameter" );
        
        assertThat( result, is( "before:parameter:after>Entity1" ) );
        Entity e2 = new Entity().set( "name", "Entity2" );
        
        String result2 = (String) new Invocation( e2 )
            .add( new AfterInterceptor() )
            .add( new BeforeInterceptor() )
            .add( new MethodCall( Reflection.getMethod( Entity.class, "makeName", String.class ) ) )
            .launch( "parameter2" );
        
        assertThat( result2, is( "before:parameter2:after>Entity2" ) );
        Entity e3 = new Entity();
        new Invocation( e3 )
            .add( new BeforeInterceptor() )
            .add( new AfterInterceptor() )
            .add( new MethodCall( Reflection.getMethod( Entity.class, "setName", String.class ) ) )
            .launch( "Entity3" );
        
        assertThat( e3.<String>get( "name" ), is( "before:Entity3:after" ) );
        
    }
    
    @Test
    public void invokeMethodPointcutTest() {
        Pointcut pcut = new MethodPointcut( Reflection.getMethod( Entity.class, "makeName", String.class ) )
            .add( new BeforeInterceptor() )
            .add( new AfterInterceptor() );

        Entity e1 = new Entity().set( "name", "Entity1" );
        String result1 = ((String) pcut.invoke( e1, "Parameter1" ));
        assertThat( result1, is( "before:Parameter1:after>Entity1" ) );
        Entity e2 = new Entity().set( "name", "Entity2" );
        String result2 = ((String) pcut.invoke( e2, "Parameter2" ));
        assertThat( result2, is( "before:Parameter2:after>Entity2" ) );
        
    }
    
    @Test( expected = MissingInvocationException.class )
    public void interceptorMissesInvokeTest() {
        Entity e = new Entity().set( "name", "Entity1" );
        
        String result = (String) new Invocation( e )
            .add( new MissingInvokeInterceptor() )
            .add( new MethodCall( Reflection.getMethod( Entity.class, "makeName", String.class ) ) )
            .launch( "parameter" );
        
    }

    @Test
    public void methodRuntimeDescriptionTest() {
        MethodDescriptor plain = MethodDescriptor.of( Entity.class, "makeName", String.class );
        
        assertFalse( plain.intercepted() );
        assertThat( plain.actual(), is( Reflection.findMethod( Entity.class, "makeName", String.class ) ) );
        
        assertThat( plain.toString(), is( "[ModelDescriptor makeName]" ) );

        MethodDescriptor intercepted = MethodDescriptor.of( Entity.class, "interceptedMakeName", String.class );
        assertTrue( intercepted.intercepted() );
        assertThat( intercepted.actual(), not( is( Reflection.findMethod( Entity.class, "interceptedMakeName", String.class ) ) ) );
        assertThat( intercepted.actual().getName(), is( "test$it$neverworks$model$aop$Entity$raw$interceptedMakeName" ) );
        assertTrue( intercepted.contains( LoggableInterceptor.class ) );
        
        assertThat( intercepted.toString(), is( "[ModelDescriptor interceptedMakeName/test$it$neverworks$model$aop$Entity$raw$interceptedMakeName chain=LoggableInterceptor,FirstInterceptor,SecondInterceptor,AnotherInterceptor]" ) );
    }

    @Test
    public void methodIntrumentedInterceptionTest() {
        List<String> logs = LoggableInterceptor.logs;
        logs.clear();
        
        //Model
        Entity e = new Entity().set( "name", "Entity1" );

        assertThat( e.interceptedGetName(), is( "1=ein>>$Entity1" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Entity.test$it$neverworks$model$aop$Entity$raw$interceptedGetName" ) );
        assertThat( logs.size(), is( 1 ) );

        assertThat( e.interceptedMakeName( "par1" ), is( "A=other:2=two:1=one:par1>Entity1" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Entity.test$it$neverworks$model$aop$Entity$raw$interceptedMakeName" ) );
        assertThat( logs.size(), is( 2 ) );

        assertThat( e.interceptedMakeName2( "par2", "par3" ), is( "2=due:1=uno:par2>Entity1<par3" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Entity.test$it$neverworks$model$aop$Entity$raw$interceptedMakeName2" ) );
        assertThat( logs.size(), is( 3 ) );
        
        assertThat( e.interceptedMakeName( 10 ), is( "2=zwei:1=ein:10#Entity1" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Entity.test$it$neverworks$model$aop$Entity$raw$interceptedMakeName" ) );
        assertThat( logs.size(), is( 4 ) );

        // Inherited Model
        Child c = new Child().set( "name", "Child1" );

        // Plain method override
        assertThat( c.interceptedMakeName( "par1" ), is( "[A=other:2=two:1=one:par1>Child1]" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Child.test$it$neverworks$model$aop$Entity$raw$interceptedMakeName" ) );
        assertThat( logs.size(), is( 5 ) );

        // Re-intercepted method override
        assertThat( c.interceptedMakeName3( "par2" ), is( "[A=A:2=B:par2>Child1]" ) );
        assertThat( logs.get( logs.size() - 2 ), is( "Logging invocation to Child.test$it$neverworks$model$aop$Child$raw$interceptedMakeName3" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Child.test$it$neverworks$model$aop$Entity$raw$interceptedMakeName3" ) );
        assertThat( logs.size(), is( 7 ) );

        // POJO
        Bean b = new Bean();
        b.setName( "Bean1" );

        assertThat( b.interceptedMakeName( "_par1" ), is( "A=_other:2=_two:1=_one:_par1>Bean1" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Bean.test$it$neverworks$model$aop$Bean$raw$interceptedMakeName" ) );
        assertThat( logs.size(), is( 8 ) );

        assertThat( b.interceptedMakeName2( "_par2", "_par3" ), is( "2=_due:1=_uno:_par2>Bean1<_par3" ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Logging invocation to Bean.test$it$neverworks$model$aop$Bean$raw$interceptedMakeName2" ) );
        assertThat( logs.size(), is( 9 ) );

    }
    
    @Test
    public void exceptionInterceptorsTest() {
        List<String> logs = LoggableInterceptor.logs;
        logs.clear();

        Entity e = new Entity().set( "name", "Entity1" );
        
        boolean catched = false;
        try {
            String x = e.uncheckedMakeName( null );
        } catch( MissingValueException ex ) {
            catched = true;
        }
        
        assertTrue( catched );
        assertThat( logs.size(), is( 2 ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Error invoking Entity.test$it$neverworks$model$aop$Entity$raw$uncheckedMakeName: MissingValueException" ) );
        
        assertThat( e.checkedMakeName( null ), is( "MISSING" ) );
        assertThat( logs.size(), is( 4 ) );
        assertThat( logs.get( logs.size() - 1 ), is( "Error invoking Entity.test$it$neverworks$model$aop$Entity$raw$uncheckedMakeName: MissingValueException" ) );
        
    }
    
}