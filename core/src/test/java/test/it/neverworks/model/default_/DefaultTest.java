package test.it.neverworks.model.default_;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

import it.neverworks.model.description.Default;

@RunWith( JUnit4.class )
public class DefaultTest {

    @Test
    public void propertyDefaultValueWithStandardProviderTest() {
        DefaultEntity e1 = new DefaultEntity();
        assertFalse( e1.model().defined( "list1" ) );
        assertEquals( e1.model().raw().get( "list1" ), null );

        // Edge via ModelInstance, works
        assertThat( e1.model().front().get( "list1" ), instanceOf( List.class ) );

        // Direct via ModelInstance, works
        DefaultEntity e2 = new DefaultEntity();
        assertThat( e2.model().direct().get( "list1" ), instanceOf( List.class ) );
        
        // Edge via standard getter, works with instrumentation
        DefaultEntity e3 = new DefaultEntity();
        assertThat( e3.getList1(), instanceOf( List.class ) ); 
        
        // Edge via ModelInstance, works
        DefaultEntity e4 = new DefaultEntity();
        assertThat( e4.model().front().get( "list2" ), instanceOf( List.class ) );
        
        // Direct via ModelInstance, works
        DefaultEntity e5 = new DefaultEntity();
        assertThat( e5.model().direct().get( "list2" ), instanceOf( List.class ) );
        
        // Edge via new style setter, works
        DefaultEntity e6 = new DefaultEntity();
        assertThat( e6.getList2(), instanceOf( List.class ) ); 
        
    }
}