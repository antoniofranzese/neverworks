package test.it.neverworks.model.virtual;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.Virtual;

public class Entity extends BaseModel {
    
    @Property
    private String name;
    
    //Classic
    private String address;
    
    @Property @Virtual
    private int age;
    
    @Virtual
    public String getCity(){
        return null;
    }
    
    public void setCity( String city ){
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}

