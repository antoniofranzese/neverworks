package test.it.neverworks.model.inheritance;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith( JUnit4.class )
public class InheritanceTest {

    @Test
    public void propertyChildObjectTest() {

        Entity e1 = new Entity();
        
        e1.setName( "E1" );
        assertThat( e1.myName(), is( "name:E1" ) );
        SubEntity s1 = new SubEntity();
        
        s1.setName( "S1" );
        assertThat( s1.myName(), is( "name:S1" ) );
        assertThat( s1.mySubName(), is( "subname:S1" ) );
        
    }
}