package test.it.neverworks.model.converter;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Date;
import java.util.TimeZone;
import java.util.GregorianCalendar;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.joda.time.DateTime;

import static it.neverworks.language.*;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.converters.*;
import it.neverworks.lang.Dates;


@RunWith( JUnit4.class )
@SuppressWarnings( "deprecation" )
public class ConverterTest {

    @Test
    public void converterDescriptionTest() {
        ModelDescriptor model = ModelDescriptor.of( ConverterEntity.class );
        assertThat( model.property( "name" ).getConverter(), nullValue() );
        assertThat( model.property( "age" ).getConverter(), instanceOf( IntConverter.class ) );
        assertThat( model.property( "code" ).getConverter(), instanceOf( IntConverter.class ) );
        assertThat( model.property( "address" ).getConverter(), instanceOf( StringConverter.class ) );
    }
        
    @Test
    public void stringAndIntTypeConversionTest() {
        ConverterEntity e = new ConverterEntity();
    
        e.model().set( "age", 15 );
        assertThat( e.getAge(), is( 15 ) );
        
        e.model().set( "age", "20" );
        assertThat( e.getAge(), is( 20 ) );
        
        e.model().set( "code", "345" );
        assertThat( e.getCode(), is( 345 ) );
        
        e.model().set( "address", 15000 );
        assertThat( e.getAddress(), is( "15000" ) );
    }
    
    @Test
    public void dateTypeConversionTest() {
        ConverterEntity e = new ConverterEntity();
        
        // Italian
        e.model().set( "birth", "25/10/1980" );
        assertThat( e.getBirth(), instanceOf( Date.class ) );
        assertThat( "Day", e.getBirth().getDate(), is( 25 ) );
        assertThat( "Month (-1)", e.getBirth().getMonth(), is( 9 ) ); 
        assertThat( "Year (-1900)", e.getBirth().getYear(), is( 80 ) );
        assertThat( "Hours", e.getBirth().getHours(), is( 0 ) );
        assertThat( "Minutes", e.getBirth().getMinutes(), is( 0 ) );
        assertThat( "Seconds", e.getBirth().getSeconds(), is( 0 ) );
    
        // English
        e.model().set( "birth", "1972-01-16" );
        assertThat( e.getBirth(), instanceOf( Date.class ) );
        assertThat( "Day", e.getBirth().getDate(), is( 16 ) );
        assertThat( "Month (-1)", e.getBirth().getMonth(), is( 0 ) ); 
        assertThat( "Year (-1900)", e.getBirth().getYear(), is( 72 ) );
        assertThat( "Hours", e.getBirth().getHours(), is( 0 ) );
        assertThat( "Minutes", e.getBirth().getMinutes(), is( 0 ) );
        assertThat( "Seconds", e.getBirth().getSeconds(), is( 0 ) );

        // Time zone offset, used to determine the right hour
        int offset = Dates.offsetHours( "2014-06-20" );

        // ISO - UTC
        e.model().set( "birth", "2014-06-20T12:51:01Z" );
        System.out.println( e.getBirth() );
        assertThat( e.getBirth(), instanceOf( Date.class ) );
        assertThat( "Day", e.getBirth().getDate(), is( 20 ) );
        assertThat( "Month (-1)", e.getBirth().getMonth(), is( 5 ) ); 
        assertThat( "Year (-1900)", e.getBirth().getYear(), is( 114 ) );
        assertThat( "Hours", e.getBirth().getHours(), is( 12 /* UTC */ + offset ) );
        assertThat( "Minutes", e.getBirth().getMinutes(), is( 51 ) );
        assertThat( "Seconds", e.getBirth().getSeconds(), is( 1 ) );
        assertThat( "Milliseconds", new DateTime( e.getBirth().getTime() ).millisOfSecond().get(), is( 0 ) );

        // ISO - CEST
        e.model().set( "birth", "2014-06-20T12:51:01+02:00" );
        assertThat( "Hours", e.getBirth().getHours(), is( 10 /* UTC */ + offset ) ); 

        // ISO with millis
        e.model().set( "birth", "2014-06-20T13:52:03.234Z" );
        assertThat( e.getBirth(), instanceOf( Date.class ) );
        assertThat( "Day", e.getBirth().getDate(), is( 20 ) );
        assertThat( "Month (-1)", e.getBirth().getMonth(), is( 5 ) ); 
        assertThat( "Year (-1900)", e.getBirth().getYear(), is( 114 ) );
        assertThat( "Hours", e.getBirth().getHours(), is( 13 + offset ) );
        assertThat( "Minutes", e.getBirth().getMinutes(), is( 52 ) );
        assertThat( "Seconds", e.getBirth().getSeconds(), is( 3 ) );
        assertThat( "Milliseconds", new DateTime( e.getBirth().getTime() ).millisOfSecond().get(), is( 234 ) );
        
    }
    
    @Test
    public void booleanTypeConversionTest() {
        ConverterEntity e = new ConverterEntity();
        e.model().set( "active", "true" );
        assertTrue( e.getActive() );
        
        e.model().set( "active", "no" );
        assertFalse( e.getActive() );
    }
    
    @Test( expected = IllegalArgumentException.class ) 
    public void wrongDateValueTest() {
        ConverterEntity e = new ConverterEntity();
        e.model().set( "birth", "25:10:1980" );
    }
    
    @Test( expected = IllegalArgumentException.class )
    public void wrongTypeNoConversionTest() {
        ConverterEntity e = new ConverterEntity();
        e.model().set( "name", 40 );
    }
    
    @Test
    public void classInheritedConverterTest() {
        ConverterEntity e = new ConverterEntity();
    
        e.model().set( "person", "MyPerson" );
        
        assertThat( e.getPerson(), instanceOf( Person.class ) );
        assertEquals( e.getPerson().getName(), "MyPerson" );
        
        Person p = new Person();
        p.setName( "SecondPerson" );
        
        e.model().set( "person", p );
    
        assertThat( e.getPerson(), instanceOf( Person.class ) );
        assertEquals( e.getPerson().getName(), "SecondPerson" );
    }

    @Test
    public void enumTypeConverterTest() {
        ConverterEntity e = new ConverterEntity();
        
        // Public Enum
        e.set( "kind", "COMPLEX" );
        assertThat( e.getKind(), is( Kind.COMPLEX ) );

        e.set( "kind", "medium" );
        assertThat( e.getKind(), is( Kind.MEDIUM ) );

        e.set( "kind", Kind.SIMPLE );
        assertThat( e.getKind(), is( Kind.SIMPLE ) );
        
        // Inner Enum
        e.set( "gender", "male" );
        assertThat( e.getGender(), is( ConverterEntity.Gender.MALE ) );

        e.set( "gender", "FEMALE" );
        assertThat( e.getGender(), is( ConverterEntity.Gender.FEMALE ) );
        
        // Auto Enum
        e.set( "autoKind", "COMPLEX" );
        assertThat( e.getAutoKind(), is( AutoKind.COMPLEX ) );

        e.set( "autoKind", "medium" );
        assertThat( e.getAutoKind(), is( AutoKind.MEDIUM ) );

        e.set( "autoKind", AutoKind.SIMPLE );
        assertThat( e.getAutoKind(), is( AutoKind.SIMPLE ) );
    }
    
    @Test
    public void mapToModelConverterTest() {
        ConverterEntity e = new ConverterEntity();
        
        e.set( "sub", arg( "name", "Sub1" ).arg( "age", "30" ) );
        assertThat( e.getSub(), instanceOf( Entity.class ) );
        assertThat( e.getSub().getName(), is( "Sub1" ) );
        assertThat( e.getSub().getAge(), is( 30 ) );
    }
    
    @Test
    public void innerPropertyConverterTest() {
        ConverterEntity e = new ConverterEntity();
        
        e.set( "!inner.date", "25/10/1980" );
        assertThat( e.getInner(), instanceOf( ConverterEntity.Inner.class ) );
        assertThat( e.getInner().getDate(), instanceOf( Date.class ) );
        assertThat( "Day", e.getInner().getDate().getDate(), is( 25 ) );
        assertThat( "Month (-1)", e.getInner().getDate().getMonth(), is( 9 ) );
        assertThat( "Year (-1900)", e.getInner().getDate().getYear(), is( 80 ) );
        assertThat( "Hours", e.getInner().getDate().getHours(), is( 0 ) );
        assertThat( "Minutes", e.getInner().getDate().getMinutes(), is( 0 ) );
        assertThat( "Seconds", e.getInner().getDate().getSeconds(), is( 0 ) );
        
        e.set( "inner.integer", "50" );
        assertThat( e.getInner().getInteger(), instanceOf( Integer.class ) );
        assertThat( e.getInner().getInteger(), is( 50 ) );
        
    }
    
    @Test( expected = IllegalArgumentException.class ) 
    public void cannotConvertEnumTest() {
        ConverterEntity e = new ConverterEntity();
        e.set( "gender", "x" );
    }
    
    @Test
    public void classicPropertyConverterTest() {
        Entity e = new Entity();
        
        e.set( "sealed", "10" );
        assertThat( e.get( "sealed" ), instanceOf( Integer.class ) );
        assertThat( e.<Integer>get( "sealed" ), is( 10 ) );

        e.set( "sealed2", "15" );
        assertThat( e.get( "sealed2" ), instanceOf( Integer.class ) );
        assertThat( e.<Integer>get( "sealed2" ), is( 15 ) );

        e.set( "classic", "20" );
        assertThat( e.get( "classic" ), instanceOf( Long.class ) );
        assertThat( e.<Long>get( "classic" ), is( 20L ) );

        e.set( "classic2", "30" );
        assertThat( e.get( "classic2" ), instanceOf( Long.class ) );
        assertThat( e.<Long>get( "classic2" ), is( 30L ) );
    
        ConverterEntity c = new ConverterEntity();
        
        c.set( "classicPerson", "Person1" );
        assertThat( c.getClassicPerson().getName(), is( "Person1" ) );

        c.set( "classicAutoPerson", "Person2" );
        assertThat( c.getClassicAutoPerson().getName(), is( "Person2" ) );
        
        c.set( "classicKind", "medium" );
        assertThat( c.getClassicKind(), is( Kind.MEDIUM ) );

        c.set( "classicKind", Kind.SIMPLE );
        assertThat( c.getClassicKind(), is( Kind.SIMPLE ) );

        c.set( "classicAutoKind", "medium" );
        assertThat( c.getClassicAutoKind(), is( Kind.MEDIUM ) );

        c.set( "classicAutoKind", Kind.SIMPLE );
        assertThat( c.getClassicAutoKind(), is( Kind.SIMPLE ) );
        
        
        
    }
    
    @Test
    public void localConverterTest() {
        ConverterEntity c = new ConverterEntity();

        // Modern property
        c.set( "visible", "Sure!" );
        assertThat( c.getVisible(), is( true ) );

        c.set( "visible", "Nope!" );
        assertThat( c.getVisible(), is( false ) );

        c.set( "visible", "yes" );
        assertThat( c.getVisible(), is( true ) );

        c.set( "visible", "no" );
        assertThat( c.getVisible(), is( false ) );

        c.set( "visible", null );
        assertThat( c.getVisible(), nullValue() );

        // Classic property
        c.set( "lockable", "Sure!" );
        assertThat( c.getLockable(), is( true ) );

        c.set( "lockable", "Nope!" );
        assertThat( c.getLockable(), is( false ) );

        c.set( "lockable", "yes" );
        assertThat( c.getLockable(), is( true ) );

        c.set( "lockable", "no" );
        assertThat( c.getLockable(), is( false ) );

        c.set( "lockable", null );
        assertThat( c.getLockable(), nullValue() );

        Entity e = new Entity();

        // Sealed property
        e.set( "sealed3", "Sure!" );
        assertThat( e.getSealed3(), is( true ) );

        e.set( "sealed3", "Nope!" );
        assertThat( e.getSealed3(), is( false ) );

        e.set( "sealed3", "yes" );
        assertThat( e.getSealed3(), is( true ) );

        e.set( "sealed3", "no" );
        assertThat( e.getSealed3(), is( false ) );

        e.set( "sealed3", null );
        assertThat( e.getSealed3(), nullValue() );


        //Inheritance
        InheritedEntity i = new InheritedEntity();

        c.set( "index", 10 );
        assertThat( c.getIndex(), is( "<10>" ) );

        i.set( "index", 20 );
        assertThat( i.getIndex(), is( "[20]" ) );

        c.set( "chainable", "Sure!" );
        assertThat( c.getChainable(), is( true ) );

        c.set( "chainable", "no" );
        assertThat( c.getChainable(), is( false ) );

        c.set( "chainable", null );
        assertThat( c.getChainable(), nullValue() );

        c.set( "primitiveChainable", "Sure!" );
        assertThat( c.getPrimitiveChainable(), is( true ) );

        c.set( "primitiveChainable", "no" );
        assertThat( c.getPrimitiveChainable(), is( false ) );

        c.set( "primitiveChainable", null );
        assertThat( c.getPrimitiveChainable(), is( false ) );
        
    }
}