package test.it.neverworks.model.aop;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

class CheckedInterceptor implements Interceptor, MethodProcessor {
    
    private String value;
    
    public CheckedInterceptor( AnnotationInfo<Checked> info ) {
        this.value = info.annotation().value();
    }
    
    public void processMethod( MethodDescriptor method ) {
        method.place( this ).last();
    }

    public Object invoke( Invocation invocation ) {
        try {
            return invocation.invoke();
        } catch( MissingValueException ex ) {
            return value;
        }
    }
    
}