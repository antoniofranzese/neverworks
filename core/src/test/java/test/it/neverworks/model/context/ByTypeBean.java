package test.it.neverworks.model.context;

public class ByTypeBean {
    
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

}