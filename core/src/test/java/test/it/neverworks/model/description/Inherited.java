package test.it.neverworks.model.description;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class Inherited extends Entity {
    
    @Property @AutoConvert
    private String code;
    
    public String getCode(){
        return this.code;
    }
    
    public void setCode( String code ){
        this.code = code;
    }
    
    private String city;
    
    public String getCity(){
        return this.city;
    }
    
    public void setCity( String city ){
        this.city = city;
    }
}