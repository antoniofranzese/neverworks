package test.it.neverworks.model.events;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith( JUnit4.class )
public class WireTest {

    @Test
    public void wireTest() {
        List<String> log = new ArrayList<String>();
        
        Entity e = new Entity().set( "name", "Entity0" ).set( "log", log );

        e.getStart().fire();
        
        assertThat( log.size(), is( 2 ) );
        assertTrue( log.contains( "START HANDLER Entity0" ) );
        assertTrue( log.contains( "STARTUP HANDLER Entity0" ) );
        log.clear();

        Sub s = new Sub().set( "name", "Sub0" ).set( "log", log );

        s.getEnd().fire();
        assertThat( log.size(), is( 1 ) );
        assertThat( log.get( 0 ), is( "END HANDLER Sub0 Sub(name=Sub0)" ) );
        log.clear();
        
        s.getStart().fire();
        assertThat( log.size(), is( 3 ) );
        assertTrue( log.contains( "START HANDLER Sub0" ) );
        assertTrue( log.contains( "DUAL HANDLER Sub0" ) );
        assertTrue( log.contains( "STARTUP HANDLER Sub0" ) );
        log.clear();

        s.getProcess().fire();
        assertThat( log.size(), is( 1 ) );
        assertThat( log.get( 0 ), is( "DUAL HANDLER Sub0" ) );
        log.clear();

        Sub s1 = new Sub().set( "name", "Sub1" ).set( "log", log );

        s1.getEnd().fire();
        assertThat( log.size(), is( 1 ) );
        assertThat( log.get( 0 ), is( "END HANDLER Sub1 Sub(name=Sub1)" ) );
        log.clear();

        s1.getProcess().fire();
        assertThat( log.size(), is( 1 ) );
        assertThat( log.get( 0 ), is( "DUAL HANDLER Sub1" ) );
        log.clear();

        s1.getStart().fire();
        assertThat( log.size(), is( 3 ) );
        assertTrue( log.contains( "START HANDLER Sub1" ) );
        assertTrue( log.contains( "DUAL HANDLER Sub1" ) );
        assertTrue( log.contains( "STARTUP HANDLER Sub1" ) );
        log.clear();
    }
}