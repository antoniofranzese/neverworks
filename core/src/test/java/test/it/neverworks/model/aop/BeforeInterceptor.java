package test.it.neverworks.model.aop;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public class BeforeInterceptor implements Interceptor {
    
    public Object invoke( Invocation invocation ) {
        invocation.setArgument( 0, "before:" + ((String) invocation.arguments().get( 0 ) ) );
        return invocation.invoke();
    }
}