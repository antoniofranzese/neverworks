package test.it.neverworks.model.collections;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Child;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.collections.Adder;
import it.neverworks.model.collections.ListAdder;
import it.neverworks.model.collections.Remover;
import it.neverworks.model.collections.MapRemover;
import it.neverworks.model.collections.MapAdder;
import it.neverworks.model.converters.BooleanConverter;
import it.neverworks.model.converters.AutoConverter;
import it.neverworks.model.converters.AutoConvert;

import static it.neverworks.test.TestUtils.*;

public class Entity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property @Collection
    private List<SubEntity> interceptedList;
    
    public List<SubEntity> getInterceptedList(){
        return this.interceptedList;
    }
    
    public void setInterceptedList( List<SubEntity> interceptedList ){
        this.interceptedList = interceptedList;
    }
    
    public void addInterceptedListItem( ListAdder<SubEntity> adder ) {
        // System.out.println( "Intercepting add " + adder.item() );
        adder.item().setParent( this );
        adder.add();
    }
    
    public void removeInterceptedListItem( Remover<SubEntity> remover ) {
        // System.out.println( "Intercepting remove " + remover.item() );
        remover.item().setParent( null );
        remover.remove();
    }
    
    @Property @Collection
    private List untypedList;
    
    public List getUntypedList(){
        return this.untypedList;
    }
    
    public void setUntypedList( List untypedList ){
        this.untypedList = untypedList;
    }

    @Property @Collection
    private List<SubEntity> plainList;
    
    public List<SubEntity> getPlainList(){
        return this.plainList;
    }
    
    public void setPlainList( List<SubEntity> plainList ){
        this.plainList = plainList;
    }
    
    @Property @Collection
    private List<String> convertedList;
    
    public List<String> getConvertedList(){
        return this.convertedList;
    }
    
    public void setConvertedList( List<String> convertedList ){
        this.convertedList = convertedList;
    }
    
    @Property @Collection
    private List<AutoSubEntity> autoConvertedList;
    
    public List<AutoSubEntity> getAutoConvertedList(){
        return this.autoConvertedList;
    }
    
    public void setAutoConvertedList( List<AutoSubEntity> autoConvertedList ){
        this.autoConvertedList = autoConvertedList;
    }
    
    public String toString() {
        return "Entity:" + name;
    }
    
    @Property @Collection
    private Map<String, SubEntity> interceptedMap;
    
    public Map<String, SubEntity> getInterceptedMap(){
        return this.interceptedMap;
    }
    
    public void setInterceptedMap( Map<String, SubEntity> interceptedMap ){
        this.interceptedMap = interceptedMap;
    }
    
    public void addInterceptedMapItem( Adder<SubEntity> adder ) {
        // print( "Intercepting add", adder.item() );
        adder.item().setParent( this );
        adder.add();
    }

    public void removeInterceptedMapItem( MapRemover<String, SubEntity> remover ) {
        // print( "Intercepting remove", remover.item() );
        remover.item().setParent( null );
        remover.remove();
    }
    
    private Set<String> insertedKeys = new HashSet<String>();
    
    public Set<String> getInsertedKeys(){
        return this.insertedKeys;
    }
    
    public void setInsertedKeys( Set<String> insertedKeys ){
        this.insertedKeys = insertedKeys;
    }
    
    @Property @Collection( child = true )
    private List<ChildEntity> childList;
    
    public List<ChildEntity> getChildList(){
        return this.childList;
    }
    
    public void setChildList( List<ChildEntity> childList ){
        this.childList = childList;
    }
    
    @Property @Collection @Child
    private Map<String, ChildEntity> childMap;
    
    public Map<String, ChildEntity> getChildMap(){
        return this.childMap;
    }
    
    public void setChildMap( Map<String, ChildEntity> childMap ){
        this.childMap = childMap;
    }
    
    @Property @Collection
    private Map<String, AutoSubEntity> autoConvertedMap;
    
    public Map<String, AutoSubEntity> getAutoConvertedMap(){
        return this.autoConvertedMap;
    }
    
    public void setAutoConvertedMap( Map<String, AutoSubEntity> autoConvertedMap ){
        this.autoConvertedMap = autoConvertedMap;
    }
    
    @Property @Collection( item = BooleanConverter.class )
    private Map<String,Object> booleans;
    
    public Map<String,Object> getBooleans(){
        return this.booleans;
    }
    
    public void setBooleans( Map<String,Object> booleans ){
        this.booleans = booleans;
    }

    @Property @Collection( item = AutoConvert.class )
    protected Map<String, Boolean> autoBooleans;
    
    public Map<String, Boolean> getAutoBooleans(){
        return this.autoBooleans;
    }
    
    public void setAutoBooleans( Map<String, Boolean> autoBooleans ){
        this.autoBooleans = autoBooleans;
    }

    @Property @Collection( item = AutoConverter.class )
    protected Map<String, Boolean> autoBooleans2;
    
    public Map<String, Boolean> getAutoBooleans2(){
        return this.autoBooleans2;
    }
    
    public void setAutoBooleans2( Map<String, Boolean> autoBooleans2 ){
        this.autoBooleans2 = autoBooleans2;
    }
}