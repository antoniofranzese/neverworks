package test.it.neverworks.model.inheritance;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Child;

class Entity extends BaseModel {
    
    @Property
    private Object name;
    
    public Object getName(){
        return this.name;
    }
    
    public void setName( Object name ){
        this.name = name;
    }
    
    public String myName() {
        return "name:" + this.name;
    }
    
}

