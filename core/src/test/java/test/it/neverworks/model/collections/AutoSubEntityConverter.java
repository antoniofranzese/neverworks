package test.it.neverworks.model.collections;

import it.neverworks.model.converters.Converter;

public class AutoSubEntityConverter implements Converter {
    public Object convert( Object value ) {
        if( value instanceof String ) {
            return new AutoSubEntity().set( "name", value );
        } else {
            return value;
        }
    }
}