package test.it.neverworks.model.defended;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Defended;

public class BaseEntity extends BaseModel {

    @Property @Defended
    private Object name;
    
    public Object getName(){
        return this.name;
    }
    
    public void setName( Object name ){
        this.name = name;
    }
    
    @Property @Defended
    private Object simple;
}