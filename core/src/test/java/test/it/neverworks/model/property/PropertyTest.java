package test.it.neverworks.model.property;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import java.util.Date;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.UnreadablePropertyException;
import it.neverworks.model.description.UnwritablePropertyException;

@RunWith( JUnit4.class )
public class PropertyTest {

    @Test
    public void propertySkipAccessorsUsingRawTest() {
        PropertyAccessEntity e = new PropertyAccessEntity();

        // Use setter
        e.model().set( "prop1", "value1" );
        assertThat( e.model().front().<String>get( "prop1" ), is( "set:value1" ) );
        
        // Skip setter
        e.model().raw().set( "prop1", "value2" );
        assertThat( e.model().front().<String>get( "prop1" ), is( "value2" ) );
        
        // Use setter
        e.model().set( "prop2", 10 );
        assertThat( e.model().front().<Integer>get( "prop2" ), is( 110 ) );
        
        // Skip setter
        e.model().raw().set( "prop2", 20 );
        assertThat( e.model().front().<Integer>get( "prop2" ), is( 20 ) );
        
        e.model().set( "prop3", "value3" ).set( "prop4", 40 );
        
        // Use getter
        assertThat( e.model().front().<String>get( "prop3" ), is( "get:value3" ) );
        assertThat( e.model().front().<Integer>get( "prop4" ), is( 240 ) );
        
        // Skip getter
        assertThat( e.model().raw().<String>get( "prop3" ), is( "value3" ) );
        assertThat( e.model().raw().<Integer>get( "prop4" ), is( 40 ) );
        
        // Renamed setter
        e.setProp5( "value5" );
        assertThat( e.getProp5(), is( "set:value5" ) );

        // Multiple setter
        e.setProp6( "value6" );
        assertThat( e.getProp6(), is( "set2:set:value6" ) );
        e.setProp7( "value7" );
        assertThat( e.getProp7(), is( "set:set2:value7" ) );

    }
    
    @Test
    public void propertyAccessorByteCodeInstrumentationTest() {
        PropertyAccessEntity e = new PropertyAccessEntity();
        
        e.setProp1( "value1" );
        assertThat( e.getProp1(), is( "set:value1" ) );
        
        e.setProp2( 10 );
        assertThat( e.getProp2(), is( 110 ) );
        
        e.setProp3( "value3" );
        assertThat( e.getProp3(), is( "get:value3" ) );
        
        e.setProp4( 40 );
        assertThat( e.getProp4(), is( 240 ) );
    }
    
    @Test
    public void propertyWithoutGetterAndSetterTest() {
        PropertyAccessEntity e = new PropertyAccessEntity();
    
        e.model().checked().set( "privateProp", "myvalue" );
        assertThat( e.model().checked().<String>get( "privateProp" ), is( "myvalue" ) );
        
    }
    
    @Test
    public void beanPropertyAccessTest() {
        PropertyAccessEntity e = new PropertyAccessEntity();
    
        // Front access
        e.model().set( "sealedProperty", "aValue" );
        assertThat( e.model().<String>get( "sealedProperty" ), is( "sealed:aValue" ) );

        // Raw access, useless
        e.model().raw().set( "sealedProperty", "anotherValue" );
        assertThat( e.model().<String>get( "sealedProperty" ), is( "sealed:anotherValue" ) );

        // Front access
        e.model().set( "classicProperty", "aValue" );
        assertThat( e.model().<String>get( "classicProperty" ), is( "classic:aValue" ) );

        // Raw access, useful
        e.model().raw().set( "classicProperty", "anotherValue" );
        assertThat( e.model().<String>get( "classicProperty" ), is( "anotherValue" ) );
        
        // No error expected
        e.model().set( "unreadableProperty", "aValue" );
        e.model().get( "unwritableProperty" );
        
    }
    
    @Test( expected = UnreadablePropertyException.class )
    public void sealedPropertyWithoutGetterIsUnreadableTest() {
        PropertyAccessEntity e = new PropertyAccessEntity();
     
        e.model().get( "unreadableProperty" );
    }
    
    @Test( expected = UnwritablePropertyException.class )
    public void sealedPropertyWithoutSetterIsUnwritableTest() {
        PropertyAccessEntity e = new PropertyAccessEntity();
     
        e.model().set( "unwritableProperty", "value" );
    }
        
}