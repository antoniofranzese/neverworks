package test.it.neverworks.model.property;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Classic;
import it.neverworks.model.description.Preserved;

public class PropertyAccessEntity extends BaseModel {

    @Property
    private String prop1;
    
    @Property
    private int prop2;
    
    @Property
    private String privateProp;
    
    @Property
    private String prop3;   
    
    @Property
    private int prop4;

    @Property @Access( setter = "valorizeProp5" )
    private String prop5;

    @Property @Access( setter = { "valorizeProp5", "valorizeProp6" } )
    private String prop6;

    @Property @Access( setter = { "valorizeProp6", "valorizeProp5" } )
    private String prop7;
    
    @Classic
    private String classicProperty;
    
    private String sealedProperty;
    
    public PropertyAccessEntity() {
        super();
    }

    public PropertyAccessEntity( String prop1 ) {
        super();
        this.prop1 = prop1;
    }

    public PropertyAccessEntity( String prop1, int prop2 ) {
        this( prop1 );
        this.prop2 = prop2;
    }

    protected void setProp1( Setter<String> setter ) {
        setter.set( "set:" + setter.value() );
    }
    
    protected void setProp2( Setter<Integer> setter ) {
        setter.set( 100 + setter.value() );
    }

    protected String getProp3( Getter<String> getter ) {
        return "get:" + getter.value();
    }
    
    protected int getProp4( Getter<Integer> getter ) {
        return 200 + getter.value();
    }
    
    protected void valorizeProp5( Setter<String> setter ) {
        setter.set( "set:" + setter.value() );
    }

    protected void valorizeProp6( Setter<String> setter ) {
        setter.set( "set2:" + setter.value() );
    }
    
    public int getUnwritableProperty(){
        return 0;
    }
    
    public void setUnreadableProperty( String prop ){

    }

    public String getProp1(){
        return this.prop1;
    }
    
    public void setProp1( String prop1 ){
        this.prop1 = prop1;
    }
    
    public int getProp2(){
        return this.prop2;
    }
    
    public void setProp2( int prop2 ){
        this.prop2 = prop2;
    }
    
    public String getSealedProperty(){
        return this.sealedProperty;
    }
    
    public void setSealedProperty( String sealedProperty ){
        this.sealedProperty = "sealed:" + sealedProperty;
    }

    public String getClassicProperty(){
        return this.classicProperty;
    }
    
    public void setClassicProperty( String classicProperty ){
        this.classicProperty = "classic:" + classicProperty;
    }

    public int getProp4(){
        return this.prop4;
    }
    
    public void setProp4( int prop4 ){
        this.prop4 = prop4;
    }

    public String getProp3(){
        return this.prop3;
    }
    
    public void setProp3( String prop3 ){
        this.prop3 = prop3;
    }

    public String getProp5(){
        return this.prop5;
    }
    
    public void setProp5( String prop5 ){
        this.prop5 = prop5;
    }
    
    public String getProp6(){
        return this.prop6;
    }
    
    public void setProp6( String prop6 ){
        this.prop6 = prop6;
    }

    public String getProp7(){
        return this.prop7;
    }
    
    public void setProp7( String prop7 ){
        this.prop7 = prop7;
    }

}
    

