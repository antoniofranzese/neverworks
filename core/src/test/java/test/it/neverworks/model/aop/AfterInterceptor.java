package test.it.neverworks.model.aop;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public class AfterInterceptor implements Interceptor {
    public Object invoke( Invocation invocation ) {
        invocation.setArgument( 0, ((String) invocation.arguments().get( 0 ) ) + ":after" );
        return invocation.invoke();
    }
}