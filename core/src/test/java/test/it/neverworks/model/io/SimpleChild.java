package test.it.neverworks.model.io;

import java.util.Date;

import com.fasterxml.jackson.databind.JsonNode;

import static it.neverworks.language.*;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.DateConverter;

public class SimpleChild implements JSONEncodable {
    
    private String name;
     
    private int code;
    
    public SimpleChild(){
        super();
    }
    
    public SimpleChild( String name, int code ) {
        super();
        this.name = name;
        this.code = code;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public int getCode(){
        return this.code;
    }
    
    public void setCode( int code ){
        this.code = code;
    }
    
    public JsonNode toJSON() {
        return JSON.encode(
            arg( "name", name )
            .arg( "code", code )
        );
    }
            
}
