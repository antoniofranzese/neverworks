package test.it.neverworks.model.aop;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

class FirstInterceptor implements Interceptor {
    
    private String value;
    
    public FirstInterceptor( String value ) {
        this.value = value;
    }
    
    public Object invoke( Invocation invocation ) {
        //System.out.println( "First invocation with " + invocation.arguments().get( 0 ) );
        if( invocation.arguments().size() > 0 ) {
            invocation.setArgument( 0, "1=" + this.value + ":" + invocation.arguments().get( 0 ) );
            return invocation.invoke();
        } else {
            return "1=" + this.value + ">>" + invocation.invoke();
        }
    }
    
}