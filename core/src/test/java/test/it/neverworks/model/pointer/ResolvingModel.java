package test.it.neverworks.model.pointer;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.features.Pointer;
import it.neverworks.model.features.Resolver;
import it.neverworks.model.features.Resolve;

public class ResolvingModel extends BaseModel implements Resolver {
    
    @Property
    private String name;
    
    @Property @Resolve( strict = true, optional = true, retain = true )
    private Resource resource1;
    
    @Property @Resolve
    private Resource resource2;
    
    public Resource getResource2(){
        return this.resource2;
    }
    
    public void setResource2( Resource resource2 ){
        this.resource2 = resource2;
    }
    
    public Resource getResource1(){
        return this.resource1;
    }
    
    public void setResource1( Resource resource1 ){
        this.resource1 = resource1;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

    public boolean resolves( Pointer pointer ) {
        return true;
    }
    
    public Object resolve( Pointer pointer ) {
        return new Resource()
            .set( "content", this.getClass().getSimpleName() + ":" + name + ":" + pointer.reference() );
            
    }
}