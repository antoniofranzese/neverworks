package test.it.neverworks.model.child;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith( JUnit4.class )
public class ChildObjectTest {

    @Test
    public void propertyChildObjectTest() {
        // ModelInstance, works
        Entity e1 = new Entity();

        /*
        ChildEntity c11 = new ChildEntity();
        c11.setName( "Child1" );

        e1.model().set( "child1", c11 );

        assertThat( c11.getParent(), is( e1 ) );
        assertThat( c11.getLastProperty(), is( "child1" ) );

        ChildEntity c12 = new ChildEntity();
        c12.setName( "Child2" );

        e1.model().set( "child1", c12 );

        assertThat( c11.getParent(), nullValue() );
        assertThat( c11.getLastProperty(), nullValue() );
        assertThat( c12.getParent(), is( e1 ) );
        assertThat( c12.getLastProperty(), is( "child1" ) );

        //Standard setter, works with instrumentation
        Entity e2 = new Entity();

        ChildEntity c21 = new ChildEntity();
        c21.setName( "Child1" );

        e2.setChild1( c21 );

        assertThat( c21.getParent(), is( e2 ) );
        assertThat( c21.getLastProperty(), is( "child1" ) );

        //New style setter, works
        Entity e3 = new Entity();

        ChildEntity c31 = new ChildEntity();
        c31.setName( "Child1" );

        e3.setChild2( c31 );

        assertThat( c31.getParent(), is( e3 ) );
        assertThat( c31.getLastProperty(), is( "child2" ) );

        ChildEntity c32 = new ChildEntity();
        c32.setName( "Child2" );

        e3.setChild2( c32 );

        assertThat( c31.getParent(), nullValue() );
        assertThat( c31.getLastProperty(), nullValue() );
        assertThat( c32.getParent(), is( e3 ) );
        assertThat( c32.getLastProperty(), is( "child2" ) );

        //ModelInstance with New style setter, works
        Entity e4 = new Entity();

        ChildEntity c41 = new ChildEntity();
        c41.setName( "Child1" );

        e4.model().set( "child2", c41 );

        assertThat( c41.getParent(), is( e4 ) );
        assertThat( c41.getLastProperty(), is( "child2" ) );

        ChildEntity c42 = new ChildEntity();
        c42.setName( "Child2" );

        e4.model().set( "child2", c42 );

        assertThat( c41.getParent(), nullValue() );
        assertThat( c41.getLastProperty(), nullValue() );
        assertThat( c42.getParent(), is( e4 ) );
        assertThat( c42.getLastProperty(), is( "child2" ) );

        // Pre-instatiated child
        Entity e5 = new Entity();
        assertThat( e5.getChild3().getParent(), is( e5 ) );
        
        // Default child class
        Entity e6 = new Entity();
        assertThat( e6.getChild4().getParent(), is( e6 ) );
        
        // Default with arguments
        Entity e7 = new Entity( arg( "child4.name", "Child4" ) );
        assertThat( e7.getChild4().getParent(), is( e7 ) );
        assertThat( e7.getChild4().getName(), is( "Child4" ) );
        */
    }
}