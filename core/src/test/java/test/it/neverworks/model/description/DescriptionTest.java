package test.it.neverworks.model.description;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.PropertyManager;
import it.neverworks.model.description.SealedPropertyManager;
import it.neverworks.model.description.ClassicPropertyManager;

@RunWith( JUnit4.class )
public class DescriptionTest {
    
    @Test
    public void beanPropertiesTest() {
        ModelDescriptor model = ModelDescriptor.of( Entity.class );
        assertThat( model.names().size(), is( 4 ) );
        assertThat( model.property( "name" ), instanceOf( PropertyManager.class ) );
        assertThat( model.property( "age" ), instanceOf( PropertyManager.class ) );
        assertThat( model.property( "address" ), instanceOf( SealedPropertyManager.class ) );
        assertThat( model.property( "city" ), not( instanceOf( SealedPropertyManager.class ) ) );
        assertThat( model.property( "city" ), instanceOf( ClassicPropertyManager.class ) );
        
        Entity e = new Entity();
        
        e.set( "name", "MyName" );
        e.set( "age", 0 );
        e.set( "address", "MyAddress" );
        e.set( "city", "MyCity" );
        
        assertThat( e.getName(), is( "MyName" ) );
        assertThat( e.getAge(), is( 0 ) );
        assertThat( e.getAddress(), is( "MyAddress" ) );
        assertThat( e.getCity(), is( "MyCity" ) );
        
    }

    @Test
    public void modelDescriptionInheritanceTest() {
        ModelDescriptor model = ModelDescriptor.of( Inherited.class );
         
        assertThat( model.names().size(), is( 5 ) );
        assertThat( model.property( "name" ), instanceOf( PropertyManager.class ) );
        assertThat( model.property( "age" ), instanceOf( PropertyManager.class ) );
        assertThat( model.property( "address" ), instanceOf( SealedPropertyManager.class ) );
        assertThat( model.property( "code" ), instanceOf( PropertyManager.class ) );
        assertThat( model.property( "city" ), instanceOf( SealedPropertyManager.class ) );
        
        Inherited i = new Inherited();
        
        i.set( "name", "MyName" );
        i.set( "age", 0 );
        i.set( "address", "MyAddress" );
        i.set( "city", "MyCity" );
        i.set( "code", "ABCD" );

        assertThat( i.getName(), is( "MyName" ) );
        assertThat( i.getAge(), is( 0 ) );
        assertThat( i.getAddress(), is( "MyAddress" ) );
        assertThat( i.getCity(), is( "MyCity" ) );
        assertThat( i.getCode(), is( "ABCD" ) );
        
        assertThat( model.property( "code" ).getConverter(), not( nullValue() ) );

        ModelDescriptor model2 = ModelDescriptor.of( SubInherited.class );
        
        // SubInherited redefines code property, avoiding @AutoConvert
        assertThat( model2.property( "code" ).getConverter(), nullValue() );
        
        
    }
}