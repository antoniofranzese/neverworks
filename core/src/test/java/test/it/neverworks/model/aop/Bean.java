package test.it.neverworks.model.aop;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class Bean {
    
    protected String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public String makeName( String prefix ) {
        return prefix + ">" + this.name;
    }
    
    @Loggable
    @First( "_one" )
    @Another( "_other" )
    @Second( "_two" )
    public String interceptedMakeName( String prefix ) {
        return prefix + ">" + this.name;
    }

    @Loggable
    @Second( "_due" )
    @First( "_uno" )
    public String interceptedMakeName2( String prefix, String suffix ) {
        return prefix + ">" + this.name + "<" + suffix;
    }

    @Second( "_B" )
    @Another( "_A" )
    public String interceptedMakeName3( String prefix ) {
        return prefix + ">" + this.name;
    }

    @Loggable
    public String throwingMethod( String prefix ) throws Exception {
        throw new Exception( "Exception" );
    }
    
    

}