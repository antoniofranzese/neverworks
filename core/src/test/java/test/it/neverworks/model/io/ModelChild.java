package test.it.neverworks.model.io;

import java.util.Date;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.DateConverter;
import it.neverworks.model.utils.Identification;

@Identification( string = { "name", "code" })
public class ModelChild extends BaseModel {
    
    @Property         
    private String name;
     
    @Property @AutoConvert
    private int code;

    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public int getCode(){
        return this.code;
    }
    
    public void setCode( int code ){
        this.code = code;
    }
            
}
