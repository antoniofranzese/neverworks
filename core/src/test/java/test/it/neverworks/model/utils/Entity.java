package test.it.neverworks.model.utils;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Calculator;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.Virtual;

public class Entity extends BaseModel {
    
    @Property
    private String name;
    
    //Classic
    private String address;
    
    @Property @Virtual
    private int age;
    
    @Virtual
    public String getCity(){
        return "within:" + address;
    }
    
    private Calculator calc = new Calculator(); 
    
    private Entity parent;
    
    public Entity getParent(){
        return this.parent;
    }
    
    public void setParent( Entity parent ){
        this.parent = parent;
    }
    
    public Calculator getCalc(){
        return this.calc;
    }
    
    public void setCalc( Calculator calc ){
        this.calc = calc;
    }
    
    public void setCity( String city ){
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}

