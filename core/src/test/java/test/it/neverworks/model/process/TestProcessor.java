package test.it.neverworks.model.process;

import it.neverworks.lang.Reflection;
import it.neverworks.aop.MethodCall;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.Setter;

public class TestProcessor implements PropertyProcessor {
    
    public void processProperty( MetaPropertyManager property ) {
        property.placeSetter( new MethodCall( this, Reflection.findMethod( this, "setter", Setter.class ) ) ).after();
    }
    
    public void setter( Setter<String> setter ) {
        setter.set( "processed:" + setter.value() );
    }

}