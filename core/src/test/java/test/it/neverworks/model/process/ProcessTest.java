package test.it.neverworks.model.process;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith( JUnit4.class )
public class ProcessTest {

    @Test
    public void externalGetterSetterProcessorTest() {
        Entity e = new Entity();

        e.setSimple( "Prop1" );
        assertThat( e.getSimple(), is( "processed:Prop1" ) );
        
        e.setSimpleWithSetter( "Prop2" );
        assertThat( e.getSimpleWithSetter(), is( "processed:setSimple:Prop2" ) );
        
        e.setComplex( "Prop3" );
        assertThat( e.getComplex(), is( "retrieved2:processed2:Prop3" ) );
        assertThat( e.model().<String>raw( "complex" ), is( "processed2:Prop3" ) );
        
        e.setComplexWithGetterSetter( "Prop4" );
        assertThat( e.getComplexWithGetterSetter(), is( "getComplex:retrieved2:processed2:setComplex:Prop4" ) );
        assertThat( e.model().<String>raw( "complexWithGetterSetter" ), is( "processed2:setComplex:Prop4" ) );
        
        e.setMultiple( "Prop5" );
        assertThat( e.getMultiple(), is( "retrieved2:processed2:processed:Prop5" ) );
        assertThat( e.model().<String>raw( "multiple" ), is( "processed2:processed:Prop5" ) );
        
        e.setMultipleWithGetterSetter( "Prop6" );
        assertThat( e.getMultipleWithGetterSetter(), is( "getMultiple:retrieved2:processed2:processed:setMultiple:Prop6" ) );
        assertThat( e.model().<String>raw( "multipleWithGetterSetter" ), is( "processed2:processed:setMultiple:Prop6" ) );
    }

}