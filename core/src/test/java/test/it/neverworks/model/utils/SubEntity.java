package test.it.neverworks.model.utils;

public class SubEntity extends Entity {
    
    private String kind;
    
    public String getKind(){
        return this.kind;
    }
    
    public void setKind( String kind ){
        this.kind = kind;
    }
}