package test.it.neverworks.model.aop;

public class MissingValueException extends RuntimeException {
    
    public MissingValueException() {
        super();
    }
}