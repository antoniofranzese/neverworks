package test.it.neverworks.model.aop;

import it.neverworks.model.Property;

public class Inherited extends Entity {
    
    public String interceptedMakeName( String prefix ) {
        return prefix + ">>" + this.name;
    }

    public String interceptedMakeName2( String prefix, String suffix ) {
        return prefix + ">>" + this.name + "<<" + suffix;
    }

    public String anotherMethod( String prefix, String suffix ) {
        return prefix + ">>" + this.name + "<<" + suffix;
    }

}