package test.it.neverworks.model.aop;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

class LoggableInterceptor implements Interceptor {
    
    public static List<String> logs = new ArrayList<String>();
    
    private Method method;
    
    public LoggableInterceptor( Method method ) {
        this.method = method;
    }
    
    public Object invoke( Invocation invocation ) {
        logs.add( "Logging invocation to " + invocation.instance().getClass().getSimpleName() + "." + method.getName() );
        
        try {
            return invocation.invoke();
        } catch( RuntimeException ex ) {
            logs.add( "Error invoking " + invocation.instance().getClass().getSimpleName() + "." + method.getName() + ": " + ex.getClass().getSimpleName() );
            throw ex;
        }
    }
    
}