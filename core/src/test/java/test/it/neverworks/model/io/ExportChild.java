package test.it.neverworks.model.io;

import java.util.Date;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.DateConverter;

import it.neverworks.model.io.Export;

@Export
public class ExportChild extends SimpleChild {
    
    public ExportChild(){
        super();
    }
    
    public ExportChild( String name, int code ) {
        super( name, code );
    }

}
