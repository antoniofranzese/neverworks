package test.it.neverworks.model.expressions;

public class NonModelEntity {
    
    private String name;
    private NonModelEntity parent;
    
    public NonModelEntity getParent(){
        return this.parent;
    }
    
    public void setParent( NonModelEntity parent ){
        this.parent = parent;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public NonModelEntity() {
        
    }
    
    public NonModelEntity( String name ) {
        this.name = name;
    }
}