package test.it.neverworks.model.expandomodel;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.utils.ExpandoModel;
import static it.neverworks.language.*;

@RunWith( JUnit4.class )
public class ExpandoModelTest {

    @Test
    public void expandoModelConverterTest() {
        Entity e = new Entity();
        
        // Standard expando
        e.set( "expando", arg( "string", "Text" ).arg( "number", 10 ) );
        assertThat( e.get( "expando" ), instanceOf( ExpandoModel.class ) );
        
        assertThat( e.getExpando().<String>get( "string" ), is( "Text" ) );
        assertThat( e.getExpando().<Integer>get( "number" ), is( 10 ) );

        assertThat( e.<String>get( "expando.string" ), is( "Text" ) );
        assertThat( e.<Integer>get( "expando.number" ), is( 10 ) );
        
        // Custom expando
        e.set( "custom", arg( "string", "Text1" ).arg( "number", 20 ) );
        assertThat( e.get( "custom" ), instanceOf( CustomExpando.class ) );
        
        assertThat( e.getCustom().<String>get( "string" ), is( "Text1" ) );
        assertThat( e.getCustom().<Integer>get( "number" ), is( 20 ) );

        assertThat( e.<String>get( "custom.string" ), is( "Text1" ) );
        assertThat( e.<Integer>get( "custom.number" ), is( 20 ) );
        
        // Assignment
        e.set( "custom", e.get( "expando" ) );
        assertThat( e.get( "custom" ) , instanceOf( CustomExpando.class ) );
        
        assertThat( e.<String>get( "custom.string" ), is( "Text" ) );
        assertThat( e.<Integer>get( "custom.number" ), is( 10 ) );
        
    }
    
    @Test
    public void expandoModelPropertiesTest() {
        CustomExpando e = new CustomExpando();
        
        assertTrue( e.has( "meta" ) );
        assertTrue( e.has( "sealed" ) );
        assertFalse( e.has( "other" ) );

        e.set( "meta", "Meta1" );
        e.set( "sealed", "Sealed1" );
        e.set( "other", "Other1" );
        
        assertTrue( e.has( "other" ) );
        
        assertThat( e.getMeta(), is( "Meta1" ) );
        assertThat( e.<String>get( "meta" ), is( "Meta1" ) );
        assertThat( e.getSealed(), is( "Sealed1" ) );
        assertThat( e.<String>get( "sealed" ), is( "Sealed1" ) );
        assertThat( e.<String>get( "other" ), is( "Other1" ) );
        
        e.setMeta( "Meta2" );
        e.setSealed( "Sealed2" );

        assertThat( e.<String>get( "meta" ), is( "Meta2" ) );
        assertThat( e.<String>get( "sealed" ), is( "Sealed2" ) );
        
        e.set( "number", "10" );
        assertThat( e.get( "number" ), instanceOf( Integer.class ) );
        assertThat( e.<Integer>get( "number" ), is( 10 ) );

        e.set( "number2", "20" );
        assertThat( e.get( "number2" ), instanceOf( String.class ) );
        
    }
}