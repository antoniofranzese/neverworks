package test.it.neverworks.model.bytecode;

import it.neverworks.model.description.Setter;

class Inherited extends Entity {

    public void setName( Setter<String> setter ){
        setter.set( "pre:" + setter.value() );
    }
    
}