package test.it.neverworks.model.expressions;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.language.*;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

import it.neverworks.model.utils.ExpandoModel;

import static it.neverworks.model.expressions.ExpressionEvaluator.apply;
import static it.neverworks.model.expressions.ExpressionEvaluator.evaluate;
import static it.neverworks.model.expressions.ExpressionEvaluator.valorize;
import static it.neverworks.model.expressions.ExpressionEvaluator.ensure;
import static it.neverworks.model.expressions.ExpressionEvaluator.parse;
import static it.neverworks.model.expressions.ExpressionEvaluator.knows;
import it.neverworks.model.expressions.Modifiers;
import it.neverworks.model.expressions.NullPropertyPathException;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Calculator;

import java.lang.reflect.Constructor;
@RunWith( JUnit4.class )
public class ExpressionTest {
    
    @Test 
    public void expressionsEvaluationOnModelsTest() {
        Entity e = new Entity()
            .set( "name", "Entity1" )
            .set( "parent", new Entity()
                .set( "name", "ParentEntity1" )
                .set( "parent", new Entity()
                    .set( "name", "GrandParent1" )
                )
            );
        
        assertEquals( evaluate( e, "name" ), "Entity1" );
        assertEquals( evaluate( e, "parent.name" ), "ParentEntity1" );
        assertEquals( evaluate( e, "parent.parent.name" ), "GrandParent1" );
        assertEquals( evaluate( e, "parent.parent.parent" ), null );
        
        
        e.value( "children" ).cat(
            new Entity()
                .set( "name", "Child1" )
                .set( "parent", e )
            ,new Entity()
                .set( "name", "Child2" )
                .set( "parent", e )
        );
        
        assertEquals( evaluate( e, "children[0]" ), e.getChildren().get( 0 ) );
        assertEquals( evaluate( e, "children[0].name" ), "Child1" );
        assertEquals( evaluate( e, "children.1.name" ), "Child2" );
        assertEquals( evaluate( e, "children[0].parent.children[1].name" ), "Child2" );
        
        e.value( "parent.children" ).cat( 
            new Entity()
                .set( "name", "ParentChild1" )
                .set( "parent", e.get( "parent" ) )
            ,new Entity()
                .set( "name", "ParentChild2" )
                .set( "parent", e.get( "parent" ) )
        );
        
        assertEquals( evaluate( e, "children[0].parent.parent.name" ), "ParentEntity1" );
        assertEquals( evaluate( e, "children[0].parent.parent.children[0].name" ), "ParentChild1" );
        assertEquals( evaluate( e, "children[0].parent.parent.children[0].parent.children[1].name" ), "ParentChild2" ); 
        
        e.value( "friends" )
            .put( "first", new Entity().set( "name", "Friend1" ) )
            .put( "second", new Entity().set( "name", "Friend2" ) )
            .put( "dot.ted", new Entity().set( "name", "Friend3" ) )
            .put( " spaced ", new Entity().set( "name", "Friend4" ) );
        
        assertEquals( evaluate( e, "friends[ first ].name" ), "Friend1" );
        assertEquals( evaluate( e, "friends[second].name" ), "Friend2" );
        assertEquals( evaluate( e, "friends.second.name" ), "Friend2" );
        assertEquals( evaluate( e, "friends[dot.ted].name" ), "Friend3" );
        assertEquals( evaluate( e, "friends[ ' spaced '].name" ), "Friend4" );
        
        // Valorization and ModelInstance integration
        valorize( e, "parent.name", "RenamedParent1" );
        assertEquals( evaluate( e, "parent.name" ), "RenamedParent1" );

        e.model().assign( "parent.parent.name", "RenamedGrand1" );
        assertEquals( e.model().eval( "parent.parent.name" ), "RenamedGrand1" );
        
    }

    @Test
    public void expressionsOnCollectionTest() {
        List list = new ArrayList();
        list.add( "one" );
        list.add( "two" );
        
        assertEquals( evaluate( list, "[0]" ), "one" );
        assertEquals( evaluate( list, "1" ), "two" );
        
        Map map = new HashMap();
        map.put( "first", "TheFirst" );
        map.put( "second", "TheSecond" );
        list.add( map );

        assertEquals( evaluate( list, "[2][first]" ), "TheFirst" );
        assertEquals( evaluate( list, "2.first" ), "TheFirst" );
        assertEquals( evaluate( list, "[2].second" ), "TheSecond" );
        assertEquals( evaluate( list, "2.[second]" ), "TheSecond" );

        List list2 = new ArrayList();
        list2.add( "ein" );
        list2.add( "zwei" );
        map.put( "third", list2 );

        assertEquals( evaluate( list, "[2][third][0]" ), "ein" );
        assertEquals( evaluate( list, "[2].third[1]" ), "zwei" );
        assertThat( (Integer)evaluate( list, "[2].third.size" ), is( 2 ) );
        assertThat( (Integer)evaluate( list, "[2].third.size()" ), is( 2 ) );
        assertThat( (Integer)evaluate( list, "size" ), is( 3 ) );
        
        // List auto expansion
        Entity e = new Entity().set( "children", list(
            new Entity().set( "name", "Child1" )      
            ,new Entity().set( "name", "Child2" )     
        ));                                            
        
        assertEquals( evaluate( e, "children[0].name" ), "Child1" );
        
        valorize( e, "children[5]", new Entity().set( "name", "Child5" ) );
        assertThat( e.<List>get( "children" ).size(), is( 6 ) );
        assertThat( e.<Integer>get( "children.size" ), is( 6 ) );
        assertThat( e.<Integer>get( "children.size()" ), is( 6 ) );
        assertThat( e.get( "children[0]" ), instanceOf( Entity.class ) );
        assertThat( e.get( "children[1]" ), instanceOf( Entity.class ) );
        assertThat( e.get( "children[2]" ), nullValue() );
        assertThat( e.get( "children[3]" ), nullValue() );
        assertThat( e.get( "children[4]" ), nullValue() );
        assertThat( e.get( "children[5]" ), instanceOf( Entity.class ) );
        
        // Array read
        Object[] ar1 = new Object[]{ "A1", "A2", "A3" };
        String[] ar2 = new String[]{ "B1", "B2", "B3" };
        ExpandoModel[] ar3 = new ExpandoModel[]{
            new ExpandoModel( arg( "name", "Item 1" ) )
            ,new ExpandoModel( arg( "name", "Item 2" ) )
        };
        
        assertThat( (String) evaluate( ar1, "[0]" ), is( "A1" ) );
        assertThat( (String) evaluate( ar2, "[2]" ), is( "B3" ) );
        assertThat( (String) evaluate( ar3, "[1].name" ), is( "Item 2" ) );
        
        valorize( ar1, "[0]", "A0" );
        assertThat( (String) evaluate( ar1, "[0]" ), is( "A0" ) );

        valorize( ar2, "[2]", "B0" );
        assertThat( (String) evaluate( ar2, "[2]" ), is( "B0" ) );
        
        valorize( ar3, "[1].name", "Item 0" );
        assertThat( (String) evaluate( ar3, "[1].name" ), is( "Item 0" ) );
        
    }

    @Test
    public void autoInstantiatingExpressionsTest() throws Exception {
        // Models
        Entity e1 = new Entity().set( "name", "E1" );
    
        assertThat( e1.getParent(), nullValue() );
        
        Entity p1 = ensure( e1, "parent.parent.parent" );
        assertThat( e1.getParent(), notNullValue() );
        assertThat( e1.getParent().getParent(), notNullValue() );
        assertThat( e1.getParent().getParent().getParent(), notNullValue() );
        
        Entity e2 = new Entity().set( "name", "E2" );
        valorize( e2, "parent.parent.parent.name", "P3", new Modifiers().withNulls() );
        assertThat( e2.getParent().getParent().getParent().getName(), is( "P3" ) );

        Entity e3 = new Entity().set( "name", "E3" );
        valorize( e3, "parent.parent.name", "P2", new Modifiers().withNulls() );
        assertThat( e3.getParent().getParent().getName(), is( "P2" ) );
        assertThat( e3.getParent().getParent().getParent(), nullValue() );

        valorize( e3, "parent.parent.parent.parent.name", "P4", new Modifiers().withNulls() );
        assertThat( e3.getParent().getParent().getParent().getParent().getName(), is( "P4" ) );
        
        // Lists
        Entity e4 = new Entity().set( "name", "E4" );
        valorize( e4, "children[2].name", "C2", new Modifiers().withNulls() );
        assertThat( e4.getChildren().size(), is( 3 ) );
        assertThat( e4.getChildren().get( 2 ).getName(), is( "C2" ) );

        Entity e5 = new Entity().set( "name", "E5" );
        valorize( e5, "parent.children[2].name", "PC2", new Modifiers().withNulls() );
        assertThat( e5.getParent().getChildren().get( 2 ).getName(), is( "PC2" ) );

        e5.set( "!parent.parent.children[3].name", "PPC3" );
        assertThat( e5.getParent().getParent().getChildren().get( 3 ).getName(), is( "PPC3" ) );

        // Maps
        Entity e6 = new Entity().set( "name", "E6" );
        valorize( e6, "friends[one].name", "F1", new Modifiers().withNulls() );
        assertThat( e6.getFriends().get( "one" ).getName(), is( "F1" ) );

        valorize( e6, "parent.friends[two].name", "F2", new Modifiers().withNulls() );
        assertThat( e6.getParent().getFriends().get( "two" ).getName(), is( "F2" ) );

        e6.set( "!parent.parent.friends[three].name", "F3" );
        assertThat( e6.getParent().getParent().getFriends().get( "three" ).getName(), is( "F3" ) );

        // Combined
        Entity e7 = new Entity().set( "name", "E7" );
        e7.set( "!parent.children[ 2 ].friends[ john ].name", "John" );
        assertThat( e7.getParent().getChildren().get( 2 ).getFriends().get( "john" ).getName(), is( "John" ) );
        
        
        // Classic bean
        NonModelEntity n1 = new NonModelEntity();
        valorize( n1, "parent.parent.name", "NP2", new Modifiers().withNulls() );
        assertThat( n1.getParent().getParent().getName(), is( "NP2" ) );
    }
    
    @Test
    public void nullPropertyPathErrorReportingTest() {
        Entity e1 = new Entity()
            .set( "name", "E1" )
            .set( "parent", new Entity()
                .set( "name", "P1" ) 
            );

        boolean caught = false;
        try{
            valorize( e1, "parent.parent.parent.name", "P3", new Modifiers().withoutNulls() );
        } catch( Exception ex ) {
            caught = true;
            assertThat( ex, instanceOf( NullPropertyPathException.class ) );
            assertThat( ((NullPropertyPathException) ex).getPath(), is( "parent.parent" ) );
        }
        assertTrue( caught );
        
        e1.set( "parent.parent", new Entity()
            .set( "name", "P2" )
        );
        
        caught = false;
        try{
            valorize( e1, "parent.parent.parent.name", "P3", new Modifiers().withoutNulls() );
        } catch( Exception ex ) {
            caught = true;
            assertThat( ex, instanceOf( NullPropertyPathException.class ) );
            assertThat( ((NullPropertyPathException) ex).getPath(), is( "parent.parent.parent" ) );
        }
        assertTrue( caught );
        
    }

    @Test( expected = NullPropertyPathException.class )
    public void nonInstantiatingExpressionErrorTest() throws Exception {
        Entity e2 = new Entity().set( "name", "E1" ).set( "parent", new Entity().set( "name", "P1" ) );
        valorize( e2, "parent.parent.parent.name", "P3", new Modifiers().withoutNulls() );
    }

    @Test
    public void expressionParserSplitStepsTest() {
        List<String> steps = parse( "ciccio.bu_ffo.pluto[ 0 ].cocco[ puppo].ciaccio[zuppo ][pig.pig][ '  poppo ' ].ci-ccio.[a].cip" ).steps();
    
        assertEquals( steps.size(), 13 );
        assertEquals( steps.get( 0 ), "ciccio" );
        assertEquals( steps.get( 1 ), "bu_ffo" );
        assertEquals( steps.get( 2 ), "pluto" );
        assertEquals( steps.get( 3 ), "0" );
        assertEquals( steps.get( 4 ), "cocco" );
        assertEquals( steps.get( 5 ), "puppo" );
        assertEquals( steps.get( 6 ), "ciaccio" );
        assertEquals( steps.get( 7 ), "zuppo" );
        assertEquals( steps.get( 8 ), "pig.pig" );
        assertEquals( steps.get( 9 ), "  poppo " );
        assertEquals( steps.get( 10 ), "ci-ccio" );
        assertEquals( steps.get( 11 ), "a" );
        assertEquals( steps.get( 12 ), "cip" );
    
    }
    
    @Test
    public void expressionTemplateTestTest() {
        Entity e = new Entity()
            .set( "name", "Entity1" )
            .set( "parent", new Entity()
                .set( "name", "ParentEntity1" )
                .set( "parent", new Entity()
                    .set( "name", "GrandParent1" )
                )
            )
            .set( "children", list( 
                new Entity()
                    .set( "name", "Child1" )
                    .set( "cars", 0 )
                ,new Entity()
                    .set( "name", "Child2" )
                    .set( "cars", 1 )
                    .set( "alive", false )
                ,new Entity()
                    .set( "name", "Child3" )
                    .set( "cars", 2 )
                    
            ));
        
        assertThat(
            apply( e, "My name is {name}" )
            ,is( "My name is Entity1" )
        );

        assertThat(
            apply( e, "{name} is my name" )
            ,is( "Entity1 is my name" )
        );

        assertThat(
            apply( e, "I''m {name}, my first is {children[0].name}, the second is {children[1].name}" )
            ,is( "I'm Entity1, my first is Child1, the second is Child2" )
        );

        assertThat(
            apply( e, "I''m {name}, '{0}' is {children[0].name}, '{1}' is {children[1].name}, '{2}' is {parent.parent.name}" )
            ,is( "I'm Entity1, {0} is Child1, {1} is Child2, {2} is GrandParent1" )
        );

        assertThat(
            apply( e, "{children[0].name} has {children[0].cars,choice,0#no cars|1#one car|1<{?,number,integer} cars}" )
            ,is( "Child1 has no cars" )
        );

        assertThat(
            apply( e, "{children[1].name} has {children[1].cars,choice,0#no cars|1#one car|1<{?,number,integer} cars}" )
            ,is( "Child2 has one car" )
        );

        assertThat(
            apply( e, "{children[2].name} has {children[2].cars,choice,0#no cars|1#one car|1<{?,number,integer} cars}" )
            ,is( "Child3 has 2 cars" )
        );

        assertThat(
            apply( e, "{children[0].name} is {children[0].alive,bool,true=alive|false=dead}" )
            ,is( "Child1 is alive" )
        );

        assertThat(
            apply( e, "{children[1].name} is {children[1].alive,bool,true=alive|false=dead}" )
            ,is( "Child2 is dead" )
        );

    }
  
    @Test
    public void expressionWithWrapperTest() {
        Map values = dict(
            pair( "simple", "Simple string" )
            ,pair( "wrapped", new Wrapper(){
                public Object asObject() {
                    return "Wrapped string";
                }
            })
        );
        
        System.out.println( "MAP: " + values );
            
        assertThat( values.get( "simple" ), instanceOf( String.class ) );
        assertThat( values.get( "wrapped" ), instanceOf( Wrapper.class ));
        
        assertThat( (String) evaluate( values, "simple" ), is( "Simple string" ) );
        assertThat( (String) evaluate( values, "wrapped" ), is( "Wrapped string" ) );
    }
    
    @Test
    public void customExpressionEnsureTest() {
        ExpandoModel model = new ExpandoModel( arg( "name", "Exp1" ) );
        
        assertThat( (String) evaluate( model, "name" ), is( "Exp1" ) );
        
        valorize( model, "!type.kind", "K1" );
        assertThat( evaluate( model, "type" ), instanceOf( ExpandoModel.class ) );
        assertThat( (String) evaluate( model, "type.kind" ), is( "K1" ) );
    }
    
    @Test
    public void knowsExpressionTest() {
        
        // Model
        Entity e = new Entity().set( "parent", new Entity() );
        
        assertTrue( knows( e, "name" ) );
        assertTrue( knows( e, "parent.name" ) );
        assertFalse( knows( e, "city" ) );
        assertFalse( knows( e, "parent.city" ) );
        
        // List
        e.value( "children" ).cat( 
            new Entity()
                .set( "name", "Child1" )
                .set( "parent", e )
        
            ,new Entity()
                .set( "name", "Child2" )
                .set( "parent", e )
        );
        
        assertTrue( knows( e, "children[0]" ) );
        assertTrue( knows( e, "children[0].parent.name" ) );
        assertFalse( knows( e, "children[2]" ) );
        
        // Map
        e.value( "friends" )
            .put( "first", new Entity().set( "name", "Friend1" ) )
            .put( "second", new Entity().set( "name", "Friend2" ) );
        
        assertTrue( knows( e, "friends.first" ) );
        assertTrue( knows( e, "friends.first.name" ) );
        assertFalse( knows( e, "friends.third" ) );
        
        // Classic
        NonModelEntity n = new NonModelEntity();
        
        assertTrue( knows( n, "name" ) );
        assertFalse( knows( n, "city" ) );
        
    }
    
    @Test
    public void optionalExpressionTest() {
        Entity e = new Entity();
        
        valorize( e, "?parentAddress", "x" );
    }
    
    @Test
    public void expressionMethodCallTest() {
        Map values = dict(
            pair( "simple", (Object)"Simple string")
            ,pair( "entity", new Entity().set( "name", "entity1" ) )
        );
        
        assertThat( evaluate( values, "keySet()" ), instanceOf( Collection.class ) );
        assertThat( (Integer) evaluate( values, "keySet().size" ), is( 2 ) );
        assertThat( (String) evaluate( values, "entity.getName()" ), is( "entity1" ) );
        assertThat( (Integer) evaluate( values, "entity.name.length()" ), is( 7 ) );
        assertThat( (Integer) evaluate( values, "entity.getName().length()" ), is( 7 ) );
        assertThat( (Integer) evaluate( values, "entity.name.size" ), is( 7 ) );
    }
    
} 