package test.it.neverworks.model.utils;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.utils.EqualsBuilder;
import it.neverworks.model.description.ModelDescriptor;

@RunWith( JUnit4.class )
public class EqualsTest {

    @Test
    public void equalsBuilderTestTest() {
        Entity e1 = new Entity()
            .set( "name", "E1" )
            .set( "age", 30 )
            .set( "address", "Addr1" );

        Entity e11 = new Entity()
            .set( "name", "E1" )
            .set( "age", 30 )
            .set( "address", "Addr1" );

        Entity e2 = new Entity()
            .set( "name", "E1" )
            .set( "age", 31 )
            .set( "address", "Addr1" );

        SubEntity s1 = new SubEntity()
            .set( "name", "E1" )
            .set( "age", 30 )
            .set( "address", "Addr1" )
            .set( "kind", "Kind1" );

        assertTrue( new EqualsBuilder( e1 )
            .add( "name" )
            .add( "age" )
            .add( "address" )
            .add( "city" )
            .equals( e11 )
        );

        assertTrue( new EqualsBuilder()
            .add( "name" )
            .add( "address" )
            .equals( e1, e2 )
        );

        assertFalse( new EqualsBuilder( e1 )
            .add( "name" )
            .add( "age" )
            .add( "address" )
            .add( "city" )
            .equals( e2 )
        );

        assertFalse( new EqualsBuilder( e1 )
            .add( "name" )
            .add( "age" )
            .add( "address" )
            .add( "city" )
            .equals( s1 )
        );

        assertTrue( new EqualsBuilder( e1 ).withInheritance()
            .add( "name" )
            .add( "age" )
            .add( "address" )
            .add( "city" )
            .equals( s1 )
        );

    }
}