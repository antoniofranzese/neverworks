package test.it.neverworks.model.bytecode;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith( JUnit4.class )
public class WeavingTest {

    @Test
    public void wovenDescriptorInheritanceTest() {
        Entity e = new Entity();
        e.setName( "Name1" );
        assertThat( e.getName(), is( "Name1" ) );
        
        Inherited i = new Inherited();
        i.setName( "Name2" );
        assertThat( i.getName(), is( "pre:Name2" ) );
        
    }
    
}