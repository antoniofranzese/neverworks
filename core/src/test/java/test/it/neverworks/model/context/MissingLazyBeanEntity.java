package test.it.neverworks.model.context;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;

public class MissingLazyBeanEntity extends BaseModel {
    
    @Property @Required @Inject( value = "missingbean", lazy = true )
    private Bean dependency;
    
    public Bean getDependency(){
        return this.dependency;
    }
    
    public void setDependency( Bean dependency ){
        this.dependency = dependency;
    }
    
}