package test.it.neverworks.model.copier;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith( JUnit4.class )
public class CopierTest {

    @Test
    public void modelCopierTest() {
        Entity e1 = new Entity()
            .set( "name", "E1" )
            .set( "address", "address1" )
            .set( "age", 30 );
            
        Entity e2 = new Entity();
        e1.model().copy().to( e2 );

        assertThat( e2.getName(), is( "E1" ) );
        assertThat( e2.getAddress(), is( "address1" ) );
        assertThat( e2.getAge(), is( 30 ) );
        
        Entity e3 = new Entity().set( "name", "E3" ).set( "age", 40 );
        e1.model().copy().excluding( "age" ).to( e3 );
        
        assertThat( e3.getName(), is( "E1" ) );
        assertThat( e3.getAddress(), is( "address1" ) );
        assertThat( e3.getAge(), is( 40 ) );

        Entity e4 = new Entity().set( "age", 50 );
        e1.model().copy().excluding( "age", "address" ).to( e4 );
        
        assertThat( e4.getName(), is( "E1" ) );
        assertThat( e4.getAddress(), nullValue() );
        assertThat( e4.getAge(), is( 50 ) );
        
        Entity e5 = new Entity().set( "name", "E5" );
        e1.model().copy().including( "age", "address" ).to( e5 );
        
        assertThat( e5.getName(), is( "E5" ) );
        assertThat( e5.getAddress(), is( "address1" ) );
        assertThat( e5.getAge(), is( 30 ) );
        
        
    }
}