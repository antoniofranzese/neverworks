package test.it.neverworks.model.expressions;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Mapper;
import it.neverworks.lang.Inspector;
import java.util.Arrays;

import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.Value;

@RunWith( JUnit4.class )
public class QueryTest {
        
    @Test 
    public void objectQueryTest() {
        List list = list(
            new Entity()
                .set( "name", "Name1" )
                .set( "age", 10 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent1" )
                )
            ,new Entity()
                .set( "name", "Name2" )
                .set( "age", 15 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent1" )
                )
            ,new Entity()
                .set( "name", "Name3" )
                .set( "age", 20 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent3" )
                )
            ,new Entity()
                .set( "name", "Name4" )
                .set( "age", 25 )     
        );

        ObjectQuery<Entity> query = new ObjectQuery<Entity>( list );
        
        List result = new ArrayList();
        for( Entity e: query.by( "name" ).eq( "Name2" ) ) {
            result.add( e.getName() );
        }
        assertThat( result, equalsAll( "Name2" ) );

        assertThat( query.by( "parent.name" ).eq( "Parent1" ).map( e -> e.get( "name" ) ), equalsAll( "Name1", "Name2" ) );

        assertThat( query.instanceOf( Entity.class ).by( "name" ).eq( "Name1" ).map( e -> e.get( "name" ) ), equalsAll( "Name1" ) );
        assertThat( query.by( "name" ).ne( "Name1" ).and( "name" ).ne( "Name2" ).map( e -> e.get( "name" ) ), equalsAll( "Name3", "Name4" ) );
        
        // Map search
        Value.of( list ).value( "[0].friends" )
            .put( "one", new Entity().set( "name", "Friend1" ) )
            .put( "two", new Entity().set( "name", "Friend2" ) );

        Value.of( list ).value( "[1].friends" )
            .put( "one", new Entity().set( "name", "Friend2" ) )
            .put( "two", new Entity().set( "name", "Friend1" ) );

        Value.of( list ).value( "[2].friends" )
            .put( "one", new Entity().set( "name", "Friend2" ) );

        Value.of( list ).value( "[3].friends" )
            .put( "two", new Entity().set( "name", "Friend1" ) );
        
        assertThat( query.by( "friends[two].name" ).eq( "Friend2" ).map( e -> e.get( "name" ) ), equalsAll( "Name1" ) );
        assertThat( query.by( "friends[one].name" ).eq( "Friend2" ).map( e -> e.get( "name" ) ), equalsAll( "Name2", "Name3" ) );
        assertThat( query.by( "friends[two].name" ).ne( "Friend2" ).map( e -> e.get( "name" ) ), equalsAll( "Name2", "Name4" ) );
        
        //Regex
        assertThat( query.by( "name" ).matches( "Name[13]+" ).map( e -> e.get( "name" ) ), equalsAll( "Name1", "Name3" ) );
        assertThat( query.by( "name" ).notMatches( "Name[13]+" ).map( e -> e.get( "name" ) ), equalsAll( "Name2", "Name4" ) );


        //Number comparison
        assertThat( query.by( "age" ).gt( 15 ).map( e -> e.get( "name" ) ), equalsAll( "Name3", "Name4" ) );
        assertThat( query.by( "age" ).ge( "15" ).map( e -> e.get( "name" ) ), equalsAll( "Name2", "Name3", "Name4" ) );
        assertThat( query.by( "age" ).eq( 15 ).map( e -> e.get( "name" ) ), equalsAll( "Name2" ) );

        //Reduce
        int ageTotal = query.by( "name" ).matches( "Name[13]+" ).reduce( new Inspector<Entity>(){
            int total = 0;
            public void inspect( Entity item ) {
                total += item.<Integer>get( "age" );
            }
        }).total;
        
        assertThat( ageTotal, is( 30 ) ); // YEAH!!!
        
    }
    
    @Test 
    public void objectQueryInstanceCriteriaTest() {
        Entity e1 = new Entity().set( "name", "Parent1" );
        Entity e2 = new Entity().set( "name", "Parent1" );
        List<Entity> list = list( 
            new Entity()
                .set( "name", "Name1" )
                .set( "parent", e1 )
            ,new Entity()
                .set( "name", "Name2" )
                .set( "parent", e2 )
            ,new Entity()
                .set( "name", "Name3" )
                .set( "parent", e1 )
            ,new Entity()
                .set( "name", "Name4" )
        );
        
        assertThat( query( list ).by( "parent" ).is( e1 ).map( e -> e.get( "name" ) ), equalsAll( "Name1", "Name3" ) );
        assertThat( query( list ).by( "parent" ).notIs( e1 ).map( e -> e.get( "name" ) ), equalsAll( "Name2", "Name4" ) );
    }
    
    @Test
    public void objectQueryMapTest() {
        ObjectQuery<Entity> query = new ObjectQuery<Entity>( testList() );        
        for( Entity parent: query.by( "age" ).ge( 15 ).map( new Mapper<Entity,Entity>(){
            public Entity map( Entity item ) {
                return item.getParent();
            }
        })){
            System.out.println( "TODO> " + parent );
        }
    }
    
    @Test
    public void objectQueryCallTest(){
        ObjectQuery<Entity> query = new ObjectQuery<Entity>( testList() );        
        assertThat( query.by( "children" ).notEmpty().<Entity>call( "child", 0 ).map( e -> e.get( "name" ) ), equalsAll( "Child 1.1", "Child 2.1", "Child 3.1" ) );
        assertThat( query.by( "children" ).notEmpty().<Entity>call( "child", 2 ).map( e -> e.get( "name" ) ), equalsAll( "Child 1.3", "Child 2.3", "Child 3.3" ) );
    }
    
    @Test
    public void objectQuerySizeTest(){
        ObjectQuery<Entity> query = new ObjectQuery<Entity>( testList() );        
        assertThat( query.by( "children.size" ).gt( 3 ).map( e -> e.get( "name" ) ), equalsAll( "Name3" ) );
        assertThat( query.by( "children.size" ).le( 3 ).map( e -> e.get( "name" ) ), equalsAll( "Name1", "Name2", "Name4" ) );
    }

    @Test
    public void objectQueryContainsTest(){
         ObjectQuery<NonModelEntity> query = new ObjectQuery<NonModelEntity>( list(
            new NonModelEntity( "John 1" )
            ,new NonModelEntity( "Jack 2" )
            ,new NonModelEntity( "Dear John 3" )
            ,new NonModelEntity( "Dear Jack 4" )
         ));        
         assertThat(query.by( "name" ).contains( "John" ).map( e -> e.getName() ), equalsAll( "John 1", "Dear John 3" ) );
         assertThat(query.by( "name" ).contains( "ck" ).map( e -> e.getName() ), equalsAll( "Jack 2", "Dear Jack 4" ) );
    }

    @Test
    public void objectSubQueryTest(){
        ObjectQuery<Entity> query = new ObjectQuery<Entity>( list(
             new Entity().set( "name", "John 1" ).set( "age", 1 ).set( "children", list(
                 new Entity().set( "name", "John 10" ).set( "age", 2 )
                 ,new Entity().set( "name", "Dear John 10" ).set( "age", 3 )
             ))
            ,new Entity().set( "name", "Jack 2" ).set( "age", 2 )
            ,new Entity().set( "name", "Dear John 3" ).set( "age", 3 ).set( "children", list(
                 new Entity().set( "name", "Jack 20" ).set( "age", 2 )
                 ,new Entity().set( "name", "Dear Jack 30" ).set( "age", 4 )
             ))
            ,new Entity().set( "name", "Dear Jack 4" ).set( "age", 4 )
        ) );        
        
        assertThat( query
            .by( "name" ).contains( "J" )
            .by( "missingAttribute" ).contains( null )
            .by( "children" ).sub( q -> q.by( "name" ).contains( "John" ).any() )
            .map( e -> e.get( "name" ) ),
            
            equalsAll( "John 1" ) 
        );

        assertThat( query
            .by( "name" ).contains( "J" )
            .by( "missingAttribute" ).contains( null )
            .sub( "children", q -> q.size() == 0 || q.by( "name" ).contains( "John" ).any() )
            .map( e -> e.get( "name" ) ),

            equalsAll( "John 1", "Jack 2", "Dear Jack 4" )
        );

        assertThat( query
            .by( "name" ).contains( "J" )
            .by( "missingAttribute" ).contains( null )
            .by( "children" ).sub( q -> q.by( "name" ).contains( "Jack" ).any() )
            .map( e -> e.get( "name" ) ),

            equalsAll( "Dear John 3" )
        );
        
    }

    @Test
    public void objectQueryCriteriaErasureTest(){
        ObjectQuery<Entity> query = new ObjectQuery<Entity>( list(
            new Entity() .set( "name", "John 1" ).set( "age", 1 )
            ,new Entity().set( "name", "Jack 2" ).set( "age", 2 )
            ,new Entity().set( "name", "Dear John 3" ).set( "age", 3 )
            ,new Entity().set( "name", "Dear Jack 4" ).set( "age", 4 )
         ) );        

         assertThat( query
             .by( "surname" ).contains( null )
             .and( "name" ).contains( "John" )
             .and( "age" ).gt( null )
             .map( e -> e.get( "name" ) ), 
             
             equalsAll( "John 1", "Dear John 3" ) 
         );
    }
    
    private List<Entity> testList() {
        List list = list( 
            new Entity()
                .set( "name", "Name1" )
                .set( "age", 10 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent1" )
                )
                .set( "children", list(
                    new Entity() .set( "name", "Child 1.1" )
                    ,new Entity().set( "name", "Child 1.2" )
                    ,new Entity().set( "name", "Child 1.3" )
                 ))
            ,new Entity()
                .set( "name", "Name2" )
                .set( "age", 15 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent1" )
                )
                .set( "children", list(
                    new Entity() .set( "name", "Child 2.1" )
                    ,new Entity().set( "name", "Child 2.2" )
                    ,new Entity().set( "name", "Child 2.3" )
                ))
            ,new Entity()
                .set( "name", "Name3" )
                .set( "age", 20 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent3" )
                )
                .set( "children", list(
                    new Entity() .set( "name", "Child 3.1" )
                    ,new Entity().set( "name", "Child 3.2" )
                    ,new Entity().set( "name", "Child 3.3" )
                    ,new Entity().set( "name", "Child 3.4" )
                ))
            ,new Entity()
                .set( "name", "Name4" )
                .set( "age", 25 )     
        );
        return list;
    }
    
    @Test
    public void missingExpressionTestv() {
        List<Entity> entities = list(
            new Entity()
                .set( "name", "Entity1" )
                .set( "friends", dict(
                    pair( "first", new Entity().set( "name", "Friend1" ) )
                    ,pair( "second", new Entity().set( "name", "Friend2" ) )
                ))
            ,new Entity()
                .set( "name", "Entity2" )
                .set( "friends", dict(
                    pair( "first", new Entity().set( "name", "Friend3" ) )
                    ,pair( "third", new Entity().set( "name", "Friend4" ) )
                ))
        ).as( Entity.class );
        
        assertThat( query( entities ).by( "friends.second" ).isMissing().map( e -> e.get( "name" ) ), equalsAll( "Entity2" ) );
        assertThat( query( entities ).by( "friends.second" ).notMissing().map( e -> e.get( "name" ) ), equalsAll( "Entity1" ) );
        assertThat( query( entities ).by( "friends.first" ).isMissing().map( e -> e.get( "name" ) ), equalsAll() );
        assertThat( query( entities ).by( "friends.first" ).notMissing().map( e -> e.get( "name" ) ), equalsAll( "Entity1", "Entity2" ) );
    }
    
}