package test.it.neverworks.model.undefined;

import it.neverworks.model.description.UndefinedInspector;
import it.neverworks.model.description.ModelInstance;

public class TestIntUndefined implements UndefinedInspector {
    public static int Undefined = 0;
    
    public boolean checkUndefined( ModelInstance instance, Object value ) {
        return ((Integer)value).intValue() == Undefined;
    }
    
    public Object undefinedValue( ModelInstance instance ) {
        return Undefined;
    }
}