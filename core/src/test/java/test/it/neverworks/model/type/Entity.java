package test.it.neverworks.model.type;

import java.util.Date;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.types.Type;
import it.neverworks.model.types.AutoType;

public class Entity extends BaseModel {
    
    @Property @Convert( IntConverter.class ) 
    private Object uncheckedInt;

    @Property @Type( Integer.class ) @Convert( IntConverter.class )
    private Object checkedInt;
    
    @Property @AutoType( Integer.class )
    private Object autoCheckedInt;
    
    @Property @Type( Person.class )
    private Object person;
    
    public Object getPerson(){
        return this.person;
    }
    
    public void setPerson( Object person ){
        this.person = person;
    }
    
    public Object getAutoCheckedInt(){
        return this.autoCheckedInt;
    }
    
    public void setAutoCheckedInt( Object autoCheckedInt ){
        this.autoCheckedInt = autoCheckedInt;
    }
    
    public Object getCheckedInt(){
        return this.checkedInt;
    }
    
    public void setCheckedInt( Object checkedInt ){
        this.checkedInt = checkedInt;
    }
    
    public Object getUncheckedInt(){
        return this.uncheckedInt;
    }
    
    public void setUncheckedInt( Object uncheckedInt ){
        this.uncheckedInt = uncheckedInt;
    }

}
