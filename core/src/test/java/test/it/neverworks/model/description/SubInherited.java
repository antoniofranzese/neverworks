package test.it.neverworks.model.description;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

public class SubInherited extends Inherited {
    
    @Property 
    private String code;
    
}