package test.it.neverworks.model.watchable;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Watcher;
import it.neverworks.model.description.Changes;

@Watchable
public class InterceptingEntity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property
    private String surname;
    
    public String getSurname(){
        return this.surname;
    }
    
    public void setSurname( String surname ){
        this.surname = surname;
    }
    
    @Property
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    private int watchCount = 0;
    
    public int getWatchCount(){
        return this.watchCount;
    }
    
    protected void watch( Watcher w ) {
        this.watchCount++;
    }
    
    protected Changes changes( Changes changes ) {
        if( changes.contains( "name" ) ) {
            changes.add( "surname" );
        }
        return changes;
    }
    
}