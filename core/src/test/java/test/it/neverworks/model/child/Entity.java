package test.it.neverworks.model.child;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.Default;

class Entity extends BaseModel {
    
    public Entity(){
        super();
    }
    
    public Entity( Arguments arguments ) {
        this();
        arguments.populate( this );
    }
    
    @Property
    private String name;

    public String getName(){
        return this.name;
    }

    public void setName( String name ){
        this.name = name;
    }

    @Property @Child
    private ChildEntity child1;

    public ChildEntity getChild1(){
        return this.child1;
    }

    public void setChild1( ChildEntity child1 ){
        this.child1 = child1;
    }

    @Property @Child
    private ChildEntity child2;

    public ChildEntity getChild2(){
        return child2;
    }

    public void setChild2( ChildEntity child2 ){
        this.child2 = child2;
    }

    @Property @Child
    private ChildEntity child3 = new ChildEntity().set( "name", "PreChild3" );

    public ChildEntity getChild3(){
        return this.child3;
    }

    public void setChild3( ChildEntity child3 ){
        this.child3 = child3;
    }

    @Property @Child @Default( ChildEntity.class )
    private ChildEntity child4;
    
    public ChildEntity getChild4(){
        return this.child4;
    }
    
    public void setChild4( ChildEntity child4 ){
        this.child4 = child4;
    }
    
}

