package test.it.neverworks.model.proxymodel;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.utils.EqualsBuilder;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.utils.ProxyModel;

@RunWith( JUnit4.class )
public class ProxyModelTest {

    @Test
    public void proxyModelTest() {
        Entity e = new Entity().set( "name", "Entity1" );
        
        ProxyModel p = new ProxyModel( e ) {
            public String getFullName() {
                return "name:" + target( "name" );
            }
            
            public void setFullName( String value ) {
                target( "name", "<" + value + ">" );
            }
        };

        assertThat( p.<String>get( "name" ), is( "Entity1" ) );
        assertThat( p.<String>get( "fullName" ), is( "name:Entity1" ) );

        p.set( "name", "Entity2" );

        assertThat( p.<String>get( "name" ), is( "Entity2" ) );
        assertThat( p.<String>get( "fullName" ), is( "name:Entity2" ) );

        p.set( "fullName", "Entity3" );

        assertThat( p.<String>get( "name" ), is( "<Entity3>" ) );
        assertThat( p.<String>get( "fullName" ), is( "name:<Entity3>" ) );

    }
}