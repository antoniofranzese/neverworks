package test.it.neverworks.model.type;

import java.util.Date;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.ToStringBuilder;

@Convert( PersonConverter.class )
public class Person extends BaseModel {
    
    @Property         
    private String name;
     
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

    public String toString() {
        return new ToStringBuilder( this ).add( "name" ).toString();
    }
    
}
