package test.it.neverworks.model.instance;

import java.util.Date;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.DateConverter;

public class Entity extends BaseModel {
    
    @Property         
    private String name;
     
    @Property @Convert( IntConverter.class ) 
    private int age;
    
    @Property @AutoConvert
    private int code;

    @Property @AutoConvert
    private String address;
    
    @Property @Convert( DateConverter.class )
    private Date birth;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
       
    public int getCode(){
        return this.code;
    }
    
    public void setCode( int code ){
        this.code = code;
    }
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }
    
    public Date getBirth(){
        return this.birth;
    }
    
    public void setBirth( Date birth ){
        this.birth = birth;
    }
        
}
