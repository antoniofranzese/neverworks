package test.it.neverworks.model.converter;

public enum Kind {
    SIMPLE, MEDIUM, COMPLEX
}