package test.it.neverworks.model.events;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Wire;
import it.neverworks.model.events.Subscribe;

public class Sub extends Entity {
        
    @Property @Subscribe( RemoveEvent.class )
    private Signal end;
    
    @Property
    private Signal process;
    
    @Wire( "end" )
    public void endMethod( Event event ) {
        log.add( "END HANDLER " + this.name + " " + event.getSource() );
    }

    @Wire({ "start", "process" })
    @Subscribe( RemoveEvent.class )
    public void dualMethod() {
        log.add( "DUAL HANDLER " + this.name  );
    }
    
    @Subscribe
    public void placeMethod( PlaceEvent event ) {
        log.add( "SUB PLACE HANDLER " + this.name + " " + event.getSource() );
    }
    
    public Signal getProcess(){
        return this.process;
    }
    
    public void setProcess( Signal process ){
        this.process = process;
    }
    
    public Signal getEnd(){
        return this.end;
    }
    
    public void setEnd( Signal end ){
        this.end = end;
    }
}