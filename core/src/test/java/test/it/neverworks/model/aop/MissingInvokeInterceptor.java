package test.it.neverworks.model.aop;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public class MissingInvokeInterceptor implements Interceptor {
    
    public Object invoke( Invocation invocation ) {
        // Does nothing
        return null;
    }
}