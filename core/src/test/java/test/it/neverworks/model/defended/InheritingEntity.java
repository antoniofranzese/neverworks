package test.it.neverworks.model.defended;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Inherited;
import it.neverworks.model.converters.AutoConvert;

public class InheritingEntity extends BaseEntity {

    @Property @Inherited @AutoConvert
    private String name;
    
}