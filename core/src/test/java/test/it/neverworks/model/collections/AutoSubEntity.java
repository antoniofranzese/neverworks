package test.it.neverworks.model.collections;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;

@Convert( AutoSubEntityConverter.class )
class AutoSubEntity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    private Entity parent;
    
    public Entity getParent(){
        return this.parent;
    }
    
    public void setParent( Entity parent ){
        this.parent = parent;
    }

    public String toString() {
        return "AutoSubEntity:" + name;
    }
    
}

