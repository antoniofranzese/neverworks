package test.it.neverworks.model.value;

import java.util.Map;
import java.util.List;
import java.util.Set;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

class Entity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    @Property
    private boolean checked;
    
    public boolean getChecked(){
        return this.checked;
    }
    
    public void setChecked( boolean checked ){
        this.checked = checked;
    }
    
    @Property
    private List<String> list;
    
    public List<String> getList(){
        return this.list;
    }
    
    public void setList( List<String> list ){
        this.list = list;
    }
    
    @Property
    private List<Integer> intList;
    
    public List<Integer> getIntList(){
        return this.intList;
    }
    
    public void setIntList( List<Integer> intList ){
        this.intList = intList;
    }
    
    @Property
    private List<Entity> entityList;
    
    public List<Entity> getEntityList(){
        return this.entityList;
    }
    
    public void setEntityList( List<Entity> entityList ){
        this.entityList = entityList;
    }
    
    @Property
    private Map<String, String> map;
    
    public Map<String, String> getMap(){
        return this.map;
    }
    
    public void setMap( Map<String, String> map ){
        this.map = map;
    }

    private Set<String> set;
    
    public Set<String> getSet(){
        return this.set;
    }
    
    public void setSet( Set<String> set ){
        this.set = set;
    }
    
    public String toString() {
        return "Entity:" + name;
    }
}

