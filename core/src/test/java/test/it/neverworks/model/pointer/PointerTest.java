package test.it.neverworks.model.pointer;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.features.Pointer;
import it.neverworks.model.features.BasePointer;
import it.neverworks.model.features.UnresolvedPointerException;

@RunWith( JUnit4.class )
public class PointerTest {

    @Test
    public void pointerTestTest() {
        Pointer p1 = new BasePointer( "resource1" );
        
        ResolvingModel m1 = new ResolvingModel().set( "name", "Model1");
        ResolvingModel m2 = new ResolvingModel().set( "name", "Model2");
        
        Resource r1 = (Resource)m1.resolve( p1 );
        assertThat( r1, notNullValue() );
        assertThat( r1.getContent(), is( "ResolvingModel:Model1:resource1" ) );
        
        Pointer p2 = new BasePointer( m1, "resource2" );

        Resource r2 = (Resource)p2.resolve();
        assertThat( r2, notNullValue() );
        assertThat( r2.getContent(), is( "ResolvingModel:Model1:resource2" ) );

        Resource r3 = (Resource)m2.resolve( p2 );
        assertThat( r3, notNullValue() );
        assertThat( r3.getContent(), is( "ResolvingModel:Model2:resource2" ) );

        Resource r4 = (Resource)p2.resolve( m2 );
        assertThat( r4, notNullValue() );
        assertThat( r4.getContent(), is( "ResolvingModel:Model2:resource2" ) );
        
    }
    
    @Test( expected = UnresolvedPointerException.class )
    public void unresolvablePointerTestTest() {
        Pointer p1 = new BasePointer( "resource1" );
        p1.resolve();
    }
    
}