package test.it.neverworks.model.converter;

import it.neverworks.model.converters.AutoConvert;

@AutoConvert
public enum AutoKind {
    SIMPLE, MEDIUM, COMPLEX
}