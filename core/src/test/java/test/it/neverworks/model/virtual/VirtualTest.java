package test.it.neverworks.model.virtual;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.ModelDescriptor;

@RunWith( JUnit4.class )
public class VirtualTest {

    @Test
    public void virtualPropertiesDescriptionTest() {
        ModelDescriptor model = ModelDescriptor.of( Entity.class );
        
        assertFalse( "unexpected virtual on meta property", model.property( "name" ).getVirtual() );
        assertTrue(  "expected virtual on meta property", model.property( "age" ).getVirtual() );
        assertFalse( "unexpected virtual on classic property", model.property( "address" ).getVirtual() );
        assertTrue(  "expected virtual on classic property", model.property( "city" ).getVirtual() );
    }
}