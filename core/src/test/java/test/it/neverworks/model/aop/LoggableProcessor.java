package test.it.neverworks.model.aop;

import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

public class LoggableProcessor implements MethodProcessor {
    
    public void processMethod( MethodDescriptor method ) {
        //System.out.println( "Loggable processing " + method.getActual() );
        method.place( new LoggableInterceptor( method.actual() ) ).first();
    }
    
}