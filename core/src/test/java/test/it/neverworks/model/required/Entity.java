package test.it.neverworks.model.required;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Required;

class Entity extends BaseModel {
    
    @Property @Required
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property
    private String address;
    
    public String getAddress(){
        return this.address;
    }
    
    public void setAddress( String address ){
        this.address = address;
    }

}

