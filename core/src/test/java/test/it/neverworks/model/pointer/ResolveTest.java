package test.it.neverworks.model.pointer;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.features.Pointer;
import it.neverworks.model.features.BasePointer;
import it.neverworks.model.features.ResolveModule;

@RunWith( JUnit4.class )
public class ResolveTest {

    @Test
    public void resolveTestTest() {
        //Base
        ResolvingModel m1 = new ResolvingModel().set( "name", "Model1" );
        
        m1.set( "resource1", new BasePointer( "Resource1" ) );
        
        assertThat( m1.model().raw( "resource1" ), nullValue() );
        assertThat( m1.model().module( ResolveModule.class ).get( "resource1" ), instanceOf( Pointer.class ) );
        
        Resource r1 = m1.get( "resource1" );

        // retained pointer
        assertThat( m1.model().raw( "resource1" ), nullValue() );
        assertThat( m1.model().module( ResolveModule.class ).get( "resource1" ), instanceOf( Pointer.class ) );

        assertThat( r1.getContent(), is( "ResolvingModel:Model1:Resource1" ) );
        
        m1.set( "name", "Renamed1" );
        Resource r2 = m1.get( "resource1" );

        // retainer pointer new evaluation
        assertThat( m1.model().raw( "resource1" ), nullValue() );
        assertThat( r2.getContent(), is( "ResolvingModel:Renamed1:Resource1" ) );
        
        m1.set( "resource1", new Resource().set( "content", "StaticResource" ) );
        assertThat( m1.model().raw( "resource1" ), notNullValue() );
        assertFalse( m1.model().module( ResolveModule.class ).has( "resource1" ) );

        Resource r3 = m1.get( "resource1" );
        assertThat( r3.getContent(), is( "StaticResource" ) );

        m1.set( "resource2", "Resource2" );

        assertThat( m1.model().raw( "resource2" ), nullValue() );
        assertThat( m1.model().module( ResolveModule.class ).get( "resource2" ), instanceOf( Pointer.class ) );

        Resource r4 = m1.get( "resource2" );

        // discarded pointer
        assertThat( m1.model().raw( "resource2" ), notNullValue() );
        assertFalse( m1.model().module( ResolveModule.class ).has( "resource2" ) );
        
        assertThat( r4.getContent(), is( "ResolvingModel:Renamed1:Resource2" ) );
        
        //Inherited
        InheritedResolvingModel im2 = new InheritedResolvingModel().set( "name", "Model2" );

        im2.set( "resource1", new BasePointer( "Resource1" ) );

        assertThat( im2.model().raw( "resource1" ), nullValue() );
        assertThat( im2.model().module( ResolveModule.class ).get( "resource1" ), instanceOf( Pointer.class ) );

        Resource ir1 = im2.get( "resource1" );

        assertThat( im2.model().raw( "resource1" ), nullValue() );
        assertThat( im2.model().module( ResolveModule.class ).get( "resource1" ), instanceOf( Pointer.class ) );
        assertThat( ir1.getContent(), is( "InheritedResolvingModel:Model2:Resource1" ) );

        im2.set( "name", "Renamed2" );
        Resource ir2 = im2.get( "resource1" );

        assertThat( im2.model().raw( "resource1" ), nullValue() );
        assertThat( ir2.getContent(), is( "InheritedResolvingModel:Renamed2:Resource1" ) );

        im2.set( "resource1", new Resource().set( "content", "StaticResource" ) );
        assertThat( im2.model().raw( "resource1" ), notNullValue() );
        assertFalse( im2.model().module( ResolveModule.class ).has( "resource1" ) );

        Resource ir3 = im2.get( "resource1" );
        assertThat( ir3.getContent(), is( "StaticResource" ) );

        im2.set( "resource2", "Resource2" );

        assertThat( im2.model().raw( "resource2" ), nullValue() );
        assertThat( im2.model().module( ResolveModule.class ).get( "resource2" ), instanceOf( Pointer.class ) );

        Resource ir4 = im2.get( "resource2" );

        assertThat( im2.model().raw( "resource2" ), notNullValue() );
        assertFalse( im2.model().module( ResolveModule.class ).has( "resource2" ) );
        assertThat( ir4.getContent(), is( "InheritedResolvingModel:Renamed2:Resource2" ) );
        
    }
    
    @Test( expected = IllegalArgumentException.class )
    public void strictPointerTestTest() {
        ResolvingModel m1 = new ResolvingModel();
        m1.set( "resource1", "Resource1" );
    }
    
}