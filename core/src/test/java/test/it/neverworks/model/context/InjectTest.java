package test.it.neverworks.model.context;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.context.Context;

import it.neverworks.model.description.RequiredPropertyException;

import it.neverworks.lang.Application;
import it.neverworks.context.Component;
import it.neverworks.model.description.PropertyPostProcessor;

@RunWith( JUnit4.class )
public class InjectTest {

    @Test
    public void injectTest() {

            // try {
            //      Context.init( "/model/context/inject-context.xml" );
                 
            //      System.out.println( Context.<Bean>get( "bean1" ) );
            //      System.out.println( Context.<Bean>get( "entity1" ) );
            //      System.out.println( Context.<Bean>get( "entity2" ) );

        
            // } catch( Exception ex ) {
            //     System.out.println( "EXCEPT");
            //     ex.printStackTrace();
            // }
        within( Context.init( "/model/context/inject-context.xml" ), ctx -> {
            
            // Constructor injection
            Entity e = new Entity();
            assertThat( e.model().raw( "dependency" ), not( nullValue() ) );
            Bean b = e.model().raw( "dependency" );
            assertThat( b.getName(), is( "Bean1" ) );

            assertThat( e.getString1(), is( "ContextString1" ) );
            assertThat( e.getString2(), is( "ContextString2" ) );
            assertThat( e.getString3(), nullValue() );

            // Scope
            Bean bd = e.getDependency();
            assertTrue( bd == b );

            Entity e2 = Context.get( "entity2" );
            assertThat( e2.getString1(), is( "BeanString1" ) );
            assertThat( e2.getString2(), is( "BeanString2" ) );
            assertThat( e2.getString3(), is( "BeanString3" ) );

            // Lazy injection
            assertThat( e.model().raw( "lazyDependency" ), nullValue() );
            assertThat( e.get( "lazyDependency" ), not( nullValue() ) );
            assertThat( e.model().raw( "lazyDependency" ), not( nullValue() ) );

            assertThat( e.getLazyDependency().getName(), is( "Lazy1" ) );

            // Type injection
            assertThat( e.model().raw( "byType" ), not( nullValue() ) );
            ByTypeBean t = e.model().raw( "byType" );
            assertThat( t.getName(), is( "Bean3" ) );

            // Autowiring
            Entity e1 = Context.get( "entity1" );
            assertThat( e1.model().raw( "dependency" ), not( nullValue() ) );
            Bean b3 = e1.model().raw( "dependency" );
            assertThat( b3.getName(), is( "Bean1" ) );
            
        });
    
    }
    
    @Test( expected = RequiredPropertyException.class )
    public void missingInjectionErrorTest() {
        MissingBeanEntity e = new MissingBeanEntity();
    }

    @Test( expected = RequiredPropertyException.class )
    public void missingLazyInjectionErrorTest() {
        MissingLazyBeanEntity e = new MissingLazyBeanEntity();
        assertThat( e, not( nullValue() ) );
        e.getDependency();
    }


}