package test.it.neverworks.model.events;

import java.util.List;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;
import it.neverworks.model.events.Signal;
import it.neverworks.model.events.Event;
import it.neverworks.model.events.Wire;
import it.neverworks.model.events.Router;
import it.neverworks.model.events.EventPublisher;
import it.neverworks.model.events.Subscribe;
import it.neverworks.model.utils.Identification;

@Identification( "name" )
public class Entity extends BaseModel implements EventPublisher {
    
    @Property
    protected Signal start;
    
    @Property
    protected String name;
    
    @Property
    protected List<String> log;
    
    @Property @Wire( "start" )
    private Signal startup;
    
    @Property @Default
    private Router router;
    
    @Wire( "start" )
    public void startMethod( Event event ) {
        log.add( "START HANDLER " + this.name );
    }

    @Wire( "startup" )
    public void startupMethod() {
       log.add( "STARTUP HANDLER " + this.name );
    }
    
    @Subscribe( PlaceEvent.class )
    public void placeMethod() {
        log.add( "PLACE HANDLER " + this.name );
    }

    @Subscribe( RemoveEvent.class )
    public void removeMethod() {
        log.add( "REMOVE HANDLER " + this.name );
    }
    
    public Signal retrievePublisher( Class<?> eventClass ) {
        return getRouter().retrieveRoute( eventClass );
    }
    
    public void place() {
        getRouter().dispatch( new PlaceEvent().set( "source", this ) );
    }

    public void remove() {
        getRouter().dispatch( new RemoveEvent().set( "source", this ) );
    }
    
    public Router getRouter(){
        return this.router;
    }
    
    public Signal getStart(){
        return this.start;
    }
    
    public void setStart( Signal start ){
        this.start = start;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

    public List<String> getLog(){
        return this.log;
    }
    
    public void setLog( List<String> log ){
        this.log = log;
    }

    public Signal getStartup(){
        return this.startup;
    }
    
    public void setStartup( Signal startup ){
        this.startup = startup;
    }
    
}