package test.it.neverworks.model.inheritance;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Child;

class SubEntity extends Entity {
    
    @Property 
    private String name;
    
    public String mySubName() {
        return "subname:" + this.name;
    }
    
    
}

