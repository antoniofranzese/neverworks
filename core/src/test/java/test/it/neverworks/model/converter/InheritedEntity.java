package test.it.neverworks.model.converter;

import it.neverworks.lang.Strings;

public class InheritedEntity extends ConverterEntity {

    @Override
    protected Object convertIndex( Object value ) {
        return "[" + Strings.valueOf( value ) + "]";
    }
    
}