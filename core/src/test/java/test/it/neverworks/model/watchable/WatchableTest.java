package test.it.neverworks.model.watchable;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.ModelDescriptor;

@RunWith( JUnit4.class )
public class WatchableTest {

    @Test
    public void simpleWatchableModelTest() {
        Entity e = new Entity();
       
        assertThat( e.model().watching(), is( false ) );
        assertThat( e.model().changes().size(), is( 0 ) );

        e.setName( "name1" );
        e.setAge( 10 );
        assertThat( e.model().changes().size(), is( 0 ) );
        
        e.model().watch();
        assertThat( e.model().watching(), is( true ) );
        
        e.setName( "name2" );
        assertThat( e.model().changes().size(), is( 1 ) );
        assertThat( e.model().changes(), hasItem( "name" ) );

        // Ignored property
        e.setAddress( "address1" );
        assertThat( e.model().changes().size(), is( 1 ) );
        assertThat( e.model().changes(), not( hasItem( "address" ) ) );
        
        e.model().ignore();
        assertThat( e.model().watching(), is( false ) );
        assertThat( e.model().changes().size(), is( 0 ) );

        e.model().watch();
        e.setName( "name2" ); // No change
        assertThat( e.model().changes().size(), is( 0 ) );
        
        e.setAge( 10 ); // No change
        assertThat( e.model().changes().size(), is( 0 ) );
        
        e.setAge( 15 ); // Change
        assertThat( e.model().changes().size(), is( 1 ) );
        assertThat( e.model().changes(), hasItem( "age" ) );
        
        e.model().ignore();
        
        // Sealed properties
        e.model().watch();
        e.set( "watchableSealed", "W1" );
        e.set( "ignoredSealed", "I1" );
        assertThat( e.model().changes().size(), is( 1 ) );
        assertThat( e.model().changes(), hasItem( "watchableSealed" ) );
        e.model().ignore();
    }
    
    @Test
    public void interceptingWatchableModelTest() {
        InterceptingEntity e = new InterceptingEntity();
        
        assertThat( e.getWatchCount(), is( 0 ) );
        
        e.model().watch();
        assertThat( e.getWatchCount(), is( 1 ) );
        
        e.setName( "name1" );
        assertThat( e.model().changes().size(), is( 2 ) );
        assertThat( e.model().changes(), hasItem( "name" ) );
        assertThat( e.model().changes(), hasItem( "surname" ) );
        
        e.model().ignore();
        
        e.model().watch();
        assertThat( e.getWatchCount(), is( 2 ) );
        
        e.setAge( 20 );
        assertThat( e.model().changes().size(), is( 1 ) );
        assertThat( e.model().changes(), hasItem( "age" ) );

    }
    
    @Test
    public void watchCascadingTestTest() {
        Entity e = new Entity();
        e.model().watch();
        
        e.set( "name", "Entity1" );
        e.set( "!mother.name", "Mother1" );
        e.set( "!father.name", "Father1" );
        
        assertThat( e.model().changes().size(), is( 3 ) );
        assertThat( e.model().changes(), hasItem( "name" ) );
        assertThat( e.model().changes(), hasItem( "mother" ) );
        assertThat( e.model().changes(), hasItem( "father" ) );
        
        e.model().ignore();
        
        //-----------

        e.model().watch();
        
        e.set( "name", "Entity2" );
        e.set( "!mother.name", "Mother2" );
        e.set( "!father.name", "Father2" );

        assertThat( e.model().changes().size(), is( 3 ) );
        assertThat( e.model().changes(), hasItem( "name" ) );
        assertThat( e.model().changes(), hasItem( "mother.name" ) );
        assertThat( e.model().changes(), hasItem( "father.name" ) );
        
        e.model().ignore();

        
        
    }
}