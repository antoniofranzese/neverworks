package test.it.neverworks.model.default_;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;

class DefaultEntity extends BaseModel {
    
    @Property @Default( ArrayList.class )
    private List list1;
    
    public List getList1(){
        return this.list1;
    }
    
    public void setList1( List list2 ){
        this.list1 = list1;
    }

    @Property @Default( ArrayList.class )
    private List list2;
    
    public List getList2(){
        return list2;
        // return direct( "list2" ).as( List.class );
    }
    
    public void setList2( List list2 ){
        this.list2 = list2;
        // direct( "list2" ).set( list2 );
    }

}

