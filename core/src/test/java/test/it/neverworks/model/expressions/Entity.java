package test.it.neverworks.model.expressions;

import it.neverworks.model.collections.Map;
import it.neverworks.model.collections.List;
import it.neverworks.model.collections.ArrayList;
import it.neverworks.model.collections.HashMap;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;

class Entity extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    @Property
    private int age;
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }

    @Property
    private Entity parent;
    
    public Entity getParent(){
        return this.parent;
    }
    
    public void setParent( Entity parent ){
        this.parent = parent;
    }
    
    @Property @Collection
    private List<Entity> children;
    
    public List<Entity> getChildren(){
        return this.children;
    }
    
    public void setChildren( List<Entity> children ){
        this.children = children;
    }
    
    @Property @Collection
    private Map<String, Entity> friends;
    
    public Map<String, Entity> getFriends(){
        return this.friends;
    }
    
    public void setFriends( Map<String, Entity> friends ){
        this.friends = friends;
    }
    
    public String toString() {
        return "<Entity " + name + ">";
    }
    
    @Property
    private int cars;
    
    public int getCars(){
        return this.cars;
    }
    
    public void setCars( int cars ){
        this.cars = cars;
    }
    
    private boolean alive = true; 
    
    public boolean getAlive(){
        return this.alive;
    }
    
    public void setAlive( boolean alive ){
        this.alive = alive;
    }
    
    public Entity child( Integer index ) {
        return getChildren().get( index );
    }
}

