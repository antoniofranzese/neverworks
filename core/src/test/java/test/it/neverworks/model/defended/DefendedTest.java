package test.it.neverworks.model.defended;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.description.DefendedPropertyViolation;

@RunWith( JUnit4.class )
public class DefendedTest {

    @Test
    public void defendedPropertyTest() {
        BaseEntity b = new BaseEntity();
        b.setName( 10 );
        assertThat( b.getName(), instanceOf( Integer.class ) );
        
        InheritingEntity i = new InheritingEntity();
        i.setName( 10 );
        assertThat( i.getName(), instanceOf( String.class ) );
    }
    
    @Test( expected = DefendedPropertyViolation.class )
    public void defendedMetaPropertyViolationTest() {
        HidingEntity h = new HidingEntity();
    }
    
    @Test( expected = DefendedPropertyViolation.class )
    public void defendedClassicPropertyViolationTest() {
        BeanHidingEntity b = new BeanHidingEntity();
    }
}