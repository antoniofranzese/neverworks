package test.it.neverworks.model.watchable;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Watchable;
import it.neverworks.model.description.Ignore;
import it.neverworks.model.description.Watch;

@Watchable
public class Person extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}