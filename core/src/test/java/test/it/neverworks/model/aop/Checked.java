package test.it.neverworks.model.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.MethodAnnotation;

@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@MethodAnnotation( processor = CheckedInterceptor.class )
public @interface Checked {
    String value();
}
