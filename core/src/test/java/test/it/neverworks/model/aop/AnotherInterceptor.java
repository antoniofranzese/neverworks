package test.it.neverworks.model.aop;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

class AnotherInterceptor implements Interceptor, MethodProcessor {
    
    private String value;
    
    public AnotherInterceptor( AnnotationInfo<Another> info ) {
        this.value = info.annotation().value();
    }
    
    public void processMethod( MethodDescriptor method ) {
        //System.out.println( "Another processing " + method.getActual() );
        method.place( this ).after( FirstInterceptor.class, SecondInterceptor.class );
    }

    public Object invoke( Invocation invocation ) {
        //System.out.println( "Another invocation with " + invocation.arguments().get( 0 ) );
        invocation.setArgument( 0, "A=" + this.value + ":" + invocation.arguments().get( 0 ) );
        return invocation.invoke();
    }
    
}