package test.it.neverworks.model.aop;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public class PropInterceptor implements Interceptor {
    private String name;
    
    public PropInterceptor( String name ) {
        this.name = name;
    }
    
    public Object invoke( Invocation invocation ) {
        System.out.println( "Prop " + name );
        System.out.println( "IN " + invocation.instance() + " " + invocation.arguments().get( 0 ) );
        return invocation.invoke();
    }
}