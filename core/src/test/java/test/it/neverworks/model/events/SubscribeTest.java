package test.it.neverworks.model.events;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith( JUnit4.class )
public class SubscribeTest {

    @Test
    public void subscribeTest() {
        List<String> log = new ArrayList<String>();
        
        Entity e = new Entity().set( "name", "Entity0" ).set( "log", log );
        
        e.place();
 
        assertThat( log.size(), is( 1 ) );
        assertTrue( log.contains( "PLACE HANDLER Entity0" ) );
        log.clear();

        e.remove();
 
        assertThat( log.size(), is( 1 ) );
        assertTrue( log.contains( "REMOVE HANDLER Entity0" ) );
        log.clear();
        
        Sub s = new Sub().set( "name", "Sub0" ).set( "log", log );
        
        s.place();
        
        assertThat( log.size(), is( 2 ) );
        assertTrue( log.contains( "PLACE HANDLER Sub0" ) );
        assertTrue( log.contains( "SUB PLACE HANDLER Sub0 Sub(name=Sub0)" ) );
        log.clear();

        s.remove();
        
        assertThat( log.size(), is( 3 ) );
        assertTrue( log.contains( "REMOVE HANDLER Sub0" ) );
        assertTrue( log.contains( "DUAL HANDLER Sub0" ) );
        assertTrue( log.contains( "END HANDLER Sub0 Sub(name=Sub0)" ) );
        log.clear();

        Sub s1 = new Sub().set( "name", "Sub1" ).set( "log", log );
        
        s1.place();
        
        assertThat( log.size(), is( 2 ) );
        assertTrue( log.contains( "PLACE HANDLER Sub1" ) );
        assertTrue( log.contains( "SUB PLACE HANDLER Sub1 Sub(name=Sub1)" ) );
        log.clear();

        s1.remove();
        
        assertThat( log.size(), is( 3 ) );
        assertTrue( log.contains( "REMOVE HANDLER Sub1" ) );
        assertTrue( log.contains( "DUAL HANDLER Sub1" ) );
        assertTrue( log.contains( "END HANDLER Sub1 Sub(name=Sub1)" ) );
        log.clear();

    }
}