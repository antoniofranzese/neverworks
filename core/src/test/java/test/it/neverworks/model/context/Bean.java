package test.it.neverworks.model.context;

import it.neverworks.model.utils.Identification;

@Identification( "name" )
public class Bean {
    
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }

}