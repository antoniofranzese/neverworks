package test.it.neverworks.model.type;

import it.neverworks.model.converters.AutoConvert;

@AutoConvert
public enum AutoEnum {
    YES,
    NO,
    PERHAPS
}