package test.it.neverworks.model.process;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.description.Process;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;

public class Entity extends BaseModel {

    @Property @Process( TestProcessor.class )
    private String simple;
    
    @Property @Process( TestProcessor.class )
    private String simpleWithSetter;
    
    @Property @Process( TestProcessor2.class )
    private String complex;
    
    @Property @Process( TestProcessor2.class )
    private String complexWithGetterSetter;
    
    @Property @Process({ TestProcessor.class, TestProcessor2.class })
    private String multiple;
    
    @Property @Process({ TestProcessor.class, TestProcessor2.class })
    private String multipleWithGetterSetter;
    
    protected void setSimpleWithSetter( Setter<String> setter ) {
        setter.set( "setSimple:" + setter.value() );
    }

    protected String getComplexWithGetterSetter( Getter<String> getter ) {
        return "getComplex:" + getter.value();
    }

    protected void setComplexWithGetterSetter( Setter<String> setter ) {
        setter.set( "setComplex:" + setter.value() );
    }

    protected String getMultipleWithGetterSetter( Getter<String> getter ) {
        return "getMultiple:" + getter.value();
    }

    protected void setMultipleWithGetterSetter( Setter<String> setter ) {
        setter.set( "setMultiple:" + setter.value() );
    }
    
    public String getMultiple(){
        return this.multiple;
    }
    
    public void setMultiple( String multiple ){
        this.multiple = multiple;
    }
    
    public String getComplex(){
        return this.complex;
    }
    
    public void setComplex( String complex ){
        this.complex = complex;
    }
    
    public String getSimple(){
        return this.simple;
    }
    
    public void setSimple( String simple ){
        this.simple = simple;
    }
    
    public String getSimpleWithSetter(){
        return this.simpleWithSetter;
    }
    
    public void setSimpleWithSetter( String simpleWithSetter ){
        this.simpleWithSetter = simpleWithSetter;
    }
    
    public String getComplexWithGetterSetter(){
        return this.complexWithGetterSetter;
    }
    
    public void setComplexWithGetterSetter( String complexWithGetterSetter ){
        this.complexWithGetterSetter = complexWithGetterSetter;
    }

    public String getMultipleWithGetterSetter(){
        return this.multipleWithGetterSetter;
    }
    
    public void setMultipleWithGetterSetter( String multipleWithGetterSetter ){
        this.multipleWithGetterSetter = multipleWithGetterSetter;
    }
    
}