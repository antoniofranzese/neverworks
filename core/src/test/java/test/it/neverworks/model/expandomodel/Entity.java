package test.it.neverworks.model.expandomodel;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.utils.ExpandoModel;

public class Entity extends BaseModel {
    
    @Property
    private ExpandoModel expando;
    
    @Property
    private CustomExpando custom;
    
    public CustomExpando getCustom(){
        return this.custom;
    }
    
    public void setCustom( CustomExpando custom ){
        this.custom = custom;
    }
    
    public ExpandoModel getExpando(){
        return this.expando;
    }
    
    public void setExpando( ExpandoModel expando ){
        this.expando = expando;
    }
    
}