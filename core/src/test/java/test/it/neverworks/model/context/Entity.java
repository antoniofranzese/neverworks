package test.it.neverworks.model.context;

import it.neverworks.model.Property;
import it.neverworks.model.BaseModel;
import it.neverworks.model.context.Inject;
import it.neverworks.model.description.Required;
import it.neverworks.model.utils.Identification;

@Identification({ "name", "dependency" })
public class Entity extends BaseModel {
    
    @Property
    private String name;

    @Property @Required @Inject( "bean1" )
    private Bean dependency;
    
    @Property @Required @Inject( lazy=true )
    private Bean lazyDependency;
    
    @Property @Inject
    private ByTypeBean byType;
    
    @Property @Inject
    private String string1;
    
    @Property @Inject({ "string2", "it.neverworks.string" })
    private String string2;

    @Property @Inject({ "string3", "it.neverworks.particular.string" })
    private String string3;
    
    public String getString3(){
        return this.string3;
    }
    
    public void setString3( String string3 ){
        this.string3 = string3;
    }
    public String getString2(){
        return this.string2;
    }
    
    public void setString2( String string2 ){
        this.string2 = string2;
    }
    
    public String getString1(){
        return this.string1;
    }
    
    public void setString1( String string1 ){
        this.string1 = string1;
    }
    
    public ByTypeBean getByType(){
        return this.byType;
    }
    
    public void setByType( ByTypeBean byType ){
        this.byType = byType;
    }
    
    public Bean getLazyDependency(){
        return this.lazyDependency;
    }
    
    public void setLazyDependency( Bean lazyDependency ){
        this.lazyDependency = lazyDependency;
    }
    
    public Bean getDependency(){
        return this.dependency;
    }
    
    public void setDependency( Bean dependency ){
        this.dependency = dependency;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}