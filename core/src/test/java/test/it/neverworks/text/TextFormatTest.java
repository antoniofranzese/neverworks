package test.it.neverworks.text;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import java.util.Locale;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.text.TextFormat;
import it.neverworks.text.ParseException;

@RunWith( JUnit4.class )
public class TextFormatTest {

    @Test
    public void testStreamingTextFormat() {
        
        // Parser
        assertThat( TextFormat.format( "{0}-{1}", "zero", "one" ), is( "zero-one" ) );
        assertThat( TextFormat.format( "={0}-{1}", "zero", "one" ), is( "=zero-one" ) );
        assertThat( TextFormat.format( "{0}-{1}=", "zero", "one" ), is( "zero-one=" ) );
        assertThat( TextFormat.format( "={0}-{1}=", "zero", "one" ), is( "=zero-one=" ) );
        assertThat( TextFormat.format( "{0}{1}", "zero", "one" ), is( "zeroone" ) );
        assertThat( TextFormat.format( "{0}{1}{0}", "zero", "one" ), is( "zeroonezero" ) );
        assertThat( TextFormat.format( "{0}'{1}'{1}", "zero", "one" ), is( "zero{1}one" ) );

        assertThat( TextFormat.format( "{0}'{'x'}'{1}", "zero", "one" ), is( "zero{x}one" ) );
        assertThat( TextFormat.format( "{0}'{'x'}'{1}{2}text{3}qu''pi'pu''pa'", "zero", "one", "two", "three" ), is("zero{x}onetwotextthreequ'pipupa" ) );
        assertThat( TextFormat.format( "{0}'{'x'}'''text'''pi'pu'pa'", "zero" ), is("zero{x}'text'pipupa" ) );

        // Autoindex
        assertThat( TextFormat.format( "{}-{}", "zero", "one" ), is( "zero-one" ) );
        assertThat( TextFormat.format( "={}-{}", "zero", "one" ), is( "=zero-one" ) );
        assertThat( TextFormat.format( "{}-{}=", "zero", "one" ), is( "zero-one=" ) );
        assertThat( TextFormat.format( "={}-{}=", "zero", "one" ), is( "=zero-one=" ) );
        assertThat( TextFormat.format( "{}{1}", "zero", "one" ), is( "zeroone" ) );
        assertThat( TextFormat.format( "{}{1}{0}", "zero", "one" ), is( "zeroonezero" ) );
        assertThat( TextFormat.format( "{}'{1}'{1}", "zero", "one" ), is( "zero{1}one" ) );

        // Nulls
        assertThat( TextFormat.format( "{0}-{1}", null, null ), is( "-" ) );
        assertThat( TextFormat.format( "={0}-{1}=", null, null ), is( "=-=" ) );

        // Optionals
        assertThat( TextFormat.format( "{0,optional,N/D}-{1,optional,{N/A}}", null, null ), is( "N/D-{N/A}" ) );
        assertThat( TextFormat.format( "{0,optional,N/D,N/A}", null ), is( "N/D,N/A" ) );

        // Dates
        assertThat( TextFormat.format( Locale.ITALIAN, "{0,date,short}", date( "16/01/1972" ) ), is( "16/01/72" ) );
        assertThat( TextFormat.format( Locale.US, "{0,date,short}", date( "16/01/1972" ) ), is( "1/16/72" ) );
        assertThat( TextFormat.format( "{0,date,dd_MM_yyyy}", date( "16/01/1972" ) ), is( "16_01_1972" ) );
        assertThat( TextFormat.format( "{0,date,dd_MM_yyyy}", null ), is( "" ) );
        assertThat( TextFormat.format( "{0,date,dd_MM_yyyy|N/D}", null ), is( "N/D" ) );
            
        //Numbers
        assertThat( TextFormat.format( Locale.ITALIAN, "{0,number,integer}", 1540 ), is( "1.540" ) );
        assertThat( TextFormat.format( Locale.US, "{0,number,integer}", 1540 ), is( "1,540" ) );
        assertThat( TextFormat.format( "{0,number,#|(#)|no}", 540 ), is( "540" ) );
        assertThat( TextFormat.format( "{0,number,#|(#)|no}", -540 ), is( "(540)" ) );
        assertThat( TextFormat.format( "{0,number,#|(#)|no}", 0 ), is( "no" ) );
        assertThat( TextFormat.format( "{0,number,+=#|-=(#)|0=no}", 540 ), is( "540" ) );
        assertThat( TextFormat.format( "{0,number,+=#|-=(#)|0=no}", -540 ), is( "(540)" ) );
        assertThat( TextFormat.format( "{0,number,+=#|-=(#)|0=no}", 0 ), is( "no" ) );
        assertThat( TextFormat.format( "{0,number,positive=#|negative=(#)|zero=no}", "540" ), is( "540" ) );
        assertThat( TextFormat.format( "{0,number,positive=#|negative=(#)|zero=no}", "-540" ), is( "(540)" ) );
        assertThat( TextFormat.format( "{0,number,positive=#|negative=(#)|zero=no}", "0" ), is( "no" ) );
        assertThat( TextFormat.format( "{0,number,positive=#|negative=(#)|zero=no}", (Number)null ), is( "" ) );
        assertThat( TextFormat.format( "{0,number,positive=#|negative=(#)|zero=no|empty=N/D}", (Number)null ), is( "N/D" ) );
        assertThat( TextFormat.format( "{0,number,positive=#|negative=(#)|zero=no|empty=N/D|error={ERROR!}}", "nonumber" ), is( "{ERROR!}" ) );
            
        //Strings
        assertThat( TextFormat.format( "{0,text,trim}", "  str  " ), is( "str" ) );
        assertThat( TextFormat.format( "{0,text,trim|upper}", "  str  " ), is( "STR" ) );
        assertThat( TextFormat.format( "{0,text,keep=[a-z]}", "abc478def" ), is( "abcdef" ) );
        assertThat( TextFormat.format( "{0,text,keep=[a-z]}", "abc478DEF" ), is( "abc" ) );
        assertThat( TextFormat.format( "{0,text,keep=[a-z]|lower}", "abc478DEF" ), is( "abcdef" ) );
        assertThat( TextFormat.format( "{0,text,waste=[a-z]}", "abc478DEF" ), is( "478DEF" ) );
        assertThat( TextFormat.format( "{0,text,waste=[0-9]|lower}", "abc478DEF" ), is( "abcdef" ) );
        assertThat( TextFormat.format( "{0,text,limit=10}", "abcdefghjklmnopq" ), is( "abcdefghjk" ) );
        assertThat( TextFormat.format( "{0,text,limit=10,...}", "abcdefghjklmnopq" ), is( "abcdefghjk..." ) );
        assertThat( TextFormat.format( "{0,text,empty=N/D}", null ), is( "N/D" ) );
        assertThat( TextFormat.format( "{0,text,empty=N/D}", "" ), is( "N/D" ) );
        assertThat( TextFormat.format( "{0,text,replace=_,-}", "one_two_three" ), is( "one-two-three" ) );
        assertThat( TextFormat.format( "{0,text,replace=_}", "one_two_three" ), is( "onetwothree" ) );
        assertThat( TextFormat.format( "{0,text,replace=\\s,_}", "one two three" ), is( "one_two_three" ) );
            
        //Selects
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", "0" ), is( "poor" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", "1" ), is( "medium" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", "2" ), is( "good" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", "3" ), is( "N/A" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", 0 ), is( "poor" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", 1 ), is( "medium" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", 2 ), is( "good" ) );
        assertThat( TextFormat.format( "{0,select,0=poor|1=medium|2=good|*=N/A}", 3 ), is( "N/A" ) );

        // Booleans
        assertThat( TextFormat.format( "{0,bool,true=yes|false=no}", true ), is( "yes" ) );
        assertThat( TextFormat.format( "{0,bool,true=yes|false=no}", false ), is( "no" ) );
        assertThat( TextFormat.format( "{0,bool,true=yes}", true ), is( "yes" ) );
        assertThat( TextFormat.format( "{0,bool,true=yes}", false ), is( "" ) );
        assertThat( TextFormat.format( "{0,bool,false=no}", true ), is( "" ) );
        assertThat( TextFormat.format( "{0,bool,false=no}", false ), is( "no" ) );
            
        // Repeat
        assertThat( TextFormat.format( "{0,repeat,#}", 5 ), is( "#####" ) );
        assertThat( TextFormat.format( "{0,repeat,!=}", 5 ), is( "!=!=!=!=!=" ) );
            
        // Expression
        BaseModel model = new BaseModel(){
            @Property
            private String name;
            
            @Property
            private String surname;
            
            @Property
            private ExpandoModel attributes;
        }
        .set( "name", "MyName" )
        .set( "surname", "MySurname" )
        .set( "!attributes.age", 10 );
        
        assertThat( TextFormat.format( "{0.name} {0.surname,text,upper}, {0.attributes.age}", model ), is( "MyName MYSURNAME, 10" ) );
        assertThat( TextFormat.format( "{name} {surname,text,upper}, {attributes.age}", model ), is( "MyName MYSURNAME, 10" ) );
        assertThat( TextFormat.format( "{0.name} {1} {0.surname,text,upper}, {0.attributes.age}", model, "and" ), is( "MyName and MYSURNAME, 10" ) );
        assertThat( TextFormat.format( "{name} {1} {surname,text,upper}, {attributes.age}", model, "and" ), is( "MyName and MYSURNAME, 10" ) );
            
        // Unterminated quoting
        assertThat( TextFormat.format( "{0}'{'x'}'''text'''pi", "zero" ), is( "zero{x}'text''pi" ) );
        
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 124L ), is( "124 B" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 12432L ), is( "12.141 KB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 124356L ), is( "121.441 KB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 12422345L ), is( "11.847 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 12446635673L ), is( "11.592 GB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 1242465656733L ), is( "1.13 TB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### *B}", 124246565673356746L ), is( "110.353 PB" ) );
  
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 124L ), is( "0 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 12432L ), is( "0.012 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 124356L ), is( "0.119 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 12422345L ), is( "11.847 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 12446635673L ), is( "11870.037 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 1242465656733L ), is( "1184907.586 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### MB}", 124246565673356746L ), is( "118490758584.363 MB" ) );

        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 124L ), is( "124 B" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 12432L ), is( "12.141 KB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 124356L ), is( "121.441 KB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 12422345L ), is( "11.847 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 12446635673L ), is( "11870.037 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 1242465656733L ), is( "1184907.586 MB" ) );
        assertThat( TextFormat.format( Locale.US, "{0,bytes,0.### >MB}", 124246565673356746L ), is( "118490758584.363 MB" ) );
        
    }

    @Test
    public void testTemplateTextFormat() {
        
        // Parser
        assertThat( new TextFormat( "{0}-{1}" ).apply( "zero", "one" ), is( "zero-one" ) );
        assertThat( new TextFormat( "={0}-{1}" ).apply( "zero", "one" ), is( "=zero-one" ) );
        assertThat( new TextFormat( "{0}-{1}=" ).apply( "zero", "one" ), is( "zero-one=" ) );
        assertThat( new TextFormat( "={0}-{1}=" ).apply( "zero", "one" ), is( "=zero-one=" ) );
        assertThat( new TextFormat( "{0}{1}" ).apply( "zero", "one" ), is( "zeroone" ) );
        assertThat( new TextFormat( "{0}{1}{0}" ).apply( "zero", "one" ), is( "zeroonezero" ) );
        assertThat( new TextFormat( "{0}'{1}'{1}" ).apply( "zero", "one" ), is( "zero{1}one" ) );

        assertThat( new TextFormat( "{0}'{'x'}'{1}" ).apply( "zero", "one" ), is( "zero{x}one" ) );
        assertThat( new TextFormat( "{0}'{'x'}'{1}{2}text{3}qu''pi'pu''pa'" ).apply( "zero", "one", "two", "three" ), is("zero{x}onetwotextthreequ'pipupa" ) );
        assertThat( new TextFormat( "{0}'{'x'}'''text'''pi'pu'pa'" ).apply( "zero" ), is("zero{x}'text'pipupa" ) );

        // Autoindex
        assertThat( new TextFormat( "{}-{}" ).apply( "zero", "one" ), is( "zero-one" ) );
        assertThat( new TextFormat( "={}-{}" ).apply( "zero", "one" ), is( "=zero-one" ) );
        assertThat( new TextFormat( "{}-{}=" ).apply( "zero", "one" ), is( "zero-one=" ) );
        assertThat( new TextFormat( "={}-{}=" ).apply( "zero", "one" ), is( "=zero-one=" ) );
        assertThat( new TextFormat( "{}{1}" ).apply( "zero", "one" ), is( "zeroone" ) );
        assertThat( new TextFormat( "{}{1}{0}" ).apply( "zero", "one" ), is( "zeroonezero" ) );
        assertThat( new TextFormat( "{}'{1}'{1}" ).apply( "zero", "one" ), is( "zero{1}one" ) );

        // Nulls
        assertThat( new TextFormat( "{0}-{1}" ).apply( (Object) null, (Object) null ), is( "-" ) );
        assertThat( new TextFormat( "={0}-{1}=" ).apply( (Object) null, (Object) null ), is( "=-=" ) );

        // Optionals
        assertThat( new TextFormat( "{0,optional,N/D}-{1,optional,{N/A}}" ).apply( (Object) null, (Object) null ), is( "N/D-{N/A}" ) );
        assertThat( new TextFormat( "{0,optional,N/D,N/A}" ).apply( (Object) null ), is( "N/D,N/A" ) );

        // Dates
        assertThat( new TextFormat( "{0,date,short}" ).apply( Locale.ITALIAN, date( "16/01/1972" ) ), is( "16/01/72" ) );
        assertThat( new TextFormat( "{0,date,short}" ).apply( Locale.US, date( "16/01/1972" ) ), is( "1/16/72" ) );
        assertThat( new TextFormat( "{0,date,dd_MM_yyyy}" ).apply( date( "16/01/1972" ) ), is( "16_01_1972" ) );
        assertThat( new TextFormat( "{0,date,dd_MM_yyyy}" ).apply( (Object) null ), is( "" ) );
        assertThat( new TextFormat( "{0,date,dd_MM_yyyy|N/D}" ).apply( (Object) null ), is( "N/D" ) );
            
        //Numbers
        assertThat( new TextFormat( "{0,number,integer}" ).apply( Locale.ITALIAN, 1540 ), is( "1.540" ) );
        assertThat( new TextFormat( "{0,number,integer}" ).apply( Locale.US, 1540 ), is( "1,540" ) );
        assertThat( new TextFormat( "{0,number,#|(#)|no}" ).apply( 540 ), is( "540" ) );
        assertThat( new TextFormat( "{0,number,#|(#)|no}" ).apply( -540 ), is( "(540)" ) );
        assertThat( new TextFormat( "{0,number,#|(#)|no}" ).apply( 0 ), is( "no" ) );
        assertThat( new TextFormat( "{0,number,+=#|-=(#)|0=no}" ).apply( 540 ), is( "540" ) );
        assertThat( new TextFormat( "{0,number,+=#|-=(#)|0=no}" ).apply( -540 ), is( "(540)" ) );
        assertThat( new TextFormat( "{0,number,+=#|-=(#)|0=no}" ).apply( 0 ), is( "no" ) );
        assertThat( new TextFormat( "{0,number,positive=#|negative=(#)|zero=no}" ).apply( "540" ), is( "540" ) );
        assertThat( new TextFormat( "{0,number,positive=#|negative=(#)|zero=no}" ).apply( "-540" ), is( "(540)" ) );
        assertThat( new TextFormat( "{0,number,positive=#|negative=(#)|zero=no}" ).apply( "0" ), is( "no" ) );
        assertThat( new TextFormat( "{0,number,positive=#|negative=(#)|zero=no}" ).apply( (Number)null ), is( "" ) );
        assertThat( new TextFormat( "{0,number,positive=#|negative=(#)|zero=no|empty=N/D}" ).apply( (Number)null ), is( "N/D" ) );
        assertThat( new TextFormat( "{0,number,positive=#|negative=(#)|zero=no|empty=N/D|error={ERROR!}}" ).apply( "nonumber" ), is( "{ERROR!}" ) );
            
        //Strings
        assertThat( new TextFormat( "{0,text,trim}" ).apply( "  str  " ), is( "str" ) );
        assertThat( new TextFormat( "{0,text,trim|upper}" ).apply( "  str  " ), is( "STR" ) );
        assertThat( new TextFormat( "{0,text,keep=[a-z]}" ).apply( "abc478def" ), is( "abcdef" ) );
        assertThat( new TextFormat( "{0,text,keep=[a-z]}" ).apply( "abc478DEF" ), is( "abc" ) );
        assertThat( new TextFormat( "{0,text,keep=[a-z]|lower}" ).apply( "abc478DEF" ), is( "abcdef" ) );
        assertThat( new TextFormat( "{0,text,waste=[a-z]}" ).apply( "abc478DEF" ), is( "478DEF" ) );
        assertThat( new TextFormat( "{0,text,waste=[0-9]|lower}" ).apply( "abc478DEF" ), is( "abcdef" ) );
        assertThat( new TextFormat( "{0,text,limit=10}" ).apply( "abcdefghjklmnopq" ), is( "abcdefghjk" ) );
        assertThat( new TextFormat( "{0,text,limit=10,...}" ).apply( "abcdefghjklmnopq" ), is( "abcdefghjk..." ) );
        assertThat( new TextFormat( "{0,text,empty=N/D}" ).apply( (Object) null ), is( "N/D" ) );
        assertThat( new TextFormat( "{0,text,empty=N/D}" ).apply( "" ), is( "N/D" ) );
        assertThat( new TextFormat( "{0,text,replace=_,-}" ).apply( "one_two_three" ), is( "one-two-three" ) );
        assertThat( new TextFormat( "{0,text,replace=_}" ).apply( "one_two_three" ), is( "onetwothree" ) );
        assertThat( new TextFormat( "{0,text,replace=\\s,_}" ).apply( "one two three" ), is( "one_two_three" ) );
            
        //Selects
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( "0" ), is( "poor" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( "1" ), is( "medium" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( "2" ), is( "good" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( "3" ), is( "N/A" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( 0 ), is( "poor" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( 1 ), is( "medium" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( 2 ), is( "good" ) );
        assertThat( new TextFormat( "{0,select,0=poor|1=medium|2=good|*=N/A}" ).apply( 3 ), is( "N/A" ) );

        // Booleans
        assertThat( new TextFormat( "{0,bool,true=yes|false=no}" ).apply( true ), is( "yes" ) );
        assertThat( new TextFormat( "{0,bool,true=yes|false=no}" ).apply( false ), is( "no" ) );
        assertThat( new TextFormat( "{0,bool,true=yes}" ).apply( true ), is( "yes" ) );
        assertThat( new TextFormat( "{0,bool,true=yes}" ).apply( false ), is( "" ) );
        assertThat( new TextFormat( "{0,bool,false=no}" ).apply( true ), is( "" ) );
        assertThat( new TextFormat( "{0,bool,false=no}" ).apply( false ), is( "no" ) );
            
        // Repeat
        assertThat( new TextFormat( "{0,repeat,#}" ).apply( 5 ), is( "#####" ) );
        assertThat( new TextFormat( "{0,repeat,!=}" ).apply( 5 ), is( "!=!=!=!=!=" ) );

            
        // Expression
        BaseModel model = new BaseModel(){
            @Property
            private String name;
            
            @Property
            private String surname;
            
            @Property
            private ExpandoModel attributes;
        }
        .set( "name", "MyName" )
        .set( "surname", "MySurname" )
        .set( "!attributes.age", 10 );
        
        assertThat( new TextFormat( "{0.name} {0.surname,text,upper}, {0.attributes.age}" ).apply( model ), is( "MyName MYSURNAME, 10" ) );
        assertThat( new TextFormat( "{name} {surname,text,upper}, {attributes.age}" ).apply( model ), is( "MyName MYSURNAME, 10" ) );
        assertThat( new TextFormat( "{0.name} {1} {0.surname,text,upper}, {0.attributes.age}" ).apply( model, "and" ), is( "MyName and MYSURNAME, 10" ) );
        assertThat( new TextFormat( "{name} {1} {surname,text,upper}, {attributes.age}" ).apply( model, "and" ), is( "MyName and MYSURNAME, 10" ) );
            
        // Unterminated quoting
        assertThat( new TextFormat( "{0}'{'x'}'''text'''pi" ).apply( "zero" ), is( "zero{x}'text''pi" ) );
            
    }
    
    @Test( expected = ParseException.class )
    public void unterminatedExpression() {
        TextFormat.format( "{0", "zero" );
    }

    @Test( expected = ParseException.class )
    public void unterminatedNestedExpression() {
        TextFormat.format( "{0,{1}", "zero" );
    }
}