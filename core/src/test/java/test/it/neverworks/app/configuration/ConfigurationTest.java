package test.it.neverworks.app.configuration;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;
import it.neverworks.context.Context;

import it.neverworks.app.Configuration;
import it.neverworks.app.MapConfiguration;
import it.neverworks.app.ConfigurationPlugin;
import it.neverworks.app.PropertiesConfiguration;

@RunWith( JUnit4.class )
public class ConfigurationTest {

    @Test
    public void pluggableConfigurationTest() {
        within( Context.init( "/app/configuration/config-context.xml" ), ctx -> {

            Configuration pluggable = Context.get( "pluggable" );
            assertThat( pluggable.<Object>get( "db.host.domain" ), is( "DbDomain" ) );
            assertThat( Context.<Object>get( "pluggable.db.host.domain" ), is( "DbDomain" ) );
            assertThat( pluggable.<String>get( "property1" ), is( "Property1" ) );
            assertThat( Context.<Object>get( "pluggable.property1" ), is( "Property1" ) );
            assertThat( pluggable.<String>get( "my.own.property2" ), is( "Property2" ) );
            assertThat( Context.<Object>get( "pluggable.my.own.property2" ), is( "Property2" ) );

            assertThat( pluggable.<String>get( "property3" ), is( "Property3" ) );
            assertThat( Context.<Object>get( "pluggable.property3" ), is( "Property3" ) );
            assertThat( pluggable.<String>get( "my.other.property4" ), is( "Property4" ) );
            assertThat( Context.<Object>get( "pluggable.my.other.property4" ), is( "Property4" ) );
                
         });
        
    }

    @Test
    public void propertiesConfigurationTest() {
        
        within( Context.init( "/app/configuration/config-context.xml" ), ctx -> {

                
            Configuration properties = Context.get( "properties" );
            assertThat( Context.<Object>get( "properties.fake.db.host" ), nullValue() );
            assertThat( properties.<Object>get( "fake.db.host" ), nullValue() );
            assertThat( Context.<Object>get( "properties.db.host.name" ), is( "DbHost" ) );
            assertThat( properties.<Object>get( "db.host.name" ), is( "DbHost" ) );
            assertThat( Context.<Object>get( "properties.db.host.domain" ), is( "DbDomain" ) );
            assertThat( properties.<Object>get( "db.host.domain" ), is( "DbDomain" ) );
            assertThat( Context.<Object>get( "properties.db.host.wrong" ), nullValue() );
            assertThat( properties.<Object>get( "db.host.wrong" ), nullValue() );
            assertThat( (String) eval( properties, "property1" ), is( "Property1" ) );
            assertThat( (String) eval( properties, "my.own.property2" ), is( "Property2" ) );
            assertThat( properties.<String>get( "property1" ), is( "Property1" ) );
            assertThat( properties.<String>get( "my.own.property2" ), is( "Property2" ) );

            // Single override
            Configuration overridden1 = Context.get( "overridden1" );
            assertThat( Context.<Object>get( "overridden1.db.host.name" ), is( "DbHost" ) );
            assertThat( overridden1.<Object>get( "db.host.name" ), is( "DbHost" ) );
            assertThat( Context.<Object>get( "overridden1.db.host.domain" ), is( "OverriddenDomain" ) );
            assertThat( overridden1.<Object>get( "db.host.domain" ), is( "OverriddenDomain" ) );
            assertThat( Context.<Object>get( "overridden1.db.user.name" ), is( "DbUser" ) );
            assertThat( overridden1.<Object>get( "db.user.name" ), is( "DbUser" ) );
            assertThat( Context.<Object>get( "overridden1.db.user.password" ), is( "DbPassword" ) );
            assertThat( overridden1.<Object>get( "db.user.password" ), is( "DbPassword" ) );


            // Multiple mixed override
            Configuration overridden2 = Context.get( "overridden2" );
            assertThat( Context.<Object>get( "overridden2.db.host.name" ), is( "DbOverriddenHost" ) );
            assertThat( overridden2.<Object>get( "db.host.name" ), is( "DbOverriddenHost" ) );
            assertThat( Context.<Object>get( "overridden2.db.host.domain" ), is( "OverriddenDomain" ) );
            assertThat( overridden2.<Object>get( "db.host.domain" ), is( "OverriddenDomain" ) );
            assertThat( Context.<Object>get( "overridden2.db.user.name" ), is( "DbOverriddenUser" ) );
            assertThat( overridden2.<Object>get( "db.user.name" ), is( "DbOverriddenUser" ) );
            assertThat( Context.<Object>get( "overridden2.db.user.password" ), is( "DbPassword" ) );
            assertThat( overridden2.<Object>get( "db.user.password" ), is( "DbPassword" ) );

            
        });
        
    }

    @Test
    public void mapConfigurationTest() {
        MapConfiguration config = new MapConfiguration();

        config.setContent( dict(
            pair( "simple1", (Object) "value1" )
            ,pair( "map1", dict( 
                pair( "key1", dict( 
                    pair( "sub1", "subValue1" )
                ))
            ))
            ,pair( "map2.key2.sub2", "subValue2" )
        ));

        assertThat( (String) eval( config, "simple1" ), is( "value1" ) );
        assertThat( eval( config, "map1" ), instanceOf( Map.class ) );
        assertThat( eval( config, "map1.key1" ), instanceOf( Map.class ) );
        assertThat( (String) eval( config, "map1.key1.sub1" ), is( "subValue1" ) );

        assertThat( eval( config, "map2" ), instanceOf( Map.class ) );
        assertThat( eval( config, "map2.key2" ), instanceOf( Map.class ) );
        assertThat( (String) eval( config, "map2.key2.sub2" ), is( "subValue2" ) );
        
        
        within( Context.init( "/app/configuration/config-context.xml" ), ctx -> {

            Configuration map = Context.get( "map" );

            assertThat( (String) eval( map, "web.path" ), is( "index.html" ) );
            assertThat( (Integer) eval( map, "internal.selected" ), is( 5 ) );
            assertThat( eval( map, "map1" ), instanceOf( Map.class ) );
            assertThat( eval( map, "map1.key1" ), instanceOf( Map.class ) );
            assertThat( (String) eval( map, "map1.key1.sub1" ), is( "subValue1" ) );
            
            
        });
        
    }
}