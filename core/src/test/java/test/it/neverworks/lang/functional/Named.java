package test.it.neverworks.lang.functional;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;

class Named extends BaseModel {
    
    @Property
    private String name;
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}

