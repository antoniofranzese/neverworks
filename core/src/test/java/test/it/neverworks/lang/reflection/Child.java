package test.it.neverworks.lang.reflection;

public class Child extends Entity {
    
    public void make( Integer value ) {
        
    }
    
    public void make( String value1, Boolean value2 ) {
        
    }
    
    public void make(){
        
    }
    
    protected void print( String value ) {
        
    }
}