package test.it.neverworks.lang.arguments;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

import it.neverworks.lang.Arguments;

public class Entity extends BaseModel {
        
    @Property
    private String name;
    
    @Property @AutoConvert
    private int age;
    
    
    public Entity() {
        
    }
    
    public Entity( Arguments arguments ) {
        for( String name: arguments.keySet() ) {
            this.set( name, arguments.get( name ) );
        }
    }
    
    public int getAge(){
        return this.age;
    }
    
    public void setAge( int age ){
        this.age = age;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}