package test.it.neverworks.lang.arguments;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static it.neverworks.language.*;
import it.neverworks.lang.Arguments;

@RunWith( JUnit4.class )
public class ArgumentsTest {

    @Test
    public void argumentsTest() {
        Entity e = new Entity( arg( "name", "MyName").arg( "age", 10 ) );
        
        assertThat( e.getName(), is( "MyName" ) );
        assertThat( e.getAge(), is( 10 ) );
        
    }


    @Test
    public void argumentsParserTest() {
        String props = "name=MyName, age=10";
        
        assertTrue( Arguments.isParsable( props ) );
        
        Arguments parsed = Arguments.parse( props );

        assertThat( parsed.<String>get( "name" ), is( "MyName" ) );
        assertThat( parsed.<String>get( "age" ), is( "10" ) );
        
        Entity e = new Entity( parsed );

        assertThat( e.getName(), is( "MyName" ) );
        assertThat( e.getAge(), is( 10 ) );
        
        String json = "{ name: \"MyName\", address: 'MyAddress', age: 10, db: { host: 'dbhost', port: 100 } }";
        assertTrue( Arguments.isParsable( json ) );
        
        Arguments jparsed = Arguments.parse( json );
        assertThat( jparsed.<String>get( "name" ), is( "MyName" ) );
        assertThat( jparsed.<String>get( "address" ), is( "MyAddress" ) );
        assertThat( jparsed.<Integer>get( "age" ), is( 10 ) );
        
        assertThat( (String) eval( jparsed, "db.host" ), is( "dbhost" ) );
        assertThat( (Integer) eval( jparsed, "db.port" ), is( 100 ) );
        
        assertFalse( Arguments.isParsable( "a string" ) );
        assertFalse( Arguments.isParsable( "=a string" ) );
        
        
        
    }

}