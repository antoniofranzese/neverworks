package test.it.neverworks.lang.reflection;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import java.util.List;

import it.neverworks.lang.Reflection;

@RunWith( JUnit4.class )
public class ReflectionTest {

    @Test
    public void findMethodsTest() {
        List entity = list().cat( Reflection.findMethods( Entity.class ) );

        assertTrue( entity.size() == 6 );
        assertTrue( entity.contains( Reflection.findMethod( Entity.class, "make" ) ) );
        assertTrue( entity.contains( Reflection.findMethod( Entity.class, "make", String.class ) ) );
        assertTrue( entity.contains( Reflection.findMethod( Entity.class, "make", Integer.class ) ) );
        assertTrue( entity.contains( Reflection.findMethod( Entity.class, "make", String.class, Integer.class ) ) );
        assertTrue( entity.contains( Reflection.findMethod( Entity.class, "print" ) ) );
        assertTrue( entity.contains( Reflection.findMethod( Entity.class, "print", String.class ) ) );

        List child = list().cat( Reflection.findMethods( Child.class ) );

        assertTrue( child.size() == 7 );
        assertTrue( child.contains( Reflection.findMethod( Child.class, "make" ) ) );
        assertFalse( child.contains( Reflection.findMethod( Entity.class, "make" ) ) );
        assertTrue( child.contains( Reflection.findMethod( Entity.class, "make", String.class ) ) );
        assertTrue( child.contains( Reflection.findMethod( Child.class, "make", Integer.class ) ) );
        assertFalse( child.contains( Reflection.findMethod( Entity.class, "make", Integer.class ) ) );
        assertTrue( child.contains( Reflection.findMethod( Entity.class, "make", String.class, Integer.class ) ) );
        assertTrue( child.contains( Reflection.findMethod( Child.class, "make", String.class, Boolean.class ) ) );
        assertTrue( child.contains( Reflection.findMethod( Entity.class, "print" ) ) );
        assertTrue( child.contains( Reflection.findMethod( Child.class, "print", String.class ) ) );
        assertFalse( child.contains( Reflection.findMethod( Entity.class, "print", String.class ) ) );

    }
}