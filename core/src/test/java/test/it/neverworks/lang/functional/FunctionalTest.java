package test.it.neverworks.lang.functional;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Functional;
import it.neverworks.lang.FunctionalIterable;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Inspector;
import java.util.Arrays;

import it.neverworks.model.expressions.ObjectQuery;

@RunWith( JUnit4.class )
public class FunctionalTest {
    
    @Test
    public void functionalTest() {
        
        // MAP
        
        List<Named> nameds = testdata().map( new Mapper<Entity, Named>(){
            public Named map( Entity item ) {
                return new Named().set( "name", item.get( "name" ) );
            }
        }).list();
        
        assertThat( nameds.size(), is( 4 ) );
        assertThat( nameds.get( 0 ).getName(), is( "Name1" ) );
        assertThat( nameds.get( 1 ).getName(), is( "Name2" ) );
        assertThat( nameds.get( 2 ).getName(), is( "Name3" ) );
        assertThat( nameds.get( 3 ).getName(), is( "Name4" ) );

        // FILTER

        List<Entity> tens = testdata().filter( new Filter<Entity>(){
            public boolean filter( Entity item ) {
                return item.getAge() % 10 == 0;
            }
        }).list();
        
        assertThat( tens.size(), is( 2 ) );
        assertThat( tens.get( 0 ).getName(), is( "Name1" ) );
        assertThat( tens.get( 1 ).getName(), is( "Name3" ) );

        // INSPECT

        Functional<Entity> testdata = testdata();
        List<Entity> original = testdata.list();

        assertThat( original.get( 0 ).getName(), is( "Name1" ) );
        assertThat( original.get( 1 ).getName(), is( "Name2" ) );
        assertThat( original.get( 2 ).getName(), is( "Name3" ) );
        assertThat( original.get( 3 ).getName(), is( "Name4" ) );

        List<Entity> inspected = testdata.inspect( new Inspector<Entity>(){
            public void inspect( Entity item ) {
                item.setName( item.getName() + ":" + item.getAge() );
            }
        }).list();

        assertThat( original.get( 0 ).getName(), is( "Name1:10" ) );
        assertThat( original.get( 1 ).getName(), is( "Name2:15" ) );
        assertThat( original.get( 2 ).getName(), is( "Name3:20" ) );
        assertThat( original.get( 3 ).getName(), is( "Name4:25" ) );

        assertThat( inspected.get( 0 ).getName(), is( "Name1:10" ) );
        assertThat( inspected.get( 1 ).getName(), is( "Name2:15" ) );
        assertThat( inspected.get( 2 ).getName(), is( "Name3:20" ) );
        assertThat( inspected.get( 3 ).getName(), is( "Name4:25" ) );

        assertTrue( inspected.get( 0 ) == original.get( 0 ) );
        assertTrue( inspected.get( 1 ) == original.get( 1 ) );
        assertTrue( inspected.get( 2 ) == original.get( 2 ) );
        assertTrue( inspected.get( 3 ) == original.get( 3 ) );

        //REDUCE

        int totalAge = testdata().reduce( new Inspector<Entity>(){

            public int total = 0;

            public void inspect( Entity item ) {
                total += item.getAge();
            }

        }).total;
        
        assertThat( totalAge, is( 70 ) );

        Map<Integer,Integer> ages = testdata().reduce( new Inspector<Entity>(){

            public Map<Integer,Integer> result = dict(
                pair( 10, 0 )
                ,pair( 20, 0 )
                ,pair( 30, 0 )
            );
            
            public void inspect( Entity item ) {
                if( item.getAge() <= 10 ) {
                    result.put( 10, result.get( 10 ) + 1 );
                } else if( item.getAge() <= 20 ) {
                    result.put( 20, result.get( 20 ) + 1 );
                } else {
                    result.put( 30, result.get( 30 ) + 1 );
                }
            }

        }).result;

        assertThat( ages.get( 10 ), is( 1 ) );
        assertThat( ages.get( 20 ), is( 2 ) );
        assertThat( ages.get( 30 ), is( 1 ) );

        // ANY
        
        assertTrue( testdata().any( new Filter<Entity>(){
            public boolean filter( Entity item ) {
                return item.getAge() > 20;
            }
        }));

        assertFalse( testdata().any( new Filter<Entity>(){
            public boolean filter( Entity item ) {
                return item.getAge() > 30;
            }
        }));

        assertTrue( testdata().any() );
        
        // ALL
        
        assertTrue( testdata().all( new Filter<Entity>(){
            public boolean filter( Entity item ) {
                return item.getAge() > 0;
            }
        }));

        assertFalse( testdata().all( new Filter<Entity>(){
            public boolean filter( Entity item ) {
                return item.getAge() > 20;
            }
        }));
        
        
        // FunctionalIterable reference
        
        List<Entity> list = testdata().list();
        Functional<Entity> functional = new FunctionalIterable( list );
        
        assertThat( list.size(), is( 4 ) );
        assertThat( functional.list().size(), is( 4 ) );
        
        list.add( new Entity() );

        assertThat( list.size(), is( 5 ) );
        assertThat( functional.list().size(), is( 5 ) );
        
    }

    private Functional<Entity> testdata() {
        List list = new ArrayList<Entity>();

        list.add( 
            new Entity()
                .set( "name", "Name1" )
                .set( "age", 10 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent1" )
                )
        );
        
        list.add( 
            new Entity()
                .set( "name", "Name2" )
                .set( "age", 15 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent1" )
                )
        );

        list.add( 
            new Entity()
                .set( "name", "Name3" )
                .set( "age", 20 )     
                .set( "parent", new Entity() 
                    .set( "name", "Parent3" )
                )
        );

        list.add( 
            new Entity()
                .set( "name", "Name4" )
                .set( "age", 25 )     
        );

        return new FunctionalIterable( list );
    }
}