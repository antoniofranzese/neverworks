package test.it.neverworks.lang.properties;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.lang.Properties;

@RunWith( JUnit4.class )
public class PropertiesTest {

    @Test
    public void anonymousInnerClassPropertiesTest() {
        Object obj = new Object(){
            private String name = "Obj1"; 
            
            public String getName(){
                return this.name;
            }
            
            public void setName( String name ){
                this.name = name;
            }

        };
        
        assertThat( (String) Properties.get( obj, "name" ), is( "Obj1" ) );

        Properties.set( obj, "name", "Renamed" );

        assertThat( (String) Properties.get( obj, "name" ), is( "Renamed" ) );

    }

    public static class TestClass {
            private static String staticString = "STATIC";

            public String getStatic() {
                return staticString;
            }

            public void setStatic( String str ) {
                staticString = str;
            }

            private String name = "Obj1"; 
            
            public String getName(){
                return this.name;
            }
            
            public void setName( String name ){
                this.name = name;
            }

            public String publicString = "PUBLIC";
            protected String protectedString = "PROTECTED";

    }

    @Test
    public void propertiesTest() {

        TestClass obj = new TestClass();

        assertThat( (String) Properties.get( obj, "name" ), is( "Obj1" ) );
        Properties.set( obj, "name", "Renamed" );
        assertThat( (String) Properties.get( obj, "name" ), is( "Renamed" ) );

        assertThat( (String) Properties.get( obj, "publicString" ), is( "PUBLIC" ) );
        Properties.set( obj, "publicString", "NEWPUBLIC" );
        assertThat( (String) Properties.get( obj, "publicString" ), is( "NEWPUBLIC" ) );

        //System.out.println( Properties.<Object>get( obj, "protectedString" ) );
    }

}