package test.it.neverworks.lang.reflection;

public class Entity {
    
    public void make() {
        
    }
    
    public void make( String value ) {
        
    }
    
    public void make( Integer value ) {
        
    }
    
    public void make( String value1, Integer value2 ) {
        
    }
    
    protected void print() {
        
    }

    protected void print( String value ) {
        
    }
}