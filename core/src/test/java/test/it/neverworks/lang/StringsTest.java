package test.it.neverworks.lang;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.lang.Strings;

@RunWith( JUnit4.class )
public class StringsTest {

    @Test
    public void stringConventionsTest() {
        assertThat( Strings.hyphen2camel( "una-piccola-stringa" ), is( "unaPiccolaStringa" ) );
        assertThat( Strings.camel2hyphen( "maCheVuoi" ), is( "ma-che-vuoi" ) );
        assertThat( Strings.camel2hyphen( "maCheBBuo" ), is( "ma-che-bbuo" ) );
        assertThat( Strings.camel2hyphen( "MaCheBBuo" ), is( "ma-che-bbuo" ) );

    }
}