package test.it.neverworks.lang;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.lang.Calculator;
import java.math.BigDecimal;

@RunWith( JUnit4.class )
public class CalculatorTest {

    @Test
    public void calculatorTest() {
        assertThat( new Calculator().push( 10 ).add( "5" ).result(), is( new BigDecimal( 15 ) ) );
        assertThat( new Calculator().push( 10 ).div( 3 ).result(), is( new BigDecimal( "3.333333333333333333333333333333333" ) ) );
        assertThat( new Calculator( 8 ).push( 7.53 ).mul( 2.5436 ).result(), is( new BigDecimal( "19.15330800" ) ) );
        assertThat( new Calculator( 8 ).push( 7.53 ).mul( 2.5436 ).result(), not( is( new BigDecimal( "19.1533080" ) ) ) );
        assertThat( new Calculator( 8 ).push( 7.53 ).mul( 2.5436 ).to( Float.class ), is( 19.153309F ) );
    }

}