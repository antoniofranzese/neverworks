package test.it.neverworks.cache;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import static it.neverworks.test.TestUtils.*;
import static it.neverworks.language.*;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import it.neverworks.cache.Cache;
import it.neverworks.cache.LRUCache;

@RunWith( JUnit4.class )
public class CacheTest {

    @Test
    public void LRUCacheTest() {
        Cache cache = new LRUCache().set( "max", 3 );
        
        Object o1 = new Object();
        Object o2 = new Object();
        Object o3 = new Object();
        
        cache.put( "A", o1 );
        cache.put( "B", o2 );
        cache.put( "C", o3 );
        
        assertThat( cache.size(), is( 3 ) );
        assertTrue( cache.contains( "A" ) );
        assertTrue( cache.contains( "B" ) );
        assertTrue( cache.contains( "C" ) );

        assertTrue( cache.get( "A" ) == o1 );
        assertTrue( cache.get( "B" ) == o2 );
        assertTrue( cache.get( "C" ) == o3 );

        Object o4 = new Object();
        cache.put( "D", o4 );

        assertThat( cache.size(), is( 3 ) );
        assertFalse( cache.contains( "A" ) );
        assertTrue( cache.contains( "B" ) );
        assertTrue( cache.contains( "C" ) );
        assertTrue( cache.contains( "D" ) );

    }
}