package it.neverworks.app;

import java.util.Properties;

import org.springframework.core.Ordered;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import it.neverworks.context.spring.ConfigurationPlaceholderConfigurer;

public class PropertiesConfigurer extends PropertiesConfiguration implements BeanFactoryAware, BeanNameAware, BeanFactoryPostProcessor, Ordered {
    
    ConfigurationPlaceholderConfigurer postProcessor;
    
    public PropertiesConfigurer() {
        super();
        this.postProcessor = new ConfigurationPlaceholderConfigurer( this );
    }
    
    @Override
    public void postProcessBeanFactory( ConfigurableListableBeanFactory beanFactory ) {
        this.postProcessor.postProcessBeanFactory( beanFactory );
    }

    @Override
    public void setBeanFactory( BeanFactory beanFactory ) {
        this.postProcessor.setBeanFactory( beanFactory );
    } 
    
    @Override
    public void setBeanName( String name ) {
        this.postProcessor.setBeanName( name );
    } 
    
    public int getOrder(){
        return this.postProcessor.getOrder();
    }
    
    public void setOrder( int order ){
        this.postProcessor.setOrder( order );
    }
}