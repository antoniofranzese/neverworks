package it.neverworks.app;

import java.util.List;
import java.util.ArrayList;

import org.springframework.core.io.Resource;

import static it.neverworks.language.*;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.expressions.Expression;

public class PropertiesConfiguration extends AbstractConfiguration {
    
    protected List<Arguments> contents = new ArrayList<Arguments>();
    
    public Object evaluateExpression( Expression expression ) {
        for( Arguments content: this.contents ) {
            if( content.has( expression.text() ) ) {
                return content.get( expression.text() );
            } else if( expression.size() > 1 ){
                expression.modifiers().withNulls();
                for( int i = expression.size() - 1; i > 0; i-- ) {
                    String key = expression.sub( 0, i ).text();
                    if( content.has( key ) ) {
                        Object root = content.get( key );
                        if( root != null ) {
                            Object value = expression.sub( i ).evaluate( root );
                            if( value != null ) {
                                return value;
                            }
                        } 
                    }
                }
            }
        }
        return null;
    }


    public Object retrieveItem( Object key ) {
        String skey = str( key );
        for( Arguments content: this.contents ) {
            if( content.containsKey( skey ) ) {
                return content.get( key );
            }
        }
        return null;
    }
    
    public boolean containsItem( Object key ) {
        String skey = str( key );
        for( Arguments content: contents ) {
            if( content.containsKey( skey ) ) {
                return true;
            }
        }
        return false;
    }

    public void setLocation( Resource location ) {
        setResource( location );
    }

    public void setLocations( Resource[] locations ) {
        setResources( locations );
    }

    public void setResource( Resource resource ){
        setResources( new Resource[]{ resource } );
    }

    public void setResources( Resource[] resources ){
        for( Resource resource: resources ) {
            if( resource.exists() ) {
                try {
                    Arguments props = Arguments.load( resource.getInputStream() );
                    contents.add( 0, props );
                } catch( Exception ex ) {
                    throw Errors.wrap( ex );
                }
            }
        }
    }
    
}