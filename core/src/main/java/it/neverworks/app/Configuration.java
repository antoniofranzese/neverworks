package it.neverworks.app;

import it.neverworks.model.Value;

public interface Configuration {
    
    public boolean has( String key );

    public <T> T get( String key );
    
    default <T> T get( String key, T defaultValue ) {
        T value = get( key );
        return value != null ? value : defaultValue;
    }

    default Value value( String key ) {
        return Value.of( get( key ) );
    }
    
    
}