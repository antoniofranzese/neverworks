package it.neverworks.app;

public interface ConfigurationPlugin {
    public boolean claims( String attribute );
    public Object retrieve( String attribute );
}