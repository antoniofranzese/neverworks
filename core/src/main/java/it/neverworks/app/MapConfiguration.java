package it.neverworks.app;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import it.neverworks.model.Value;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Expression;
import it.neverworks.model.expressions.Modifiers;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Ensure;

public class MapConfiguration extends AbstractConfiguration {
    
    protected Map<String, Object> content = new HashMap<String, Object>();

    public Object retrieveItem( Object key ) {
        return content.get( str( key ) );
    }
    
    public boolean containsItem( Object key ) {
        return content.containsKey( str( key ) );
    }
    
    public Map<String, Object> getContent(){
        return this.content;
    }
    
    public void setContent( Map<String, Object> content ){
        this.content = new ExpandingMap();
        for( String key: content.keySet() ) {
            ExpressionEvaluator.valorize( this.content, key, content.get( key ), Modifiers.WITH_NULLS );
        }
    }
    
    private class ExpandingMap extends HashMap<String,Object> implements Ensure {
        
        public Object ensureExpression( Expression expression ) {
            String text = expression.text();
            if( ! this.containsKey( text ) ) {
                this.put( text, new ExpandingMap() );
            }
            return this.get( text );
        }

    }
    
}