package it.neverworks.app;

import it.neverworks.context.ApplicationEvent;

public class ApplicationShutdownEvent extends ApplicationEvent {
    
    public ApplicationShutdownEvent( Object source ) {
        super( source );
    }
}