package it.neverworks.app;

import it.neverworks.context.ApplicationEvent;

public class ApplicationStartupEvent extends ApplicationEvent {
    
    public ApplicationStartupEvent( Object source ) {
        super( source );
    }
    
}