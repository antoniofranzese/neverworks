package it.neverworks.app;

import java.util.Map;
import java.util.HashMap;
import it.neverworks.lang.Strings;
import it.neverworks.model.Value;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Evaluate;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Expression;

public abstract class AbstractConfiguration implements Configuration, Retrieve, Inspect, Evaluate, ConfigurationPlugin {
    
    public Object evaluateExpression( Expression expression ) {
        if( containsItem( expression.text() ) ) {
            return retrieveItem( expression.text() );
        } else if( expression.size() > 1 ){
            expression.modifiers().withNulls();
            for( int i = expression.size() - 1; i > 0; i-- ) {
                String key = expression.sub( 0, i ).text();
                if( containsItem( key ) ) {
                    Object root = retrieveItem( key );
                    return root == null ? null : expression.sub( i ).evaluate( root );
                }
            }
            return null;
        } else {
            return null;
        }
    }
    
    public boolean has( String key ) {
        return containsItem( key );
    }

    public <T> T get( String key ) {
        return (T) ( ExpressionEvaluator.isExpression( key ) ? ExpressionEvaluator.evaluate( this, key ) : this.retrieveItem( key ) );
    }

    public boolean claims( String attribute ) {
        return has( attribute );
    }
    
    public Object retrieve( String attribute ) {
        return get( attribute );
    }

}