package it.neverworks.app;

import static it.neverworks.language.*;

import java.util.List;
import java.util.Iterator;

import it.neverworks.model.Value;
import it.neverworks.cache.Cache;
import it.neverworks.cache.SimpleLRUCache;
import it.neverworks.model.features.Evaluate;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Expression;

public class PluggableConfiguration implements Configuration, Evaluate {
    
    private List<ConfigurationPlugin> plugins;

    public Object evaluateExpression( Expression expression ) {
         for( ConfigurationPlugin plugin: this.plugins ) {
            if( plugin.claims( expression.text() ) ) {
                return plugin.retrieve( expression.text() );
            } else if( expression.size() > 1 ){
                expression.modifiers().withNulls();
                for( int i = expression.size() - 1; i > 0; i-- ) {
                    String key = expression.sub( 0, i ).text();
                    if( plugin.claims( key ) ) {
                        Object root = plugin.retrieve( key );
                        if( root != null ) {
                            Object value = expression.sub( i ).evaluate( root );
                            if( value != null ) {
                                return value;
                            }
                        } 
                    }
                }
            }
        }
        return null;
    }

    public Object retrieveItem( Object key ) {
        return get( str( key ) );
    }
    
    public boolean containsItem( Object key ) {
        return has( str( key ) );
    }

    public <T> T get( String key ) {
        return ExpressionEvaluator.evaluate( this, key );
    }
    
    public <T> T get( String key, T defaultValue ) {
        Object value = get( key );
        return value != null ? (T) value : defaultValue;
    }
    
    public Value value( String key ) {
        return Value.of( get( key ) );
    }
    
    public boolean has( String key ) {
        return ExpressionEvaluator.knows( this, key );
    }
    
    public void setPlugins( List<ConfigurationPlugin> plugins ){
        this.plugins = plugins;
        for( ConfigurationPlugin plugin: plugins ) {
            if( plugin instanceof ConfigurationAware ) {
                ((ConfigurationAware) plugin).setConfiguration( this );
            }
        }
    }
    
    
}
