package it.neverworks.app;

public interface ConfigurationAware {
    void setConfiguration( Configuration configuration );
}