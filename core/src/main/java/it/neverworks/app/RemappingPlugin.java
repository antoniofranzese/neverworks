package it.neverworks.app;

import java.util.Map;

public class RemappingPlugin implements ConfigurationPlugin, ConfigurationAware {
    
    private Map<String,String> mappings;
    private Configuration configuration;

    public boolean claims( String attribute ) {
        return configuration != null && mappings != null && mappings.containsKey( attribute );
    }

    public Object retrieve( String attribute ) {
        return claims( attribute ) ? configuration.get( mappings.get( attribute ) ) : null;
    }

    public void setConfiguration( Configuration configuration ) {
        this.configuration = configuration;
    }
    
    public void setMappings( Map<String,String> mappings ){
        this.mappings = mappings;
    }
}