package it.neverworks.io;

import java.io.File;
import java.io.InputStream;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.crypto.Cipher;
import it.neverworks.model.BaseModel;

public abstract class AbstractDispatchingProvider extends BaseModel implements FileProvider, NameSpaceAware {
    
    protected String nameSpace;
    protected Cipher cipher = new Cipher(); 
    protected boolean crypt = true; 
    protected final static String CRYPT_PREFIX = "~";
    
    public String getNameSpace(){
        return this.nameSpace;
    }
    
    public FileBadge reference( Arguments arguments ) {
        return wrap( new FileBadge( arguments ) );
    }

    public Arguments dereference( FileBadge badge ) {
        return unwrap( badge ).getArguments();
    }

    protected FileBadge tag( FileBadge badge ) {
        if( Strings.hasText( this.nameSpace ) ) {
            return new FileBadge( this.nameSpace + badge.getToken() );
        } else {
            return badge;
        }
    }
    
    protected FileBadge untag( FileBadge badge ) {
        if( Strings.hasText( this.nameSpace ) && badge.getToken().startsWith( this.nameSpace ) ) {
            return new FileBadge( badge.getToken().substring( getNameSpace().length() ) );
        } else {
            return badge;
        }
    }

    protected FileBadge encrypt( FileBadge badge ) {
        if( crypt && cipher != null ) {
            return new FileBadge( CRYPT_PREFIX + cipher.encrypt( badge.getToken() ) );
        } else { 
            return badge;
        }
    }
    
    protected FileBadge decrypt( FileBadge badge ) {
        if( crypt && cipher != null && badge.getToken().startsWith( CRYPT_PREFIX ) ) {
            return new FileBadge( cipher.decrypt( badge.getToken().substring( CRYPT_PREFIX.length() ) ) );
        } else {
            return badge;
        }
    }
    
    protected FileBadge wrap( FileBadge badge ) {
        return tag( encrypt( badge ) ); 
    }

    protected FileBadge unwrap( FileBadge badge ) {
        return decrypt( untag( badge ) ); 
    }
    
	public File getFile( FileBadge badge ) {
        return getInfo( badge ).get( "file" );
    }
    
	public InputStream getStream( FileBadge badge ) {
        return getInfo( badge ).get( "stream" );
    }
    
	public FileInfo getInfo( FileBadge badge ) {
        throw new UnsupportedOperationException();
    }
    
    public void storeNameSpace( String nameSpace ) {
        this.nameSpace = nameSpace;
    }
    
    public Cipher getCipher(){
        return this.cipher;
    }
    
    public void setCipher( Cipher cipher ){
        this.cipher = cipher;
    }
    
    public boolean getCrypt(){
        return this.crypt;
    }
    
    public void setCrypt( boolean crypt ){
        this.crypt = crypt;
    }
    
}