package it.neverworks.io;

import java.io.InputStream;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

public class FileProviderStreamResolver implements InputStreamResolver {
    
    protected FileProvider provider;
    
    public FileProviderStreamResolver() {
        super();
    }

    public FileProviderStreamResolver( FileProvider provider ) {
        this();
        this.provider = provider;
    }
    
    public InputStream resolveInputStream( Object source ) {
        return resolveFileInfo( source ).getStream();
    }

    protected FileInfo resolveFileInfo( Object source ) {
        FileBadge badge;
        if( source instanceof Arguments ) {
            badge = new FileBadge( (Arguments) source );
        } else {
            badge = new FileBadge( Strings.valueOf( source ) );
        }
        FileInfo info = this.provider.getInfo( badge );
        return info;
    }

    public FileProvider getProvider(){
        return this.provider;
    }
    
    public void setProvider( FileProvider provider ){
        this.provider = provider;
    }

}