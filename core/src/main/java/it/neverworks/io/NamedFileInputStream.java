package it.neverworks.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import it.neverworks.lang.Strings;
import it.neverworks.model.utils.ToStringBuilder;

public class NamedFileInputStream extends FileInputStream implements NamedStream {

    protected File file;
    protected String name;

    public NamedFileInputStream( String file ) throws FileNotFoundException {
        this( new File( file ) );
    }

    public NamedFileInputStream( File file ) throws FileNotFoundException {
        this( file, file.getName() );
    }

    public NamedFileInputStream( File file, String name ) throws FileNotFoundException {
        super( file );
        this.file = file;
        this.name = Strings.hasText( name ) ? name : file.getName();
    }

    public String getName() {
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "name", this.name )
            .toString();
    }

}