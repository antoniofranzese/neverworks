package it.neverworks.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import it.neverworks.lang.Errors;
import it.neverworks.context.Context;

public class Files {
    
    private final static Object ME = new Object();
    
    private static FileRepository temporaryFileRepository = null;
    
    public static void copy( File sourceFile, File destFile ) {
        try {
            if( !destFile.exists() ) {
                destFile.createNewFile();
            }

            FileChannel source = null;
            FileChannel destination = null;

            try {
                source = new FileInputStream( sourceFile ).getChannel();
                destination = new FileOutputStream( destFile ).getChannel();
                destination.transferFrom( source, 0, source.size() );
            } finally {
                if( source != null ) {
                    source.close();
                }
                if( destination != null ) {
                    destination.close();
                }
            }
        
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static FileRepository temporary() {
        if( temporaryFileRepository == null ) {
            synchronized( ME ) {
                if( temporaryFileRepository == null ) {
                    if( Context.ready() && Context.contains( "it.neverworks.io.FileRepository" ) ) {
                        temporaryFileRepository = Context.get( "it.neverworks.io.FileRepository" );
                    } else if( Context.ready() && Context.contains( "it.neverworks.ui.FileRepository" ) ) {
                        temporaryFileRepository = Context.get( "it.neverworks.ui.FileRepository" );
                    } else {
                        temporaryFileRepository = new TemporaryFileRepository();
                    }
                }
            }
        }
        return temporaryFileRepository;
    }
    
    public void setTemporaryFileRepository( FileRepository aTemporaryFileRepository ) {
        if( temporaryFileRepository != null ) {
            synchronized( ME ) {
                if( temporaryFileRepository != null ) {
                    temporaryFileRepository.shutdown();
                    temporaryFileRepository = null;
                }
            }
        }
        temporaryFileRepository = aTemporaryFileRepository;
    }
    
}