package it.neverworks.io;

import java.io.Reader;
import java.io.InputStream;
import java.io.IOException;

public class ReaderInputStream extends InputStream {
    
    private Reader reader;
    
    public ReaderInputStream( Reader reader ) {
        super();
        this.reader = reader;
    }
    
    public int read() throws IOException {
        return reader.read();
    }
    
}