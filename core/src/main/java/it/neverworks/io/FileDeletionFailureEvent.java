package it.neverworks.io;

import java.io.File;

public class FileDeletionFailureEvent extends AbstractDeletionEvent {

    private Exception exception;
    
    public Exception getException(){
        return this.exception;
    }
    
    public FileDeletionFailureEvent( Object source, File file, Exception exception ) {
        super( source, file );
        this.exception = exception;
    }
    
}