package it.neverworks.io;

import java.io.InputStream;

public interface TypedInputStreamProvider extends InputStreamProvider, TypedProvider {

}