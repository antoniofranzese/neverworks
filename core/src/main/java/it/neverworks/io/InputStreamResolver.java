package it.neverworks.io;

import java.io.InputStream;

public interface InputStreamResolver {
    InputStream resolveInputStream( Object source );
}