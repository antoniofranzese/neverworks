package it.neverworks.io;

import java.io.OutputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import it.neverworks.lang.Errors;
import it.neverworks.model.converters.Converter;

public class OutputStreamConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof OutputStream ) {
            return value;

        } else if( value instanceof File ) {
            try {
                return new FileOutputStream( (File) value );
            } catch( IOException ex ) {
                return value;
            }
            
        } else if( value instanceof FileInfo ) {
            return ((FileInfo) value).openStream();

        } else if( value instanceof FileInfoProvider ) {
            return convert( ((FileInfoProvider) value).retrieveFileInfo() );

        } else {
            return value;
        }

    }

}