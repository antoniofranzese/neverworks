package it.neverworks.io;

import java.io.File;
import it.neverworks.context.ApplicationEvent;

public abstract class AbstractDeletionEvent extends ApplicationEvent {

    protected File file;
    
    public AbstractDeletionEvent( Object source, File file ) {
        super( source );
        this.file = file;
    }

    public File getFile(){
        return this.file;
    }

}