package it.neverworks.io;

public interface NameSpaceAware {
    public void storeNameSpace( String namespace );
}