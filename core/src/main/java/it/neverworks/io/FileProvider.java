package it.neverworks.io;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import it.neverworks.lang.Arguments;

public interface FileProvider {
    
	public FileInfo getInfo( FileBadge badge );

	default File getFile( FileBadge badge ) {
        return getInfo( badge ).getFile();
    }
    
	default InputStream getStream( FileBadge badge ) {
        return getInfo( badge ).getStream();
    }

	default File getFile( FileInfo info ) {
        if( info != null ) {
    	    return getFile( info.getBadge() );
        } else {
            throw new IllegalArgumentException( "Null FileInfo" );
        }
	}
    
	default InputStream getStream( FileInfo info ) {
        if( info != null ) {
    	    return getStream( info.getBadge() );
        } else {
            throw new IllegalArgumentException( "Null FileInfo" );
        }
	}

	default File getFile( Arguments arguments ) {
        if( arguments != null ) {
    	    return getFile( reference( arguments ) );
        } else {
            throw new IllegalArgumentException( "Null Arguments" );
        }
	}
    
	default InputStream getStream( Arguments arguments ) {
        if( arguments != null ) {
    	    return getStream( reference( arguments ) );
        } else {
            throw new IllegalArgumentException( "Null Arguments" );
        }
    }

	default FileInfo getInfo( Arguments arguments ) {
        if( arguments != null ) {
    	    return getInfo( reference( arguments ) );
        } else {
            throw new IllegalArgumentException( "Null Arguments" );
        }
	}

    default FileBadge reference( Arguments arguments ) {
        return new FileBadge( arguments );
    }
    
    default Arguments dereference( FileBadge badge ) {
        return badge.getArguments();
    }

}