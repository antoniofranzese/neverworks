package it.neverworks.io;

public class MissingFileException extends RuntimeException {
    
    public MissingFileException( String message ) {
        super( message );
    }

    public MissingFileException( String message, Throwable cause ) {
        super( message, cause );
    }

}