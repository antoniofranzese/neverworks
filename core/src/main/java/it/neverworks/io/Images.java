package it.neverworks.io;

import static it.neverworks.language.*;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.awt.image.BufferedImage;
import java.awt.Graphics2D;
import java.awt.Color;
import javax.imageio.stream.ImageOutputStream;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.FileCacheImageOutputStream;
import javax.imageio.plugins.jpeg.JPEGImageWriteParam;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.IIOImage;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.model.types.TypeDefinition;

public class Images {

    private final static TypeDefinition<InputStream> InputStreamType = TypeDefinition.of( InputStream.class );
    private final static TypeDefinition<Color> ColorType = TypeDefinition.of( Color.class );

    public static void write( RenderedImage source, Object output, Arguments args ) {
        try {
            String format = args.get( "format", "png" );

            ImageOutputStream stream = null;
            
            if( output instanceof ImageOutputStream ) {
                stream = (ImageOutputStream) output;
            } else if( output instanceof File ) {
                stream = new FileImageOutputStream( (File) output );
            } else if( output instanceof OutputStream ) {
                stream = new FileCacheImageOutputStream( (OutputStream) output, new File( System.getProperty("java.io.tmpdir") ) );
            } else {
                throw new IllegalArgumentException( "Unknown output: " + repr( output ) );
            }

            if( "jpg".equalsIgnoreCase( format ) || "jpeg".equalsIgnoreCase( format ) ) {
                JPEGImageWriteParam jpegParams = new JPEGImageWriteParam( null );
                jpegParams.setCompressionMode( ImageWriteParam.MODE_EXPLICIT );
                float quality = Float( args.get( "quality", 70 ) ) / 100.0f;

                jpegParams.setCompressionQuality( quality );

                ImageWriter writer = ImageIO.getImageWritersByFormatName( "jpg" ).next();
                
                writer.setOutput( stream );

                writer.write( null, new IIOImage( source, null, null ), jpegParams );
                stream.flush();
                stream.close();
            } else {
                ImageIO.write( source, format, stream );
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }

    }
    
    public static BufferedImage read( Object source) {
        try {
            return ImageIO.read( InputStreamType.process( source ) );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static byte[] asBytes( RenderedImage image ) {
        return asBytes( image, new Arguments() );
    }
    
    public static byte[] asBytes( RenderedImage image, Arguments args ) {
        try {

            if( image == null ) {
                image = new BufferedImage( 1, 1, BufferedImage.TYPE_INT_ARGB );
            }
            
            String format = args.get( "format", "png" );
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ImageIO.write( image, format, outputStream );
            return outputStream.toByteArray();
            
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public static InputStream asStream( RenderedImage image ) {
        return asStream( image, new Arguments() );
    }
    
    public static InputStream asStream( RenderedImage image, Arguments args ) {
        return new ByteArrayInputStream( asBytes( image, args ) );
    }
    
    public static BufferedImage error( Throwable throwable ) {
        return error( throwable, new Arguments() );
    }
    
    public static BufferedImage error( Throwable throwable, Arguments args ) {
        int width = Numbers.Int( args.get( "width", 500 ) );
        int height = Numbers.Int( args.get( "height", 500 ) );
        Exception ex = Errors.unwrap( throwable );
        
        BufferedImage image = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );
        Graphics2D g2d = image.createGraphics();
        
        g2d.setColor( args.get( ColorType, "background", Color.WHITE ) );
        g2d.fillRect( 0, 0, width, height );
        
        g2d.setColor( args.get( ColorType, "color", Color.BLACK )  );
        int y = 15;
        
        g2d.drawString( ex.getClass().getName() + ": " + ex.getMessage(), 5, y );

        for( StackTraceElement el: ex.getStackTrace() ) {
            y += g2d.getFont().getSize() + 1;
            g2d.drawString( Strings.message( "at {0}.{1}({2}:{3})", el.getClassName(), el.getMethodName(), el.getFileName(), el.getLineNumber() ), 15, y );
        }
        
        return image;
    }
    
    public static BufferedImage text( String text ) {
        return text( text, new Arguments() );
    }
    
    public static BufferedImage text( String text, Arguments args ) {
        int width = Numbers.Int( args.get( "width", 500 ) );
        int height = Numbers.Int( args.get( "height", 500 ) );

        BufferedImage image = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );
        Graphics2D g2d = image.createGraphics();
        
        g2d.setColor( args.get( ColorType, "background", Color.WHITE ) );
        g2d.fillRect( 0, 0, width, height );
        
        g2d.setColor( args.get( ColorType, "color", Color.BLACK )  );
        int y = 15;
        
        for( String s: Strings.safe( text ).split( "\n" ) ) {
            g2d.drawString( s, 5, y );
            y += g2d.getFont().getSize() + 1;
        }
        
        return image;
    }
    
    private static class TextWriter {
        private BufferedImage image;
        private Graphics2D g2d;
        private int x = 0;
        private int y = 0;

        public TextWriter( int width, int height ) {
            this.image = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );
            this.g2d = image.createGraphics();
        }
        
        public TextWriter top( int top ) {
            this.y = top;
            return this;
        }

        public TextWriter left( int left ) {
            this.x = left;
            return this;
        }
        
        public TextWriter println( String str ) {
            this.g2d.drawString( str, this.x, this.y );
            this.y += g2d.getFont().getSize() + 1;
            return this;
        }
        
        public BufferedImage image() {
            return this.image;
        }

    }
    
}