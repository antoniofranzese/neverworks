package it.neverworks.io;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Arguments;
import it.neverworks.io.FileInfo;
import it.neverworks.io.FileRepository;
import it.neverworks.io.FileInfoProvider;
import it.neverworks.io.AutoDeleteFileInputStream;

public interface ContentProvider extends TypedProvider {

    OutputStream to( OutputStream stream );
    
    default File to( File file ) {
        try {
            if( ! file.exists() ) {
                file.createNewFile();
            }
            to( new FileOutputStream( file ) );
            return file;
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    default FileInfo to( FileInfo info ) {
        to( info.openStream() );
        if( info.get( "type" ) == null ) {
            info.set( "type", getMimeType() );
        }
        return info;
    }
    
    default FileInfo to( FileRepository repository, Arguments arguments ) {
        if( arguments.get( "type" ) == null ) {
            arguments.set( "type", getMimeType() );
        }
        return to( repository.<FileInfo>create( arguments ) );
    }
    
    default FileInfo to( FileInfoProvider infoProvider ) {
        return to( infoProvider.retrieveFileInfo() );
    }
 
    default FileInfo to( FileRepository repository ) {
        return to( repository.<FileInfo>create() );
    }


}