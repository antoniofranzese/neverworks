package it.neverworks.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.io.UnsupportedEncodingException;
import java.io.PushbackInputStream;
import java.io.PushbackReader;
import java.io.Closeable;
import java.io.Flushable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import org.apache.commons.lang.ArrayUtils;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Numbers;
import it.neverworks.log.Logger;

public class Streams {

    private static final Logger logger = Logger.of( Streams.class );

    public static Object close( Object closeable ) {
        if( closeable instanceof Closeable ) {
        	try {
                flush( closeable );
        		((Closeable) closeable).close();
        	} catch (IOException e) {
        		throw Errors.wrap( e );
        	}
        }
        return closeable;
    }

    public static Object flush( Object flushable ) {
        if( flushable instanceof Flushable ) {
        	try {
        		((Flushable) flushable).flush();
        	} catch (IOException e) {
        		throw Errors.wrap( e );
        	}
        }
        return flushable;
    }

    public static void copy( InputStream input, OutputStream output ) {
        copy( input, output, new Arguments() );
    }

    public static void copy( InputStream input, OutputStream output, Arguments arguments ) {
        boolean close = Booleans.isTrue( arguments.get( "close", false ) );
        int max =  Numbers.Int( arguments.get( "max", -1 ) );

        logger.debug( "Starting stream copy, max = {}, close = {}", max, close );
            
		try {
			byte buffer[] = new byte[ 8192 ];
			int readCount;
            int total = 0;
			while ( ( max <= 0 || total < max ) && ( readCount = input.read( buffer ) ) != -1 ) {
                int len = max > 0 ? (( max - total ) > readCount ? readCount : max - readCount) : readCount;
				output.write( buffer, 0, len );
                total += len;
			}

            logger.debug( "{0} bytes copied, flushing output stream", total );
			output.flush();
            
            if( close ) {
                logger.debug( "Closing streams" );
                input.close();
    			output.close();
            }
		} catch (IOException e) {
			throw Errors.wrap( e );
		}
    }
    	
    public static byte[] asBytes( InputStream inputStream ) {
        return asBytes( inputStream, new Arguments() );
    }
    
    public static byte[] asBytes( InputStream inputStream, Arguments arguments ) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        copy( inputStream, outputStream, arguments );
        return outputStream.toByteArray();
    }

    public static String asString( InputStream inputStream ) {
        return asString( inputStream, new Arguments() );
    }
    
    public static String asString( InputStream inputStream, Arguments arguments ) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            copy( inputStream, outputStream, arguments );
            return arguments.get( String.class, "encoding", null ) == null 
                ? outputStream.toString()
                : outputStream.toString( arguments.get( String.class, "encoding" ) );
        } catch( UnsupportedEncodingException e ) {
            throw Errors.wrap( e );
        }
    }

    
    public static String asString( Reader reader ) {
        return asString( reader, new Arguments() );
    }
    
    public static String asString( Reader reader, Arguments arguments ) {
        StringWriter writer = new StringWriter();
        copy( reader, writer, arguments );
        return writer.toString();
    }
    
    public static void copy( Reader reader, Writer writer ) {
        copy( reader, writer, new Arguments() );
    }

    public static void copy( Reader reader, Writer writer, Arguments arguments ) {
        boolean close = Booleans.isTrue( arguments.get( "close", false ) );
        int max =  Numbers.Int( arguments.get( "max", Integer.MAX_VALUE ) );

        logger.debug( "Starting reader/writer copy, max = {}, close = {}", max, close );
            
        try {
            char[] buf = new char[ 1024 ];
            int n;
            int count = 0;
            while( count < max && ( n = reader.read( buf ) ) >= 0 ) {
                int len = ( max - count ) > n ? n : max - count;
                writer.write( buf, 0, len );
                count += len;
            }

            logger.debug( "{0} characters copied, flushing writer", count );
            writer.flush();
            
            if( close ) {
                logger.debug( "Closing reader/writer" );
                reader.close();
                writer.close();
            }
        } catch (IOException e) {
            throw Errors.wrap( e );
        }
    }

    
    public static InputStream asStream( String string ) {
        return new ByteArrayInputStream( string != null ? string.getBytes() : new byte[0] );
    }

    public static InputStream asStream( File file ) {
        try {
            return new NamedBufferedInputStream( new FileInputStream( file ) );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static InputStream asStream( byte[] bytes ) {
        return new ByteArrayInputStream( bytes );
    }
    
    public static InputStream asStream( Byte[] bytes ) {
        return new ByteArrayInputStream( ArrayUtils.toPrimitive( bytes ) ); 
    }
    
    public static InputStream marking( InputStream stream ) {
        if( stream != null ) {
            if( stream.markSupported() ) {
                return stream;
            } else {
                return new BufferedInputStream( stream );
            }
        } else {
            return null;
        }
    }

    public static InputStream plane( InputStream stream ) {
        try {
            if( stream != null ) {
                
                if( stream.markSupported() ) {
                    stream.mark( 2 );
                    if( stream.read() == -1 ) {
                        return null;
                    } else {
                        stream.reset();
                        return stream;
                    }

                } else {
                    PushbackInputStream pushback = new PushbackInputStream( stream );
                    int b = pushback.read();
                    if( b == -1 ) {
                        return null;
                    } else {
                        pushback.unread( b );
                        return pushback;
                    }
                }

            } else {
                return stream;
            }
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static Reader plane( Reader reader ) {
        try {
            if( reader != null ) {
                if( reader.markSupported() ) {
                    reader.mark( 2 );
                    if( reader.read() == -1 ) {
                        return null;
                    } else {
                        reader.reset();
                        return reader;
                    }
                        
                } else {
                    PushbackReader pushback = new PushbackReader( reader );
                    int b = pushback.read();
                    if( b == -1 ) {
                        return null;
                    } else {
                        pushback.unread( b );
                        return pushback;
                    }
                }
                    
            } else {
                return reader;
            }
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public static void gzip( String string, OutputStream out ) {
        try {
            GZIPOutputStream gzip = new GZIPOutputStream( out );
            gzip.write( string.getBytes( "UTF-8" ) );
            gzip.close();
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public static InputStream empty() {
        return new ByteArrayInputStream( new byte[ 0 ] );
    }
    
    
}
