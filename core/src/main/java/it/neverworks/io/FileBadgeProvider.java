package it.neverworks.io;

public interface FileBadgeProvider {
    FileBadge retrieveFileBadge();
}