package it.neverworks.io;

import java.io.File;
import java.io.Writer;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import it.neverworks.lang.Arguments;

public interface FileRepository extends FileProvider {

    public File createFile();
    public File createFile( Arguments arguments );
    
    public FileInfo create();
    public FileInfo create( Arguments arguments );
    public OutputStream openStream( FileBadge badge );
    default OutputStream openStream( FileInfo info ) {
        if( info != null ) {
            return openStream( info.getBadge() );
        } else {
            throw new IllegalArgumentException( "Null FileInfo" );
        }
    }
    
    public Writer openWriter( FileBadge badge );
    default Writer openWriter( FileInfo info ) {
        if( info != null ) {
            return openWriter( info.getBadge() );
        } else {
            throw new IllegalArgumentException( "Null FileInfo" );
        }
    }
    
    
    public void describe( FileBadge badge, Arguments arguments );
    default void describe( FileInfo info, Arguments arguments ) {
        if( info != null ) {
            describe( info.getBadge(), arguments );
        } else {
            throw new IllegalArgumentException( "Null FileInfo" );
        }
    }
    
    public void update( FileBadge badge, Arguments arguments );
    default void update( FileInfo info, Arguments arguments ) {
        if( info != null ) {
            update( info.getBadge(), arguments );
        } else {
            throw new IllegalArgumentException( "Null FileInfo" );
        }
    }
    
    default void shutdown() {}

    public <T extends FileBadge> T putFile( File file );
    public <T extends FileBadge> T putFile( File file, Arguments arguments );
    public <T extends FileBadge> T putStream( InputStream stream );
    public <T extends FileBadge> T putStream( InputStream stream, Arguments arguments );
    
}