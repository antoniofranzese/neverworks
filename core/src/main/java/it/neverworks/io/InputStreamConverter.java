package it.neverworks.io;

import java.io.InputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.File;
import it.neverworks.lang.Errors;
import it.neverworks.io.Streams;
import it.neverworks.model.converters.Converter;

public class InputStreamConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof InputStream ) {
            return value;
        } else if( value instanceof InputStreamProvider ) {
            return  ((InputStreamProvider) value).toStream();
        } else if( value instanceof FileInfoProvider ) {
            return convert( ((FileInfoProvider) value).retrieveFileInfo() );
        } else if( value instanceof FileInfo ) {
            return ((FileInfo) value).getStream();
        } else if( value instanceof File ) {
            try {
                return new NamedFileInputStream( (File) value );
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        } else if( value instanceof String ) {
            return Streams.asStream( (String) value );
        } else {
            return value;
        }
    }
}