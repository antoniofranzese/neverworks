package it.neverworks.io;

import java.util.Map;
import java.util.List;
import java.io.File;
import java.io.InputStream;

import static it.neverworks.language.*;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.collections.Collection;
import it.neverworks.model.collections.MapAdder;
import it.neverworks.io.FileProvider;
import it.neverworks.io.FileBadge;
import it.neverworks.io.FileInfo;

public class NameSpaceDispatcher extends BaseModel implements FileProvider {
    
    protected final static String NAMESPACE_SEPARATOR = "/";
    protected final static String DEFAULT_NAMESPACE = "DEFAULT";
    
    @Property @Collection
    protected Map<String,FileProvider> providers;
    
	public File getFile( FileBadge badge ) {
        return providerFor( badge ).getFile( badge );
    }
    
	public InputStream getStream( FileBadge badge ) {
        return providerFor( badge ).getStream( badge );
    }
    
	public FileInfo getInfo( FileBadge badge ) {
        return providerFor( badge ).getInfo( badge );
    }
    
    protected FileProvider providerFor( FileBadge badge ) {
        String token = badge.getToken();
        int index = token.indexOf( NAMESPACE_SEPARATOR );
        String ns = index < 0 ? DEFAULT_NAMESPACE : token.substring( 0, index );
        
        if( providers.containsKey( ns ) ) {
            return providers.get( ns );
        } else {
            throw new IllegalStateException( "No '" + ns + "' provider configured for token: " + token );
        }
    }
    
    public Map<String,FileProvider> getProviders(){
        return this.providers;
    }  
    
    public void setProviders( Map<String,FileProvider> providers ){
        this.providers = providers;
    }
    
    protected void addProvidersItem( MapAdder<String,FileProvider> adder ){
        adder.add();
        if( adder.item() instanceof NameSpaceAware ) {
            ((NameSpaceAware) adder.item()).storeNameSpace( adder.key() + NAMESPACE_SEPARATOR );
        }
    }
    
    
}