package it.neverworks.io;

public interface FileInfoProvider {
    FileInfo retrieveFileInfo();
}