package it.neverworks.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class AutoDeleteFileInputStream extends NamedFileInputStream {

    public AutoDeleteFileInputStream( String name ) throws FileNotFoundException {
      this( new File( name ) );
   }
   
   public AutoDeleteFileInputStream( File file ) throws FileNotFoundException {
      super( file );
   }

   public void close() throws IOException {
       try {
           super.close();
       } finally {
           this.cleanup();
       }
   }
   
   protected void finalize() throws IOException {
       cleanup();
   }
   
   protected void cleanup() throws IOException {
       if( this.file != null ) {
           try {
               file.delete();
           } catch( Exception ex ) {
               // Nothing to do
           } finally {
               this.file = null;
           }
       }
   }

}