package it.neverworks.io;

import java.io.Closeable;
import java.io.IOException;
import it.neverworks.lang.Guard;

public interface CloseableGuard<T> extends Closeable, Guard<T> {

    default Throwable exit( Throwable error ){
        try {
            this.close();
        } catch( IOException ex ) {

        }
        return error;
    }

}