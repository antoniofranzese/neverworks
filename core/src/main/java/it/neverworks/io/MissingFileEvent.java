package it.neverworks.io;

import it.neverworks.context.ApplicationEvent;

public class MissingFileEvent extends ApplicationEvent {

    private FileBadge badge;
    
    public MissingFileEvent( Object source, FileBadge badge ) {
        super( source );
        this.badge = badge;
    }

    public MissingFileEvent( Object source, String token ) {
        super( source );
        this.badge = new FileBadge( token );
    }
    
    public FileBadge getBadge(){
        return this.badge;
    }
    
}