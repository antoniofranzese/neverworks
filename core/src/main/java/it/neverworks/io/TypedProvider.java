package it.neverworks.io;

import java.io.InputStream;

public interface TypedProvider {
    String getMimeType();
}