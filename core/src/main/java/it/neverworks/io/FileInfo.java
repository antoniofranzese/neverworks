package it.neverworks.io;

import java.io.File;
import java.io.Writer;
import java.io.InputStream;
import java.io.OutputStream;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Access;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.utils.ExpandoModel;

@Convert( FileInfoConverter.class )
public class FileInfo extends BaseModel {
    
    @Property @Access( setter = "updateProperty" )
    protected String name;
    
    @Property
    protected FileBadge badge;
    
    @Property @Access( setter = "updateProperty" )
    protected String type;
    
    @Property
    protected FileRepository repository;
    
    @Property
    protected FileProvider provider;
    
    @Property @Convert( InputStreamProviderConverter.class )
    private InputStreamProvider stream;
    
    @Property
    protected ExpandoModel attributes;
    
    public FileInfo() {
        super();
    }
    
    public FileInfo( Arguments arguments ) {
        this();
        set( arguments );
    }

    protected void setAttributes( Setter<ExpandoModel> setter ) {
        if( setter.value().contains( "name" ) ) {
            set( "name", setter.value().get( "name" ) );
            setter.value().remove( "name" );
        }
        if( setter.value().contains( "type" ) ) {
            set( "type", setter.value().get( "type" ) );
            setter.value().remove( "type" );
        }
        setter.set();
    }

    protected void updateProperty( Setter setter ) {
        setter.set();
        update( setter.name(), setter.raw() );
    }
    
    protected void update( String name, Object value ) {
        if( this.repository != null ) {
            this.repository.update( this.badge, new Arguments().arg( name, value ) );
        }
    }

    public <T> T get( String name ) {
        try {
            if( ! model().has( name ) && attributes != null ){
                return attributes.get( name );
            } else {
                return super.get( name );
            }
        } catch( MissingPropertyException ex ) {
            if( name.startsWith( "!" ) ) {
                return null;
            } else {
                throw ex;
            }
        }
    }
    
    public <T extends BaseModel> T set( String name, Object value ) {
        if( ! model().has( name ) ) {
            if( attributes == null ) {
                attributes = new ExpandoModel();
            }
            attributes.set( name, value );
            update( name, value );
            return (T)this;
        } else {
            return super.set( name, value );
        }
    }
    
     
	public File getFile() {
	    if( provider != null ) {
	        if( badge != null ) {
	            return provider.getFile( badge );
	        } else {
    	        throw new IllegalStateException( "Cannot retrieve File, missing FileBadge in FileInfo" );
	        }
	    } else {
	        throw new IllegalStateException( "Cannot retrieve File, FileInfo is not connected to a FileProvider" );
	    }
	}
	
    protected InputStream getStream( Getter<InputStreamProvider> getter ) {
        if( getter.defined() ) {
            return getter.get().toStream();
            
        } else if( this.provider != null ) {
	        if( this.badge != null ) {
                getter.decline();
	            return this.provider.getStream( badge );
	        } else {
    	        throw new IllegalStateException( "Cannot retrieve Stream, missing FileBadge in FileInfo" );
	        }
            
	    } else {
	        throw new IllegalStateException( "Cannot retrieve Stream, FileInfo is not connected to a FileProvider" );
	    }
    }

    public OutputStream openStream() {
        if( repository != null ) {
            if( badge != null ) {
                return repository.openStream( badge );
            } else {
                throw new IllegalStateException( "Cannot open Stream, missing FileBadge in FileInfo" );
            }
        } else {
            throw new IllegalStateException( "Cannot open Stream, FileInfo is not connected to a FileRepository" );
        }
    }

    public Writer openWriter() {
        if( repository != null ) {
            if( badge != null ) {
                return repository.openWriter( badge );
            } else {
            throw new IllegalStateException( "Cannot open Writer, missing FileBadge in FileInfo" );
            }
        } else {
            throw new IllegalStateException( "Cannot open Writer, FileInfo is not connected to a FileRepository" );
        }
    }
    
    @Virtual
    public boolean isConnected() {
        return this.repository != null;
    }
    
    @Virtual
    public boolean isStatic() {
        return this.stream instanceof StaticInputStreamProvider;
    }
    
    protected void setRepository( Setter<FileRepository> setter ) {
        setter.set();
        if( this.provider == null ) {
            this.provider = setter.value();
        }
    }
    
    public FileBadge getBadge(){
        return this.badge;
    }
    
    public void setBadge( FileBadge badge ){
        this.badge = badge;
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName( String name ){
        this.name = name;
    }
    
    public String getType(){
        return this.type;
    }
    
    public void setType( String type ){
        this.type = type;
    }

    public FileRepository getRepository(){
        return this.repository;
    }
    
    public void setRepository( FileRepository repository ){
        this.repository = repository;
    }

    public FileProvider getProvider(){
        return this.provider;
    }
    
    public void setProvider( FileProvider provider ){
        this.provider = provider;
    }
    
    public ExpandoModel getAttributes(){
        return this.attributes;
    }
    
    public void setAttributes( ExpandoModel attributes ){
        this.attributes = attributes;
    }
    
    public InputStream getStream(){
        return this.model().get( "stream" );
    }
    
    public void setStream( InputStream stream ){
        this.model().set( "stream", stream );
    }

    public void setStream( InputStreamProvider stream ){
        this.model().set( "stream", stream );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "badge" )
            .add( "name" )
            .add( "type" )
            .add( "connected" )
            .add( "attributes" )
            .toString();
    }
}