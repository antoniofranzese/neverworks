package it.neverworks.io;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.utils.ToStringBuilder;

@Convert( FileBadgeConverter.class )
public class FileBadge extends BaseModel {
    
    protected String token;
    protected Arguments arguments;

    public FileBadge( Object object ) {
        this( Strings.valueOf( object ) );
    }
    
    public FileBadge( String token ) {
        if( Strings.hasText( token ) ) {
            this.token = token;
        } else {
            throw new IllegalArgumentException( "Invalid FileBadge token: " + token );
        }
    }
    
    public FileBadge( Arguments arguments ) {
        this( arguments.format() );
    }
    
    public String getToken(){
        return this.token;
    }

    public Arguments getArguments() {
        if( arguments == null ) {
            arguments = Arguments.isParsable( this.token ) ? Arguments.parse( this.token ) : new Arguments().arg( "token", token );
        }
        return arguments;
    }
    
    public <T> T get( String name ) {
        if( "token".equals( name ) ) {
            return (T) this.token;
        } else {
            return getArguments().<T>get( name );
        }
    }
    
    public <T extends BaseModel> T set( String name, Object value ) {
        throw new UnsupportedOperationException( this.getClass().getSimpleName() + " is not mutable" );
    }

    public String toString() {
        return new ToStringBuilder( this ).add( "token" ).toString();
    }
    
    public Object convert( Object value ) {
        if( value instanceof FileBadge ) {
            return value;
        } else if( value instanceof String ) {
            return new FileBadge( (String) value );
        } else {
            return value;
        }
    }
}