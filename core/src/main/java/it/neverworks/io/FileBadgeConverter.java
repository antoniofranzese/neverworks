package it.neverworks.io;

import it.neverworks.model.converters.Converter;

public class FileBadgeConverter implements Converter {

    public Object convert( Object value ) {
        if( value instanceof FileBadge ) {
            return value;
        } else if( value instanceof String ) {
            return new FileBadge( (String) value );
        } else {
            return value;
        }
    }

}