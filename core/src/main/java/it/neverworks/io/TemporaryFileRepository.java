package it.neverworks.io;

import static it.neverworks.language.*;

import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.DisposableBean;

import it.neverworks.log.Logger;
import it.neverworks.lang.Arguments;
import it.neverworks.io.Streams;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.encoding.URL;
import it.neverworks.model.utils.ToStringBuilder;

public class TemporaryFileRepository extends AbstractFileRepository implements InitializingBean, DisposableBean, ListableFileProvider, ApplicationEventPublisherAware, Closeable {
    
    private static final Logger logger = Logger.of( TemporaryFileRepository.class );
    
    protected String filePrefix = "NWK-";
    protected String infoSuffix = ".inf";
    protected String temporaryPath;
    protected int maxAge = 86400; 
    protected int cleanupDelay = 120; 
    protected int cleanupPeriod = 3600; 
    private boolean cleanupOnStart = false;
    
    protected Timer cleanupTimer;
    
    protected boolean encode = true; 
    protected ApplicationEventPublisher publisher;
    
    public TemporaryFileRepository() {
        super();
        setTemporaryPath( System.getProperty( "java.io.tmpdir" ) );
    }

    public TemporaryFileRepository( Arguments arguments ) {
        this();
        set( arguments );
    }

    public File createFile( Arguments arguments ) {
        return getNewFile( arguments.<String>get( "extension", "tmp" ) );
    }

    public OutputStream openStream( FileBadge badge ) {
        try {
            return new FileOutputStream( getFile( badge ) );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public Writer openWriter( FileBadge badge ) {
        try {
            return new FileWriter( getFile( badge ) );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    /* FileRepository interface */
    public File getFile( FileBadge badge ) {
        return getFile( badge.getToken() );
    }
    
    public InputStream getStream( FileBadge badge ) {
        return getFileAsStream( badge.getToken() );
    }
    
    public FileInfo getInfo( FileBadge badge ) {
        File file = getFile( badge.getToken() );
        if( file.exists() ) {
            FileInfo result = new FileInfo()
                .set( "badge", badge )
                .set( "attributes", readInfo( badge.getToken() ) )
                .set( "repository", this );
            return result;

        } else {
            throw new MissingFileException( "Cannot retrieve file for token: " + badge.getToken() );
        }
    }

    public File getFile( String token ) {
        return new File( temporaryPath + extractToken( token ) );
    }
    
    public InputStream getFileAsStream( String token ) {
        try {
            return new NamedFileInputStream( getFile( token ), readInfo( token ).get( "name", null ) );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public List<FileInfo> list( FileInfo info ) {
        String token = extractToken( info.getBadge().getToken() );
        File dir = getFile( token );
        List<FileInfo> result = new ArrayList<FileInfo>();
        if( dir.isDirectory() ) {
            for( File file: dir.listFiles() ) {
                if( ! file.getName().endsWith( infoSuffix ) ) {
                    result.add( getInfo( new FileBadge( qualifyToken( token + "/" + file.getName() ) ) ) );
                }
            }
            return result;
        } else {
            throw new IllegalArgumentException( "Not a directory: " + info.getBadge() );
        }
    }
    
    protected String storeStream( InputStream stream, Arguments arguments ) {
        ensureTemporaryPath();
        try {
            String fileExt = arguments.<String>get( "extension", null );
            
            File tmp = null;
            do {
                String fileId = filePrefix + generateFileId() + ( fileExt != null ? "." + fileExt : "" );
                tmp = new File( temporaryPath + fileId );
            } while( tmp.exists() );
            
            try {
                tmp.createNewFile();
            } catch( Exception ex ) {
                throw new IOException( "Error creating " + tmp.getCanonicalPath() + ": " + ex.getMessage(), ex );
            }

            logger.debug( "Creating temporary file: {name}", tmp );
            Streams.copy( stream, new FileOutputStream( tmp ), arg( "close", true ) );

            if( arguments.has( "extension" ) ) {
                arguments.remove( "extension" );
            }

            writeInfo( tmp.getName(), arguments );
            return tmp.getName();

        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    protected Arguments readInfo( String token ) {
        Arguments result = new Arguments();

        try {
            File inf = getFile( token + infoSuffix );

            if( inf.exists() ) {
                Properties props = new Properties();
                props.load( new FileInputStream( inf ) );
                for( String key: props.stringPropertyNames() ) {
                    String value = Strings.safe( Strings.valueOf( props.get( key ) ) ).trim();
                    if( Strings.hasText( value ) ) {
                        result.put( key, decodeValue( value ) );
                    }
                }
            }
            
            return result;

        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        
    }

    protected void writeInfo( String token, Arguments arguments ) {
        try {
            if( arguments.size() > 0 ) {
                File inf = new File( temporaryPath + extractToken( token ) + infoSuffix );
                inf.createNewFile();
            
                FileWriter writer = new FileWriter( inf );
                for( String key: arguments.keys() ) {
                    if( arguments.get( key ) != null ) {
                        writer.write( Strings.message( "{0}={1}\n", key, encodeValue( arguments.get( key ) ) ) );
                    }
                }
            
                writer.flush();
                writer.close();
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        
    }
    
    protected String encodeValue( Object value ) {
        return encode ? URL.encode( value ) : Strings.valueOf( value );
    }
    
    protected String decodeValue( Object value ) {
        return encode ? URL.decode( value ) : Strings.valueOf( value );
    }
    
    public void cleanUp(){
        if( this.cleanupTimer != null ) {
            logger.info( "Cancelling internal cleanup task" );
            this.cleanupTimer.cancel();
        }
        cleanMainFolder();
    }
    
    protected void cleanMainFolder() {
        if( this.maxAge >= 0 ) {
            ensureTemporaryPath();
            logger.info( "Starting folder cleanup: {}", temporaryPath );
            cleanFolder( new File( temporaryPath ) );
            logger.info( "Folder cleanup terminated" );
        }
    }
    
    protected void cleanFolder( File folder ) {
        logger.debug( "Cleaning folder: {absolutePath}", folder );
        if( folder.exists() && folder.isDirectory() ) {
            File[] files = folder.listFiles();

            if( files != null ) {
                for( File file: files ) {
                    if( file.isDirectory() ) {
                        cleanFolder( file );
                        if( file.listFiles() == null || file.listFiles().length == 0 ) {
                            logger.debug( "Deleting folder: {absolutePath}", file );

                            FileDeletionRequestEvent folderEvent = new FileDeletionRequestEvent( this, file );
                            if( publisher != null ) publisher.publishEvent( folderEvent );

                            if( folderEvent.isAllowed() ) {
                                try {
                                    file.delete();
                                    if( publisher != null ) publisher.publishEvent( new FileDeletionSuccessEvent( this, file ) );
                                } catch( Exception ex ) {
                                    if( publisher != null ) publisher.publishEvent( new FileDeletionFailureEvent( this, file, ex ) );
                                }
                            }
                        }

                    } else {
                        if( ( !Strings.hasText( filePrefix ) || file.getName().startsWith( filePrefix ) ) 
                            && ( ! file.getName().endsWith( infoSuffix ) )
                            && ( System.currentTimeMillis() - file.lastModified() ) / 1000 > maxAge ) {
                            logger.debug( "Deleting file: {absolutePath}", file );

                            FileDeletionRequestEvent fileEvent = new FileDeletionRequestEvent( this, file );
                    
                            if( fileEvent.isAllowed() ) {
                                try {
                                    if( file.delete() ) {
                                        if( publisher != null ) publisher.publishEvent( new FileDeletionSuccessEvent( this, file ) );
                                    } else {
                                        if( publisher != null ) publisher.publishEvent( new FileDeletionFailureEvent( this, file, new Exception( "Cannot delete" ) ) );
                                    }
                                } catch( Exception ex ) {
                                    if( publisher != null ) publisher.publishEvent( new FileDeletionFailureEvent( this, file, ex ) );
                                }

                                File inf = new File( file.getAbsolutePath() + infoSuffix );
                                if( inf.exists() ) {
                                    logger.debug( "Deleting inf: {absolutePath}", inf );
                                    try {
                                        if( inf.delete() ) {
                                            if( publisher != null ) publisher.publishEvent( new FileDeletionSuccessEvent( this, inf ) );
                                        } else {
                                            if( publisher != null ) publisher.publishEvent( new FileDeletionFailureEvent( this, inf, new Exception( "Cannot delete" ) ) );
                                        }
                                    } catch( Exception ex ) {
                                        if( publisher != null ) publisher.publishEvent( new FileDeletionFailureEvent( this, inf, ex ) );
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    public String getPath() {
        return getTemporaryPath();
    }
    
    public void setPath( String path ) {
        setTemporaryPath( path );
    }

    public void setFolder( String folder ) {
        setTemporaryPath( System.getProperty( "java.io.tmpdir" ) + File.separator + folder );
    }
    
    public String getTemporaryPath() {
        return temporaryPath;
    }

    public void setTemporaryPath( String temporaryPath ) {
        this.temporaryPath = ensurePath( temporaryPath );
    }

    protected String ensureTemporaryPath() {
        return ensurePath( this.temporaryPath );
    }

    protected String ensurePath( String path ) {
        try {
            File file = new File( path );
            if( !file.isDirectory() ) {
                try {
                    logger.info( "Creating {absolutePath} directory", file );
                    file.mkdirs();
                } catch( Exception e ) {
                    logger.error( "Cannot create directory {absolutePath} ", file );
                    logger.error( e );
                }
            }
            String absPath = file.getCanonicalPath();
            return absPath.endsWith( "/" ) ? absPath : absPath + "/";
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public int getMaxAge() {
        return maxAge;
    }

    public void setCleanupPeriod( int cleanupPeriod ){
        this.cleanupPeriod = cleanupPeriod;
    }
    
    public void setCleanupDelay( int cleanupDelay ){
        this.cleanupDelay = cleanupDelay;
    }
    
    public void setMaxAge( int maxAge ) {
        this.maxAge = maxAge;
    }
    
    public void setFilePrefix( String filePrefix ){
        this.filePrefix = filePrefix;
    }
    
    public void setEncode( boolean encode ){
        this.encode = encode;
    }
    
    public void setCleanupOnStart( boolean cleanupOnStart ){
        this.cleanupOnStart = cleanupOnStart;
    }
    
    @Override
    public void setApplicationEventPublisher( ApplicationEventPublisher publisher ) {
        this.publisher = publisher;
    }

    public void afterPropertiesSet() throws Exception {
        logger.info( "Starting repository in {temporaryPath}", this );
        if( this.cleanupOnStart ) {
            this.cleanUp();
        }
        if( this.maxAge >= 0 ) {
            logger.info( "Scheduling built in cleaner in {temporaryPath}", this );
            this.cleanupTimer = new Timer( /* daemon */ true );
            this.cleanupTimer.scheduleAtFixedRate( new CleanupTask(), this.cleanupDelay * 1000, this.cleanupPeriod * 1000 );
        }
    }
    
    public void destroy() throws Exception {
        //TODO: clean folder? 
        //set max age to 0 and clean
        if( this.cleanupTimer != null ) {
            this.cleanupTimer.cancel();
            this.cleanupTimer = null;
        }
    }

    public void shutdown() {
        try {
            this.destroy();
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public void close() {
        this.shutdown();
    }

    protected class CleanupTask extends TimerTask {
        
        public void run() {
            TemporaryFileRepository.this.cleanMainFolder();
        }
    
    }

    @Override
    protected void finalize() throws Throwable {
        this.destroy();
        if( this.cleanupTimer != null ) {
            this.cleanupTimer.cancel();
        }
    }

    /* Legacy implementation */
    
    public File getNewFile( String fileExtension ) {
        ensureTemporaryPath();
        try {
            File tmpfile = File.createTempFile( filePrefix + generateFileId(), fileExtension, new File( temporaryPath ) );
            return tmpfile;
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "path", this.temporaryPath )
            .add( "filePrefix", this.filePrefix )
            .add( "maxAge", this.maxAge )
            .add( "cleanupPeriod", this.cleanupPeriod )
            .add( "cleanupDelay", this.cleanupDelay )
            .toString();
    }
    
}