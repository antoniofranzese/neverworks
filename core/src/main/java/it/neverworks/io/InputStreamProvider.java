package it.neverworks.io;

import java.io.InputStream;

public interface InputStreamProvider {
    InputStream toStream();
}