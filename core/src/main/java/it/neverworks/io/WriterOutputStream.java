package it.neverworks.io;

import java.io.Writer;
import java.io.OutputStream;
import java.io.IOException;

public class WriterOutputStream extends OutputStream {
    
    private Writer writer;
    
    public WriterOutputStream( Writer writer ) {
        super();
        this.writer = writer;
    }
    
    public void write( int b ) throws IOException {
        writer.write( b );
    }
    
    public void flush() throws IOException {
        writer.flush();
    }
    
}