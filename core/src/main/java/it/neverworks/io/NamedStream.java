package it.neverworks.io;

public interface NamedStream {
    String getName();
}