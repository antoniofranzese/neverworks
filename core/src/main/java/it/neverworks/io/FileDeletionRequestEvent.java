package it.neverworks.io;

import java.io.File;

public class FileDeletionRequestEvent extends AbstractDeletionEvent {

    protected boolean allowed = true;
    
    public FileDeletionRequestEvent( Object source, File file ) {
        super( source, file );
        this.file = file;
    }

    public boolean isAllowed(){
        return this.allowed;
    }
    
    public void setAllowed( boolean allowed ){
        this.allowed = allowed;
    }
    
    public void allow() {
        setAllowed( true );
    }
    
    public void veto() {
        setAllowed( false );
    }
}