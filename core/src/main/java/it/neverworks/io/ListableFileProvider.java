package it.neverworks.io;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import it.neverworks.lang.Arguments;

public interface ListableFileProvider extends FileProvider {

    public <T extends FileInfo> List<T> list( FileInfo info );
    
}