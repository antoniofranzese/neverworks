package it.neverworks.io;

import java.io.InputStream;
import it.neverworks.model.utils.ToStringBuilder;

public class StaticInputStreamProvider implements InputStreamProvider {
    
    private InputStream stream;
    
    public StaticInputStreamProvider( InputStream stream ) {
        this.stream = stream;
    }
    
    public InputStream toStream() {
        return this.stream;
    }

    public String toString() {
        return new ToStringBuilder( this )
            .add( "stream", this.stream )
            .toString();
    }
}