package it.neverworks.io;

import java.io.InputStream;
import java.io.BufferedInputStream;

public class NamedBufferedInputStream extends BufferedInputStream implements NamedStream {
    
    private String name;
    
    public NamedBufferedInputStream( InputStream stream ) {
        super( stream );
        if( stream instanceof NamedStream ) {
            this.name = ((NamedStream) stream).getName();
        }
    }

    public NamedBufferedInputStream( InputStream stream, String name ) {
        super( stream );
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
}