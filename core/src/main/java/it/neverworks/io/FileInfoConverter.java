package it.neverworks.io;

import java.util.Map;
import it.neverworks.model.converters.Converter;

public class FileInfoConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Map ) {
            Map map = (Map) value;
            FileInfo info = new FileInfo();
            if( map.containsKey( "name" ) ) {
                info.setName( (String)map.get( "name" ) );
            }
            if( map.containsKey( "token" ) ) {
                info.setBadge( new FileBadge( (String)map.get( "token" ) ) );
            }
            if( map.containsKey( "type" ) ) {
                info.setType( (String)map.get( "type" ) );
            }
            return info;
        } else {
            return value;
        }
    }
}