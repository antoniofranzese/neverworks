package it.neverworks.io;

import java.io.File;

public class FileDeletionSuccessEvent extends AbstractDeletionEvent {

    public FileDeletionSuccessEvent( Object source, File file ) {
        super( source, file );
    }
    
}