package it.neverworks.io;

import java.util.List;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.ByteArrayInputStream;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.BaseModel;

public abstract class AbstractFileRepository extends BaseModel implements FileRepository {
    
    private final static String NAMESPACE_SEPARATOR = "/";

    private String namespace;
    
    public File createFile(){
        return createFile( new Arguments() );
    }

    public FileInfo create() {
        return create( new Arguments() );
    }

    public FileInfo create( Arguments arguments ) {
        InputStream stream = arguments.has( InputStream.class, "stream" ) ? arguments.get( "stream" ) : new ByteArrayInputStream( new byte[ 0 ] );
        String token = storeStream( stream, arguments );
        return getInfo( new FileBadge( qualifyToken( token ) ) );
    }

    public FileBadge putFile( File file ) {
        return putFile( file, new Arguments() );
    }

    public FileBadge putFile( File file, Arguments arguments ) {
        return new FileBadge( qualifyToken( storeFile( file, arguments ) ) );
    }
    
    public FileBadge putStream( InputStream stream ) {
        return putStream( stream, new Arguments() );
    }

    public FileBadge putStream( InputStream stream, Arguments arguments ) {
        return new FileBadge( qualifyToken( storeStream( stream, arguments ) ) );
    }

    protected String storeFile( File file, Arguments arguments ) {
        try {
    		return storeStream( new FileInputStream( file ), arguments ) ;
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
	}

	protected abstract String storeStream( InputStream stream, Arguments arguments );
	
    protected String generateFileId() {
		return Strings.generateHexID( 6 )
		+ "-" + Strings.generateHexID( 6 )
		+ "-" + Strings.generateHexID( 6 )
		+ "-" + Strings.generateHexID( 6 )
		+ "-" + Strings.generateHexID( 6 )
		+ "-" + Strings.generateHexID( 6 );
	}
    
    public String getNamespace(){
        return this.namespace;
    }
    
    public void setNamespace( String namespace ){
        this.namespace = namespace;
    }
    
    protected String qualifyToken( String token ) {
        return namespace != null ? ( namespace + NAMESPACE_SEPARATOR + token ) : token;
    }

    protected String extractToken( String qualified ) {
        int index = qualified.indexOf( NAMESPACE_SEPARATOR );
        if( index < 0 ) {
            return qualified;
        } else {
            if( namespace != null ) {
                String ns = qualified.substring( 0, index );
                String token = qualified.substring( index + 1 );
                if( namespace.equals( ns ) ) {
                    return token;
                } else {
                    throw new IllegalArgumentException( "Namespace mismatch for " + this.getClass().getSimpleName() + ": " + namespace + " expected, " + ns + " obtained" );
                }
            } else {
                throw new IllegalArgumentException( "Unexpected namespace for " + this.getClass().getSimpleName() + ": " + qualified.substring( 0, index ) );
            }
        }
    }   

    public void describe( FileBadge badge, Arguments arguments ) {
        writeInfo( badge.getToken(), arguments );
    }

    public void update( FileBadge badge, Arguments arguments ) {
        boolean updated = false;
        Arguments current = readInfo( badge.getToken() );

        if( arguments != null ) {
            for( String key: arguments.keys() ) {
                if( ! current.has( key ) || ( current.get( key ) == null ? arguments.get( key ) != null : !current.get( key ).equals( arguments.get( key ) ) ) ) {
                    current.set( key, arguments.get( key ) );
                    updated = true;
                }
            }
        }

        if( updated ) {
            writeInfo( badge.getToken(), current );
        }
    }
    
    protected abstract Arguments readInfo( String token );
    protected abstract void writeInfo( String token, Arguments arguments );
    

}