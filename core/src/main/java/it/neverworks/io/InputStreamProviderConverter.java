package it.neverworks.io;

import java.io.InputStream;
import it.neverworks.model.converters.Converter;

public class InputStreamProviderConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof InputStreamConverter ) {
            return value;
        } else if( value instanceof InputStream ) {
            return new StaticInputStreamProvider( (InputStream) value );
        } else {
            return value;
        }
    }
}