package it.neverworks.aop;

import it.neverworks.model.ModelException;

public class MissingInvocationException extends AopException {
    
    public MissingInvocationException( String message ) {
        super( message );
    }

}