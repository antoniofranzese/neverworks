package it.neverworks.aop;

/*
 * Rappresenta un oggetto invocabile come metodo
 */
public interface Proxy {
    Object invoke( Object instance, Object... arguments );
}