package it.neverworks.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.MethodAnnotation;

@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@MethodAnnotation( processor = InterceptProcessor.class )
public @interface Intercept {
    Class<? extends Interceptor> value();
}
