package it.neverworks.aop;

public interface InvocationAware {
    void storeInvocation( Invocation invocation );
}