package it.neverworks.aop;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.Errors;

/*
 * Interceptor che implementa la chiamata ad un metodo dell'istanza contenuta nell'Invocation.
 * Va posto generalmente in coda alla catena dal momento che non propaga ulteriormente l'invocazione.
 */
public class MethodCall implements Interceptor {
    
    private Method method;
    private Object staticInstance;
    
    public MethodCall( Method method ) {
        if( method != null ) {
            this.method = method;
            Reflection.makeAccessible( method );
        } else {
            throw new NullPointerException( "Null method in " + this.getClass().getName() );
        }
    }
    
    public MethodCall( Object instance, Method method ) {
        this( method );
        if( instance != null ) {
            this.staticInstance = instance;
        } else {
            throw new NullPointerException( "Null instance in " + this.getClass().getName() );
        }
    }
    
    public Object invoke( Invocation invocation ) {
        try {
            return this.method.invoke( this.staticInstance != null ? this.staticInstance : invocation.instance(), invocation.arguments().toArray() );
        } catch( InvocationTargetException ex ) {
            throw Errors.wrap( ex );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public boolean equals( Object other ) {
        if( other instanceof MethodCall ) {
            MethodCall o = (MethodCall) other;
            if( ! this.method.equals( o.method ) ) {
                return false;
            } else if( this.staticInstance != null ? this.staticInstance != o.staticInstance : o.staticInstance != null ) {
                return false;
            } else {
                return true;
            }                
        } else {
            return false;
        }
    }
}