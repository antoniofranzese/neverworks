package it.neverworks.aop;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Hatching;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.UnimplementedMethodException;

/*
 * Rappresenta il contesto dell'invocazione di una catena di interceptor
 * su di una specifica istanza e con uno specifico insieme di argomenti.
 * Ogni singolo interceptor e' responsabile della propagazione dell'invocazione
 * all'interceptor successivo.
 * Esiste una specifica istanza dell'Invocation per ogni passo della catena.
 * Ogni interceptor deve obbligatoriamente richiamare invoke(), se intende proseguire
 * nell'invocazione, oppure decline(), se non intende andare oltre.
 */
public class Invocation implements Hatching {

    // L'istanza su cui si lancia l'invocazione
    protected Object instance;
    
    // La catena di interceptor da invocare
    protected List<Interceptor> chain;
    
    // Gli argomenti specifici della chiamata
    protected Object[] arguments;
    
    // Contenitore di informazioni condiviso dagli interceptor
    protected Map<String, Object> context;
    
    // Inteceptor corrente
    protected int index;
    
    // Indica che l'invocazione e' stata avviata
    protected boolean launched;
    
    // Indica che l'interceptor ha stabilito come continuare l'invocazione
    protected boolean performed;
    
    public Invocation( Object instance ) {
        super();
        this.instance = instance;
        this.chain = new ArrayList<Interceptor>();
        this.context = null;
        this.arguments = new Object[]{};
        this.index = 0;
        this.launched = false;
        this.performed = false;
    }

    protected Invocation() {
        super();
    }
    
    protected Invocation init( Object instance, List<Interceptor> chain, Object[] arguments, Map<String, Object> context, int index ) {
        this.instance = instance;
        this.chain = chain;
        this.context = context;
        this.index = index;
        this.launched = true;
        this.performed = false;
        setArguments( arguments );
        return this;
    }
    
    protected void setArguments( Object[] arguments ) {
        this.arguments = arguments;
        for( Object argument: this.arguments ) {
            if( argument instanceof InvocationAware ) {
                ((InvocationAware) argument).storeInvocation( this );
            }
        }
    }

    public Map<String, Object> context() {
        if( this.context == null ) {
            this.context = new HashMap<String, Object>();
        }
        return this.context;
    }
    
    public FluentList<Object> arguments() {
        return new FluentArrayList<Object>( this.arguments );
    }

    public <T> T argument( int index ) {
        if( index < this.arguments.length ) {
            return (T) this.arguments[ index ];
        } else {
            throw new IllegalArgumentException( "Invocation argument index out of bounds: " + index );
        }
    }
    
    public Invocation setArgument( int index, Object value ) {
        if( index < this.arguments.length ) {
            this.arguments[ index ] = value;
            return this;
        } else {
            throw new IllegalArgumentException( "Invocation argument index out of bounds: " + index );
        }
    }
    
    public <T> T instance() {
        return (T) this.instance;
    }
    
    public Invocation add( Interceptor interceptor ) {
        if( !launched ) {
            this.chain.add( interceptor );
            return this;
        } else {
            throw new AopException( "Cannot add interceptor, invocation already launched" );
        }
    }

    public Invocation add( Iterable<Interceptor> interceptors ) {
        if( interceptors != null ) {
            for( Interceptor interceptor: interceptors ) {
                this.add( interceptor );
            }
        }
        return this;
    }
    
    public Object launch( Object... arguments ) {
        if( !launched ) {
            if( chain.size() > 0 ) {
                setArguments( arguments );
                this.launched = true;
                //System.out.println( "Invoke on launch " + this.chain.get( 0 ) );
                Object result = this.chain.get( 0 ).invoke( this );
                if( !performed && chain.size() > 1 ) {
                    throw new MissingInvocationException(  chain.get( 0 ).getClass().getName() + " interceptor must call invoke() or decline() on invocation argument" );
                }
                return result;
            } else {
                return null;
            }
        } else {
            throw new RuntimeException( "Cannot launch, invocation already launched" );
        }
    }
    
    public Object invoke() {
        //System.out.println( "Invoke on invoke " + String.valueOf( index ) + " " + this.chain.get( index + 1 ) );
        return invoke( this.arguments );
    }
    
    public Object invoke( Object... arguments ) {
        this.performed = true;
        int nextIndex = this.index + 1;

        Invocation nextInvocation = Objects.hatch( this );
        nextInvocation.init( this.instance, this.chain, arguments, this.context, nextIndex );

        Object result = this.chain.get( nextIndex ).invoke( nextInvocation );

        if( !nextInvocation.performed && nextIndex < this.chain.size() - 1 ) {
            throw new MissingInvocationException( this.chain.get( nextIndex ).getClass().getName() + " interceptor must call invoke() or decline() on invocation argument"  );
        }
        return result;
    }

    public Object hatch() {
        return new Invocation();
    }
    
    public void decline() {
        this.performed = true;
    }
    
    public boolean terminated() {
        return this.index >= this.chain.size() - 1;
    }
}