package it.neverworks.aop;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

public class InterceptProcessor implements MethodProcessor {
    
    private Class<? extends Interceptor> interceptorClass;
    
    public InterceptProcessor( AnnotationInfo<Intercept> info ) {
        this.interceptorClass = info.annotation().value();
    }
    
    public void processMethod( MethodDescriptor method ) {
        Interceptor interceptor = (Interceptor) Reflection.newInstance( this.interceptorClass );
        method.place( interceptor ).first();
    }
    
}
