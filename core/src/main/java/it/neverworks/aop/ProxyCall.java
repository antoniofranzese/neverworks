package it.neverworks.aop;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.Errors;

public class ProxyCall implements Interceptor {
    
    private Proxy proxy;
    
    public ProxyCall( Proxy proxy ) {
        this.proxy = proxy;
    }
    
    public Object invoke( Invocation invocation ) {
        return this.proxy.invoke( invocation.instance(), invocation.arguments().toArray() );
    }
}