package it.neverworks.aop;

import it.neverworks.model.ModelException;

public class AopException extends ModelException {
    
    public AopException( String message ) {
        super( message );
    }

}