package it.neverworks.aop;

public interface Interceptor {
    
    Object invoke( Invocation invocation );
    
}