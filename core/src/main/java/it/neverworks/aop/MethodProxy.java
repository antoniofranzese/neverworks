package it.neverworks.aop;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Errors;

/*
 * Proxy rappresentante di un metodo che non necessita di intercettazione
 */
public class MethodProxy implements Proxy {
    
    protected Method method;
    protected Object staticInstance;
    
    public MethodProxy( Method method ) {
        if( method != null ) {
            this.method = method;
            Reflection.makeAccessible( method );
        } else {
            throw new NullPointerException( "Null method in " + this.getClass().getName() );
        }
    }

    public MethodProxy( Object instance, Method method ) {
        this( method );
        if( instance != null ) {
            this.staticInstance = instance;
        } else {
            throw new NullPointerException( "Null instance in " + this.getClass().getName() );
        }
    }
    
    public Object invoke( Object instance, Object... params ) {
        try {
            return this.method.invoke( this.staticInstance != null ? this.staticInstance : instance, params );
        } catch( InvocationTargetException ex ) {
            throw Errors.wrap( ex );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public boolean equals( Object other ) {
        if( other instanceof MethodProxy ) {
            MethodProxy o = (MethodProxy) other;
            if( ! this.method.equals( o.method ) ) {
                return false;
            } else if( this.staticInstance != null ? this.staticInstance != o.staticInstance : o.staticInstance != null ) {
                return false;
            } else {
                return true;
            }                
        } else {
            return false;
        }
    }
    
    public String toString() {
        return this.method.getName() + " method proxy";
    }
    
}