package it.neverworks.aop;

import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Method;

/*
 * Poincut che si conclude automaticamente con una chiamata al metodo originario.
 * Mantiene sempre il metodo in coda alla catena.
 */
public class MethodPointcut extends Pointcut {
    
    public MethodPointcut( Method method ) {
        super();
        this.chain.add( new MethodCall( method ) );
    }
    
    public Pointcut add( Interceptor interceptor ) {
        if( interceptor != null ) {
            this.chain.add( this.chain.size() - 1, interceptor );
        }
        return this;
    }
    
    public int size() {
        return this.chain.size() - 1;
    }
}