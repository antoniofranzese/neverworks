package it.neverworks.aop;

import java.util.List;
import java.util.ArrayList;
import java.lang.reflect.Method;

/*
 * Proxy rappresentante di una catena di Interceptor
 */
public class Pointcut implements Proxy {
    
    protected List<Interceptor> chain;
    
    public Pointcut() {
        this.chain = new ArrayList<Interceptor>();
    }
    
    public Pointcut( Iterable<Interceptor> interceptors ) {
        this();
        if( interceptors != null ) {
            for( Interceptor interceptor: interceptors ) {
                this.add( interceptor );
            }
        }
    }

    public Pointcut add( Interceptor interceptor ) {
        if( interceptor != null ) {
            this.chain.add( interceptor );
        }
        return this;
    }

    public Pointcut add( Iterable<Interceptor> interceptors ) {
        if( interceptors != null ) {
            for( Interceptor interceptor: interceptors ) {
                if( interceptor != null ) {
                    add( interceptor );
                }
            }
        }
        return this;
    }
    
    public Object invoke( Object instance, Object... arguments ) {
        return ( instance instanceof Invocation ? (Invocation) instance : new Invocation( instance ) )
            .add( this.chain )
            .launch( arguments );
    }
    
    public int size() {
        return this.chain.size();
    }
    
    public boolean equals( Object other ) {
        if( other instanceof Pointcut ) {
            Pointcut o = (Pointcut) other;
            if( this.chain.size() != o.chain.size() ) {
                return false;
            } else {
                for( int i = 0; i < this.chain.size(); i++ ) {
                    if( ! this.chain.get( i ).equals( o.chain.get( i ) ) ) {
                        return false;
                    }
                }
                return true;
            }
        } else {
            return false;
        }
    }
    
}