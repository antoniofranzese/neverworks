package it.neverworks;

import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Enumeration;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.joda.time.DateTime;

import it.neverworks.lang.Pair;
import it.neverworks.lang.Tuple;
import it.neverworks.lang.Dates;
import it.neverworks.lang.Dates;
import it.neverworks.lang.Range;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Undefined;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.FormattingWriter;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Functional;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Calculator;
import it.neverworks.lang.Guard;
import it.neverworks.lang.Work;
import it.neverworks.lang.ReturningWork;
import it.neverworks.lang.Threads;
import it.neverworks.lang.SynchronizedPredicate;
import it.neverworks.i18n.Translator;
import it.neverworks.i18n.Translation;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.Template;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.Model;
import it.neverworks.model.Hook;
import it.neverworks.model.Value;
import it.neverworks.text.TextFormat;
import it.neverworks.log.Console;

@SuppressWarnings( "unchecked" )
public class language  {

    /* lang */

    public static <T> Collections.FunctionalBuilder<T> each( Class<T> target ) {
        return new Collections.FunctionalBuilder<T>( target );
    }

    public static <T> Functional<T> each( Iterable<T> source ) {
        return Collections.<T>functional( source );
    }

    public static <T> Functional<T> each( Enumeration<T> source ) {
        return Collections.<T>functional( source );
    }

    public static <K,V> Functional<K> each( Map<K,V> source ) {
        return Collections.<K>toFunctional( source );
    }

    public static <T> Functional<T> each( T[] source ) {
        return Collections.<T>functional( source );
    }

    public static <T> Functional<T> each( Stream<T> source ) {
        return Collections.<T>functional( source );
    }

    public static Arguments arg( String key, Object value ) {
        return new Arguments().arg( key, value );
    }

    public static <K,V> FluentMap<K,V> dict( Tuple... items ) {
        return new FluentHashMap<K,V>().cat( items );
    }

    public static <T> FluentList<T> list( T... items ) {
        return new FluentArrayList<T>().cat( items );
    }

    public static <T> FluentList<T> args( T... items ) {
        return list( items );
    }

    public static <T> T[] array( Class<T> type, Iterable<T> items ) {
        return Collections.toArray( type, items );
    }
    
    /* model */
    
    public static <T> ObjectQuery<T> query( Iterable<T> iterable ) {
        return new ObjectQuery<T>( iterable );
    }

    public static <T> ObjectQuery<T> query( Class<T> cls ) {
        return new ObjectQuery<T>( cls );
    }
    
    public static <T> ObjectQuery<T> query( T[] array ) {
        return new ObjectQuery<T>( array );
    }

    public static ObjectQuery query( Object source) {
        return Value.queryOf( source );
    }

    public static Object eval( Object root, String expression ) {
        return ExpressionEvaluator.evaluate( root, expression );
    }

    public static ExpressionEvaluator.EvaluateFunction<Object> eval( String expression ) {
        return ExpressionEvaluator.evaluate( expression );
    }

    public static void assign( Object root, String expression, Object value ) {
        ExpressionEvaluator.valorize( root, expression, value );
    }
    
    public static boolean knows( Object root, String expression ) {
        return ExpressionEvaluator.knows( root, expression );
    }
    
    public static Value hook( Object root, String name ) {
        return Hook.of( root, name );
    }
    
    public static Value hook( Wrapper wrapper ) {
        return Hook.of( wrapper );
    }
    

    /* i18n */
    
    public static String T( String message ) {
        return Translation.translator().translate( message );
    }

    public static String M( String message, Object... params ) {
        Translator t = Translation.translator();
        return TextFormat.format( t.current(), t.translate( message ), params );
    }

    public static String F( String message, Object... params ) {
        Translator t = Translation.translator();
        return new Formatter( t.current() ).format( t.translate( message ), params ).toString();
    }
    
    /* types */
    
    public static <T> T convert( Object source, Class<T> target ) {
        return (T) TypeFactory.shared( target ).convert( source );
    }
    
    public static <T> TypeDefinition<T> type( Class<T> target ) {
        return TypeFactory.shared( target );
    }

    public static TypeDefinition type( String target ) {
        return TypeFactory.shared( target );
    }

    /* output */

    public static Console console = new Console();
    
    private static FormattingWriter out = new FormattingWriter( System.out );
    private static FormattingWriter err = new FormattingWriter( System.err );

    public static void print( Object... objects ) {
        out.print( objects );
    }

    public static void println( Object... objects ) {
        out.println( objects );
    }

    public static void printm( String message, Object... objects ) {
        out.printm( message, objects );
    }

    public static void printf( String format, Object... objects ) {
        out.printf( format, objects );
    }

    public static void printt( String template, Object root ) {
        out.print( new Template( template ).apply( root ) );
    }
    
    public static String repr( Object object ) {
        return Objects.repr( object );
    }
    
    public static String msg( String message, Object... params ) {
        return TextFormat.format( message, Value.from( params ) );
    }
    public static String msg( Locale locale, String message, Object... params ) {
        return TextFormat.format( locale, message, Value.from( params ) );
    }
    
    public static String str( Object object ) {
        return Strings.safe( Strings.valueOf( object ) );
    }

    public static Date date() {
        return Dates.now();
    }

    public static DateTime datetime() {
        return Dates.datetime( Dates.now() );
    }

    public static Date date( Object object ) {
        return Dates.date( object );
    }

    public static DateTime datetime( Object object ) {
        return Dates.datetime( object );
    }
    
    /* Utilities */

    public static void rethrow( Exception ex ) {
        throw Errors.wrap( ex );
    }

    public static RuntimeException error( Exception ex ) {
        return Errors.wrap( ex );
    }
    
    public static void sleep( Number timeout ) {
        Threads.sleep( timeout );
    }    
    
    public static boolean empty( Object object ) {
        return Value.isEmpty( object );
    }
    
    public static int len( Object object ) {
        return Value.size( object );
    }

    public static Range range() {
        return Range.of( 0 );
    }

    public static Range range( Object object ) {
        return Range.of( object );
    }

    public static Range range( Object start, Object stop ){
        return Range.of( start, stop );
    }

    public static Tuple tuple( Object... values ) {
        return Tuple.of( values );
    }

    public static Pair pair( Object value1, Object value2 ) {
        return Pair.of( value1, value2 );
    }

    public static Calculator calc() {
        return new Calculator();
    }
    
    public static Calculator calc( Object value ) {
        return new Calculator().push( value );
    }
    
    public final static Undefined undefined = Undefined.value();
    
    public static boolean undefined( Object obj ) {
        return Undefined.matches( obj );
    }
    
    public static Class This() {
        return Reflection.findClass( Thread.currentThread().getStackTrace()[ 2 ].getClassName() );
    }

    public static <T> void within( Guard<T> guard, Work<T> work ) {
        if( guard != null && work != null ) {
            guard.surround( work );
        }
    }

    public static <T,O> O from( Guard<T> guard, ReturningWork<T,O> work ) {
        if( guard != null && work != null ) {
            return guard.extract( work );
        } else {
            return null;
        }
    }
    
    public static <T> Guard<T> sync( T target, Predicate<T> predicate ) {
        return new SynchronizedPredicate<T>( target, predicate );
    }

    public static <T> void sync( T target, Predicate<T> predicate, Work<T> work ) {
        within( sync( target, predicate ), work );
    }
    
    /* Numbers */
    
    public static TypeDefinition<Boolean> Bool() {
        return type( Boolean.class );
    }

    public static TypeDefinition<Byte> Byte() {
        return type( Byte.class );
    }

    public static TypeDefinition<Short> Short() {
        return type( Short.class );
    }

    public static TypeDefinition<Integer> Int() {
        return type( Integer.class );
    }

    public static TypeDefinition<Long> Long() {
        return type( Long.class );
    }

    public static TypeDefinition<Float> Float() {
        return type( Float.class );
    }

    public static TypeDefinition<Double> Double() {
        return type( Double.class );
    }

    public static TypeDefinition<BigDecimal> Decimal() {
        return type( BigDecimal.class );
    }

    public static Boolean NBool( Object number ) {
        return Booleans.NBoolean( number );
    }
    
    public static boolean Bool( Object number ) {
        return Booleans.Boolean( number );
    }

    public static Boolean NBoolean( Object number ) {
        return Booleans.NBoolean( number );
    }
    
    public static boolean Boolean( Object number ) {
        return Booleans.Boolean( number );
    }
    
    public static Byte NByte( Object number ) {
        return Numbers.NByte( number );
    }
    
    public static byte Byte( Object number ) {
        return Numbers.Byte( number );
    }

    public static Short NShort( Object number ) {
        return Numbers.NShort( number );
    }
    
    public static short Short( Object number ) {
        return Numbers.Short( number );
    }

    public static Integer NInt( Object number ) {
        return Numbers.NInt( number );
    }
    
    public static int Int( Object number ) {
        return Numbers.Int( number );
    }

    public static Integer NInteger( Object number ) {
        return Numbers.NInt( number );
    }
    
    public static int Integer( Object number ) {
        return Numbers.Int( number );
    }

    public static Long NLong( Object number ) {
        return Numbers.NLong( number );
    }
    
    public static long Long( Object number ) {
        return Numbers.Long( number );
    }

    public static Float NFloat( Object number ) {
        return Numbers.NFloat( number );
    }
    
    public static float Float( Object number ) {
        return Numbers.Float( number );
    }

    public static Double NDouble( Object number ) {
        return Numbers.NDouble( number );
    }
    
    public static double Double( Object number ) {
        return Numbers.Double( number );
    }

    public static BigDecimal NDecimal( Object number ) {
        return Numbers.NDecimal( number );
    }
    
    public static BigDecimal Decimal( Object number ) {
        return Numbers.Decimal( number );
    }
    
    public static BigDecimal NDecimal( Object number, int scale ) {
        return Numbers.NDecimal( number, scale );
    }
    
    public static BigDecimal Decimal( Object number, int scale ) {
        return Numbers.Decimal( number, scale );
    }

    public static BigDecimal NDecimal( Object number, int scale, Object rounding ) {
        return Numbers.NDecimal( number, scale, type( RoundingMode.class ).process( rounding ) );
    }
    
    public static BigDecimal Decimal( Object number, int scale, Object rounding ) {
        return Numbers.Decimal( number, scale,type( RoundingMode.class ).process( rounding ) );
    }
    
}