package it.neverworks.crypto;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.StringConverter;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.description.Setter;

import javax.crypto.spec.DESKeySpec;
import javax.crypto.SecretKeyFactory;
import javax.crypto.SecretKey;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;

public class Cipher implements Model {
    
    @AutoConvert
    public static enum Encoding {
        HEX, BASE64
    }
    
    @AutoConvert
    public static enum Algorithm {
        AES, DES, RSA
    }
    
    @Property
    private Encoding encoding = Encoding.HEX;
    
    @Property
    private Algorithm algorithm = Algorithm.DES; 
    
    private SecretKey key = null;

    public Cipher() {
        this( Strings.generateHexID( 128 ) );
    }
    
    public Cipher( String key ) {
        super();
        set( "key", key );
    }
    
    public Cipher( Arguments arguments ) {
        this();
        set( arguments );
    }

    public String encrypt( String value ) {
        try {
            byte[] clear = Strings.safe( value ).getBytes( "UTF8" );      

            javax.crypto.Cipher cipher = createJavaCipher(); 
            cipher.init( javax.crypto.Cipher.ENCRYPT_MODE, this.key );
            byte[] encrypted = cipher.doFinal( clear );
        
            switch( this.encoding ) {
                case BASE64:
                    return new String( Base64.encodeBase64( encrypted ) );
                default:
                    return new String( new Hex().encode( encrypted ) );
            }

        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public String decrypt( String value ) {
        try {
            byte[] encrypted;
            switch( this.encoding ) {
                case BASE64:
                    encrypted = Base64.decodeBase64( Strings.safe( value ).getBytes( "UTF8" ) );
                    break;
                default:
                    encrypted = new Hex().decode( Strings.safe( value ).getBytes( "UTF8" ) );
            }

            javax.crypto.Cipher decipher = createJavaCipher();
            decipher.init( javax.crypto.Cipher.DECRYPT_MODE, this.key );
            return new String( decipher.doFinal( encrypted ) );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public boolean canDecrypt( String value ) {
        try {
            decrypt( value );
            return true;
        } catch( Exception ex ) {
            return false;
        }
    }
    
    protected javax.crypto.Cipher createJavaCipher() {
        try {
            switch( this.algorithm ) {
                case AES:
                    return javax.crypto.Cipher.getInstance( "AES" );
                case RSA:
                    return javax.crypto.Cipher.getInstance( "RSA" );
                default:
                    return javax.crypto.Cipher.getInstance( "DES" );
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    protected void setAlgorithm( Setter<Algorithm> setter ) {
        if( setter.value() != null ) {
            setter.set();
        } else {
            setter.set( Algorithm.DES );
        }
    }
    
    @Virtual
    @Convert( StringConverter.class )
    public void setKey( String key ) {
        try {
            DESKeySpec keySpec = new DESKeySpec( Strings.safe( key ).getBytes( "UTF8" ) );
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance( "DES" );
            this.key = keyFactory.generateSecret( keySpec );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public Encoding getEncoding(){
        return this.encoding;
    }
    
    public void setEncoding( Encoding encoding ){
        this.encoding = encoding;
    }
    
    public Algorithm getAlgorithm(){
        return this.algorithm;
    }
    
    public void setAlgorithm( Algorithm algorithm ){
        this.algorithm = algorithm;
    }
    
}