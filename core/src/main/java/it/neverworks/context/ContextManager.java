package it.neverworks.context;

import it.neverworks.lang.Guard;
import it.neverworks.model.Value;

public interface ContextManager {

    public <T> T get( String name );
    public <T> T get( Class<T> type );
    public int count( Class<?> type );
    public boolean has( String name );
    public boolean has( Class type );
    public void shutdown();
    public TrailManager getTrailManager();
    public void registerLocalContextComponent( LocalContextComponent component );
    public void registerEventListener( EventListener listener );
    public LocalContext backupLocalContext();
    public void restoreLocalContext( LocalContext local );
    public void cleanupLocalContext( LocalContext local );
    public void subscribeShutdown( Runnable runnable );
    public void publish( ApplicationEvent event );
    public ContextManager set( String name, Object value );
    public <T> T call( String name, Object... arguments );
    
    default Value value( String path ) {
        return Value.of( this.get( path ) );
    }
}