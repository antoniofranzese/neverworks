package it.neverworks.context;

public interface LocalContextComponent {
    public void backup( LocalContext context );
    public void restore( LocalContext context );
    public void cleanup( LocalContext context );
}