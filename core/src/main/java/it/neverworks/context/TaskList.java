package it.neverworks.context;

import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.WeakReference;

public class TaskList {
    
    private List<WeakReference<Task>> tasks;

    public TaskList() {
        this.tasks = new ArrayList<WeakReference<Task>>();
    }

    public Task enlist( Task task ) {
        this.tasks.add( new WeakReference<Task>( task ) );
        return task;
    }

    public synchronized TaskList collect() {
        List<WeakReference<Task>> collected = new ArrayList<WeakReference<Task>>();
        
        for( WeakReference<Task> taskRef: this.tasks ) {
            Task task = taskRef.get();
            if( task != null && task.isAlive() ) {
                collected.add( taskRef );
            }    
        }

        this.tasks = collected;
        return this;
    }

    public TaskList shutdown() {
       System.out.println( "Shuttin down");
       for( WeakReference<Task> taskRef: this.tasks ) {
            Task task = taskRef.get();
            if( task != null ) {
                System.out.println( "Shutting " + task );
                task.stop();
            }    
        }
        return this;
    }

    @Override
    public void finalize() {
         this.shutdown();
    }

}