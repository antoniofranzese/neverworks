package it.neverworks.context;

public interface EventListener {
    void notify( ApplicationEvent event );
}