package it.neverworks.context;

import java.lang.reflect.Method;
import it.neverworks.log.Logger;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;

class TrailInterceptor implements Interceptor, MethodProcessor {
    
    private final static Logger logger = Logger.of( "TRAILS" );
    
    private String name;
    private Method frontMethod;
    private boolean trans = false;
    
    public TrailInterceptor( AnnotationInfo<Trail> info ) {
        this.name = Strings.safe( info.annotation().value() ).trim();
        this.trans = Strings.isEmpty( name );
    }
    
    public void processMethod( MethodDescriptor method ) {
        if( Strings.isEmpty( this.name ) ) {
            throw new RuntimeException( "Empty trail name on " + Reflection.fullName( frontMethod ) );
        }
        method.place( this ).first();
        this.frontMethod = method.front();
    }

    public Object invoke( Invocation invocation ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "Switching to trail ''{}'' for {}", name, Reflection.fullName( frontMethod ) );
        }

        TrailContext ctx = null;
        if( this.trans ) {
            ctx = Trails.open();
        } else {
            Trails.select( name );
        }

        try {
            return invocation.invoke();
        } finally {
            if( logger.isDebugEnabled() ) {
                logger.debug( "Leaving trail ''{}'' for {}", name, Reflection.fullName( frontMethod ) );
            }

            if( this.trans ) {
                ctx.close();
            } else {
                Trails.deselect();
            }
        }

    }
    
    public void setName( String name ){
        this.name = name;
    }
    
}