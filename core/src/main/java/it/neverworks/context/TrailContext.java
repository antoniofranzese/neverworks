package it.neverworks.context;

import java.io.Closeable;
import it.neverworks.lang.Guard;

public class TrailContext implements Guard<TrailManager>, Closeable {
    
    private String id;
    private boolean closed;
    private TrailManager manager;
    
    public TrailContext( TrailManager manager, String id ) {
        this.manager = manager;
        this.id = id;
    }
    
    public TrailManager enter() {
        return this.manager;
    }
    
    public Throwable exit( Throwable error ){
        this.close();
        return error;
    }
    
    public void close() {
        if( ! this.closed ) {
            synchronized( this ) {
                if( ! this.closed ) {
                    this.manager.close( this );
                }
            }
        }
    }
    
    public String getId(){
        return this.id;
    }
    
}