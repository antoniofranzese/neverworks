package it.neverworks.context;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
public @interface Component {
    String name() default "";
    String scope() default "";
    boolean lazy() default false;
    String init() default "";
    String destroy() default "";
}
