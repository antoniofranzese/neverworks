package it.neverworks.context;

import it.neverworks.model.events.Event;
import it.neverworks.model.utils.ExpandoModel;

public class TaskEvent extends Event {
    
    private Task task;

    public TaskEvent( Task task ) {
        super();
        this.task = task;
    }
    
    public <T extends Task> T getTask() {
        return (T) this.task;
    }

    public ExpandoModel getAttributes() {
        return getTask().getAttributes();
    }
    
    public Exception getError() {
        return getTask().getError();
    }
    
    public <T> T own( T object ) {
        return getTask().own( object );
    }

    public boolean isRunning() {
        return getTask().isRunning();
    }

    // public void sleep( Number number ) {
    //     getTask().sleep( number );
    // }
}