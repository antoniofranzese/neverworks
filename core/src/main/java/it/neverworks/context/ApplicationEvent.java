package it.neverworks.context;

import it.neverworks.lang.Strings;
import it.neverworks.model.Value;
import it.neverworks.model.Property;
import it.neverworks.model.Modelize;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.ModelInstanceAware;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Describe;
import it.neverworks.model.features.Get;
import it.neverworks.model.utils.ExpandoModel;

@Modelize
public class ApplicationEvent extends org.springframework.context.ApplicationEvent implements ModelInstanceAware, Get, Retrieve, Store, Inspect, Describe {
    
    public ApplicationEvent( Object source ) {
        super( source );
    }
    
    @Property
    private ExpandoModel attributes;
    
    public ExpandoModel getAttributes(){
        return this.attributes;
    }
    
    public void setAttributes( ExpandoModel attributes ){
        this.attributes = attributes;
    }
    /* Model */
    
    protected ModelInstance modelInstance;
    
    public ModelInstance model() {
        return this.modelInstance;
    }
    
    public ModelInstance retrieveModelInstance() {
        return this.modelInstance;
    }
    
    public void storeModelInstance( ModelInstance instance ) {
        this.modelInstance = instance;
    }
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( name );
    }

    public <T> T get( String name, T defaultValue ) {
        T value = (T)this.modelInstance.eval( name );
        return value != null ? value : defaultValue;
    }
    
    public <T extends ApplicationEvent> T set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }
    
    public Object retrieveItem( Object key ) {
        return this.modelInstance.get( Strings.valueOf( key ) );
    }
    
    public void storeItem( Object key, Object value ) {
        this.modelInstance.set( Strings.valueOf( key ), value );
    }
    
    public boolean containsItem( Object key ) {
        return this.modelInstance.has( Strings.valueOf( key ) );
    }
    
}