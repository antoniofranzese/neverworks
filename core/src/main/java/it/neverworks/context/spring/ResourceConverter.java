package it.neverworks.context.spring;

import org.springframework.core.io.Resource;
import it.neverworks.model.converters.Converter;
import it.neverworks.context.Context;

public class ResourceConverter implements Converter {

    public Object convert( Object value ) {
        if( value instanceof Resource ) {
            return value;

        } else if( value instanceof String ) {

            if( Context.getManager() instanceof SpringContextManager ) {
                return ((SpringContextManager) Context.getManager() ).getResource( (String) value );
            } else {
                return value;
            }

        } else {
            return value;
        }
    }

}