package it.neverworks.context.spring;

import static it.neverworks.language.*;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import it.neverworks.lang.Strings;
import it.neverworks.context.TrailContext;
import it.neverworks.context.TrailManager;
import it.neverworks.context.TrailDef;
import it.neverworks.log.Logger;

public class SpringTrailManager implements TrailManager {
    
    private static final Logger logger = Logger.of( "TRAILS" );

    private Map<String, TrailStorage> trails = new HashMap<String, TrailStorage>();
    
    public TrailContext open() {
        String name = null;
        do {
            name = "transient:" + Strings.generateHexID( 64 );   
        } while( trails.containsKey( name ) );
        
        trails.put( name, new TrailStorage( name ) );
        select( name );
        
        return new TrailContext( this, name );
    }
    
    public void close( TrailContext context ) {
        if( trails.containsKey( context.getId() ) ) {
            synchronized( trails ) {
                TrailStorage storage = trails.get( context.getId() );
                if( storage == TrailScope.current() ) {
                    deselect();
                }
                storage.shutdown();
                trails.remove( context.getId() );
            }
        } else {
            throw new IllegalArgumentException( msg( "Unknown trail: {0}", context.getId() ) );
        }
    }

    public void select( String name ) {
        if( ! trails.containsKey( name ) ) {
            synchronized( trails ) {
                if( ! trails.containsKey( name ) ) {
                    trails.put( name, new TrailStorage( "system:" + name ) );
                }
            }
        }
        TrailScope.push( trails.get( name ) );
    }
    
    public void deselect() {
        TrailScope.pop();
    }
    
    public TrailDef current() {
        TrailStorage storage = TrailScope.current();
        if( storage != null ) {
            return new TrailDef( storage.getId() );
        } else {
            return null;
        }
    }
    
    public void shutdown() {
        synchronized( trails ) {
            for( TrailStorage storage: trails.values() ) {
                storage.shutdown();
            }
            trails.clear();
        }
    }

    public void setup( Map<String,List<Runnable>> setup ) {
        if( setup != null ) {
            for( String name: setup.keySet() ) {
                if( setup.get( name ) != null ) {
                    this.select( name );
                    for( Runnable starter: setup.get( name ) ) {
                        starter.run();
                    }
                    this.deselect();
                }
            }
        }
    }

}