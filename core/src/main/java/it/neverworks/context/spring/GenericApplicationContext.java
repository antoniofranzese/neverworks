package it.neverworks.context.spring;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import it.neverworks.context.Context;

public class GenericApplicationContext extends org.springframework.context.support.GenericApplicationContext {
    
    public GenericApplicationContext() {
        super();
        new Context().setApplicationContext( this );
    }
    
}