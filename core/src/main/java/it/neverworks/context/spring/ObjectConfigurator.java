package it.neverworks.context.spring;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.FactoryBean;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.context.Trails;

public class ObjectConfigurator implements FactoryBean, InitializingBean, DisposableBean {
    
    private Object target;
    private String initMethod;
    private boolean singleton = true; 
    private String trail;
    private String destroyMethod;
    
    public Object getObject() {
        return target;
    } 
    
    public Class getObjectType() {
        return target.getClass();
    }
    
    public boolean isSingleton() {
        return singleton;
    }

    public void afterPropertiesSet() throws Exception {
        if( Strings.hasText( trail ) ) {
            Trails.select( trail );
        }

        try {
            if( Strings.hasText( initMethod ) ) {
                Reflection.invokeMethod( target, Reflection.findMethod( target, initMethod ) );
            } else if( target instanceof InitializingBean ) {
                ((InitializingBean) target).afterPropertiesSet();
            }

        } finally {
            if( Strings.hasText( trail ) ) {
                Trails.deselect();
            }
        }

    }
    
    public void destroy() throws Exception {
        if( Strings.hasText( trail ) ) {
            Trails.select( trail );
        }

        try {
            if( Strings.hasText( destroyMethod ) ) {
                Reflection.invokeMethod( target, Reflection.findMethod( target, destroyMethod ) );
            } else if( target instanceof DisposableBean ) {
                ((DisposableBean) target).destroy();
            }

        } finally {
            if( Strings.hasText( trail ) ) {
                Trails.deselect();
            }
        }

    }
    
    public void setTrail( String trail ){
        this.trail = trail;
    }
    
    public void setSingleton( boolean singleton ){
        this.singleton = singleton;
    }
    
    public void setInitMethod( String initMethod ){
        this.initMethod = initMethod;
    }
    
    public void setDestroyMethod( String destroyMethod ){
        this.destroyMethod = destroyMethod;
    }
    
    public void setTarget( Object target ){
        this.target = target;
    }
    
}