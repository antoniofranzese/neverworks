package it.neverworks.context.spring;

import javax.servlet.ServletRequestListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.http.HttpSessionListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import it.neverworks.log.Logger;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import it.neverworks.lang.Framework;
import it.neverworks.lang.Application;
import it.neverworks.context.spring.WebContextLoader;
import it.neverworks.app.ApplicationStartupEvent;
import it.neverworks.app.ApplicationShutdownEvent;
import it.neverworks.httpd.SessionCreatedEvent;
import it.neverworks.httpd.SessionDestroyedEvent;
import it.neverworks.httpd.j2ee.HttpSessionMapAdapter;

public class J2EEContextListener extends ContextLoaderListener implements ServletRequestListener, HttpSessionListener {

    private static final Logger logger;
    private final static String TRAIL_ATTRIBUTE;

    static {
        Framework.boot();
        logger = Logger.of( J2EEContextListener.class );
        TRAIL_ATTRIBUTE = TrailScope.class.getName();
    }

    //private WebApplicationContext wac = null;

    public J2EEContextListener() {
        super();
    }

    public J2EEContextListener( WebApplicationContext wac ) {
        super( wac );
    }
    
    public void requestInitialized( ServletRequestEvent event ) {
        HttpSession session = ((HttpServletRequest) event.getServletRequest()).getSession();
        
        if( logger.isTraceEnabled() ) {
            logger.trace( "[{}] Request init begin: {}", Thread.currentThread().getName(), session );
        }
        
        TrailStorage storage = (TrailStorage) session.getAttribute( TRAIL_ATTRIBUTE );
        
        //Tomcat session fixation management
        if( storage != null 
            && storage.getAttribute( "session" ) != null
            && ! session.getId().equals( storage.getAttribute( "session" ) ) ) {

            logger.debug( "Session ID changed, now {}, resetting storage", session.getId() );
            storage.shutdown();
            storage.setId( "session:" + session.getId() );
            storage.setAttribute( "session", session.getId() );
        }
        
        TrailScope.bind( storage );

        if( logger.isTraceEnabled() ) {
            logger.trace( "[{}] Request init end: {}", Thread.currentThread().getName(), session );
        }
        
    }
    
    public void requestDestroyed( ServletRequestEvent event ) {
        try {
            if( logger.isTraceEnabled() ) {
                logger.trace( "[{}] Request destroy begin: {}", Thread.currentThread().getName(), ((HttpServletRequest) event.getServletRequest()).getSession( false ) );
            }
            
            // ((HttpServletRequest) event.getServletRequest()).getSession().setAttribute( TRAIL_ATTRIBUTE, TrailScope.current() );
            TrailScope.bind( null );
            
            if( logger.isTraceEnabled() ) {
                logger.trace( "[{}] Request destroy end: {}", Thread.currentThread().getName(), ((HttpServletRequest) event.getServletRequest()).getSession( false ) );
            }
            
        } catch( Exception ex ) {
            ex.printStackTrace();

        } finally {
            TrailScope.cleanup();
        }
    }
    
    public void contextInitialized( ServletContextEvent event ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "[{}] Context init", Thread.currentThread().getName() );
        }
        
        System.setProperty( "web.root", event.getServletContext().getContextPath() );
        Application.setProperty( "web.root", event.getServletContext().getContextPath() );
        super.contextInitialized( event );
        // if( this.wac == null ) {
        //     this.wac = WebApplicationContextUtils.getRequiredWebApplicationContext( event.getServletContext() );
        // }
    }
    
    public void contextDestroyed( ServletContextEvent event ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "[{}] Context destroy", Thread.currentThread().getName() );
        }
        
        super.contextDestroyed( event );
    }
    
    public void sessionCreated( HttpSessionEvent event ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "[{}] Session init begin", Thread.currentThread().getName() );
        }
        String id = "session:" + event.getSession().getId();
        
        TrailStorage storage = TrailScope.current();
        if( storage != null ) {
            if( logger.isDebugEnabled() ) {
                logger.debug( "[{}] Renaming {} to {}",  Thread.currentThread().getName(), storage.getId(), id );
            }
            
            storage.setId( id );
        } else {
            if( logger.isDebugEnabled() ) {
                logger.debug( "[{}] New storage for {}",  Thread.currentThread().getName(), id );
            }
            storage = new TrailStorage( id );
        }
        storage.setAttribute( "session", event.getSession().getId() );
        event.getSession().setAttribute( TRAIL_ATTRIBUTE, storage );

        if( this.getCurrentWebApplicationContext() != null ) {
            this.getCurrentWebApplicationContext().publishEvent( 
                new SessionCreatedEvent( this, new HttpSessionMapAdapter( event.getSession() ) ) 
            );
        }

        if( logger.isDebugEnabled() ) {
            logger.debug( "[{}] Session init end", Thread.currentThread().getName() );
        }
        
    }
    
    public void sessionDestroyed( HttpSessionEvent event ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "[{}] Session destroy begin", Thread.currentThread().getName() );
        }
        
        boolean rebound = false;
        if( TrailScope.current() == null ) {
            TrailStorage storage = (TrailStorage) event.getSession().getAttribute( TRAIL_ATTRIBUTE );
            if( storage != null ) {
                TrailScope.bind( storage );
                rebound = true;
            }
        }
        
        if( this.getCurrentWebApplicationContext() != null ) {
            this.getCurrentWebApplicationContext().publishEvent( 
                new SessionDestroyedEvent( this, new HttpSessionMapAdapter( event.getSession() ) ) 
            );
        }

        if( rebound ) {
            TrailScope.cleanup();
        } else {
            TrailScope.replace( new TrailStorage( "invalid-session:" + event.getSession().getId() ) );
        }
        if( logger.isDebugEnabled() ) {
            logger.debug( "[{}] Session destroy end", Thread.currentThread().getName() );
        }
    }

    // @Override
    // protected ContextLoader createContextLoader(){
    //     return new WebContextLoader();
    // }

}