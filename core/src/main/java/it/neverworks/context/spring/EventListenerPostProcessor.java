package it.neverworks.context.spring;

import static it.neverworks.language.*;

import java.lang.reflect.Method;
import it.neverworks.lang.WeakReference;
import it.neverworks.lang.Reflection;
import it.neverworks.context.ApplicationEvent;
import it.neverworks.context.EventListener;
import it.neverworks.context.ContextException;
import it.neverworks.context.Listen;
import it.neverworks.log.Logger;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class EventListenerPostProcessor implements BeanPostProcessor {
    
    @Logger.ForMe private static Logger logger;
    private SpringContextManager manager;
    
    public EventListenerPostProcessor( SpringContextManager manager ) {
        this.manager = manager;
    }
    
    public Object postProcessAfterInitialization( Object bean, String beanName ) {
        return bean;
    } 
    
    public Object postProcessBeforeInitialization( Object bean, String beanName ) {
        for( Method method: Reflection.findMethods( bean.getClass() ) ) {
            Listen listen = (Listen)method.getAnnotation( Listen.class );
            if( listen != null ) {
                Class[] parameterTypes = method.getParameterTypes();
                
                for( Class<? extends ApplicationEvent> eventType: listen.value() ) {
                    if( eventType != null ) {
                        if( parameterTypes.length == 0 ) {
                            logger.debug( "Adding new listener on {0.name}: {1}", eventType, method );
                            this.manager.registerEventListener( new EmptyListener( eventType, bean, method ) );

                        } else if( parameterTypes.length == 1 && eventType.isAssignableFrom( parameterTypes[ 0 ] ) ) {
                            logger.debug( "Adding new listener on {0.name}: {1}", eventType, method );
                            this.manager.registerEventListener( new ArgumentListener( eventType, bean, method ) );

                        } else {
                            throw new ContextException( "Unusable " + eventType.getName() + " listener signature on " + method );
                        }
                    }
                }
            }
        }

        return bean;
    } 
    
    public static class ArgumentListener implements EventListener {
        
        protected Class<? extends ApplicationEvent> eventType;
        protected WeakReference beanRef;
        protected Method method;
        
        public ArgumentListener( Class<? extends ApplicationEvent> eventType, Object bean, Method method ) {
            this.eventType = eventType;
            this.beanRef = new WeakReference( bean );
            this.method = method;
        }                   
                            
        public void notify( ApplicationEvent event ) {
            if( this.eventType.isInstance( event ) ) {
                within( beanRef, bean -> {
                    invoke( bean, this.method, event );
                });
            }
        }
        
        protected void invoke( Object bean, Method method, ApplicationEvent event ) {
            Reflection.invokeMethod( bean, method, event );
        }
    
    }

    public static class EmptyListener extends ArgumentListener {
            
        public EmptyListener( Class<? extends ApplicationEvent> eventType, Object bean, Method method ) {
            super( eventType, bean, method );
        }
        
        protected void invoke( Object bean, Method method, ApplicationEvent event ) {
            Reflection.invokeMethod( bean, method );
        }
    }

}