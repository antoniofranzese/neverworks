package it.neverworks.context.spring;

import java.util.Map;
import java.util.HashMap;
import it.neverworks.context.LocalContext;

public class LocalContextSnapshot implements LocalContext {
    
    private Map<String, Object> info;
    
    public LocalContextSnapshot( Map info ) {
        this.info = info;
    }
    
    public LocalContextSnapshot() {
        this.info = new HashMap<String, Object>();
    }
    
    public <T> T get( String name ) {
        return (T) this.info.get( name );
    }
    
    public boolean has( String name ) {
        return this.info.containsKey( name );
    }
    
    public void put( String name, Object value ) {
        this.info.put( name, value );
    }
}