package it.neverworks.context.spring;

import it.neverworks.context.LocalContextComponent;
import it.neverworks.context.LocalContext;

public class TrailLocalContextComponent implements LocalContextComponent {
    
    private static final String TRAIL_ATTRIBUTE = TrailScope.class.getName();

    public void backup( LocalContext context ) {
        context.put( TRAIL_ATTRIBUTE, TrailScope.current() );
    }                                 
    
    public void restore( LocalContext context ) {
        //TODO: un meccanismo per evitare il trasferimento su richiesta
        if( context.has( TRAIL_ATTRIBUTE ) ) {
            TrailScope.bind( context.<TrailStorage>get( TRAIL_ATTRIBUTE  ) );
        }
    }
    
    public void cleanup( LocalContext context ) {
        if( context.has( TRAIL_ATTRIBUTE ) ) {
            TrailScope.bind( null );
        }
    }

}