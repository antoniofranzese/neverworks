package it.neverworks.context.spring.scheduling;

import java.util.List;
import java.util.TimerTask;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.context.Trails;
import it.neverworks.aop.Proxy;
import it.neverworks.aop.Pointcut;
import it.neverworks.aop.MethodPointcut;
import it.neverworks.aop.Interceptor;

public class ScheduledTimerTask /* extends org.springframework.scheduling.timer.ScheduledTimerTask */ {
    
    private String trail = "ScheduledTimerTask";
    private List<Interceptor> interceptors;
    private boolean enabled = true;
    
    // public TimerTask getTimerTask() {
    //     if( enabled ) {
    //         TimerTask task = super.getTimerTask();
    //         Pointcut pointcut = new MethodPointcut( Reflection.findMethod( task, "run" ) );
    //         if( interceptors != null ) {
    //             for( Interceptor interceptor: interceptors ) {
    //                 pointcut.add( interceptor );
    //             }
    //         }
    //         return new TrailTimerTask( pointcut, task, this.trail );
    //     } else {
    //         return new TimerTask(){
    //             public void run(){}
    //         };
    //     }
    // }
    
    private static class TrailTimerTask extends TimerTask {
        
        private Proxy proxy;
        private TimerTask task;
        private String trail;
        
        public TrailTimerTask( Proxy proxy, TimerTask task, String trail ) {
            this.proxy = proxy;
            this.task = task;
            this.trail = trail;
        }
        
        public boolean cancel() {
            return this.task.cancel();
        }
        
        public void run() {
            if( Strings.hasText( this.trail ) ) {
                Trails.select( this.trail );
            }
            
            try {

                this.proxy.invoke( this.task );
            
            } finally {
                if( Strings.hasText( this.trail ) ) {
                    Trails.deselect();
                }
            }
        } 
        
        public long scheduledExecutionTime() {
            return this.task.scheduledExecutionTime();
        }
    }
    
    public String getTrail(){
        return this.trail;
    }
    
    public void setTrail( String trail ){
        this.trail = trail;
    }
    
    public List<Interceptor> getInterceptors(){
        return this.interceptors;
    }
    
    public void setInterceptors( List<Interceptor> interceptors ){
        this.interceptors = interceptors;
    }
    
    public boolean getEnabled(){
        return this.enabled;
    }
    
    public void setEnabled( boolean enabled ){
        this.enabled = enabled;
    }
    
}