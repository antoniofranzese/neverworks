package it.neverworks.context.spring;

import java.util.ArrayList;
import java.util.List;

import it.neverworks.log.Logger;
import org.springframework.core.io.Resource;

public class PropertyPlaceholderConfigurer extends org.springframework.beans.factory.config.PropertyPlaceholderConfigurer {
	private static final Logger logger = Logger.of( PropertyPlaceholderConfigurer.class );

	@Override
	public void setLocation( Resource location ) {
		setLocations( new Resource[] {location} );
	}

	@Override
	public void setLocations( Resource[] locations ) {
		List<Resource> resources = new ArrayList<Resource>();
		for( Resource resource : locations ) {
			if ( resource.exists() ) {
				resources.add( resource );
			}
		}
		Resource[] current = new Resource[ resources.size() ];
		current = resources.toArray( current );
		super.setLocations( current );
	}
	
}
