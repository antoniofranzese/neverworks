package it.neverworks.context.spring;

import javax.servlet.ServletContext;

import org.springframework.web.context.ContextLoader;
import org.springframework.context.ApplicationContextException;;

public class WebContextLoader extends ContextLoader {
    
    protected Class determineContextClass( ServletContext servletContext ) throws ApplicationContextException {
        return XmlWebApplicationContext.class;
    }
    
}