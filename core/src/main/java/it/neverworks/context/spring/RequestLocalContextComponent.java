package it.neverworks.context.spring;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestAttributes;
import it.neverworks.context.LocalContextComponent;
import it.neverworks.context.LocalContext;

public class RequestLocalContextComponent implements LocalContextComponent {
    
    private static final String REQUEST_ATTRIBUTE = RequestLocalContextComponent.class.getName() + ".request";

    public void backup( LocalContext context ) {
        context.put( REQUEST_ATTRIBUTE, RequestContextHolder.getRequestAttributes() );
    }                                 
    
    public void restore( LocalContext context ) {
        if( context.has( REQUEST_ATTRIBUTE ) ) {
            RequestContextHolder.setRequestAttributes( context.<RequestAttributes>get( REQUEST_ATTRIBUTE ) );
        }
    }
    
    public void cleanup( LocalContext context ) {
        if( context.has( REQUEST_ATTRIBUTE ) ) {
            RequestContextHolder.setRequestAttributes( null );
        }
    }

}