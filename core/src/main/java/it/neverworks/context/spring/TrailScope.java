package it.neverworks.context.spring;

import java.util.Stack;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;
import it.neverworks.log.Logger;

public class TrailScope implements Scope {

    private static final Logger logger = Logger.of( "TRAILS" );

    private static final ThreadLocal<TrailStorage> localStorage = new ThreadLocal<TrailStorage>();
    private static final ThreadLocal<Stack<TrailStorage>> storageStack = new ThreadLocal<Stack<TrailStorage>>();
    
    public Object resolveContextualObject( String key ) {
        // if (REFERENCE_REQUEST.equals(key)) {
        //     return this.request;
        // }
        // else if (REFERENCE_SESSION.equals(key)) {
        //     return getSession(true);
        // }
        // else {
            return null;
        // }
    }

    public Object get( String name, ObjectFactory objectFactory ) {
        TrailStorage storage = currentStorage();

        if( logger.isTraceEnabled() ) {
            logger.trace( "[{}] Requesting ''{}'' from {} trail", thread(), name, storage.getId() );
        }

        if( ! storage.contains( name ) ) {
            if( logger.isTraceEnabled() ) {
                logger.trace( "[{}] Creating new instance for ''{}''", thread(), name );
            }

            storage.put( name, objectFactory.getObject() );
        }

        Object result = storage.get( name );
        if( logger.isTraceEnabled() ) {
            logger.trace( "[{}] Returning: {}", thread(), result );
        }

        return result;
    }
    
    public String getConversationId() {
        return currentStorage().getId();
    }
    
    public void registerDestructionCallback( String name, Runnable callback ) {
        if( logger.isTraceEnabled() ) {
            logger.trace( "[{}] Registering destruction for ''{}'' in {} trail", thread(), name, currentStorage().getId() );
        }

        currentStorage().register( name, callback );
    }
    
    public Object remove( String name ) {
        if( logger.isTraceEnabled() ) {
            logger.trace( "[{}] Removing ''{}'' from {} trail", thread(), name, currentStorage().getId() );
        }

        return currentStorage().remove( name );
    }
    
    private TrailStorage currentStorage() {
        TrailStorage storage = localStorage.get();
        if( storage == null ) {
            if( logger.isDebugEnabled() ){
                logger.debug( "New trail storage for thread: {}", thread() );   
            }

            storage = new TrailStorage( "thread:" + thread() );
            localStorage.set( storage );
        }
        return storage;
    }
    
    private static String thread() {
        return Thread.currentThread().getName();
    }

    public static void push( TrailStorage storage ) {
        if( localStorage.get() != null ) {
            if( storageStack.get() == null ) {
                storageStack.set( new Stack<TrailStorage>() );
            }
            storageStack.get().push( localStorage.get() );
        }
        bind( storage );
    }
    
    public static void pop() {
        if( storageStack.get() != null && storageStack.get().size() > 0 ) {
            bind( storageStack.get().pop() );
        } else {
            bind( null );
        }
    }
    
    public static void bind( TrailStorage storage ) {
        if( logger.isTraceEnabled() ) {
            if( storage != null ) {
                logger.trace( "[{}] Binding {} trail", thread(), storage );
            } else {
                logger.trace( "[{}] Unbinding trail", thread() );
            }
        }

        localStorage.set( storage );
    }

    public static boolean bound() {
        return localStorage.get() != null;
    }
    
    public static TrailStorage current() {
        return localStorage.get();
    }

    public static void reset() {
        if( localStorage.get() != null ) {
            if( logger.isDebugEnabled() ) {
                logger.debug( "[{}] Resetting {} trail", thread(), localStorage.get().getId() );
            }

            localStorage.get().shutdown();

        } else {
            if( logger.isDebugEnabled() ) {
                logger.debug( "[{}] No current trail on reset", thread() );
            }

        }

        localStorage.set( null );
    }
    
    public static void replace( TrailStorage storage ) {
        reset();
        bind( storage );
    }
    
    public static void cleanup() {
        localStorage.remove();
        storageStack.remove();
    }
    
    public static void shutdown() {
        if( bound() ) {
            current().shutdown();
        }
    }
}