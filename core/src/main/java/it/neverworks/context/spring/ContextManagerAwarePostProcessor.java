package it.neverworks.context.spring;

import org.springframework.beans.factory.config.BeanPostProcessor;
import it.neverworks.context.ContextManagerAware;
import it.neverworks.context.ContextManager;

public class ContextManagerAwarePostProcessor implements BeanPostProcessor {
    
    private ContextManager manager;
    
    public ContextManagerAwarePostProcessor( ContextManager manager ) {
        this.manager = manager;
    }
    
    public Object postProcessAfterInitialization( Object bean, String beanName ) {
        return bean;
    } 
    
    public Object postProcessBeforeInitialization( Object bean, String beanName ) {
        if( bean instanceof ContextManagerAware ) {
            ((ContextManagerAware) bean).setContextManager( this.manager );
        }
        return bean;
    } 
}