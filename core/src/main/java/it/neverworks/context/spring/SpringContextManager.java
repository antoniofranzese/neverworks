package it.neverworks.context.spring;

import java.util.Date;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.io.Resource;
import it.neverworks.log.Logger;

import static it.neverworks.language.*;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Callable;
// import it.neverworks.lang.Filters;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Application;
import it.neverworks.lang.ClassScanner;
import it.neverworks.lang.Framework;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Annotations;
import it.neverworks.context.ContextManager;
import it.neverworks.context.LocalContext;
import it.neverworks.context.LocalContextComponent;
import it.neverworks.context.TrailManager;
import it.neverworks.context.Component;
import it.neverworks.context.ContextException;
import it.neverworks.context.ApplicationEvent;
import it.neverworks.context.EventListener;
import it.neverworks.model.Value;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Invoke;
import it.neverworks.model.features.Store;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.app.ApplicationStartupEvent;
import it.neverworks.app.ApplicationShutdownEvent;

public class SpringContextManager implements ContextManager, ApplicationListener, BeanFactoryPostProcessor, Inspect, Retrieve, Store {
    
    private static final Logger logger = Logger.of( SpringContextManager.class );

    private ApplicationContext applicationContext;
    private SpringTrailManager trailManager;
    private boolean started = false;
    private List<Runnable> shutdownRunnables = new ArrayList<>();
    private List<EventListener> eventListeners = new ArrayList<>();
    
    public SpringContextManager( ApplicationContext applicationContext ) {
        this.applicationContext = applicationContext;

        if( applicationContext instanceof ConfigurableApplicationContext ) {
            ((ConfigurableApplicationContext) applicationContext).addApplicationListener( this );
            ((ConfigurableApplicationContext) applicationContext).addBeanFactoryPostProcessor( this );
        } else {
            throw new ContextException( "ApplicationContext is not a ConfigurableApplicationContext" );
        }
        
        this.registerLocalContextComponent( new RequestLocalContextComponent() );
        this.registerLocalContextComponent( new TrailLocalContextComponent() );
        this.trailManager = new SpringTrailManager();
    }
    
    public boolean containsItem( Object key ) {
        return applicationContext.containsBean( Strings.valueOf( key ) );
    }
    
    public Object retrieveItem( Object key ) {
        return applicationContext.getBean( Strings.valueOf( key ) );
    }

    public void storeItem( Object key, Object value ) {
        throw new UnsupportedOperationException( "Top level bean replace is not supported" );
    }

	public <T> T get( String name ) {
        if( applicationContext.containsBean( name ) ) {
            return (T)applicationContext.getBean( name );
        } else if( ExpressionEvaluator.isExpression( name ) ) {
            return (T) ExpressionEvaluator.evaluate( this, name );
        } else {
            throw new ContextException( "Cannot retrieve bean: " + name );
        }
    }
    
    public <T> T get( Class<T> type ) {
        try {
            Map beans = applicationContext.getBeansOfType( type );
            if( beans != null && beans.size() == 1 ) {
                return (T) beans.values().iterator().next();
            } else {
                throw new RuntimeException( "Unexpected bean count for " + type + ": " + beans.size() );
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        
    }

    public ContextManager set( String name, Object value ) {
        ExpressionEvaluator.valorize( this, name, value );
        return this;
    }
    
    public int count( Class<?> type ) {
        try {
            Map beans = applicationContext.getBeansOfType( type );
            if( beans != null  ) {
                return beans.size();
            } else {
                return 0;
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
	public boolean has( String name ) {
        return applicationContext.containsBean( name );
    }

    public boolean has( Class type ) {
        try {
            Map beans = applicationContext.getBeansOfType( type );
            return beans.size() == 1;
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
        
    }
    
    public <T> T call( String name, Object... arguments ) {
        Object callable = get( name );
        
        if( callable instanceof Invoke ) {
            return (T) ((Invoke) callable).invokeInstance( Value.from( arguments ) );
        } else if( callable instanceof Callable ) {
            return (T) ((Callable) callable).call( Value.from( arguments ) );
        } else {
            throw new ContextException( "Context object \"" + name + "\" is not callable: " + Objects.repr( callable ) );
        } 
        
    }
    
    public Resource getResource( String location ) {
        return applicationContext.getResource( location );
    }
    
    public TrailManager getTrailManager() {
        return this.trailManager;
    }
    
    public void shutdown() {
        ((ConfigurableApplicationContext) this.applicationContext).close();
    }
    
    public void registerEventListener( EventListener listener ) {
        this.eventListeners.add( listener );
    }
    
    public void onApplicationEvent( org.springframework.context.ApplicationEvent event ) {
        if( event instanceof ContextStartedEvent || event instanceof ContextRefreshedEvent) {
            if( ! this.started ) {
                this.started = true;
                this.applicationContext.publishEvent( new ApplicationStartupEvent( this ) );
            }
        } else if( event instanceof ContextClosedEvent ) {
            if( this.started ) {
                this.started = false;
                this.applicationContext.publishEvent( new ApplicationShutdownEvent( this ) );

                for( Runnable runnable: this.shutdownRunnables ) {
                    runnable.run();
                }
                this.trailManager.shutdown();

            }
        } else if( event instanceof ApplicationEvent ) {
            for( EventListener listener: this.eventListeners ) {
                listener.notify( (ApplicationEvent) event );
            }
        }
        
    }
    
    public void publish( ApplicationEvent event ) {
        if( this.applicationContext != null ) {
            this.applicationContext.publishEvent( event );
        }
    }
    
    public void postProcessBeanFactory( ConfigurableListableBeanFactory beanFactory ) {
        logger.info( "Postprocessing BeanFactory" );

        beanFactory.registerScope( "trail", new TrailScope() );
        beanFactory.addBeanPostProcessor( new ContextManagerAwarePostProcessor( this ) );
        beanFactory.addBeanPostProcessor( new EventListenerPostProcessor( this ) );

        if( beanFactory instanceof BeanDefinitionRegistry ) {
            BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
        
            logger.info( "Scanning components" );
            try( ClassScanner scan = Application.scanner() ) {

                for( Class cls: scan.annotatedWith( Component.class ) ) {
            
                    logger.debug( "Adding component: {0.name}", cls );
                    Component annotation = Annotations.get( cls, Component.class );
                    String name = Strings.safe( annotation.name() ).trim();
                    if( empty( name ) ) {
                        do {
                            name = cls.getName() + "#" + Strings.generateHexID( 8 );
                        } while( registry.isBeanNameInUse( name ) );
                    }
            
                    if( ! registry.isBeanNameInUse( name ) ) {
                        String scope = Strings.hasText( annotation.scope() ) ? annotation.scope().trim().toLowerCase() : "singleton";

                        logger.debug( "Registering {} definition in ''{}'' scope as ''{}''", cls.getName(), scope, name );

                        BeanDefinitionBuilder builder = BeanDefinitionBuilder
                            .genericBeanDefinition( cls )
                            .setScope( scope )
                            .setLazyInit( annotation.lazy() );
                    
                        if( ! empty( annotation.init() ) ) {
                            builder.setInitMethodName( annotation.init().trim() );
                        }

                        if( ! empty( annotation.destroy() ) ) {
                            builder.setDestroyMethodName( annotation.destroy().trim() );
                        }

                        BeanDefinition definition = builder.getBeanDefinition();
                        
                        definition.setAttribute( "neverworks.scan", true );
                        
                        registry.registerBeanDefinition( name, definition );

                    } else {
                        logger.debug( "Skipping registration of {0.name} definition, ''{1}'' is already in use", cls, name );
                    }

                }

            }
        
        } else {
            throw new ContextException( "ApplicationContext is not a BeanDefinitionRegistry" );
        
        }
        
    }
    
    private static List<LocalContextComponent> localContextComponents = new ArrayList<LocalContextComponent>();
    
    public void registerLocalContextComponent( LocalContextComponent component ) {
        if( localContextComponents.contains( component ) ) {
            localContextComponents.remove( component );
        }
        localContextComponents.add( component );
    }
    
    public LocalContext backupLocalContext() {
        LocalContext local = new LocalContextSnapshot();
        for( LocalContextComponent component: localContextComponents ) {
            component.backup( local );
        }
        return local;
    }

    public void restoreLocalContext( LocalContext local ) {
        for( LocalContextComponent component: localContextComponents ) {
            component.restore( local );
        }
    }

    public void cleanupLocalContext( LocalContext local ) {
        for( LocalContextComponent component: localContextComponents ) {
            component.cleanup( local );
        }
    }
    
    public static void init( String... resources ) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext( resources );
    }
    
    public void subscribeShutdown( Runnable runnable ) {
        this.shutdownRunnables.add( runnable );
    }
    
    public ApplicationContext getApplicationContext(){
        return this.applicationContext;
    }
}