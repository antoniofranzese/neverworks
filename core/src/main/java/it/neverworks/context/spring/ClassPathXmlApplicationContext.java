package it.neverworks.context.spring;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import it.neverworks.context.Context;

public class ClassPathXmlApplicationContext extends org.springframework.context.support.ClassPathXmlApplicationContext {
    
    public ClassPathXmlApplicationContext( String[] resources ) {
        super( resources, /* refresh */ false );
        new Context().setApplicationContext( this );
        this.refresh();
    }
    
}