package it.neverworks.context.spring;

import org.springframework.beans.factory.support.DefaultListableBeanFactory;

import it.neverworks.context.Context;

public class XmlWebApplicationContext extends org.springframework.web.context.support.XmlWebApplicationContext {
    
    public XmlWebApplicationContext() {
        super();
        new Context().setApplicationContext( this );
    }
    
}