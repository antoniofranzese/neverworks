package it.neverworks.context.spring;

import java.util.Properties;

import it.neverworks.context.spring.PropertyPlaceholderConfigurer;
import it.neverworks.lang.Strings;
import it.neverworks.app.Configuration;
import it.neverworks.log.Logger;

public class ConfigurationPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
    
    private final static Logger logger = Logger.of( ConfigurationPlaceholderConfigurer.class );
    
    protected Configuration configuration;
    
    public ConfigurationPlaceholderConfigurer() {
        super();
    }

    public ConfigurationPlaceholderConfigurer( Configuration configuration ) {
        this();
        this.configuration = configuration;
    }
    
    protected String resolvePlaceholder( String placeholder, Properties props, int systemPropertiesMode ) {
        if( this.configuration != null && this.configuration.has( placeholder ) ) {
            logger.debug( "Resolving ''{}'' from configuration", placeholder );
            return Strings.valueOf( this.configuration.get( placeholder ) );

        } else {
            logger.debug( "Resolving ''{}'' from properties", placeholder );
            return super.resolvePlaceholder( placeholder, props, systemPropertiesMode );
        }
    } 
    
    public void setConfiguration( Configuration configuration ){
        this.configuration = configuration;
    }
    
}

