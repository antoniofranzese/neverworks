package it.neverworks.context.spring;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.Cleanable;
import it.neverworks.log.Logger;

public class TrailStorage implements Cleanable {

    private static final Logger logger = Logger.of( "TRAILS" );
    
    private Map<String, StoredBean> beans = new HashMap<String, StoredBean>();
    private String id;
    private Map<String, Object> attributes = new HashMap<String,Object>();
    
    public TrailStorage( String id ) {
        this.id = id;
    }
    
    public <T> T getAttribute( String key ) {
        return (T) ( this.attributes.containsKey( key ) ? this.attributes.get( key ) : null );
    }
    
    public void setAttribute( String key, Object value ) {
        this.attributes.put( key, value );
    }
    
    public boolean contains( String name ) {
        return beans.containsKey( name );
    }
    
    public Object get( String name ) {
        return beans.get( name ).instance;
    }
    
    public void put( String name, Object instance ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "Put {}: {}", name, instance );
        }
        
        StoredBean bean = beans.get( name );
        if( bean == null ) {
            bean = new StoredBean();
            beans.put( name, bean );
        }
        
        bean.instance = instance;
    }
    
    public void register( String name, Runnable callback ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "Register {} for {}", callback, name );
        }

        StoredBean bean = beans.get( name );
        if( bean == null ) {
            bean = new StoredBean();
            beans.put( name, bean );
        }

        if( bean.callbacks == null ) {
            bean.callbacks = new ArrayList<Runnable>();
        }

        bean.callbacks.add( callback );
    }
    
    public Object remove( String name ) {
        if( logger.isDebugEnabled() ) {
            logger.debug( "Remove {}", name );
        }
        StoredBean bean = beans.remove( name );
        if( bean != null ) {
            if( bean.callbacks != null ) {
                for( Runnable callback: bean.callbacks ) {
                    callback.run();
                }
            }
            return bean.instance;
        } else {
            return null;
        }
    }
    
    public void shutdown() {
        for( String name: new ArrayList<String>( beans.keySet() ) ) {
            remove( name );
        }
    }
    
    public void clean() {
        shutdown();
    }
    
    private class StoredBean {
        private Object instance;
        private List<Runnable> callbacks;
    }
    
    public String getId(){
        return this.id;
    }
    
    public void setId( String id ){
        this.id = id;
    }
    
    public String toString() {
        return "TrailStorage("+ this.id + ")";
    }
    
}