package it.neverworks.context;

public class TrailDef {
    
    private String name;

    public TrailDef( String name ) {
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String toString() {
        return "TrailDef(" + this.name + ")";
    }
    
}