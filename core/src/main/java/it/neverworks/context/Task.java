package it.neverworks.context;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.io.Closeable;

import it.neverworks.lang.Work;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Monitor;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.context.Context;
import it.neverworks.context.LocalContext;
import it.neverworks.model.Modelize;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.events.Promise;
import it.neverworks.model.events.Dispatcher;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.features.Describe;
import it.neverworks.model.features.Probe;
import it.neverworks.model.features.Get;

@Modelize
public class Task implements Runnable, Describe, Probe, Get {
    
    protected LocalContext localContext;
    protected List<Closeable> closeables;
    protected List<Timeout> timeouts;
    protected long startDate = 0;
    protected int startDelay = 0;
    protected Monitor monitor;
    protected Thread thread;
    protected RuntimeException error;
    protected Promise promise;
    protected TaskEvent event;
    protected ExpandoModel attributes;
    protected Dispatcher dispatcher;
    protected boolean stopped = false;
    
    public Task() {
        super();
        this.localContext = Context.getManager().backupLocalContext();
        this.closeables = new ArrayList<Closeable>();
        this.thread = new Thread( this );
        this.monitor = new Monitor();
        this.timeouts = new ArrayList<Timeout>();
    }

    public Task( Arguments attributes ) {
        this();
        getAttributes().set( attributes );
    }

    public Task( Dispatcher dispatcher ) {
        this();
        this.dispatcher = dispatcher;
    }

    public Task( Dispatcher dispatcher, Arguments attributes ) {
        this( dispatcher );
        getAttributes().set( attributes );
    }

    public final void run() {
        this.startDate = System.currentTimeMillis();
        
        Context.getManager().restoreLocalContext( localContext );
        try {
            if( this.startDelay > 0 ) {
                Thread.sleep( this.startDelay );
            }

            execute();
        } catch( InterruptedException iex ) {
            this.stopped = true;

        } catch( Exception ex ) {
            this.handle( ex );
        } finally {
            try {
                for( Closeable closeable: Collections.reverse( this.closeables ) ) {
                    closeable.close();
                }
            } catch( Exception ex ) {
                this.handle( ex );
            } finally {
                try {
                    Context.getManager().cleanupLocalContext( localContext );
                } finally {
                    this.release();
                }
            }
        }
    }
    
    public <T> T own( T object ) {
        if( object instanceof Closeable ) {
            this.closeables.add( (Closeable) object );
        }
        return object;
    }
    
    public ExpandoModel getAttributes() {
        checkError();
        if( this.attributes == null ) {
            synchronized( this ) {
                if( this.attributes == null ) {
                    this.attributes = new ExpandoModel();
                }
            }
        }
        return this.attributes;
    }
    
    public void execute() throws Exception {
        if( this.dispatcher != null ) {
            this.dispatcher.dispatch( this.getEvent() );
        }
    }
    
    @Override
    public void finalize() {
        if( this.isAlive() ) {
            this.stop();
        }
    }

    /* Thread */
    
    public <T extends Task> T start() {
        return start( 0 );
    }

    public <T extends Task> T start( int delay ) {
        if( this.thread.getState() == Thread.State.NEW ) {
            this.startDelay = delay > 0 ? delay : 0;
            this.thread.start();
            if( this.timeouts != null ) {
                for( Timeout timeout: this.timeouts ) {
                    timeout.start();
                }
            }
        }
        
        return (T) this;
    }

    public <T extends Task> T restart() {
        return restart( 0 );
    }
    
    public <T extends Task> T restart( int delay ) {
        this.thread = new Thread( this );
        this.monitor = new Monitor();
        return start( delay );
    }

    protected void checkError() {
        if( this.error != null ) {
            throw Errors.wrap( this.error );
        }
    }
    
    public boolean hasError() {
        return this.error != null;
    }
    
    public boolean isAlive() {
        checkError();
        return this.thread.isAlive() && ! this.monitor.released();
    }

    public boolean isRunning() {
        return this.thread.isAlive() && ! this.stopped && ! this.thread.interrupted();
    }

    public boolean isStarted() {
        checkError();
        return this.thread.getState() != Thread.State.NEW;
    }

    public boolean isStopped() {
        return this.stopped;
    }
    
    public <T extends Task> T stop() {
        if( this.thread.isAlive() ) {
            this.thread.interrupt();
            this.stopped = true;
        }
        return (T) this;
    }

    protected Thread thread() {
        checkError();
        return this.thread;
    }
    
    public <T extends Task> T hold() {
        this.monitor.hold();
        return (T) this;
    }
    
    public <T extends Task> T hold( int timeout ) {
        this.monitor.hold( timeout );
        return (T) this;
    }
    
    public void release() {
        this.monitor.release();
        if( hasError() ) {
            this.getPromise().reject( getEvent() );
        } else {
            this.getPromise().resolve( getEvent() );
        }
    }
    
    public void join() {
        if( this.isAlive() ) {
            try {
                this.thread.join();
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        }
    }
    
    // public void sleep( Number timeout ) throws InterruptedException {
    //     if( this.isRunning() ) {
    //         this.thread.sleep( Numbers.Long( timeout ) );
    //     }
    // }

    public void sleep( Number timeout, Work<Task> work ) {
        if( this.isRunning() ) {
            try {
                this.thread.sleep( Numbers.Long( timeout ) );
                if( work != null ) {
                    try {
                        work.doWithinWork( this );
                    } catch( Exception ex ) {
                        this.handle( ex );
                    }
                }
            } catch( InterruptedException iex ) {
                this.stopped = true;
            } 
        }
    }

    protected void handle( Exception ex ) {
        if( !( ex instanceof InterruptedException ) ) {
            this.error = Errors.wrap( ex );
            throw this.error;
        }
    }
    
    private class Timeout extends Thread {
        private int timeout;
        private Dispatcher<TaskEvent> dispatcher;
        
        public Timeout( int timeout, Dispatcher<TaskEvent> dispatcher ) {
            this.timeout = timeout;
            this.dispatcher = dispatcher;
        }
        
        public void run() {
            if( Task.this.isAlive() ) {
                long start = System.currentTimeMillis();

                hold( this.timeout );
                long elapsed = System.currentTimeMillis() - start;
                if( elapsed >= this.timeout ) {
                    dispatcher.dispatch( getEvent() );
                }
            }
        }
    }
    
    private class Interval extends Timeout {
        public Interval( int timeout, Dispatcher<TaskEvent> dispatcher ) {
            super( timeout, dispatcher );
        }
        
        public void run() {
            while( Task.this.isAlive() ) {
                super.run();
            }
        }
    }
        
    public <T extends Task> T when( int timeout, Dispatcher<TaskEvent> dispatcher ) {
        return schedule( new Timeout( timeout, processDispatcher( dispatcher ) ) );
    }

    public <T extends Task> T every( int timeout, Dispatcher<TaskEvent> dispatcher ) {
        return schedule( new Interval( timeout, processDispatcher( dispatcher ) ) );
    }
    
    protected <T extends Task> T schedule( Timeout timeout ) {
        this.timeouts.add( timeout );
        if( this.thread.getState() != Thread.State.NEW 
            && this.thread.getState() != Thread.State.TERMINATED ) {
            timeout.start();
        }
        return (T)this;
    }

    public <T extends Task> T then( Dispatcher<TaskEvent> dispatcher ) {
        return then( processDispatcher( dispatcher ), processDispatcher( dispatcher ) );
    }
    
    public <T extends Task> T then( Dispatcher<TaskEvent> resolver, Dispatcher<TaskEvent> rejector ) {
        this.getPromise().then( processDispatcher( resolver ), processDispatcher( rejector ) );
        return (T) this;
    }
    
    protected Promise getPromise() {
        if( this.promise == null ) {
            synchronized( this ) {
                if( this.promise == null ) {
                    this.promise = new Promise();
                }
            }
        }
        return this.promise;
    }

    protected TaskEvent getEvent() {
        if( this.event == null ) {
            synchronized( this ) {
                if( this.event == null ) {
                    this.event = new TaskEvent( this );
                }
            }
        }
        return this.event;
    }
    
    public Exception getError() {
        if( this.error != null ) {
            return Errors.unwrap( this.error );
        } else {
            return null;
        }
    }
    
    public long getElapsed() {
        return this.startDate > 0 ? System.currentTimeMillis() - this.startDate : 0L;
    }
    
    protected Dispatcher<TaskEvent> processDispatcher( Dispatcher dispatcher ) {
        return dispatcher;
    }

    /* Model */
    
    @Modelize
    protected ModelInstance modelInstance;
    
    public ModelInstance model() {
        return this.modelInstance;
    }
    
    public ModelInstance retrieveModelInstance() {
        return this.modelInstance;
    }
    
    public <T> T get( String name ) {
        checkError();
        return (T)this.modelInstance.eval( name );
    }

    public <T> T get( String name, T defaultValue ) {
        checkError();
        T value = (T)this.modelInstance.eval( name );
        return value != null ? value : defaultValue;
    }
    
    public <T extends Task> T set( String name, Object value ) {
        checkError();
        this.modelInstance.assign( name, value );
        return (T)this;
    }

    public <T extends Task> T set( Arguments arguments ) {
        checkError();
        if( arguments != null ) {
            for( String name: arguments.keys() ) {
                set( name, arguments.get( name ) );
            }
        }
        return (T)this;
    }
    
}

