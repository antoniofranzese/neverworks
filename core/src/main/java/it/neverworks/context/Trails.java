package it.neverworks.context;

import java.util.Map;
import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import it.neverworks.context.spring.SpringTrailManager;

public class Trails implements ApplicationContextAware {
    
    private static TrailManager trailManager = null;
    private Map<String,List<Runnable>> setup;

    public static TrailContext open() {
        return manager().open();
    }
    
    public synchronized static void select( String name ) {
        manager().select( name );
    }
    
    public synchronized static void deselect() {
        manager().deselect();
    }
    
    public static TrailDef current() {
        return manager().current();
    }
    
    protected static TrailManager manager() {
        if( trailManager != null ) {
            return trailManager;
        } else {
            throw new RuntimeException( "Trails is not initialized" );
        }
    }
    
    public void setSetup( Map<String,List<Runnable>> setup ){
        this.setup = setup;
    }

    /* package */ static void setManager( TrailManager manager ) {
        trailManager = manager;
    }

    /* Spring */
    
    public void setApplicationContext( ApplicationContext appContext ){
        if( trailManager != null ) {
            trailManager.setup( this.setup );
        }
    }

}