package it.neverworks.context;

import it.neverworks.lang.Guard;
import it.neverworks.lang.Errors;

public class ContextGuard implements Guard<ContextManager> {
    
    public ContextManager enter() {
        return Context.getManager();
    }

    public Throwable exit( Throwable error ){
        Context.shutdown();
        return error;
    }

}