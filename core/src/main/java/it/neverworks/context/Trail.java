package it.neverworks.context;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.MethodAnnotation;

@Target( ElementType.METHOD )
@Retention( RetentionPolicy.RUNTIME )
@MethodAnnotation( processor = TrailInterceptor.class )
public @interface Trail {
    String value();
}
