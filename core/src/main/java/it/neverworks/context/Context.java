package it.neverworks.context;

import java.util.List;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import it.neverworks.context.spring.SpringContextManager;
import it.neverworks.model.Value;

@SuppressWarnings("unchecked")
public class Context implements ApplicationContextAware {

    private static ContextManager contextManager = null;

	public static <T> T get( String name ) {
        if( contextManager != null ) {
            return contextManager.get( name );
        } else {
            throw new ContextException( "Context is not initialized" );
        }
    }
    
    public static Value value( String name ) {
        return Value.of( get( name ) );
    }

	public static <T> T get( Class<T> type ) {
        if( contextManager != null ) {
            return contextManager.get( type );
        } else {
            throw new ContextException( "Context is not initialized" );
        }
    }

	public static int count( Class<?> type ) {
        if( contextManager != null ) {
            return contextManager.count( type );
        } else {
            return 0;
        }
    }
    
	public static boolean contains( String name ) {
        if( contextManager != null ) {
            return contextManager.has( name );
        } else {
            return false;
        }
    }

	public static boolean contains( Class type ) {
        if( contextManager != null ) {
            return contextManager.has( type );
        } else {
            return false;
        }
    }
    
	public static <T> T call( String name, Object... arguments ) {
        if( contextManager != null ) {
            return (T) contextManager.call( name, arguments );
        } else {
            throw new ContextException( "Context is not initialized" );
        }
    }
    
	public static ContextManager set( String name, Object value ) {
        if( contextManager != null ) {
            return contextManager.set( name, value );
        } else {
            throw new ContextException( "Context is not initialized" );
        }
    }
    
    public static ContextManager getManager() {
        if( contextManager != null ) {
            return contextManager;
        } else {
            throw new ContextException( "Context is not initialized" );
        }
    }

    public static boolean ready() {
        return contextManager != null;
    }

    private void setManager( ContextManager manager ) {
        contextManager = manager;
        if( manager != null ) {
            Trails.setManager( manager.getTrailManager() );
            
            if( localContextComponents != null ) {
                for( LocalContextComponent component: localContextComponents ) {
                    manager.registerLocalContextComponent( component );
                }
            }

            manager.subscribeShutdown( new Runnable(){
                public void run() {
                    new Context().setManager( null );
                }
            });
        } else {
            Trails.setManager( null );
        }
    }

    private List<LocalContextComponent> localContextComponents;
    
    public List<LocalContextComponent> getLocalContextComponents(){
        return this.localContextComponents;
    }
    
    public void setLocalContextComponents( List<LocalContextComponent> localContextComponents ){
        this.localContextComponents = localContextComponents;
    }
    
    //TODO: accantonare ContextManager precedente, se presente, e ripristinarlo
    public static ContextGuard init( String... resources ) {
        SpringContextManager.init( resources );
        return new ContextGuard();
    }
    
    public static void shutdown() {
        if( ready() ) {
            getManager().shutdown();
        }
    }

    public void publish( ApplicationEvent event ) {
        if( ready() ) {
            getManager().publish( event );
        }
    }

    /* Spring */
    
    public static ContextGuard init( ApplicationContext appContext ) {
        new Context().setManager( new SpringContextManager( appContext ) );
        return new ContextGuard();
    }

    public void setApplicationContext( ApplicationContext appContext ){
        if( contextManager != null ) {
            if( contextManager instanceof SpringContextManager ) {
                SpringContextManager spring = (SpringContextManager)contextManager;
                if( spring.getApplicationContext() != appContext ) {
                    throw new ContextException( "Context is initialized with a different Spring ApplicationContext" );
                }
            } else {
                throw new ContextException( "Context is initialized with a different ContextManager" );
            }
        } else {
            setManager( new SpringContextManager( appContext ) );
        }
        
    }

}