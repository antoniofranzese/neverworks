package it.neverworks.context;

public interface LocalContext {
    public <T> T get( String name );
    public boolean has( String name );
    public void put( String name, Object value );
}