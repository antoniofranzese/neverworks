package it.neverworks.context;

import java.util.Map;
import java.util.List;

public interface TrailManager {
    void select( String trail );
    void deselect();
    void shutdown();
    void setup( Map<String,List<Runnable>> setup );
    TrailDef current();
    TrailContext open();
    void close( TrailContext context );
}