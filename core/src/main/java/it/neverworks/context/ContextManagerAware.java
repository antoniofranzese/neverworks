package it.neverworks.context;

public interface ContextManagerAware {
    void setContextManager( ContextManager manager );
}