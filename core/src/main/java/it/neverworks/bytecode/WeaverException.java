package it.neverworks.bytecode;

import javassist.CtClass;
import it.neverworks.lang.Strings;

public class WeaverException extends RuntimeException {
    
    public WeaverException( CtClass cls, String message ) {
        super( Strings.message( "Error instrumenting {0.name} class: {1}", cls, message ) );
    }
    
    public WeaverException( CtClass cls, Throwable cause ) {
        super( Strings.message( "Error instrumenting {0.name} class: {1.message}", cls, cause ), cause );
    }
}