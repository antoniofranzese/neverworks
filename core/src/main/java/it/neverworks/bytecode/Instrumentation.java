package it.neverworks.bytecode;

import java.util.Enumeration;
import java.util.List;
import java.util.ArrayList;
import java.net.URL;
import java.io.File;

import javassist.ClassPool;

import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.lang.ClassScanner;

public class Instrumentation {

    private Weaver[] weavers = null;

    public static void main( String[] args ) throws Exception {

        Instrumentation instr = new Instrumentation();
        List<String> packages = new ArrayList<String>();
 
        for( String arg: args ) {
            if( arg.startsWith( "-P" ) ) {
                packages.add( arg.substring( 2 ).trim() );
            } else if( arg.startsWith( "-V" ) ) {
                WeaverContext.setVerbose( true );
            } else if( arg.startsWith( "-T" ) ) {
                WeaverContext.setTargetClassFolder( arg.substring( 2 ).trim() );
            } else if( arg.startsWith( "-W" ) ) {
                String weaverSpec = arg.substring( 2 ).trim();
                if( weaverSpec.equalsIgnoreCase( "auto" ) ) {
                    instr.scanWeavers();
                } else {
                    Class[] classes = Collections.each( weaverSpec.split( "," ) )
                        .map( n -> Reflection.findClass( n.trim() ) )
                        .filter( c -> c != null )
                        .array( Class.class );
                }
            }
        }

        instr.instrument( packages.toArray( new String[ packages.size() ] ) );

    }

    public Instrumentation addWeavers( Class[] classes ) {

        return this;
    }

    public Instrumentation scanWeavers() {
        try( ClassScanner scanner = new ClassScanner( "it.neverworks" ) ) {
            this.weavers = (Weaver[]) scanner.annotatedWith( AutoWeaver.class )
                .inspect( cls -> WeaverContext.logger().info( "Adding Weaver " + cls.getName() ) )
                .map( cls -> (Weaver) Reflection.newInstance( cls ) )
                .array( Weaver.class );
        }
        return this;
    }
 
    public void instrument( String[] packages ) throws Exception {

        // Default weaver set
        if( this.weavers == null ) {
            throw new Exception( "No weavers specified, provide a weaver list or request a classpath scan");
        }
       
        for( String pkg: packages ) {
            int count = 0;
            WeaverContext.logger().debug( "Scanning package: {}", pkg );
            for( String cls: scanPackage( pkg ) ) {
                WeaverContext.logger().debug( "Analyzing class: {}", cls );
                boolean instrumented = false; 
                for( Weaver weaver: this.weavers ) {
                    try {
                        if( weaver.instrument( cls ) ) {
                            instrumented = true;
                        }
                    } catch( Exception ex ) {
                        WeaverContext.logger().error( "Error instrumenting " + cls + " with " + weaver.getClass().getSimpleName() );
                        ex.printStackTrace();
                        break;
                    }
                }
                
                if( instrumented ) {
                    count++;
                };
            }
            //WeaverContext.log( "Instrumenting " + pkg + ", " + String.valueOf( count ) + " classes" );
        }
    }
    
    
    private String[] scanPackage( String packageName ) throws Exception {
        List<File> dirs = new ArrayList<File>();
        
        if( packageName.indexOf( "@" ) > 0 ) {
            String[] parts = packageName.split( "@" );
            String path = parts[ 1 ] + File.separator + parts[ 0 ].replace( ".", File.separator );
            dirs.add( new File( path ) );
            packageName = parts[ 0 ];

        } else {

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            String path = packageName.replace( '.', '/' );
            Enumeration<URL> resources = classLoader.getResources( path );

            while (resources.hasMoreElements()) {
                URL resource = resources.nextElement();
                dirs.add( new File( resource.getFile() ) );
            }
        }
        

        ArrayList<String> classes = new ArrayList<String>();
        for( File directory : dirs ) {
            classes.addAll( findClasses( directory, packageName ) );
        }

        return classes.toArray( new String[ classes.size() ] );
    }
    
    private List<String> findClasses( File directory, String packageName ) throws Exception {
        List<String> classes = new ArrayList<String>();

        if( !directory.exists() ) {
            return classes;
        }
        
        File[] files = directory.listFiles();
        for( File file : files ) {

            if( file.isDirectory() ) {
                classes.addAll( findClasses( file, packageName + "." + file.getName() ) );

            } else if( file.getName().endsWith( ".class" ) ) {
                classes.add(  packageName + '.' + file.getName().substring( 0, file.getName().length() - 6 ) );
            }
        }
        return classes;
        
    }
    
    
 }