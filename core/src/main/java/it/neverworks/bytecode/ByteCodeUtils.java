package it.neverworks.bytecode;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.bytecode.ConstPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import javassist.bytecode.annotation.ClassMemberValue;
import javassist.bytecode.annotation.EnumMemberValue;
import javassist.bytecode.annotation.BooleanMemberValue;
import javassist.bytecode.annotation.IntegerMemberValue;
import javassist.bytecode.annotation.AnnotationMemberValue;

import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang.StringUtils;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Range;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Reflection;

import static it.neverworks.language.*;

public class ByteCodeUtils {

    public static ClassPool getPool() {
        return WeaverContext.getPool();
    }
    
    public static CtClass getType( String name ) {
        try {
            return getPool().get( name );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public static CtClass getType( Class cls ) {
        return getType( cls.getName() );
    }
    
    public static boolean isObject( CtClass cls ) {
        return cls.getName().equals( "java.lang.Object" );
    }

    public static java.lang.annotation.Annotation getAnnotation( CtClass cls, Class annotationClass ) throws Exception {
        java.lang.annotation.Annotation annotation = (java.lang.annotation.Annotation)cls.getAnnotation( annotationClass );
        if( annotation == null 
            && annotationClass.getAnnotation( java.lang.annotation.Inherited.class ) != null
            && ! isObject( cls.getSuperclass() ) ) {
            
            return getAnnotation( cls.getSuperclass(), annotationClass );
        } else {
            return annotation;
        }
    }
    
    public static boolean hasAnnotation( CtClass cls, Class annotationClass ) throws Exception {
        return getAnnotation( cls, annotationClass ) != null;
    }
    
    public static CtClass getImplementingClass( CtClass source, Class iface ) throws Exception{
        return getImplementingClass( source, getType( iface ) );
    }

    public static CtClass getImplementingClass( CtClass source, CtClass iface ) throws Exception{
        CtClass result = null;
        for( CtClass cls: source.getInterfaces() ) {
            if( cls.equals( iface ) ) {
                result = source;
            } else if( hasInterface( cls, iface ) ) {
                result = source;
            }
        }

        if( result == null && ! isObject( source.getSuperclass() ) ){
            result = getImplementingClass( source.getSuperclass(), iface );
        }

        if( result != null ) {
            CtClass superClass = source.getSuperclass();
            if( ! isObject( superClass ) ) {
                CtClass superImplementing = getImplementingClass( superClass, iface );
                if( superImplementing != null ) {
                    result = superImplementing;
                }
            }
        }

        return result;
    }

    public static boolean hasSuperclass( CtClass source, CtClass superclass ) throws Exception {
        if( source.equals( superclass ) ) {
            return true;
        } else if( ! isObject( source.getSuperclass() ) ) {
            return hasSuperclass( source.getSuperclass(), superclass );
        } else {
            return false;
        }
    }

    public static boolean hasInterface( CtClass source, String iface ) {
        return hasInterface( source, getType( iface ) );
    }

    public static boolean hasInterface( CtClass source, Class iface ) {
        return hasInterface( source, getType( iface ) );
    }

    public static boolean hasInterface( CtClass source, CtClass iface ) {
        try {
            while( ! isObject( source ) ) {
                for( CtClass inspected: source.getInterfaces() ) {
                    if( inspected.equals( iface ) ) {
                        return true;
                    } else if( hasInterface( inspected, iface ) ) {
                        return true;
                    }
                } 
                
                source = source.getSuperclass();
            }
        
        } catch( Exception ex ) {
            return false;
        }
        return false;
    }

    public static boolean hasDeclaredInterface( CtClass source, String iface ) {
        return hasDeclaredInterface( source, getType( iface ) );
    }

    public static boolean hasDeclaredInterface( CtClass source, Class iface ) {
        return hasDeclaredInterface( source, getType( iface ) );
    }

    public static boolean hasDeclaredInterface( CtClass source, CtClass iface ) {
        try {
            return list().cat( source.getInterfaces() ).contains( iface );
        } catch( Exception ex ) {
            return false;
        }
    }
    
    public static String getJavaSignature( CtMethod method ) throws Exception {
        return getJavaTypeList( method.getParameterTypes() );
    }

    public static String getJavaTypeList( CtClass[] types ) throws Exception {
        String[] names = new String[ types.length ];
        for( int i = 0; i < names.length; i++ ) {
            names[ i ] = types[ i ].getName() + ".class";
        }
        return StringUtils.join( names, "," );
    }
    
    public static String boxValue( CtField field, String placeholder ) throws Exception {
        return boxValue( field.getType(), placeholder );
    }    
    
    public static String boxValue( CtClass type, String placeholder ) throws Exception {
        if( type.equals( CtClass.byteType ) ) {
            return "Byte.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.shortType ) ) {
            return "Short.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.intType ) ) {
            return "Integer.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.longType ) ) {
            return "Long.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.floatType ) ) {
            return "Float.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.doubleType ) ) {
            return "Double.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.booleanType ) ) {
            return "Boolean.valueOf(" + placeholder + ")";

        } else if( type.equals( CtClass.charType ) ) {
            return "Character.valueOf(" + placeholder + ")";

        } else {
            return placeholder;
        }
    }
    
    public static class AnnotationBuilder {
        private ConstPool constPool;
        private ClassFile clsFile;
        private Annotation annotation;
        private CtClass targetClass;
        private Object target;
        
        public String toString() {
            return "AnnotationBuilder(annotation=" + annotation + ")";
        }
        
        public AnnotationBuilder( CtClass cls, String annotationClass ) {
            this.clsFile = cls.isFrozen() ? cls.getClassFile2() : cls.getClassFile();
            this.constPool = clsFile.getConstPool();
            this.annotation = new Annotation( annotationClass, constPool );
            this.targetClass = cls;
        }

        public AnnotationBuilder( CtClass cls, Annotation annotation ) {
            this.clsFile = cls.isFrozen() ? cls.getClassFile2() : cls.getClassFile();
            this.constPool = clsFile.getConstPool();
            this.annotation = annotation;
            this.targetClass = cls;
        }

        public AnnotationBuilder( CtMethod method, String annotationClass ) {
            this( method.getDeclaringClass(), annotationClass );
            this.target = method;
        }

        public AnnotationBuilder( CtMethod method, Annotation annotation ) {
            this( method.getDeclaringClass(), annotation );
            this.target = method;
        }
        
        public AnnotationBuilder( CtField field, String annotationClass ) {
            this( field.getDeclaringClass(), annotationClass );
            this.target = field;
        }

        public AnnotationBuilder( CtField field, Annotation annotation ) {
            this( field.getDeclaringClass(), annotation );
            this.target = field;
        }
        
        public Annotation annotation() {
            return this.annotation;
        }
        
        protected MemberValue wrap( Object value ) {
            if( value instanceof String ) {
                return new StringMemberValue( (String) value, constPool );

            } else if( value instanceof Integer ) {
                IntegerMemberValue intMember = new IntegerMemberValue( constPool );
                intMember.setValue( ((Integer) value) );
                return intMember;

            } else if( value instanceof Boolean ) {
                return new BooleanMemberValue( (Boolean) value, constPool );
                
            } else if( value instanceof Class ) {
                return new ClassMemberValue( ((Class) value).getName(), constPool );

            } else if( value instanceof List ) {
                ArrayMemberValue arrayMember = new ArrayMemberValue( constPool );
                MemberValue[] members = new MemberValue[ ((List) value).size() ];
                for( int i: Range.of( value ) ) {
                    members[ i ] = wrap( ((List) value).get( i ) );
                }
                arrayMember.setValue( members );
                return arrayMember;
                
            } else if( value instanceof Annotation ) {
                return new AnnotationMemberValue( (Annotation) value, constPool );    

            } else {
                throw new IllegalArgumentException( "Cannot wrap unsupported annotation member: " + Objects.repr( value ) );
            }
        }
        
        protected Object unwrap( MemberValue value ) {
            if( value instanceof StringMemberValue ) {
                return ((StringMemberValue) value).getValue();

            } else if( value instanceof IntegerMemberValue ) {
                return ((IntegerMemberValue) value).getValue();

            } else if( value instanceof BooleanMemberValue ) {
                return ((BooleanMemberValue) value).getValue();

            } else if( value instanceof ClassMemberValue ) {
                return Reflection.findClass( ((ClassMemberValue) value).getValue() );

            } else if( value instanceof ArrayMemberValue ) {
                List result = new ArrayList();
                for( MemberValue item: ((ArrayMemberValue) value).getValue() ) {
                    result.add( unwrap( item ) );
                }
                return result;
                
            } else {
                    throw new IllegalArgumentException( "Cannot unwrap nsupported annotation member: " + Objects.repr( value ) );
            }
        }
        
        public AnnotationBuilder set( String name, Object value ) {
            annotation.addMemberValue( name, wrap( value ) );
            return this;
        }

        public AnnotationBuilder set( String name, Class enumClass, String value ) {
            EnumMemberValue enumValue = new EnumMemberValue( constPool );
            enumValue.setType( enumClass.getName() );
            enumValue.setValue( value );
            annotation.addMemberValue( name, enumValue );
            return this;
        }

        public String getString( String name ) {
            return (String) unwrap( this.annotation.getMemberValue( name ) );
        }

        public Boolean getBoolean( String name ) {
            return (Boolean) unwrap( this.annotation.getMemberValue( name ) );
        }

        public Integer getInteger( String name ) {
            return (Integer) unwrap( this.annotation.getMemberValue( name ) );
        }

        public Class getType( String name ) {
            return (Class) unwrap( this.annotation.getMemberValue( name ) );
        }

        public <T> List<T> getList( String name ) {
            return (List<T>) unwrap( this.annotation.getMemberValue( name ) );
        }
        
        public void annotate( CtField field ) {
            AnnotationsAttribute attribute = (AnnotationsAttribute)field.getFieldInfo().getAttribute( AnnotationsAttribute.visibleTag );
            if( attribute == null ) {
                attribute = new AnnotationsAttribute( constPool, AnnotationsAttribute.visibleTag );
                field.getFieldInfo().addAttribute( attribute );
            }
            attribute.addAnnotation( annotation );
        }

        public void annotate( CtMethod method ) {
            AnnotationsAttribute attribute = (AnnotationsAttribute)method.getMethodInfo().getAttribute( AnnotationsAttribute.visibleTag );
            if( attribute == null ) {
                attribute = new AnnotationsAttribute( constPool, AnnotationsAttribute.visibleTag );
                method.getMethodInfo().addAttribute( attribute );
            }
            attribute.addAnnotation( annotation );
        }

        public void annotate( CtClass cls ) {
            if( ! cls.isFrozen() ) {
                AnnotationsAttribute attribute = (AnnotationsAttribute)this.clsFile.getAttribute( AnnotationsAttribute.visibleTag );
                if( attribute == null ) {
                    attribute = new AnnotationsAttribute( constPool, AnnotationsAttribute.visibleTag );
                    this.clsFile.addAttribute( attribute );
                }
                attribute.addAnnotation( annotation );
            } else {
                throw new IllegalStateException( "Cannot annotate frozen class: " + cls.getName() );
            }
        }
        
        public void annotate() {
            if( this.target instanceof CtMethod ) {
                annotate( (CtMethod) this.target );
            } else if( this.target instanceof CtField ) {
                annotate( (CtField) this.target );
            } else {
                annotate( this.targetClass );
            }
        }
        
    }

    public static AnnotationBuilder createAnnotation( CtClass cls, Class<? extends java.lang.annotation.Annotation> annotationClass ) {
        return new AnnotationBuilder( cls, annotationClass.getName() );
    }

    public static AnnotationBuilder createAnnotation( CtClass cls, String annotationClass ) {
        return new AnnotationBuilder( cls, annotationClass );
    }

    public static AnnotationBuilder createAnnotation( CtMethod method, Class<? extends java.lang.annotation.Annotation> annotationClass ) {
        return new AnnotationBuilder( method, annotationClass.getName() );
    }

    public static AnnotationBuilder createAnnotation( CtMethod method, String annotationClass ) {
        return new AnnotationBuilder( method, annotationClass );
    }

    public static AnnotationBuilder createAnnotation( CtField field, Class<? extends java.lang.annotation.Annotation> annotationClass ) {
        return new AnnotationBuilder( field, annotationClass.getName() );
    }

    public static AnnotationBuilder createAnnotation( CtField field, String annotationClass ) {
        return new AnnotationBuilder( field, annotationClass );
    }

    public static AnnotationBuilder extractAnnotation( CtClass cls, Class<? extends java.lang.annotation.Annotation> annotationClass ) {
        return extractAnnotation( cls, annotationClass.getName() );
    }
    
    public static AnnotationBuilder extractAnnotation( CtMethod method, Class<? extends java.lang.annotation.Annotation> annotationClass ) {
        return extractAnnotation( method, annotationClass.getName() );
    }
    
    public static AnnotationBuilder extractAnnotation( CtField field, Class<? extends java.lang.annotation.Annotation> annotationClass ) {
        return extractAnnotation( field, annotationClass.getName() );
    }

    public static AnnotationBuilder extractAnnotation( CtClass cls, String annotationClass ) {
        ClassFile file = cls.isFrozen() ? cls.getClassFile2() : cls.getClassFile();
        AnnotationsAttribute attr = (AnnotationsAttribute) file.getAttribute( AnnotationsAttribute.visibleTag );
        return new AnnotationBuilder( cls, attr.getAnnotation( annotationClass ) );
    }

    public static AnnotationBuilder extractAnnotation( CtMethod method, String annotationClass ) {
        MethodInfo info = method.getMethodInfo();
        AnnotationsAttribute attr = (AnnotationsAttribute) info.getAttribute( AnnotationsAttribute.visibleTag );
        return new AnnotationBuilder( method, attr.getAnnotation( annotationClass ) );
    }
    
    public static AnnotationBuilder extractAnnotation( CtField field, String annotationClass ) {
        FieldInfo info = field.getFieldInfo();
        AnnotationsAttribute attr = (AnnotationsAttribute) info.getAttribute( AnnotationsAttribute.visibleTag );
        return new AnnotationBuilder( field, attr.getAnnotation( annotationClass ) );
    }
    
    public static void mark( CtClass cls, String marker ) throws Exception {
        if( ! isMarked( cls, marker ) ) {
            List<String> markers = cls.hasAnnotation( Weavers.class ) ? extractAnnotation( cls, Weavers.class ).getList( "value" ) : new ArrayList<String>();
            markers.add( marker );
            createAnnotation( cls, Weavers.class )
                .set( "value", markers )
                .annotate();
        }
    }
    
    public static boolean isMarked( CtClass cls, String marker ) throws Exception {
        if( cls.hasAnnotation( Weavers.class )  ) {
            return extractAnnotation( cls, Weavers.class ).getList( "value" ).contains( marker );
        } else {
            return false;
        }
    }

    public static void mark( CtMethod method, String marker ) throws Exception {
        if( ! isMarked( method, marker ) ) {
            List<String> markers = method.hasAnnotation( Weavers.class ) ? extractAnnotation( method, Weavers.class ).getList( "value" ) : new ArrayList<String>();
            markers.add( marker );
            createAnnotation( method, Weavers.class )
                .set( "value", markers )
                .annotate();
        }
    }
    
    public static boolean isMarked( CtMethod method, String marker ) throws Exception {
        if( method.hasAnnotation( Weavers.class )  ) {
            return extractAnnotation( method, Weavers.class ).getList( "value" ).contains( marker );
        } else {
            return false;
        }
    }

    public static void mark( CtField field, String marker ) throws Exception {
        if( ! isMarked( field, marker ) ) {
            List<String> markers = field.hasAnnotation( Weavers.class ) ? extractAnnotation( field, Weavers.class ).getList( "value" ) : new ArrayList<String>();
            markers.add( marker );
            createAnnotation( field, Weavers.class )
                .set( "value", markers )
                .annotate();
        }
    }
    
    public static boolean isMarked( CtField field, String marker ) throws Exception {
        if( field.hasAnnotation( Weavers.class )  ) {
            return extractAnnotation( field, Weavers.class ).getList( "value" ).contains( marker );
        } else {
            return false;
        }
    }

    public static void write( CtClass cls ) throws Exception {
        WeaverContext.write( cls );
    }
}