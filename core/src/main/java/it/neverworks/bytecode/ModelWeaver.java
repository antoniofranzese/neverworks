package it.neverworks.bytecode;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.CtField;
import javassist.CtConstructor;
import javassist.Modifier;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.FieldAccess;

import java.util.Set;
import java.util.HashSet;

import static it.neverworks.language.*;
import it.neverworks.model.Model;
import it.neverworks.model.Modelize;
import it.neverworks.model.Property;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.DefaultFactory;
import it.neverworks.model.description.Preserved;
import it.neverworks.model.description.Synthesize;

@AutoWeaver
public class ModelWeaver implements Weaver {
    private final static String DEFAULT_INSTANCE_FIELD = "__model__";
    private final static String PROPERTY_DESCRIPTOR = "it.neverworks.model.description.PropertyDescriptor";
    private final static String MODEL_DESCRIPTOR = "it.neverworks.model.description.ModelDescriptor";
    private final static String MODEL_INSTANCE = "it.neverworks.model.description.ModelInstance";
    private final static String INSTANCE_AWARE = "it.neverworks.model.description.ModelInstanceAware";
    
    //private final static String PROPERTY_PREFIX = "__neverworks$model$property$";
    
    public boolean instrument( String className ) throws Exception {

        CtClass cls = ByteCodeUtils.getType( className );
        boolean instrumented = false;

        if( !cls.isInterface()  ) {
            // Instrumenta tutte le classi annotate con @Modelize
            if( ByteCodeUtils.getAnnotation( cls, Modelize.class ) != null ) {   
                instrumentClass( cls );

            } else if( ByteCodeUtils.hasInterface( cls, Model.class ) ) {
                CtClass modelClass = ByteCodeUtils.getImplementingClass( cls, Model.class );
                modelizeClass( modelClass );
                instrumentClass( modelClass );

                if( ! cls.equals( modelClass ) ) {
                    instrumentClass( cls );
                }
                
            } else {
                // E' un errore contenere @Property senza annotazione @Modelize
                // I getter/setter non potrebbero funzionare correttamente
                boolean shouldInstrument = false;
                for( CtField field: cls.getDeclaredFields() ) {
                    if( field.getAnnotation( Property.class ) != null ) {
                        shouldInstrument = true;
                    }
                }
            
                if( shouldInstrument ) {
                    
                    throw new Exception( cls.getName() + " declares @Property fields, it must be annotated with @Modelize or implement Model" );
                }
            }
        }
    
        return instrumented;
        
    }
    
    private CtField findInstanceField( CtClass cls ) throws Exception {
        CtField instanceField = null;

        while( ! ByteCodeUtils.isObject( cls ) && ByteCodeUtils.hasAnnotation( cls, Modelize.class ) ) {

            // Cerca il campo ModelInstance, se indicato
            for( CtField fld: cls.getDeclaredFields() ) {
                if( fld.hasAnnotation( Modelize.class ) ) {
                    if( instanceField == null ) {
                        instanceField = fld;
                    } else {
                        throw new Exception( cls.getName() + " has multiple @Modelize fields, either declared or inherited" );
                    }
                }
            }
            
            cls = cls.getSuperclass();
        }
        
        return instanceField;
    }

    private void modelizeClass( CtClass cls ) throws Exception {

        if( cls.isFrozen() ) {
            cls.defrost();
        }

        if( ! cls.hasAnnotation( Modelize.class ) ) {
            WeaverContext.debug( "Synthesizing @Modelize on " + cls.getName() );
            ByteCodeUtils.createAnnotation( cls, "it.neverworks.model.Modelize" )
                .set( "synthetic", true )
                .annotate( cls );
        }

        ByteCodeUtils.write( cls );
        
    
    }
    
    private void instrumentClass( CtClass cls ) throws Exception {
        
        if( !ByteCodeUtils.isObject( cls.getSuperclass() ) && ByteCodeUtils.hasAnnotation( cls, Modelize.class ) ) {
            if( ByteCodeUtils.hasAnnotation( cls.getSuperclass(), Modelize.class ) ) {
                instrumentClass( cls.getSuperclass() );
            }
        } 
        
        if( ! ByteCodeUtils.isMarked( cls, "model" ) ) {
            
            WeaverContext.debug( "Instrumenting model on " + cls.getName() );

              // Scongela la classe in esame
            if( cls.isFrozen() ) {
                cls.defrost();
            }
        
            CtField instanceField = findInstanceField( cls );
            
            boolean created = false;
            if( instanceField == null ) {
                // Modifica l'implementazione della prima classe in gerarchia annotata con @Modelize
                instanceField = new CtField( ByteCodeUtils.getType( MODEL_INSTANCE ), DEFAULT_INSTANCE_FIELD, cls );
                instanceField.setModifiers( Modifier.PUBLIC );
                cls.addField( instanceField );
                // CtField instanceField = new CtField( ByteCodeUtils.getType( MODEL_INSTANCE ), instanceFieldName, cls );
                // instanceField.setModifiers( Modifier.PUBLIC );
                // String instanceInitializer = "new " + MODEL_INSTANCE + "( this );";
                // cls.addField( instanceField, CtField.Initializer.byExpr( instanceInitializer ) );

                ByteCodeUtils.createAnnotation( cls, "it.neverworks.model.Modelize" )
                    .set( "synthetic", true )
                    .annotate( instanceField );
                    
                created = true;
            }
            
            String instanceFieldName = instanceField.getName();
            
            if( instanceField.getDeclaringClass().equals( cls ) ) {
                
                WeaverContext.debug( "Instrumenting @Modelize on " + cls.getName() + " using " + ( created ? "new " : "existing ") + instanceFieldName + " field" );
                
                // Evita serializzazione XML
                ByteCodeUtils.createAnnotation( cls, "com.thoughtworks.xstream.annotations.XStreamOmitField" ).annotate( instanceField );
                ByteCodeUtils.createAnnotation( cls, "javax.xml.bind.annotation.XmlTransient" ).annotate( instanceField );
                ByteCodeUtils.createAnnotation( cls, "javax.persistence.Transient" ).annotate( instanceField );
                ByteCodeUtils.createAnnotation( cls, "com.fasterxml.jackson.annotation.JsonIgnore" ).annotate( instanceField );

                // String instanceInitializer = "new " + MODEL_INSTANCE + "( this );";
               //  cls.addField( instanceField, CtField.Initializer.byExpr( instanceInitializer ) );

                for( CtConstructor constructor: cls.getDeclaredConstructors() ) {
                    WeaverContext.debug( "Instumenting main constructor " + constructor.getLongName() );
                    //constructor.instrument( new PropertyFieldEditor() );
                    
                    constructor.insertBeforeBody(
                        "if( $0." + instanceFieldName + " == null ) { " + 
                        "$0." + instanceFieldName + " = new " + MODEL_INSTANCE + "( this );" + 
                        ( cls.subtypeOf( ByteCodeUtils.getType( INSTANCE_AWARE ) ) ? "storeModelInstance( $0." + instanceFieldName + " ); " : "" ) + 
                        "}"  
                        //+ "$0." + instanceFieldName + ".init();"
                    );

                    if( ! constructor.hasAnnotation( Preserved.class ) ) {
                        constructor.instrument( new PropertyFieldEditor( instanceFieldName ) );
                    }

                    constructor.insertAfter(
                        "{ if( $0.getClass() == " + cls.getName() + ".class ){ $0." + instanceFieldName + ".init(); }; }"
                    );
                    
                }

                // Modelized implementation
                String retrieveMethodBody = "{ return $0." + instanceFieldName + ";}";
                
                try {
                    CtMethod retrieveMethod = cls.getMethod( "retrieveModelInstance", "()Lit/neverworks/model/description/ModelInstance;" );
                    
                    if( ! retrieveMethod.hasAnnotation( Preserved.class ) ) {
                        if( retrieveMethod.getDeclaringClass().equals( cls ) ) {
                            WeaverContext.debug( "Replacing retrieveModelInstance implementation on " + cls.getName() );
                            retrieveMethod.setBody( retrieveMethodBody );
                        } else {
                            throw new NotFoundException( "default method" );
                        }
                    }

                } catch( NotFoundException ex1 ) {
                    WeaverContext.debug( "Creating retrieveModelInstance implementation on " + cls.getName() );
                    CtMethod retrieveMethod = new CtMethod( ByteCodeUtils.getType( MODEL_INSTANCE ), "retrieveModelInstance", new CtClass[]{}, cls );
                    retrieveMethod.setBody( retrieveMethodBody );
                    cls.addMethod( retrieveMethod );
                }
                
                if( ! cls.subtypeOf( ByteCodeUtils.getType( "it.neverworks.model.description.Modelized" ) ) ) {
                    WeaverContext.debug( "Adding Modelized interface to " + cls.getName() );
                    CtClass modelInterface = ByteCodeUtils.getType( "it.neverworks.model.description.Modelized" );
                    cls.addInterface( modelInterface );
                }
                
            } else {
                for( CtConstructor constructor: cls.getDeclaredConstructors() ) {
                    //System.out.println( "Instrumenting sub constructor " + constructor.getLongName() );
                    if( ! constructor.hasAnnotation( Preserved.class ) ) {
                        constructor.instrument( new PropertyFieldEditor( instanceFieldName ) );
                    }
                    
                    //TODO: verificare se serve ancora
                    // constructor.insertBeforeBody( "{ " +
                    //     "storeModelInstance( $0." + instanceFieldName + " ); " +
                    //     "}" );
                    constructor.insertAfter( "{ " +
                        "if( $0.getClass() == " + cls.getName() + ".class ){ $0." + instanceFieldName + ".init(); };" +
                        "}" );
                }
                
            }

            // Crea il supporto per le proprieta'
            for( CtField field: cls.getDeclaredFields() ) {
                if( field.hasAnnotation( Property.class ) ) {
            
                    Synthesize synthesize = (Synthesize) field.getAnnotation( Synthesize.class );
                    
                    if( field.hasAnnotation( Modelize.class ) ) {
                        throw new Exception( "Incompatible @Property and @Modelize on " + cls.getName() + "." + field.getName() );
                    }

                    //TODO: sostituire con la lettura del metodo getter da PropertyDescriptor
                    String getterName = "get" + field.getName().substring( 0, 1 ).toUpperCase() + field.getName().substring( 1 );
                    String getterSignature = "()" + field.getSignature();
            
                    String getterBody = "{ "
                        // + modelInstanceRetrieval
                        // + "return ($r)mi.manager(\"" + field.getName() + "\").getFrontValue( mi );"
                        + "return ($r)$0." + instanceFieldName + ".manager(\"" + field.getName() + "\").getFrontValue( $0." + instanceFieldName + " );"
                        + "}";

                    try {
                        // E' preferibile ricercare con getMethod perche' consente di specificare
                        // la firma completa
                        CtMethod getterMethod = cls.getMethod( getterName, getterSignature );
                    
                        // E' necessario verificare se il metodo e' dichiarato in questa classe
                        // perche' getMethod ricerca nell'intera gerarchia
                        if( ! getterMethod.hasAnnotation( Preserved.class ) && getterMethod.getDeclaringClass().equals( cls ) ) {
                            getterMethod.setBody( getterBody );
                        }
                
                    } catch( NotFoundException ex ) {
                        if( synthesize != null && synthesize.readable() ) {
                            WeaverContext.debug( "Synthesizing getter for " + cls.getName() + "." + field.getName() );
                            CtMethod newGetterMethod = new CtMethod( field.getType(), getterName, new CtClass[]{}, cls );
                            newGetterMethod.setBody( getterBody );
                            cls.addMethod( newGetterMethod );
                            ByteCodeUtils.mark( newGetterMethod, "synthesized" );
                        }    
                        
                    }
            
                    //TODO: sostituire con la lettura del metodo setter da PropertyDescriptor
                    String setterName = "set" + field.getName().substring( 0, 1 ).toUpperCase() + field.getName().substring( 1 );
                    String setterSignature = "(" + field.getSignature() + ")V";
                    String setterBody = "{ "
                        // + modelInstanceRetrieval
                        // + "mi.manager(\"" + field.getName() + "\").setFrontValue( mi, " + ByteCodeUtils.boxValue( field, "$1" ) + " );"
                        // + "System.out.println( \"Manager: \" + $0. " +  descriptorFieldName + " );"
                        + "$0." + instanceFieldName + ".manager(\"" + field.getName() + "\").setFrontValue( $0." + instanceFieldName  + ",  "+ ByteCodeUtils.boxValue( field, "$1" ) + " );"
                        + "}";
            
                    try {
                        // E' preferibile ricercare con getMethod perche' consente di specificare
                        // la firma completa
                        CtMethod setterMethod = cls.getMethod( setterName, setterSignature );
                    
                        // E' necessario verificare se il metodo e' dichiarato in questa classe
                        // perche' getMethod ricerca nell'intera gerarchia
                        if( ! setterMethod.hasAnnotation( Preserved.class ) && setterMethod.getDeclaringClass().equals( cls ) ) {
                            setterMethod.setBody( setterBody );
                        }

                    } catch( NotFoundException ex ) {
                        if( synthesize != null && synthesize.writable() ) {
                            WeaverContext.debug( "Synthesizing setter for " + cls.getName() + "." + field.getName() );
                            CtMethod newSetterMethod = new CtMethod( CtClass.voidType, setterName, new CtClass[]{ field.getType() }, cls );
                            newSetterMethod.setBody( setterBody );
                            cls.addMethod( newSetterMethod );
                            ByteCodeUtils.mark( newSetterMethod, "synthesized" );
                        }    
                    }
            
                }
    
            }
            
            // Aggiunge un'annotazione per tracciare il lavoro del weaver
            //ByteCodeUtils.createAnnotation( cls, "it.neverworks.bytecode.PropertyWoven" ).annotate( cls );
            ByteCodeUtils.mark( cls, "model" );
    
            // Congela la classe per catturare eventuali errori di weaving
            ByteCodeUtils.write( cls );
            
        }
        
    }
    
    private Set<String> ownProperties( CtClass cls ) throws Exception {
        Set<String> props = new HashSet<String>();
        for( CtField field: cls.getDeclaredFields() ) {
            if( field.getAnnotation( Property.class ) != null ) {
                props.add( field.getName() );
            }
        }
        if( !cls.getSuperclass().getName().equals( "java.lang.Object" ) ) {
            props.addAll( ownProperties( cls.getSuperclass() ) );
        } 
        return props;
    }
    
    public static class PropertyFieldEditor extends ExprEditor {
        
        private String instanceFieldName;
        
        public PropertyFieldEditor( String instanceFieldName ) {
            super();
            this.instanceFieldName = instanceFieldName;
        }
        
        public void edit( FieldAccess fa ) {
            try {
                if( fa.isWriter()
                    && ! fa.getField().getType().isPrimitive() 
                    && fa.getField().getAnnotation( Property.class ) != null 
                    && fa.getField().getAnnotation( Default.class ) != null ) {
                    
                    String name = fa.getField().getName();
                    Default def = (Default) fa.getField().getAnnotation( Default.class );
                    
                    if( def.value().equals( void.class ) 
                        && def.factory().equals( DefaultFactory.class ) ) {
                        
                        WeaverContext.debug( "Instrumenting @Default on " + fa.getField().getDeclaringClass().getName() + "." + name );
                    
                        ByteCodeUtils.createAnnotation( fa.getField().getDeclaringClass(), "it.neverworks.model.description.Default" )
                            .set( "factory", it.neverworks.model.description.InitValueDefaultFactory.class )
                            .annotate( fa.getField() );

                        String init = msg( "'{' (({0}) $0.{1}.module( {0}.class )).set(\"{2}\", {3} ); '}'"
                            ,"it.neverworks.model.description.InstanceInitValues"
                            ,instanceFieldName
                            ,name
                            ,ByteCodeUtils.boxValue( fa.getField(), "$1" ) 
                        );    

                        fa.replace( init );
                    }
                }

            } catch( Exception ex ) {
                ex.printStackTrace();
            }
        
        }
    }
}