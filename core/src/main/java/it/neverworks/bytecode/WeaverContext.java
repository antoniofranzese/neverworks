package it.neverworks.bytecode;

import javassist.ClassPool;
import javassist.CtClass;
import it.neverworks.lang.Strings;
import it.neverworks.log.Logger;
import it.neverworks.log.SystemOutLogger;

public class WeaverContext {
    
    private static ClassPool pool = ClassPool.getDefault();
    
    public static ClassPool getPool() {
        return pool;
    }
    
    public static void setPool( ClassPool aPool ) {
        pool = aPool;
    }

    private static String targetClassFolder = null;

    public static void setTargetClassFolder( String folder ) {
        targetClassFolder = folder;
    }

    public static void write( CtClass cls ) throws Exception {
        cls.freeze();
        if( targetClassFolder != null ) {
            cls.writeFile( targetClassFolder );
        } else {
            cls.writeFile();
        }
    }

    protected static Logger logger = new SystemOutLogger().withLevel( Logger.Level.INFO );

    public static Logger logger() {
        return logger;
    }

    public static void log( Object message ) {
        logger.info( Strings.valueOf( message ) );
    }

    public static void debug( Object message ) {
        logger.debug( Strings.valueOf( message ) );
    }

    public static void setVerbose( boolean verbose ) {
        logger = new SystemOutLogger().withLevel( verbose ? Logger.Level.DEBUG : Logger.Level.INFO );
    }

    public static void setLogger( Logger aLogger ){
        logger = aLogger;
    }

}