package it.neverworks.bytecode;

public interface Weaver {
    boolean instrument( String className ) throws Exception;
}