package it.neverworks.bytecode;

import javassist.ClassPool;
import javassist.Modifier;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtField;
import javassist.CtConstructor;
import javassist.NotFoundException;
import javassist.bytecode.ConstPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.StringMemberValue;

import java.util.List;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;
import it.neverworks.model.utils.Identification;

@AutoWeaver
public class IdentificationWeaver implements Weaver {
    private CtClass ObjectClass = ByteCodeUtils.getType( "java.lang.Object" );
    private CtClass StringClass = ByteCodeUtils.getType( "java.lang.String" );
    
    public boolean instrument( String className ) throws Exception {

        CtClass cls = ByteCodeUtils.getType( className );
        boolean instrumented = false;

        // Instrumenta solo le classi, se non sono state gia' trattate
        if( !cls.isInterface() 
            && cls.getAnnotation( Identification.class ) != null
            && ! ByteCodeUtils.isMarked( cls, "identification" ) ) {
            //&& cls.getAnnotation( IdentificationWoven.class ) == null ) {
            instrumentClass( cls );
        }
        return instrumented;
        
    }
    
    private void instrumentClass( CtClass cls ) throws Exception {
        int counter = 0;

        // Scongela la classe in esame
        if( cls.isFrozen() ) {
            cls.defrost();
        }

        ClassFile clsFile = cls.getClassFile();
        ConstPool constPool = clsFile.getConstPool();

        Identification annotation = (Identification) cls.getAnnotation( Identification.class );
        
        List<String> hash = annotation.hash() != null ? Collections.<String>toList( annotation.hash() ) : new ArrayList<String>();
        List<String> equals = annotation.equals() != null ? Collections.<String>toList( annotation.equals() ) : new ArrayList<String>();
        List<String> string = annotation.string() != null ? Collections.<String>toList( annotation.string() ) : new ArrayList<String>();
        
        if( annotation.common() != null && annotation.common().length > 0 ) {
            for( int i = annotation.common().length - 1; i >= 0; i-- ) {
                if( ! hash.contains( annotation.common()[ i ] ) ) {
                    hash.add( 0, annotation.common()[ i ] );
                }
                if( ! string.contains( annotation.common()[ i ] ) ) {
                    string.add( 0, annotation.common()[ i ] );
                }
                if( ! equals.contains( annotation.common()[ i ] ) ) {
                    equals.add( 0, annotation.common()[ i ] );
                }
            }

        } else if( annotation.value() != null ) {
            if( hash.size() == 0 ) {
                hash = Collections.<String>toList( annotation.value() );
            }
            if( equals.size() == 0 ) {
                equals = Collections.<String>toList( annotation.value() );
            }
            if( string.size() == 0 ) {
                string = Collections.<String>toList( annotation.value() );
            }
        }

        List<String> added = list();
        
        if( hash.size() > 0 ) {
            try {
                cls.getDeclaredMethod( "hashCode" );
            } catch( NotFoundException ex ) {
                added.add( "hashCode" );
                
                String body = "{ return new it.neverworks.model.utils.HashCodeBuilder( this )";
                for( String name: hash ) {
                    body += ".add( \"" + ( name.startsWith( "!" ) ? name.substring( 1 ) : name ) + "\" )";
                }
                body += ".hashCode(); }";
                
                CtMethod hashMethod = new CtMethod( CtClass.intType, "hashCode", new CtClass[]{}, cls );
                hashMethod.setBody( body );
                cls.addMethod( hashMethod );
                
                ByteCodeUtils.mark( hashMethod, "synthesized" );
            }
        }

        try {
            cls.getDeclaredMethod( "toString" );
        } catch( NotFoundException ex ) {
            added.add( "toString" );
            
            String body = "{ return new it.neverworks.model.utils.ToStringBuilder( this )";
            for( String name: string ) {
                body += ".add( \"" + name + "\" )";
            }
            body += ".toString(); }";
            
            CtMethod stringMethod = new CtMethod( StringClass, "toString", new CtClass[]{}, cls );
            stringMethod.setBody( body );
            cls.addMethod( stringMethod );

            ByteCodeUtils.mark( stringMethod, "synthesized" );
        }

        if( equals.size() > 0 ) {
            try {
                cls.getDeclaredMethod( "equals", new CtClass[]{ ObjectClass } );

            } catch( NotFoundException ex ) {
                added.add( "equals" );
                
                String body = "{ return new it.neverworks.model.utils.EqualsBuilder( this )";
                for( String name: equals ) {
                    body += ".add( \"" + name + "\" )";
                }
                body += ".equals( $1 ); }";
                
                CtMethod equalsMethod = new CtMethod( CtClass.booleanType, "equals", new CtClass[]{ ObjectClass }, cls );
                equalsMethod.setBody( body );
                cls.addMethod( equalsMethod );

                ByteCodeUtils.mark( equalsMethod, "synthesized" );
            }
        }
        
        // Aggiunge un'annotazione per tracciare il lavoro del weaver
        //ByteCodeUtils.createAnnotation( cls, "it.neverworks.bytecode.IdentificationWoven" ).annotate( cls );
        ByteCodeUtils.mark( cls, "identification" );
        
        if( added.size() > 0 ) {
            WeaverContext.debug( "Creating identification methods for " + cls.getName() + " " + added );
        }
        
        // Congela la classe per catturare eventuali errori di weaving
        ByteCodeUtils.write( cls );
        
        
    }
    
}