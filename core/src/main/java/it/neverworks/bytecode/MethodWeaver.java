package it.neverworks.bytecode;

import javassist.ClassPool;
import javassist.Modifier;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtField;
import javassist.CtConstructor;
import javassist.NotFoundException;
import javassist.bytecode.ConstPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.StringMemberValue;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.description.MethodAnnotation;

@AutoWeaver
public class MethodWeaver implements Weaver {
    private final static String DESCRIPTOR = "it.neverworks.model.description.MethodDescriptor";
    
    public boolean instrument( String className ) throws Exception {

        CtClass cls = ByteCodeUtils.getType( className );
        boolean instrumented = false;

        // Instrumenta solo le classi, se non sono state gia' trattate
        //if( !cls.isInterface() && cls.getAnnotation( MethodWoven.class ) == null ) {
        if( !cls.isInterface() && ! ByteCodeUtils.isMarked( cls, "methods" ) ) {
            instrumentClass( cls );
        }
        return instrumented;
        
    }
    
    private void instrumentClass( CtClass cls ) throws Exception {
        boolean performed = false;
        int counter = 0;

        // Scongela la classe in esame
        if( cls.isFrozen() ) {
            cls.defrost();
        }

        ClassFile clsFile = cls.getClassFile();
        ConstPool constPool = clsFile.getConstPool();

        for( CtMethod method: cls.getDeclaredMethods() ) {
            boolean instrument = false;
            
            // Rileva se sul metodo sono presenti annotazioni di processo 
            // (annotate a loro volta con MethodAnnotation)
            for( Object annotation: method.getAnnotations() ) {
                if( ( annotation instanceof java.lang.annotation.Annotation ) 
                    && ((java.lang.annotation.Annotation) annotation).annotationType().getAnnotation( MethodAnnotation.class ) != null ) {
                    instrument = true;
                }
            }
            
            if( instrument ) {
                WeaverContext.debug( "Instrumenting method " + cls.getName() + "." + method.getName() );
                performed = true;
                
                String classSpec = cls.getName().replaceAll( "\\.", "\\$" );
                // Per gestire eventuali overload, il campo del descriptor e' numerato e qualificato con la declaring class
                String fieldName = "__neverworks$model$" + classSpec + "$method" + String.valueOf( counter++ ) + "__";
                String methodName = method.getName();
                CtClass[] exceptionTypes = method.getExceptionTypes();

                // Annota il metodo originale con @RawMethod, rendendolo riconoscibile dal MethodDescriptor
                ByteCodeUtils.createAnnotation( cls, "it.neverworks.bytecode.RawMethod" )
                    .set( "peer", methodName )
                    .set( "field", fieldName )
                    .annotate( method );

                // Rinomina il metodo originale
                method.setName( classSpec + "$raw$" + method.getName() );

                // Campo privato che conterra' il MethodDescriptor 
                CtField descriptor = new CtField( ByteCodeUtils.getType( DESCRIPTOR ), fieldName, cls );
                descriptor.setModifiers( Modifier.STATIC + Modifier.PRIVATE );
                
                // Controlla la firma
                String sig = ByteCodeUtils.getJavaSignature( method );
                String typesInit = Strings.hasText( sig ) ? "new Class[]{ " + sig + " }" : "new Class[0]";

                // Il campo viene inizializzato alla creazione della classe
                String initializer = DESCRIPTOR + ".of( " + cls.getName() + ".class, \"" + method.getName() + "\", " + typesInit + " );";
                cls.addField( descriptor, CtField.Initializer.byExpr( initializer ) );

                // Crea il metodo frontale, con la sstessa firma del metodo originale
                CtMethod newMethod = new CtMethod( method.getReturnType(), methodName, method.getParameterTypes(), cls );
                newMethod.setExceptionTypes( exceptionTypes );
                
                String body = "{"
                    // + "System.out.println( \"Bytecode interceptor for " + methodName +" in " + cls.getName() +"\" ); "
                    // + "if( $0." + fieldName + "== null ){ $0." + fieldName + "=" + DESCRIPTOR + ".of( " + cls.getName() +".class, \"" + methodName + "$original\", $sig ); }"
                    + "return ($r)$0." + fieldName + ".invoke( $0, $args );"
                    + "}";
                
                newMethod.setBody( body );
                cls.addMethod( newMethod );
                
                // Annota il metodo frontale con @FrontMethod, rendendolo riconoscibile dal MethodDescriptor
                ByteCodeUtils.createAnnotation( cls, "it.neverworks.bytecode.FrontMethod" )
                    .set( "peer", method.getName() )
                    .set( "field", fieldName )
                    .annotate( newMethod );
                
            }
        }
        
        //cls.addField( stat, CtField.Initializer.byExpr( DESCRIPTOR + ".of( " + cls.getName() + ".class, \"interceptedMakeName$original\", new Class[]{ String.class } );"));
        
        // CtConstructor initializer = cls.makeClassInitializer();
        // initializer.insertBefore( "{if( testfield == null ) testfield=" + DESCRIPTOR + ".of( " + cls.getName() + ".class, \"interceptedMakeName$original\", new Class[]{ " + ByteCodeUtils.getJavaSignature( method )+" } );}" );
        
        if( performed ) {
            // Aggiunge un'annotazione per tracciare il lavoro del weaver
            //ByteCodeUtils.createAnnotation( cls, "it.neverworks.bytecode.MethodWoven" ).annotate( cls );
            ByteCodeUtils.mark( cls, "methods" );
            
            // Congela la classe per catturare eventuali errori di weaving
            ByteCodeUtils.write( cls );
            
        }
        
    }
    
}