package it.neverworks.bytecode;

import java.util.List;
import java.util.ArrayList;

import javassist.ClassPool;
import javassist.Modifier;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtField;
import javassist.CtConstructor;
import javassist.NotFoundException;
import javassist.bytecode.ConstPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.StringMemberValue;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.description.MethodAnnotation;
import it.neverworks.model.description.Required;
import it.neverworks.bytecode.ByteCodeUtils;
import it.neverworks.bytecode.Weaver;
import it.neverworks.bytecode.WeaverException;
import it.neverworks.model.context.Inject;

@AutoWeaver
public class StaticsWeaver implements Weaver {
    
    public boolean instrument( String className ) throws Exception {

        CtClass cls = ByteCodeUtils.getType( className );
        boolean instrumented = false;

        if( !cls.isInterface() && ! ByteCodeUtils.isMarked( cls, "statics" ) ) {
            try {
                instrumentClass( cls );
            } catch( Exception ex ) {
                throw new WeaverException( cls, ex );
            }
        }
        return instrumented;
        
    }
    
    private void instrumentClass( CtClass cls ) throws Exception {
        boolean performed = false;
        
        if( cls.isFrozen() ) {
            cls.defrost();
        }

        ClassFile clsFile = cls.getClassFile();
        ConstPool constPool = clsFile.getConstPool();

        List<CtField> instrumentables = new ArrayList<CtField>();
        CtField[] fields = cls.getDeclaredFields();
        for( int i: range( fields ) ) {
            if( ( fields[ i ].getModifiers() & Modifier.STATIC ) != 0 
                && fields[ i ].hasAnnotation( Inject.class ) ) {

                instrumentables.add( fields[ i ] );
            }
        }
        
        if( instrumentables.size() > 0 ) {
            
            WeaverContext.debug( "Instrumenting static properties on " + cls.getName() );

            StringBuilder body = new StringBuilder( "{\n" );
            for( CtField instrumentable: instrumentables ) {
                Inject inject = (Inject) instrumentable.getAnnotation( Inject.class );
                
                String[] beanNames = inject.value();
                for( int i = 0; i < beanNames.length; i++ ) {
                    if( Strings.isEmpty( beanNames[ i ] ) || ".".equals( beanNames[ i ].trim() ) ) {
                        beanNames[ i ] = instrumentable.getName();
                    } else {
                        beanNames[ i ] = beanNames[ i ].trim();
                    }
                }
                
                body.append( msg( "{0} = ({1}) it.neverworks.model.context.Injects.{2}( {3}, {1}.class ); \n"
                    ,instrumentable.getName()
                    ,instrumentable.getType().getName() 
                    ,instrumentable.hasAnnotation( Required.class ) ? "require" : "lookup"
                    ,beanNames.length > 1 
                        ? "new String[] { " + each( beanNames ).map( s -> "\"" + s + "\"" ).list().join( ", " ) + " }"
                        : "\"" + beanNames[ 0 ] + "\""
                ));
                
                ByteCodeUtils.mark( instrumentable, "statics.inject" );
            }
            body.append( "}" );

            CtConstructor initializer = cls.getClassInitializer();
            if( initializer == null ) {
                initializer = cls.makeClassInitializer();
            }
            initializer.insertBefore( body.toString() );
            
            ByteCodeUtils.mark( cls, "statics" );
        
            ByteCodeUtils.write( cls );
            
            
        }
        
    }
    
}