package it.neverworks.test;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Description;

import java.util.Collection;
import java.util.ArrayList;

import static it.neverworks.language.*;
import it.neverworks.lang.Collections;

public class TestUtils {
    
    private static class IterableMatcher extends BaseMatcher {
        
        protected Object[] values;
        protected String error = null;
        
        public IterableMatcher( Object[] values ) {
            this.values = values;
        }
        
        public boolean matches( Object item ) {
            Object[] sources; 
            try {
                sources = Collections.toArray( item );
            } catch( IllegalArgumentException ex ) {
                error = "Unlistable item: " + item.getClass().getName();
                return false;
            }
            
            if( sources.length != values.length ) {
                error = "Length mismatch, expected " + String.valueOf( values.length ) + ", actual " + String.valueOf( sources.length );
                return false;
            }
            
            if( ! matchItems( sources, values ) ) {
                return false;
            }
            
            return true;
        } 
        
        public boolean matchItems( Object[] sources, Object[] values ) {
            for( int i = 0; i < sources.length; i++ ) {
                if( sources[ i ] == null ? values[ i ] != null : !sources[i].equals( values[ i ] ) ) {
                    error = "Item " + String.valueOf( i ) + " mismatch, expected " + String.valueOf( values[ i ] ) + ", actual " + String.valueOf( sources[ i ] );
                    return false;
                }
            }
            return true;
        }
 
        public void describeTo( Description description) {
            StringBuilder sb = new StringBuilder();

            sb.append( "[" );
            for( int i = 0; i < values.length; i++ ) {
                sb.append( String.valueOf( values[ i ] ) );
                if( i < ( values.length - 1 ) ) {
                    sb.append( ", " );
                }
            }
            sb.append( "]" );
            if( error != null ) {
                sb.append( " (" ).append( error ).append( ")" );
            }
            
            description.appendText( sb.toString() );
        }
    }
    
    private static class IterableUnorderedMatcher extends IterableMatcher {

        public IterableUnorderedMatcher( Object[] values ) {
            super( values );
        }

        public boolean matchItems( Object[] sources, Object[] values ) {
            for( int i = 0; i < sources.length; i++ ) {
                boolean found = false;

                for( int j = 0; j < values.length; j++ ) {
                    if( sources[ i ] == null ? values[ j ] == null : sources[ i ].equals( values[ j ] ) ) {
                        found = true;
                    }
                }

                if( ! found ) {
                    error = "Expected '" + sources[ i ] +"' item not found in: " + list().cat( values ).toString();
                    return false;
                }
            }
            return true;
        }
    }
    
    public static Matcher equalsAll( Object... objects ) {
        return new IterableMatcher( objects );
    }

    public static Matcher containsAll( Object... objects ) {
        return new IterableUnorderedMatcher( objects );
    }
    
    public static void print( Object... objects ) {
        
        StringBuilder b = new StringBuilder();
        for( Object obj: objects ) {
            b.append( obj ).append( " " );
        }
        
        System.out.println( b );
        
    }
}