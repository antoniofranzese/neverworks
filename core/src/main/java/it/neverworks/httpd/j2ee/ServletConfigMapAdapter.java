package it.neverworks.httpd.j2ee;

import java.util.Set;
import javax.servlet.ServletConfig;

import it.neverworks.lang.MapLike;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;

public class ServletConfigMapAdapter implements MapLike<String,String> {
    private ServletConfig config;
    
    public ServletConfigMapAdapter( ServletConfig config ) {
        super();
        this.config = config;
    }
    
    @Override
	public Set<String> keySet() {
		return Collections.set( config.getInitParameterNames() );
	}
    
    @Override
    public String get( Object key ) {
        return config.getInitParameter( Strings.valueOf( key ) );
    }
    
}

