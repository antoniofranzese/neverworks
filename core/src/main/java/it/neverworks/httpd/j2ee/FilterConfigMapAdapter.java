package it.neverworks.httpd.j2ee;

import java.util.Set;
import javax.servlet.FilterConfig;

import it.neverworks.lang.MapLike;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;

public class FilterConfigMapAdapter implements MapLike<String,String> {
    private FilterConfig config;

    public FilterConfigMapAdapter( FilterConfig config ) {
        super();
        this.config = config;
    }

    @Override
	public Set<String> keySet() {
		return Collections.set( config.getInitParameterNames() );
	}

    @Override
    public String get( Object key ) {
        return config.getInitParameter( Strings.valueOf( key ) );
    }

}
