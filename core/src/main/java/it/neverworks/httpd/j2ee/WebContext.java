package it.neverworks.httpd.j2ee;

import java.util.Map;

import javax.servlet.ServletException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanFactory;

import it.neverworks.lang.Properties;
import it.neverworks.lang.Errors;

public class WebContext {
    
    private WebApplicationContext wac;
    
    public WebContext( WebApplicationContext wac ) {
        this.wac = wac;
    }
    
    public void inject( Object target, Map<String,String> config ) throws ServletException {
        for( String name: config.keySet()  ) {
            String value = config.get( name );
            if( wac.containsBean( value ) ) {
                try {
                    Properties.set( target, name, wac.getBean( value ) );
                } catch( Exception ex ) {
                    Exception unwrapped = Errors.unwrap( ex );
                    throw new ServletException( "Error injecting '" + name + "' property on " + target.getClass().getName() + ": " + unwrapped.getMessage(), unwrapped );
                }

            } else {
                try {
                    Properties.set( target, name, value );
                } catch( Exception ex ) {
                    Exception unwrapped = Errors.unwrap( ex );
                    throw new ServletException( "Error setting '" + name + "' property on " + target.getClass().getName() + ": " + unwrapped.getMessage(), unwrapped );
                }
            }
        }
        
        if( target instanceof ApplicationContextAware ) {
            ((ApplicationContextAware) target).setApplicationContext( this.wac );
        }

        if( target instanceof BeanFactoryAware  ) {
            ((BeanFactoryAware) target).setBeanFactory( (BeanFactory) this.wac );
        }

        if( target instanceof ApplicationEventPublisherAware && this.wac instanceof ApplicationEventPublisher ) {
            ((ApplicationEventPublisherAware) target).setApplicationEventPublisher( (ApplicationEventPublisher) this.wac );
        }
        
        if( target instanceof InitializingBean ) {
            try {
                ((InitializingBean) target).afterPropertiesSet();
            } catch( Exception ex ) {
                throw new ServletException( ex );
            }
        }
        
    }
    
}