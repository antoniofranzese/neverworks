package it.neverworks.httpd.j2ee;

import java.util.Set;
import javax.servlet.http.HttpSession;
import it.neverworks.lang.MapLike;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;

public class HttpSessionMapAdapter implements MapLike<String,Object> {
    private HttpSession session;

    public HttpSessionMapAdapter( HttpSession session ) {
        super();
        this.session = session;
    }

    @Override
	public Set<String> keySet() {
		return Collections.set( session.getAttributeNames() );
	}

    @Override
    public Object get( Object key ) {
        return session.getAttribute( Strings.valueOf( key ) );
    }

}
