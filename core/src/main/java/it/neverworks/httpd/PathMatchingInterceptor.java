package it.neverworks.httpd;

import java.util.regex.Pattern;
import it.neverworks.lang.Strings;

public abstract class PathMatchingInterceptor extends PathMatchingHandler implements ChainingHandler {

    protected Handler next;
    protected boolean initialized = false;
    
    public void dispatch( Request request ) throws Exception {
        if( ! this.initialized ) {
            synchronized( this ) {
                if( ! this.initialized ) {
                    this.init();
                    this.initialized = true;
                }
            }
        }
        if( next != null ) {
            next.handle( request );
        }
    }

    protected void init() {
        this.next = this.propagatePath( this.next );
    }

    public Handler getNext(){
        return this.next;
    }
    
    public void setNext( Handler next ){
        this.next = next;
    }
    
}