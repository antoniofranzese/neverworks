package it.neverworks.httpd;

import javax.servlet.ServletException;
import javax.servlet.Filter;
import javax.servlet.FilterConfig;

import org.springframework.web.context.support.WebApplicationContextUtils;

import it.neverworks.httpd.j2ee.WebContext;
import it.neverworks.httpd.j2ee.FilterConfigMapAdapter;

public abstract class InjectableFilter implements Filter {

    @Override
    public void init( FilterConfig config ) throws ServletException {
        new WebContext( WebApplicationContextUtils.getRequiredWebApplicationContext( config.getServletContext() ) )
            .inject( this, new FilterConfigMapAdapter( config ) );
    }
    
    public void destroy(){}

    
}