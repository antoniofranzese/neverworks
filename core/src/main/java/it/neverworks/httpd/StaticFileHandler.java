package it.neverworks.httpd;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.util.Map;
import java.util.HashMap;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;

import it.neverworks.model.Model;

public class StaticFileHandler implements Model, Handler {
    
    protected String path;
    protected String folder;
    protected Map<String, String> extensions;
    protected boolean inferMimeTypes = false;
    protected String defaultMimeType = "application/octet-stream";

    public StaticFileHandler() {
        Map<String,String> ext = new HashMap<String, String>();
        ext.put( "css",  "text/css; encoding=UTF-8" );
        ext.put( "gif",  "image/gif" );
        ext.put( "htm",  "text/html; encoding=UTF-8" );
        ext.put( "html", "text/html; encoding=UTF-8" );
        ext.put( "ico",  "image/x-icon" );
        ext.put( "jpeg", "image/jpeg" );
        ext.put( "jpg",  "image/jpeg" );
        ext.put( "js",   "application/javascript" );
        ext.put( "less", "text/plain; encoding=UTF-8" );
        ext.put( "ogg",  "audio/ogg" );
        ext.put( "png",  "image/png" );
        ext.put( "pdf",  "application/pdf" );
        ext.put( "svg",  "image/svg+xml" );
        ext.put( "wav",  "audio/wav" );
        ext.put( "xml",  "text/xml" );
        this.extensions = ext;
    }
    
    public boolean claims( Request request ) {
        return request.getPath().startsWith( this.path );
    }
    
    public void handle( Request request ) throws Exception {
        //System.out.println( "REQUEST: " + request.getMethod() + " " + request.getPath() );
        if( request.getMethod() == Method.GET ) {
            doGet( request );

        } else if( request.getMethod() == Method.POST ) {
            doPost( request );
            
        } else {
            request.getResponse().setCode( "400" ); // Unavailable
        }
    }
    
    protected void doGet( Request request ) throws Exception {
        
        // Strip the server part
        String localPath = request.getPath().substring( this.path.length() );

        if( localPath.length() > 0 ) {
            streamFile( request, localPath );
        } else {
            request.getResponse().setCode( "400" ); // Unavailable
        }
    }
    
    protected void doPost( Request request ) throws Exception {
        request.getResponse().setCode( "400" ); // Unavailable
    }
    
    protected void streamFile( Request request, String localPath ) throws Exception {
        Response response = request.getResponse();
        String fullPath = this.folder + ( !this.folder.endsWith( "/" ) && !localPath.startsWith( "/" ) ? "/" : "" ) + localPath;
        File file = new File( fullPath );

        if( file.exists() ) {
            String mimeType = null;

            // Search known extensions
            if( fullPath.indexOf( "." ) > 0 ) {
                String extension = fullPath.substring( fullPath.lastIndexOf( "." ) + 1 );
                if( this.extensions.containsKey( extension ) ) {
                    mimeType = this.extensions.get( extension );
                }
            }
            
            // Try to infer mime type
            if( mimeType == null && this.inferMimeTypes ) {
                MagicMatch match = null;
                try {
                    match = Magic.getMagicMatch( file, /* extensionHints */ true, /* onlyMimeMatch */ false);
                } catch( MagicMatchNotFoundException ex ) {
                    match = null;
                }

                if( match != null ) {
                    mimeType = match.getMimeType();
                }
            }
            
            // Fallback
            if( mimeType == null ) {
                mimeType = this.defaultMimeType;
            }
            
            response.setHeader( "Content-Type", mimeType );
            response.setHeader( "Content-Length", String.valueOf( file.length() ) );
            setHeaders( response, localPath );
            response.setCode( "200" );
            
            InputStream stream = new FileInputStream( file );
            response.stream( stream );
            stream.close();
            
        } else {
            response.setCode( "404" ); // Not found
        }
        
    }
    
    protected void setHeaders( Response response, String localPath ) {
        
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
    
    public String getFolder(){
        return this.folder;
    }
    
    public void setFolder( String folder ){
        this.folder = folder;
    }
    
    public boolean getInferMimeTypes(){
        return this.inferMimeTypes;
    }
    
    public void setInferMimeTypes( boolean inferMimeTypes ){
        this.inferMimeTypes = inferMimeTypes;
    }
    
    public String getDefaultMimeType(){
        return this.defaultMimeType;
    }
    
    public void setDefaultMimeType( String defaultMimeType ){
        this.defaultMimeType = defaultMimeType;
    }
    
}