package it.neverworks.httpd;

import java.util.ArrayList;
import java.util.List;

public class Dispatcher {
    
    private List<Handler> handlers = new ArrayList<Handler>();

    public Dispatcher() {
        super();
        this.setup();
    }
    
    public void setup() {}
    
    public void handle( Request request ) throws Exception {
        Handler handler = null;
        for( Handler candidate: this.handlers ) {
            if( candidate.claims( request ) ){
                handler = candidate;
                break;
            }
        }
        if( handler != null ) {
            try {
                handler.handle( request );
            } catch( Exception ex ) {
                //TODO: gestire meglio
                ex.printStackTrace();
                // request.getResponse().setCode( "500" );
                // request.getResponse().flush();
                throw ex;
            }
            request.getResponse().flush();
        } else {
            throw new Exception( "No handler for " + request.getPath() );
        }
        
    }
    
    public Dispatcher addHandler( Handler handler ) {
        this.handlers.add( handler );
        return this;
    }
    
    public List<Handler> getHandlers(){
        return this.handlers;
    }
    
    public void setHandlers( List<Handler> handlers ){
        this.handlers = handlers;
    }

}
