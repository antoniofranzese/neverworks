package it.neverworks.httpd;

public interface Handler {
    
    public boolean claims( Request request );
    public void handle( Request request ) throws Exception;
    
}