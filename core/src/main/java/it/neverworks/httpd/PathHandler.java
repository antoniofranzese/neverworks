package it.neverworks.httpd;

import it.neverworks.lang.Strings;

public interface PathHandler extends Handler {
    String getPath();
    void setPath( String path );

    default Handler propagatePath( Handler handler ) {
        if( Strings.hasText( this.getPath() )
            && handler instanceof PathHandler 
            && Strings.isEmpty( ((PathHandler) handler).getPath() ) ) {
            ((PathHandler) handler).setPath( this.getPath() );
        }
        return handler;
    }

}