package it.neverworks.httpd;

import java.util.regex.Pattern;
import it.neverworks.model.Model;

public abstract class PathMatchingHandler implements Model, Handler, PathHandler {
    
    protected String path;
    protected Pattern regex;
    
    public boolean claims( Request request ) {
        if( this.regex != null ) {
            //System.out.println( "Matching " + request.getPath() + " to " + this.path );
            return regex.matcher( request.getPath() ).matches();
        } else {
            //System.out.println( "No matcher" );
            return false;
        }
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
        if( path != null ) {
            this.regex = Pattern.compile( path );
        } else {
            this.regex = null;
        }
    }
    
    public Pattern getRegex(){
        return this.regex;
    }
}