package it.neverworks.httpd;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletConfig;

import org.springframework.web.context.support.WebApplicationContextUtils;

import it.neverworks.httpd.j2ee.WebContext;
import it.neverworks.httpd.j2ee.ServletConfigMapAdapter;

public class InjectableServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        new WebContext( WebApplicationContextUtils.getRequiredWebApplicationContext( getServletContext() ) )
            .inject( this, new ServletConfigMapAdapter( getServletConfig() ) );
    }

}