package it.neverworks.httpd;

import static it.neverworks.language.*;

import java.util.List;

public class Chain implements Handler, PathHandler {
    
    protected String path;

    protected List<Handler> handlers;
    protected Handler first = null;
    protected boolean initialized = false;

    public boolean claims( Request request ) {
        this.init();
        if( this.first != null ) {
            return this.first.claims( request );
        } else {
            return false;
        }
    }
    
    public void handle( Request request ) throws Exception {
        if( ! this.initialized ) {
            synchronized( this ) {
                if( ! this.initialized ) {
                    this.init();
                    this.initialized = true;
                }
            }
        }

        if( this.first != null ) {
            this.first.handle( request );
        } 
    }
    
    protected void init() {
        if( this.handlers != null && this.handlers.size() > 0 ) {
            this.first = this.propagatePath( this.handlers.get( 0 ) );

            if( this.handlers.size() > 1 ) {
                Handler current = this.first;
                for( int i: range( 1, this.handlers.size() ) ) {
                    if( current instanceof ChainingHandler ) {
                        ((ChainingHandler) current).setNext( this.handlers.get( i ) );
                        current = this.handlers.get( i );
                    } else {
                        throw new IllegalStateException( msg( "Cannot chain {0.class.name} handler to {1.class.name} non-chaining handler", this.handlers.get( i ), current  ) );
                    }
                }
            }
        }
    }

    public List<Handler> getHandlers(){
        return this.handlers;
    }
    
    public void setHandlers( List<Handler> handlers ){
        this.handlers = handlers;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }

}