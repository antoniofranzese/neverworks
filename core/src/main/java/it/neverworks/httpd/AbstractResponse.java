package it.neverworks.httpd;

import static it.neverworks.language.*;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.Dates;
import it.neverworks.lang.Arguments;

public abstract class AbstractResponse implements Response {
    
    protected List<String> cookies;
    
    
    public void setCookie( String name, String value, Arguments arguments ) {
        within( sync( this, t -> t.cookies == null ), t ->{
            this.cookies = new ArrayList<String>();
        });
        
        Date expires = arguments.get( type( Date.class ), "expires" );
        Number age = arguments.get( type( Integer.class), "age" );
        String path = arguments.get( type( String.class ), "path" );
        String domain = arguments.get( type( String.class ), "domain" );

        StringBuilder cookie = new StringBuilder();
        cookie.append( name ).append( "=" ).append( value );

        if( expires != null ) {
            cookie.append( "; Expires=" ).append( Dates.toHTTP( expires ) );
        } else if( age != null ) {
            cookie.append( "; Expires=" ).append( Dates.toHTTP( Dates.DateTime().plusSeconds( age.intValue() ) ) );
        }
        
        if( ! empty( path ) ) {
            cookie.append( "; Path=" ).append( path );
        }

        if( ! empty( domain ) ) {
            cookie.append( "; Domain=" ).append( path );
        }
        
        this.cookies.add( cookie.toString() );
        
    }

}