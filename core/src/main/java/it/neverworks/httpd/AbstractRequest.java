package it.neverworks.httpd;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.HTTP;

public abstract class AbstractRequest implements Request {
 
    protected Arguments cookies;
    protected Arguments arguments;
 
    public Arguments getCookies() {
        if( this.cookies == null ) {
            String header = getHeader( "Cookie" );
            if( Strings.hasText( header ) ) {
                this.cookies = Arguments.parse( header, '=', ';' );
            } else {
                this.cookies = new Arguments();
            }
        }
        return this.cookies;
    }

    public String getCookie( String name ) {
        return Strings.hasText( name ) ? Strings.plane( this.getCookies().get( name.trim() ) ) : null;
    }
    
    public Arguments getArguments() {
        if( this.arguments == null ) {
            this.arguments = HTTP.queryArguments( this.getQueryString() );
        }
        return this.arguments;
    }

    public String getArgument( String name ) {
        return Strings.hasText( name ) ? getArguments().get( name ) : null;
    }
   
}