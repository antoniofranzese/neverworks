package it.neverworks.httpd;

public interface ChainingHandler extends Handler {
    Handler getNext();
    void setNext( Handler next );
}