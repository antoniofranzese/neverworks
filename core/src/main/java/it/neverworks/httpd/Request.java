package it.neverworks.httpd;

import java.io.InputStream;
import java.util.Map;

import it.neverworks.lang.Arguments;

public interface Request {
    
    public Method getMethod();
    
    public String getPath();
    public String getQueryString();
    
    public Response getResponse();
    
    public String getBodyAsString();
    public InputStream getBodyAsStream();
    
    public String getHeader( String name );
    public Map<String,String> getHeaders();
    
    public String getCookie( String name );
    public Arguments getCookies();

    public Arguments getArguments();
    public String getArgument( String name );

}