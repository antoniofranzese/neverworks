package it.neverworks.httpd;

import java.util.Map;
import org.springframework.context.ApplicationEvent;

public class SessionCreatedEvent extends ApplicationEvent {
    
    Map<String, Object> session;
    
    public SessionCreatedEvent( Object source, Map<String,Object> session ) {
        super( source );
        this.session = session;
    }
    
    public Map<String, Object> getSession(){
        return this.session;
    }
}