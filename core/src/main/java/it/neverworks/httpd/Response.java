package it.neverworks.httpd;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;

import it.neverworks.lang.Arguments;

public interface Response {

    public void setCode( String code );
    public void setHeader( String name, String value );
    public void write( Object object ) throws Exception;
    public void stream( InputStream stream ) throws Exception;
    public void flush() throws Exception;
    public Writer getWriter() throws Exception;
    public OutputStream getStream() throws Exception;

    default void setCookie( String name, String value ) {
        setCookie( name, value, new Arguments() );
    }
    
    public void setCookie( String name, String value, Arguments arguments );

}