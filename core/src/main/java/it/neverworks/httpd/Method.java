package it.neverworks.httpd;

public enum Method {
    GET, POST, HEAD, PUT, DELETE, OPTIONS, UNKNOWN
}