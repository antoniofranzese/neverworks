package it.neverworks.httpd;

import java.io.InputStream;
import org.apache.commons.fileupload.RequestContext;

public class UploadRequestContext implements RequestContext {

    private Request request;
    
    public UploadRequestContext( Request request ) {
        this.request = request;
    }
        
    public String getCharacterEncoding() {
        return null;
    }
    
    public int	getContentLength() {
        String cl = this.request.getHeader( "content-length" );
        return cl != null ? Integer.parseInt( cl ) : 0;
    }

    public String getContentType() {
        return this.request.getHeader( "content-type" );
    }
    
    public InputStream getInputStream() {
        return this.request.getBodyAsStream();
    }
}
