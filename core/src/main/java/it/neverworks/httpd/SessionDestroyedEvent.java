package it.neverworks.httpd;

import java.util.Map;
import org.springframework.context.ApplicationEvent;

public class SessionDestroyedEvent extends ApplicationEvent {
    
    Map<String, Object> session;
    
    public SessionDestroyedEvent( Object source, Map<String,Object> session ) {
        super( source );
        this.session = session;
    }
    
    public Map<String, Object> getSession(){
        return this.session;
    }
}