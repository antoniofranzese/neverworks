package it.neverworks.template;

import static it.neverworks.language.*;

import java.io.IOException;
import java.io.Writer;
import java.io.FileWriter;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.resource.loader.StringResourceLoader;
import org.apache.velocity.runtime.resource.util.StringResourceRepository;
import org.apache.velocity.runtime.resource.util.StringResourceRepositoryImpl;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.io.StringWriter;
import it.neverworks.io.Streams;

public class VelocityTemplateEngine implements Model, TemplateEngine {

    private final static String STRING_REPO = "it.neverworks.repository.strings";
	protected final static Log log = LogFactory.getLog( VelocityTemplateEngine.class );
	protected final static ThreadLocal<VelocityEngine> localStreamEngine = new ThreadLocal<VelocityEngine>();
	protected final static ThreadLocal<VelocityEngine> localStaticEngine = new ThreadLocal<VelocityEngine>();
    VelocityContext baseContext = null;

    protected VelocityEngine staticEngine() {
        try {
            if( localStaticEngine.get() == null ) {
            
        		VelocityEngine engine = new VelocityEngine();

                if( templates != null ) {
            		if( log.isDebugEnabled() ) 
            			log.debug( "Using template path: " + templates.getFile().getAbsolutePath() );

                    engine.setProperty( Velocity.RESOURCE_LOADER, "file" );
            		engine.setProperty( "file.resource.loader.path", templates.getFile().getAbsolutePath() );
                
                }
                
        		engine.init();
            
                localStaticEngine.set( engine );
                
            }
            return localStaticEngine.get();
            
        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
        
    }
    
    protected VelocityEngine streamEngine() {
        if( localStreamEngine.get() == null ) {
        
    		VelocityEngine engine = new VelocityEngine();


            engine.setProperty( Velocity.RESOURCE_LOADER, "file" );
    		engine.setProperty( "file.resource.loader.path", System.getProperty( "java.io.tmpdir" ) );

            // engine.setProperty( Velocity.RESOURCE_LOADER, "string" );
            // engine.addProperty( "string.resource.loader.class", StringResourceLoader.class.getName() );
            // engine.addProperty( "string.resource.loader.description", "Velocity StringResource loader" );
            // engine.addProperty( "string.resource.loader.class", "org.apache.velocity.runtime.resource.loader.StringResourceLoader" );
            // engine.addProperty( "string.resource.loader.repository.name", STRING_REPO );
            // engine.addProperty( "string.resource.loader.repository.static", "false" );
            // StringResourceRepository repo = new StringResourceRepositoryImpl();
            // engine.setApplicationAttribute( STRING_REPO, repo );

    		engine.init();
        
            localStreamEngine.set( engine );
            
        }
        return localStreamEngine.get();
            
    }
	
	@Override
	public String apply( Object template, TemplateContext context ) {

		StringWriter writer = new StringWriter();
        merge( template, context, writer );

		return writer.toString();
	}

    public InputStream stream( Object template, TemplateContext context ) {
        try {
            File file = File.createTempFile( "nwk-velocity-stream-",".txt" );
            Writer fileWriter = new FileWriter( file );
        
            merge( template, context, fileWriter );
            fileWriter.flush();
            fileWriter.close();
        
            return new FileInputStream( file );
            
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }

    }
    
    protected void merge( Object template, TemplateContext context, Writer writer ) {
		String templateName = null;
        try {
    		VelocityContext vmcontext = ((VelocityTemplateContext)context).getVelocityContext();
            Template vmtemplate = null;

            if( template instanceof String && this.templates != null ) {
                try {
                
                    templateName = template + this.extension;    
        			vmtemplate = staticEngine().getTemplate( templateName );
            
        		} catch( ResourceNotFoundException e ) {
                    vmtemplate = null;
                }
            }
            
            if( vmtemplate == null ) {
                VelocityEngine engine = streamEngine();
                
                InputStream templateStream = null;
                if( template instanceof String ) {
                    templateStream = Streams.asStream( (String) template );
                } else if( template instanceof InputStream ) {
                    templateStream = (InputStream)template;
                } else {
                    throw new IllegalArgumentException( "Unknown template type: " + repr( template ) );
                }
                
                File file = File.createTempFile( "nwk-velocity-template-", this.extension );
                Streams.copy( templateStream, new FileOutputStream( file ), arg( "close", true ) );
                templateName = file.getName();
                
                vmtemplate = engine.getTemplate( templateName );
                
                // StringResourceRepository repo = (StringResourceRepository) engine.getApplicationAttribute( STRING_REPO );
                // System.out.println( "Repo: " + repo );
                // templateName = "static-string.vm";
                // repo.putStringResource( templateName, template );
                
            }
            
			vmtemplate.merge( vmcontext, writer );
            
		} catch (ParseErrorException e) {
			throw new RuntimeException( "Parse error in '" + Strings.limit( templateName, 50, "..." ) + "'", e );
		} catch (MethodInvocationException e) {
			throw new RuntimeException( "Method invocation error evaluating '" + Strings.limit( templateName, 50, "..." ) + "'", e );
		} catch (Exception e) {
			throw new RuntimeException( "Exception evaluating '" + Strings.limit( templateName, 50, "..." ) + "'", e );
		}
        
    }

	@Override
	public TemplateContext createContext() {
        VelocityTemplateContext ctx;
        if( baseContext != null ) {
    		ctx = new VelocityTemplateContext( baseContext );
        } else {
    		ctx = new VelocityTemplateContext();
        }
        ctx.put( "NWK", new VelocityTemplateUtilities() );
        return ctx;
	}

    @Property @AutoConvert
	private Resource templates;

	public void setTemplatePath( Resource resource ) {
		this.set( "templates", resource );
	}
    
    public void setTemplates( Resource resource ) {
        this.templates = resource;
    }
    
    @Property
    private String extension = ".vm";
    
    public void setExtension( String extension ) {
        this.extension = extension;
    }
    
    public void setContext( Map<String, Object> context ) {
        baseContext = new VelocityContext();
        for( Map.Entry<String, Object> entry: context.entrySet() ) {
            baseContext.put( entry.getKey(), entry.getValue() );
        }
    }

}
