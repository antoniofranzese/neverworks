package it.neverworks.template;

import static it.neverworks.language.*;

import java.util.Date;
import java.util.List;

import it.neverworks.lang.Dates;
import it.neverworks.lang.Range;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.Calculator;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.encoding.HTML;
import it.neverworks.context.Context;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.description.Synthesize;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.Property;
import it.neverworks.model.Model;
import it.neverworks.model.Value;

public class VelocityTemplateUtilities implements Model {

    public Class cls( String name ) {
        return Reflection.findClass( name );
    }
    
    public Date getNow() {
        return Dates.now();
    }

    public Date date() {
        return Dates.now();
    }
    
    public Date date( Object date ) {
        return Dates.date( date );
    }

    public Number number( Object number ) {
        return Numbers.number( number );
    }
    
    public String msg( String message, Object... parameters ) {
        return message( message, parameters );
    }

    public String message( String message, Object... parameters ) {
        return Strings.message( message, parameters );
    }

    public String format( String format, Object value ) {
        if( value != null ) {
            if( Numbers.isNumber( value ) ) {
                return Strings.message( "{0,number," + format + "}", value );
            } else if( Dates.isDate( value ) ) {
                return Strings.message( "{0,date," + format + "}", value );
            } else if( Booleans.isBoolean( value ) ) {
                return Strings.message( "{0,boolean," + format + "}", value );
            } else {
                return Strings.valueOf( value );
            }
        } else {
            return "";
        }
    }

    public TypeDefinition type( Object object ) {
        if( object instanceof Class ) {
            return TypeFactory.shared( (Class) object );
        } else if( object instanceof String ) {
            return TypeFactory.shared( (String) object );
        } else if( object != null ) {
            return TypeFactory.shared( object.getClass() );
        } else {
            throw new IllegalArgumentException( "Null type " );
        }
    }
    
    public ObjectQuery query( Object object ) {
        if( Collections.isIterable( object ) ) {
            return new ObjectQuery( Collections.iterable( object ) );
        } else {
            throw new IllegalArgumentException( "Cannot query " + Objects.repr( object) );
        }
    }
    
    public Object eval( Object root, String expression ) {
        return ExpressionEvaluator.evaluate( root, expression );
    }
    
    public void assign( Object root, String expression, Object value ) {
        ExpressionEvaluator.valorize( root, expression, value );
    }

    public Object bean( String name ) {
        return Context.get( name );
    }
    
    public Object get( String name ) {
        if( model().has( name ) ) {
            return model().get( name );
        } else {
            return Context.get( name );
        }
    }
    
    public boolean empty( Object object ) {
        return Value.isEmpty( object );
    }
    
    public int len( Object object ) {
        return Value.size( object );
    }

    public Range range( Object object ) {
        return Range.of( object );
    }

    public Range range( Object start, Object stop ){
        return Range.of( start, stop );
    }
    
    public static Calculator calc() {
        return new Calculator();
    }
    
    public static Calculator calc( Object value ) {
        return new Calculator().push( value );
    }
    
    @Property 
    private HTMLUtilities html = new HTMLUtilities(); 
    
    public class HTMLUtilities {
        
        public String encode( Object obj ) {
            return HTML.encode( obj ).replaceAll( "[\\n\\r]+", "<br/>" );
        }

        public String escape( Object obj ) {
            return HTML.escape( obj ).replaceAll( "[\\n\\r]+", "<br/>" );
        }

        public FluentList<String> lines( Object obj ) {
            return list().cat( Strings.safe( obj ).split( "<br\\s*/?>" ) ).as( String.class );
        }

    };

    @Property
    private DateUtilities date = new DateUtilities();

    public class DateUtilities {

        public Date getNow() {
            return Dates.now();
        }

        public Date parse( Object date ) {
            return Dates.date( date );
        }

        public String format( String format, Object date ) {
            return Strings.message( "{0,date," + format + "}", date );
        }
    
    }

    @Property
    private NumberUtilities number = new NumberUtilities();

    public class NumberUtilities {

        public Number parse( Object number ) {
            return Numbers.number( number );
        }

        public String format( String format, Object number ) {
            return Strings.message( "{0,number," + format + "}", number );
        }
    
        public Number round( Object number ) {
            return round( number, 0 );
        }

        public Number round( Object number, int decimals ) {
            return Numbers.round( number, decimals );
        }
    }

    
}