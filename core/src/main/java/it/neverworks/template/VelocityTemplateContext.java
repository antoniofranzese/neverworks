package it.neverworks.template;

import org.apache.velocity.VelocityContext;

public class VelocityTemplateContext implements TemplateContext {

	private VelocityContext context;
	
	public VelocityTemplateContext() {
		super();
		context = new VelocityContext();
	}

	public VelocityTemplateContext( VelocityContext baseContext ) {
		super();
		context = new VelocityContext( baseContext );
	}

	@Override
	public Object get(String key) {
		return context.get(key);
	}

	@Override
	public Object put(String key, Object value) {
		return context.put(key, value);
	}

	public VelocityContext getVelocityContext() {
		return context;
	}
	
	
}
