package it.neverworks.template;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import it.neverworks.lang.Strings;
import it.neverworks.model.features.Retrieve;

public class DateField implements Retrieve {
    
    private Date date;
    
    public DateField( Date date ) {
        this.date = date;
    }
    
    public Object retrieveItem( Object key ) {
        if( "short".equalsIgnoreCase( Strings.valueOf( key ) ) ) {
            return DateFormat.getDateInstance( DateFormat.SHORT ).format( date );
        } else if( "medium".equalsIgnoreCase( Strings.valueOf( key ) ) ) {
            return DateFormat.getDateInstance( DateFormat.MEDIUM ).format( date );
        } else if( "long".equalsIgnoreCase( Strings.valueOf( key ) ) ) {
            return DateFormat.getDateInstance( DateFormat.LONG ).format( date );
        } else if( "full".equalsIgnoreCase( Strings.valueOf( key ) ) ) {
            return DateFormat.getDateInstance( DateFormat.FULL ).format( date );
        } else {
            return new SimpleDateFormat( Strings.valueOf( key ) ).format( date );
        }
    }
    
    public String toString() {
        return DateFormat.getDateInstance( DateFormat.LONG ).format( date );
    }
}