package it.neverworks.template;

import java.util.Map;
import java.io.InputStream;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.Strings;

public interface TemplateEngine {
	TemplateContext createContext();
    
    default TemplateContext createContext( FluentMap map ) {
        TemplateContext ctx = this.createContext();
        for( Object key: map.keys() ) {
            ctx.put( Strings.valueOf( key ), map.get( key ) );
        }
        return ctx;
    }

    default TemplateContext createContext( Map map ) {
        TemplateContext ctx = this.createContext();
        for( Object key: map instanceof FluentMap ? ((FluentMap) map).keys() : map.keySet() ) {
            ctx.put( Strings.valueOf( key ), map.get( key ) );
        }
        return ctx;
    }
	
    String apply( Object template, TemplateContext context );
    InputStream stream( Object template, TemplateContext context );
	
    default String apply( Object template ) {
        return apply( template, this.createContext() );
    }
    
    default InputStream stream( Object template ) {
        return stream( template, this.createContext() );
    }

    default String apply( Object template, Map context ) {
        return apply( template, this.createContext( context ) );
    }
    
    default InputStream stream( Object template, Map context ) {
        return stream( template, this.createContext( context ) );
    }
    
    
}
