package it.neverworks.template;

public interface TemplateContext {
	Object get( String key );
	Object put( String key, Object value );
}
