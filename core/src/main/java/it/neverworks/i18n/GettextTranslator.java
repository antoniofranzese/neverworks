package it.neverworks.i18n;

import java.util.List;
import java.util.Arrays;
import java.util.Locale;
import java.util.ResourceBundle;

import it.neverworks.log.Logger;

import gnu.gettext.GettextResource;
import org.springframework.beans.factory.InitializingBean;
import it.neverworks.lang.Strings;

public class GettextTranslator implements Translator, InitializingBean {
    
    private final static Locale FALLBACK_LOCALE = Locale.ENGLISH; 
    private static final Logger logger = Logger.of( GettextTranslator.class );
    
    private Locale currentLocale; 
    private ResourceBundle bundle; 
    private String bundleName = "messages"; 
    private Locale defaultLocale = Locale.ENGLISH; 
    private List<Locale> supportedLocales = Arrays.asList( new Locale[]{ Locale.ENGLISH } );
    
    public String translate( String text ) {
        if( bundle != null && Strings.hasText( text ) ) {
            return GettextResource.gettext( bundle, text );
        } else {
            return text;
        }
    }
    
    public void select( Locale locale ) {
        try {
            bundle = ResourceBundle.getBundle( bundleName, locale );
        } catch( Exception ex ) {
            logger.error( "Cannot retrieve translation bundle ''{}'' for {}: {}", bundleName, locale, ex.getMessage() );
            bundle = null;
        }
        currentLocale = locale;
    }
    
    public boolean supports( Locale locale ) {
        return supportedLocales.contains( locale );
    }
    
    public Locale current() {
        return this.currentLocale != null ? this.currentLocale : ( this.defaultLocale != null ? this.defaultLocale : FALLBACK_LOCALE );
    }
    
    public void afterPropertiesSet() {
        select( defaultLocale );
    }
    
    public void setDefaultLocale( Locale defaultLocale ){
        this.defaultLocale = defaultLocale;
    }
    
    public void setBundleName( String bundleName ){
        this.bundleName = bundleName;
    }

    public void setSupportedLocales( List<Locale> supportedLocales ){
        this.supportedLocales = supportedLocales;
    }

}