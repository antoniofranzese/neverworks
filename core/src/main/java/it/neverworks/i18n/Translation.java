package it.neverworks.i18n;

import java.util.Locale;

import it.neverworks.lang.Guard;
import it.neverworks.model.types.TypeDefinition;

public class Translation {
    
    private static Translator translator;
    private static TypeDefinition<Locale> LocaleType = TypeDefinition.of( Locale.class );
    
    public static Guard<Translator> local( String locale ) {
        return local( LocaleType.process( locale ) );
    }

    public static Guard<Translator> local( Locale locale ) {
        return new TransientLocaleGuard( locale != null ? locale : Locale.getDefault() );
    }

    public static String translate( String message ) {
        return translator().translate( message );
    }

    public static void select( Locale locale ) {
        translator().select( locale );
    }

    public static void select( String locale ) {
        select( LocaleType.process( locale ) );
    }
    
    public static boolean supports( Locale locale ) {
        return translator().supports( locale );
    }
    
    public static Locale current() {
        try {
            return translator().current();
        } catch( Exception ex ) {
            return Locale.getDefault();
        }
    }
    
    public static Translator translator() {
        if( translator == null ) {
            translator = new NullTranslator();
        }
        
        return translator;
    }
    
    public void setTranslator( Translator tr ) {
        translator = tr;
    }
}