package it.neverworks.i18n;

import java.util.Locale;
import it.neverworks.lang.Guard;

public class TransientLocaleGuard implements Guard<Translator> {
    
    private Locale newLocale;
    private Locale oldLocale;
    
    public TransientLocaleGuard( Locale newLocale ) {
        this.newLocale = newLocale;
    }
    
    public Translator enter(){
        this.oldLocale = Translation.current();
        Translation.select( this.newLocale );
        return Translation.translator();
    }
    
    public Throwable exit( Throwable error ){
        Translation.select( this.oldLocale );
        return error;
    }
    
}