package it.neverworks.i18n;

import java.util.Locale;

public interface Translator {
    String translate( String text );
    void select( Locale locale );
    boolean supports( Locale locale );
    Locale current();
}