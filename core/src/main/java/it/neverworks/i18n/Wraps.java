package it.neverworks.i18n;

import java.util.Formatter;
import it.neverworks.text.TextFormat;

public class Wraps {

    public static String T( String message ) {
        return Translation.translator().translate( message );
    }

    public static String M( String message, Object... params ) {
        Translator t = Translation.translator();
        return TextFormat.format( t.current(), t.translate( message ), params );
    }

    public static String F( String message, Object... params ) {
        Translator t = Translation.translator();
        return new Formatter( t.current() ).format( t.translate( message ), params ).toString();
    }
    
}