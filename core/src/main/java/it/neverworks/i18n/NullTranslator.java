package it.neverworks.i18n;

import java.util.Locale;

public class NullTranslator implements Translator {
    
    private Locale locale = Locale.ENGLISH;
    
    public String translate( String text ) {
        return text;
    }
    
    public void select( Locale locale ) {
        this.locale = locale;
    }
    
    public Locale current() {
        return locale;
    }
    
    public boolean supports( Locale locale ) {
        return this.locale.equals( locale );
    }
}