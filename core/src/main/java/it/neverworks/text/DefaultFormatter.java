package it.neverworks.text;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Collections;

public class DefaultFormatter implements TextFormatter {
    
    public String format( Object value ) {
        if( value != null ) {
            if( Collections.isArray( value ) ) {
                return Strings.valueOf( Collections.list( value ) );
            } else {
                return Strings.valueOf( value );
            }
        } else {
            return "";
        }
    }

}