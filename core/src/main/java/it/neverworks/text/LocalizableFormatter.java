package it.neverworks.text;

import java.util.Locale;

public interface LocalizableFormatter {
    public String format( Locale locale, Object value );
}