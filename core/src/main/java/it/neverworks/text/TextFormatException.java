package it.neverworks.text;

public class TextFormatException extends RuntimeException {
    
    public TextFormatException( String message ) {
        super( message );
    }
    
    public TextFormatException( String message, Throwable cause ) {
        super( message, cause );
    }
    
}