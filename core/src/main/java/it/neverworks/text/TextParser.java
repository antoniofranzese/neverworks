package it.neverworks.text;

import it.neverworks.lang.Strings;

public abstract class TextParser {
    
    protected void parse( String str ) {
        char[] chars = Strings.raw( str ); 

        int expressionDepth = 0;
        boolean inExpression = false;
        boolean expression = false;
        boolean inQuoting = false;
        boolean quoting = false;
        boolean collect = false;
        boolean inText = false;
        int total = chars.length;
        int start = 0;
        int end = 0;

        for( int index = 0; index < total; index++ ) {

            if( chars[ index ] == '\'' ) {
                if( index > start ) {
                    collect = true;
                    if( inQuoting ) {
                        end = index;
                    } else {
                        end = index - 1;
                    }
                }
                if( inQuoting ) {
                    quoting = true;
                }
                inQuoting = ! inQuoting;

            } else if( chars[ index ] == '{' ) {
                if( ! inQuoting ) {
                    expressionDepth++;
                    if( ! inExpression ) {
                        inExpression = true;
                        if( index > start ) {
                            collect = true;
                            end = index - 1;
                        }
                    }
                }

            } else if( chars[ index ] == '}' ) {
                if( ! inQuoting ) {
                    expressionDepth--;
                    if( expressionDepth == 0 && inExpression ) {
                        inExpression = false;
                        expression = true;
                        if( index > start ) {
                            collect = true;
                            end = index;
                        }
                    }
                }
            }

            if( index == ( total - 1 ) ) {
                if( inExpression ) {
                    throw new ParseException( "Unterminated expression string" );
                } else {
                    if( inQuoting ) {
                        quoting = false;
                    }
                    collect = true;
                    end = index;
                }
            }

            if( collect ) {
                
                int length = end - start + 1;
                
                if( length > 0 ) {
                    
                    if( expression ) {
                        
                        String path = null;
                        String format = null;
                        String spec = null;

                        boolean split = false;
                        int field = 0;
                        int first = start;
                        for( int c = start; c < end; c++ ) {
                            if( ( chars[ c ] == ',' && field < 2 ) || c == end - 1 ) {
                                split = true;
                            }
                            if( split ) {
                                split = false;
                                String part = new String( chars, first + 1, c - first - ( c == end - 1 ? 0 : 1 ) );
                                switch( field ) {
                                    case 0:
                                        path = part;
                                        break;
                                    case 1:
                                        format = part;
                                        break;
                                    case 2:
                                        spec = part;
                                        break;
                                }
                                field++;
                                first = c;
                            }
                        }
                        
                        onExpression( path != null ? path.trim() : null, format != null ? format.trim() : null, spec != null ? spec.trim() : null );

                    } else {
                        String text;
                        if( quoting ) {
                            if( length == 2 ) {
                                text = "'";
                            } else {
                                text = new String( chars, start + 1, length - 2 );
                            }
                        } else {
                            text = new String( chars, start, length );
                        }

                        onText( text );

                    }

                    start = end + 1;
                }

                collect = false;
                expression = false;
                quoting = false;
                
            }
        }
    }
    
    public abstract void onText( String text );
    public abstract void onExpression( String path, String format, String spec );

}