package it.neverworks.text;

import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.io.InputStream;
import java.util.Locale;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.io.Streams;
import it.neverworks.io.FileInfo;
import it.neverworks.io.StringWriter;
import it.neverworks.i18n.Translation;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Modifiers;

public class TextFormat {

    /* Template use case */
    
    private final static Modifiers MODIFIERS = new Modifiers().withNulls().withOptional();
    
    private List<Chunk> chunks;

    public TextFormat( String template ) {
        super();
        parse( template );
    }

    public TextFormat( InputStream stream ) {
        this( Streams.asString( stream ) );
    }

    public TextFormat( Reader reader ) {
        this( Streams.asString( reader ) );
    }

    public TextFormat( File file ) {
        this( Streams.asStream( file ) );
    }

    public TextFormat( FileInfo info ) {
        this( info.getStream() );
    }

    protected void parse( String template ) {
        this.chunks = new TemplateParser().process( template );
    }
    
    public void apply( Writer writer, Object... arguments ) {
        apply( writer, Translation.current(), arguments );
    }

    public void apply( Writer writer, Locale locale, Object... arguments ) {
        try {
            for( Chunk chunk: this.chunks ) {
                chunk.apply( writer, locale, arguments );
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public String apply( Object... arguments ) {
        return apply( Translation.current(), arguments );
    }

    public String apply( Locale locale, Object... arguments ) {
        StringWriter writer = new StringWriter();
        apply( writer, locale, arguments );
        return writer.toString();
    }
    
    private static interface Chunk {
        public void apply( Writer writer, Locale locale, Object... arguments ) throws Exception;
    }

    private static class TextChunk implements Chunk {
        private String text;
        
        public void apply( Writer writer, Locale locale, Object... arguments ) throws Exception {
            writer.write( text );
        }
    }
    
    private static class FieldChunk implements Chunk {
        private int index;
        private TextFormatter formatter;
        private String expr = null;
        
        public void apply( Writer writer, Locale locale, Object... arguments ) throws Exception {
            try {
                Object value = arguments[ index ];
                if( value != null && this.expr != null ) {
                    value = ExpressionEvaluator.evaluate( value, this.expr );
                }
                
                if( formatter instanceof LocalizableFormatter ) {
                    writer.write( ((LocalizableFormatter) formatter).format( locale, value ) );
                } else {
                    writer.write( formatter.format( value ) );
                }
            } catch( ArrayIndexOutOfBoundsException ex ) {
                throw new TextFormatException( "Missing format argument: " + index, ex );
            }
        }
    }
    
    private static class TemplateParser extends TextParser {
        
        List<Chunk> chunks;
        private int autoIndex = 0;
        
        public List process( String expression ) {
            this.chunks = new ArrayList<Chunk>();
            parse( expression );
            return this.chunks;
        }

        public void onText( String text ) {
            TextChunk chunk = new TextChunk();
            chunk.text = text;
            this.chunks.add( chunk );
        }
    
        public void onExpression( String path, String format, String spec ) {
            try {
                FieldChunk chunk = new FieldChunk();
                if( Strings.hasText( path ) ) {
                    try {
                        chunk.index = Integer.parseInt( path );
                    } catch( NumberFormatException nf ) {
                        int dot = path.indexOf( "." );
                        if( dot > 0 ) {
                            try {
                                chunk.index = Integer.parseInt( path.substring( 0, dot ) );
                                chunk.expr = path.substring( dot + 1 );
                            } catch( NumberFormatException nf2 ) {
                                chunk.index = 0;
                                chunk.expr = path;
                            }
                        } else {
                            chunk.index = 0;
                            chunk.expr = path;
                        }
                    }
                } else {
                    chunk.index = this.autoIndex;
                    this.autoIndex += 1;
                }
                
                chunk.formatter = TextFormatterRegistry.getConfiguredFormatter( format, spec );
                this.chunks.add( chunk );

            } catch( NumberFormatException nfe ) {
                throw new IllegalArgumentException( "Invalid argument index: " + path, nfe );
            }
        }
        
    }
    
    /* Streaming use case */

    public static void format( Writer writer, String template, Object... arguments ) {
        format( writer, Translation.current(), template, arguments );
    }

    public static void format( Writer writer, Locale locale, String template, Object... arguments ) {
        new StreamingParser( arguments, writer, locale ).parse( template );
    }

    public static String format( String template, Object... arguments ) {
        return format( Translation.current(), template, arguments );
    }

    public static String format( Locale locale, String template, Object... arguments ) {
        StringWriter writer = new StringWriter();
        new StreamingParser( arguments, writer, locale ).parse( template );
        return writer.toString();
    }
    
    private static class StreamingParser extends TextParser {
        private Object[] arguments;
        private Writer writer;
        private Locale locale;
        private int autoIndex = 0;
        
        private StreamingParser( Object[] arguments, Writer writer, Locale locale ) {
            this.arguments = arguments;
            this.writer = writer;
            this.locale = locale;
        }
        
        public void onText( String text ) {
            try {
                this.writer.write( text );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        }
    
        public void onExpression( String path, String format, String spec ) {
            try {
                
                int index;
                String expr = null;

                if( Strings.hasText( path ) ) {
                    try {
                        index = Integer.parseInt( path );
                    } catch( NumberFormatException nf ) {
                        int dot = path.indexOf( "." );
                        if( dot > 0 ) {
                            try {
                                index = Integer.parseInt( path.substring( 0, dot ) );
                                expr = path.substring( dot + 1 );
                            } catch( NumberFormatException nfe2 ) {
                                index = 0;
                                expr = path;
                            }
                        } else {
                            index = 0;
                            expr = path;
                        }
                    }
                } else {
                    index = this.autoIndex;
                    this.autoIndex += 1;
                }
                
                TextFormatter formatter = Strings.hasText( format ) ? TextFormatterRegistry.getFormatter( format ) : TextFormatterRegistry.getDefaultFormatter();

                if( Strings.hasText( spec ) && formatter instanceof ConfigurableFormatter ) {
                    ((ConfigurableFormatter) formatter).setFormatSpecification( spec );
                }
                
                Object value = this.arguments != null ? this.arguments[ index ] : null;
                if( value != null && expr != null ) {
                    value = ExpressionEvaluator.evaluate( value, expr, MODIFIERS );
                }
                
                if( this.locale != null && formatter instanceof LocalizableFormatter ) {
                    this.writer.write( ((LocalizableFormatter) formatter).format( this.locale, value ) );
                } else {
                    this.writer.write( formatter.format( value ) );
                }

            } catch( ArrayIndexOutOfBoundsException oob ) {
                throw new TextFormatException( "Missing format argument: " + path, oob );
            } catch( NumberFormatException nfe ) {
                throw new IllegalArgumentException( "Invalid argument index: " + path, nfe );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        }
        
    }
    
}