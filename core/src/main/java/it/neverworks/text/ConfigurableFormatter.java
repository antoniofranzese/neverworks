package it.neverworks.text;

public interface ConfigurableFormatter {
    
    public void setFormatSpecification( String specification );
}