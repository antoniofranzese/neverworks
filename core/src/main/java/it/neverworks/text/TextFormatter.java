package it.neverworks.text;

public interface TextFormatter {
    public String format( Object value );
}