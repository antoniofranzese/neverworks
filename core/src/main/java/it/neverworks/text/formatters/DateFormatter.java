package it.neverworks.text.formatters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import it.neverworks.lang.Dates;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Arguments;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.text.LocalizableFormatter;

public class DateFormatter implements TextFormatter, ConfigurableFormatter, LocalizableFormatter {
    
    protected String dateFormat;
    protected String emptyFormat;
    protected String errorFormat = null;
    
    public String format( Object value ) {
        return format( Translation.current(), value );
    }

    public String format( Locale locale, Object value ) {
        try {
            Date date = Dates.date( value );
            if( date != null ) {
                return format( locale ).map( date );
            } else {
                return emptyFormat;
            }
        } catch( Exception ex ) {
            if( errorFormat != null ) {
                return errorFormat;
            } else {
                throw Errors.wrap( ex );
            }
        }
    }

    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );
            this.dateFormat = args.get( 0, null );
            this.emptyFormat = args.get( 1, "" );

            for( String name: args.names() ) {
                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );
                
                } else if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                
                } else if( "default".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                    this.emptyFormat = this.errorFormat;

                } else if( "format".equalsIgnoreCase( name ) ) {
                    if( this.dateFormat == null ) {
                        this.dateFormat = args.get( name );
                    } else {
                        throw new IllegalArgumentException( "Multiple date format: " + specification );
                    }

                }
            }
        }
    }
    
    protected Format format( Locale locale ) {
        if( Strings.hasText( this.dateFormat ) ) {
            if( "short".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getDateInstance( DateFormat.SHORT, locale ) );

            } else if( "medium".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getDateInstance( DateFormat.DEFAULT, locale ) );

            } else if( "long".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getDateInstance( DateFormat.LONG, locale ) );

            } else if( "full".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getDateInstance( DateFormat.FULL, locale ) );

            } else if( "iso".equalsIgnoreCase( this.dateFormat ) ) {
                return new JodaFormat( ISODateTimeFormat.basicDate() );

            } else if( "iso-utc".equalsIgnoreCase( this.dateFormat ) ) {
                return new JodaFormat( ISODateTimeFormat.basicDate().withZoneUTC() );

            } else {
                return new JavaFormat( new SimpleDateFormat( this.dateFormat, locale ) ); 

            }

        } else {
            return new JavaFormat( DateFormat.getDateInstance( DateFormat.DEFAULT, locale ) );
        }
    }
    
    protected interface Format extends Mapper<Date, String>{}
        
    protected class JavaFormat implements Format {
        private DateFormat format;
        
        public JavaFormat( DateFormat format ) {
            this.format = format;
        }
        
        public String map( Date value ) {
            return this.format.format( value );
        }
    }
    
    protected class JodaFormat implements Format {
        private DateTimeFormatter format;
        
        public JodaFormat( DateTimeFormatter format ) {
            this.format = format;
        }
        
        public String map( Date value ) {
            return this.format.print( value.getTime() );
        }
    }
    
}