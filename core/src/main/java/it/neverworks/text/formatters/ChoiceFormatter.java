package it.neverworks.text.formatters;

import java.text.ChoiceFormat;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;
import it.neverworks.text.TextFormat;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class ChoiceFormatter implements TextFormatter, ConfigurableFormatter {

    protected String specification;
    protected boolean formatted;

    public String format( Object value ) {
        if( Strings.hasText( this.specification ) ) {
            String actualSpecification;
            if( this.formatted ) {
                actualSpecification = TextFormat.format( this.specification, value );
            } else {
                actualSpecification = this.specification;
            }
            return new ChoiceFormat( actualSpecification ).format( Numbers.number( value ) );
        } else {
            throw new IllegalArgumentException( "Empty choice format" );
        }
    }

    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            this.formatted = specification.indexOf( "{" ) >= 0 && specification.indexOf( "}" ) > 0;
            this.specification = this.formatted ? specification.replaceAll( "\\{\\?", "{0" ) : specification;
        } else {
            this.specification = null;
        }
    }
    
}