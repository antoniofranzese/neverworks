package it.neverworks.text.formatters;

import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class SelectFormatter implements TextFormatter, ConfigurableFormatter {

    private String defaultFormat = "";
    private Map<String,String> formats = new HashMap<String, String>();
    
    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );
            for( String name: args.names() ) {

                if( "*".equals( name ) || "default".equalsIgnoreCase( name ) ) {
                    this.defaultFormat = args.get( name );

                } else {
                    this.formats.put( name, args.get( name ) );
                } 
            }
        }
        
    }
    
    public String format( Object value ) {
        String str = Strings.valueOf( value );
        if( Strings.hasText( str ) && formats.containsKey( str ) ) {
            return formats.get( str );
        } else {
            return this.defaultFormat;
        }
    }
    
}