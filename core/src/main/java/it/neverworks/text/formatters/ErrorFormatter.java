package it.neverworks.text.formatters;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Booleans;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class ErrorFormatter implements TextFormatter, ConfigurableFormatter {

    private String emptyFormat = null;
    private boolean unwrap = true;
    private boolean full = false;
    
    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );
            
            for( String name: args.names() ) {

                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );
                    
                } else if( "unwrap".equalsIgnoreCase( name ) ) {
                    this.unwrap = Booleans.isTrue( args.get( name ) );

                } else if( "full".equalsIgnoreCase( name ) ) {
                    this.full = Booleans.isTrue( args.get( name ) );
                
                }
            }
        }
    }
    
    public String format( Object value ) {
        if( value instanceof Throwable ) {
            Throwable ex = (Throwable) value;
            
            if( this.unwrap ) {
                ex = Errors.unwrapThrowable( ex );
            }
            
            if( Strings.hasText( ex.getMessage() ) ) {
                return ex.getMessage();
            
            } else if( Strings.hasText( this.emptyFormat ) ) {
                return this.emptyFormat;
            
            } else {
                if( this.full ) {
                    return ex.getClass().getName();
                } else {
                    return ex.getClass().getSimpleName();
                }
            }

        } else {
            return Strings.valueOf( value );
        }
    }
    
}