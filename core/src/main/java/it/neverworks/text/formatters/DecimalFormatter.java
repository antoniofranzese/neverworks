package it.neverworks.text.formatters;

import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import static it.neverworks.language.*;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.text.LocalizableFormatter;

public class DecimalFormatter implements TextFormatter, ConfigurableFormatter, LocalizableFormatter {
    
    protected String pattern = null;
    protected boolean group = false;
    protected boolean currency = false;
    protected int decimals = 2;
    protected String emptyFormat = "";
    protected String errorFormat = null;
    
    public String format( Object value ) {
        return format( Translation.current(), value );
    }

    public String format( Locale locale, Object value ) {
        if( value != null ) {
            try {
                Number number = convertNumber( value );
                if( number != null ) {
                    return getNumberFormat( locale ).format( number );
                } else {
                    return this.emptyFormat;
                }
            } catch( Exception ex ) {
                if( this.errorFormat != null ) {
                    return this.errorFormat;
                } else {
                    throw Errors.wrap( ex );
                }
            }
        } else {
            return this.emptyFormat;
        }
    }
    
    protected Number convertNumber( Object value ) {
        return Numbers.number( value );
    }
    
    public void setFormatSpecification( String specification ) {

        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );

            this.decimals = args.get( type( Integer.class ), 0, 2 );

            for( String name: args.names() ) {
                
                if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                    
                } else if( "empty".equalsIgnoreCase( name ) || "null".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );

                } else if( "default".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                    this.emptyFormat = this.errorFormat;

                } else if( "decimals".equalsIgnoreCase( name ) ) {
                    this.decimals = args.get( type( Integer.class ), name );

                } else if( name.startsWith( "group" ) ) {
                    this.group = args.get( type( Boolean.class ), name );

                } else if( "currency".equalsIgnoreCase( name ) ) {
                    this.currency = args.get( type( Boolean.class ), name );

                } else {
                    throw new IllegalArgumentException( "Unknown '" + name + "' argument: " + specification );
                }
            }

        }
    }
    
    protected NumberFormat getNumberFormat( Locale locale ) {
        return new DecimalFormat( getNumberPattern(), DecimalFormatSymbols.getInstance( locale ) );
    }
    
    protected String getNumberPattern() {
        if( this.pattern == null ) {
            this.pattern = 
                ( this.currency ? "\u00A4 " : "" )
                + ( this.group ? "#,##0" : "0" )
                + ( decimals > 0 ? ( "." + Strings.repeat( 0, this.decimals ) ) : "" );
        }
        return this.pattern;
    }
    
}