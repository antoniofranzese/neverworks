package it.neverworks.text.formatters;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.encoding.URL;

public class URLFormatter implements TextFormatter, ConfigurableFormatter {

    private String emptyFormat = null;
    
    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );
            
            for( String name: args.names() ) {

                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );
                }
            }
        }
    }
    
    public String format( Object value ) {
        if( value == null && this.emptyFormat != null ) {
            return this.emptyFormat;
        } else {
            return URL.encode( value );
        }
    }
    
}