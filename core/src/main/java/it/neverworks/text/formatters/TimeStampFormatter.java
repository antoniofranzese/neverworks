package it.neverworks.text.formatters;

import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.joda.time.format.ISODateTimeFormat;

import it.neverworks.lang.Strings;

public class TimeStampFormatter extends DateFormatter {
    
    protected String separator = " ";
    
    @Override
    protected Format format( Locale locale ) {
        if( Strings.hasText( this.dateFormat ) ) {
            if( "short".equalsIgnoreCase( this.dateFormat ) ) {
                return new TimeStampFormat( DateFormat.SHORT, DateFormat.SHORT, locale, this.separator );

            } else if( "medium".equalsIgnoreCase( this.dateFormat ) ) {
                return new TimeStampFormat( DateFormat.MEDIUM, DateFormat.MEDIUM, locale, this.separator );

            } else if( "long".equalsIgnoreCase( this.dateFormat ) ) {
                return new TimeStampFormat( DateFormat.LONG, DateFormat.LONG, locale, this.separator );

            } else if( "full".equalsIgnoreCase( this.dateFormat ) ) {
                return new TimeStampFormat( DateFormat.FULL, DateFormat.FULL, locale, this.separator );

            } else if( "iso".equalsIgnoreCase( this.dateFormat ) ) {
                return new JodaFormat( ISODateTimeFormat.dateTimeNoMillis() );

            } else if( "iso-utc".equalsIgnoreCase( this.dateFormat ) ) {
                return new JodaFormat( ISODateTimeFormat.dateTimeNoMillis().withZoneUTC() );

            } else {
                return new JavaFormat( new SimpleDateFormat( this.dateFormat, locale ) ); 
            }
        } else {
            return new TimeStampFormat( DateFormat.SHORT, DateFormat.MEDIUM, locale, this.separator );
        }
    }
    
    protected class TimeStampFormat implements Format {
        private DateFormat timeFormat;
        private DateFormat dateFormat;
        
        public TimeStampFormat( int dateFormat, int timeFormat, Locale locale, String separator ) {
            this.dateFormat = DateFormat.getDateInstance( dateFormat, locale );
            this.timeFormat = DateFormat.getTimeInstance( timeFormat, locale );
        }
        
        public String map( Date value ) {
            return dateFormat.format( value ) + separator + timeFormat.format( value );
        }
    }
    
}