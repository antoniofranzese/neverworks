package it.neverworks.text.formatters;

import it.neverworks.lang.Numbers;

public class LongFormatter extends IntegerFormatter {
    
    protected Number convertNumber( Object value ) {
        return Numbers.convert( value, Long.class );
    }
    
    
}