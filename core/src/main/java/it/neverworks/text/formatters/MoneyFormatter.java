package it.neverworks.text.formatters;

public class MoneyFormatter extends DecimalFormatter {
    
    public MoneyFormatter() {
        super();
        this.decimals = 2;
        this.group = true;
    }
    
}