package it.neverworks.text.formatters;

import java.util.List;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Arguments;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class StringFormatter implements TextFormatter, ConfigurableFormatter {

    private boolean trim = false;
    private boolean upper = false;
    private boolean lower = false;
    private boolean join = false;
    private String keep = null;
    private String waste = null;
    private Integer limit = null;
    private String truncation = null;
    private String emptyFormat = "";
    private String errorFormat = null;
    private String search = null;
    private String replace = null;
    
    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );

            for( Object positional: args.positionals() ) {

                if( "trim".equalsIgnoreCase( (String) positional ) ) {
                    this.trim = true;

                } else if( "upper".equalsIgnoreCase( (String) positional ) || "uppercase".equalsIgnoreCase( (String) positional ) ) {
                    this.upper = true;

                } else if( "lower".equalsIgnoreCase( (String) positional ) || "lowercase".equalsIgnoreCase( (String) positional ) ) {
                    this.lower = true;

                } else if( "join".equalsIgnoreCase( (String) positional ) ) {
                    this.join = true;
                } 

            }

            for( String name: args.names() ) {

                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );

                } else if( "keep".equalsIgnoreCase( name ) ) {
                    this.keep = args.get( name );

                } else if( "waste".equalsIgnoreCase( name ) ) {
                    this.waste = args.get( name );

                } else if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );

                } else if( "default".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                    this.emptyFormat = this.errorFormat;

                } else if( "limit".equalsIgnoreCase( name ) && Strings.hasText( args.get( name ) ) ) {
                    String[] parts = args.<String>get( name ).split( "," );
                    
                    this.limit = Integer.parseInt( parts[ 0 ] );
                    if( parts.length > 1 ) {
                        this.truncation = parts[ 1 ];
                    }

                } else if( "replace".equalsIgnoreCase( name ) && Strings.hasText( args.get( name ) ) ) {
                    String[] parts = args.<String>get( name ).split( "," );
                    
                    this.search = parts[ 0 ];
                    if( parts.length > 1 ) {
                        this.replace = parts[ 1 ];
                    }

                } else if( "trim".equalsIgnoreCase( name ) ) {
                    this.trim = Booleans.isTrue( args.get( name ) );

                } else if( "upper".equalsIgnoreCase( name ) || "uppercase".equalsIgnoreCase( name ) ) {
                    this.upper = Booleans.isTrue( args.get( name ) );

                } else if( "lower".equalsIgnoreCase( name ) || "lowercase".equalsIgnoreCase( name ) ) {
                    this.lower = Booleans.isTrue( args.get( name ) );

                } else if( "join".equalsIgnoreCase( name ) ) {
                    this.join = Booleans.isTrue( args.get( name ) );
                }
            }
        }
        
    }
    
    protected String convertString( Object value ) {
        return Strings.valueOf( value );
    }
    
    public String format( Object value ) {
        try {
            String str = Strings.valueOf( value );
            if( Strings.hasText( str ) ) {
                if( this.upper ) {
                    str = str.toUpperCase();
                }
                
                if( this.lower ) {
                    str = str.toLowerCase();
                }

                if( Strings.hasText( this.search ) ) {
                    str = str.replaceAll( this.search, Strings.safe( this.replace ) );
                }
                
                if( this.keep != null ) {
                    str = Strings.keep( str, this.keep );
                }
                
                if( this.waste != null ) {
                    str = Strings.waste( str, this.waste );
                }
                
                if( this.join ) {
                    str = str.replaceAll( "[\n\r]+", " " );
                }

                if( this.trim ) {
                    str = str.trim();
                }
                
                if( this.limit != null ) {
                    str = Strings.limit( str, this.limit, this.truncation );
                }
                
                return str;
            } else {
                return this.emptyFormat;
            }
        } catch( Exception ex ) {
            if( this.errorFormat != null ) {
                return this.errorFormat;
            } else {
                throw Errors.wrap( ex );
            }
        }
    }
    
}