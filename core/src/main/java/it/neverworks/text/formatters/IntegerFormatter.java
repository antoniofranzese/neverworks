package it.neverworks.text.formatters;

import it.neverworks.lang.Numbers;

public class IntegerFormatter extends NumberFormatter {
    
    public IntegerFormatter() {
        super();
        this.nonzeroes = "#";
        this.zeroes = "0";
    }
    
    protected Number convertNumber( Object value ) {
        return Numbers.convert( value, Integer.class );
    }
    
    
}