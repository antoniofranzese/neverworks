package it.neverworks.text.formatters;

import java.util.Map;
import java.util.HashMap;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

public class FormatterUtils {
    
    public static Arguments parseArguments( String specification ) {
        return Arguments.parse( specification, '=', '|' );
    }
    
}