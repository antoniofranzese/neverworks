package it.neverworks.text.formatters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import org.joda.time.format.ISODateTimeFormat;

import it.neverworks.lang.Strings;

public class TimeFormatter extends DateFormatter {
    
    @Override
    protected Format format( Locale locale ) {
        if( Strings.hasText( this.dateFormat ) ) {
            if( "short".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getTimeInstance( DateFormat.SHORT, locale ) );

            } else if( "medium".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getTimeInstance( DateFormat.DEFAULT, locale ) );

            } else if( "long".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getTimeInstance( DateFormat.LONG, locale ) );

            } else if( "full".equalsIgnoreCase( this.dateFormat ) ) {
                return new JavaFormat( DateFormat.getTimeInstance( DateFormat.FULL, locale ) );

            } else if( "iso".equalsIgnoreCase( this.dateFormat ) ) {
                return new JodaFormat( ISODateTimeFormat.basicTimeNoMillis() );

            } else if( "iso-utc".equalsIgnoreCase( this.dateFormat ) ) {
                return new JodaFormat( ISODateTimeFormat.basicTimeNoMillis().withZoneUTC() );

            } else {
                return new JavaFormat( new SimpleDateFormat( this.dateFormat, locale ) ); 
            }

        } else {
            return new JavaFormat( DateFormat.getTimeInstance( DateFormat.DEFAULT, locale ) );
        }
    }
    
}