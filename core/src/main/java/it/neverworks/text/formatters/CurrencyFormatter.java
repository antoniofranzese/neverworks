package it.neverworks.text.formatters;

public class CurrencyFormatter extends MoneyFormatter {
    
    public CurrencyFormatter() {
        super();
        this.currency = true;
    }
    
}