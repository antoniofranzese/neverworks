package it.neverworks.text.formatters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Arguments;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.encoding.HTML;

public class HTMLFormatter implements TextFormatter, ConfigurableFormatter {

    protected boolean sanitize = true;
    protected boolean escape = false;
    protected String emptyFormat = "";
    protected String errorFormat = "";
    
    public String format( Object value ) {
        try {
            if( value != null ) {
                String html = this.escape ? HTML.escape( value ) : HTML.encode( value );
                return this.sanitize && this.escape ? HTML.sanitize( html ) : html;
            } else {
                return emptyFormat;
            }
        } catch( Exception ex ) {
            if( errorFormat != null ) {
                return errorFormat;
            } else {
                throw Errors.wrap( ex );
            }
        }
    }

    public void setFormatSpecification( String specification ) {
        String codec = null;
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );
            if( args.hasPositionals() ) {
                this.escape = args.positionals().contains( "escape" );
            }

            for( String name: args.names() ) {
                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );
                
                } else if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
            
                } else if( "safe".equalsIgnoreCase( name ) || "sanitize".equalsIgnoreCase( name ) ) {
                    this.sanitize = Booleans.isTrue( args.get( name ) );

                } else if( "escape".equalsIgnoreCase( name ) ) {
                    this.escape = Booleans.isTrue( args.get( name ) );

                }
            }
            
        }
    }
    
}