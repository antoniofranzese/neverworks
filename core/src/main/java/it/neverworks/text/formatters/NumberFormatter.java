package it.neverworks.text.formatters;

import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.text.LocalizableFormatter;

public class NumberFormatter implements TextFormatter, ConfigurableFormatter, LocalizableFormatter {
    
    protected String nonzeroes = null;
    protected String zeroes = null;
    protected String emptyFormat = "";
    protected String errorFormat = null;
    
    public String format( Object value ) {
        return format( Translation.current(), value );
    }

    public String format( Locale locale, Object value ) {
        if( value != null ) {
            try {
                Number number = convertNumber( value );
                if( number != null && Numbers.compare( number, 0 ) != 0 ) {
                    return formatNumber( locale, this.nonzeroes, number );
                } else {
                    if( this.zeroes != null ) {
                        return this.zeroes;
                    } else {
                        return formatNumber( locale, this.nonzeroes, 0 );
                    }
                }
            } catch( Exception ex ) {
                if( this.errorFormat != null ) {
                    return this.errorFormat;
                } else {
                    throw Errors.wrap( ex );
                }
            }
        } else {
            return this.emptyFormat;
        }
    }
    
    protected String formatNumber( Locale locale, String format, Number number ) {
        return getNumberFormat( locale, format ).format( number );
    }
    
    protected Number convertNumber( Object value ) {
        return Numbers.number( value );
    }
    
    public void setFormatSpecification( String specification ) {

        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );

            String positives = args.get( String.class, 0, null );
            String negatives = args.get( String.class, 1, null );
            String zeroes = args.get( String.class, 2, null );
            Integer decimals = null;

            for( String name: args.names() ) {
                
                if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                    
                } else if( "empty".equalsIgnoreCase( name ) || "null".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );

                } else if( "positive".equalsIgnoreCase( name ) || "pos".equalsIgnoreCase( name ) || "+".equalsIgnoreCase( name ) ) {
                    if( positives == null ) {
                        positives = args.<String>get( name ).trim();
                    } else {
                        throw new IllegalArgumentException( "Multiple positive number format: " + specification );
                    }

                } else if( "negative".equalsIgnoreCase( name ) || "neg".equalsIgnoreCase( name ) || "-".equalsIgnoreCase( name ) ) {
                    if( negatives == null ) {
                        negatives = args.<String>get( name ).trim();
                    } else {
                        throw new IllegalArgumentException( "Multiple negative number format: " + specification );
                    }
                
                } else if( "zero".equalsIgnoreCase( name ) || "0".equalsIgnoreCase( name ) ) {
                    if( zeroes == null ) {
                        zeroes = args.<String>get( name ).trim();
                    } else {
                        throw new IllegalArgumentException( "Multiple zero format: " + specification );
                    }
            
                } else if( "default".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                    this.emptyFormat = this.errorFormat;

                } else if( "decimals".equalsIgnoreCase( name ) || "dec".equalsIgnoreCase( name ) ) {
                    decimals = Numbers.NInt( args.<String>get( name ) );

                } else {
                    throw new IllegalArgumentException( "Unknown '" + name + "' argument: " + specification );
                }
            }

            if( decimals != null && decimals > 0 && ! Strings.hasText( positives ) ) {
                positives = "#." + Strings.repeat( "0", decimals );
            }

            if( Strings.hasText( positives ) ) {
                if( Strings.hasText( negatives ) ) {
                    this.nonzeroes = positives + ";" + negatives;
                } else {
                    this.nonzeroes = positives;
                }
            }
            
            if( Strings.hasText( zeroes ) ) {
                this.zeroes = zeroes;
            }
        }
    }
    
    protected NumberFormat getNumberFormat( Locale locale, String specification ) {
        
        if( Strings.hasText( specification ) ) {
            if( "integer".equalsIgnoreCase( specification ) ) {
                return	NumberFormat.getIntegerInstance( locale );
            } else if( "currency".equalsIgnoreCase( specification ) ) {
                return NumberFormat.getCurrencyInstance( locale );
            } else if( "percent".equalsIgnoreCase( specification ) ) {
                return NumberFormat.getPercentInstance( locale );
            } else {
                return new DecimalFormat( specification, DecimalFormatSymbols.getInstance( locale ) );
            }

        } else {
            return NumberFormat.getInstance( locale );
        }
    }
    
}