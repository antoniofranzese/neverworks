package it.neverworks.text.formatters;

import it.neverworks.lang.Strings;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class OptionalFormatter implements TextFormatter, ConfigurableFormatter {

    protected String option;

    public String format( Object value ) {
        
        if( value == null || ( value instanceof String && Strings.isEmpty( value ) ) ) {
            return option;
        } else {
            return Strings.valueOf( value );
        }
    }

    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            this.option = specification;
        } else {
            this.option = "";
        }
    }
    
}