package it.neverworks.text.formatters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Arguments;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.text.LocalizableFormatter;
import it.neverworks.encoding.json.JSONCodec;
import it.neverworks.encoding.json.JavaScriptCodec;
import it.neverworks.encoding.json.JavaScriptObjectCodec;
import it.neverworks.encoding.json.RelaxedJSONCodec;
import it.neverworks.encoding.json.UnquotedJSONCodec;
import it.neverworks.encoding.JSON;

public class JSONFormatter implements TextFormatter, ConfigurableFormatter, LocalizableFormatter {

    protected JSONCodec codec;
    protected String emptyFormat = "";
    protected String errorFormat = null;
    
    public String format( Object value ) {
        return format( Translation.current(), value );
    }

    public String format( Locale locale, Object value ) {
        try {
            if( value != null ) {
                return this.codec != null ? this.codec.print( value ) : JSON.standard.print( value );
            } else {
                return emptyFormat;
            }
        } catch( Exception ex ) {
            if( errorFormat != null ) {
                return errorFormat;
            } else {
                throw Errors.wrap( ex );
            }
        }
    }

    public void setFormatSpecification( String specification ) {
        String codec = null;
        if( Strings.hasText( specification ) ) {
            Arguments args = FormatterUtils.parseArguments( specification );
            codec = args.get( 0, null );
            this.emptyFormat = args.get( 1, "" );

            for( String name: args.names() ) {
                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );
                
                } else if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                
                } else if( "codec".equalsIgnoreCase( name ) ) {
                    codec = args.get( name );
                }
            }
            
            if( codec == null ) {
                codec = "standard";
            } else {
                codec = codec.trim().toLowerCase();
            }
            
            if( Strings.hasText( codec ) ) {
                switch( codec.trim().toLowerCase() ) {
                    case "standard":
                        this.codec = new JSONCodec();
                        break;
                    case "lang":
                    case "javascript":
                    case "js":
                        this.codec = new JavaScriptCodec();
                        break;
                    case "obj":
                    case "object":
                        this.codec = new JavaScriptObjectCodec();
                        break;
                    case "relaxed":
                        this.codec = new RelaxedJSONCodec();
                        break;
                    case "unquoted":
                        this.codec = new UnquotedJSONCodec();
                        break;
                    default:
                        throw new IllegalArgumentException( "Unknown JSON formatter codec: " + codec );
                }
                
            } else {
                this.codec = new JSONCodec();
            }

        }

    }
    
}