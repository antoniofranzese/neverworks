package it.neverworks.text.formatters;

import static it.neverworks.language.*;

import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Tuple;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.FluentList;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.text.LocalizableFormatter;

public class BytesFormatter extends NumberFormatter {
    
    private final static Pattern BYTE_SPEC = Pattern.compile( "(>?)([^B]?)B" );
    private final static long K = 1024L;

    private final static FluentList<Tuple> DIVIDERS = list(
        tuple( "P", "PB", Long( Math.pow( K, 5 ) ) )
        ,tuple( "T", "TB", Long( Math.pow( K, 4 ) ) )
        ,tuple( "G", "GB", Long( Math.pow( K, 3 ) ) )
        ,tuple( "M", "MB", Long( Math.pow( K, 2 ) ) )
        ,tuple( "K", "KB", K )
        ,tuple( "B", "B", 1L )
    );

    private final static String[] UNITS = DIVIDERS.map( d -> d.get( 1 ) ).array( String.class );
    private final static Long[] AMOUNTS = DIVIDERS.map( d -> d.get( 2 ) ).array( Long.class );

    @Override
    protected String formatNumber( Locale locale, String format, Number number ) {
        Matcher m = BYTE_SPEC.matcher( format );

        if( m.find() ) {
            String fmt = m.group( 0 );
            boolean limited = ! empty( m.group( 1 ) );
            String spec = ( ! empty( m.group( 2 ) ) ? m.group( 2 ) : "*" ).toUpperCase();
            
            Tuple result = null;
            
            // System.out.println( msg( "Spec: |{}|, limited {}, fmt |{}| ", spec, limited, fmt ) );
            
            if( "*".equals( spec ) ) {
                result = divideBytes( number );
            
            } else {
                Tuple divider = DIVIDERS.filter( d -> d.get( 0 ).equals( spec ) ).list().head();
                if( divider != null ) {
                    if( limited ) {
                        result = divideBytes( number, divider.get( 2 ) );
                    } else {
                        result = tuple( Double( number ) / Double( divider.get( 2 ) ), divider.get( 1 ) );
                    }
                    
                } else {
                    throw new IllegalArgumentException( "Invalid byte format: " + fmt );
                }
            }
            
            return super.formatNumber( locale, format.replace( fmt.trim(), result.get( 1 ) ), result.get( 0 ) );
            
        } else {
            return super.formatNumber( locale, format, number );
        }
    }

    public Tuple divideBytes( Number value ) {
        return divideBytes( value, DIVIDERS.get( 0 ).get( 2 ) );
    }
    
    public Tuple divideBytes( Number value, long max ) {
            
        int i = 0;
        while( AMOUNTS[ i ] > Math.max( 1, max ) ) {
            i++;
        }
        for( ; i < AMOUNTS.length; i++ ){
            final long amount = AMOUNTS[ i ];
            if( Numbers.compare( value, amount ) >= 0 ){
                return tuple( Double( value ) / Double( amount ), UNITS[i] );
            }
        }
        return tuple( value, "B" );
    }
    
}