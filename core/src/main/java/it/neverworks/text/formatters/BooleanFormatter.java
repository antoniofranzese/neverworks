package it.neverworks.text.formatters;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Booleans;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class BooleanFormatter implements TextFormatter, ConfigurableFormatter {

    private String trueFormat = null;
    private String falseFormat = null;
    private String emptyFormat = "";
    private String errorFormat = null;
    
    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            
            Arguments args = FormatterUtils.parseArguments( specification );
            this.trueFormat = args.get( 0, null );
            this.falseFormat = args.get( 1, null );
            
            for( String name: args.names() ) {
                if( "empty".equalsIgnoreCase( name ) ) {
                    this.emptyFormat = args.get( name );
                } else if( "error".equalsIgnoreCase( name ) ) {
                    this.errorFormat = args.get( name );
                } else if( "true" .equalsIgnoreCase( name) ) {
                    if( trueFormat == null ) {
                        this.trueFormat = args.get( name );
                    } else {
                        throw new IllegalArgumentException( "Multiple true format: " + specification );
                    }
                } else if( "false".equalsIgnoreCase( name ) ) {
                    if( falseFormat == null ) {
                        this.falseFormat = args.get( name );
                    } else {
                        throw new IllegalArgumentException( "Multiple false format: " + specification );
                    }
                } 

            }
        }
        
        if( this.trueFormat == null ) {
            this.trueFormat = "";
        }

        if( this.falseFormat == null ) {
            this.falseFormat = "";
        }
        
    }
    
    public String format( Object value ) {
        try {
            if( value != null ) {
                if( Booleans.isTrue( value ) ) {
                    return this.trueFormat;
                } else {
                    return this.falseFormat;
                }
            } else {
                return this.emptyFormat;
            }
        } catch( Exception ex ) {
            if( this.errorFormat != null ) {
                return this.errorFormat;
            } else {
                throw Errors.wrap( ex );
            }
        }
    }
    
}