package it.neverworks.text.formatters;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Numbers;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.ConfigurableFormatter;

public class RepeatFormatter implements TextFormatter, ConfigurableFormatter {

    private String repeatString = null;
    private String falseFormat = null;
    private String emptyFormat = "";
    private String errorFormat = null;
    
    public void setFormatSpecification( String specification ) {
        if( Strings.hasText( specification ) ) {
            this.repeatString = specification;
        }
    }
    
    public String format( Object value ) {
        try {
            if( Strings.hasText( repeatString ) ) {
                return Strings.repeat( repeatString, Numbers.number( value ).intValue() );
            } else {
                return "";
            }
        } catch( Exception ex ) {
            return "";
        }
    }
    
}