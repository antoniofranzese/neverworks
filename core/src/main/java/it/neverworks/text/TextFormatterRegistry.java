package it.neverworks.text;

import java.util.Map;
import java.util.HashMap;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Strings;

public class TextFormatterRegistry {
    
    private static Map<String,Class<? extends TextFormatter>> formatters = new HashMap();
    private static TextFormatter defaultFormatter = new DefaultFormatter();
    
    static {
        registerFormatter( "date",      it.neverworks.text.formatters.DateFormatter.class );
        registerFormatter( "time",      it.neverworks.text.formatters.TimeFormatter.class );
        registerFormatter( "datetime",  it.neverworks.text.formatters.TimeStampFormatter.class );
        registerFormatter( "timestamp", it.neverworks.text.formatters.TimeStampFormatter.class );
        registerFormatter( "number",    it.neverworks.text.formatters.NumberFormatter.class );
        registerFormatter( "int",       it.neverworks.text.formatters.IntegerFormatter.class );
        registerFormatter( "integer",   it.neverworks.text.formatters.IntegerFormatter.class );
        registerFormatter( "decimal",   it.neverworks.text.formatters.DecimalFormatter.class );
        registerFormatter( "money",     it.neverworks.text.formatters.MoneyFormatter.class );
        registerFormatter( "currency",  it.neverworks.text.formatters.CurrencyFormatter.class );
        registerFormatter( "long",      it.neverworks.text.formatters.LongFormatter.class );
        registerFormatter( "choice",    it.neverworks.text.formatters.ChoiceFormatter.class );
        registerFormatter( "bool",      it.neverworks.text.formatters.BooleanFormatter.class );
        registerFormatter( "boolean",   it.neverworks.text.formatters.BooleanFormatter.class );
        registerFormatter( "optional",  it.neverworks.text.formatters.OptionalFormatter.class );
        registerFormatter( "string",    it.neverworks.text.formatters.StringFormatter.class );
        registerFormatter( "text",      it.neverworks.text.formatters.StringFormatter.class );
        registerFormatter( "select",    it.neverworks.text.formatters.SelectFormatter.class );
        registerFormatter( "repeat",    it.neverworks.text.formatters.RepeatFormatter.class );
        registerFormatter( "json",      it.neverworks.text.formatters.JSONFormatter.class );
        registerFormatter( "html",      it.neverworks.text.formatters.HTMLFormatter.class );
        registerFormatter( "error",     it.neverworks.text.formatters.ErrorFormatter.class );
        registerFormatter( "repr",      it.neverworks.text.formatters.ReprFormatter.class );
        registerFormatter( "bytes",     it.neverworks.text.formatters.BytesFormatter.class );
        registerFormatter( "url",       it.neverworks.text.formatters.URLFormatter.class );
        
        //legacy
        registerFormatter( "nullable",  DefaultFormatter.class );

    }
    
    public static void registerFormatter( String name, Class<? extends TextFormatter> formatterClass ) {
        formatters.put( name, formatterClass );
    }

    public static void registerDefaultFormatter( Class<? extends TextFormatter> formatterClass ) {
        defaultFormatter = (TextFormatter) Reflection.newInstance( formatterClass );
    }

    public static TextFormatter getDefaultFormatter() {
        return defaultFormatter;
    }

    public static TextFormatter getFormatter( String name ) {
        TextFormatter formatter = null;
        if( formatters.containsKey( name ) ) {
            return (TextFormatter) Reflection.newInstance( formatters.get( name ) );
        } else {
            throw new IllegalArgumentException( "Unknown text formatter: " + name );
        }
    }
    
    public static TextFormatter getConfiguredFormatter( String name, String specification ) {
        if( Strings.hasText( name ) ) {
            TextFormatter formatter = getFormatter( name );
            
            if( Strings.hasText( specification ) && formatter instanceof ConfigurableFormatter ) {
                ((ConfigurableFormatter) formatter).setFormatSpecification( specification );
            }
            
            return formatter;
            
        } else {
            return getDefaultFormatter();
        }
    }

}