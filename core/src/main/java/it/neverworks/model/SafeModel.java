package it.neverworks.model;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Arguments;

public class SafeModel extends BaseModel {
    
    public SafeModel() {
        super();
    }
    
    public SafeModel( Arguments arguments ) {
        this();
        set( arguments );
    }

    public <T> T get( String name ) {
        return (T)this.modelInstance.safeEval( name );
    }

    public <T> T probe( String name, Wrapper initializer ) {
        return this.modelInstance.safeProbe( name, initializer );
    }
    
    public <T extends BaseModel> T set( String name, Object value ) {
        return (T) this.modelInstance.safeAssign( name, value ).actual();
    }

    public <T extends BaseModel> T set( Arguments arguments ) {
        return (T) this.modelInstance.safeAssign( arguments ).actual();
    }

    public <T extends BaseModel> T set( String arguments ) {
        return (T) this.modelInstance.safeAssign( arguments ).actual();
    }

    public <T extends BaseModel> T set( Tuple tuple, String... names ) {
        return (T) this.modelInstance.safeAssign( tuple, names ).actual();
    }
    
}