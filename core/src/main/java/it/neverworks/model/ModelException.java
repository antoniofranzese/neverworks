package it.neverworks.model;

public class ModelException extends RuntimeException {
    
    public ModelException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public ModelException( String message ){
        super( message );
    }
}