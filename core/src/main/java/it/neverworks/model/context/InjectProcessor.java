package it.neverworks.model.context;

import static it.neverworks.language.*;

import it.neverworks.log.Logger;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Application;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.DefaultFactory;
import it.neverworks.model.description.DefaultMetadata;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyInitializer;
import it.neverworks.model.description.RequiredPropertyException;
import it.neverworks.context.Context;

public class InjectProcessor implements PropertyProcessor, DefaultFactory, PropertyInitializer, PropertyMetadata {
    
	private static final Logger logger = Logger.of( InjectProcessor.class );

    private Inject annotation;
    private String[] beanNames;
    private MetaPropertyManager property;
    private boolean validate = false;
    private DefaultFactory cascadingDefault = null;
    
    public InjectProcessor( AnnotationInfo<Inject> info ) {
        this.annotation = info.getAnnotation();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        this.property = property;
        this.beanNames = this.annotation.value();
        for( int i = 0; i < this.beanNames.length; i++ ) {
            if( Strings.isEmpty( this.beanNames[ i ] ) || ".".equals( this.beanNames[ i ].trim() ) ) {
                this.beanNames[ i ] = property.getName();
            } else {
                this.beanNames[ i ] = this.beanNames[ i ].trim();
            }
        }
        if( logger.isDebugEnabled() ) {
            logger.debug( "Will inject {} into {}", this.beanNames, property.getFullName() );
        }
        
        property.metadata().add( this );
        if( ! this.annotation.lazy() ) {
            property.addInitializer( this );
        }
        this.validate = this.annotation.validate();
    }

    protected Object getBean( ModelInstance instance, String feature ) {

        Object selectedBean = null;

        for( String beanName: this.beanNames ) {
           try {
                selectedBean = Context.get( beanName );
            } catch( Exception ex ) {
                selectedBean = null;
            }

            if( selectedBean != null ) {
                logger.debug( "Injecting {0} into {1.fullName} via {2}", beanName, this.property, feature );
                break;
            } 
        }

        if( selectedBean == null ) {
            Class beanType = this.property.getStorageType();
            if( beanType != null & beanType != Object.class && Context.contains( beanType ) ) {
                logger.debug( "Injecting {0.name} instance into {1.fullName} via {2}", beanType, this.property, feature );
                selectedBean = Context.get( beanType );
            }
        } 

        if( selectedBean == null ) {
            for( String beanName: this.beanNames ) {

                selectedBean = Application.getProperty( beanName );

                if( selectedBean != null ) {
                    if( logger.isDebugEnabled() ) {
                        logger.debug( "Reading application property {0} into {1.fullName} via {}", beanName, this.property, feature );
                    }
                    break;
                } 
            }
        }
        
        if( selectedBean == null ) {
            for( String beanName: this.beanNames ) {
                try {
                    selectedBean = System.getProperty( beanName );
                } catch( Exception ex ) {
                    selectedBean = null;
                }

                if( selectedBean != null ) {
                    if( logger.isDebugEnabled() ) {
                        logger.debug( "Reading system property {0} into {1.fullName} via {}", beanName, this.property, feature );
                    }
                    break;
                } 
            }
        }
        
        if( selectedBean == null && cascadingDefault != null ){
            selectedBean = cascadingDefault.getDefaultValue( instance );
        }

        return selectedBean;
        
    }

    public Object getDefaultValue( ModelInstance instance ) {
        return getBean( instance, "default" );
    }

    public void initProperty( MetaPropertyManager property, ModelInstance instance ) {
        if( instance.undefined( property.getName() ) ) {
            
            Object processed = null;
            Object bean = getBean( instance, "init" );
            try {
                processed = property.process( bean );
            } catch( IllegalArgumentException ex ) {
                throw new IllegalArgumentException( "Error on injection: " + ex.getMessage(), ex );
            }
            
            if( processed != null ) {
                instance.raw( property.getName(), processed ) ;
            } else if( validate ) {
                throw new RequiredPropertyException( 
                    property
                    ,Strings.message( 
                        "Missing \"{0}\" injection, {1} {2} injectables"
                        ,this.beanNames
                        ,Context.count( property.getStorageType() )
                        ,property.getStorageType().getName()
                    ) 
                );
            } else {
                if( logger.isDebugEnabled() ) {
                    logger.debug( "Missing {} injection, {} {} injectables"
                        ,this.beanNames
                        ,Context.count( property.getStorageType() )
                        ,property.getStorageType().getName()
                    );
                }
            }
        }
    }

    public void meetProperty( MetaPropertyManager property ){
        if( property.metadata().contains( DefaultMetadata.class ) ) {
            cascadingDefault = property.metadata( DefaultMetadata.class ).getFactory();
            
        } else if( ! property.getRequired() ) {
            validate = false;
        }
        
        property.setDefault( this );
    }
        
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( InjectProcessor.class ) ) {
            processProperty( property );
        }
    }
    
}