package it.neverworks.model.context;

import java.util.ArrayList;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.beans.factory.support.ManagedMap;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.BeansException;

import it.neverworks.log.Logger;

import static it.neverworks.language.*;
import it.neverworks.context.Component;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.collections.CollectionMetadata;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.converters.Converter;

@Component
public class Adapter implements InstantiationAwareBeanPostProcessor {
    
    private static final Logger logger = Logger.of( Adapter.class );
    
    public boolean postProcessAfterInstantiation( Object bean, String beanName ) {
        return true; // Perform property population
    } 

    public Object postProcessBeforeInstantiation( Class beanClass, String beanName ) {
        return null; // Default instantiation
    } 
    
    public Object postProcessAfterInitialization( Object bean, String beanName ) throws BeansException {
        return bean; // Original bean
    }

    public Object postProcessBeforeInitialization( Object bean, String beanName ) throws BeansException {
        return bean;  // Original bean
    }

    public PropertyValues postProcessPropertyValues( PropertyValues pvs, java.beans.PropertyDescriptor[] pds, Object bean, String beanName ) {
        if( bean instanceof Modelized ) {
            logger.debug( "PostProcessing \"{0}\" {1.class.name} instance", beanName, bean );
            ModelDescriptor model = ModelDescriptor.of( bean );
            
            for( PropertyValue pv: pvs.getPropertyValues() ) {
                
                PropertyDescriptor property = model.property( pv.getName() );

                if( pv.getValue() instanceof TypedStringValue ) {
                    if( property.getConverter() != null ) {
                        pv.setConvertedValue( property.process( ((TypedStringValue) pv.getValue()).getValue() ) );
                    } else {
                        Converter converter = AutoConvertProcessor.inferConverter( property.getType() );
                        pv.setConvertedValue( converter.convert( ((TypedStringValue) pv.getValue()).getValue() ) );
                    }

                } else if( pv.getValue() instanceof ManagedList && property.metadata().contains( CollectionMetadata.class ) ) {
                    CollectionMetadata collection = property.metadata( CollectionMetadata.class );
                    ManagedList list = (ManagedList) pv.getValue();

                    for( int i: range( list ) ) {
                        if( list.get( i ) instanceof TypedStringValue ) {
                            list.set( i, collection.processItem( ((TypedStringValue) list.get( i ) ).getValue() ) );
                        }
                    }
                    
                } else if( pv.getValue() instanceof ManagedMap && property.metadata().contains( CollectionMetadata.class ) ) {
                    CollectionMetadata collection = property.metadata( CollectionMetadata.class );
                    ManagedMap map = (ManagedMap) pv.getValue();

                    for( Object originalKey: new ArrayList( map.keySet() ) ) {

                        Object processedKey;
                        if( originalKey instanceof TypedStringValue ) {
                            processedKey = collection.processKey( ((TypedStringValue) originalKey ).getValue() );
                        } else {
                            processedKey = originalKey;
                        }
                        
                        Object processedValue;
                        if( map.get( originalKey ) instanceof TypedStringValue ) {
                            processedValue = collection.processItem( ((TypedStringValue) map.get( originalKey ) ).getValue() );
                        } else {
                            processedValue = map.get( originalKey );
                        }
                        
                        map.remove( originalKey );
                        map.put( processedKey, processedValue );
                    }
                
                }
            
            }
        }

        return pvs;
    } 
}