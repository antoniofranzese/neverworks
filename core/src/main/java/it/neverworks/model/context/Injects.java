package it.neverworks.model.context; 

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.context.Context;
import it.neverworks.context.ContextException;

public class Injects {

    public static <T> T lookup( String name ) {
        return lookup( name, (Class<T>) null );
    }

    public static <T> T lookup( Class<T> type ) {
        return lookup( (String) null, type );
    }

    public static <T> T lookup( String name, Class<T> type ) {
        if( Strings.hasText( name ) && Context.contains( name ) ) {
            return (T) Context.get( name );
        } else if( type != null && Context.contains( type ) && Context.count( type ) == 1 ){
            return Context.get( type );
        } else {
            return null;
        }
    }
    
    public static <T> T lookup( String[] names, Class<T> type ) {
        for( String name: names ) {
            if( Strings.hasText( name ) && Context.contains( name ) ) {
                return (T) Context.get( name );
            }
        }
        
        if( Context.contains( type ) && Context.count( type ) == 1 ){
            return Context.get( type );
        } else {
            return null;
        }
    }
    
    public static <T> T require( String name ) {
        return require( name, (Class<T>) null );
    }
    
    public static <T> T require( Class<T> type ) {
        return require( (String) null, type );
    }
    
    public static <T> T require( String name, Class<T> type ) {
        T object = lookup( name, type );
        if( object != null ) {
            return object;
        } else {
            throw new ContextException( msg( "Missing required <{1}> \"{0}\" injection", name, type ) ); 
        }            
    }
    
    public static <T> T require( String[] names, Class<T> type ) {
        T object = lookup( names, type );
        if( object != null ) {
            return object;
        } else {
            throw new ContextException( msg( "Missing required <{1}> \"{0}\" injection", list().cat( names ).join( ", " ), type ) ); 
        }            
    }

}