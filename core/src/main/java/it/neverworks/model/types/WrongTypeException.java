package it.neverworks.model.types;

public class WrongTypeException extends IllegalArgumentException {
    
    protected TypeDefinition type;
    
    public WrongTypeException( String message, TypeDefinition type ) {
        super( message );
        this.type = type;
    }

    public WrongTypeException( String message, Throwable source, TypeDefinition type ) {
        super( message, source );
        this.type = type;
    }
    
    public TypeDefinition getType(){
        return this.type;
    }
    

}