package it.neverworks.model.types;

import it.neverworks.lang.Java;
import it.neverworks.model.converters.Converter;

public abstract class PrimitiveType<T> extends BaseType<T> {

    protected Class primitiveType;

    public PrimitiveType( Class<?> type ) {
        this.type = Java.getReferenceType( type );
        this.primitiveType = Java.getPrimitiveType( type );
    }

    public PrimitiveType( Class<?> type, Converter converter ) {
        this( type );
        this.converter = converter;
    }
    
    public boolean isInstance( Object object ) {
        if( this.type != null ) {
            return this.type.isInstance( object ) || this.primitiveType.isInstance( object );
        } else {
            return true;
        }
    }
    
    public abstract T newInstance();
    
}