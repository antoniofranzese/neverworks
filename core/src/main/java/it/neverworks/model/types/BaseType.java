package it.neverworks.model.types;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.Reshaper;
import it.neverworks.text.TextFormat;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;

public abstract class BaseType<T> extends AbstractType<T> {
    
    protected Class<?> type = Object.class;
    protected Converter converter = null;
    
    public Converter getConverter() {
        return this.converter;
    }
    
    public Class<?> getRawType(){
        return this.type;
    }
    
}