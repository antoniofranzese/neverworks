package it.neverworks.model.types;

import it.neverworks.lang.Reflection;

public interface TypeAware {
    void setRelatedClass( Class<?> cls );

    static void set( TypeAware target, java.lang.reflect.Type type ) {
        if( target instanceof GenericTypeAware ) {
            ((GenericTypeAware) target).setRelatedType( type );
        } else {
            target.setRelatedClass( Reflection.getRawClass( type ) );
        }
    }
}