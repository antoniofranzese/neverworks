package it.neverworks.model.types;

import it.neverworks.lang.Reflection;

public interface GenericTypeAware extends TypeAware {
    default void setRelatedClass( Class<?> cls ) {
        this.setRelatedType( (java.lang.reflect.Type) cls );
    }

    void setRelatedType( java.lang.reflect.Type type );

}