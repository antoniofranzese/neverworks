package it.neverworks.model.types;

import it.neverworks.lang.Numbers;
import it.neverworks.model.converters.Converter;

public class ShortType extends PrimitiveType<Short> {
    
    public ShortType(){
        super( Short.class );
    }
    
    public String getName() {
        return "short";
    }
    
    public String toString() {
        return "ShortType";
    }
    
    public Short newInstance(){
        return Numbers.Short( 0 );
    }

}