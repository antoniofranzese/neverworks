package it.neverworks.model.types;

import it.neverworks.lang.Numbers;
import it.neverworks.model.converters.Converter;

public class DoubleType extends PrimitiveType<Double> {
    
    public DoubleType(){
        super( Double.class );
    }
    
    public String getName() {
        return "double";
    }
    
    public String toString() {
        return "DoubleType";
    }

    public Double newInstance(){
        return Numbers.Double( 0 );
    }
    
}