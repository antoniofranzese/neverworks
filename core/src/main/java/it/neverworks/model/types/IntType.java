package it.neverworks.model.types;

import it.neverworks.lang.Numbers;
import it.neverworks.model.converters.Converter;

public class IntType extends PrimitiveType<Integer> {
    
    public IntType(){
        super( Integer.class );
    }
    
    public String getName() {
        return "int";
    }
    
    public String toString() {
        return "IntType";
    }
    
    public Integer newInstance(){
        return Numbers.Int( 0 );
    }
    
}