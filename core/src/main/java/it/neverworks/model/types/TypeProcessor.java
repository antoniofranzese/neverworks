package it.neverworks.model.types;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;

public class TypeProcessor implements PropertyProcessor, PropertyMetadata {

    protected TypeDefinition definition;

    public TypeProcessor( AnnotationInfo<Type> info ) {
        this.definition = TypeFactory.build( info.getAnnotation().value() );
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.setType( this.definition );
        property.metadata().add( this );
    }

    public void meetProperty( MetaPropertyManager property ) {
        if( property.getConverter() == null ) {
            property.setConverter( this.definition.getConverter() );
        }
    }
    
    public void inheritProperty( MetaPropertyManager property ) {
        if( ! property.metadata().contains( TypeProcessor.class ) ) {
            processProperty( property );
        }
    }

}