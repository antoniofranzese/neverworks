package it.neverworks.model.types;

import it.neverworks.model.converters.Converter;

public class BooleanType extends PrimitiveType<Boolean> {
    
    public BooleanType() {
        super( Boolean.class );
    }

    public String getName() {
        return "boolean";
    }
    
    public String toString() {
        return "BooleanType";
    }

    public Boolean newInstance(){
        return Boolean.FALSE;
    }
    
}