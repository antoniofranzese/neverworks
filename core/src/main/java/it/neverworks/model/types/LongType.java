package it.neverworks.model.types;

import it.neverworks.lang.Numbers;
import it.neverworks.model.converters.Converter;

public class LongType extends PrimitiveType<Long> {
    
    public LongType(){
        super( Long.class );
    }
    
    public String getName() {
        return "long";
    }
    
    public String toString() {
        return "LongType";
    }
    
    public Long newInstance(){
        return Numbers.Long( 0 );
    }

}