package it.neverworks.model.types;

import it.neverworks.lang.Numbers;
import it.neverworks.model.converters.Converter;

public class ByteType extends PrimitiveType<Byte> {
    
    public ByteType() {
        super( Byte.class );
    }
    
    public String getName() {
        return "byte";
    }
    
    public String toString() {
        return "ByteType";
    }

    public Byte newInstance(){
        return Numbers.Byte( 0 );
    }
    
}