package it.neverworks.model.types;

import it.neverworks.lang.annotation.AnnotatedClass;
import it.neverworks.lang.annotation.AnnotationManager;

import it.neverworks.model.types.TypeAware;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.converters.ConvertProcessor;
import it.neverworks.model.converters.Convert;
import it.neverworks.lang.Reflection;

public class CustomType<T> extends BaseType<T> implements MutableTypeDefinition {
    
    public CustomType() {
        this( null );
    }
    
    public CustomType( Class<?> type ) {
        if( type != null ) {
            this.type = type;
            AnnotatedClass annotated = AnnotationManager.getAnnotatedClass( this.type );
            Convert convert = (Convert)annotated.getAnnotation( Convert.class );
            // Convert convert = type.getAnnotation( Convert.class );
            if( convert != null ) {
                this.converter = ConvertProcessor.process( convert, this.type );

            } else {
                AutoConvert autoConvert = (AutoConvert)annotated.getAnnotation( AutoConvert.class );
                if( autoConvert != null ) {
                    this.converter = AutoConvertProcessor.inferConverter( this.type );
                }
            }
        }
    }
    
    public void setType( Class<?> type ){
        this.type = type;
    }

    public void setConverter( Converter converter ) {
        this.converter = converter;
    }
    
}