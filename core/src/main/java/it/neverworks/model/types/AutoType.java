package it.neverworks.model.types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = AutoTypeProcessor.class )
public @interface AutoType {
    Class<?> value();
    String setup() default "";
}
