package it.neverworks.model.types;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;

import it.neverworks.lang.Reflection;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.AutoConvertProcessor;

public class TypeFactory {

    public static TypeDefinition autoBuild( Class<?> source ) {
        return auto( source );
    }

    private static Map shareds = new HashMap(); 

    public static <T> TypeDefinition<T> shared( Class<T> target ) {
        if( ! shareds.containsKey( target ) ) {
            synchronized( shareds ) {
                if( ! shareds.containsKey( target ) ) {
                    shareds.put( target, auto( target ) );
                }
            }
        }
        return (TypeDefinition<T>) shareds.get( target );
    }

    public static TypeDefinition shared( String target ) {
        Class c = Reflection.findClass( target );
        if( c != null ) {
            return shared( c );
        } else {
            throw new IllegalArgumentException( "Cannot find class by name: " + target );
        }
    }

    public static TypeDefinition auto( Type source ) {
        TypeDefinition definition = build( source );
        if( definition.getConverter() == null ) {
            return build( definition, AutoConvertProcessor.inferConverter( Reflection.getRawClass( source ) ) );
        } else {
            return definition;
        }
    }

    public static TypeDefinition build( Type source, Converter converter ) {
        return build( build( source ), converter );
    }
    
    public static TypeDefinition build( TypeDefinition definition, Converter converter ) {
        if( !( definition instanceof MutableTypeDefinition ) ) {
            definition = new CustomType( definition.getRawType() );
        }
        ((MutableTypeDefinition) definition).setConverter( converter );
        return definition;
    }
    
    public static TypeDefinition build( Type source ) {
        Class raw = Reflection.getRawClass( source );
        if( raw.equals( byte.class ) ) {
            return new ByteType();
        } else if( raw.equals( short.class ) || raw.equals( Short.class ) ) {
            return new ShortType();
        } else if( raw.equals( int.class ) || raw.equals( Integer.class ) ) {
            return new IntType();
        } else if( raw.equals( long.class ) || raw.equals( Long.class ) ) {
            return new LongType();
        } else if( raw.equals( float.class ) || raw.equals( Float.class ) ) {
            return new FloatType();
        } else if( raw.equals( double.class ) || raw.equals( Double.class ) ) {
            return new DoubleType();
        } else if( raw.equals( boolean.class ) || raw.equals( Boolean.class ) ) {
            return new BooleanType();
        } else if( raw.equals( char.class ) || raw.equals( Character.class ) ) {
            return new CharType();
        } else if( TypeDefinition.class.isAssignableFrom( raw ) && ! raw.isInterface() ) {
            return (TypeDefinition)Reflection.newInstance( raw );
        } else {
            return new CustomType( raw );
        }
    }
    
}