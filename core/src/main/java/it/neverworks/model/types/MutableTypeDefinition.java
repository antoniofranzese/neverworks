package it.neverworks.model.types;

import it.neverworks.model.converters.Converter;

public interface MutableTypeDefinition {
    void setType( Class<?> type );
    void setConverter( Converter converter );
}