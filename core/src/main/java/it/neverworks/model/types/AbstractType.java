package it.neverworks.model.types;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.Reshaper;
import it.neverworks.text.TextFormat;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;

public abstract class AbstractType<T> implements TypeDefinition<T> {
    
    public boolean isInstance( Object object ) {
        Class type = this.getRawType();
        return type != null ? type.isInstance( object ) : true;
    }
    
    public boolean is( Class<?> other ) {
        Class type = this.getRawType();
        return type != null && other != null ? other.isAssignableFrom( type ) : false;
    }
    
    public T convert( Object value ) {
        Converter converter = this.getConverter();
        Class type = this.getRawType();
        if( converter != null ) {
            Object converted = converter.convert( value );
            if( isInstance( converted ) ) {
                return (T) converted;
            } else if( value != null && type != null ){
                Converter valueConverter = TypeFactory.shared( value.getClass() ).getConverter();
                if( valueConverter instanceof Reshaper ) {
                    Object reshaped = ((Reshaper) valueConverter).reshape( value, type );
                    if( isInstance( reshaped ) ) {
                        return (T) reshaped;
                    } else {
                        return (T) value;
                    }
                } else {
                    return (T) value;
                }
            } else {
                return (T) value;
            }
            
        } else {
            return (T) value;
        }
    }
    
    public T process( Object value ) {
        return process( value, null );
    }

    public T process( Object value, String description, Object... parameters ) {
        if( value != null ){
            Object converted = convert( value );
            if( isInstance( converted ) ) {
                return (T)converted;
            } else {
                String source = null;
                if( parameters != null && parameters.length > 0 ) {
                    source = TextFormat.format( description, parameters );
                } else {
                    source = description;
                }
                
                if( Strings.isEmpty( source ) ) {
                    source = "value";
                }
                
                throw new WrongTypeException( "Wrong " + source + " type, <" + this.getName() + "> or convertible equivalent expected, " + Objects.repr( converted ) + " received", this );
            }
        } else {
            return null;
        }
    }
    
    public T map( Object value ) {
        return (T) process( value );
    }
    
    public T newInstance() {
        Class type = this.getRawType();
        if( type != null ) {
            return (T) Reflection.newInstance( type );
        } else {
            throw new RuntimeException( "Cannot instantiate untyped Type: " + this.getClass().getName() );
        }
    }
    
    public String getName() {
        Class type = this.getRawType();
        return type != null ? type.getName() : "<null type>";
    }
    
    public String toString() {
        Class type = this.getRawType();
        Converter converter = this.getConverter();
       return this.getClass().getSimpleName() + "<class=" + ( type != null ? type.getName() : "untyped" ) + ( converter != null ? ", converter=" + converter.getClass().getName() : "" )+ ">";
    }
    
    public boolean equals( Object other ) {
        if( other instanceof TypeDefinition ) {
            //TODO: refine class match
            TypeDefinition oth = (TypeDefinition) other;
            return ( this.getRawType() == null ? oth.getRawType() == null : this.getRawType().equals( oth.getRawType() ) )
                && ( this.getConverter() == null ? oth.getConverter() == null : this.getConverter().getClass().equals( oth.getConverter() == null ? null : oth.getConverter().getClass() ) );
        
        } else {
            return false;
        }
    
    }
}