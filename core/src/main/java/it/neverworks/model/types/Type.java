package it.neverworks.model.types;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = TypeProcessor.class )
public @interface Type {
    Class<?> value();
}
