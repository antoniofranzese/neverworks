package it.neverworks.model.types;

import it.neverworks.lang.Reflection;
import it.neverworks.model.converters.Converter;

public class TypeDefinitionConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof TypeDefinition ) {
            return value;

        } else if( value instanceof Class ) {
            return TypeFactory.auto( (Class) value );

        } else if( value instanceof String ) {
            Class cls = Reflection.findClass( (String) value );
            if( cls != null ) {
                return TypeFactory.auto( cls );
            } else {
                return value;
            }

        } else if( value instanceof Converter ) {
            return TypeFactory.build( Object.class, (Converter) value );

        } else {
            return value;
        }
    }
}