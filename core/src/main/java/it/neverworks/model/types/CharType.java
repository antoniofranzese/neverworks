package it.neverworks.model.types;

import it.neverworks.model.converters.Converter;

public class CharType extends PrimitiveType<Character> {
    
    public CharType(){
        super( Character.class );
    }
    
    public String getName() {
        return "char";
    }
    
    public String toString() {
        return "CharType";
    }

    public Character newInstance(){
        return Character.MIN_VALUE;
    }

}