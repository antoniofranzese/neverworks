package it.neverworks.model.types;

import it.neverworks.model.converters.Converter;

public abstract class TypeProxy<T> implements TypeDefinition<T> {
    
    public abstract TypeDefinition<T> actual();
    
    public boolean isInstance( Object object ) {
        return actual().isInstance( object );
    }
    
    public boolean is( Class<?> type ) {
        return actual().is( type );
    }

    public Class<?> getRawType() {
        return actual().getRawType();
    }
    
    public String getName() {
        return actual().getName();
    }
    
    public Converter getConverter() {
        return actual().getConverter();
    }
    
    public T convert( Object value ) {
        return actual().convert( value );
    }
    
    public T process( Object value ) {
        return actual().process( value );
    }
    
    public T process( Object value, String description, Object... parameters ) {
        return actual().process( value, description, parameters );
    }
    
    public T newInstance() {
        return actual().newInstance();
    }
    
    public T map( Object value ) {
        return (T) actual().map( value );
    }

}