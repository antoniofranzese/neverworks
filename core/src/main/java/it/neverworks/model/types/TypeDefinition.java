package it.neverworks.model.types;

import it.neverworks.lang.Mapper;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;

@Convert( TypeDefinitionConverter.class )
public interface TypeDefinition<T> extends Mapper<Object,T> {
    boolean isInstance( Object object );
    boolean is( Class<?> object );
    Class<?> getRawType();

    default Class getStorageType() {
        Class type = this.getRawType();
        return type != null ? type : Object.class;
    }

    String getName();
    Converter getConverter();
    T convert( Object value );
    T process( Object value );
    T process( Object value, String description, Object... parameters );
    T newInstance();

    default boolean isSuitable( Object object ) {
        if( isInstance( object ) ) {
            return true;
        } else {
            try {
                return isInstance( convert( object ) );
            } catch( Exception ex ) {
                return false;
            }
        }
    }
    
    static <D> TypeDefinition<D> of( Class<D> cls ) {
        return TypeFactory.build( cls );
    }
}
