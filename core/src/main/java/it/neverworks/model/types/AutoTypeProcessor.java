package it.neverworks.model.types;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.converters.AutoConvertProcessor;

public class AutoTypeProcessor extends AutoConvertProcessor {

    protected AutoType annotation;

    public AutoTypeProcessor( AnnotationInfo<AutoType> info ) {
        super( info.getAnnotation().setup() );
        this.annotation = info.getAnnotation();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.setType( TypeFactory.build( this.annotation.value() ) );
        this.processConverter( property, this.annotation.value() );
    }

}