package it.neverworks.model.types;

import it.neverworks.lang.Numbers;
import it.neverworks.model.converters.Converter;

public class FloatType extends PrimitiveType<Float> {
    
    public FloatType(){
        super( Float.class );
    }
    
    public String getName() {
        return "float";
    }
    
    public String toString() {
        return "FloatType";
    }
    
    public Float newInstance(){
        return Numbers.Float( 0 );
    }
    
}