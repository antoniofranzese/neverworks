package it.neverworks.model;

import it.neverworks.lang.Range;
import it.neverworks.lang.Tuple;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Arguments;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.features.Describe;
import it.neverworks.model.features.Probe;
import it.neverworks.model.features.Get;
import it.neverworks.model.features.Language;
import it.neverworks.model.utils.ModelAdapter;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.utils.HashCodeBuilder;
import it.neverworks.model.utils.EqualsBuilder;

public interface Model extends Modelized, Describe, Probe, Get, Language {
    
    public static Model of( Object object ) {
        if( object instanceof Model ) {
            return (Model)object;
        } else if( object instanceof ModelInstance ) {
            return ((ModelInstance) object).is( Model.class ) ? (Model) ((ModelInstance) object).actual() : new ModelAdapter( (ModelInstance) object );
        } else {
            return new ModelAdapter( ModelInstance.of( object ) );
        }
    }

    default public <T> T get( String name ) {
        return (T)this.retrieveModelInstance().eval( name );
    }

    default public <T> T get( String name, T defaultValue ) {
        return this.retrieveModelInstance().safeEval( name, defaultValue );
    }
    
    default public ModelInstance model() {
        return this.retrieveModelInstance();
    }

    default ModelInstance model( Object object ) {
        return ModelInstance.of( object instanceof String ? get( (String) object ) : object );
    }
    
    default public ModelInstance retrieveModelInstance() {
        throw new RuntimeException( this.getClass().getName() + " is not instrumented" );
    }

    default public <T extends Model> T set( String name, Object value ) {
        return (T) this.retrieveModelInstance().assign( name, value ).actual();
    }

    default public <T extends Model> T set( Arguments arguments ) {
        return (T) this.retrieveModelInstance().assign( arguments ).actual();
    }

    default public <T extends Model> T set( String arguments ) {
        return (T) this.retrieveModelInstance().assign( arguments ).actual();
    }

    default public <T extends Model> T set( Tuple tuple, String... names ) {
        return (T) this.retrieveModelInstance().assign( tuple, names ).actual();
    }

}