package it.neverworks.model.io;

public interface State {
    Iterable<String> names();
    boolean has( Object key );
    Object get( Object key );
    State sub( Object key );
}