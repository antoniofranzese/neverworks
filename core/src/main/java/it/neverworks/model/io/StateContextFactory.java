package it.neverworks.model.io;

import java.util.Map;
import it.neverworks.lang.Arguments;

public class StateContextFactory {
    
    private Map<String, Object> arguments;
    
    public StateContextFactory() {
        this( new Arguments() );
    }
    
    public StateContextFactory( Map<String,Object> arguments ) {
        super();
        this.arguments = arguments;
    }
    
    public StateContext buildContext() {
        return populate( new AnyStateContext() );
    } 
    
    public StateContext buildContext( String name ) {
        return populate( new SimpleStateContext( name ) );
    }
    
    protected StateContext populate( StateContext ctx ) {
        for( String name: arguments.keySet() ) {
            ctx.setProperty( name, arguments.get( name ) );
        }
        return ctx;
    }
    
    public void setArguments( Map<String, Object> arguments ){
        this.arguments = arguments;
    }
    
}