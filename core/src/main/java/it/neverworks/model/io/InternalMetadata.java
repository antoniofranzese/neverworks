package it.neverworks.model.io;

import it.neverworks.model.description.ClassicPropertyManager;
import it.neverworks.model.description.ClassicPropertyProcessor;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;

public class InternalMetadata implements PropertyProcessor, PropertyMetadata, ClassicPropertyProcessor {
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( InternalMetadata.class ) ) {
            processProperty( property );
        }
    }

    public void processProperty( ClassicPropertyManager property ) {
        property.metadata().add( this );
    }

}