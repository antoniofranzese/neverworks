package it.neverworks.model.io;

import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;

import it.neverworks.model.converters.Converter;

public abstract class AbstractStateContext implements StateContext {
    
    protected Map<String, Object> properties = new HashMap<String, Object>();
    protected Map<Class, Converter> converters;
    
    public abstract boolean matches( String pattern );
    public abstract boolean equals( Object other );
    
    public Object convert( Object value ) {
        if( converters == null ) {
            converters = properties.containsKey( "converters" ) ? ( (Map<Class, Converter>) properties.get( "converters" ) ) : new HashMap<Class, Converter>();
        }
        if( value != null && converters.containsKey( value.getClass() ) ) {
            return converters.get( value.getClass() ).convert( value );
        } else {
            return value;
        }
    }
    
    public State extract( Object value ) {
        return StateIO.extract( value, this );
    }

    public Object process( Object value ) {
        return StateIO.isExportable( value ) ? StateIO.extract( value, this ) : value;
    }
    
    public State wrap( Object source ) {
        return StateIO.wrap( source, this );
    }

    public Object merge( State source, Object target ) {
        return StateIO.merge( source, target, this );
    }

    public <T> T getProperty( String name ) {
        return properties.containsKey( name ) ? (T) properties.get( name ) : null;
    }

    public void setProperty( String name, Object value ) {
        properties.put( name, value );
    }
    
}