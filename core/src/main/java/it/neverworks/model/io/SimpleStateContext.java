package it.neverworks.model.io;

import java.util.regex.Pattern;

public class SimpleStateContext extends AbstractStateContext {
    
    public String name;
    
    public SimpleStateContext( String name ) {
        this.name = name;
    }
    
    public boolean matches( String pattern ) {
        return Pattern.compile( pattern ).matcher( name ).matches();
    }
    
    public boolean equals( Object other ) {
        if( other instanceof String && name.equals( other ) ) {
            return true;
        } else {
            return false;
        }
    }
    
}