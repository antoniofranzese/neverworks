package it.neverworks.model.io;

import java.util.List;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Iterator;

import it.neverworks.lang.Objects;
import it.neverworks.lang.ListLike;

public class ListState implements ListLike<Object>, State, ConfigurableState {

    protected List list;
    protected boolean writable;
    protected StateContext context;
    protected boolean qualified = false;
    protected boolean extract = false;
    
    public ListState( Collection collection, StateContext context ) {
        if( collection instanceof List ) {
            this.list = (List) collection;
            this.writable = true;
        } else {
            this.list = new ArrayList( collection );
            this.writable = false;
        }
        this.context = context;
    }

    public Iterable<String> names() {
        throw new RuntimeException( "ListState does not support names" );
    }
    
    protected Object process( Object item ) {
        Object result = extract || qualified ? context.extract( item ) : context.process( item );
        if( qualified && result instanceof ConfigurableState ) {
            ((ConfigurableState) result).setProperty( "qualified", true );
        }
        return result;
    }

    public boolean has( Object key ) {
        if( key instanceof Number ) {
            int index = ((Number) key).intValue();
            if( index < this.size() ) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public Object get( int index ) {
        return context.process( list.get( index ) );
    }

    public Object get( Object key ) {
        if( key instanceof Number ) {
            int index = ((Number) key).intValue();
            if( index < list.size() ) {
                return this.process( list.get( index ) );
            } else {
                throw new IllegalArgumentException( "ListState index out of range: " + index );
            }
        } else {
            throw new IllegalArgumentException( "Invalid key for ListState: " + Objects.repr( key ) );
        }
    }
    
    public Object set( int index, Object element ) {
        if( writable ) {
            return context.process( list.set( index, element ) );
        } else {
            throw readonly( "set" );
        }
    } 

    public boolean add( Object e ) {
        if( writable ) {
            return list.add( e );
        } else {
            throw readonly( "add" );
        }
    }

    public void add( int index, Object element ) {
        if( writable ) {
            list.add( index, element );
        } else {
            throw readonly( "add" );
        }
    } 
    
    public int size() {
        return list.size();
    }

    private class StateIterator implements Iterator {
        private Iterator iterator;
        public StateIterator( Iterator iterator ) {
            this.iterator = iterator;
        }
        
        public boolean hasNext() {
            return this.iterator.hasNext();
        }
        
        public Object next() {
            return ListState.this.process( this.iterator.next() );
        }
        
        public void remove() {
            this.iterator.remove();
        }
    }
    
    public Iterator iterator() {
        return new StateIterator( list.iterator() );
    }
    
    public State sub( Object key ) {
        Object value = this.get( key );
        if( value instanceof State ) {
            return (State) value; 
        } else {
            throw new IllegalArgumentException( "Key does not contain a State: " + key );
        }
    }

    public Object getProperty( String name ) {
        if( "qualified".equals( name ) ) {
            return this.qualified;
        } if( "extract".equals( name ) ) {
            return this.extract;
        } else {
            return null;
        }
    }
    
    public void setProperty( String name, Object value ) {
        if( "qualified".equals( name ) ) {
            this.qualified = (Boolean) value;
        } else if( "extract".equals( name ) ) {
            this.extract = (Boolean) value;
        }
    }
    
    protected RuntimeException readonly( String method ) {
        return new RuntimeException( "Cannot use '" + method + "' method, this ListState is read-only" );
    }
    
    public String toString() {
        return this.getClass().getSimpleName() + "[" + ( this.list != null ? this.list.size() : "null" ) + "]";
    }
    
}