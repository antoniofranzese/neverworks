package it.neverworks.model.io;

import java.util.Map;
import java.util.Iterator;
import java.util.Collection;

import it.neverworks.lang.Objects;

public class MapState extends MapLikeState {
    private Map map;
    
    public MapState( Map map ) {
        this( map, new AnyStateContext() );
    }
    
    public MapState( Map map, StateContext context ) {
        if( map != null ) {
            this.map = map;
            this.context = context;
        } else {
            throw new IllegalArgumentException( "Null map" );
        }
    }
    
    public Iterable<String> names() {
        return this.map.keySet();
    }
    
    public boolean has( Object key ) {
        return this.map.containsKey( key );
    }
    
    public Object get( Object key ) {
        return context.process( this.map.get( key ) );
    }
    
	public Object put( String key, Object value ) {
	    return this.map.put( key, value );
	}
    
    public int size() {
        return map.size();
    }
    
    public String toString() {
        return "MapState(" + map.keySet() + ")";
    }
    
}