package it.neverworks.model.io;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

import static it.neverworks.language.*;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Objects;
import it.neverworks.model.description.Modelized;

public class StateIO {
    
    public static State extract( Object model ) {
        return extract( model, new AnyStateContext() );
    }

    public static State extract( Object model, StateContext context ) {
        if( model != null ) {
            if( model instanceof Exportable ) {
                return ((Exportable) model).exportState( context );
            } else {
                return wrap( model, context );
            }
        } else {
            return EmptyState.INSTANCE;
        }
    }
    public static State wrap( Object obj ) {
        return wrap( obj, new AnyStateContext() );
    }
    
    public static State wrap( Object obj, StateContext context ) {
        if( obj instanceof Class ) {
            Class cls = (Class)obj;
            if( Collection.class.isAssignableFrom( cls ) ) {
                throw new RuntimeException( "Collection wrap is not supported (yet)" );
            } else {
                return new ModelState( cls, context );
            }
            
        } else if( obj != null ) {
            if( obj instanceof State ) {
                return (State) obj;
            } else if( obj instanceof Collection ){
                return new ListState( (Collection) obj, context );
            } else if( obj instanceof Map ){
                return new MapState( (Map) obj, context );
            } else {
                return new ModelState( obj, context );
            }
        } else {
            return EmptyState.INSTANCE;
        }
    }
 
    public static <T> T merge( Object source, Class targetClass ) {
        return merge( source, targetClass, new AnyStateContext() );
    }
    
    public static <T> T merge( Object source, Class targetClass, StateContext context ) {
        Object target;
        if( Importable.class.isAssignableFrom( targetClass ) ) {
            target = type( targetClass ).newInstance();
        } else {
            target = wrap( targetClass, context );
        }
        return merge( source, target, context );
    }

    public static <T> T merge( Object source, Object target ) {
        return merge( source, target, new AnyStateContext() );
    }

    public static <T> T merge( Object source, Object target, StateContext context ) {
        State sourceState = wrap( source, context );
        if( target instanceof Importable ) {
            ((Importable) target).importState( sourceState, context );
            return (T)target;
        } else {
            State targetState = wrap( target, context );
            if( targetState instanceof MergeableState ) {
                return (T)((MergeableState) targetState).merge( sourceState );
            } else {
                throw new IllegalArgumentException( "Target is not writable: " + Objects.repr( target ) );
            }
        }
        
    }

    private final static Map<Class, Description> cache = new HashMap<Class, Description>();

    private static class Description {
        private boolean exportable = false;
        private boolean importable = false;
    }
    
    /* package */ static boolean isExportable( Object value ) {
        if( value != null ) {
            return isExportable( value.getClass() );
        } else {
            return false;
        }
    }

    /* package */ static boolean isExportable( Class valueClass ) {
        if( valueClass != null ) {
            return getDescription( valueClass ).exportable;
        } else {
            return false;
        }
    }
    
    /* package */ static Description getDescription( Class target ) {
        if( !( cache.containsKey( target ) ) ) {
            Description d = new Description();
            d.exportable = Modelized.class.isAssignableFrom( target ) 
                || Exportable.class.isAssignableFrom( target )
                || Collection.class.isAssignableFrom( target )
                || Map.class.isAssignableFrom( target )
                || Annotations.has( target, Export.class );

            cache.put( target, d );
        }

        return cache.get( target );
    }
    
}