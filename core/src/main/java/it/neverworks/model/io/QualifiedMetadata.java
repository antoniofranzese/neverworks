package it.neverworks.model.io;

import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;

public class QualifiedMetadata implements PropertyProcessor, PropertyMetadata {
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( QualifiedMetadata.class ) ) {
            processProperty( property );
        }
    }

}