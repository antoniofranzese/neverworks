package it.neverworks.model.io;

import java.util.List;
import java.util.Map;
import java.util.Collection;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.collections.CollectionMetadata;
import it.neverworks.model.types.TypeDefinition;

public class ModelCollectionState implements MergeableState {

    protected PropertyDescriptor property;
    protected CollectionMetadata metadata;
    protected StateContext context;

    public ModelCollectionState( PropertyDescriptor property, StateContext context ) {
        this.property = property;
        this.metadata = property.metadata( CollectionMetadata.class );
        this.context = context;
    }
    
    public Iterable<String> names() {
        throw unsupported( "names" );
    }

    public boolean has( Object key ) {
        throw unsupported( "has" );
    }
    
    public Object get( Object key ) {
        throw unsupported( "get" );
    }
    
    public State sub( Object key ){
        throw unsupported( "sub" );
    }

    public Object merge( State state ) {
        Object target = metadata.newInstance();
        boolean qualified = property.metadata().contains( QualifiedMetadata.class );

        //System.out.println( "Collection Merging (qualified=" + qualified + ") " + property.getFullName() + ": " + state );
        if( state instanceof Collection && target instanceof Collection ) {
            Collection collection = (Collection) target;
            TypeDefinition targetType = metadata.getItemType();
            Class targetClass = targetType.getStorageType();
            //System.out.println( "Target " + targetType );


            for( Object item: (Collection)state ) {
                Class itemClass = targetClass;
                
                if( qualified && item instanceof Map ) {
                    if( ((Map) item).containsKey( ".class" ) ) {
                        String className = (String)((Map) item).get( ".class" );
                        if( Strings.hasText( className ) ) {
                            Class qualifiedClass = Reflection.findClass( className );
                            if( qualifiedClass != null ) {
                                itemClass = qualifiedClass;
                            } else {
                                throw new RuntimeException( "Missing qualifying class: " + className );
                            }
                        }
                    }
                }

                if( itemClass != null ) {
                    collection.add( context.merge( context.wrap( item ), context.wrap( itemClass ) ) );
                } else {
                    collection.add( item );
                }
            }
        }
        return target;
    }
    
    protected RuntimeException unsupported( String method ) {
        return new RuntimeException( this.getClass().getSimpleName() + "does not support '" + method + "' method" );
    }
}