package it.neverworks.model.io;

import it.neverworks.lang.Strings;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;

public class ExportMetadata implements PropertyProcessor, PropertyMetadata {
    
    private AnnotationInfo<Export> info;
    private String commonName;
    private String jsonName;
    private String xmlName;
    
    public ExportMetadata( AnnotationInfo<Export> info ) {
        this.info = info;
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
        this.processName( property.getName() );
    }
    
    protected void processName( String name ) {
        if( Strings.hasText( this.info.annotation().json() ) ) {
            this.jsonName = this.info.annotation().json();
        } else if( Strings.hasText( this.info.annotation().value() ) ) {
            this.jsonName = this.info.annotation().value();
        } else {
            this.jsonName = name;
        }

        if( Strings.hasText( this.info.annotation().xml() ) ) {
            this.xmlName = this.info.annotation().xml();
        } else if( Strings.hasText( this.info.annotation().value() ) ) {
            this.xmlName = this.info.annotation().value();
        } else {
            this.xmlName = name;
        }

        if( Strings.hasText( this.info.annotation().value() ) ) {
            this.commonName = this.info.annotation().value();
        } else {
            this.commonName = name;
        }

    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( ExportMetadata.class ) ) {
            processProperty( property );
        }
    }
    
    public String getJsonName() {
        return this.jsonName;
    }
    
    public String getXmlName() {
        return this.xmlName;
    }
    
    public String getCommonName(){
        return this.commonName;
    }

}