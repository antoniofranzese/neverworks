package it.neverworks.model.io;

public interface Importable {
    void importState( State state, StateContext context );
}