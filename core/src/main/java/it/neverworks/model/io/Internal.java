package it.neverworks.model.io;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.TYPE, ElementType.METHOD })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = InternalMetadata.class )
public @interface Internal {
    String[] value() default {};
}
