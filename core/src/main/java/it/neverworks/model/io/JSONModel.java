package it.neverworks.model.io;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import it.neverworks.encoding.JSON;
import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.Modelized;
import it.neverworks.lang.Arguments;

public interface JSONModel extends Modelized, JSONEncodable {
    
    default JsonNode toJSON() {
        ModelInstance model = this.retrieveModelInstance();
        ObjectNode result = JSON.object();
        for( String name: model.names() ) {
            PropertyDescriptor property = model.property( name );
            if( property.isReadable() && ! property.metadata().contains( InternalMetadata.class ) ) {
                result.set( 
                    property.metadata().contains( ExportMetadata.class ) ? property.metadata( ExportMetadata.class ).getJsonName() : name
                    , JSON.encode( model.get( name ) ) );
            }
        }
        return JSON.encode( result );
    }
    
}