package it.neverworks.model.io;

import java.util.regex.Pattern;

public class AnyStateContext extends AbstractStateContext {
    
    public boolean matches( String pattern ) {
        return true;
    }
    
    public boolean equals( Object other ) {
        if( other instanceof String || other instanceof AnyStateContext ) {
            return true;
        } else {
            return false;
        }
    }
    
}