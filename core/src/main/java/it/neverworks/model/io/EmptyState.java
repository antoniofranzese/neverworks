package it.neverworks.model.io;

import java.util.Iterator;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.encoding.json.JSONEncodable;

public class EmptyState implements State, JSONEncodable {
    public final static EmptyState INSTANCE = new EmptyState();
    private final static ArrayList<String> NAMES = new ArrayList();
    
    private EmptyState() {}
    
    public Iterable<String> names() {
        return NAMES;
    }
    
    public boolean has( Object name ) {
        return false;
    }
    
    public Object get( Object name ) {
        return null;
    }
    
    public State sub( Object name ) {
        return new EmptyState();
    }
    
    public String toString() {
        return "EmptyState";
    }
    
    public JsonNode toJSON() {
        return NullNode.instance;
    }
    
}