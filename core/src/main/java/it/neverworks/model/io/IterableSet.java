package it.neverworks.model.io;

import java.util.Collection;
import java.util.Set;
import java.util.Iterator;

public class IterableSet<E> implements Set<E> {
    
    private Iterable<E> iterable;
    
    public IterableSet( Iterable<E> iterable ) {
        this.iterable = iterable;
    }
    
    public boolean add( E e ) {
        throw unsupported( "add" );
    }
        
    public boolean addAll( Collection<? extends E> c )  {
        throw unsupported( "addAll" );
    }
        
    public void clear()  {
        throw unsupported( "" );
    }
        
    public boolean contains( Object o )  {
        throw unsupported( "" );
    }
        
    public boolean containsAll( Collection<?> c )  {
        throw unsupported( "" );
    }
        
    public boolean isEmpty()  {
        throw unsupported( "" );
    }
        
    public Iterator<E> iterator()  {
        return this.iterable.iterator();
    }
        
    public boolean remove( Object o )  {
        throw unsupported( "" );
    }
        
    public boolean removeAll( Collection<?> c )  {
        throw unsupported( "" );
    }
        
    public boolean retainAll( Collection<?> c )  {
        throw unsupported( "" );
    }
        
    public int size()  {
        throw unsupported( "" );
    }
        
    public Object[] toArray() {
        throw unsupported( "" );
    }
        
    public <T> T[] toArray( T[] a ) {
        throw unsupported( "" );
    }
    
    protected RuntimeException unsupported( String method ) {
        return new IllegalStateException( "Method is not supported: " + method );
    }
        
}