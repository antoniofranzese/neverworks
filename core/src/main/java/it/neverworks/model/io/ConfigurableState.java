package it.neverworks.model.io;

public interface ConfigurableState {
    
    Object getProperty( String name );
    void setProperty( String name, Object value );
}