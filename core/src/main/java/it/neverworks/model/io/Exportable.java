package it.neverworks.model.io;

public interface Exportable {
    State exportState( StateContext context );
}