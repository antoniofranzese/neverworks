package it.neverworks.model.io;

import java.util.Map;
import java.util.Set;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Collection;

import static it.neverworks.language.*;
import it.neverworks.lang.Objects;
import it.neverworks.model.Model;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.PropertyMetadataRegistry;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.collections.CollectionMetadata;

public class ModelState extends MapLikeState implements MergeableState, ConfigurableState {
    private ModelInstance model;
    private ClassDescription description;
    private final static Map<Class, ClassDescription> cache = new HashMap<Class, ClassDescription>();
    private boolean qualified = false;

    private class PropertyDescription {
        private boolean export = false;
        private boolean qualified = false;
    }

    private class ClassDescription {
        private Map<String, PropertyDescription> properties = new HashMap<String, PropertyDescription>();
    }

    public ModelState( Class modelClass, StateContext context ) {
        this( type( modelClass ).newInstance(), context );
    }
    
    public ModelState( Object model, StateContext context ) {
        if( model != null ) {
            this.model = model instanceof Modelized ? ModelInstance.of( model ) : new ModelInstance( model );
            Class target = this.model.descriptor().target();
            if( !( cache.containsKey( target ) ) ) {
                ClassDescription d = new ClassDescription();

                for( PropertyDescriptor property: this.model.descriptor().properties() ) {
                    if( !( property.metadata().contains( InternalMetadata.class ) ) ) {
                        PropertyDescription p = new PropertyDescription();
                        if( property.metadata().contains( QualifiedMetadata.class ) ) {
                            p.qualified = true;
                        }
                        if( property.metadata().contains( ExportMetadata.class ) ) {
                            p.export = true;
                        }
                        d.properties.put( property.getName(), p );
                    }
                }
                
                cache.put( target, d );
            }
            this.description = cache.get( target );
            this.context = context;
        } else {
            throw new IllegalArgumentException( "Null model" );
        }
    }
    
    public Iterable<String> names() {
        if( qualified ) {
            Set names = new HashSet( description.properties.keySet() );
            names.add( ".class" );
            return names;
        } else {
            return description.properties.keySet();
        }
    }
    
    public boolean has( Object key ) {
        return description.properties.containsKey( key ) || ( qualified && ".class".equals( key ) );
    }
    
    public Object get( Object key ) {
        String name = key.toString();
        if( ".class".equals( name ) ) {
            return model.descriptor().target().getName();
        } else {
            Object value = context.convert( this.model.get( name ) );
            Object result = isExportable( key, value != null ? value.getClass() : null ) ? context.extract( value ) : value;
            return configure( model.descriptor().property( name ), result );
        }
    }
    
	public Object put( String key, Object value ) {
        if( description.properties.containsKey( key ) ) {
            Object current = this.model.get( key );
            this.model.set( key, value );
    	    return current;
        } else {
            throw new IllegalArgumentException( "Invalid key: " + key );
        }
	}

    public Object merge( State state ) {
        for( String name: state.names() ) {
            if( description.properties.containsKey( name ) ) {
                PropertyDescriptor property = model.descriptor().property( name );
                Class type = property.getStorageType();
                if( isExportable( name, type ) ) {
                    State target;
                    if( property.metadata().contains( CollectionMetadata.class ) ) {
                        // CollectionMetadata meta = property.metadata( CollectionMetadata.class );
                        // System.out.println( "Type: " + meta.getItemType().getType() );
                        target = new ModelCollectionState( property, context );
                    } else {
                        target = new ModelState( type, context );
                    }
                    //System.out.println( "Merging " + name + " as " + type.getName() + " to " + target );
                    
                    model.set( name, context.merge( state.sub( name ), configure( property, target ) ) );
                } else {
                    //System.out.println( "Setting " + name + " to " + state.get( name ) );
                    model.set( name, state.get( name ) );
                }
            }
        }
        return model.actual();
    }
    
    public int size() {
        return description.properties.size();
    }

    public String toString() {
        return "ModelState(" + Objects.className( model.actual() ) + ": " + this.names() + ")";
    }
    
    protected boolean isExportable( Object key, Class valueClass ) {
        return StateIO.isExportable( valueClass ) 
            || description.properties.get( key ).export;
    }

    protected Object configure( PropertyDescriptor property, Object target ) {
        if( target instanceof ConfigurableState ){
            PropertyMetadataRegistry metadata = property.metadata();
            if( metadata.contains( QualifiedMetadata.class ) ) {
                ((ConfigurableState) target).setProperty( "qualified", true );
            }
            if( metadata.contains( ExportMetadata.class ) ) {
                ((ConfigurableState) target).setProperty( "extract", true );
            }
        }
        return target;
    }
    
    public Object getProperty( String name ) {
        if( "qualified".equals( name ) ) {
            return this.qualified;
        } else {
            return null;
        }
    }
    
    public void setProperty( String name, Object value ) {
        if( "qualified".equals( name ) ) {
            this.qualified = (Boolean) value;
        }
    }
    
}