package it.neverworks.model.io;

public interface MergeableState extends State {
    Object merge( State state );
}