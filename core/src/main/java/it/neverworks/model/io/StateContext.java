package it.neverworks.model.io;

public interface StateContext {
    boolean matches( String expression );
    State extract( Object value );
    State wrap( Object value );
    Object process( Object value );
    Object convert( Object value );
    <T> T getProperty( String name );
    void setProperty( String name, Object value );
    Object merge( State source, Object target );
}