package it.neverworks.model.io;

import java.util.Map;
import java.util.Set;

import it.neverworks.lang.MapLike;

public abstract class MapLikeState implements MapLike<String, Object>, State {

    protected StateContext context;

    public abstract Iterable<String> names();
    public abstract boolean has( Object key );
	public abstract Object get( Object key );

    public State sub( Object key ) {
        Object value = get( key );
        if( value instanceof State ) {
            return (State) value; 
        } else if( value == null ) {
            return EmptyState.INSTANCE;
        } else {
            throw new IllegalArgumentException( "Key does not contain a State: " + key );
        }
    }

	@Override
	public boolean isEmpty() {
		return names().iterator().hasNext();
	}

	@Override
	public Set<String> keySet() {
		return new IterableSet<String>( names() );
	}

	@Override
	public void putAll( Map<? extends String, ? extends Object> m ) {
        for( Map.Entry entry: m.entrySet() ) {
            this.put( entry.getKey().toString(), entry.getValue() );
        }
	}

	@Override
	public boolean containsKey( Object key ) {
		return has( key );
	}

}
