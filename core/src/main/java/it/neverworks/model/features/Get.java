package it.neverworks.model.features;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Wrapper;
import it.neverworks.model.Value;
import it.neverworks.model.Hook;
import it.neverworks.model.expressions.ExpressionEvaluator;

public interface Get {

    default <T> T get( String name ) {
        return ExpressionEvaluator.evaluate( this, name );
    }
   
    default <T> T get( String name, T defaultValue ) {
        T value = get( name );
        return value != null ? value : defaultValue;
    }
    
    default Value value( String name ) {
        return Value.of( get( name ) );
    }
    
    default Value hook( String name ) {
        return Hook.of( this, name );
    }

    default Value hook( Object root, String name ) {
        return Hook.of( root instanceof String ? hook( (String) root ) : root, name );
    }

    default Value hook( Wrapper wrapper) {
        return Hook.of( wrapper );
    }
    
    default Tuple tuple( String... names ) {
        Object[] values = new Object[ names.length ];
        for( int i = 0; i < names.length; i++ ) {
            values[ i ] = get( names[ i ] );
        }
        return Tuple.of( values );
    }
    
}


