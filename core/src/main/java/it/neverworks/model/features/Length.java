package it.neverworks.model.features;

public interface Length {
    int itemsCount();
}