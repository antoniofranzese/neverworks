package it.neverworks.model.features;

import it.neverworks.model.description.ModelCopier;
import it.neverworks.model.description.ModelInstance;

public interface Copy {
    default ModelCopier copyObject() {
        return new ModelCopier( ModelInstance.of( this ) );
    }
}