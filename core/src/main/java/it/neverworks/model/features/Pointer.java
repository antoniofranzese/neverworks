package it.neverworks.model.features;

public interface Pointer {
    
    public Object reference();
    public Object resolve();
    
    default boolean retained() {
        return true;
    }

    default Object resolve( Object requestor ) {
        if( requestor instanceof Resolver && ((Resolver) requestor).resolves( this ) ) {
            try {
                return ((Resolver) requestor).resolve( this );
            } catch( Exception ex ) {
                throw new UnresolvedPointerException( this, ex.getMessage(), ex );
            }
        } else {
            return this.resolve();
        }
    }

}