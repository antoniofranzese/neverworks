package it.neverworks.model.features;

public interface Retrieve {
    Object retrieveItem( Object key );
}