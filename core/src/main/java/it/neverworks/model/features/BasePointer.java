package it.neverworks.model.features;

import it.neverworks.model.utils.ToStringBuilder;

public class BasePointer implements Pointer {
    
    protected Object reference;
    protected Resolver resolver;

    public BasePointer( Object reference ) {
        this.reference = reference;
    }
    
    public BasePointer( Resolver resolver, Object reference ) {
        this.resolver = resolver;
        this.reference = reference;
    }
    
    public Object reference(){
        return this.reference;
    }
    
    public Object resolve() {
        try {
            if( this.resolver != null ) {
                return this.resolver.resolve( this );
            } else {
                throw new IllegalStateException( "Missing resolver" );
            }
        } catch( Exception ex ) {
            throw new UnresolvedPointerException( this, ex.getMessage(), ex );
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "reference", this.reference )
            .add( "!resolver", this.resolver )
            .toString();
    }

}