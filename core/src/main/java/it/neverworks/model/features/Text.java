package it.neverworks.model.features;
    
public interface Text {
    public String asText();
}