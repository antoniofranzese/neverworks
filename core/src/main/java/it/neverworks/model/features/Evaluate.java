package it.neverworks.model.features;

import it.neverworks.model.expressions.Expression;

public interface Evaluate {
    public Object evaluateExpression( Expression expression );
}