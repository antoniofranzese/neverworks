package it.neverworks.model.features;

import it.neverworks.model.expressions.ObjectQuery;

public interface Query {
    <T> ObjectQuery<T> queryItems();
}