package it.neverworks.model.features;

import java.util.Map;
import java.util.Enumeration;

import it.neverworks.lang.Collections;
import it.neverworks.lang.Functional;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.Value;

public interface Language extends Get, Describe {
    
    public static class FunctionalBuilder<T> extends Collections.FunctionalBuilder {

        private Get parent;
    
        public FunctionalBuilder( Class target, Get parent ) {
            super( target );
            this.parent = parent;
        }
    
        public Functional<T> in( String source ) {
            return build( this.parent.get( source ) );
        }
    }
 
    default <T> ObjectQuery<T> query( Iterable<T> iterable ) {
        return new ObjectQuery<T>( iterable );
    }

    default <T> ObjectQuery<T> query( T[] array ) {
        return new ObjectQuery<T>( array );
    }

    default <T> ObjectQuery<T> query( String name ) {
        return Value.queryOf( this.get( name ) );
    }

    default ObjectQuery query( Object source) {
        return Value.queryOf( source );
    }

    default <T> FunctionalBuilder<T> each( Class<T> target ) {
        return new FunctionalBuilder<T>( target, (Get) this );
    }
    
    default <T> Functional<T> each( String name ) {
        return Collections.<T>functional( get( name ) );
    }
    
    default <T> Functional<T> each( Iterable<T> source ) {
        return Collections.<T>functional( source );
    }

    default <T> Functional<T> each( Enumeration<T> source ) {
        return Collections.<T>functional( source );
    }

    default <T> Functional<T> each( T[] source ) {
        return Collections.<T>functional( source );
    }
    
    default <K,V> Functional<K> each( Map<K,V> source ) {
        return Collections.<K>functional( source );
    }

    default <T> T call( String name, Object... arguments ) {
        return this.model().<T>call( name, arguments );
    }
    
}
