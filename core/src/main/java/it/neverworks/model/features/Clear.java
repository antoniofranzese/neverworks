package it.neverworks.model.features;

public interface Clear {
    void clearItems();
}