package it.neverworks.model.features;

import it.neverworks.model.description.ModelCopier;
import it.neverworks.model.description.ModelInstance;

public interface Duplicate {
    default Object duplicateObject() {
        return new ModelCopier( ModelInstance.of( this ) ).to( this.getClass() );
    }
}