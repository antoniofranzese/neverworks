package it.neverworks.model.features;

import it.neverworks.lang.Wrapper;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Expression;

public interface Probe extends Describe {

    default <T> T probe( String name, Wrapper initializer ) {
        return this.model().probe( name, initializer );
    }
    
}