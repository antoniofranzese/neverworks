package it.neverworks.model.features;

public interface Insert {
    public void insertItem( Object where, Object item );
}