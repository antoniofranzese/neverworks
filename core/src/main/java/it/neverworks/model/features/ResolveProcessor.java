package it.neverworks.model.features;

import static it.neverworks.language.*;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.Getter;
import it.neverworks.model.description.Setter;
import it.neverworks.model.description.GetterInterceptor;
import it.neverworks.model.description.SetterInterceptor;
import it.neverworks.model.description.PropertyException;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeProxy;
import it.neverworks.model.ModelException;


public class ResolveProcessor implements PropertyProcessor, PropertyMetadata {
    
    private boolean strict = false;
    private boolean optional = false;
    private boolean retain = false;
    
    public ResolveProcessor( AnnotationInfo<Resolve> info ) {
        this.strict = info.annotation().strict();
        this.optional = info.annotation().optional();
        this.retain = info.annotation().retain();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }

    public void meetProperty( MetaPropertyManager property ) {
        if( property.getType().is( String.class ) && ! strict ) {
            throw new PropertyException( property, "String property needs strict pointers" );
        } else if( property.getStorageType().isPrimitive() ) {
            throw new PropertyException( property, "Resolve cannot be used with primitive types" );
        }
        
        property.placeGetter( new ResolveGetter() ).after();
        property.placeSetter( new ResolveSetter() ).before();
        property.setType( new ResolveType( property.getType() ) );
        
        if( ! strict ) {
            Converter converter = property.getConverter();

            if( converter instanceof InstanceConverter ) {
                property.setConverter( new ResolveInstanceConverter( converter ) );
            
            } else {
                property.setConverter( new ResolveConverter( converter ) );
            
            }
        }
    }    

    public void inheritProperty( MetaPropertyManager property ) {
        
    }    
    
    public class ResolveGetter extends GetterInterceptor {
    
        public Object get( Getter getter ) {
            
            if( getter.undefined() ) {
                
                ResolveModule module = getter.instance().module( ResolveModule.class );

                if( module.has( getter.name() ) ) {
                    getter.decline();
                    
                    Pointer pointer = module.get( getter.name() );
                    
                    try {

                        try {
                            Object value = getter.property().process( pointer.resolve( getter.actual() ) );

                            if( retain || pointer.retained() ) {
                                getter.setRaw( null );

                            } else {
                                getter.setRaw( value );
                                module.remove( getter.name() );
                            } 
                            
                            return value;

                        } catch( UnresolvedPointerException ex ) {
                            getter.setRaw( null );

                            if( optional ) {
                                return null;
                            } else {
                                throw ex;
                            }
                        }
                        
                    } catch( IllegalArgumentException ex ) {
                        throw new IllegalArgumentException( "Error resolving " + pointer + ": " + ex.getMessage(), ex );
                    }

                } else {
                    return getter.get();
                }

            } else {
                return getter.get();
            }

        }
    
    }

    public static class ResolveSetter extends SetterInterceptor {
    
        public void set( Setter setter ) {
            if( setter.value() instanceof Pointer ) {
                setter.instance().module( ResolveModule.class ).set( setter.name(), setter.value() );
                setter.set( null );
            } else {
                setter.instance().module( ResolveModule.class ).remove( setter.name() );
                setter.set();
            }
        }
    
    }
    
    public class ResolveType<T> extends TypeProxy<T> {
        
        private TypeDefinition<T> actual;
        
        public ResolveType( TypeDefinition<T> actual ) {
            this.actual = actual;
        }

        public TypeDefinition<T> actual() {
            return this.actual;
        }
        
        @Override
        public boolean isInstance( Object object ) {
            if( object instanceof Pointer ) {
                return true;
            } else {
                return actual.isInstance( object );
            }
        }
        
    }
    
    public class ResolveConverter implements Converter {
        
        protected Converter converter;
        
        public ResolveConverter( Converter converter ) {
            this.converter = converter;
        }
        
        public Object convert( Object value ) {
            return convertPointer( converter != null ? converter.convert( value ) : value );
        }
        
        protected Object convertPointer( Object value ) {
            if( value instanceof String ) {
                String s = (String) value;
                if( s.startsWith( "&" ) ) {
                    return new BasePointer( s.substring( 1 ) );
                } else {
                    return new DisposablePointer( s.startsWith( "*" ) ? s.substring( 1 ) : s );
                }
            } else {
                return value;
            }
        }
    }
    
    public class ResolveInstanceConverter extends ResolveConverter implements InstanceConverter {
        public ResolveInstanceConverter( Converter converter ) {
            super( converter );
        }
        
        public Object convert( ModelInstance instance, Object value ) {
            return convertPointer( converter != null ? ((InstanceConverter)converter).convert( instance, value ) : value );
        }
    }

}