package it.neverworks.model.features;

import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;

public interface ChildOf<T> extends ChildModel {
    
    default void adoptModel( ModelInstance model, PropertyDescriptor property ){
        this.storeParent( (T) model.actual() );
    }

    default void orphanModel( ModelInstance model, PropertyDescriptor property ){
        this.removeParent();
    }
    
    void storeParent( T parent );

    default void removeParent(){}

}