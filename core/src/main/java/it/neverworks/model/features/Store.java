package it.neverworks.model.features;

public interface Store {
    void storeItem( Object key, Object value );
}