package it.neverworks.model.features;

import it.neverworks.model.description.ModelInstance;

public interface Describe {
    ModelInstance model();
}