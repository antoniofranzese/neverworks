package it.neverworks.model.features;

public interface Placer {
    void placeFirst();
    void placeLast();
    void placeAt( int index );
    void placeBefore( Object item );
    void placeAfter( Object item );
}