package it.neverworks.model.features;

public interface Invoke {
    Object invokeInstance( Object... arguments );
}