package it.neverworks.model.features;

public interface Delete {
    void deleteItem( Object key );
}