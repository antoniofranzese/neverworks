package it.neverworks.model.features;

public interface Append {
    public void appendItem( Object item );
}