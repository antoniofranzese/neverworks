package it.neverworks.model.features;

import it.neverworks.model.expressions.Expression;

public interface Ensure {
    public Object ensureExpression( Expression expression );
}