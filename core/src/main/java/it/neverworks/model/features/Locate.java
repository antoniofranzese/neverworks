package it.neverworks.model.features;

public interface Locate {
    Object locateItem( Object item );
}