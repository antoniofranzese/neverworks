package it.neverworks.model.features;

public interface Inspect {
    boolean containsItem( Object key );
}