package it.neverworks.model.features;

public class DisposablePointer extends BasePointer {

    public DisposablePointer( Object reference ) {
        super( reference );
    }
    
    public DisposablePointer( Resolver resolver, Object reference ) {
        super( resolver, reference );
    }

    public boolean retained() {
        return false;
    }
}