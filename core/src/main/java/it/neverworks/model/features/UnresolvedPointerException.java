package it.neverworks.model.features;

import it.neverworks.model.ModelException;

public class UnresolvedPointerException extends ModelException {
    
    public UnresolvedPointerException( Pointer pointer, String message ) {
        super( "[" + pointer + "] " + message );
        this.pointer = pointer;
    }
    
    public UnresolvedPointerException( Pointer pointer, String message, Throwable cause ) {
        super( "[" + pointer + "] " + message, cause );
        this.pointer = pointer;
    }
    
    private Pointer pointer;
    
    public Pointer getPointer(){
        return this.pointer;
    }
    
}