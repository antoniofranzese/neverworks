package it.neverworks.model.features;

public interface Resolver {

    public boolean resolves( Pointer pointer );
    public <T> T resolve( Pointer pointer );
    
}


