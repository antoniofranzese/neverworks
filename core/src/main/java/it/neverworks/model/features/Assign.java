package it.neverworks.model.features;

import it.neverworks.model.expressions.Expression;

public interface Assign {
    public void assignExpression( Expression expression, Object value );
}