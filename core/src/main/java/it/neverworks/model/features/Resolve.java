package it.neverworks.model.features;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = ResolveProcessor.class )
public @interface Resolve {
    boolean optional() default false;
    boolean strict() default false;
    boolean retain() default false;
}
