package it.neverworks.model.converters;

import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.ModelException;

public interface InstanceConverter extends Converter {
    public Object convert( ModelInstance instance, Object value );
    
    default Object convert( Object value ) {
        throw new ModelException( this.getClass().getName() + " cannot be used as a standalone Converter" );
    }
}