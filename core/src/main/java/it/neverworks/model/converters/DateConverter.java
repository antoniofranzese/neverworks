package it.neverworks.model.converters;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Dates;
import it.neverworks.model.types.TypeAware;

public class DateConverter implements Converter, TypeAware {
    
    protected Class<?> targetType;
    
    public Object convert( Object value ) {
        if( this.targetType == null ? value instanceof java.util.Date : this.targetType.isInstance( value ) ) {
            return value;

        } else {
            try {
                return toTargetType( Dates.date( value ) );
            } catch( Exception ex ) {
                return value;
            }
        }
    }
    
    protected Object toTargetType( java.util.Date date ) {
        if( targetType == null || date == null ) {
            return date;
        } else if( targetType.equals( java.sql.Date.class ) ) {
            return new java.sql.Date( date.getTime() );
        } else if( targetType.equals( java.sql.Time.class ) ) {
            return new java.sql.Time( date.getTime() );
        } else if( targetType.equals( java.sql.Timestamp.class ) ) {
            return new java.sql.Timestamp( date.getTime() );
        } else {
            return date;
        }
    }
    
    public void setRelatedClass( Class<?> type ) {
        this.targetType = type;
    }
    
}