package it.neverworks.model.converters;

import java.util.regex.Pattern;

public class PatternConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Pattern ) {
            return value;

        } else if( value instanceof String ) {
            try {
                return Pattern.compile( (String)value );
            } catch( Exception ex ) {
                return value;
            }

        } else {
            return value;
        }

    }
    
    
}