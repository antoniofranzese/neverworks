package it.neverworks.model.converters;

import static it.neverworks.language.*;

import java.lang.reflect.Method;

import it.neverworks.lang.Reflection;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.types.TypeAware;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.ModelException;

public class AccessorConverter implements InstanceConverter, TypeAware {
    
    private Method accessor;
    private Class<?> type;
    private boolean isStatic;
    
    public AccessorConverter( Method accessor ) {
        this.accessor = accessor;
        this.isStatic = Reflection.isStatic( accessor );
    }
    
    public Object convert( Object value ) {
        if( this.type != null && this.type.isInstance( value ) ) {
            return value;
        } else if( this.accessor != null ) {
            if( this.isStatic ) {
                return Reflection.invokeMethod( null, this.accessor, value );
            } else {
                throw new ModelException( msg( "Non-static {0.declaringClass.name}.{0.name} method cannot be used as a standalone converter", this.accessor ) );
            }
        } else {
            return value;
        }
    }

    public Object convert( ModelInstance instance, Object value ) {
        if( this.type != null && this.type.isInstance( value ) ) {
            return value;
        } else if( this.accessor != null ) {
            return Reflection.invokeMethod( instance.actual(), this.accessor, value );
        } else {
            return value;
        }
    }
    
    public void setRelatedClass( Class<?> type ) {
        if( ! Object.class.equals( type ) ) {
            this.type = type;
        }
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "!target", this.type != null ? this.type.getName() : null )
            .add( "!method", this.accessor != null ? ( this.accessor.getDeclaringClass().getName() + "." + this.accessor.getName() ) : null )
            .toString();
    }
}