package it.neverworks.model.converters;

import static it.neverworks.language.*;

import java.io.Reader;
import java.io.InputStream;
import it.neverworks.lang.Strings;
import it.neverworks.io.Streams;

public class StringConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof String ) {
            return value;

        } else if( value instanceof InputStream ) {
            return Streams.asString( (InputStream) value );
            
        } else if( value instanceof Reader ) {
            return Streams.asString( (Reader) value );

        } else if( value == null ) {
            return null;

        } else {
            return Strings.valueOf( value );
        }
    }

}