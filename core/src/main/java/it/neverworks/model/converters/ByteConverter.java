package it.neverworks.model.converters;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class ByteConverter extends PrimitiveConverter {
    
    private final static Byte ZERO = Numbers.Byte( 0 );
    
    public Object convert( Object value ) {
        if( value instanceof Byte ) {
            return value;

        } else if( value instanceof Number ) {
            return ((Number) value).byteValue();

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Byte.parseByte( ((String) value).trim() );
                } catch( NumberFormatException ex ) {
                    return checkPrimitive( value, ZERO );
                }
            } else {
                return checkPrimitive( null, ZERO );
            }
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}