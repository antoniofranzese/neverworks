package it.neverworks.model.converters;

import it.neverworks.lang.Booleans;

public class BooleanConverter extends PrimitiveConverter {
    
    public Object convert( Object value ) {
        try {
            return Booleans.bool( value );
        } catch( Exception ex ) {
            return checkPrimitive( value, false );
        }
    }
    
}