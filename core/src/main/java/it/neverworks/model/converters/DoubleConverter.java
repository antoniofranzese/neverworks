package it.neverworks.model.converters;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class DoubleConverter extends PrimitiveConverter {
    
    private final static Double ZERO = Numbers.Double( 0 );

    public Object convert( Object value ) {
        if( value instanceof Double ) {
            return value;

        } else if( value instanceof Number ) {
            return ((Number) value).doubleValue();

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Double.parseDouble( ((String) value).trim() );
                } catch( NumberFormatException ex ) {
                    return checkPrimitive( value, ZERO );
                }
            } else {
                return checkPrimitive( null, ZERO );
            }
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}