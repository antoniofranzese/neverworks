package it.neverworks.model.converters;

import java.util.Map;
import java.util.HashMap;
import java.lang.reflect.Field;
import it.neverworks.lang.Errors;
import it.neverworks.model.types.TypeAware;

public class EnumConverter implements Converter, TypeAware {
    
    Class<? extends Enum> enumType;
    Map fields = new HashMap();
    Map uppercaseFields = new HashMap();
    
    public EnumConverter() {
        super();
    }
    
    public EnumConverter( Class type ) {
        super();
        setRelatedClass( type );
    }
    
    public void setRelatedClass( Class type ) {
        // Narrow
        enumType = type.asSubclass( Enum.class );
        for( Field field: type.getDeclaredFields() ) {
            if( field.getType().equals( type ) ) {
                fields.put( field.getName(), field );
                uppercaseFields.put( field.getName().toUpperCase(), field );
            }
        }
    }
    
    public Object convert( Object value ) {
        if( value != null ) {
            if( value instanceof String ) {
                // Controlla prima il match esatto
                if( fields.containsKey( value ) ) {
                    try {
                        return ((Field)fields.get( value )).get( enumType );
                    } catch( Exception ex ) {
                        Errors.lower( ex );
                    }
                // Controlla ignorando il case    
                } else if( uppercaseFields.containsKey( ((String) value).toUpperCase() ) ) {
                    try {
                        return ((Field)uppercaseFields.get( ((String) value).toUpperCase() )).get( enumType );
                        
                    } catch( Exception ex ) {
                        Errors.lower( ex );
                    }
                }
            }
        }
        return value;
    }
    
}