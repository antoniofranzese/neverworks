package it.neverworks.model.converters;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = LocalConvertProcessor.class )
public @interface LocalConvert {
    String value() default "";
}
