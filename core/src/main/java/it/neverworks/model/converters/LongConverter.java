package it.neverworks.model.converters;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class LongConverter extends PrimitiveConverter {
    
    private final static Long ZERO = Numbers.Long( 0 );
    
    public Object convert( Object value ) {
        if( value instanceof Long ) {
            return value;

        } else if( value instanceof Number ) {
            return ((Number) value).longValue();

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Long.parseLong( ((String) value).trim() );
                } catch( NumberFormatException ex ) {
                    return checkPrimitive( value, ZERO );
                }
            } else {
                return checkPrimitive( null, ZERO );
            }
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}