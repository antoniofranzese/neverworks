package it.neverworks.model.converters;

import static it.neverworks.language.*;

import java.io.InputStream;
import java.util.Properties;
import java.util.Map;
import java.util.LinkedHashMap;
import java.lang.reflect.Type;

import it.neverworks.model.Model;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.description.ClassicPropertyManager;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.PropertyManager;
import it.neverworks.model.description.ClassicPropertyProcessor;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.lang.Application;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;

@SuppressWarnings( "unchecked" )
public class AutoConvertProcessor implements PropertyProcessor, ClassicPropertyProcessor {
    
    protected static Map<Class,ConverterDefinition> converters = new LinkedHashMap<Class,ConverterDefinition>();
    
    protected static class ConverterDefinition {
        private Class[] converter;
        private Arguments setup;
        
        public ConverterDefinition( Class converter ) {
            this.converter = new Class[]{ converter };
        }
        
        public ConverterDefinition( Convert annotation ) {
            this.converter = annotation.value();
            this.setup = Arguments.parse( annotation.setup() ); 
        }
        
        public ConverterDefinition( ConverterDefinition other ) {
            this.converter = other.converter;
            this.setup = other.setup;
        }
    }
    
    static {

        for( String source: list( "META-INF/application/converters.properties", "META-INF/neverworks/converters.properties" ) ) {
            Arguments args = Arguments.load( Application.resource( source ) );
            for( String name: args.keys() ) {
                Class convertible = Reflection.findClass( name );
            
                if( convertible != null && ! converters.containsKey( convertible ) ) {
                    Class converter = Reflection.findClass( args.get( name ) );
                    if( converter != null ) {
                        converters.put( convertible, new ConverterDefinition( converter ) );
                    }
                }
            }
            
        }

        if( ! converters.containsKey( String.class ) )    converters.put( String.class,    new ConverterDefinition( StringConverter.class ) );
        if( ! converters.containsKey( Byte.class ) )      converters.put( Byte.class,      new ConverterDefinition( ByteConverter.class ) );
        if( ! converters.containsKey( Short.class ) )     converters.put( Short.class,     new ConverterDefinition( ShortConverter.class ) );
        if( ! converters.containsKey( Integer.class ) )   converters.put( Integer.class,   new ConverterDefinition( IntConverter.class ) );
        if( ! converters.containsKey( Long.class ) )      converters.put( Long.class,      new ConverterDefinition( LongConverter.class ) );
        if( ! converters.containsKey( Float.class ) )     converters.put( Float.class,     new ConverterDefinition( FloatConverter.class ) );
        if( ! converters.containsKey( Double.class ) )    converters.put( Double.class,    new ConverterDefinition( DoubleConverter.class ) );
        if( ! converters.containsKey( Boolean.class ) )   converters.put( Boolean.class,   new ConverterDefinition( BooleanConverter.class ) );
        if( ! converters.containsKey( Enum.class ) )      converters.put( Enum.class,      new ConverterDefinition( EnumConverter.class ) );
        if( ! converters.containsKey( Modelized.class ) ) converters.put( Modelized.class, new ConverterDefinition( ModelConverter.class ) );
        if( ! converters.containsKey( Class.class ) )     converters.put( Class.class,     new ConverterDefinition( ClassConverter.class ) );

        converters.put( byte.class,    converters.get( Byte.class ) );
        converters.put( short.class,   converters.get( Short.class ) );
        converters.put( int.class,     converters.get( Integer.class ) );
        converters.put( long.class,    converters.get( Long.class ) );
        converters.put( float.class,   converters.get( Float.class ) );
        converters.put( double.class,  converters.get( Double.class ) );
        converters.put( boolean.class, converters.get( Boolean.class ) );
    } 

    protected String setup;
    
    protected AutoConvertProcessor( String setup ) {
        this.setup = setup;
    }
    
    public AutoConvertProcessor( AnnotationInfo<AutoConvert> info ) {
        this( info.getAnnotation().setup() );
    }

    public void processProperty( ClassicPropertyManager property ) {
        this.processConverter( property, property.getPropertyDescriptor().getPropertyType() );
    }
    
    public void processProperty( MetaPropertyManager property ) {
        this.processConverter( property, property.getField().getType() );
    }
    
    protected void processConverter( PropertyManager property, Class<?> type ) {
        ConvertProcessor.assign( property, inferConverter( type, this.setup ), type );
    }
    
    public static Converter inferConverter( TypeDefinition type ) {
        return inferConverter( type.getRawType() );
    }    

    public static Converter inferConverter( Type type ) {
        AutoConvert ann = Annotations.get( type, AutoConvert.class );
        return inferConverter( type, ann != null ? ann.setup() : null );
    }
    
    protected static Converter inferConverter( Type type, String setup ) {
        ConverterDefinition def = null;
        
        if( Annotations.has( type, Convert.class ) ) {
            Convert ann = Annotations.get( type, Convert.class );
            def = new ConverterDefinition( ann );
        } 
        
        Class raw = Reflection.getRawClass( type );

        if( def == null && converters.containsKey( raw ) ) {
            def = converters.get( raw );
        }
        
        if( def == null ) {
            for( Class cls: converters.keySet() ) {
                if( cls.isAssignableFrom( raw ) ) {
                    def = new ConverterDefinition( converters.get( cls ) );
                    break;
                }
            }
        }
        
        if( def == null ) {
            def = new ConverterDefinition( PassThroughConverter.class );
        }

        if( ! converters.containsKey( raw ) ) {
            converters.put( raw, def );
        }

        return ConvertProcessor.process( def.converter, Strings.hasText( setup ) ? Arguments.parse( setup ) : def.setup, type );
    }

}