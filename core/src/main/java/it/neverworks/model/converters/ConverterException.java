package it.neverworks.model.converters;

import it.neverworks.model.ModelException;

public class ConverterException extends ModelException {
    
    public ConverterException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public ConverterException( String message ){
        super( message );
    }
}