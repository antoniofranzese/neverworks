package it.neverworks.model.converters;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class FloatConverter extends PrimitiveConverter {
    
    private final static Float ZERO = Numbers.Float( 0 );

    public Object convert( Object value ) {
        if( value instanceof Float ) {
            return value;

        } else if( value instanceof Number ) {
            return ((Number) value).floatValue();

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Float.parseFloat( ((String) value).trim() );
                } catch( NumberFormatException ex ) {
                    return checkPrimitive( value, ZERO );
                }
            } else {
                return checkPrimitive( null, ZERO );
            }
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}