package it.neverworks.model.converters;

import it.neverworks.lang.Strings;

public class CharConverter extends PrimitiveConverter {
    
    private final static Character ZERO = Character.toChars( 0 )[ 0 ];
    
    public Object convert( Object value ) {
        if( value instanceof Character ) {
            return value;

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                return ((String) value ).charAt( 0 );
            } else {
                return checkPrimitive( null, ZERO );
            }

        } else if( value instanceof Number ) {
            return Character.toChars( ((Number) value).intValue() )[ 0 ];
            
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}