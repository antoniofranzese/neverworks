package it.neverworks.model.converters;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.TYPE, ElementType.METHOD })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = AutoConvertProcessor.class )
public @interface AutoConvert {
    String setup() default "";
}
