package it.neverworks.model.converters;

import it.neverworks.lang.Objects;

public class PassThroughConverter implements Converter {
    
    public Object convert( Object value ) {
        return value;
    }
    
}