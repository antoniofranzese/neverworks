package it.neverworks.model.converters;

import it.neverworks.lang.Strings;

public class EmptyStringConverter extends StringConverter {
    
    public Object convert( Object value ) {
        String converted = (String) super.convert( value );
        if( Strings.hasText( converted ) ) {
            return converted;
        } else {
            return null;
        }
    }
}