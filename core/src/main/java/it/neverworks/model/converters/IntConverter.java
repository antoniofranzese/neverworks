package it.neverworks.model.converters;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class IntConverter extends PrimitiveConverter {
    
    private final static Integer ZERO = Numbers.Int( 0 );

    public Object convert( Object value ) {
        if( value instanceof Integer ) {
            return value;

        } else if( value instanceof Number ) {
            return ((Number) value).intValue();

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Integer.parseInt( ((String) value).trim() );
                } catch( NumberFormatException ex ) {
                    return checkPrimitive( value, ZERO );
                }
            } else {
                return checkPrimitive( null, ZERO );
            }
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}