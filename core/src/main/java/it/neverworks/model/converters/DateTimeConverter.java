package it.neverworks.model.converters;

import org.joda.time.DateTime;
import it.neverworks.lang.Dates;
import it.neverworks.lang.Objects;

public class DateTimeConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof DateTime ) {
            return value;

        } else {
            try {
                return Dates.datetime( value );
            } catch( Exception ex ) {
                return value;
            }
        }

    }

}