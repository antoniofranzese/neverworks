package it.neverworks.model.converters;

import java.lang.reflect.Type;

import it.neverworks.lang.Reflection;
import it.neverworks.model.ModelException;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.types.GenericTypeAware;

public class AutoConverter implements Converter, GenericTypeAware, PropertyAware {
    
    private Converter converter;
    private Type type;
    private PropertyDescriptor property;

    public Object convert( Object value ) {
        return getConverter().convert( value );
    }

    protected Converter getConverter() {
        if( this.converter == null ) {
            Type type = this.type;
            
            if( type == null && this.property != null ) {
                this.type = property.getStorageType();
            }
            
            if( type != null ) {
                this.converter = AutoConvertProcessor.inferConverter( type );
            } else {
                throw new ModelException( "Cannot infer auto converter" );
            }
        }

        return this.converter;
    }

    public void setPropertyDescriptor( PropertyDescriptor property ) {
        this.property = property;
    }

    public void setRelatedType( Type type ) {
        this.type = type;
    }

}