package it.neverworks.model.converters;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

import it.neverworks.lang.Range;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.description.ClassicPropertyProcessor;
import it.neverworks.model.description.ClassicPropertyManager;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyManager;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.types.TypeAware;
import it.neverworks.model.ModelException;
import it.neverworks.model.Value;

public class ConvertProcessor implements PropertyProcessor, ClassicPropertyProcessor {
    
    Convert annotation;
    Object target;
    
    public ConvertProcessor( AnnotationInfo<Convert> info ) {
        this.annotation = info.getAnnotation();
        this.target = info.getTarget();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        Class targetType = null;
        if( target instanceof Class ) {
            targetType = (Class) target;
        } else if( target instanceof Field ) {
            targetType = ((Field) target).getType();
        }

        processPropertyConverter( property, targetType );
    }
    
    public void processProperty( ClassicPropertyManager property ) {
        processPropertyConverter( property, property.getPropertyDescriptor().getPropertyType() );
    }
        
    protected void processPropertyConverter( PropertyManager property, Class targetType ) {
        assign( property, process( annotation, targetType ), targetType );
    }
    
    public static void assign( PropertyManager property, Converter converter, Class targetType ) {
        if( converter != null ) {

            if( converter instanceof PropertyAware ) {
                ((PropertyAware)converter).setPropertyDescriptor( property );
            }

            if( property.getConverter() == null ) {
                property.setConverter( converter );
            } else {
                if( property.getConverter() instanceof ChainedConverter ) {
                    ((ChainedConverter) property.getConverter()).append( converter );
                } else {
                    ChainedConverter chained = new ChainedConverter()
                        .append( property.getConverter() )
                        .append( converter );
                    
                    if( targetType != null ) {
                        TypeAware.set( chained, targetType );
                    }
                    
                    property.setConverter( chained );
                }
            }
        }
    }

    public static Converter process( Type targetType ) {
        if( Annotations.has( targetType, Convert.class ) ) {
            return process( Annotations.get( targetType, Convert.class ), targetType );
        } else {
            return null;
        }
    }

    public static Converter process( Convert annotation, Type targetType ) {
        return process( annotation.value(), Arguments.parse( annotation.setup() ), targetType );
    }

    public static Converter process( Class converterType, Arguments arguments, Type targetType ) {
        return process( new Class[]{ converterType }, arguments, targetType );
    }
    
    public static Converter process( Class[] converterTypes, Arguments arguments, Type targetType ) {
        Converter[] converters = new Converter[ converterTypes.length ];
        
        for( int i: Range.of( converterTypes ) ) {
            Converter converter = null;
        
            if( converterTypes[ i ] != null ) {
                converter = (Converter)Reflection.newInstance( converterTypes[ i ] );
            }

            converters[ i ] = process( converter, arguments, targetType );
            
        }
        
        if( converters.length == 0 ) {
            return null;
        } else if( converters.length == 1 ) {
            return converters[ 0 ];
        } else {
            return new ChainedConverter( converters );
        }
    }

    public static Converter process( Converter converter, Arguments arguments, Type targetType ) {
        
        if( converter != null  ) {

            if( converter instanceof TypeAware ) {
                if( targetType != null ) {
                    TypeAware.set( (TypeAware) converter, targetType );
                 } else {
                     throw new ModelException( "Missing converter target type for: " + targetType );
                }
            }

            if( arguments != null ) {
                if( converter instanceof Modelized ) {
                    ModelInstance instance = ModelInstance.of( converter );
                    for( String key: arguments.keys() ) {
                        instance.safeAssign( key, arguments.get( key ) );
                    }
                } else {
                    for( String key: arguments.keys() ) {
                        Properties.set( converter, key, arguments.get( key ) );
                    }
                }
            }
            
        }

        return converter;
    }
    
}