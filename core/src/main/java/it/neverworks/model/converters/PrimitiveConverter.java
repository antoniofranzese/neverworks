package it.neverworks.model.converters;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Booleans;
import it.neverworks.model.types.TypeAware;

public abstract class PrimitiveConverter implements Converter, TypeAware {
    
    protected Class<?> type;

    protected Object checkPrimitive( Object value, Object _default ) {
        if( value == null && this.type != null && this.type.isPrimitive() ) {
            return _default;
        } else {
            return value;
        }
    }

    public void setRelatedClass( Class<?> type ) {
        this.type = type;
    }
    
    public abstract Object convert( Object value );
    
}