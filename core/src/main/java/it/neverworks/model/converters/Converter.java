package it.neverworks.model.converters;

public interface Converter {
    public Object convert( Object value );
}