package it.neverworks.model.converters;

public interface Reshaper {
    public Object reshape( Object value, Class<?> target );
}