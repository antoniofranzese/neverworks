package it.neverworks.model.converters;

import java.util.Map;
import java.io.InputStream;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;

public class ArgumentsConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Arguments ) {
            return value;
        } else if( value instanceof String ) {
            return Arguments.parse( (String) value );
        } else if( value instanceof Map ) {
            return new Arguments( (Map) value );
        } else if( value instanceof Iterable ) {
            return new Arguments( (Iterable) value );
        } else if( value instanceof InputStream ) {
            return Arguments.load( (InputStream) value );
         } else if( Collections.isListable( value ) ) {
            return new Arguments( Collections.list( value ) );
        } else {
            return value;
        }
    }

    
}