package it.neverworks.model.converters;

import static it.neverworks.language.*;
import java.util.Map;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;

public class ClassConverter implements Converter {
    
    private final static Map<String, Class> PRIMITIVES = dict(
        pair( "byte", byte.class )
        ,pair( "char", char.class )
        ,pair( "int", int.class )
        ,pair( "long", long.class )
        ,pair( "short", short.class )
        ,pair( "float", float.class )
        ,pair( "double", double.class )
    );

    public Object convert( Object value ) {
        if( value instanceof Class ) {
            return value;

        } else if( value instanceof String ) {
            try {
                return Class.forName( (String) value );                
            } catch( ClassNotFoundException ex ) {
                if( PRIMITIVES.containsKey( (String) value ) ) {
                    return PRIMITIVES.get( (String) value );
                } else {
                    return value;
                }
            }

        } else {
            return value;
        }

    }
    
}