package it.neverworks.model.converters;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;

public class NumberConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Number ) {
            return value;

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Numbers.number( value );
                } catch( Exception ex ) {
                    return value;
                }
            } else {
                return null;
            }
            
        } else {
            return value;
        }

    }

}