package it.neverworks.model.converters;

import java.io.InputStream;
import java.io.Reader;
import java.util.Map;
import java.util.Collection;
import it.neverworks.lang.Booleans;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.types.TypeAware;
import it.neverworks.model.Model;

public class ModelConverter implements Converter, TypeAware {
    
    protected Class type = Model.class;
    protected ModelDescriptor descriptor;
    protected boolean lenient = false;
    
    public ModelConverter() {
        super();
    }
    
    public ModelConverter( Class type ) {
        super();
        setRelatedClass( type );
    }
    
    public ModelConverter( Arguments arguments ) {
        super();
        this.lenient = arguments.get( Boolean.class, "lenient", false );
        this.type = arguments.get( Class.class, "type" );
    }
    
    public void setLenient( Object value ) {
        this.lenient = Booleans.bool( value );
    }
    
    public void setRelatedClass( Class<?> type ) {
        this.type = type;
    }
    
    public Object convert( Object value ) {
        if( value != null && type != null ) {
            if( type.isAssignableFrom( value.getClass() ) ) {
                return value;
            
            } else if( ModelInstance.supports( value ) && ! ( value instanceof ExpandoModel ) ) {
                try {
                    if( type.equals( Model.class ) ) {
                        return Model.of( value );
                    } else {
                        ModelInstance instance = ModelInstance.of( Reflection.newInstance( type ) );
                        instance.copy().from( ModelInstance.of( value ) );
                        return instance.actual();
                    }
                } catch( Exception ex ) {
                    return value;
                }

            } else if( value instanceof Map ) {
                if( descriptor == null ) {
                    descriptor = ModelDescriptor.of( type );
                }
            
                Object model = Reflection.newInstance( descriptor.target() );
                ModelInstance instance = ModelInstance.of( model );
            
                // Conserva l'ordine di impostazione, se possibile
                Iterable<String> keys = value instanceof Arguments ? ((Arguments) value).keys() : ((Map<String, Object>) value).keySet();
                for( String key: keys ) {
                    if( descriptor.has( key ) ) {
                        instance.checked( key, ((Map) value).get( key ) );
                    } else if( ExpressionEvaluator.isExpression( key ) ) {
                        instance.safeAssign( key, ((Map) value).get( key ) );
                    } else if( !lenient ){
                        throw new ConverterException( "Cannot convert to model, missing '" + key + "' property in " + descriptor.targetName() );
                    }
                }
                return model;
                
            } else if( Arguments.isParsable( value ) ) {
                return convert( Arguments.parse( value ) );
        
            } else if( value instanceof InputStream ) {
                return convert( Arguments.load( (InputStream) value ) );
    
            } else if( value instanceof Reader ) {
                return convert( Arguments.load( (Reader) value ) );

            } else {
                return value;
            }

        } else {
            return value;
        }
        
    }

}