package it.neverworks.model.converters;

import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class ShortConverter extends PrimitiveConverter {
    
    private final static Short ZERO = Numbers.Short( 0 );
    
    public Object convert( Object value ) {
        if( value instanceof Short ) {
            return value;

        } else if( value instanceof Number ) {
            return ((Number) value).shortValue();

        } else if( value instanceof String ) {
            if( Strings.hasText( ((String) value ) ) ) {
                try {
                    return Short.parseShort( ((String) value).trim() );
                } catch( NumberFormatException ex ) {
                    return checkPrimitive( value, ZERO );
                }
            } else {
                return checkPrimitive( null, ZERO );
            }
        } else {
            return checkPrimitive( value, ZERO );
        }

    }
    
}