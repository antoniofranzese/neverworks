package it.neverworks.model.converters;

import java.util.Locale;
import java.util.Set;
import java.util.HashSet;
import java.util.Arrays;

import it.neverworks.lang.Properties;
import it.neverworks.lang.MissingPropertyException;

public class LocaleConverter implements Converter {
    
    private static final Set<String> ISO_LANGUAGES = new HashSet<String>( Arrays.asList( Locale.getISOLanguages() ) );
    private static final Set<String> ISO_COUNTRIES = new HashSet<String>( Arrays.asList( Locale.getISOCountries() ) );

    public Object convert( Object value ) {
        if( value instanceof Locale ) {
            return value;
        } else if( value instanceof String ) {
            String s = ((String) value).trim();
            if( s.length() == 2 ) {
                if( ISO_LANGUAGES.contains( s ) ) {
                    return new Locale.Builder().setLanguage( s ).build();
                } else if( ISO_COUNTRIES.contains( s ) ) {
                    return new Locale.Builder().setRegion( s ).build();
                } else {
                    return s;
                }
            } else if( s.length() == 5 && ( s.charAt( 2 ) == '-' || s.charAt( 2 ) == '_' || s.charAt( 2 ) == ' ' ) ) {
                String language = s.substring( 0, 2 );
                String country = s.substring( 3, 5 ).toUpperCase();
                if( ISO_LANGUAGES.contains( language ) && ISO_COUNTRIES.contains( country ) ) {
                    return new Locale.Builder().setLanguage( language ).setRegion( country ).build();                    
                } else {
                    return s;
                }
            } else {
                try {
                    return Properties.get( Locale.class, s.toUpperCase() );
                } catch( MissingPropertyException ex ) {
                    return s;
                }
            }
        } else {
            return value;
        }
    }
}