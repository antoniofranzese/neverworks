package it.neverworks.model.converters;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.description.ClassicPropertyProcessor;
import it.neverworks.model.description.ClassicPropertyManager;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyManager;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.types.TypeAware;
import it.neverworks.model.ModelException;
import it.neverworks.model.Value;

public class LocalConvertProcessor implements PropertyProcessor, ClassicPropertyProcessor {
    
    LocalConvert annotation;
    Object target;
    
    public LocalConvertProcessor( AnnotationInfo<LocalConvert> info ) {
        this.annotation = info.getAnnotation();
        this.target = info.getTarget();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        Class targetType = null;
        if( target instanceof Field ) {
            targetType = ((Field) target).getType();
        }

        processPropertyConverter( property, targetType );
    }
    
    public void processProperty( ClassicPropertyManager property ) {
        processPropertyConverter( property, property.getPropertyDescriptor().getPropertyType() );
    }
        
    protected void processPropertyConverter( PropertyManager property, Class targetType ) {
        Class target = property.getOwner().getTarget();
        String accessorName = Strings.hasText( this.annotation.value() ) ? this.annotation.value().trim() : createAccessorName( property );

        Method accessorMethod = Reflection.findMethod( target, accessorName, Object.class );

        if( accessorMethod != null && ! accessorMethod.getReturnType().equals( void.class ) ) {
            AccessorConverter converter = new AccessorConverter( accessorMethod );
            TypeAware.set( converter, targetType );
            ConvertProcessor.assign( property, converter, targetType );
        } else {
            throw new ModelException( "Missing or wrong local converter method '" + accessorName + "' for " + property.getFullName() + " property" );
        }
    }
    
    protected String createAccessorName( PropertyManager property ) {
        String name = property.getName();
        return "convert" + name.substring( 0, 1 ).toUpperCase() + name.substring( 1 );
    }
    
}