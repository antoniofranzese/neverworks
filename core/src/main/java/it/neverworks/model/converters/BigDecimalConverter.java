package it.neverworks.model.converters;

import java.math.BigDecimal;
import it.neverworks.lang.Numbers;

public class BigDecimalConverter implements Converter {
    
    public Object convert( Object value ) {
        try {
            return Numbers.decimal( value );
        } catch( Exception ex ) {
            return value;
        }
    }
    
    public final static BigDecimalConverter INSTANCE = new BigDecimalConverter();
    
}