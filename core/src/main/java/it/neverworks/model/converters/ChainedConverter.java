package it.neverworks.model.converters;

import it.neverworks.lang.Java;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.types.TypeAware;

public class ChainedConverter implements InstanceConverter, TypeAware {
    
    private FluentList<Converter> chain;
    private Class<?> type;
    
    public ChainedConverter() {
        this.chain = new FluentArrayList<Converter>();
    }
    
    public ChainedConverter( Converter... chain ) {
        this.chain = new FluentArrayList<Converter>().cat( chain );
    }

    public ChainedConverter( Iterable<Converter> chain ) {
        this.chain = new FluentArrayList<Converter>().cat( chain );
    }
    
    @Override
    public Object convert( Object value ) {
        if( this.type != null && this.type.isInstance( value ) ) {
            return value;
        } else {
            Object current = value;
            for( Converter converter: this.chain ) {
                if( converter != null ) {
                    current = converter.convert( current );
                }
                
                if( this.type != null && this.type.isInstance( current ) ) {
                    break;
                }
                
            }
            return current;
        }
    }
    
    @Override
    public Object convert( ModelInstance instance, Object value ) {

        if( this.type != null && this.type.isInstance( value ) ) {
            return value;

        } else {
            Object current = value;

            for( Converter converter: this.chain ) {
                if( converter != null ) {
                    if( converter instanceof InstanceConverter ) {
                        current = ((InstanceConverter) converter).convert( instance, current );
                    } else {
                        current = converter.convert( current );
                    }

                    if( this.type != null && this.type.isInstance( current ) ) {
                        break;
                    }
                
                }
            }
            return current;
        }
    }
    
    public ChainedConverter append( Converter converter ) {
        if( converter instanceof ChainedConverter ) {
            for( Converter c: ((ChainedConverter) converter).chain ) {
                this.chain.append( c );
            }
        } else {
            this.chain.append( converter );
        }
        return this;
    }
    
    public void setRelatedClass( Class<?> type ) {
        this.type = Java.getReferenceType( type );
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "!target", this.type != null ? this.type.getName() : null )
            .add( "*chain", this.chain )
            .toString();
    }
    
}