package it.neverworks.model;

import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.expressions.ExpressionEvaluator;

public class Extra {
    
    public static <T> ObjectQuery<T> query( Iterable<T> iterable ) {
        return new ObjectQuery<T>( iterable );
    }
    
    public static Object eval( Object root, String expression ) {
        return ExpressionEvaluator.evaluate( root, expression );
    }

    public static void assign( Object root, String expression, Object value ) {
        ExpressionEvaluator.valorize( root, expression, value );
    }

}