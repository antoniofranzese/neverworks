package it.neverworks.model;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.Collection;
import java.util.Iterator;
import java.util.Stack;
import java.util.Queue;
import java.util.Date;
import java.util.regex.Pattern;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.lang.reflect.Method;
import java.lang.reflect.Array;

import org.joda.time.DateTime;

import it.neverworks.language;
import it.neverworks.lang.Dates;
import it.neverworks.lang.Range;
import it.neverworks.lang.Guard;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Callable;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Undefined;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Calculator;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Filters;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Functional;
import it.neverworks.lang.FunctionalIterable;

import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.IntConverter;
import it.neverworks.model.converters.LongConverter;
import it.neverworks.model.converters.BooleanConverter;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Length;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Delete;
import it.neverworks.model.features.Append;
import it.neverworks.model.features.Clear;
import it.neverworks.model.features.Invoke;
import it.neverworks.model.features.Query;
import it.neverworks.model.features.Locate;
import it.neverworks.model.features.Insert;
import it.neverworks.model.features.Place;
import it.neverworks.model.features.Placer;
import it.neverworks.model.features.Text;
import it.neverworks.model.description.InstanceMethod;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import static it.neverworks.model.expressions.ExpressionEvaluator.isExpression;
import static it.neverworks.model.expressions.ExpressionEvaluator.evaluate;
import static it.neverworks.model.expressions.ExpressionEvaluator.valorize;

@SuppressWarnings( "unchecked" )
public class Value implements Retrieve, Store, Length, Inspect, Append, Delete, Invoke, Wrapper, Locate, Insert, Place, Text, Guard<Value> {

    protected static class WrappedValue extends Value {
        private Wrapper wrapper;
        
        public WrappedValue( Wrapper wrapper ) {
            this.wrapper = wrapper;
        }
        
        public Object asObject() {
            return this.wrapper.asObject();
        }
    }

    public static Value of( Object object ) {
        if( object instanceof Value ) {
            return (Value)object; 
        } else if( object instanceof Wrapper ) {
            return new WrappedValue( (Wrapper)object );
        } else {
            return new Value( object );
        }
    }
    
    public static Object from( Object object ) {
        return object instanceof Wrapper ? ((Wrapper) object).asObject() : object;
    }

    public static Iterable from( Iterable iterable ) {
        if( iterable != null ) {
            ArrayList result = new ArrayList();
            for( Object object: iterable ) {
                result.add( Value.from( object ) );
            }
            return result;
        } else {
            return null;
        }
    }

    public static Object[] from( Object[] array ) {
        if( array != null ) {
            Object[] result = new Object[ array.length ];
            for( int i = 0; i < array.length; i++  ) {
                result[ i ] = Value.from( array[ i ] );
            }
            return result;
        } else {
            return null;
        }
    }

    public static Arguments from( Arguments arguments ) {
        if( arguments != null ) {
            Arguments result = new Arguments();
            for( String name: arguments.names() ) {
                result.arg( name, Value.from( arguments.<Object>get( name ) ) );
            }
            return result;
        } else {
            return null;
        }
    }
    
    public static Map from( Map map ) {
        if( map != null ) {
            Map result = new HashMap();
            for( Object key: map.keySet() ) {
                result.put( Value.from( key ), Value.from( map.get( key ) ) );
            }
            return result;
        } else {
            return null;
        }
    }
    

    private Object wrapped;
    
    public Value() {
        this.wrapped = null;
    }

    protected Value( Object wrapped ) {
        this.wrapped = wrapped;
    }
    
    protected void setObject( Object newValue ) {
        this.wrapped = newValue;
    }

    public <T> T as( Class<T> instanceClass ) {
        try {
            return (T)this.asObject();
        } catch( ClassCastException ex ) {
            throw new ValueException( "Cannot cast to " + instanceClass.getName() + ": " + Objects.repr( this.asObject() ), ex );
        }
    }
    
    public Object actual() {
        return this.asObject();
    }
    
    public Object asObject() {
        return this.wrapped;
    }

    public String asString() {
        return (String)this.asObject();
    }
    
    public int asInt() {
        return ((Integer)this.asObject()).intValue();
    }

    public long asLong() {
        return ((Long)this.asObject()).longValue();
    }
    
    public boolean asBoolean() {
        return ((Boolean)this.asObject()).booleanValue();
    }

    public boolean asBool() {
        return ((Boolean)this.asObject()).booleanValue();
    }

    public float asFloat() {
        return ((Float)this.asObject()).floatValue();
    }

    public double asDouble() {
        return ((Double)this.asObject()).doubleValue();
    }

    public short asShort() {
        return ((Short)this.asObject()).shortValue();
    }

    public byte asByte() {
        return ((Byte)this.asObject()).byteValue();
    }

    public BigDecimal asDecimal() {
        return (BigDecimal) this.asObject();
    }
    
    public char asChar() {
        return ((Character)this.asObject()).charValue();
    }

    public Collection asCollection() {
        return (Collection)this.asObject();
    }
    
    public List asList() {
        return (List)this.asObject();
    }

    public <T> List<T> asList( Class<T> itemClass ) {
        return (List<T>)this.asObject();
    }
    
    public Set asSet() {
        return (Set)this.asObject();
    }
    
    public <T> Set<T> asSet( Class<T> itemClass ) {
        return (Set<T>)this.asObject();
    }

    public Queue asQueue() {
        return (Queue)this.asObject();
    }
    
    public <T> Queue<T> asQueue( Class<T> itemClass ) {
        return (Queue<T>)this.asObject();
    }
    
    public Stack asStack() {
        return (Stack)this.asObject();
    }
    
    public <T> Stack<T> asStack( Class<T> itemClass ) {
        return (Stack<T>)this.asObject();
    }

    public Map asMap() {
        return (Map)this.asObject();
    }

    public <K,V> Map<K,V> asMap( Class<K> keyClass, Class<V> itemClass ) {
        return (Map<K,V>)this.asObject();
    }
    
    public Arguments asArguments() {
        return asArgs();
    }
    
    public Arguments asArgs() {
        return Arguments.process( this.asObject() );
    }

    public Date asDate() {
        return (Date)this.asObject();
    }
    
    public Object retrieveItem( Object key ) {
        Object value = this.asObject();
        if( value instanceof List ) {
            if( Numbers.isNumber( key ) ) {
                return ((List)value).get( Numbers.Int( value ) );
            } else {
                throw new ValueException( "List index must be a Number or convertible equivalent" );
            }
        } else if( value instanceof Map ) {
            return ((Map)value).get( key );
        } else {
            return this.get( key );
        }
    }
    
    public void storeItem( Object key, Object item ) {
        set( (String)key, item );
    }
    
    public void deleteItem( Object key ) {
        remove( key );
    }

    public int itemsCount() {
        return size();
    }
    
    public boolean containsItem( Object key ) {
        return contains( key );
    }

    public void appendItem( Object item ) {
        add( item );
    }

    public void insertItem( Object where, Object item ) {
        if( where instanceof Number ) {
            this.add( ((Number) where).intValue(), item );
        } else {
            Object value = this.asObject();
            if( value instanceof Insert ) {
                ((Insert) value).insertItem( where, item );
            } else {
                throw new ValueException( "Non numeric insert index is not supported for this value" );
            }
        }
    }
    
    public Object locateItem( Object item ) {
        return this.keyOf( item );
    }
    
    public Placer placeItem( Object item ) {
        return this.place( item );
    }

    public Value value( Object key ){
        return Value.of( get( this.asObject(), key ) );
    }

    // public <T> T get(){
    //     return (T) this.asObject();
    // }

    public <T> T get( Object key ){
        return (T) get( this.asObject(), key );
    }
    
    public static Object get( Object value, Object key ){
        Object rawKey = Value.from( key );
        if( value != null ) {
            if( isExpression( rawKey ) ) {
                return evaluate( value, (String)rawKey );
            } else {
                if( value instanceof Retrieve ) {
                    return ((Retrieve) value).retrieveItem( rawKey );
                
                } else if( rawKey instanceof String 
                        && value instanceof Modelized 
                        && ((Modelized) value).retrieveModelInstance().has( (String)rawKey ) ) {
                    return ((Modelized) value).retrieveModelInstance().get( (String)rawKey );

                } else if( rawKey instanceof String 
                        && Properties.has( value, (String) rawKey ) ) {
                    return Properties.get( (String)rawKey );

                } else if( value instanceof Map ) {
                    return ((Map)value).get( rawKey );

                } else if( value instanceof List ){
                    if( rawKey instanceof Number ) {
                        return ((List)value).get( ((Number)rawKey).intValue() );
                    } else {
                        throw new ValueException( "List index must be a Number" );
                    }
                } else {
                    InstanceMethod method = InstanceMethod.lookup( value, Strings.valueOf( rawKey ) );
                    if( method != null ) {
                        return method;
                    } else {
                        throw new ValueException( "Cannot get item, unsupported value: " + Objects.repr( value ) );
                    }
                }

            }
            
        } else {
            throw new ValueException( "Cannot get item, value is null" );
        }
    }
    
    public Value set( Object newValue ) {
        this.setObject( Value.from( newValue ) );
        return this;
    }

    public Value setAll( Arguments arguments ) {
        for( String key: arguments.keys() ) {
            set( key, arguments.get( key ) );
        }
        return this;
    }

    public Value set( String expression, Object targetValue ) {
        Object value = this.asObject();
        if( value != null ) {
            if( isExpression( expression ) ) {
                valorize( value, expression, Value.from( targetValue ) );
                
            } else if( value instanceof Store ) {
                ((Store) value).storeItem( expression, Value.from( targetValue ) );
            
            } else if( value instanceof Modelized ) {
                ((Modelized) value).retrieveModelInstance().set( expression, Value.from( targetValue ) );
            
            } else {
                valorize( value, expression, Value.from( targetValue ) );
            }

            return this;
        } else {
            throw new ValueException( "Cannot set expression, value is null" );
        }
    }

    public Value set( Class<?> targetClass, Object newValue ) {
        this.setObject( language.type( targetClass ).convert( Value.from( newValue ) ) );
        return this;
    }

    public Value set( Class<?> targetClass, String expression, Object newValue ) {
        this.set( expression, language.type( targetClass ).convert( Value.from( newValue ) ) );
        return this;
    }

    public Value setString( Object value ) {
        this.setObject( Strings.valueOf( Value.from( value ) ) );
        return this;
    }
    
    public Value setString( String expression, Object value ) {
        this.set( expression, Strings.valueOf( Value.from( value ) ) );
        return this;
    }
    
    public Value setInt( Object value ) {
        this.setObject( IntType.process( Value.from( value ) ) );
        return this;
    }

    public Value setInt( String expression, Object value ) {
        this.set( expression, IntType.process( Value.from( value ) ) );
        return this;
    }

    public Value setLong( Object value ) {
        this.setObject( LongType.process( Value.from( value ) ) );
        return this;
    }

    public Value setLong( String expression, Object value ) {
        this.set( expression, LongType.process( Value.from( value ) ) );
        return this;
    }

    public Value setFloat( Object value ) {
        this.setObject( FloatType.process( Value.from( value ) ) );
        return this;
    }

    public Value setFloat( String expression, Object value ) {
        this.set( expression, FloatType.process( Value.from( value ) ) );
        return this;
    }

    public Value setDouble( Object value ) {
        this.setObject( DoubleType.process( Value.from( value ) ) );
        return this;
    }

    public Value setDouble( String expression, Object value ) {
        this.set( expression, DoubleType.process( Value.from( value ) ) );
        return this;
    }

    public Value setDecimal( Object value ) {
        this.setObject( DecimalType.process( Value.from( value ) ) );
        return this;
    }

    public Value setDecimal( String expression, Object value ) {
        this.set( expression, DecimalType.process( Value.from( value ) ) );
        return this;
    }

    public Value setBoolean( Object value ) {
        this.setObject( BooleanType.process( Value.from( value ) ) );
        return this;
    }
    
    public Value setBoolean( String expression, Object value ) {
        this.set( expression, BooleanType.process( Value.from( value ) ) );
        return this;
    }
    
    public Value setDate( Object value ) {
        this.setObject( DateType.process( Value.from( value ) ) );
        return this;
    }

    public Value setDate( String expression, Object value ) {
        this.set( expression, DateType.process( Value.from( value ) ) );
        return this;
    }

    public Value setDateTime( Object value ) {
        this.setObject( DateTimeType.process( Value.from( value ) ) );
        return this;
    }
    
    public Value setDateTime( String expression, Object value ) {
        this.set( expression, DateTimeType.process( Value.from( value ) ) );
        return this;
    }
    
    public Value setList( Object value ) {
        this.setObject( Collections.list( Value.from( value ) ) );
        return this;
    }
    
    public Value setList( String expression, Object value ) {
        this.set( expression, Collections.list( Value.from( value ) ) );
        return this;
    }
    
    public Value add( Object object ) {
        Object value = this.asObject();

        if( value != null ) {
            if( value instanceof Append ) {
                ((Append) value).appendItem( Value.from( object ) );
            } else if( value instanceof Map ) {
                throw new ValueException( "Cannot add to Map, use put( key, value ) instead" );
            } else if( value instanceof Collection ) {
                ((Collection) value ).add( Value.from( object ) );
            } else {
                throw new ValueException( "Cannot add item, unsupported value: " + Objects.repr( value ) );
            }
        
        } else {
            throw new ValueException( "Cannot add item, value is null" );
        }
        return this;
    }

    public Value add( int index, Object object ) {
        Object value = this.asObject();

        if( value != null ) {
            if( value instanceof Insert ) {
                ((Insert) value).insertItem( index, Value.from( object ) );
            } else if( value instanceof Map ) {
                throw new ValueException( "Cannot add to Map, use put( key, value ) instead" );
            } else if( value instanceof List ) {
                ((List) value ).add( index, Value.from( object ) );
            } else if( value instanceof String ) {
                this.setObject( ((String) value).substring( 0, index ) + Strings.valueOf( object ) + ((String) value).substring( index ) );
            } else {
                throw new ValueException( "Cannot add item, unsupported value: " + Objects.repr( value ) );
            }
        
        } else {
            throw new ValueException( "Cannot add item, value is null" );
        }

        return this;
    }

    public Value cat( Iterable iterable ) {
        for( Object o: iterable ) {
            this.add( o );
        }
        return this;
    }

    public Value cat( Object obj ) {
        if( Collections.isListable( obj ) ) {
            for( Object o: Collections.list( obj ) ) {
                this.add( o );
            }
        } else {
            this.add( obj );
        }
        return this;
    }

    public Value cat( Object... objects ) {
        Object value = this.asObject();

        for( Object object: objects ){
            if( value != null ) {
                if( value instanceof Append ) {
                    ((Append) value).appendItem( Value.from( object ) );
                } else if( value instanceof Map ) {
                    throw new ValueException( "Cannot add to Map, use put( key, value ) instead" );
                } else if( value instanceof Collection ) {
                    ((Collection) value ).add( Value.from( object ) );
                } else {
                    throw new ValueException( "Cannot add item, unsupported value: " + Objects.repr( value ) );
                }
            
            } else {
                throw new ValueException( "Cannot add item, value is null" );
            }
        }

        return this;
    }

    public Value put( Object key, Object object ) {
        Object value = this.asObject();
        if( value != null ) {
            if( value instanceof Store ) {
                ((Store) value).storeItem( Value.from( key ), Value.from( object ) );
            } else if( value instanceof Map ) {
                ((Map) value).put( Value.from( key ), Value.from( object ) );
            } else if( value instanceof List ) {
                Object rawKey = Value.from( key );
                if( rawKey instanceof String ) {
                    try {
                        rawKey = Integer.parseInt( (String) rawKey );
                    } catch( NumberFormatException ex ) {
                        rawKey = null;
                    }
                }
                if( rawKey instanceof Number ) {
                    ((List) value ).add( ((Number) rawKey).intValue(), Value.from( object ) );
                } else {
                    throw new ValueException( "Cannot put item, List key must be a Number: " + Objects.repr( Value.from( key ) ) );
                }
            } else {
                throw new ValueException( "Cannot add item, unsupported value: " + Objects.repr( value ) );
            }
            
        } else {
            throw new ValueException( "Cannot put item, value is null" );
        }

        return this;
    }
    
    public ValuePlacer place( Object item ) {
        Object value = this.asObject();

        if( value != null ) {
            if( value instanceof Place ) {
                return new NativePlacer( this, ((Place) value).placeItem( Value.from( item ) ) );
            } else {
                return new ValuePlacer( this, item );
            }
            
        } else {
            throw new ValueException( "Cannot place item, value is null" );
        }
    }
    
    public class ValuePlacer implements Placer {
        
        protected Value root;
        protected Object item;
        
        public ValuePlacer( Value root, Object item ) {
            this.root = root;
            this.item = item;
        }
        
        public Value at( int index ) {
            this.root.add( index, item );
            return this.root;
        }
        
        public Value first() {
            this.root.add( 0, item );
            return this.root;
        }

        public Value last() {
            this.root.add( item );
            return this.root;
        }
        
        public Value before( Object other ) {
            this.root.add( this.root.indexOf( other ), item );
            return this.root;
        }

        public Value after( Object other ) {
            this.root.add( this.root.indexOf( other ), item );
            return this.root;
        }
    
        public void placeFirst() {
            first();
        }
    
        public void placeLast() {
            last();
        }
    
        public void placeAt( int index ) {
            at( index );
        }
    
        public void placeBefore( Object item ) {
            before( item );
        }
    
        public void placeAfter( Object item ) {
            after( item );
        }
    
    }

    public class NativePlacer extends ValuePlacer {
        
        protected Placer placer;
        
        public NativePlacer( Value root, Placer placer ) {
            super( root, null );
            this.placer = placer;
        }
        
        public Value at( int index ) {
            this.placer.placeAt( index );
            return this.root;
        }
        
        public Value first() {
            this.placer.placeFirst();
            return this.root;
        }

        public Value last() {
            this.placer.placeLast();
            return this.root;
        }
        
        public Value before( Object other ) {
            this.placer.placeBefore( Value.from( other ) );
            return this.root;
        }

        public Value after( Object other ) {
            this.placer.placeAfter( Value.from( other ) );
            return this.root;
        }
    
    }
    
    public Value remove( Object object ) {
        Object value = this.asObject();
        Object raw = Value.from( object );
        
        if( value != null ) {
            if( value instanceof Delete ) {
                ((Delete) value).deleteItem( Value.from( object ) );
            } else if( value instanceof Map ) {
                ((Map) value).remove( raw );
            
            } else if( value instanceof Collection ) {

                if( value instanceof List && raw instanceof Number ) {
                    ((List) value).remove( ((Number) raw).intValue() );
                } else {
                    ((Collection) value).remove( raw );
                } 
            
            } else {
                throw new ValueException( "Cannot remove item, unsupported value: " + Objects.repr( value ) );
            }
                
        } else {
            throw new ValueException( "Cannot remove item, value is null" );
        }
        
        return this;
    }

    public static boolean contains( Object value, Object object ) {
        if( value instanceof Inspect ) {
            return ((Inspect) value).containsItem( Value.from( object ) );
        } else if( value instanceof Map ) {
            return ((Map)value).containsKey( Value.from( object ) );
        } else if( value instanceof Collection ){
            return ((Collection)value).contains( Value.from( object ) );
        } else if( value instanceof String ){
            return ((String)value).indexOf( object != null ? Strings.valueOf( object ) : null ) >= 0;
        } else if( Collections.isArray( value ) ){
            int len = Array.getLength( value );
            for( int i = 0; i < len; i++ ) {
                if( object != null ? object.equals( Array.get( value, i ) ) : Array.get( value, i ) == null ) {
                    return true;
                }
            } 
            return false;
        } else if( value == null ) {
            return false;
        } else {
            throw new ValueException( "Unsupported type for contains: " + Objects.repr( value ) );
        }
    }

    public boolean contains( Object object ) {
        return Value.contains( this.asObject(), object );
    }
    
    public static int size( Object value ) {
        if( value != null ) {
            if( value instanceof Length ) {
                return ((Length) value).itemsCount();
            } else if( value instanceof Collection ) {
                return ((Collection) value).size();
            } else if( Collections.isArray( value ) ) {
                return Array.getLength( value );
            } else if( value instanceof Map ) {
                return ((Map) value).size();
            } else if( value instanceof String ) {
                return ((String) value).length();
            } else if( value instanceof ObjectQuery ) {
                return ((ObjectQuery) value).list().size();
            } else {    
                throw new ValueException( "Unsupported type for size: " + Objects.repr( value ) );
            }
        } else {
            return 0;
        }
    }

    public int size() {
        return Value.size( this.asObject() );
    }
    
    public static void clear( Object value ) {
        if( value != null ) {
            if( value instanceof Clear ) {
                ((Clear) value).clearItems();
            } else if( value instanceof Collection ) {
                ((Collection) value).clear();
            } else if( value instanceof Map ) {
                ((Map) value).clear();
            } else {    
                throw new ValueException( "Unsupported type for clear: " + Objects.repr( value ) );
            }
        }
    }
    
    public Value clear() {
        Object value = this.asObject();
        if( value instanceof String ) {
            setObject( "" );
        } else {
            clear( value );
        }
        return this;
    }
    
    public Value call( Object... arguments ) {
        return Value.of( invokeInstance( arguments ) );
    }

    public Object invokeInstance( Object... arguments ) {
        Object value = this.asObject();
        for( int i: Range.of( arguments ) ) {
            if( arguments[ i ] instanceof Wrapper ) {
                arguments[ i ] = ((Wrapper) arguments[ i ]).asObject();
            }
        }
        
        if( value instanceof Invoke ) {
            return ((Invoke) value).invokeInstance( Value.from( arguments ) );
        } else if( value instanceof Callable ) {
            return ((Callable) value).call( Value.from( arguments ) );
        } else {
            throw new ValueException( "Value is not callable: " + Objects.repr( value ) );
        } 
    }
    
    public static Object invoke( Object callable, Object ... arguments ) {
        if( callable instanceof Invoke ) {
            return ((Invoke) callable).invokeInstance( Value.from( arguments ) );
        } else if( callable instanceof Callable ) {
            return ((Callable) callable).call( Value.from( arguments ) );
        } else {
            throw new ValueException( "Object is not callable: " + Objects.repr( callable ) );
        } 
    }
    
    public Functional<Value> each() {
        return new FunctionalIterable<Value>( new ValueIterable( Collections.iterable( this.asObject() ) ) );
    }
    
    public <T> Functional<T> each( Class<T> type ) {
        if( type != null ) {
            if( Value.class.equals( type ) ) {
                return (Functional<T>) each();
            } else {
                return Collections.<T>functional( this.asObject() ).filter( Filters.instance( type ) );
            }
        } else {
            throw new IllegalArgumentException( "Null type" );
        }
    }
    
    public Iterable<Integer> indexes() {
        Object value = this.asObject();
        if( value instanceof List ) {
            return Range.of( ((List) value).size() );
        } else if( value == null ){
            return new ArrayList<Integer>();
        } else {
            throw new ValueException( "Cannot iterate numerical indexes on " + Objects.repr( value ) );
        }
    }
 
    public Iterable keys() {
        return keys( Object.class );
    }
    
    public <T> Iterable<T> keys( Class<T> type ) {
        Object value = this.asObject();
        if( value instanceof Map ) {
            return ((Map) value).keySet();
        } else if( value instanceof List ) {
            if( type == Integer.class ) {
                return (Iterable<T>) Range.of( ((List) value).size() );
            } else {
                throw new ValueException( "List keys must be Integers" );
            }
        } else if( value == null ){
            return new ArrayList<T>();
        } else {
            throw new ValueException( "Value is not iterable: " + Objects.repr( value ) );
        }
    }


    public static class ValueIterable implements Iterable<Value> {
        private Iterable source;
        
        public ValueIterable( Iterable source ) {
            this.source = source;
        }
        
        public Iterator<Value> iterator() {
            return new ValueIterator( source.iterator() );
        }
    }
    
    public static class ValueIterator implements Iterator<Value> {
        
        private Iterator source;
        
        public ValueIterator( Iterator source ) {
            this.source = source;
        }
        
        public boolean hasNext() {
            return source.hasNext();
        }
        
        public Value next() {
            return Value.of( source.next() );
        }
        
        public void remove() {
            source.remove();
        }
    }

    public static <T> ObjectQuery<T> queryOf( Object value ) {
        if( value instanceof Query ) {
            return ((Query) value).<T>queryItems();
        } else {
            return new ObjectQuery<T>( Collections.<T>toIterable( value ) );
        }
    }

    public <T> ObjectQuery<T> query( String path ) {
        return queryOf( this.get( path ) );
    }
    
    public <T> ObjectQuery<T> query() {
        return queryOf( this.asObject() );
    }

    public <T> ObjectQuery<T> query( Class<T> requestedClass ) {
        return queryOf( this.asObject() ).instanceOf( requestedClass );
    }
    
    public static Object keyOf( Object value, Object item ) {
        if( value != null ) {
            if( value instanceof Locate ) {
                return ((Locate) value).locateItem( Value.from( item ) );
            } else if( value instanceof List ) {
                return ((List) value).indexOf( Value.from( item ) );
            } else if( value instanceof String ) {
                return ((String) value).indexOf( Strings.valueOf( item ) );
            } else if( value instanceof Map ){
                for( Map.Entry entry: (Set<Map.Entry>) ((Map) value).entrySet() ) {
                    if( entry.getValue() != null ? entry.getValue().equals( item ) : item == null ) {
                        return entry.getKey();
                    }
                }
                return null;
            } else if( value == null ) {
                return null;
            } else {    
                throw new ValueException( "Unsupported type for item location: " + Objects.repr( value ) );
            }
        } else {
            throw new ValueException( "Cannot locate item, value is null" );
        }
    }

    public Object keyOf( Object item ) {
        return keyOf( this.asObject(), item );
    }

    public static int indexOf( Object value, Object item ) {
        if( value != null ) {
            Object index = keyOf( value, item );
            try {
                return index instanceof Number ? ((Number) index).intValue() : IntType.process( index );
            } catch( IllegalArgumentException ex ) {
                throw new ValueException( "Invalid index type: " + Objects.repr( index ) );
            }
        } else {
            return -1;
        }
    }
    
    public int indexOf( Object item ) {
        return indexOf( this.asObject(), item );
    }

    public String toString() {
        return Strings.valueOf( this.asObject() );
    }
    
    public String toNString() {
        return Strings.plane( this.asObject() );
    }

    public Integer toNInt() {
        return this.asObject() == null ? null : toInt();
    }
    
    public int toInt() {
        Object value = this.asObject();
        return value == null ? 0 : value instanceof Integer ? asInt() : IntType.process( value );
    }

    public Long toNLong() {
        return this.asObject() == null ? null : toLong();
    }
    
    public long toLong() {
        Object value = this.asObject();
        return value == null ? 0 : value instanceof Long ? asLong() : LongType.process( value );
    }

    public Float toNFloat() {
        return this.asObject() == null ? null : toFloat();
    }
    
    public float toFloat() {
        Object value = this.asObject();
        return value == null ? 0 : value instanceof Float ? asFloat() : FloatType.process( value );
    }

    public Double toNDouble() {
        return this.asObject() == null ? null : toDouble();
    }
    
    public double toDouble() {
        Object value = this.asObject();
        return value == null ? 0 : value instanceof Double ? asDouble() : DoubleType.process( value );
    }

    public BigDecimal toNDecimal() {
        return this.asObject() == null ? null : toDecimal();
    }
    
    public BigDecimal toDecimal() {
        Object value = this.asObject();
        return value instanceof BigDecimal ? asDecimal() : Numbers.safeDecimal( value );
    }

    public BigDecimal toNDecimal( int scale ) {
        return this.asObject() == null ? null : toDecimal( scale );
    }
    
    public BigDecimal toDecimal( int scale ) {
        Object value = this.asObject();
        return value instanceof BigDecimal ? asDecimal() : Numbers.safeDecimal( value, scale );
    }

    public Boolean toNBoolean() {
        return this.asObject() == null ? null : toBoolean();
    }
    
    public boolean toBoolean() {
        Object value = this.asObject();
        return value == null ? false : value instanceof Boolean ? asBoolean() : BooleanType.process( value );
    }
    
    public Date toDate() {
        Object value = this.asObject();
        return value == null ? null : DateType.process( value );
    }

    public DateTime toDateTime() {
        Object value = this.asObject();
        return value == null ? null : DateTimeType.process( value );
    }
    
    public List toList() {
        return Collections.list( this.asObject() );
    }

    public <T> List<T> toList( Class<T> type ) {
        return Collections.<T>toList( this.asObject() );
    }
    
    public <T> T to( Class<T> type ) {
        return (T)( language.type( type ).convert( this.asObject() ) );
    }

    public <T> T to( Class<T> type, Converter converter ) {
        return (T)( converter.convert( this.asObject() ) );
    }
    
    public boolean isNull() {
        return this.asObject() == null;
    }

    public boolean notNull() {
        return this.asObject() != null;
    }

    public static boolean isEmpty( Object value ) {
        if( value != null ) {
            if( value instanceof Length ) {
                return ((Length) value).itemsCount() <= 0;
            } else if( value instanceof String ) {
                return ! Strings.hasText( value );
            } else if( value instanceof Collection ) {
                return ((Collection)value).isEmpty();
            } else if( value instanceof Map ) {
                return ((Map)value).isEmpty();
            } else if( value instanceof Iterable ) {
                return ((Iterable)value).iterator().hasNext();
            } else if( Undefined.matches( value ) ) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static boolean notEmpty( Object value ) {
        return ! isEmpty( value );
    }

    public boolean isEmpty() {
        return Value.isEmpty( this.asObject() );
    }

    public boolean notEmpty() {
        return ! isEmpty();
    }
    
    public boolean matches( String pattern ) {
        return matches( Pattern.compile( pattern ) );
    }
    
    public boolean matches( Pattern pattern ) {
        Object value = this.asObject();
        return value != null && pattern.matcher( value instanceof String ? (String) value : asText( value ) ).matches();
    }
    
    public boolean notMatches( String pattern ) {
        return notMatches( Pattern.compile( pattern ) );
    }
    
    public boolean notMatches( Pattern pattern ) {
        return ! matches( pattern );
    }
    
    public boolean isTrue() {
        return toBoolean() == true;
    }

    public boolean isFalse() {
        return toBoolean() == false;
    }
    
    public boolean instanceOf( Class<?> type ) {
        return type.isInstance( this.asObject() ); 
    }

    public static boolean is( Object value, Object other ) {
        if( other instanceof Class ) {
            if( value instanceof Class ) {
                return ((Class) other).isAssignableFrom( (Class) value );
            } else {
                return  ((Class) other).isInstance( value ); 
            }
        } else {
            if( value != null ) {
                if( value.getClass().isInstance( other ) ) {
                    return value.equals( other );
                } else {
                    return value.equals( language.type( value.getClass() ).convert( other ) );
                }
            } else {
                return other == null;
            }
        }
    }

    public boolean is( Object other ) {
        return is( this.asObject(), other );
    }

    public Value or( Object alternative ) {
        return this.asObject() == null ? Value.of( alternative ) : this;
    }
    
    public static String asText( Object value ) {
        if( value != null ) {
            if( value instanceof Text ) {
                return ((Text) value).asText();
            } else {
                return Strings.valueOf( value );
            }
        } else {
            return null;
        }
    }
    
    public String asText() {
        return asText( asObject() );
    }
    
    public Calculator calc() {
        return new Calculator().push( this.toDecimal() );
    }

    public Calculator calc( int scale ) {
        return new Calculator( scale ).push( this.toDecimal() );
    }

    public Calculator calc( int scale, Object roundingMode ) {
        return new Calculator( scale, RoundingModeType.process( roundingMode ) ).push( this.toDecimal() );
    }

    public boolean equals( Object other ) {
        if( other instanceof Value ) {
            return this.asObject() == null ? ((Value)other).asObject() == null : this.asObject().equals( ((Value)other).asObject() );
        } else {
            return this.asObject() == null ? other == null : this.asObject().equals( other );
        }
    }

    private final static TypeDefinition<String> StringType = TypeFactory.auto( String.class );
    private final static TypeDefinition<Integer> IntType = TypeFactory.auto( Integer.class );
    private final static TypeDefinition<Long> LongType = TypeFactory.auto( Long.class );
    private final static TypeDefinition<Float> FloatType = TypeFactory.auto( Float.class );
    private final static TypeDefinition<Double> DoubleType = TypeFactory.auto( Double.class );
    private final static TypeDefinition<BigDecimal> DecimalType = TypeFactory.auto( BigDecimal.class );
    private final static TypeDefinition<Boolean> BooleanType = TypeFactory.auto( Boolean.class );
    private final static TypeDefinition<Date> DateType = TypeFactory.auto( Date.class );
    private final static TypeDefinition<DateTime> DateTimeType = TypeFactory.auto( DateTime.class );
    
    private final static TypeDefinition<RoundingMode> RoundingModeType = TypeFactory.auto( RoundingMode.class );
    
    @Override
    public Value enter(){
        if( this.isNull() ) {
            throw new Guard.Exit();
        } else {
            return this;
        }
    }
    
    public boolean gt( Object value ) {
        return compare( value ) > 0;
    }
    
    public boolean ge( Object value ) {
        return compare( value ) >= 0;
    }
    
    public boolean lt( Object value ) {
        return compare( value ) < 0;
    }
    
    public boolean le( Object value ) {
        return compare( value ) <= 0;
    }
    
    public int compare( Object value ) {
        return compare( asObject(), from( value ) );
    }
    
    public static int compare( Object target, Object value ) {
        if( target instanceof Date || target instanceof DateTime || value instanceof Date || value instanceof DateTime ) {
            return Dates.date( target ).compareTo( Dates.date( value ) );
            
        } else if( target instanceof Number || value instanceof Number ) {
            return Numbers.compare( target, value );
            
        } else if( target instanceof String || value instanceof String ) {
            return Strings.safe( target ).compareTo( Strings.valueOf( value ) );

        } else if( target instanceof Comparable  ) {
            return ( (Comparable) target ).compareTo( value );
        
        } else {
            throw new ValueException( "Uncomparable values" );
        }
    }
    

}
