package it.neverworks.model;

import it.neverworks.lang.Strings;
import it.neverworks.model.Value;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.ModelInstanceAware;
import it.neverworks.model.features.Describe;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Store;

@Modelize
public abstract class AbstractModel implements Retrieve, Store, Inspect, Describe {
    
    @Modelize
    protected ModelInstance modelInstance;
    
    public abstract <T> T get( String name );

    public ModelInstance model() {
        return this.modelInstance;
    }
    
    public ModelInstance model( Object object ) {
        return ModelInstance.of( object instanceof String ? get( (String) object ) : object );
    }
    
    public Object retrieveItem( Object key ) {
        return this.modelInstance.get( Strings.valueOf( key ) );
    }
    
    public void storeItem( Object key, Object value ) {
        this.modelInstance.set( Strings.valueOf( key ), value );
    }
    
    public boolean containsItem( Object key ) {
        return this.modelInstance.has( Strings.valueOf( key ) );
    }
        
}