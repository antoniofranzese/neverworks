package it.neverworks.model.utils;

import it.neverworks.lang.Callable;
import it.neverworks.model.features.Invoke;

public class CallableInvoker<T> implements Callable<T> {

    private Invoke invoker;
    
    public CallableInvoker( Invoke invoker ) {
        this.invoker = invoker;
    }
    
    public T call( Object... arguments ) {
        return (T) invoker.invokeInstance( arguments );
    }
    
}