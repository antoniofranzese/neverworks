package it.neverworks.model.utils;

import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.model.expressions.ExpressionEvaluator;

public class ExpandoModel extends CoreExpandoModel {
    
    public ExpandoModel() {
        super();
    }
    
    public ExpandoModel( Map<String, Object> properties ) {
        super( properties );
    }
    
    public <T extends ExpandoModel> T set( String name, Object value ) {
        ExpressionEvaluator.valorize( this, name, value );
        return (T)this;
    }

    public <T extends ExpandoModel> T set( Arguments arguments ) {
        if( arguments != null ) {
            for( String key: arguments.keys() ) {
                set( key, arguments.get( key ) );
            }
        }
        return (T)this;
    }
    
    public <T extends ExpandoModel> T set( String arguments ) {
        return set( Arguments.parse( arguments ) );
    }
    
 }
