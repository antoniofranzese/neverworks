package it.neverworks.model.utils;

import it.neverworks.lang.Strings;
import it.neverworks.model.Value;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.expressions.ExpressionEvaluator;

public class ToStringBuilder {
    
    private StringBuilder snippets = new StringBuilder();
    private Object object;
    private boolean started = false;
    
    public ToStringBuilder( Object object ) {
        this( object, object.getClass().getSimpleName() );
    }

    public ToStringBuilder( Object object, String name ) {
        this.object = object;
        this.snippets.append( name ).append( "(");
    }
    
    public ToStringBuilder add( String name, Object value ) {
        if( name.startsWith( "!" ) ) {
            if( value == null ) {
                return this;
            } else {
                name = name.substring( 1 );
            }
        } else if( name.startsWith( "*" ) ) {
            if( Value.isEmpty( value ) ) {
                return this;
            } else {
                name = name.substring( 1 );
            }
        }
        
        Object text = null;
        try {
            text = value != null ? Strings.valueOf( value ) : "<null>";
        } catch( Exception ex ) {
            text = "<!>";
        }

        if( text != null ) {
            if( this.started ) {
                this.snippets.append( ", " );
            } else {
                this.started = true;
            }
        
            this.snippets.append( name ).append( "=" ).append( text );
        }
        
        return this;
    }

    public ToStringBuilder add( String name ) {
        if( name.startsWith( "*" ) ) {
            String clean = name.substring( 1 );
            return iterable( clean, evaluate( this.object, clean ) );
            
        } else if( name.startsWith( "!" ) ) {
            String clean = name.substring( 1 );
            return optional( clean, evaluate( this.object, clean ) );
            
        } else {
            return add( name, evaluate( this.object, name ) );
        }
    }
    
    public ToStringBuilder optional( String name, Object value ) {
        if( value != null ) {
            add( name, value );
        }
        return this;
    }
    
    public ToStringBuilder all() {
        for( String name: ModelDescriptor.of( this.object ).names() ) {
            add( name );
        }
        return this;
    }
    
    public ToStringBuilder iterable( String name, Object value ) {
        if( ! Value.isEmpty( value ) ) {
            add( name, value );
        }
        return this;
    }
    

    protected String evaluate( Object obj, String name ) {
        try {
            Object value = ExpressionEvaluator.evaluate( obj, name );
            return value != null ? Strings.valueOf( value ) : "<null>";
        } catch( Exception ex ) {
            return "<!>";
        }
    }

    public String toString() {
        this.snippets.append( ")" );
        return this.snippets.toString();
    }
    
}