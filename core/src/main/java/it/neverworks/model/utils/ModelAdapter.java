package it.neverworks.model.utils;

import it.neverworks.model.Model;
import it.neverworks.model.Modelize;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.Preserved;

@Modelize
public class ModelAdapter implements Model {
    
    @Modelize
    private ModelInstance instance;
    
    @Preserved
    public ModelAdapter( ModelInstance instance ) {
        this.instance = instance;
    }
    
    public ModelInstance model() {
        return this.instance;
    }
    
    @Preserved
    public ModelInstance retrieveModelInstance() {
        return this.instance;
    }

}
