package it.neverworks.model.utils;

import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Collection;

import it.neverworks.language;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Ensure;
import it.neverworks.model.features.Assign;
import it.neverworks.model.features.Get;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.description.UnwritablePropertyException;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Expression;
import it.neverworks.model.ModelException;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.Value;
import it.neverworks.model.Hook;

@Convert( ExpandoModelConverter.class )
public class CoreExpandoModel extends AbstractModel implements Map<String, Object>, Retrieve, Inspect, Store, Ensure, Assign, Get {
    
    protected Map<String, Object> properties;
    
    public CoreExpandoModel() {
        this.properties = new HashMap<String, Object>();
    }
    
    public CoreExpandoModel( Map<String, Object> properties ) {
        this.properties = new HashMap<String, Object>( properties );
    }
    
    public boolean containsItem( Object key ) {
        return model().has( Strings.valueOf( key ) ) || properties.containsKey( key );
    }
    
    public Object ensureExpression( Expression expression ) {
        String text = expression.text();
        if( model().has( text ) ) {
            try {
                model().set( text, model().property( text ).getType().newInstance() );
            } catch( Exception ex ) {
                return null;
            }
        } else if( ! properties.containsKey( text ) ) {
            properties.put( text, Reflection.newInstance( this.getClass() ) );
        }
        return get( text );
    }

    public void assignExpression( Expression expression, Object value ) {
        //TODO: verifica gestione unwrap/raw 
        storeItem( expression.text(), value );
    }

    public Object retrieveItem( Object key ) {
        String name = Strings.valueOf( key );
        if( Strings.hasText( name ) ) {
            
            if( model().has( name ) ) {
                return Value.from( model().<Object>get( name ) );
            } else if( properties.containsKey( name ) ) {
                return Value.from( properties.get( name ) );
            } else {
                throw new MissingPropertyException( this, name );
            }
            
        } else {
            throw new ModelException( "Null property name" );
        }
    }
    
    public <T> T get( String name ) {
        return (T) ExpressionEvaluator.evaluate( this, name );
    }
    
    public <T> T get( String name, T defaultValue ) {
        Object value = get( name );
        return (T) ( value != null ? value : defaultValue );
    }
    
    public void storeItem( Object key, Object value ) {
        String name = Strings.valueOf( key );
        if( model().has( name ) ) {
            model().set( name, value );
        } else {            
            properties.put( name, value );
        }
    }
    
    public boolean contains( String name ) {
        return containsItem( name );
    }
    
	public void clear () {
		properties.clear();
	}

	public Set<Entry<String, Object>> entrySet () {
		return properties.entrySet();
	}

	public Object get ( Object key ) {
	    return get( Strings.valueOf( key ) );
	}

	public Object get ( Object key, Object defaultValue ) {
        Object value = get( Strings.valueOf( key ) );
        return value != null ? value : defaultValue;
	}
	
	public boolean isEmpty () {
		return properties.isEmpty() && model().names().isEmpty();
	}

	@Override
	public Set<String> keySet () {
        Set<String> result = new HashSet<String>( properties.keySet() );
        result.addAll( model().names() );
        result.remove( "empty" );
		return result;
	}

	@Override
	public void putAll(Map<? extends String, ? extends Object> m) {
        if( m != null ) {
            for( String key: m.keySet() ) {
                storeItem( key, m.get( key ) );
            }
        }
	}

	public Object remove ( Object key ) {
		return properties.remove( key );
	}

	public int size () {
		return properties.size();
	}

	public Collection<Object> values () {
        List<Object> result = new ArrayList<Object>( properties.values() ); 
        for( String name: model().names() ) {
            result.add( get( name ) );
        }
		return result;
	}

	@Override
	public boolean containsKey(Object key) {
		return containsItem( key );
	}

	@Override
	public boolean containsValue(Object value) {
		if( properties.containsValue( value ) ){
		    return true;
		} else {
		    for( String name: model().names() ) {
		        Object v = get( name );
                if( v != null ? v.equals( value ) : value == null ) {
                    return true;
                }
		    }
            return false;
		}
	}

	@Override
	public Object put(String key, Object value) {
        Object existing = containsKey( key ) ? get( key ) : null;
        storeItem( key, value );
        return existing;
	}
    
    public boolean has( String name ) {
        return containsItem( name );
    }

    /*package*/ Map<String, Object> getProperties() {
        return this.properties;
    }
    
    /*package*/ void setProperties( Map<String, Object> properties ){
        for( String name: properties.keySet() ) {
            storeItem( name, properties.get( name ) );
        }
    }
    
    public String toString() {
        return this.getClass().getSimpleName() + "(" + language.list().cat( properties.keySet() ).join( ", " ) + ")";
    }
}

