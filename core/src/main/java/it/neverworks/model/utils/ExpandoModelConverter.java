package it.neverworks.model.utils;

import java.io.Reader;
import java.io.InputStream;
import java.util.Map;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.TypeAware;

public class ExpandoModelConverter implements Converter, TypeAware {
    
    private Class<? extends ExpandoModel> type = ExpandoModel.class;
    
    public void setRelatedClass( Class<?> type ) {
        this.type = (Class<? extends ExpandoModel>)type;
    }
    
    public Object convert( Object value ) {
        if( value != null ) {
            if( this.type.isAssignableFrom( value.getClass() ) ) {
                return value;

            } else if( value instanceof Map ) {
                ExpandoModel result = (ExpandoModel) Reflection.newInstance( this.type );
                // Usa un metodo package per non obbligare ad ereditare il costruttore con Map
                result.setProperties( ((Map) value) );
                return result;

            } else if( Arguments.isParsable( value ) ) {
                return convert( Arguments.parse( value ) );
        
            } else if( value instanceof InputStream ) {
                return convert( Arguments.load( (InputStream) value ) );
    
            } else if( value instanceof Reader ) {
                return convert( Arguments.load( (Reader) value ) );

            } else if( ModelInstance.supports( value ) ) {
                ModelInstance source = ModelInstance.of( value );
                ExpandoModel target = (ExpandoModel) Reflection.newInstance( this.type );
                for( String name: source.names() ) {
                    if( source.property( name ).getReadable() ) {
                        target.set( name, source.get( name ) );
                    }
                }
                return target;
 
            } else {
                return value;
            }
            
        } else {
            return null;
        }
        
    }
    
}