package it.neverworks.model.utils;

import it.neverworks.model.BaseModel;
import it.neverworks.model.Property;
import it.neverworks.model.description.Defended;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.features.Inspect;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.lang.Strings;

public class ProxyModel extends BaseModel implements Inspect {
    
    @Property @Defended
    protected Object target;
    
    public ProxyModel() {
        super();
    }
    
    public ProxyModel( Object target ) {
        super();
        this.target = target;
    }
    
    public boolean containsItem( Object key ) {
        String name = Strings.valueOf( key );
        return this.modelInstance.has( name ) || ( this.target != null && ExpressionEvaluator.knows( this.target, name ) );
    }

    public Object retrieveItem( Object key ) {
        String name = Strings.valueOf( key );
        if( this.modelInstance.has( name ) ) {
            return this.modelInstance.get( name );
        } else if( this.target != null && ExpressionEvaluator.knows( this.target, name ) ) {
            return ExpressionEvaluator.evaluate( this.target, name );
        } else {
            throw new MissingPropertyException( this, name );
        }
    }
    
    public void storeItem( Object key, Object value ) {
        String name = Strings.valueOf( key );
        
        if( this.modelInstance.has( name ) ) {
            this.modelInstance.set( name, value );
        } else if( this.target != null && ExpressionEvaluator.knows( this.target, name ) ) {
            ExpressionEvaluator.valorize( this.target, name, value );
        } else {
            throw new MissingPropertyException( this, name );
        }
    }
    
    protected <T> T target( String expr ) {
        return (T) ExpressionEvaluator.evaluate( this.target, expr );
    }

    protected <T extends ProxyModel> T target( String expr, Object value ) {
        ExpressionEvaluator.valorize( this.target, expr, value );
        return (T) this;
    }
    
    public <T> T get( String name ) {
        return (T) ExpressionEvaluator.evaluate( this, name );
    }
    
    public <T extends BaseModel> T set( String name, Object value ) {
        ExpressionEvaluator.valorize( this, name, value );
        return (T)this;
    }
    
}