package it.neverworks.model.utils;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

import it.neverworks.lang.Collections;
import it.neverworks.lang.Properties;
import it.neverworks.model.description.ModelDescriptor;
import static it.neverworks.model.expressions.ExpressionEvaluator.evaluate;

public class EqualsBuilder {
    
    private Object master;
    private List<Comparator> properties = new ArrayList<Comparator>();
    private boolean allowSubClasses = false;
    
    public EqualsBuilder( Object master ) {
        this.master = master;
    }
    
    public EqualsBuilder() {}
    
    public EqualsBuilder add( String name ) {
        if( name.startsWith( "!" ) ) {
            this.properties.add( new NullablePropertyComparator( name.substring( 1 ) ) );
        } else if( name.startsWith( "*" ) ) {
            this.properties.add( new IterablePropertyComparator( name.substring( 1 ) ) );
        } else {
            this.properties.add( new RequiredPropertyComparator( name ) );
        }
        return this;
    }

    public EqualsBuilder iterable( String name ) {
        this.properties.add( new IterablePropertyComparator( name ) );
        return this;
    }

    public EqualsBuilder optional( String name ) {
        this.properties.add( new IterablePropertyComparator( name ) );
        return this;
    }

    public EqualsBuilder withInheritance() {
        this.allowSubClasses = true;
        return this;
    }

    public boolean equals( Object other ) {
        if( this.master != null ) {
            return equals( this.master, other );
        } else {
            throw new RuntimeException( "EqualsBuilder needs a master object to compare" );
        }
    }

    public boolean equals( Object master, Object other ) {
        if( other != null ) {
            if( master == other ) {
                return true;
            } else {
                if( allowSubClasses ? master.getClass().isAssignableFrom( other.getClass() ) : master.getClass().equals( other.getClass() ) ) {
                
                    for( Comparator property: properties ) {
                        if( property.compare( master, other ) != 0 ) {
                            return false;
                        }
                    }
                
                    return true;
                
                } else {
                    return false;
                }
            }
            
        } else {
            return false;
        }
    }
    
    private class RequiredPropertyComparator implements Comparator {
        protected String name;
        
        public RequiredPropertyComparator( String name ) {
            this.name = name;
        }
        
        public int compare( Object o1, Object o2 ) {
            Object value1 = evaluate( o1, name );
            Object value2 = evaluate( o2, name );
            if( value1 != null && value1.equals( value2 ) ) {
                return 0;
            } else {
                return -1;
            }
        }
    }

    private class NullablePropertyComparator implements Comparator {
        protected String name;
        
        public NullablePropertyComparator( String name ) {
            this.name = name;
        }
        
        public int compare( Object o1, Object o2 ) {
            Object value1 = evaluate( o1, name );
            Object value2 = evaluate( o2, name );
            if( value1 != null ? value1.equals( value2 ) : value2 == null ) {
                return 0;
            } else {
                return -1;
            }
        }
    }
    
    private class IterablePropertyComparator extends NullablePropertyComparator {
        
        public IterablePropertyComparator( String name ) {
            super( name );
        }
        
        public int compare( Object o1, Object o2 ) {
            Object value1 = evaluate( o1, name );
            Object value2 = evaluate( o2, name );
            if( value1 instanceof Iterable
                && value2 instanceof Iterable
                && Collections.equals( (Iterable) value1, (Iterable) value2 ) ) {
                return 0;

            } else if( value1 == null && value2 == null ) {
                return 0;

            } else {
                return super.compare( o1, o2 );
            }
        }
    }

    
}