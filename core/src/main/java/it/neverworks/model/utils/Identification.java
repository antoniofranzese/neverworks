package it.neverworks.model.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
public @interface Identification {
    String[] value() default {};
    String[] hash() default {};
    String[] string() default {};
    String[] equals() default {};
    String[] common() default {};
}
