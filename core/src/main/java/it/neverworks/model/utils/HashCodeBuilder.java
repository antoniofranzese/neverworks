package it.neverworks.model.utils;

import java.util.Map;
import java.util.HashMap;
import it.neverworks.lang.Numbers;
import it.neverworks.model.description.ModelDescriptor;
import static it.neverworks.model.expressions.ExpressionEvaluator.evaluate;

public class HashCodeBuilder {
    
    private static Map classNumbersRegistry = new HashMap();
    
    private class ClassNumbers {
        private int number1;
        private int number2;
    }
    
    private org.apache.commons.lang.builder.HashCodeBuilder builder;
    private Object object;
    
    public HashCodeBuilder( Object object ) {
        String className = object.getClass().getName();
        if( ! classNumbersRegistry.containsKey( className ) ) {
            synchronized( classNumbersRegistry ) {
                if( ! classNumbersRegistry.containsKey( className ) ) {
                    ClassNumbers numbers = new ClassNumbers();
                    numbers.number1 = className.hashCode();
                    if( numbers.number1 % 2 == 0 ) {
                        numbers.number1 += 1 ;
                    }

                    numbers.number2 = Numbers.random( 1, Integer.MAX_VALUE );
                    if( numbers.number2 % 2 == 0 ) {
                        numbers.number2 += 1 ;
                    }
                    classNumbersRegistry.put( className, numbers );
                }
            }
        }
        ClassNumbers classNumbers = (ClassNumbers) classNumbersRegistry.get( object.getClass().getName() );
        this.builder = new org.apache.commons.lang.builder.HashCodeBuilder( classNumbers.number1, classNumbers.number2 );
        this.object = object;
    }
    
    public HashCodeBuilder add( String name ) {
        if( name.startsWith( "*" ) ) {
            Object value = evaluate( this.object, name.substring( 1 ) );
            if( value instanceof Iterable ) {
                for( Object item: (Iterable) value ) {
                    this.builder.append( item );
                }
            } else {
                this.builder.append( value );
            }
        } else {
            this.builder.append( (Object) evaluate( this.object, name ) );
        }
        return this;
    }
    
    public HashCodeBuilder append( Object value ) {
        this.builder.append( value );
        return this;
    }
    
    public int toHashCode() {
        return this.builder.toHashCode();
    }

    public int hashCode() {
        return this.builder.toHashCode();
    }
    
}