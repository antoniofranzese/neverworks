package it.neverworks.model.validators;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Numbers;
import it.neverworks.model.Value;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class RangeValidator<T> extends AbstractNumberValidator<T> {
    
    @Property @AutoConvert
    private Number min;
    
    @Property @AutoConvert
    private Number max;
    
    public RangeValidator() {
        super();
    }
    
    public RangeValidator( Number max ) {
        this();
        this.max = max;
    }

    public RangeValidator( Number min, Number max ) {
        this();
        this.min = min;
        this.max = max;
    }
    
    @Override
    protected void validateNumber( T object, Number number ) {
        if( min != null && Numbers.compare( number, min ) < 0 ) {
            raise( T( "Value is lesser than {min,number}" ), object );
        } else if( max != null && Numbers.compare( number, max ) > 0 ) {
            raise( T( "Value is greater than {max,number}" ), object );
        }
    }
    
    @Override
    protected Arguments getErrorArguments( T object ) {
        return super.getErrorArguments( object )
            .arg( "min", this.min )
            .arg( "max", this.max );
    }
    
    public Number getMax(){
        return this.max;
    }
    
    public void setMax( Number max ){
        this.max = max;
    }
    
    public Number getMin(){
        return this.min;
    }
    
    public void setMin( Number min ){
        this.min = min;
    }
    
}