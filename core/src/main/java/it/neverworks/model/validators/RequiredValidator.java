package it.neverworks.model.validators;

import static it.neverworks.language.*;

public class RequiredValidator<T> extends AbstractValidator<T> {
    
    @Override
    public void validate( T object ) {
        if( value( object ) == null ) {
           raise( T( "Missing value" ), object );
        }
    }
    
}