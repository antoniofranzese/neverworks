package it.neverworks.model.validators;

import static it.neverworks.language.*;

import it.neverworks.lang.Numbers;

public abstract class AbstractNumberValidator<T> extends AbstractValidator<T> {
    
    private boolean lenient = false;
    
    @Override
    public void validate( T object ) {
        Number number = null;
        Object value = value( object );

        try {
            number = Numbers.number( value );
        } catch( Exception ex ) {
            if( this.lenient ) {
                number = null;
            } else {
                raise( T( "Invalid number" ), object );
            } 
        }
        
        if( number != null ) {
            validateNumber( object, number );    
        };
        
    }
    
    protected abstract void validateNumber( T object, Number number );
    
    public boolean getLenient(){
        return this.lenient;
    }
    
    public void setLenient( boolean lenient ){
        this.lenient = lenient;
    }
    
}