package it.neverworks.model.validators;

import it.neverworks.lang.Filter;
import it.neverworks.model.Model;

public interface Validator<T> extends Filter<T> {
    
    void validate( T object );
    
    default boolean valid( T object ) {
        try {
            this.validate( object );
            return true;
        } catch( ValidationException ex ) {
            return false;
        }
    }

    default boolean invalid( T object ) {
        try {
            this.validate( object );
            return false;
        } catch( ValidationException ex ) {
            return true;
        }
    }
    
    default Filter valid() {
        return new ValidFilter<T>( this );
    }

    default Filter invalid() {
        return new ValidFilter<T>( this );
    }

    default boolean filter( T object ) {
        return invalid( object );
    }

    public static class ValidFilter<T> implements Filter<T> {
    
        private Validator<T> validator;
    
        public ValidFilter( Validator<T> validator ) {
            this.validator = validator;
        }
    
        public boolean filter( T object ) {
            return this.validator.valid( object );
        }

    }
}