package it.neverworks.model.validators;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Model;
import it.neverworks.model.Property;
import it.neverworks.model.expressions.Modifiers;
import it.neverworks.model.expressions.ExpressionEvaluator;

public abstract class AbstractValidator<T> implements Validator<T>, Model {
    
    private final static Modifiers MODIFIERS = new Modifiers().withNulls().freeze();

    @Property
    protected String path;

    protected Object value( T object ) {
        return this.path == null || object == null ? object : ExpressionEvaluator.evaluate( object, this.path, MODIFIERS ); 
    }
    
    protected void raise( String message, T object ) {
        raise( message, getErrorArguments( object ) );
    }

    protected void raise( String message, Arguments args ) {
        throw new ValidationException( this, msg( message, args ), args.get( "value" ), args.get( "target" ), args.get( "path" ) );
    }
    
    protected Arguments getErrorArguments( T object ) {
        return arg( "target", object )
            .arg( "value", value( object ) )
            .arg( "path", this.path );
    }

}