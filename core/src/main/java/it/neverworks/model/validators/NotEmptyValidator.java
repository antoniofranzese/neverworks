package it.neverworks.model.validators;

import static it.neverworks.language.*;

import it.neverworks.model.Value;

public class NotEmptyValidator<T> extends AbstractValidator<T> {
    
    @Override
    public void validate( T object ) {
        Object value = value( object );
        if( value != null && Value.isEmpty( value ) ) {
           raise( T( "Empty value" ), object );
        }
    }
    
}