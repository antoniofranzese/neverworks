package it.neverworks.model.validators;

import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyPostProcessor;
import it.neverworks.model.description.MetaPropertyManager;

public class ValidationMetadata implements PropertyMetadata, PropertyProcessor, PropertyPostProcessor {

    public void processProperty( MetaPropertyManager property ) {
        
    }
    
    public void postProcessProperty( MetaPropertyManager property ) {

    }
    
    public void meetProperty( MetaPropertyManager property ) {
        
    }
    
    public void inheritProperty( MetaPropertyManager property ) {
        
    }
    
}