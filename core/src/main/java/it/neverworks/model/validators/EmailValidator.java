package it.neverworks.model.validators;

import static it.neverworks.language.*;

import java.util.regex.Pattern;
import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class EmailValidator<T> extends PatternValidator<T> {

    protected final static Pattern DEFAULT = Pattern.compile( "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$" ); 
    
    public EmailValidator() {
        this( DEFAULT );
    }

    public EmailValidator( String pattern ) {
        this( Pattern.compile( pattern ) );
    }
    
    public EmailValidator( Pattern pattern ) {
        super( pattern, T( "Invalid email address" ) );
    }
    
}