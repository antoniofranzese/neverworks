package it.neverworks.model.validators;

import static it.neverworks.language.*;

import java.util.regex.Pattern;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class PatternValidator<T> extends AbstractValidator<T> {
    
    @Property @AutoConvert
    protected Pattern pattern;
    
    @Property
    protected String message;
    
    public PatternValidator() {
        this( (Pattern) null );
    }
    
    public PatternValidator( String pattern ) {
        this( Pattern.compile( pattern ) );
    }

    public PatternValidator( Pattern pattern ) {
        this( pattern, T( "Value does not match ''{pattern}''" ) );
    }

    public PatternValidator( String pattern, String message ) {
        this( Pattern.compile( pattern ), message );
    }

    public PatternValidator( Pattern pattern, String message ) {
        this.pattern = pattern;
        this.message = message;
    }
    
    @Override
    public void validate( T object ) {
        if( this.pattern != null ) {
            String value = Strings.valueOf( value( object ) );
            if( Strings.hasText( value ) && ! this.pattern.matcher( value ).matches() ) {
                raise( this.message , object );
            }
        }
    }
    
    @Override
    protected Arguments getErrorArguments( T object ) {
        return super.getErrorArguments( object )
            .arg( "pattern", this.pattern.pattern() );
    }
    
}