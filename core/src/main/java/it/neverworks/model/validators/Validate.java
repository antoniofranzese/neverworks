package it.neverworks.model.validators;

import java.lang.annotation.Repeatable;

@Repeatable( Validations.class )
public @interface Validate {
    Class<? extends Validator> value();
    String setup() default "";
}