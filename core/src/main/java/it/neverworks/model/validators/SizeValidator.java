package it.neverworks.model.validators;

import static it.neverworks.language.*;

import it.neverworks.lang.Arguments;
import it.neverworks.model.Value;
import it.neverworks.model.Property;
import it.neverworks.model.converters.AutoConvert;

public class SizeValidator<T> extends AbstractValidator<T> {
    
    @Property @AutoConvert
    private Integer min;
    
    @Property @AutoConvert
    private Integer max;
    
    public SizeValidator() {
        super();
    }
    
    public SizeValidator( Number max ) {
        this();
        this.max = Int( max );
    }

    public SizeValidator( Number min, Number max ) {
        this();
        this.min = Int( min );
        this.max = Int( max );
    }
    
    @Override
    public void validate( T object ) {
        Object value = value( object );
        if( value != null ) {
            int size = Value.size( value );
            if( min != null && size < min ) {
                raise( T( "Size ({size,number}) is smaller than {min,number}" ), getErrorArguments( object ).arg( "size", size ) );
            } else if( max != null && size > max ) {
                raise( T( "Size ({size,number}) is larger than {max,number}" ), getErrorArguments( object ).arg( "size", size ) );
            }
        }
    }
    
    @Override
    protected Arguments getErrorArguments( T object ) {
        return super.getErrorArguments( object )
            .arg( "min", this.min )
            .arg( "max", this.max );
    }
    
    public Integer getMax(){
        return this.max;
    }
    
    public void setMax( Integer max ){
        this.max = max;
    }
    
    public Integer getMin(){
        return this.min;
    }
    
    public void setMin( Integer min ){
        this.min = min;
    }
    
}