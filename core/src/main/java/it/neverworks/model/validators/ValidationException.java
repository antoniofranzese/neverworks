package it.neverworks.model.validators;

import static it.neverworks.language.*;

import it.neverworks.model.utils.Identification;

@Identification({ "message", "value", "target", "path", "validator" })
public class ValidationException extends RuntimeException {
    
    private Object value;
    private Object target;
    private String path;
    private Validator validator;
    
    public ValidationException( Validator validator, String message, Object value, Object target, String path ) {
        super( message );
        this.validator = validator;
        this.target = target;
        this.path = path;
    }

    public ValidationException( Validator validator, String message, Object value ) {
        this( validator, message, value, value, null );
    }

    public ValidationException( String message, Object value, Object target, String path ) {
        this( null, message, value, target, path );
    }

    public ValidationException( String message, Object value ) {
        this( null, message, value, value, null );
    }

    public ValidationException( String message ) {
        this( null, message, null, null, null );
    }

    public ValidationException join( Object target ) {
        return join( target, null );
    }
    
    public ValidationException join( Object target, String path ) {
        return new ValidationException( this.validator, this.getMessage(), this.value, target, path );
    }
    
    public String getPath(){
        return this.path;
    }
    
    public Object getTarget(){
        return this.target;
    }
    
    public Object getValue(){
        return this.value;
    }
    
    public Validator getValidator(){
        return this.validator;
    }
    
    public String getFullMessage() {
        return msg( "{message} [{value,text,limit=50,...}]", this );
    }
    
}