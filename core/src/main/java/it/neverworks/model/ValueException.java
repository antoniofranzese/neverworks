package it.neverworks.model;

public class ValueException extends RuntimeException {
    
    public ValueException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public ValueException( String message ){
        super( message );
    }
}