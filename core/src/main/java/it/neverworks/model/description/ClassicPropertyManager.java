package it.neverworks.model.description;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.AnnotationInfo;

import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.model.converters.ConvertProcessor;
import it.neverworks.model.converters.AutoConvertProcessor;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.ModelException;

@SuppressWarnings( "unchecked" )
public class ClassicPropertyManager extends BasePropertyManager {
    
    protected java.beans.PropertyDescriptor property;
    protected Field field;
    protected Method getter; 
    protected Method setter; 
    
    public ClassicPropertyManager( String name, ModelDescriptor model, java.beans.PropertyDescriptor property, Field field ) {
        super( name, model );
        this.property = property;
        this.name = property.getName();
        this.field = field;
        Reflection.makeAccessible( field );

        processAnnotations( field );
        processAccessors();
        setupProperty();
    }    
    
    protected ClassicPropertyManager( String name, ModelDescriptor model, java.beans.PropertyDescriptor property ) {
        super( name, model );
        this.property = property;
        this.name = property.getName();
        
        processAccessors();
        setupProperty();
    }

    protected void setupProperty() {
        // Type
        if( this.type == null ) {
            this.type = TypeFactory.build( this.getStorageType() );
        }

        //TODO: attivare solo su richiesta specifica
        //processTypeAnnotations();
        
        // Undefined
        if( this.undefined == null ) {
            this.undefined = new StandardUndefinedInspector();
        }

        // Converter
        if( this.converter instanceof InstanceConverter ) {
            this.hasInstanceConverter = true;
        }

    }

    protected void processAccessors() {
        // Getter
        this.getter = property.getReadMethod();
        if( this.getter != null ) {
            this.readable = true;
            Reflection.makeAccessible( this.getter );
            processAnnotations( this.getter );
        } else {
            this.readable = false;
        }
            
        // Setter
        this.setter = property.getWriteMethod();
        if( this.setter != null ) {
            this.writable = true;
            Reflection.makeAccessible( this.setter );
            processAnnotations( this.setter );
        } else {
            this.writable = false;
        }
    }
    
    public Class getStorageType() {
        if( this.property != null ) {
            return this.property.getPropertyType();
        } else if( this.field != null ) {
           return this.field.getType();
        } else if( this.getter != null ) {
            return this.getter.getReturnType();
        } else if( this.setter != null && this.setter.getParameterTypes().length > 0 ) {
            return this.setter.getParameterTypes()[0];
        } else {
            return Object.class;
        }
    }

    protected void processAnnotations( AccessibleObject source ) {
        for( Annotation annotation: source.getAnnotations() ) {
            processAnnotation( new AnnotationInfo( annotation, source ) );
            this.metadata.add( annotation );
        }
    }

    protected void processAnnotation( AnnotationInfo info ) {
        PropertyProcessor processor = detectProcessor( info );
        if( processor != null && processor instanceof ClassicPropertyProcessor ) {
            ((ClassicPropertyProcessor) processor).processProperty( this );
        }
    }
    
    protected void processTypeAnnotations() {
        Class type = this.type.getRawType();
        if( type != null ) {
            for( Annotation classAnnotation: Annotations.all( type ) ) {
                processAnnotation( new AnnotationInfo( classAnnotation, type ) );
            }
        }
    }

    public Object get( Object object ) {
        return getDirectValue( ModelInstance.of( object ) );
    }
    
    public Object getFrontValue( ModelInstance instance ) {
        return getDirectValue( instance );
    }
    
    public Object getCheckedValue( ModelInstance instance ) {
        return getDirectValue( instance );
    }
    
    public Object getUncheckedValue( ModelInstance instance ) {
        return getDirectValue( instance );
    }
    
    public Object getDirectValue( ModelInstance instance ) {
        if( this.readable ) {
            try {
                return this.getter.invoke( instance.actual );
            } catch( InvocationTargetException ex ) {
                throw Errors.wrap( ex );
            } catch( IllegalAccessException ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            throw new UnreadablePropertyException( this, "property is not readable" );
        }
    }

    public Object getRawValue( ModelInstance instance ) {
        try {
            //System.out.println( "Raw " + this.getFullName() +  " = " + this.field.get( instance.actual ) );
            return this.field.get( instance.actual );
        } catch( IllegalAccessException ex ) {
            throw new ModelException( "Illegal access getting " + this.getFullName(), ex );
        }
    }

    public boolean hasRawValue( ModelInstance instance ) {
        return true;
    }
    
    public void set( Object object, Object value ) {
        setFrontValue( ModelInstance.of( object ), value );
    }

    public void setFrontValue( ModelInstance instance, Object value ) {
        if( this.writable ) {
            setCheckedValue( instance, value );
        } else {
            throw new UnwritablePropertyException( this, "property is not writable" );
        }
    }

    public void setCheckedValue( ModelInstance instance, Object value ) {
       setUncheckedValue( instance, process( instance, value ) );
    }

    public void setUncheckedValue( ModelInstance instance, Object value ) {
        boolean watching = !virtual && !ignored && instance.watching();
        Object previous = null;
        
        if( watching ) {
            try {
                previous = getRawValue( instance );
            } catch( UnreadablePropertyException ex ) {
                watching = false;
            }
        }

        setDirectValue( instance, value );

        if( watching ) {
            Object current = getRawValue( instance );
            if( !( current == null ? previous == null : current.equals( previous ) ) ) {
                instance.touch( this.name );
            }
        }
    }

    public void setDirectValue( ModelInstance instance, Object value ) {
        if( this.writable ) {
            try {
                this.setter.invoke( instance.actual, value );
            } catch( InvocationTargetException ex ) {
                throw Errors.wrap( ex );
            } catch( IllegalAccessException ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            throw new UnwritablePropertyException( this, "property is not writable" );
        }
    }

    public void setRawValue( ModelInstance instance, Object value ) {
        // instance.module( AccessTracking.class ).perform( "raw", this.name );
        
        try {
            this.field.set( instance.actual, value );
            
        } catch( IllegalAccessException ex ) {
            throw new ModelException( "Illegal access setting " + this.getFullName(), ex );
        }
    }
    
    public void deleteRawValue( ModelInstance instance ) {
        //TODO: gestire campi primitivi
        this.setRawValue( instance, this.undefined.undefinedValue( instance ) );
    }
    
    public Field getField(){
        return this.field;
    }
    
    public java.beans.PropertyDescriptor getPropertyDescriptor(){
        return this.property;
    } 
    
    public String toString() {
        return this.getFullName() + " classic property";
    }
    
}