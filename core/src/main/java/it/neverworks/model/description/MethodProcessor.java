package it.neverworks.model.description;

public interface MethodProcessor {    
    void processMethod( MethodDescriptor method );
}