package it.neverworks.model.description;

import it.neverworks.model.ModelException;

public class PropertyException extends ModelException {

    protected PropertyDescriptor property;
    
    public PropertyException( PropertyDescriptor property, String error ) {
        super( "(" + property.getFullName() + ") " + error );
        this.property = property;
    }

    public PropertyException( PropertyDescriptor property, String error, Throwable ex ) {
        super( "(" + property.getFullName() + ") " + error, ex );
        this.property = property;
    }
    
    public PropertyDescriptor getProperty() {
        return this.property;
    }
}