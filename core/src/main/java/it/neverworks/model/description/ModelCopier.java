package it.neverworks.model.description;

import java.util.Map;
import java.util.Set;
import java.util.HashSet;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.model.features.Copy;
import it.neverworks.model.features.Duplicate;

public class ModelCopier {
    private ModelInstance instance;
    private Set<String> names;
    private boolean allNamesIncluded;
    private boolean deep = false;
    
    public ModelCopier( ModelInstance instance ){
        this.instance = instance;
        this.allNamesIncluded = true;
        this.names = new HashSet( instance.descriptor().names() );
    }

    public ModelCopier( Object object ){
        this( ModelInstance.of( object ) );
    }
    
    public ModelCopier deep() {
        this.deep = true;
        return this;
    }
    
    public ModelCopier shallow() {
        this.deep = false;
        return this;
    }

    public ModelCopier including( Iterable<String> names ) {
        if( this.allNamesIncluded ) {
            this.names.clear();
            this.allNamesIncluded = false;
        }
        
        for( String name: names ) {
            this.names.add( name );
        }
        return this;
    }    
    
    public ModelCopier including( String... names ) {
        return including( Collections.list( names ) );
    }
    
    public ModelCopier excluding( Iterable<String> names ) {
        for( String name: names ) {
            if( this.names.contains( name ) ) {
                this.names.remove( name );
            }
        }
        return this;
    }
    
    public ModelCopier excluding( String... names ) {
        return excluding( Collections.list( names ) );
    }    

    public <T> T same() {
        return (T) to( instance.getType() );
    }
    
    public <T> T to( Class<T> cls ) {
        if( Arguments.class.equals( cls ) ) {
            return (T) toArguments();
        } else {
            return to( (T) Reflection.newInstance( cls ) );
        }
    }
    
    public <T> T to( Object targetObject ) {
        return to( ModelInstance.of( targetObject ) );
    }

    public <T> T to( ModelInstance targetInstance ) {
        ModelDescriptor target = targetInstance.descriptor();
        for( String name: this.names ) {
            //TODO: livello di accesso piu' basso se meta e non virtual
            if( this.instance.property( name ).getReadable()
                && target.has( name ) 
                && target.property( name ).getWritable() ) {
                
                Object value = this.instance.get( name );
                if( this.deep ) {
                    value = duplicate( value );
                }
                targetInstance.set( name, value );
            }
        }
        return (T) targetInstance.actual();
    }
    
    public Arguments toArguments() {
        Arguments target = new Arguments();
        for( String name: this.names ) {
            //TODO: livello di accesso piu' basso se meta e non virtual
            if( this.instance.property( name ).getReadable() ) {
                
                Object value = this.instance.get( name );
                if( this.deep ) {
                    value = duplicate( value );
                }
                target.set( name, value );
            }
        }
        return target;
    }

    public <T> T from( Object sourceObject ) {
        if( sourceObject instanceof Map ) {
            return from( (Map) sourceObject );
        } else if( Arguments.isParsable( sourceObject ) ) {
            return from( Arguments.parse( sourceObject ) );
        } else if( sourceObject != null ) {
            return from( ModelInstance.of( sourceObject ) );
        } else {
            return (T) this.instance.actual();
        }
    }

    public <T> T from( ModelInstance sourceInstance ) {
        ModelDescriptor source = sourceInstance.descriptor();
        for( String name: this.names ) {
            //TODO: livello di accesso piu' basso se meta e non virtual
            if( source.has( name )
                && source.property( name ).getReadable()
                && this.instance.has( name ) 
                && this.instance.property( name ).getWritable() ) {
                
                Object value = sourceInstance.get( name );
                if( this.deep ) {
                    value = duplicate( value );
                }
                this.instance.set( name, value );
            }
        }
        return (T) this.instance.actual();
    }
    
    public <T> T from( Map source ) {
        for( String name: this.names ) {
            //TODO: livello di accesso piu' basso se meta e non virtual
            if( source.containsKey( name )
                && this.instance.has( name ) 
                && this.instance.property( name ).getWritable() ) {
                
                Object value = source.get( name );
                if( this.deep ) {
                    value = duplicate( value );
                }
                this.instance.set( name, value );
            }
        }
        return (T) this.instance.actual();
    }
    
    public static Object duplicate( Object source ) {
        if( source != null ) {
            if( source instanceof Duplicate ) {
                return ((Duplicate) source).duplicateObject();
            } else {
                return copy( source ).to( source.getClass() );
            }
        } else {
            return null;
        }
    }

    public static Object duplicate( ModelInstance instance ) {
        if( instance.is( Duplicate.class ) ) {
            return ((Duplicate) instance.actual()).duplicateObject();
        } else {
            return copy( instance ).to( instance.getType() );
        }
    }

    public static ModelCopier copy( Object source ) {
        if( source instanceof Copy ) {
            return ((Copy) source).copyObject();
        } else {
            return new ModelCopier( source );
        }
    }

    public static ModelCopier copy( ModelInstance instance ) {
        if( instance.is( Copy.class ) ) {
            return ((Copy) instance.actual()).copyObject();
        } else {
            return new ModelCopier( instance );
        }
    }

}
