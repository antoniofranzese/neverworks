package it.neverworks.model.description;

import it.neverworks.model.ModelException;

public class DefendedPropertyViolation extends ModelException {
    public DefendedPropertyViolation( String message ) {
        super( message );
    }
}