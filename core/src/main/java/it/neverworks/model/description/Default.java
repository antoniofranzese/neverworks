package it.neverworks.model.description;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = DefaultMetadata.class )
public @interface Default {
    Class<?> value() default void.class;
    Class<? extends DefaultFactory> factory() default DefaultFactory.class;
}
