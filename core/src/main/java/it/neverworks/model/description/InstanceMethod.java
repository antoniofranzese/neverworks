package it.neverworks.model.description;

import java.lang.reflect.Method;

import static it.neverworks.language.each;
import it.neverworks.lang.Callable;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.lang.FluentList;
import it.neverworks.model.features.Invoke;

public class InstanceMethod<T> implements Invoke, Callable<T> {
    private Object instance;
    private Method[] methods;

    public static InstanceMethod lookup( Object obj, String name ) {
        if( obj != null && Strings.hasText( name ) ) {
            Method[] methods = Reflection.findMethods( obj.getClass(), name ).array( Method.class );
            if( methods.length > 0 ) {
                return new InstanceMethod( obj, methods );
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
    
    public InstanceMethod( Object instance, Method[] methods ) {
        this.instance = instance;
        this.methods = methods;
    }

    public InstanceMethod( Object instance, Iterable<Method> methods ) {
        this.instance = instance;
        this.methods = Collections.toArray( Method.class, methods );
    }

    public InstanceMethod( Object instance, FluentList<Method> methods ) {
        this.instance = instance;
        this.methods = methods.array( Method.class );
    }

    public T call( Object... arguments ) {
        return (T) invokeInstance( arguments );
    }
    
    public Object invokeInstance( Object... arguments ) {
        Method method = null;
        if( methods.length == 1 ) {
            method = methods[ 0 ];
        } else {
            if( arguments.length > 0 ) {
                Class[] types = each( arguments ).map( Reflection.CLASSES_MAPPER ).list().toArray( new Class[ arguments.length ] );
                method = Reflection.findMatchingMethod( instance.getClass(), methods[0].getName(), types );
            } else {
                method = Reflection.findMethod( instance.getClass(), methods[0].getName() );
            }
        }
        if( method != null ) {
            return Reflection.invokeMethod( instance, method, arguments );
        } else {
            throw new RuntimeException( 
                "Cannot found matching " + instance.getClass().getName() + "." + methods[0].getName() + 
                " method for: " + each( arguments ).map( Reflection.CLASSES_MAPPER ).list() 
            );
        }
        
    }
    
    public String toString() {
        return "Method '" + methods[0].getName() + "', bound to " + instance;
    }

}
