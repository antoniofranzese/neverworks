package it.neverworks.model.description;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( ElementType.FIELD )
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = UndefinedProcessor.class )
public @interface Undefined {
    Class<? extends UndefinedInspector> value();
}
