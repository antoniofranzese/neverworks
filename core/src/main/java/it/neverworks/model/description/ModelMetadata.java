package it.neverworks.model.description;

public interface ModelMetadata {
    public void meetModel( ModelDescriptor model );
    public void inheritModel( ModelDescriptor model );
}