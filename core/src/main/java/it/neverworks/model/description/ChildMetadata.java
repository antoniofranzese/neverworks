package it.neverworks.model.description;

public class ChildMetadata implements PropertyProcessor, PropertyMetadata, PropertyInitializer {
    
    private boolean adoptOnComplete = true;
    
    public void processProperty( MetaPropertyManager property ) {
        property.setChild( true );
        property.addInitializer( this );
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        if( property.metadata().contains( DefaultMetadata.class ) ) {
            adoptOnComplete = false;
        }
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( ChildMetadata.class ) ) {
            processProperty( property );
        }
    }

    public void initProperty( MetaPropertyManager property, ModelInstance instance ) {
        if( adoptOnComplete ) {
            adoptModel( instance, property );
        }
    }
    
    public void adoptModel(  ModelInstance instance, MetaPropertyManager property ) {
        // System.out.println( "Adopting " + property.getName() );
        Object current = instance.raw( property.getName() );
        if( current != null && current instanceof ChildModel ) {
            ((ChildModel) current).adoptModel( instance, property );
        }
    }

}