package it.neverworks.model.description;

import java.util.List;
import java.util.ArrayList;

public class ModelMetadataCollection implements ModelMetadata {
    
    private List<ModelMetadata> collection = new ArrayList<ModelMetadata>();
    private ModelDescriptor model = null;
    
    public void add( ModelMetadata metadata ) {
        this.collection.add( metadata );
        if( this.model != null ) {
            metadata.meetModel( model );
        }
    }
    
    public void meetModel( ModelDescriptor model ) {
        this.model = model;
        for( ModelMetadata metadata: this.collection ) {
            metadata.meetModel( model );
        }
    }
    
    public void inheritModel( ModelDescriptor model ) {
        for( ModelMetadata metadata: this.collection ) {
            metadata.inheritModel( model );
        }
    }
    
}
