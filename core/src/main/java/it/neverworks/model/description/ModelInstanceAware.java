package it.neverworks.model.description;

public interface ModelInstanceAware {
    void storeModelInstance( ModelInstance instance );
}