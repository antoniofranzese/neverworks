package it.neverworks.model.description;

import java.lang.reflect.Method;

import static it.neverworks.language.each;
import it.neverworks.lang.Callable;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.model.features.Invoke;

public class AbstractMethod<T> {
    private Class type;
    private Method[] methods;

    public AbstractMethod( Class type, Method[] methods ) {
        this.type = type;
        this.methods = methods;
    }
    
    public InstanceMethod<T> bind( Object object ) {
        return new InstanceMethod<T>( object, methods );
    }

    public String toString() {
        return "Method '" + methods[0].getName() + "', unbound";
    }

}
