package it.neverworks.model.description;

public interface ModelInstanceInitializer {
    void initModelInstance( ModelInstance instance );
}