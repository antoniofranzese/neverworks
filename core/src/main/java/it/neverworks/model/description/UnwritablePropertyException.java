package it.neverworks.model.description;

public class UnwritablePropertyException extends PropertyException {
    
    public UnwritablePropertyException( PropertyDescriptor property, String error ) {
        super( property, error );
    }
    
}