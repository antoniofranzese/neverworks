package it.neverworks.model.description;

import java.lang.reflect.Constructor;

import it.neverworks.model.ModelException;
import it.neverworks.lang.Reflection;

public class StandardDefaultFactory implements DefaultFactory, PropertyAware {
    
    private PropertyDescriptor property;
    private Class<?> source;
    private Constructor constructor;
    
    public StandardDefaultFactory( Class<?> source ) {
        this.source = source;
        this.constructor = Reflection.getConstructor( this.source );
        if( this.constructor == null ) {
            throw new ModelException( "Missing default constructor for " + source.getName() );
        }
    }
    
    public void setPropertyDescriptor( PropertyDescriptor property ) {
        this.property = property;
    }
    
    public Object getDefaultValue( ModelInstance instance ) {
        return Reflection.newInstance( this.constructor );
    }

}