package it.neverworks.model.description;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention( RetentionPolicy.RUNTIME )
public @interface ModelAnnotation {
    Class<? extends ModelProcessor> processor();
}
