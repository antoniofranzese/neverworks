package it.neverworks.model.description;

import it.neverworks.lang.AnnotationInfo;

public class WatchMetadata implements PropertyProcessor, PropertyMetadata {
    
    /* package */ WatchChanges changes;
    /* package */ String name;
    
    public WatchMetadata( AnnotationInfo<Watch> info ) {
        this.changes = info.getAnnotation().changes();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        this.name = property.getName();
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( WatchMetadata.class ) ) {
            processProperty( property );
        }
    }

    public WatchChanges getChanges(){
        return this.changes;
    }
 
    public String getName(){
        return this.name;
    }
    
    
}