package it.neverworks.model.description;

public interface PropertyMetadata {
    public void meetProperty( MetaPropertyManager property );
    public void inheritProperty( MetaPropertyManager property );
}