package it.neverworks.model.description;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public abstract class GetterInterceptor<T> implements Interceptor {

    public Object invoke( Invocation invocation ) {
        Getter<T> getter = invocation.argument( 0 );
        return get( getter );
    }
    
    public abstract T get( Getter<T> getter );

}
