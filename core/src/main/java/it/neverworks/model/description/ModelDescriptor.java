package it.neverworks.model.description;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.annotation.Annotation;

import it.neverworks.lang.annotation.AnnotatedClass;
import it.neverworks.lang.annotation.AnnotatedMethod;
import it.neverworks.lang.annotation.AnnotationManager;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.Application;
import it.neverworks.lang.ClassScanner;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.lang.MissingMethodException;
import it.neverworks.model.Property;
import it.neverworks.model.ModelException;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.expressions.ObjectQuery;

@SuppressWarnings( "unchecked" )
public class ModelDescriptor {
    
    private static Map<Class<?>, ModelDescriptor> classRegistry = new HashMap<>();
    
    private static List<PropertyPostProcessor> propertyPostProcessors;

    static {
        try( ClassScanner scan = Application.scanner() ) {
            propertyPostProcessors = scan.implementing( PropertyPostProcessor.class )
                .map( cls -> (PropertyPostProcessor) Reflection.newInstance( cls ) )
                .list();
        };
    }        
    
    public static ModelDescriptor of( Object model ) {
        if( model != null ) {
            return of( model.getClass() );
        } else {
            throw new ModelException( "Cannot get metadata on null model" );
        }
    }
    
    public static ModelDescriptor of( Class<?> modelClass ) {
        if( modelClass != null ) {
            
            if( !classRegistry.containsKey( modelClass ) ) {
                synchronized( classRegistry ) {
                    if( !classRegistry.containsKey( modelClass ) ) {
                        classRegistry.put( modelClass, new ModelDescriptor( modelClass ) );
                    }
                }
            }
            
            return classRegistry.get( modelClass );
            
        } else {
            throw new ModelException( "Cannot get metadata on null model class" );
        }
        
    }
    
    protected Class<?> target;
    protected Map properties = new HashMap();
    protected Map methods = new HashMap();
    protected ModelMetadataRegistry metadata;
    protected List<ModelInstanceInitializer> initializers;
    /*package*/ boolean watchable = false;
    /*package*/ boolean hasRetrieve = false;
    /*package*/ boolean hasStore = false;
    
    private ModelDescriptor( Class<?> target ) {
        this.target = target;
        this.metadata = new ModelMetadataRegistry( this );
        this.hasRetrieve = Retrieve.class.isAssignableFrom( target );
        this.hasStore = Store.class.isAssignableFrom( target );

        ModelDescriptor base = null;
        if( target.getSuperclass() != Object.class ) {
            base = ModelDescriptor.of( target.getSuperclass() );
        }

        for( Field field: target.getDeclaredFields() ) {
            String name = field.getName();

            //Meta Properties
            if( field.getAnnotation( Property.class ) != null ) {
                MetaPropertyManager prop;
                
                if( base != null && base.names().contains( name ) && base.property( name ) instanceof MetaPropertyManager ) {
                    // Redefined Meta Property
                    prop = new MetaPropertyManager( name, this, field, (MetaPropertyManager)base.property( name ) );
                } else {
                    // New Meta Property
                    prop = new MetaPropertyManager( name, this, field );
                }
                
                properties.put( prop.getName(), prop );


            } else {
                
                // Defended meta property fields
                if( base != null 
                    && base.has( name ) 
                    && base.property( name ).metadata().contains( DefendedMetadata.class )
                    && field.getAnnotation( Inherited.class ) == null ) {
                
                        throw new DefendedPropertyViolation( "Field " + field.getDeclaringClass().getName() + "." + field.getName() + " hides defended property " +  base.property( name ).getFullName() );

                } else if( field.getAnnotation( Classic.class ) != null ) {
                    java.beans.PropertyDescriptor classicDescriptor = Properties.getDescriptor( target, field.getName() );
                    // for( java.beans.PropertyDescriptor descriptor: Properties.getDescriptors( target ) ) {
                    //     if( descriptor.getName().equals( field.getName() ) ) {
                    //         classicDescriptor = descriptor;
                    //         break;
                    //     }
                    // }
            
                    if( classicDescriptor != null ) {
                        ClassicPropertyManager prop = new ClassicPropertyManager( name, this, classicDescriptor, field );
                        properties.put( prop.getName(), prop );
                    } else {
                        throw new ModelException( "Cannot retrieve JavaBean PropertyDescriptor for @Classic property " + field.getDeclaringClass().getName() + "." + field.getName() );
                    }
                }
            }
        }

        //Inherit Meta Properties
        if( base != null ) {
            for( String name: base.names() ) {
                if( !properties.containsKey( name ) && base.property( name ) instanceof MetaPropertyManager ) {
                    MetaPropertyManager prop = new MetaPropertyManager( name, this, (MetaPropertyManager)base.property( name ) );
                    properties.put( prop.getName(), prop );
                }
            }
        }
        
        //Bean properties
        for( java.beans.PropertyDescriptor descriptor: Properties.getDescriptors( target ) ) {
            if( !properties.containsKey( descriptor.getName() ) && allowProperty( target, descriptor ) ) {
                SealedPropertyManager prop = new SealedPropertyManager( descriptor.getName(), this, descriptor );
                properties.put( prop.getName(), prop );
            }
        }

        //Class annotations
        processAnnotations();

        // Base metadata inheritance
        if( base != null ) {
            for( ModelMetadata baseMeta: base.metadata() ) {
                baseMeta.inheritModel( this );
            }
        }
        
        //System.out.println( "Model meeting " + this  );
        
        // Property meeting
        for( PropertyDescriptor property: properties() ) {
            ((BasePropertyManager) property).meetModel( this );
        }
        
        // Model meeting
        metadata.meet();

        //System.out.println( "End Model meeting " + this  );

    }
    
    protected boolean allowProperty( Class<?> target, java.beans.PropertyDescriptor descriptor ) {
        if( "class".equals( descriptor.getName() ) ) {
            return false;
        }
        
        if( net.sf.cglib.proxy.Factory.class.isAssignableFrom( target ) ) {
            if( "callback".equals( descriptor.getName() ) || "callbacks".equals( descriptor.getName() ) ) {
                return false;
            }
        }
        
        return true;
    }
    
    protected void processAnnotations() {	
        AnnotatedClass annotated = AnnotationManager.getAnnotatedClass( this.target );

        for( Annotation annotation: annotated.getAllAnnotations() ) {
        
            if( annotation != null ) {
                this.metadata.add( annotation );
                // Accetta le annotazioni annotate a loro volta con ModelAnnotation
                ModelAnnotation modelAnnotation = annotation.annotationType().getAnnotation( ModelAnnotation.class );
                if( modelAnnotation != null ) {
                    // L'annotazione deve avere un processor associato
                    Class processorClass = modelAnnotation.processor();
                    ModelProcessor processor = null;
                    if( processorClass != null ) {
                
                        // Verifichiamo se il processor ha un costruttore che accetta l'annotazione originaria
                        // Constructor annotationCtor = Reflection.getConstructor( processorClass, annotation.annotationType() );
                        // if( annotationCtor != null ) {
                        //     processor = (ModelProcessor)Reflection.newInstance( annotationCtor, annotation );
                        Constructor annotationCtor = Reflection.getConstructor( processorClass, AnnotationInfo.class );
                        if( annotationCtor != null ) {
                            processor = (ModelProcessor)Reflection.newInstance( annotationCtor, new AnnotationInfo( annotation, this.target ) );
                
                        } else {
                            // Verifichiamo se il processor ha un costruttore senza parametri
                            Constructor emptyCtor = Reflection.getConstructor( processorClass );
                            if( emptyCtor != null ) {
                                processor = (ModelProcessor)Reflection.newInstance( emptyCtor );
                            } else {
                                throw new ModelException( this.target.getName() + ": Cannot process " + annotation.annotationType().getName() + ": missing compatible processor constructor" );
                            }
                        }
    
                    } else {
                        throw new ModelException( this.target.getName() + ": Cannot process " + annotation.annotationType().getName() + ": missing processor class" );
                    }
    
                    // Esecuzione del processor
                    if( processor != null ) {
                        processor.processModel( this );
                    }
            
                }
                
            } else {
                // TODO: Bug di AnnotationManager, sostituirlo con una nuova realizzazione
                //System.out.println( "NULL ANNOTATION!!!" );
            }

        }
        
    }
    
    public Class<?> getTarget(){
        return this.target;
    }

    public Class<?> target(){
        return this.target;
    }

    public String targetName(){
        return Objects.className( this.target );
    }
    
    public FluentList<PropertyDescriptor> properties(){
        return new FluentArrayList<PropertyDescriptor>( this.properties.values() );
    }

    public FluentList<PropertyManager> managers(){
        return new FluentArrayList<PropertyManager>( this.properties.values() );
    }

    public ObjectQuery<PropertyDescriptor> query() {
        return new ObjectQuery( this.properties.values() );
    }
    
    public boolean has( String name ) {
        return this.properties.containsKey( name );
    }

    public boolean has( String name, Class<?> type ) {
        return has( name ) && property( name ).is( type );
    }

    public boolean has( PropertyDescriptor descriptor ) {
        return this.properties.containsValue( descriptor );
    }
    
    public boolean knows( String name ) {
        return has( name ) || does( name );
    }
    
    public FluentList<String> names() {
        return new FluentArrayList<String>( this.properties.keySet() );
    }
    
    public ModelMetadataRegistry metadata() {
        return this.metadata;
    }

    public ModelMetadataRegistry getMetadata() {
        return this.metadata;
    }

    public <T extends ModelMetadata> T metadata( Class<T> metaclass ) {
        return metadata.instance( metaclass );
    }
    
    public <T extends Annotation> T annotation( Class<T> annoclass ) {
        return metadata.annotation( annoclass );
    }

    public boolean retains( Class metaclass ) {
        return metadata.contains( metaclass );
    }

    public boolean watchable() {
        return this.watchable;
    }
    
    public PropertyDescriptor property( String name ) {
        if( name != null ) {
            if( this.properties.containsKey( name ) ) {
                return (PropertyDescriptor)this.properties.get( name );
            } else {
                throw new MissingPropertyException( this.target, name );
            }
        } else {
            throw new ModelException( "Cannot retrieve null property on " + this.target.getName() );
        }
    }

    public PropertyManager manager( String name ) {
        PropertyDescriptor descriptor = property( name );
        if( descriptor instanceof PropertyManager ) {
            return (PropertyManager) descriptor;
        } else {
            throw new ModelException( "Descriptor is not a PropertyManager: " + descriptor );
        }
    }
    
    public boolean does( String name ) {
        try {
            method( name );
            return true;
        } catch( MissingMethodException ex ) {
            return false;
        }
    }
    
    public <T> AbstractMethod<T> method( String name ) {
        if( name != null ) {
            if( ! methods.containsKey( name ) ) {
                synchronized( methods ) {
                    if( ! methods.containsKey( name ) ) {
                        Method[] methods = Reflection.findMethods( this.target, name ).array( Method.class );
                        if( methods.length > 0 ) {
                            this.methods.put( name, new AbstractMethod( this.target, methods ) );
                        } else {
                            throw new MissingMethodException( this.target, name );
                        }
                    }
                }
            }
            return (AbstractMethod) this.methods.get( name );

        } else {
            throw new ModelException( "Cannot retrieve null method on " + this.target.getName() );
        }
    }
    
    public void addInitializer( ModelInstanceInitializer initializer ) {
        if( this.initializers == null ) {
            this.initializers = new ArrayList<ModelInstanceInitializer>();
        }
        this.initializers.add( initializer );
    }
    

    public void init( ModelInstance instance ) {
        for( PropertyManager manager: (Collection<PropertyManager>) this.properties.values() ) {
            manager.init( instance );
        }
        
        if( this.initializers != null ) {
            for( ModelInstanceInitializer initializer: this.initializers ) {
                initializer.initModelInstance( instance );
            }
        }
    }

    public String toString() {
        return "ModelDescriptor<" + this.target.getName() + ">";
    }

}