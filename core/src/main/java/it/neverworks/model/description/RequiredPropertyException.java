package it.neverworks.model.description;

public class RequiredPropertyException extends PropertyException {
    
    public RequiredPropertyException( PropertyDescriptor property ) {
        super( property, "Property is required" );
    }

    public RequiredPropertyException( PropertyDescriptor property, String error ) {
        super( property, error );
    }
    
    public RequiredPropertyException( Object model, String property ) {
        super( ModelInstance.of( model ).descriptor().property( property ), "Property is required" );
    }

    public RequiredPropertyException( Object model, String property, String error ) {
        super( ModelInstance.of( model ).descriptor().property( property ), error );
    }
    
}