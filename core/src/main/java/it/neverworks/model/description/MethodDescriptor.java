package it.neverworks.model.description;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Collections;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Proxy;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Pointcut;
import it.neverworks.aop.MethodPointcut;
import it.neverworks.bytecode.RawMethod;
import it.neverworks.bytecode.FrontMethod;
import it.neverworks.lang.Reflection;
import it.neverworks.model.ModelException;

@SuppressWarnings( "unchecked" )
public class MethodDescriptor implements Proxy {

    private static Map<Class<?>, Registry> classRegistry = new HashMap();

    private static class Registry {
        private Map<Method, MethodDescriptor> methods = new HashMap();
    }

    public static MethodDescriptor of( Class owner, String name, Class... types ) {
        
        Method method = Reflection.findMethod( owner, name, types );
        if( method != null ) {
            return of( method );
        } else {
            throw new RuntimeException( "No " + name + " method found in " + owner.getName() + " with signature:" + Arrays.asList( types ) );
        }
        
    }
    
    public static MethodDescriptor of( it.neverworks.lang.Method method ) {
        return of( method.getActual() );
    }
    
    private static Registry registry( Class cls ) {
        if( !classRegistry.containsKey( cls ) ) {
            synchronized( classRegistry ) {
                if( !classRegistry.containsKey( cls ) ) {
                    classRegistry.put( cls, new Registry() );
                }
            }
        }
        return classRegistry.get( cls );
    }

    public static MethodDescriptor of( Method method ) {
        //System.out.println( "Locating descriptor for " + method );
        Map<Method, MethodDescriptor> methodRegistry = registry( method.getDeclaringClass() ).methods;
        
        if( !methodRegistry.containsKey( method ) ) {
            synchronized( methodRegistry ) {
                if( !methodRegistry.containsKey( method ) ) {
                    //System.out.println( "Creating descriptor for " + method );
                    MethodDescriptor descriptor = null;
                    
                    // Front Method
                    if( method.getAnnotation( FrontMethod.class ) != null ) {
                        //System.out.println( "is front" );
                        FrontMethod annotation = method.getAnnotation( FrontMethod.class );
                        Method raw = Reflection.findMethod( method.getDeclaringClass(), annotation.peer(), method.getParameterTypes() );
                        descriptor = new MethodDescriptor( method, raw );
                    
                    // Raw Method
                    } else if( method.getAnnotation( RawMethod.class ) != null ) {
                        //System.out.println( "is raw" );
                        RawMethod annotation = method.getAnnotation( RawMethod.class );
                        Method front = Reflection.findMethod( method.getDeclaringClass(), annotation.peer(), method.getParameterTypes() );

                        // Condivide il descriptor con quello del metodo front
                        if( methodRegistry.containsKey( front ) ) {
                            descriptor = methodRegistry.get( front );
                        } else {
                            descriptor = new MethodDescriptor( front, method );
                            methodRegistry.put( front, descriptor );
                        }

                    // Plain Method    
                    } else {
                        //System.out.println( "is plain" );
                        
                        descriptor = new MethodDescriptor( method );
                    }
                    
                    
                    methodRegistry.put( method, descriptor );
                }
            }
        }
        
        return methodRegistry.get( method );
        
    }
    
    private Method front;
    private Method raw;
    
    private List<Interceptor> chain;
    private Pointcut pointcut = null;
    private boolean parsed = false;
    
    private MethodDescriptor( Method method ) {
        this( method, method );
    }
    
    private MethodDescriptor( Method front, Method raw ) {
        this.front = front;
        this.raw = raw;
        
        this.chain = new ArrayList<Interceptor>();
        this.processAnnotations();
        
        //TODO: raccogliere le indicazioni di posizionamento ed elaborarle alla fine
        
        if( front != raw ) {
            this.pointcut = new MethodPointcut( raw );
            for( Interceptor interceptor: this.chain ) {
                this.pointcut.add( interceptor );
            }
        }
    }

    protected void processAnnotations() {	

        for( Annotation annotation: raw.getAnnotations() ) {
        
            // Accetta le annotazioni annotate a loro volta con ModelAnnotation
            MethodAnnotation methodAnnotation = annotation.annotationType().getAnnotation( MethodAnnotation.class );
            if( methodAnnotation != null ) {
                
                // L'annotazione deve avere un processor associato
                Class processorClass = methodAnnotation.processor();
                MethodProcessor processor = null;
                if( processorClass != null ) {
                    
                    // Verifichiamo se il processor ha un costruttore che accetta l'annotazione originaria
                    Constructor annotationCtor = Reflection.getConstructor( processorClass, AnnotationInfo.class );
                    if( annotationCtor != null ) {
                        processor = (MethodProcessor)Reflection.newInstance( annotationCtor, new AnnotationInfo( annotation, raw ) );
                    
                    } else {
                        // Verifichiamo se il processor ha un costruttore senza parametri
                        Constructor emptyCtor = Reflection.getConstructor( processorClass );
                        if( emptyCtor != null ) {
                            processor = (MethodProcessor)Reflection.newInstance( emptyCtor );
                        } else {
                            throw new ModelException( raw.getName() + ": Cannot process " + annotation.annotationType().getName() + ": missing compatible processor constructor" );
                        }
                    }
        
                } else {
                    throw new ModelException( raw.getName() + ": Cannot process " + annotation.annotationType().getName() + ": missing processor class" );
                }
        
                // Esecuzione del processor
                if( processor != null ) {
                    if( processor instanceof DeferredMethodProcessor ) {
                        ModelDescriptor.of( raw.getDeclaringClass() ).metadata( ModelMethodMetadata.class ).addDeferral( this, processor );
                        
                    } else {
                        processor.processMethod( this );
                    }
                }
                
            }
        }
        
    }
    
    public Object invoke( Object instance, Object... params ) {
        try {
            if( this.pointcut != null && this.pointcut.size() > 0 ) {
                // System.out.println( "Pointcut invoke " + raw );
                return this.pointcut.invoke( instance, params );
            } else {
                // System.out.println( "Plain invoke " + raw );
                return raw.invoke( instance, params );
            }
        } catch( InvocationTargetException ex ) {
            throw Errors.wrap( ex );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public class InterceptorPlacer {
        private Interceptor interceptor;
        private MethodDescriptor descriptor;
        private List<Interceptor> chain;
        
        public InterceptorPlacer( MethodDescriptor descriptor, Interceptor interceptor ) {
            this.descriptor = descriptor;
            this.interceptor = interceptor;
            this.chain = descriptor.chain;
        }
        
        public MethodDescriptor before( Class<? extends Interceptor>... classes ) {
            int index = chain.size();
            
            for( int i = 0; i < chain.size(); i++ ) {
                for( Class<? extends Interceptor> cls: classes ) {
                    if( cls.isAssignableFrom( chain.get( i ).getClass() ) && i < index ) {
                        index = i;
                    }
                }
            }

            chain.add( index, interceptor );

            return descriptor;
        }

        public MethodDescriptor after( Class<? extends Interceptor>... classes ) {
            int index = 0;
            
            for( int i = 0; i < chain.size(); i++ ) {
                for( Class<? extends Interceptor> cls: classes ) {
                    if( cls.isAssignableFrom( chain.get( i ).getClass() ) && i > index ) {
                        index = i;
                    }
                }
            }

            chain.add( chain.size() > 0 ? index + 1 : 0, interceptor );
            
            return descriptor;
        }

        public MethodDescriptor first() {
            chain.add( 0, interceptor );
            return descriptor;
        }

        public MethodDescriptor last() {
            chain.add( interceptor );
            return descriptor;
        }
        
    }
    
    public InterceptorPlacer place( Interceptor interceptor ) {
        return new InterceptorPlacer( this, interceptor );
    }

    public boolean contains( Class<? extends Interceptor> cls ) {
        for( Interceptor interceptor: this.chain ) {
            if( cls.isAssignableFrom( interceptor.getClass() ) ) {
                return true;
            }
        }
        return false;
    }
    
    public Method front(){
        return this.front;
    }
    
    public Method raw(){
        return this.raw;
    }
    
    public Method actual(){
        return this.raw;
    }
    
    public Method getActual(){
        return this.raw;
    }
    
    public boolean intercepted() {
        return this.front != this.raw;
    }
    
    public String toString() {
        String chainLog = null;
        if( intercepted() ) {
            List<String> names = new ArrayList<String>();
            for( Interceptor interceptor: this.chain ) {
                names.add( interceptor.getClass().getSimpleName() );
            }
            chainLog = " chain=" + Collections.join( names, "," );
        } else {
            chainLog = "";
        }
        return "[ModelDescriptor " + front.getName() + ( intercepted() ? "/" + raw.getName() : "" ) + chainLog + "]";
    }
}