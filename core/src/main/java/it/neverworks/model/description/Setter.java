package it.neverworks.model.description;

public class Setter<T> extends Getter<T> {
    
    protected T value;
    /*package*/ Boolean changed = null;
    
    public Setter( ModelInstance instance, PropertyManager property, T value ) {
        super( instance, property );
        this.value = value;
        this.performed = false;
    }
    
    public T get() {
        return (T)property.getDirectValue( instance );
    }

    public T get( Object value ) {
        return (T)property.process( this.instance, value );
    }

    public T value() {
        return value;
    }
    
    public T getValue() {
        return value;
    }
    
    public T set() {
        return set( value );
    }
    
    public T set( T value ) {
        performed = true;
        if( this.invocation != null && ! this.invocation.terminated() ) {
            invocation.invoke( new Setter( instance, property, value ) );
        } else {
            property.setDirectValue( instance, value );
        }
        return raw();
    }
    
    public void setValue( T value ) {
        this.value = value;
    }
    
    public void decline() {
        performed = true;
        this.ignore();
    }
    
    public boolean performed() {
        return performed;
    }
    
    public void setChanged( boolean changed ) {
        this.changed = changed;
    }
    
    public Setter change() {
        this.changed = true;
        return this;
    }

    public Setter touch() {
        this.changed = true;
        return this;
    }
    
    public Setter ignore() {
        this.changed = false;
        return this;
    }
}