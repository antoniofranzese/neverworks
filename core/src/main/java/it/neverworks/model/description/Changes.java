package it.neverworks.model.description;

import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.Collections;


public class Changes implements Iterable<String>{
    
    protected Set<String> properties;
    protected ModelInstance instance;
    
    public Changes( ModelInstance instance ) {
        this( instance, new HashSet<String>() );
    }

    private Changes( ModelInstance instance, Set<String> properties ) {
        this.instance = instance;
        this.properties = properties;
    }

    public Changes add( String name ) {
        this.properties.add( name );
        return this;
    }
    
    public Changes remove( String name ) {
        this.properties.remove( name );
        return this;
    }
    
    public Changes clear() {
        this.properties.clear();
        return this;
    }

    public Changes retain( String... names ) {
        this.properties.retainAll( Collections.<String>toList( names ) );
        return this;
    }
    
    public boolean contains( String name ) {
        return this.properties.contains( name );
    }
    
    public boolean containsAny( String... names ) {
        for( String name: names ) {
            if( this.properties.contains( name ) ) {
                return true;
            }
        }
        return false;
    }
    
    public Changes replace( String replacing, String replacement ) {
        if( this.properties.contains( replacing ) ) {
            this.properties.remove( replacing );
            this.properties.add( replacement );
        }
        return this;
    }
    
    public Changes cascaded( String name ) {
        if( this.instance != null && this.instance.raw( name ) != null ) {
            return ModelInstance.of( this.instance.raw( name ) ).changes();
        } else {
            return new Changes( null );
        }
    }
    
    public Changes details( String... names ) {
        for( String name: names ) {
            if( !contains( name ) && this.instance != null && this.instance.raw( name ) != null ) {
                for( String change: ModelInstance.of( this.instance.raw( name ) ).changes() ) {
                    this.add( name + "." + change );
                }
            }
        }
        return this;
    }
    
    public Changes whole( String... names ) {
        for( String name: names ) {
            if( this.cascaded( name ).size() > 0 ) {
                this.add( name );   
            }
        }
        return this;
    }
    
    public Iterator<String> iterator() {
        return this.properties.iterator();
    }

    public int size() {
        return this.properties.size();
    }
    
    public Changes clone() {
        return new Changes( this.instance, new HashSet<String>( this.properties ) );
    }
    
    public String toString() {
        return properties.toString();
    }
    
    public List<String> toList() {
        ArrayList<String> v = new ArrayList<String>( this.properties.size() );
        v.addAll( this.properties );
        return v;
    }
}