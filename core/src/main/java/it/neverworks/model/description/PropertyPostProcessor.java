package it.neverworks.model.description;

public interface PropertyPostProcessor {    
    void postProcessProperty( MetaPropertyManager property );
}