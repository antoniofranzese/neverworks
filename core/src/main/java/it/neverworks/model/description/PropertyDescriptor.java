package it.neverworks.model.description;

import java.lang.annotation.Annotation;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.TypeDefinition;

public interface PropertyDescriptor {
    
    ModelDescriptor getModel();
    TypeDefinition getType();
    Class getStorageType();

    String getFullName();
    String getName();
    Converter getConverter();
    boolean getReadable();
    boolean getWritable();
    boolean getRequired();
    boolean getChild();
    DefaultFactory getDefault();
    UndefinedInspector getUndefined();
    ModelDescriptor getOwner();
    boolean getInherited();
    boolean getProxable();
    boolean getVirtual();
    PropertyMetadataRegistry metadata();
    default PropertyMetadataRegistry getMetadata() {
        return metadata();
    }
    <T extends PropertyMetadata> T metadata( Class<T> metaclass );
    <T extends Annotation> T annotation( Class<T> annotationClass );
    boolean retains( Class metaclass );

    Object getFrontValue( ModelInstance instance );
    void setFrontValue( ModelInstance instance, Object value );
    
    Object get( Object object );
    void set( Object object, Object value );
    
    Object convert( Object value );
    Object convert( ModelInstance instance, Object value );
    Object process( ModelInstance instance, Object value );
    Object process( Object object );
    boolean accepts( Object value );
    boolean permits( Object value );

    boolean is( Class type );
    
    default boolean isReadable() {
        return this.getReadable();
    }
    
    default boolean isWritable() {
        return this.getWritable();
    }
    
    default boolean isRequired() {
        return this.getRequired();
    }
    
    default boolean isChild() {
        return this.getChild();
    }
    
    default boolean isInherited() {
        return this.getInherited();
    }
    
    default boolean isProxable() {
        return this.getProxable();
    }
    
    default boolean isVirtual() {
        return this.getVirtual();
    }
    
}