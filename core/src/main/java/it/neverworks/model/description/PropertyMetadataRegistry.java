package it.neverworks.model.description;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.lang.annotation.Annotation;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Reflection;
import it.neverworks.model.ModelException;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Inspect;

public class PropertyMetadataRegistry implements Iterable<PropertyMetadata>, Inspect, Retrieve {
    private Map metadata = new HashMap();
    private Map annotations = new HashMap();
    private PropertyDescriptor property;
    private boolean met = false;
    
    public PropertyMetadataRegistry( PropertyDescriptor property ) {
        this.property = property;
    }
    
    public void add( Annotation annotation ) {
        annotations.put( annotation.annotationType(), annotation );
    }
    
    public void add( PropertyMetadata meta ) {
        if( !metadata.containsKey( meta.getClass() ) ) {
            metadata.put( meta.getClass(), meta );
            if( this.met && this.property instanceof MetaPropertyManager ) {
                meta.meetProperty( (MetaPropertyManager) this.property );
            }
        } else {
            throw new ModelException( "Duplicate " + meta.getClass().getName() + " metadata in " + this.property.getFullName() + " property" );
        }
    }
    
    public <T extends Annotation> T annotation( Class<T> metaclass ) {
        if( annotations.containsKey( metaclass ) ) {
            return (T)annotations.get( metaclass );
        } else {
            throw new ModelException( "Missing " + metaclass.getName() + " annotation in " + this.property.getFullName() + " property" );
        }
    }
    
    public <T extends PropertyMetadata> T instance( Class<T> metaclass ) {
        if( ! metadata.containsKey( metaclass ) ) {
            synchronized( this ) {
                if( ! metadata.containsKey( metaclass ) ) {
                    add( (PropertyMetadata) Reflection.newInstance( metaclass ) );
                }                
            }
        }
        return (T)metadata.get( metaclass );
    }

    public boolean contains( Class metaclass ) {
        return metadata.containsKey( metaclass ) || annotations.containsKey( metaclass );
    }
    
    public Collection<PropertyMetadata> all() {
        return Collections.<PropertyMetadata>list( this.metadata.values() );
    }

    public Collection<Annotation> annotations() {
        return Collections.<Annotation>list( this.annotations.values() );
    }

    public boolean containsItem( Object item ) {
        if( item instanceof Class ) {
            return contains( (Class) item );
        } else if( item instanceof PropertyMetadata ) {
            return metadata.containsValue( item );
        } else if( item instanceof Annotation ) {
            return annotations.containsValue( item );
        } else {
            return false;
        }
    }

    public Object retrieveItem( Object key ) {
        if( key instanceof Class ) {
            if( PropertyMetadata.class.isAssignableFrom( (Class) key ) ) {
                return this.metadata.get( (Class) key );
            } else if( Annotation.class.isAssignableFrom( (Class) key ) ) {
                return this.annotations.get( (Class) key );
            }
        }
        throw new IllegalArgumentException( "Invalid metadata class key: " + Objects.repr( key ) );
    }
    
    public Iterator<PropertyMetadata> iterator() {
        return metadata.values().iterator();
    }
    
    public void meet() {
        if( ! this.met ) {
            this.met = true;
            if( this.property instanceof MetaPropertyManager ) {
                for( PropertyMetadata meta: this ) {
                    meta.meetProperty( (MetaPropertyManager) this.property );
                }
            }
        }
    }
    
}
