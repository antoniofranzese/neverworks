package it.neverworks.model.description;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;

import it.neverworks.aop.Proxy;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.InstanceConverter;

import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.WrongTypeException;

import it.neverworks.model.ModelException;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.AnnotationInfo;


@SuppressWarnings( "unchecked" )
public abstract class BasePropertyManager implements PropertyManager {
    
    protected ModelDescriptor model;
    protected String name;
    
    protected TypeDefinition type = null;
    protected Converter converter = null;
    protected boolean hasInstanceConverter = false;

    protected boolean readable = true;
    protected boolean writable = true;
    protected boolean proxable = false;
    protected boolean ignored = false; 
    protected boolean required = false;
    protected boolean child = false;
    protected DefaultFactory default_ = null;
    protected UndefinedInspector undefined;
    
    protected boolean virtual = false;
    protected boolean inherited = false;
    protected ModelDescriptor owner;
    
    protected PropertyMetadataRegistry metadata;
    
    protected BasePropertyManager() {}
        
    public BasePropertyManager( String name, ModelDescriptor model ) {
        this.name = name;
        this.metadata = new PropertyMetadataRegistry( this );

        this.model = model;
        this.owner = model;
    }
    
    public void initValue( ModelInstance instance, Object value ) {
        if( ! hasRawValue( instance ) ) {
            setRawValue( instance, value );
        }
    }
    
    protected PropertyProcessor detectProcessor( AnnotationInfo info ) {

        PropertyAnnotation propertyAnnotation = info.annotation().annotationType().getAnnotation( PropertyAnnotation.class );
        if( propertyAnnotation != null ) {
        
            // L'annotazione dovrebbe avere un processor associato
            Class processorClass = propertyAnnotation.processor();
            PropertyProcessor processor = null;
            if( processorClass != null ) {
            
                // Verifichiamo se il processor ha un costruttore che accetta l'annotazione originaria
                Constructor annotationCtor = Reflection.getConstructor( processorClass, AnnotationInfo.class );
                if( annotationCtor != null ) {
                    processor = (PropertyProcessor)Reflection.newInstance( annotationCtor, info );
            
                } else {
                    // Verifichiamo se il processor ha un costruttore senza parametri
                    Constructor emptyCtor = Reflection.getConstructor( processorClass );
                    if( emptyCtor != null ) {
                        processor = (PropertyProcessor)Reflection.newInstance( emptyCtor );
                    } else {
                        throw new PropertyException( this, "Cannot process " + info.annotation().annotationType().getName() + ": missing compatible processor constructor" );
                    }
                }

            } else {
                throw new PropertyException( this, "Cannot process " + info.annotation().annotationType().getName() + ": missing processor class" );
            }

            return processor;
        
        } else {
            return null;
        }
        
    }
    
    public void meetModel( ModelDescriptor model ) {
        
    }
    
    public void init( ModelInstance instance ) {

    }

    public ModelDescriptor getModel() {
        return this.model;
    }
    
    public TypeDefinition getType() {
        return this.type;
    }
    
    public void setType( TypeDefinition type ){
        this.type = type;
    }
    
    public String getFullName() {
        return this.model.getTarget().getName() + "." + this.name;
    }
    
    public String getName(){
        return this.name;
    }
    
    public Converter getConverter(){
        return this.converter;
    }
    
    public void setConverter( Converter converter ){
        this.converter = converter;
    }
    
    public boolean getReadable(){
        return this.readable;
    }
    
    public boolean getWritable(){
        return this.writable;
    }
    
    public boolean getRequired(){
        return this.required;
    }
    
    public void setRequired( boolean required ){
        this.required = required;
    }

    public boolean getChild(){
        return this.child;
    }
    
    public void setChild( boolean child ){
        this.child = child;
    }
    
    public DefaultFactory getDefault(){
        return this.default_;
    }

    public void setDefault( DefaultFactory default_ ){
        this.default_ = default_;
    }
    
    public UndefinedInspector getUndefined(){
        return this.undefined;
    }
    
    public void setUndefined( UndefinedInspector undefined ){
        if( undefined != null ) {
            this.undefined = undefined;
        } else {
            throw new ModelException( "Undefined definition cannot be null" );
        }
    }
    
    public boolean getProxable(){
        return this.proxable;
    }
    
    public void setProxable( boolean proxable ){
        this.proxable = proxable;
    }
    
    public ModelDescriptor getOwner(){
        return this.owner;
    }
    
    public boolean getInherited(){
        return this.inherited;
    }
    
    public boolean getVirtual(){
        return this.virtual;
    }
    
    public void setVirtual( boolean virtual ){
        this.virtual = virtual;
    }
    
    public boolean getIgnored(){
        return this.ignored;
    }
    
    public void setIgnored( boolean ignored ){
        this.ignored = ignored;
    }

    public String toString() {
        return this.getFullName() + ( this.inherited ? " inherited" : "" ) + " property";
    }
    
    public PropertyMetadataRegistry metadata() {
        return this.metadata;
    }

    public <T extends PropertyMetadata> T metadata( Class<T> metaclass ) {
        return this.metadata.instance( metaclass );
    }
    
    public <T extends Annotation> T annotation( Class<T> annotationClass ) {
        return this.metadata.annotation( annotationClass );
    }

    public boolean retains( Class metaclass ) {
        return this.metadata.contains( metaclass );
    }

    public PropertyMetadataRegistry getMetadata() {
        return this.metadata;
    }
    
    public Object convert( Object value ) {
        return convert( null, value );
    }
    
    public Object convert( ModelInstance instance, Object value ) {
        if( this.converter != null ) {
            if( hasInstanceConverter && instance != null ) {
                return ((InstanceConverter) this.converter).convert( instance, value );
            } else {
                return this.converter.convert( value );
            }
        } else {
            return value;
        }
    }
    
    public Object process( Object value ) {
        return process( null, value );
    }

    public Object process( ModelInstance instance, Object value ) {
        Object converted = convert( instance, value );
        
        if( converted == null || accepts( converted ) ) {
            return converted;
        } else {
            throw new WrongPropertyTypeException(
                "Wrong type for " + this.getFullName() +
                " property, <" + this.type.getName() + "> or convertible equivalent expected, " +
                Objects.repr( converted ) + " received"
            , this );
        }
    }

    public boolean permits( Object value ) {
        try {
            process( value );
            return true;
        } catch( Exception ex ) {
            return false;
        }
    }
    
    public boolean accepts( Object value ) {
        return this.type.isInstance( value );
    }

    public boolean is( Class type ) {
        if( this.type != null ) {
            return this.type.is( type );
        } else {
            return Object.class.equals( type );
        }
    }
}