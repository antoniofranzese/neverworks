package it.neverworks.model.description;

import java.lang.reflect.Constructor;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.AnnotationInfo;

public class ProcessProcessor implements PropertyProcessor {

    protected AnnotationInfo<Process> annotationInfo;

    public ProcessProcessor( AnnotationInfo<Process> info ) {
        this.annotationInfo = info;
    }
    
    public void processProperty( MetaPropertyManager property ) {
        for( Class processorClass: this.annotationInfo.getAnnotation().value() ) {
            if( PropertyProcessor.class.isAssignableFrom( processorClass ) ) {
                
                PropertyProcessor processor;
                
                Constructor annotationCtor = Reflection.getConstructor( processorClass, AnnotationInfo.class );
                if( annotationCtor != null ) {
                    processor = (PropertyProcessor)Reflection.newInstance( annotationCtor, this.annotationInfo );
            
                } else {
                    Constructor emptyCtor = Reflection.getConstructor( processorClass );
                    if( emptyCtor != null ) {
                        processor = (PropertyProcessor)Reflection.newInstance( emptyCtor );
                    } else {
                        throw new IllegalArgumentException( "Cannot process " + processorClass.getName() + " on " + property.getFullName() + ": missing compatible processor constructor" );
                    }
                }
                
                processor.processProperty( property );
            
            } else {
                throw new IllegalArgumentException( "Invalid PropertyProcessor on " + property.getFullName() + ": " + processorClass.getName() );
            }
        }
    }

}