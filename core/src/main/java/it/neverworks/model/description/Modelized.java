package it.neverworks.model.description;

public interface Modelized {
    ModelInstance retrieveModelInstance();
}