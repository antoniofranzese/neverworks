package it.neverworks.model.description;

public interface ModelAwarePropertyMetadata extends PropertyMetadata {
    public void meetModel( MetaPropertyManager property );
}