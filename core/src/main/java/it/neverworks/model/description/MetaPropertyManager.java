package it.neverworks.model.description;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.lang.Reflection;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.ModelException;
import it.neverworks.aop.Proxy;
import it.neverworks.aop.Pointcut;
import it.neverworks.aop.ProxyCall;
import it.neverworks.aop.MethodCall;
import it.neverworks.aop.Interceptor;
import it.neverworks.aop.MethodProxy;
import it.neverworks.model.converters.InstanceConverter;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class MetaPropertyManager extends BasePropertyManager {
    
    protected Field field;
    protected Proxy setter;
    protected Proxy getter;
    protected String[] getterNames;
    protected String[] setterNames;
    protected List<PropertyInitializer> initializers;
    protected MetaPropertyManager base;
    
    //Override
    public MetaPropertyManager( String name, ModelDescriptor model, Field field, MetaPropertyManager base ) {
        super( name, model );
        this.processField( field );

        if( base.metadata().contains( DefendedMetadata.class ) 
            && ! metadata().contains( InheritedMetadata.class ) ) {

            throw new DefendedPropertyViolation( "Property " + getFullName() + " hides defended property " + base.getFullName() );
        } 
        
        for( PropertyMetadata meta: base.metadata() ) {
            meta.inheritProperty( this );
        }

        this.base = base;
        this.metadata().meet();
        this.setupProperty();
    }

    //Inherit
    public MetaPropertyManager( String name, ModelDescriptor model, MetaPropertyManager original ) {
        super( name, model );
        this.owner = original.model;
        this.inherited = true;

        this.processField( original.field );

        this.metadata().meet();
        this.setupProperty();
    } 

    //Create
    public MetaPropertyManager( String name, ModelDescriptor model, Field field )    {
        super( name, model );
        this.processField( field );
        this.metadata().meet();
        this.setupProperty();
    }

    public Class getStorageType() {
        return this.field.getType();
    }

    private void processField( Field field ) {
        this.field = field;
        Reflection.makeAccessible( field );

        this.processAnnotations();

        // Type
        if( this.type == null ) {
            this.type = TypeFactory.build( this.field.getType() );
        }
        
        this.processTypeAnnotations();
            
        // Undefined
        if( this.undefined == null ) {
            this.undefined = new StandardUndefinedInspector();
        }
        
    }
    
    private void setupProperty() {
        
        // Accessors
        if( this.getterNames == null || this.getterNames.length == 0 ) {
            this.getterNames = new String[]{ "get" + this.name.substring( 0, 1 ).toUpperCase() + this.name.substring( 1 ) };
        }

        if( this.setterNames == null || this.setterNames.length == 0 ) {
            this.setterNames = new String[]{ "set" + this.name.substring( 0, 1 ).toUpperCase() + this.name.substring( 1 ) };
        }

        this.buildGetter();
        this.buildSetter();

        // Converter
        if( this.converter instanceof InstanceConverter ) {
            this.hasInstanceConverter = true;
        }
        
    }
    
    // Elabora le annotazioni poste direttamente sul campo
    protected void processAnnotations() {
        for( Annotation annotation: Annotations.all( field ) ) {
            processAnnotation( new AnnotationInfo( annotation, field ) );
            this.metadata.add( annotation );
        }
    }

    // Elabora le annotazioni poste sulla classe del tipo che abbiano un PropertyProcessor
    protected void processTypeAnnotations() {
        Class type = this.type.getRawType();
        if( type != null ) {
            for( Annotation classAnnotation: Annotations.all( type ) ) {
                processAnnotation( new AnnotationInfo( classAnnotation, type ) );
            }
        }
    }

    protected boolean processAnnotation( AnnotationInfo info ) {
        PropertyProcessor processor = detectProcessor( info );
        if( processor != null ) {
            processor.processProperty( this );
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void meetModel( ModelDescriptor model ) {
        //System.out.println( "Meet model: " + this );
        for( PropertyMetadata meta: metadata() ) {
            if( meta instanceof ModelAwarePropertyMetadata ) {
                ((ModelAwarePropertyMetadata) meta).meetModel( this );
            }
        }

    }
    
    @Override
    public void init( ModelInstance instance ) {
        if( this.initializers != null ) {
            for( PropertyInitializer initializer: this.initializers ) {
                initializer.initProperty( this, instance );
            }
        }
    }
    
    public void addInitializer( PropertyInitializer initializer ) {
        if( this.initializers == null ) {
            this.initializers = new ArrayList<PropertyInitializer>();
        }
        this.initializers.add( initializer );
    }
        
    public Object get( Object object ) {
        if( object instanceof Modelized ) {
            return getFrontValue( ((Modelized) object).retrieveModelInstance() );
        } else {
            throw new PropertyException( this, "Object is not a Modelized" );
        }
    }

    public Object getFrontValue( ModelInstance instance ) {
        if( this.readable ) {
            return this.getCheckedValue( instance );
        } else {
            throw new UnreadablePropertyException( this, "property is not readable" );
        }
    }
    
    public Object getCheckedValue( ModelInstance instance ) {
        Object value = this.getUncheckedValue( instance );
        if( value == null && this.required ) {
            throw new RequiredPropertyException( this );
        } else {
            return value;
        }
    }
    
    public Object getUncheckedValue( ModelInstance instance ) {
        //System.out.println( "Unchecked " + this.getFullName() );
        if( this.getter != null ) {
            
            Getter invocation = new Getter( instance, this );
            Object value = this.getter.invoke( instance.actual, invocation );

            //TODO: controllo di invocazione
            if( !invocation.performed ) {
                throw new PropertyException( this, "Getter must call get() or decline() on invocation argument" );
            }
            
            return value;

        } else {
            return this.getDirectValue( instance );
        }
    }
    
    public Object getDirectValue( ModelInstance instance ) {
        //TODO: entry point per interceptor
        //System.out.println( "Direct " + this.getFullName() );
        if( proxable ) {
            return processDefaultValue( instance, instance.getInstanceValue( this ) );
        } else {
            return processDefaultValue( instance, this.getRawValue( instance ) );
        }
        
    }
    
    protected Object processDefaultValue( ModelInstance instance, Object value ) {
        //System.out.println( "Default " + this.getFullName() );
        if( this.default_ != null && this.undefined.checkUndefined( instance, value ) ) {
            Object defaultValue =  this.default_.getDefaultValue( instance );
            try {
                value = process( defaultValue );
                
            } catch( IllegalArgumentException ex ) {
                throw new IllegalArgumentException( "Error on default value: " + ex.getMessage(), ex );
            }
            this.setDirectValue( instance, value );
        }
        return value;
    }
    
    public Object getRawValue( ModelInstance instance ) {
        try {
            //System.out.println( "Raw " + this.getFullName() +  " = " + this.field.get( instance.actual ) );
            return this.field.get( instance.actual );
        } catch( IllegalAccessException ex ) {
            throw new ModelException( "Illegal access getting " + this.getFullName(), ex );
        }
    }
    
    public boolean hasRawValue( ModelInstance instance ) {
        return !( this.undefined.checkUndefined( instance, this.getRawValue( instance ) ) );
    }
    
    
    public void set( Object object, Object value ) {
        if( object instanceof Modelized ) {
            setFrontValue( ((Modelized) object).retrieveModelInstance(), value );
        } else {
            throw new PropertyException( this, "Object is not a Modelized" );
        }
    }

    public void setFrontValue( ModelInstance instance, Object value ) {
        if( this.writable ) {
            this.setCheckedValue( instance, value );
        } else {
            throw new UnwritablePropertyException( this, "property is not writable" );
        }
    }
    
    public void setCheckedValue( ModelInstance instance, Object value ) {
        this.setUncheckedValue( instance, process( instance, value ) );
    }
    
    public void setUncheckedValue( ModelInstance instance, Object value ) {
        //System.out.println( "setUnchecked " + this.getFullName() + " " + instance.watching );
        Object previous = null;
        Boolean changed = null;
        boolean watching = ! virtual && instance.watching;
        
        if( watching ) {
            previous = this.getRawValue( instance );
        }
        
        if( this.setter != null ) {
            
 			Setter invocation = new Setter( instance, this, value );
            this.setter.invoke( instance.actual, invocation );
            
            if( !invocation.performed ) {
                throw new PropertyException( this, "Setter must call set() or decline() on invocation argument" );
            }
            
            changed = invocation.changed;
                
        } else {
            this.setDirectValue( instance, value );
        }
        
        if( !ignored && watching ) {
            if( changed == null ) {
                Object current = this.getRawValue( instance );
                if( !( current == null ? previous == null : current.equals( previous ) ) ) {
                    instance.touch( this.name );
                }
            } else {
                if( changed.booleanValue() ) {
                    instance.touch( this.name );
                }
            }
        }
            
    }
    
    public void setDirectValue( ModelInstance instance, Object value ) {
        if( proxable ) {
            instance.setInstanceValue( this, value );
        } else {
            this.setRawValue( instance, value );
        }
    }
    
    public void setRawValue( ModelInstance instance, Object value ) {
         
        Object previous = this.getRawValue( instance );

        if( previous != value ) {
            if( this.child ) {
                this.orphanValue( instance, previous );
            }
        
            try {
                this.setFieldValue( instance, value );
            
                if( this.child ) {
                    this.adoptValue( instance, value );
                }
            
            } catch( IllegalAccessException ex ) {
                throw new ModelException( "Illegal access setting " + this.getFullName(), ex );
            }
        }
    }
    
    protected void setFieldValue( ModelInstance instance, Object value ) throws IllegalAccessException {
        this.field.set( instance.actual, value );
        if( this.base != null ) {
            this.base.setFieldValue( instance, value );
        }
    }
    
    public void deleteRawValue( ModelInstance instance ) {
        //System.out.println(  "Deleting " + this.getName() + " with " + this.undefined.undefinedValue( instance ) );
        this.setRawValue( instance, this.undefined.undefinedValue( instance ) );
    }
    
    protected void orphanValue( ModelInstance instance, Object value ) {
        if( value != null && value instanceof ChildModel ) {
            ( (ChildModel)value ).orphanModel( instance, this );
        }
    }
    
    protected void adoptValue( ModelInstance instance, Object value ) {
        if( value != null && value instanceof ChildModel ) {
            ( (ChildModel)value ).adoptModel( instance, this );
        }
    }
    
    protected List<Interceptor> beforeGetters;
    protected List<Interceptor> afterGetters;
    protected List<Interceptor> beforeSetters;
    protected List<Interceptor> afterSetters;
    
    protected void buildGetter() {
        
        List<Method> getterMethods = new ArrayList<Method>();

        if( this.getterNames != null ) {
            for( String getterName: this.getterNames ) {
                Method getterMethod = Reflection.findMethod( model.getTarget(), getterName, Getter.class );
                if( getterMethod != null ) {
                    getterMethods.add( getterMethod );
                }
            }
        }
        
        if( this.beforeGetters == null && this.afterGetters == null && getterMethods.size() <= 1 ) {
            if( getterMethods.size() == 1 ) {
                this.getter = new MethodProxy( getterMethods.get( 0 ) );
            } else {
                this.getter = null;
            }
            
        } else {
            Pointcut pcut = new Pointcut();

            if( this.beforeGetters != null ) {
                pcut.add( this.beforeGetters );
            }

            for( Method getterMethod: getterMethods ) {
                pcut.add( new MethodCall( getterMethod ) );
            }

            if( this.afterGetters != null ) {
                pcut.add( this.afterGetters );
            }

            this.getter = pcut;
        }
    }

    protected void buildSetter() {
        
        List<Method> setterMethods = new ArrayList<Method>();

        if( this.setterNames != null ) {
            for( String setterName: this.setterNames ) {
                Method setterMethod = Reflection.findMethod( model.getTarget(), setterName, Setter.class );
                if( setterMethod != null ) {
                    setterMethods.add( setterMethod );
                }
            }
        }
        
        if( this.beforeSetters == null && this.afterSetters == null && setterMethods.size() <= 1 ) {
            if( setterMethods.size() == 1 ) {
                this.setter = new MethodProxy( setterMethods.get( 0 ) );
            } else {
                this.setter = null;
            }
            
        } else {
            Pointcut pcut = new Pointcut();

            if( this.beforeSetters != null ) {
                pcut.add( this.beforeSetters );
            }

            for( Method setterMethod: setterMethods ) {
                pcut.add( new MethodCall( setterMethod ) );
            }

            if( this.afterSetters != null ) {
                pcut.add( this.afterSetters );
            }

            this.setter = pcut;
        }
    }
 
    public class GetterPlacer {
        private Interceptor interceptor;
        
        public GetterPlacer( Interceptor interceptor ) {
            this.interceptor = interceptor;
        }
        
        public void before() {
            if( beforeGetters == null ) {
                beforeGetters = new ArrayList<Interceptor>();
            }
            beforeGetters.add( this.interceptor );
            buildGetter();
        }

        public void after() {
            if( afterGetters == null ) {
                afterGetters = new ArrayList<Interceptor>();
            }
            afterGetters.add( this.interceptor );
            buildGetter();
        }
    }
    
    public GetterPlacer placeGetter( Interceptor interceptor ) {
        return new GetterPlacer( interceptor );
    }

    public class SetterPlacer {
        private Interceptor interceptor;
        
        public SetterPlacer( Interceptor interceptor ) {
            this.interceptor = interceptor;
        }
        
        public void before() {
            if( beforeSetters == null ) {
                beforeSetters = new ArrayList<Interceptor>();
            }
            beforeSetters.add( this.interceptor );
            buildSetter();
        }

        public void after() {
            if( afterSetters == null ) {
                afterSetters = new ArrayList<Interceptor>();
            }
            afterSetters.add( this.interceptor );
            buildSetter();
        }
    }
    
    public SetterPlacer placeSetter( Interceptor interceptor ) {
        return new SetterPlacer( interceptor );
    }

    public Proxy getGetter() {
        return this.getter;
    }
    
    public Proxy getSetter(){
        return this.setter;
    }
    
    public Field getField(){
        return this.field;
    }
    
    public void setReadable( boolean readable ){
        this.readable = readable;
    }
    
    public void setWritable( boolean writable ){
        this.writable = writable;
    }
    
    public void setGetterNames( String[] getterNames ){
        this.getterNames = getterNames;
    }
    
    public void setSetterNames( String[] setterNames ){
        this.setterNames = setterNames;
    }
    
}