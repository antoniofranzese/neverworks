package it.neverworks.model.description;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.ModelException;
import it.neverworks.model.types.TypeAware;

@SuppressWarnings( "unchecked" )
public class DefaultMetadata implements PropertyProcessor, PropertyMetadata {
    
    protected Class<? extends DefaultFactory> factoryClass = StandardDefaultFactory.class;
    protected Class<?> defaultClass;
    private DefaultFactory factory;
    
    public DefaultMetadata( AnnotationInfo<Default> info ) {
        factoryClass = info.getAnnotation().factory();
        defaultClass = info.getAnnotation().value();
        
        if( factoryClass.equals( DefaultFactory.class ) ) {
            if( DefaultFactory.class.isAssignableFrom( defaultClass ) ) {
                // Narrow della classe
                factoryClass = defaultClass.asSubclass( DefaultFactory.class );
            } else {
                factoryClass = StandardDefaultFactory.class;
            }
        }
        
    }
    
    public DefaultMetadata( Class<?> defaultClass ) {
        this.defaultClass = defaultClass;
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        if( this.defaultClass == void.class ) {
            this.defaultClass = property.getStorageType();
        }
        
        if( this.defaultClass.isPrimitive() ) {
            throw new ModelException( "Cannot use @Default on primitive types (" + property.getFullName() + ")" );
        }

        if( factoryClass != null ) {
            
            if( Reflection.hasConstructor( factoryClass, Class.class ) ) {
                factory = (DefaultFactory)Reflection.withConstructor( factoryClass, Class.class ).newInstance( defaultClass );
                
            } else if( Reflection.hasConstructor( factoryClass ) ) {
                factory = (DefaultFactory)Reflection.withConstructor( factoryClass ).newInstance();
                
            } else {
                throw new ModelException( "Missing compatible constructor for " + factoryClass.getName() + " on " + property.getFullName() + " property" );
                
            }

            if( factory instanceof PropertyAware ) {
                ((PropertyAware) factory).setPropertyDescriptor( property );
            }

            if( factory instanceof TypeAware ) {
                TypeAware.set( (TypeAware) factory, defaultClass );
            }

            property.setDefault( factory );

        } else {
            throw new ModelException( "Null factory class for @Default on " + property.getFullName() + " property" );

        }
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( DefaultMetadata.class ) ) {
            property.metadata().add( this );
        }
    }
    
    public DefaultFactory getFactory(){
        return this.factory;
    }
    
}