package it.neverworks.model.description;

public interface ModelProcessor {    
    void processModel( ModelDescriptor model );
}