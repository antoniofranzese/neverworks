package it.neverworks.model.description;

public interface PropertyAware {    
    void setPropertyDescriptor( PropertyDescriptor property );
}