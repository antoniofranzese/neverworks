package it.neverworks.model.description;

import java.util.Collection;
import java.util.Map;
import java.util.HashMap;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Callable;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.aop.Pointcut;
import it.neverworks.aop.MethodPointcut;
import it.neverworks.model.Value;
import it.neverworks.model.ModelException;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Invoke;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Store;
import it.neverworks.model.description.InstanceMethod;
import it.neverworks.model.expressions.Expression;

import static it.neverworks.model.expressions.ExpressionEvaluator.isExpression;
import static it.neverworks.model.expressions.ExpressionEvaluator.evaluate;
import static it.neverworks.model.expressions.ExpressionEvaluator.valorize;
import static it.neverworks.model.expressions.ExpressionEvaluator.parse;

@SuppressWarnings( "unchecked" )
public class ModelInstance {
    
    public static boolean supports( Object object ) {
        return ( object instanceof Modelized );
    }

    public static ModelInstance of( Object object ) {
        if( object instanceof Modelized ) {
            return ((Modelized) object).retrieveModelInstance();
        } else if( object instanceof ModelInstance ) {
            return (ModelInstance) object;
        } else if( object != null ){
            throw new ModelException( object.getClass().getName() + " instance does not provide a ModelInstance" );
        } else {
            throw new ModelException( "Cannot provide ModelInstance of null" );
        }
    }
    
    /* package */ Object actual;
    /* package */ boolean watchable;
    /* package */ boolean watching;
    protected WatchableMetadata watchableMetadata;
    private Changes changes;
    private ModelDescriptor model;
    private Accessor frontsAccessor;
    private CheckedAccessor checkedsAccessor;
    private UncheckedAccessor uncheckedsAccessor;
    private DirectAccessor directsAccessor;
    private RawAccessor rawsAccessor;
    private Map<Class<?>, Object> modules;
    private boolean initialized = false;
    
    public ModelInstance( Object actual ) {
        this.actual = actual;
        this.model = ModelDescriptor.of( actual );
        this.frontsAccessor = new Accessor( this );
        this.watchable = this.model.watchable;
        this.watching = false;
        if( this.watchable ) {
            this.watchableMetadata = this.model.metadata( WatchableMetadata.class );
        }
        // this.init();
    } 

    public <M> M module( Class<M> moduleClass ) {
        if( this.modules == null ) {
            synchronized( this ) {
                if( this.modules == null ) {
                    this.modules = new HashMap<Class<?>, Object>();
                }
            }
        }
        if( !( this.modules.containsKey( moduleClass ) ) ) {
            synchronized( this ) {
                if( !( this.modules.containsKey( moduleClass ) ) ) {
                    Object module = Reflection.newInstance( moduleClass );
                    if( module instanceof ModelInstanceAware ) {
                        ((ModelInstanceAware) module).storeModelInstance( this );
                    }
                    this.modules.put( moduleClass, module );
                }
            }
        }
        return (M)this.modules.get( moduleClass );
    }
    
    public ModelDescriptor descriptor() {
        return this.model;
    }
    
    public PropertyManager manager( String name ) {
        return (PropertyManager)this.model.property( name );
    }

    public PropertyDescriptor property( String name ) {
        return this.model.property( name );
    }

    public ModelInstance init() {
        if( !this.initialized ) {
            this.initialized = true;

            this.model.init( this );

        }
        return this;
    }
    
    public boolean initialized() {
        return this.initialized;
    }
    
    public <T> T get( String name ) {
        if( this.model.has( name ) ) {
            return (T)this.manager( name ).getFrontValue( this );

        } else if( this.model.does( name ) ) {
            return (T)this.model.method( name ).bind( this.actual );

        } else {
            throw new MissingPropertyException( this.actual, name );
        }
    }

    public <T> T get( PropertyDescriptor descriptor ) {
        if( this.model.has( descriptor ) ) {
            return (T)descriptor.getFrontValue( this );

        } else {
            throw new MissingPropertyException( this.actual, descriptor.getFullName() );
        }
    }
    
    public InstanceMethod method( String name ) {
        return InstanceMethod.lookup( this.actual, name );
    }

    public Value value( String name ) {
        return Value.of( get( name ) );
    }
    
    public <T> T eval( String expression ) {
        return eval( expression, null );
    }

    public <T> T safeEval( String expression ) {
        return eval( expression, true );
    }

    public <T> T eval( String expression, T defaultValue ) {
        T value = eval( expression );
        return value != null ? value : defaultValue;
    }

    public <T> T safeEval( String expression, T defaultValue ) {
        T value = safeEval( expression );
        return value != null ? value : defaultValue;
    }

    public <T> T eval( String expression, Boolean allowNulls ) {
        if( isExpression( expression ) ) {
            Expression parsed = parse( expression );
            if( allowNulls != null ) {
                if( Boolean.TRUE.equals( allowNulls ) ) {
                    parsed.modifiers().withNulls();
                } else {
                    parsed.modifiers().withoutNulls();
                }
            }
            return evaluate( this.actual, parsed );
        } else {
            return get( expression );
        }
    }

    public ModelInstance set( String name, Object value ) {
        this.manager( name ).setFrontValue( this, value );  
        return this;
    }

    public ModelInstance assign( String expression, Object value ) {
        return assign( (Boolean) null, expression, value );
    }
    
    public ModelInstance safeAssign( String expression, Object value ) {
        return assign( true, expression, value );
    }

    public ModelInstance assign( Arguments arguments ) {
        return assign( (Boolean) null, arguments );
    }
    
    public ModelInstance safeAssign( Arguments arguments ) {
        return assign( true, arguments );
    }
    
    public ModelInstance assign( String arguments ) {
        return assign( (Boolean) null, arguments );
    }
    
    public ModelInstance safeAssign( String arguments ) {
        return assign( true, arguments );
    }
    
    public ModelInstance assign( Tuple tuple, String... names ) {
        return assign( (Boolean) null, tuple, names );
    }
    
    public ModelInstance safeAssign( Tuple tuple, String... names ) {
        return assign( true, tuple, names );
    }

    protected ModelInstance assign( Boolean allowNulls, Arguments arguments ) {
        if( arguments != null ) {
            for( String name: arguments.keys() ) {
                assign( allowNulls, name, arguments.get( name ) );
            }
        }
        return this;
    }

    protected ModelInstance assign( Boolean allowNulls, String arguments ) {
        return assign( allowNulls, Arguments.parse( arguments ) );
    }


    protected ModelInstance assign( Boolean allowNulls, Tuple tuple, String... names ) {
        if( tuple != null ) {
            for( int i = 0; i < names.length; i++ ) {
                assign( allowNulls, names[ i ], tuple.get( i ) );
            }
        }
        return this;
    }
    
    protected ModelInstance assign( Boolean allowNulls, String expression, Object value ) {
        if( isExpression( expression ) ) {
            Expression parsed = parse( expression );
            if( allowNulls != null ) {
                if( Boolean.TRUE.equals( allowNulls ) ) {
                    parsed.modifiers().withNulls();
                } else {
                    parsed.modifiers().withoutNulls();
                }
            }
            valorize( this.actual, parsed, value ); 
        } else {
            this.manager( expression ).setFrontValue( this, Objects.unwrap( value ) );  
        } 
        return this;
    }
    

    public <T> T probe( String name, Wrapper initializer ) {
        return probe( parse( name ), initializer );
    }

    public <T> T safeProbe( String name, Wrapper initializer ) {
        Expression expr = parse( name );
        expr.modifiers().withNulls().withOptional();
        return probe( expr, initializer );
    }
    
    public <T> T probe( Expression expression, Wrapper initializer ) {
        T value = (T)evaluate( this.actual, expression );
        if( value == null ) {
            synchronized( this.actual ) {
                value = (T)evaluate( this.actual, expression );
                if( value == null ) {
                    Object newValue = initializer != null ? initializer.asObject() : null;
                    expression.modifiers().withoutOptional();
                    valorize( this.actual, expression, newValue );
                    value = (T)evaluate( this.actual, expression );
                }
            }
        }
        return value;
    }
    
    public Accessor front() {
        return this.frontsAccessor;
    }

    /* Checked */
    public Accessor checked() {
        if( this.checkedsAccessor == null ) {
            this.checkedsAccessor = new CheckedAccessor( this );
        }
        return this.checkedsAccessor;
    }

    public <T> T checked( String name ) {
        return (T)this.manager( name ).getCheckedValue( this );
    }

    public ModelInstance checked( String name, Object value ) {
        this.manager( name ).setCheckedValue( this, value );
        return this;
    }

    /* Unchecked */
    public Accessor unchecked() {
        if( this.uncheckedsAccessor == null ) {
            this.uncheckedsAccessor = new UncheckedAccessor( this );
        }
        return this.uncheckedsAccessor;
    }

    public <T> T unchecked( String name ) {
        return (T)this.manager( name ).getUncheckedValue( this );
    }

    public ModelInstance unchecked( String name, Object value ) {
        this.manager( name ).setUncheckedValue( this, value );
        return this;
    }

    /* Direct */
    public Accessor direct() {
        if( this.directsAccessor == null ) {
            this.directsAccessor = new DirectAccessor( this );
        }
        return this.directsAccessor;
    }

    public <T> T direct( String name ) {
        return (T)this.manager( name ).getDirectValue( this );
    }

    public ModelInstance direct( String name, Object value ) {
        this.manager( name ).setDirectValue( this, value );
        return this;
    }

    /* Raw */
    public Accessor raw() {
        if( this.rawsAccessor == null ) {
            this.rawsAccessor = new RawAccessor( this );
        }
        return this.rawsAccessor;
    }

    public <T> T raw( String name ) {
        return (T)this.manager( name ).getRawValue( this );
    }

    public ModelInstance raw( String name, Object value ) {
        this.manager( name ).setRawValue( this, value );
        return this;
    }

    public class Accessor {
        protected ModelInstance instance;
        
        public Accessor( ModelInstance instance ) {
            this.instance = instance;
        }

        public <T> T get( String name ) {
            return (T)this.instance.manager( name ).getFrontValue( this.instance );
        }
        
        public Accessor set( String name, Object value ) {
            this.instance.manager( name ).setFrontValue( this.instance, value );
            return this;
        }
    }

    public class CheckedAccessor extends Accessor {
        public CheckedAccessor( ModelInstance instance ) {
            super( instance );
        }

        public <T> T get( String name ) {
            return (T)this.instance.manager( name ).getCheckedValue( this.instance );
        }

        public Accessor set( String name, Object value ) {
            this.instance.manager( name ).setCheckedValue( this.instance, value );
            return this;
        }
    }
    
    public class UncheckedAccessor extends Accessor {
        public UncheckedAccessor( ModelInstance instance ) {
            super( instance );
        }

        public <T> T get( String name ) {
            return (T)this.instance.manager( name ).getUncheckedValue( this.instance );
        }

        public Accessor set( String name, Object value ) {
            this.instance.manager( name ).setUncheckedValue( this.instance, value );
            return this;
        }
    }
    
    public class DirectAccessor extends Accessor {
        public DirectAccessor( ModelInstance instance ) {
            super( instance );
        }

        public <T> T get( String name ) {
            return (T)this.instance.manager( name ).getDirectValue( this.instance );
        }

        public Accessor set( String name, Object value ) {
            this.instance.manager( name ).setDirectValue( this.instance, value );
            return this;
        }
    }

    public class RawAccessor extends Accessor {
        public RawAccessor( ModelInstance instance ) {
            super( instance );
        }

        public <T> T get( String name ) {
            return (T)this.instance.manager( name ).getRawValue( this.instance );
        }

        public Accessor set( String name, Object value ) {
            this.instance.manager( name ).setRawValue( this.instance, value );
            return this;
        }
    }

    public boolean defined( String name ) {
        return this.manager( name ).hasRawValue( this );
    }

    public boolean undefined( String name ) {
        return !defined( name );
    }
    
    public ModelInstance delete( String name ) {
        this.manager( name ).deleteRawValue( this );
        return this;
    }
    
    public FluentList<String> names() {
        return this.model.names();
    }
    
    public FluentList<PropertyDescriptor> properties() {
        return this.model.properties();
    }
    
    public boolean has( String name ) {
        return this.model.has( name );
    }

    public boolean has( String name, Class<?> type ) {
        return this.model.has( name, type );
    }

    public boolean does( String name ) {
        return this.model.does( name );
    }

    public boolean knows( String name ) {
        return has( name ) || does( name );
    }
    
    public <T> T as( Class<T> actualClass ) {
        return (T)this.actual;
    }

    public boolean is( Class<?> actualClass ) {
        return actualClass.isInstance( this.actual );
    }
    
    public <T> T actual(){
        return (T)this.actual;
    }
    
    public Class getType() {
        return this.model.getTarget();
    }
    
    public ModelCopier copy( String expression ) {
        return ModelCopier.copy( ModelInstance.of( eval( expression ) ) );
    }

    public ModelCopier copy() {
        return ModelCopier.copy( this );
    }

    public <T> T duplicate() {
        return (T) ModelCopier.duplicate( this );
    }

    // Watch
    public boolean watchable() {
        return this.watchable;
    }

    public boolean watching() {
        return this.watching;
    }
    
    public ModelInstance watch() {
        if( watchable ) {
            if( !watching ) {
                this.changes = new Changes( this );
                
                Watcher watcher = new Watcher( this, Watcher.Operation.WATCH );
                
                for( WatchMetadata meta: watchableMetadata.cascades ) {
                    watcher.cascade( meta.name );
                }
                
                if( watchableMetadata.watchMethod != null ) {
                    Reflection.invokeMethod( actual, watchableMetadata.watchMethod, watcher );
                }
                watching = true;
            }
        } else {
            throw new ModelException( model.getTarget().getName() + " is not watchable" );
        }
        
        return this;
    }

    public ModelInstance ignore() {
        if( watchable ) {
            if( watching ) {
                Watcher watcher = new Watcher( this, Watcher.Operation.IGNORE );
                
                for( WatchMetadata meta: watchableMetadata.cascades ) {
                    watcher.cascade( meta.name );
                }
                
                if( watchableMetadata.ignoreMethod != null ) {
                    Reflection.invokeMethod( actual, watchableMetadata.ignoreMethod, watcher );
                }
                
                watching = false;
            }
        } else {
            throw new ModelException( model.getTarget().getName() + " is not watchable" );
        }
        
        return this;
    }

    public Changes changes() {
        if( watchable ) {
            if( watching ) {
                Changes chg = changes.clone();

                for( WatchMetadata meta: watchableMetadata.cascades ) {
                    if( meta.changes == WatchChanges.WHOLE ) {
                        chg.whole( meta.name );
                    } else if( meta.changes == WatchChanges.DETAILS ) {
                        chg.details( meta.name );
                    }
                }

                if( watchableMetadata.changesMethod != null ) {
                    return (Changes)Reflection.invokeMethod( actual, watchableMetadata.changesMethod, chg );
                } else {
                    return chg;
                }
                
            } else {
                return new Changes( this );
            }
        } else {
            throw new ModelException( model.getTarget().getName() + " is not watchable" );
        }
    }
    
    public ModelInstance touch( String name ) {
        if( watching ) {
            changes.add( name );
        }
        return this;
    }

    public ModelInstance touch( PropertyDescriptor property ) {
        return touch( property.getName() );
    }
    
    public boolean changed( String name ) {
        return changes().contains( name );
    }

    public boolean changed( PropertyDescriptor property ) {
        return changes().contains( property.getName() );
    }
    
    public Changes rawChanges() {
        return changes;
    }
    
    // Proxy
    public Object getInstanceValue( PropertyManager property ) {
        // intercept
        // System.out.println( "InterGet" );
        if( pointcuts.containsKey( property.getName() ) ) {
            return pointcuts.get( property.getName() ).onGet().invoke( this, property );
        } else {
            return property.getRawValue( this );
        }
    }
    
    public Object getRawValue( PropertyManager property ) {
        return property.getRawValue( this );
    }
    
    public void setInstanceValue( PropertyManager property, Object value ) {
        // intercept
        // System.out.println( "InterSet" );
        if( pointcuts.containsKey( property.getName() ) ) {
            pointcuts.get( property.getName() ).onSet().invoke( this, property, value );
        }
        this.setRawValue( property, value );
    }
    
    public void setRawValue( PropertyManager property, Object value ) {
        property.setRawValue( this, value );
    }
    
    //TODO: instanziare solo se serve
    protected Map<String, PropertyPointcut> pointcuts = new HashMap<String, PropertyPointcut>();
    
    public PropertyPointcut pointcut( String name ) {

        if( !pointcuts.containsKey( name ) ) {
            synchronized( actual ) {
                if( !pointcuts.containsKey( name ) ) {
                    pointcuts.put( name, new PropertyPointcut() );
                }
            }
        }
        manager( name ).setProxable( true );
        return pointcuts.get( name );
    }
    
    public class PropertyPointcut {
        private Pointcut get;
        private Pointcut set;
        
        public PropertyPointcut() {
            this.get = new MethodPointcut( Reflection.getMethod( ModelInstance.class, "getRawValue", PropertyManager.class ) );
            this.set = new MethodPointcut( Reflection.getMethod( ModelInstance.class, "setRawValue", PropertyManager.class, Object.class ) );
        }
        
        public Pointcut onGet() {
            return this.get;
        }

        public Pointcut onSet() {
            return this.set;
        }
    }
    
    public <T> T call( String name, Object... arguments ) {
        Object callable = eval( name );
        if( callable instanceof Invoke ) {
            return (T)( ((Invoke) callable).invokeInstance( arguments ) );
        } else if( callable instanceof Callable ) {
            return (T)( ((Callable) callable).call( arguments ) );
            
        //TODO: separare parse espressione
        } else if( callable == null && ( name.startsWith( "!" ) || name.startsWith( "?" ) ) ) {
            return null;
            
        } else {
            throw new ModelException( descriptor().getTarget().getName() + "." + name + " is not callable"  );
        } 
    }
    
    public String toString() {
        return "ModelInstance<" + this.actual() + ">";
    }
}
