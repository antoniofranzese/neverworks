package it.neverworks.model.description;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target( ElementType.TYPE )
@Retention( RetentionPolicy.RUNTIME )
@ModelAnnotation( processor = WatchableMetadata.class )
public @interface Watchable {
    String watch() default "watch";
    String ignore() default "ignore";
    String changes() default "changes";
}
