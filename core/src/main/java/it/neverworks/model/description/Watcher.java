package it.neverworks.model.description;

public class Watcher {
    
    public static enum Operation {
        WATCH, IGNORE
    }
    
    private Operation operation;
    private ModelInstance instance;
    
    public Watcher( ModelInstance instance, Operation operation ) {
        this.instance = instance;
        this.operation = operation;
    }
    
    public void cascade( String... names ) {
        for( String name: names ) {
            if( instance.raw( name ) != null ) {
                if( this.operation == Operation.WATCH ) {
                    ModelInstance.of( instance.raw( name ) ).watch();
                } else {
                    ModelInstance.of( instance.raw( name ) ).ignore();
                }
            }
        }
    }
    
}