package it.neverworks.model.description;

public class RequiredProcessor implements PropertyProcessor {
    
    public void processProperty( MetaPropertyManager property ) {
        property.setRequired( true );
    }

}