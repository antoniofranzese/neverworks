package it.neverworks.model.description;

import it.neverworks.model.types.WrongTypeException;

public class WrongPropertyTypeException extends WrongTypeException {
    
    protected PropertyManager property;
    
    public WrongPropertyTypeException( String message, PropertyManager property ) {
        super( message, property.getType() );
        this.property = property;
    }

    public WrongPropertyTypeException( String message, Throwable source, PropertyManager property ) {
        super( message, source, property.getType() );
        this.property = property;
    }
    
    public PropertyManager getProperty() {
        return this.property;
    }

}