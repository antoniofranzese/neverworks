package it.neverworks.model.description;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention( RetentionPolicy.RUNTIME )
public @interface MethodAnnotation {
    Class<? extends MethodProcessor> processor();
}
