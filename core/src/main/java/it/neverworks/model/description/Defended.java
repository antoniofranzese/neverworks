package it.neverworks.model.description;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.PropertyAnnotation;

@Target( { ElementType.FIELD } )
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = DefendedMetadata.class )
public @interface Defended {
    
}
