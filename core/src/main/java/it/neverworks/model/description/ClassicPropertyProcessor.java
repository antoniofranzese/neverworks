package it.neverworks.model.description;

public interface ClassicPropertyProcessor {    
    void processProperty( ClassicPropertyManager property );
}