package it.neverworks.model.description;

public interface DefaultFactory {
    Object getDefaultValue( ModelInstance instance );
}