package it.neverworks.model.description;

import java.lang.reflect.Constructor;

import it.neverworks.model.ModelException;
import it.neverworks.lang.Reflection;

public class InitValueDefaultFactory implements DefaultFactory, PropertyAware {
    
    private PropertyDescriptor property;
    
    public void setPropertyDescriptor( PropertyDescriptor property ) {
        this.property = property;
    }
    
    public Object getDefaultValue( ModelInstance instance ) {
        Object value = null;
        
        InstanceInitValues module = instance.module( InstanceInitValues.class );
        if( module.has( property.getName() ) ) {
            value = module.remove( property.getName() );
        }

        return value;
    }

}