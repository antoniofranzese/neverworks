package it.neverworks.model.description;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;

import it.neverworks.model.converters.Converter;

import it.neverworks.model.ModelException;
import it.neverworks.lang.Reflection;


@SuppressWarnings( "unchecked" )
public class SealedPropertyManager extends ClassicPropertyManager {
    
    public SealedPropertyManager( String name, ModelDescriptor model, java.beans.PropertyDescriptor property ) {
        super( name, model, property );
    }

    public Object getRawValue( ModelInstance instance ) {
        return getDirectValue( instance );
    }

    public void setRawValue( ModelInstance instance, Object value ) {
        setDirectValue( instance, value );
    }
    
    public Field getField(){
        throw new PropertyException( this, "Cannot access Field on sealed property" );
    }
    
    public String toString() {
        return this.getFullName() + " sealed property";
    }
    
}