package it.neverworks.model.description;

public enum WatchChanges {
    WHOLE, DETAILS, MANUAL 
}