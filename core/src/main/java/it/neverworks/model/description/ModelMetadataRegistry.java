package it.neverworks.model.description;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.lang.annotation.Annotation;

import it.neverworks.lang.Objects;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Collections;
import it.neverworks.model.ModelException;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Retrieve;

public class ModelMetadataRegistry implements Iterable<ModelMetadata>, Inspect, Retrieve {

    private Map metadata = new HashMap();
    private Map annotations = new HashMap();

    private ModelDescriptor model;
    private boolean met;
    
    public ModelMetadataRegistry( ModelDescriptor model ) {
        this.model = model;
    }
    
    public void add( ModelMetadata meta ) {
        if( !metadata.containsKey( meta.getClass() ) ) {
            metadata.put( meta.getClass(), meta );
            if( this.met ) {
                meta.meetModel( this.model );
            }
        } else {
            throw new ModelException( "Duplicate " + meta.getClass().getName() + " metadata in " + this.model.getTarget().getName() + " class" );
        }
    }

    public void add( Annotation annotation ) {
        this.annotations.put( annotation.annotationType(), annotation );
    }
    
    public <T extends ModelMetadata> T instance( Class<T> metaclass ) {
        if( ! metadata.containsKey( metaclass ) ) {
            synchronized( this ) {
                if( ! metadata.containsKey( metaclass ) ) {
                    add( (ModelMetadata) Reflection.newInstance( metaclass ) );
                }                
            }
        }
        return (T)metadata.get( metaclass );
    }
    
    public <T extends Annotation> T annotation( Class<T> annoclass ) {
        if( this.annotations.containsKey( annoclass ) ) {
            return (T)this.annotations.get( annoclass );
        } else {
             throw new ModelException( "Missing " + annoclass.getName() + " annotation in " + this.model.getTarget().getName() + " model" );
         }
    }

    public Collection<ModelMetadata> all() {
        return Collections.<ModelMetadata>list( this.metadata.values() );
    }

    public Collection<Annotation> annotations() {
        return Collections.<Annotation>list( this.annotations.values() );
    }

    public boolean has( Class<? extends ModelMetadata> metaclass ) {
        return contains( metaclass );
    }
    
    public boolean contains( Class metaclass ) {
        return metadata.containsKey( metaclass ) || annotations.containsKey( metaclass );
    }
    
    public boolean containsItem( Object item ) {
        if( item instanceof Class ) {
            return contains( (Class) item );
        } else if( item instanceof ModelMetadata ) {
            return metadata.containsValue( item );
        } else if( item instanceof Annotation ) {
            return annotations.containsValue( item );
        } else {
            return false;
        }
    }
    
    public Object retrieveItem( Object key ) {
        if( key instanceof Class ) {
            if( ModelMetadata.class.isAssignableFrom( (Class) key ) ) {
                return this.metadata.get( (Class) key );
            } else if( Annotation.class.isAssignableFrom( (Class) key ) ) {
                return this.annotations.get( (Class) key );
            }
        }
        throw new IllegalArgumentException( "Invalid metadata class key: " + Objects.repr( key ) );
    }
    
    public Iterator<ModelMetadata> iterator() {
        return metadata.values().iterator();
    }
    
    public void meet() {
        if( ! this.met ) {
            this.met = true;
            for( ModelMetadata meta: this ) {
                meta.meetModel( this.model );
            }
        }
    }
            
}
