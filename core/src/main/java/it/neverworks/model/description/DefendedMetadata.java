package it.neverworks.model.description;

import java.lang.reflect.Field;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;

public class DefendedMetadata implements PropertyProcessor, PropertyMetadata {
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){

    }
    
}