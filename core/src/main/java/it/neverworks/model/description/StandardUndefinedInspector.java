package it.neverworks.model.description;

public class StandardUndefinedInspector implements UndefinedInspector {
    
    public boolean checkUndefined( ModelInstance instance, Object value ) {
        return value == null;
    }
    
    public Object undefinedValue( ModelInstance instance ) {
        return null;
    }
}