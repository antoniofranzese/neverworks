package it.neverworks.model.description;

public class IgnoreMetadata implements PropertyProcessor, PropertyMetadata, ClassicPropertyProcessor {
    
    public void processProperty( MetaPropertyManager property ) {
        property.setIgnored( true );
        property.metadata().add( this );
    }

    public void processProperty( ClassicPropertyManager property ) {
        property.setIgnored( true );
    }
    
    public void meetProperty( MetaPropertyManager property ){
        
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( IgnoreMetadata.class ) ) {
            processProperty( property );
        }
    }

}