package it.neverworks.model.description;

public class UnreadablePropertyException extends PropertyException {

    public UnreadablePropertyException( PropertyDescriptor property, String error ) {
        super( property, error );
    }
    
}