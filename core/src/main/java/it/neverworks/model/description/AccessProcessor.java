package it.neverworks.model.description;

import it.neverworks.lang.AnnotationInfo;

public class AccessProcessor implements PropertyProcessor {

    protected Access annotation;

    public AccessProcessor( AnnotationInfo<Access> info ) {
        this.annotation = info.getAnnotation();
    }
    
    public void processProperty( MetaPropertyManager property ) {
        property.setReadable( annotation.readable() );
        property.setWritable( annotation.writable() );

        if( annotation.getter().length > 0 ) {
            property.setGetterNames( annotation.getter() );
        }

        if( annotation.setter().length > 0 ) {
            property.setSetterNames( annotation.setter() );
        }
        
    }

}