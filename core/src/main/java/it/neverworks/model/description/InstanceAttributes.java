package it.neverworks.model.description;

import java.util.Map;
import java.util.HashMap;

public class InstanceAttributes {
    
    private Map<String, Object> attributes;
    
    public void set( String name, Object value ) {
        if( this.attributes == null ) {
            synchronized( this ) {
                if( this.attributes == null ) {
                    this.attributes = new HashMap<String, Object>();
                }
            }
        }
        this.attributes.put( name, value );
    }

    public boolean has( String name ) {
        return this.attributes != null && this.attributes.containsKey( name );
    }
    
    public <T> T get( String name ) {
        if( has( name ) ) {
            return (T) this.attributes.get( name );
        } else {
            return null;
        }
    }
    
    public <T> T remove( String name ) {
        if( this.attributes != null ) {
            return (T) this.attributes.remove( name );
        } else {
            return null;
        }
    }
    
}