package it.neverworks.model.description;

public class VirtualProcessor implements PropertyProcessor, ClassicPropertyProcessor {
    
    public void processProperty( MetaPropertyManager property ) {
        property.setVirtual( true );
    }

    public void processProperty( ClassicPropertyManager property ) {
        property.setVirtual( true );
    }

}