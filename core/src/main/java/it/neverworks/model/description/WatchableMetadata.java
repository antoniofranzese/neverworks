package it.neverworks.model.description;

import java.lang.reflect.Method;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.lang.Reflection;

public class WatchableMetadata implements ModelProcessor, ModelMetadata {    
    
    protected String watchName;
    protected String ignoreName;
    protected String changesName;
    /*package*/ Method watchMethod;
    /*package*/ Method ignoreMethod;
    /*package*/ Method changesMethod;
    /*package*/ WatchMetadata[] cascades;
    
    public WatchableMetadata( AnnotationInfo<Watchable> info ) {
        watchName = info.getAnnotation().watch();
        ignoreName = info.getAnnotation().ignore();
        changesName = info.getAnnotation().changes();
    }
    
    public void processModel( ModelDescriptor model ) {
        model.watchable = true;
        
        this.watchMethod = Reflection.findMethod( model.getTarget(), this.watchName, Watcher.class );
        if( this.watchMethod != null ) {
            Reflection.makeAccessible( this.watchMethod );
        }
        
        this.ignoreMethod = Reflection.findMethod( model.getTarget(), this.ignoreName, Watcher.class );
        if( this.ignoreMethod != null ) {
            Reflection.makeAccessible( this.ignoreMethod );
        }
        
        this.changesMethod = Reflection.findMethod( model.getTarget(), this.changesName, Changes.class );
        if( this.changesMethod != null ) {
            Reflection.makeAccessible( this.changesMethod );
        }
        
        List cascades = new ArrayList<WatchMetadata>();
        for( String name: model.names() ) {
            if( model.property( name ).metadata().contains( WatchMetadata.class ) ) {
                cascades.add( model.property( name ).metadata( WatchMetadata.class ) );
            }
        }
        this.cascades = (WatchMetadata[]) cascades.toArray( new WatchMetadata[ cascades.size() ]);
        
        model.metadata.add( this );
        
    }
    
    public void meetModel( ModelDescriptor model ) {
        
    }
    
    public void inheritModel( ModelDescriptor model ) {
        
    }
    
}