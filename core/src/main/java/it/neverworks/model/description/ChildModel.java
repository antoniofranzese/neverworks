package it.neverworks.model.description;

public interface ChildModel {
    
    default void adoptModel( ModelInstance model, PropertyDescriptor property ){}
    default void orphanModel( ModelInstance model, PropertyDescriptor property ){}
    
}