package it.neverworks.model.description;

public interface UndefinedInspector {
    boolean checkUndefined( ModelInstance instance, Object value );
    Object undefinedValue( ModelInstance instance );
}