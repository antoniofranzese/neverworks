package it.neverworks.model.description;

import it.neverworks.aop.Invocation;
import it.neverworks.aop.InvocationAware;
import it.neverworks.lang.Wrapper;

public class Getter<T> implements InvocationAware {
    
    protected ModelInstance instance;
    protected PropertyManager property;
    protected Invocation invocation;
    /*package*/ boolean performed;
    
    public Getter( ModelInstance instance, PropertyManager property ) {
        this.instance = instance;
        this.property = property;
    }
    
    public T get() {
        this.performed = true;
        if( this.invocation != null && ! this.invocation.terminated() ) {
            return (T)this.invocation.invoke();
        } else {
            return (T)property.getDirectValue( instance );
        }
    }

    public T get( Object value ) {
        this.decline();
        return (T)property.process( this.instance, value );
    }

    public T probe( Wrapper initializer ) {
        if( this.undefined() ) {
            synchronized( this.actual() ) {
                if( this.undefined() ) {
                    this.setDirect( (T)property.process( this.instance, initializer.asObject() ) );
                }
            }
        }
        return get();
    }

    public void decline() {
        performed = true;
        if( this.invocation != null ) {
            this.invocation.decline();
        }
    }

    public T value() {
        return get();
    }

    public T value( Object value) {
        return get( value );
    }

    public T getValue() {
        return get();
    }
    
    public String name() {
        return property.getName();
    }
    
    public String fullName() {
        return property.getFullName();
    }
    
    public String getName() {
        return property.getName();
    }

    public T direct() {
        return (T)property.getDirectValue( instance );
    }
    
    public T getDirect() {
        return (T)property.getDirectValue( instance );
    }

    public void setDirect( T value ) {
        property.setDirectValue( instance, value );
    }

    public T raw() {
        return (T)property.getRawValue( instance );
    }

    public T getRaw() {
        return (T)property.getRawValue( instance );
    }

    public void setRaw( T value ) {
        property.setRawValue( instance, value );
    }
    
    public boolean exists() {
        return property.hasRawValue( instance );
    }

    public boolean defined() {
        return property.hasRawValue( instance );
    }

    public boolean undefined() {
        return !defined();
    }
    
    public void delete() {
        property.deleteRawValue( instance );
    }

    public ModelInstance instance() {
        return instance;
    }

    public PropertyManager property() {
        return property;
    }
    
    public <T> T actual() {
        return (T) instance.actual();
    }
    
    public Invocation invocation() {
        return this.invocation;
    }
    
    public void storeInvocation( Invocation invocation ) {
        this.invocation = invocation;
    }

}