package it.neverworks.model.description;

public enum AccessLevel {
    FRONT
    ,CHECKED
    ,UNCHECKED
    ,DIRECT
    ,RAW
}