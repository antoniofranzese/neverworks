package it.neverworks.model.description;

import it.neverworks.aop.Interceptor;
import it.neverworks.aop.Invocation;

public abstract class SetterInterceptor<T> implements Interceptor {

    public Object invoke( Invocation invocation ) {
        Setter<T> setter = invocation.argument( 0 );
        set( setter );
        return null;
    }
    
    public abstract void set( Setter<T> setter );

}
