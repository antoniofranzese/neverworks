package it.neverworks.model.description;

import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.ModelException;
import it.neverworks.lang.Reflection;

@SuppressWarnings( "unchecked" )
public class UndefinedProcessor implements PropertyProcessor {
    
    protected Class<? extends UndefinedInspector> definitionClass;
    
    public UndefinedProcessor( AnnotationInfo<Undefined> info ) {
        this.definitionClass = info.getAnnotation().value();
    }
    
    public UndefinedProcessor( Class<? extends UndefinedInspector> definitionClass ) {
        this.definitionClass = definitionClass;
    }

    public void processProperty( MetaPropertyManager property ) {
        UndefinedInspector definition = null;
        try {
           definition = (UndefinedInspector)Reflection.newInstance( this.definitionClass );
        } catch( Exception ex ) {
            throw new ModelException( "Error instantiating @Undefined definition on " + property.getFullName() + " property", ex );
        }

        if( definition instanceof PropertyAware ) {
            ((PropertyAware)definition).setPropertyDescriptor( property );
        }

        property.setUndefined( definition );

    }

}