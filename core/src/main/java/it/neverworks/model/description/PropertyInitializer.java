package it.neverworks.model.description;

public interface PropertyInitializer {
    void initProperty( MetaPropertyManager property, ModelInstance instance );
}