package it.neverworks.model.description;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = AccessProcessor.class )
public @interface Access {
    boolean readable() default true;
    boolean writable() default true;
    String[] getter() default {};
    String[] setter() default {};
}
