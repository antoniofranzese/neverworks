package it.neverworks.model.description;

public interface PropertyProcessor {    
    void processProperty( MetaPropertyManager property );
}