package it.neverworks.model.description;

import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.description.ModelMetadata;
import it.neverworks.model.description.ModelDescriptor;

public class ModelMethodMetadata implements ModelMetadata {
    
    private List<Deferral> deferrals = new ArrayList<Deferral>();
    private boolean met = false;
    
    private class Deferral {
        private MethodDescriptor method;
        private MethodProcessor processor;
    }
    
    public void meetModel( ModelDescriptor model ) {
        this.met = true;
        for( Deferral deferral: this.deferrals ) {
            deferral.processor.processMethod( deferral.method );
        }
    }
    
    public void inheritModel( ModelDescriptor model ) {
        
    }
    
    public void addDeferral( MethodDescriptor method, MethodProcessor processor ) {
        if( this.met ) {
            processor.processMethod( method );
        } else {
            Deferral deferral = new Deferral();
            deferral.method = method;
            deferral.processor = processor;
            this.deferrals.add( deferral );
        }
    }
    
}