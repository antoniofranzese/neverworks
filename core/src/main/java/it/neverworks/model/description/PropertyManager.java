package it.neverworks.model.description;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.annotation.Annotation;

import it.neverworks.aop.Proxy;
import it.neverworks.model.converters.Converter;

import it.neverworks.model.ModelException;
import it.neverworks.lang.Reflection;


@SuppressWarnings( "unchecked" )
public interface PropertyManager extends PropertyDescriptor {
    
    Object getFrontValue( ModelInstance instance );
    Object getCheckedValue( ModelInstance instance );
    Object getUncheckedValue( ModelInstance instance );
    Object getDirectValue( ModelInstance instance );
    Object getRawValue( ModelInstance instance );
    boolean hasRawValue( ModelInstance instance );
    void init( ModelInstance instance );   
    void initValue( ModelInstance instance, Object value );
    void setFrontValue( ModelInstance instance, Object value );
    void setCheckedValue( ModelInstance instance, Object value );
    void setUncheckedValue( ModelInstance instance, Object value );
    void setDirectValue( ModelInstance instance, Object value );
    void setRawValue( ModelInstance instance, Object value );
    void deleteRawValue( ModelInstance instance );
    void setConverter( Converter converter );
    void setRequired( boolean required );
    void setChild( boolean child );
    void setDefault( DefaultFactory default_ );
    void setUndefined( UndefinedInspector undefined );
    void setProxable( boolean proxable );
   
}