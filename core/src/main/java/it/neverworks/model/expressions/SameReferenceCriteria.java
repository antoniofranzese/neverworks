package it.neverworks.model.expressions;

public class SameReferenceCriteria extends SingleValueCriteria {
    
    public SameReferenceCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target != null && target == value;
    }

}