package it.neverworks.model.expressions;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Collection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Callable;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Functional;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Inspector;
import it.neverworks.lang.Calculator;
import it.neverworks.model.features.Invoke;
import it.neverworks.model.features.Length;
import it.neverworks.model.utils.ExpandoModel;
import it.neverworks.model.Value;

public class ObjectQuery<T> implements Functional<T>, Length {

    private final static Modifiers DEFAULT_MODIFIERS = new Modifiers().withNulls();
    
    Iterable<T> source;
    List<Criteria> criteria;
    FluentList result;
    FluentList indexes;
    boolean useValues = false;
    private Integer resultIndex = null;
    
    private ObjectQuery() {
        this.criteria = new ArrayList<Criteria>();
    }

    public ObjectQuery( Iterable<T> source ) {
        this();
        this.source = source != null ? source : new FluentArrayList<T>();
    }

    public ObjectQuery( Class<T> source ) {
        this( Collections.<T>iterable( source ) );
    }

    public ObjectQuery( T[] source ) {
        this( Collections.<T>iterable( source ) );
    }

    public ObjectQuery( Object source ) {
        this();
        if( Collections.isIterable( source ) ) {
            this.source = Collections.iterable( source );
            
        } else if( source == null ) {
            this.source = new FluentArrayList<T>();
        
        } else {
            throw new IllegalArgumentException( "ObjectQuery source is not iterable: " + Objects.repr( source ) );
        }
    }

    public ObjectCriteriaBuilder<T> by( String expression ) {
        return new ObjectCriteriaBuilder<T>( this, expression );
    }

    public ObjectQuery<T> by( Filter filter ) {
        if( filter != null ) {
            this.add( new FilterCriteria( filter ) );
        }
        return this;
    }
    
    public ObjectQuery<T> and( Filter filter ) {
        return by( filter );
    }

    public ObjectCriteriaBuilder<T> byItem() {
        return new ObjectCriteriaBuilder<T>( this, null );
    }

    public ObjectCriteriaBuilder<T> and( String expression ) {
        return by( expression );
    }

    public ObjectQuery<T> sub( String expression, SubEnquirer enquirer ) {
        if( enquirer != null ) {
            this.add( new SubQueryCriteria( expression, enquirer ) );
        }
        return this;
    }
    
    public <O> ObjectQuery<O> instanceOf( Class<O> type ) {
        byItem().instanceOf( type );
        return (ObjectQuery<O>) this;
    }

    public ObjectQuery<T> notInstanceOf( Class<?> type ) {
        byItem().notInstanceOf( type );
        return this;
    }
    
    public Iterator<T> iterator() {
        return list().iterator();
    }
    
    public ObjectQuery<T> add( Criteria criteria ) {
        if( result != null ) {
            result = null;
            this.criteria = new ArrayList<Criteria>();
        }
        this.criteria.add( criteria );
        return this;
    }
    
    public int size() {
        return count();
    }
    
    public int count() {
        return list().size();
    }
    
    public int itemsCount() {
        return count();
    }
    
    // @Override
    // protected Iterable<T> iterable() {
    //     return list();
    // }
    
    @Override
    public FluentList<T> list() {
        if( result == null ) {
            if( this.criteria.size() > 0 ) {
                result = new FluentArrayList();
                boolean include;
                for( Object item: source ) {
                    include = true;
                    for( Criteria crit: criteria ) {
                        try {
                            if( include && !crit.matches( item ) ) {
                                include = false;
                                break;
                            }
                        } catch( Exception ex ) {
                            include = false;
                            break;
                        }
                    }
                    if( include ) {
                        result.add( item );
                    }
                }
                
            } else if( source instanceof FluentList ) {
                result = (FluentList) source;
            
            } else {
                result = new FluentArrayList<T>().cat( source );
            }
        }
        return (FluentList<T>)result;
    }
    
    public <R> ObjectQuery<R> get( String... expressions ) {
        return select( expressions );
    }

    public <R> ObjectQuery<R> select( String... expressions ) {
        if( expressions.length == 1 ) {
            List list = list();
            FluentList results = new FluentArrayList( list.size() );

            for( Object item: list ) {
                results.add( (R)ExpressionEvaluator.evaluate( item, expressions[0], DEFAULT_MODIFIERS ) );
            }
            return new ObjectQuery<R>( results );
        } else {
            return (ObjectQuery<R>) select( ExpandoModel.class, expressions );
        }
    }

    public <R> ObjectQuery<R> select( Map<String, Object> mappings ) {
        return (ObjectQuery<R>) select( ExpandoModel.class, mappings );
    }

    public <R> ObjectQuery<R> select( Class<R> resultClass, String... expressions ) {
        Map<String, Object> mappings = new HashMap<String, Object>();
        for( String expression: expressions ) {
            mappings.put( expression, expression );
        }
        return select( resultClass, mappings );
    }
    
    public <R> ObjectQuery<R> select( Class<R> resultClass, Map<String, Object> mappings ) {
        if( mappings.size() == 0 ) {
            return (ObjectQuery<R>) this;
        } else {
            List list = list();
            FluentList results = new FluentArrayList( list.size() );

            for( Object item: list ) {
                Object result = Reflection.newInstance( resultClass );
                for( String expression: mappings.keySet() ) {
                    ExpressionEvaluator.valorize( result, expression, ExpressionEvaluator.evaluate( item, Strings.valueOf( mappings.get( expression ) ), DEFAULT_MODIFIERS ) );
                }
                results.add( result );
            }

            return new ObjectQuery<R>( results );
        }
    }
    
    public <R> FluentList<R> list( String expression ) {
        List list = list();
        FluentList<R> result = new FluentArrayList<R>( list.size() );
        for( Object item: list ) {
            result.add( (R)ExpressionEvaluator.evaluate( item, expression, DEFAULT_MODIFIERS ) );
        }
        return result;
    }
    
    public <T> T result() {
        List list = list();
        if( list.size() == 1 ) {
            return (T)list.get( 0 );
        } else if( list.size() == 0 ) {
            return null;
        } else if( this.resultIndex != null ) {
            if( resultIndex >= 0 && list.size() > resultIndex ) {
                return (T) list.get( resultIndex );
            } else if( resultIndex < 0 && list.size() > 0 ) {
                return (T) list.get( list.size() + resultIndex );
            } else {
                return null;
            }

        } else {
            throw new QueryException( "Non unique result from query" );
        }
    }
    
    public <R> R result( String expression ) {
        return (R)ExpressionEvaluator.evaluate( result(), expression, DEFAULT_MODIFIERS );
    }
    
    public ObjectQuery<T> first() {
        this.resultIndex = 0;
        return this;
    }
    
    public ObjectQuery<T> last() {
        this.resultIndex = -1;
        return this;
    }

    public ObjectQuery<T> nth( int index ) {
        this.resultIndex = index;
        return this;
    }
    
    public FluentList<Integer> indexes() {
        if( indexes == null ) {
            indexes = new FluentArrayList();
            boolean include;
            int index = 0;
            for( Object item: source ) {
                include = true;
                for( Criteria crit: criteria ) {
                    try {
                        if( include && !crit.matches( item ) ) {
                            include = false;
                            break;
                        }
                    } catch( Exception ex ) {
                        include = false;
                        break;
                    }
                }
                if( include ) {
                    indexes.add( index );
                }
                index++;
            }
        }
        return (FluentList<Integer>)indexes;
    }
    
    public Integer index() {
        List<Integer> indexes = this.indexes();
        if( indexes != null ) {
            if( indexes.size() == 1 ) {
                return indexes.get( 0 );
            } else {
                throw new QueryException( "Non unique result index from query" );
            }
        } else {
            return null;
        }
    }
    
    public Value value() {
        return Value.of( result() );
    }
    
    public Value value( String expression ) {
        return Value.of( result( expression ) );
    }

    public Value value( int index ) {
        return Value.of( get( index ) );
    }
    
    public T get( int index ) {
        List list = list();
        if( index < list.size() ) {
            return (T) list.get( index );
        } else {
            throw new QueryException( "Result index out of bounds: " + index );
        }
    }

    public ObjectQuery<T> set( String path, Object value ) {
        for( Object item: this.list() ) {
            ExpressionEvaluator.valorize( item, path, value );
        }
        return this;
    }
    
    public <E> Functional<E> call( String path, Object... arguments ) {
        FluentArrayList<E> results = new FluentArrayList<E>();
        
        for( Object item: this.list() ) {
            results.add( (E) invoke( item, path, arguments ) );
        }
        
        return results;
    }
    
    public ObjectQuery<T> exec( String path, Object... arguments ) {
        for( Object item: this.list() ) {
            invoke( item, path, arguments );
        }
        return this;
    }
    
    protected Object invoke( Object item, String path, Object... arguments ) {
        Object method = ExpressionEvaluator.evaluate( item, path );
        if( method instanceof Invoke ) {
            return ((Invoke) method).invokeInstance( arguments );
        } else if( method instanceof Callable ) {
            return ((Callable) method).call( arguments );
        } else {
            throw new QueryException( "Expression '" + path + "' is not callable on: " + Objects.repr( item ) );
        }
    }
    
    public <O> ObjectQuery<O> as( Class<O> other ) {
        return (ObjectQuery<O>) this;
    }
    
    public ObjectQuery<T> sort( String path ) {
        FluentList<T> sortable = new FluentArrayList<T>( this.list() );
        
        java.util.Collections.sort( sortable, new PathComparator( path ) );
        
        return new ObjectQuery<T>( sortable );
    }

    public ObjectQuery<T> sort( String... paths ) {
        FluentList<T> sortable = new FluentArrayList<T>( this.list() );
        
        java.util.Collections.sort( sortable, new CompositePathComparator( paths ) );
        
        return new ObjectQuery<T>( sortable );
    }

    public ObjectQuery<T> sort( Iterable<String> paths ) {
        FluentList<T> sortable = new FluentArrayList<T>( this.list() );
        
        java.util.Collections.sort( sortable, new CompositePathComparator( Collections.toArray( String.class, paths ) ) );
        
        return new ObjectQuery<T>( sortable );
    }

    public <O> O[] array( Class<O> target ) {
        return list().array( target );
    }
    
    public String join( String delimiter ) {
        return list().join( delimiter );
    }
    
    private class CompositePathComparator implements Comparator {
        private List<Comparator> comparators;
        
        public CompositePathComparator( String[] paths ) {
            comparators = new ArrayList<Comparator>();
            for( String path: paths ) {
                comparators.add( new PathComparator( path ) );
            }
        }
        
        public int compare( Object item1, Object item2 ) {
            Iterator<Comparator> it = comparators.iterator();
            int result = 0;
            while( it.hasNext() && result == 0 ) {
                result = it.next().compare( item1, item2 );
            }
            return result;
        }
    }
    
    private class PathComparator implements Comparator {
        private String expression;
        private int direction;
        
        public PathComparator( String expression ) {
            if( expression.startsWith( "-" ) ) {
                this.expression = expression.substring( 1 );
                this.direction = -1;
            } else {
                this.expression = expression;
                this.direction = 1;
            }
        }
        
        public int compare( Object item1, Object item2 ){
            Object obj1 = ExpressionEvaluator.evaluate( item1, expression );
            Object obj2 = ExpressionEvaluator.evaluate( item2, expression );
            
            if( obj1 != null && !( obj1 instanceof Comparable ) ) {
                obj1 = Strings.valueOf( obj1 );
            }

            if( obj2 != null && !( obj2 instanceof Comparable ) ) {
                obj2 = Strings.valueOf( obj2 );
            }
            
            if( obj1 != null ) {
                if( obj2 == null ) {
                    return 1 * direction;
                } else {
                    return ((Comparable) obj1).compareTo( obj2 ) * direction;
                }

            } else if( obj1 == null ) {
                if( obj2 == null ) {
                    return 0;
                } else {
                    return -1 * direction;
                }

            } else if( obj2 == null ) {
                return 1 * direction;

            } else {
                throw new IllegalArgumentException( "Cannot compare " + Objects.className( obj1 ) + " and " + Objects.className( obj2 ) );
            }
        }
    }
    
    public Value max() {
        return max( "/" );
    }
    
    public Value max( String path ) {
        return Value.of( this.reduce( new MaxInspector( path ) ).result() );
    }

    public Value min() {
        return min( "/" );
    }
    
    public Value min( String path ) {
        return Value.of( this.reduce( new MinInspector( path ) ).result() );
    }

    public Value avg() {
        return avg( "/" );
    }
    
    public Value avg( String path ) {
        return Value.of( this.reduce( new AvgInspector( path ) ).result() );
    }

    public Value sum() {
        return sum( "/" );
    }
    
    public Value sum( String path ) {
        return Value.of( this.reduce( new SumInspector( path ) ).result() );
    }
 
    private abstract class AggregationInspector implements Inspector {
        protected String path;
        protected Number result;

        public AggregationInspector( String path ) {
            this.path = path;
        }
        
        protected Number number( Object source ) {
            return Numbers.number( ExpressionEvaluator.evaluate( source, path ) );
        }    
        
        public Number result() {
            return this.result != null ? this.result : 0;
        }                   
    }
    
    private class MaxInspector extends AggregationInspector {
    
        public MaxInspector( String path ) {
            super( path );
        }
    
        public void inspect( Object source ) {
            Number current = number( source );

            if( this.result == null ) {
                this.result = Long.MIN_VALUE;
            }

            if( current != null ) {
                this.result = Numbers.compare( current, this.result ) > 0 ? current : this.result;
            }
        }
    }
    
    private class MinInspector extends AggregationInspector {
    
        public MinInspector( String path ) {
            super( path );
        }
    
        public void inspect( Object source ) {
            Number current = number( source );

            if( this.result == null ) {
                this.result = Long.MAX_VALUE;
            }

            if( current != null ) {
                this.result = Numbers.compare( current, this.result ) < 0 ? current : this.result;
            }

        }
    }                                                 

    private class AvgInspector extends AggregationInspector {
        private Calculator calc = new Calculator().push( 0 );
        private int count = 0;
        
        public AvgInspector( String path ) {
            super( path );
        }
    
        public void inspect( Object source ) {
            Number current = number( source );
            if( current != null ) {
                this.calc.add( current );
                this.count += 1;
            }
        }
        
        public Number result() {
            return this.count > 0 ? calc.div( this.count ).result() : 0;
        }                   
        
    }                                                 

    private class SumInspector extends AggregationInspector {
        private Calculator calc = new Calculator().push( 0 );
        
        public SumInspector( String path ) {
            super( path );
        }
    
        public void inspect( Object source ) {
            Number current = number( source );
            if( current != null ) {
                this.calc.add( current );
            }
        }
        
        public Number result() {
            return calc.result();
        }                   
        
    }                                                 
                                                     
    
}