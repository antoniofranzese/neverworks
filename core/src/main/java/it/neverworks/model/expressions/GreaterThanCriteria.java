package it.neverworks.model.expressions;

public class GreaterThanCriteria extends ComparatingCriteria {
    
    public GreaterThanCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target != null && value != null && compare( target, value ) > 0;
    }

}