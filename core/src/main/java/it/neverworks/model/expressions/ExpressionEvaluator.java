package it.neverworks.model.expressions;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.lang.reflect.Method;
import java.lang.reflect.Array;

import it.neverworks.language;
import it.neverworks.lang.Range;
import it.neverworks.lang.Mapper;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Collections;
import it.neverworks.lang.MissingPropertyException;
import it.neverworks.model.Value;
import it.neverworks.model.features.Store;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Evaluate;
import it.neverworks.model.features.Assign;
import it.neverworks.model.features.Ensure;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.Modelized;
import it.neverworks.model.collections.ModelCollection;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.description.InstanceMethod;

public class ExpressionEvaluator {
    
    private final static Modifiers WITHOUT_NULLS = new Modifiers().withoutNulls().freeze();
    private final static Modifiers WITH_RAW_WITHOUT_NULLS = new Modifiers().withRaw().withoutNulls().freeze();
    
    public static <T> T evaluate( Object root, Expression expression ) {
        return evaluate( root, expression.text(), expression.steps(), expression.modifiers() );
    }
    
    public static <T> T evaluate( Object root, String expression ) {
        Modifiers modifiers = parseModifiers( expression );
        return evaluate( root, modifiers.expression, modifiers );
    }
    
    public static <T> T evaluate( Object root, String expression, Modifiers modifiers ) {
        
        if( root == null ) {
            if( modifiers.allowNulls ) {
                return null;
            } else {
                throw new ExpressionException( "Null root" );
            }
        } else {
            if( Strings.isEmpty( expression ) || "/".equals( expression ) ) {
                return (T)root;
            } else {
                return evaluate( root, expression, parseExpression( expression ), modifiers );
            }
            
        }
        
    }
    
    protected static <T> T evaluate( Object root, String expression, List<String> steps, Modifiers modifiers ) {
        Object current = Value.from( root );
        boolean allowMissing = modifiers.allowNulls || modifiers.optional;
        int length = steps.size();
        int index = 0;
        
        while( current != null && index < length ) {
            String step = steps.get( index );
        
            // Method call
            if( step.endsWith( "()" ) ) {
                Method method = current instanceof Class 
                    ? Reflection.findMethod( (Class) current, step.substring( 0, step.length() - 2 ) )
                    : Reflection.findMethod( current, step.substring( 0, step.length() - 2 ) );
                
                if( method != null ) {
                    current = Reflection.invokeMethod( current instanceof Class ? null : current, method );
                } else {
                    String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                    String message = "Error evaluating '" + currentPath + "' on '" + expression + "': missing " + step + " method on " + Objects.className( current ) + " class"; 
                    throw new ExpressionException( message );
                }
            
            // Evaluate interface
            } else if( current instanceof Evaluate ) {
                return (T)((Evaluate) current).evaluateExpression( new Expression( steps.subList( index, length ), (Modifiers) modifiers.clone() ) );
            
            // Class
            } else if( "class".equalsIgnoreCase( step ) ) {
                current = current.getClass();
                
            // Retrieve interface
            } else if( current instanceof Retrieve ) {
                try {
                    if( allowMissing && current instanceof Inspect && ! ((Inspect) current).containsItem( step ) ) {
                        return (T) InstanceMethod.lookup( current, step );
                    } else {
                        current = ((Retrieve) current).retrieveItem( step );
                    }
                } catch( Exception ex ) {
                    String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                    Exception error = Errors.unwrap( ex );
                    String message = "Error evaluating '" + currentPath + "' on '" + expression + "': " + error.getMessage();
                    if( ex instanceof MissingPropertyException ) {
                        if( allowMissing ) {
                            return (T) InstanceMethod.lookup( current, step );
                        } else {
                            throw new MissingPropertyException( message, current, step );
                        }
                    } else {
                        throw new ExpressionException( message + " (" + error.getClass().getName() + ")", error );
                    }
                }
            
            // Model property
            } else if( current instanceof Modelized ) {
                ModelInstance instance = ((Modelized)current).retrieveModelInstance();
                
                if( instance.knows( step ) ) {
                    current = instance.get( step );
                } else if( allowMissing ) {
                    current = null;
                } else {
                    String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                    throw new MissingPropertyException( "Error evaluating '" + currentPath + "' on '" + expression + "': missing property/method on " + current.getClass().getName() + " instance", current, step );
                }

            // List item    
            } else if( current instanceof List ) {
                try {
                    if( "size".equals( step ) ) {
                        current = ((List) current).size();
                    } else {
                        int listIndex = Integer.parseInt( step );
                        if( listIndex < ((List) current).size() ) {
                            current = ((List) current).get( listIndex );
                        } else if( allowMissing ) {
                            current = null;    
                        } else {
                            String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                            throw new MissingListIndexException( 
                                "Error evaluating '" + currentPath + "' on '" + expression + "': list index out of range"
                                ,root
                                ,expression
                                ,listIndex 
                            );
                        }
                    }
                
                } catch( NumberFormatException ex ) {
                    String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                    throw new NumberFormatException( "Error evaluating '" + currentPath + "' on '" + expression + "': bad number for list index (" + step + ")" );
                    
                }

            // Map item
            } else if( current instanceof Map ) {
                if( ((Map) current).containsKey( step ) ) {
                    current = ((Map) current).get( step );
                } else if( "size".equals( step ) ) {
                    current = ((Map) current).size();
                } else if( allowMissing ) {
                    current = null;
                } else {
                    String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                    throw new MissingMapKeyException( 
                        "Error evaluating '" + currentPath + "' on '" + expression + "': missing key in Map" 
                        ,root
                        ,expression
                        ,step
                    );
                }
                
            // Array item    
            } else if( Collections.isArray( current ) ) {
                try {
                    int arrayIndex = Integer.parseInt( step );
                    if( arrayIndex < Array.getLength( current ) ) {
                        current = Array.get( current, arrayIndex );
                    } else if( allowMissing ) {
                        current = null;
                    } else {
                        String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                        throw new MissingListIndexException(
                            "Error evaluating '" + currentPath + "' on '" + expression + "': array index out of range"
                            ,root
                            ,expression
                            ,arrayIndex
                        );
                    }

                } catch( NumberFormatException ex ) {
                    String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                    throw new NumberFormatException( "Error evaluating '" + currentPath + "' on '" + expression + "': bad number for array index (" + step + ")" );

                }
                
            // Bean property
            } else {
                try {
                    current = Properties.get( current, step );
                } catch( Exception ex ) {
                    boolean recovered = false;
                    if( ex instanceof MissingPropertyException && index == length - 1 ) {
                    
                        if( "size".equals( step ) ) {
                            Object backup = current;
                            try {
                                current = Value.size( current );
                                recovered = true;
                            } catch( Exception ex1 ) {
                                current = backup;
                                recovered = false;
                            }
                        } 
    
                        if( ! recovered ) {
                            InstanceMethod method = InstanceMethod.lookup( current, step );
                            if( method != null ) {
                                current = method;
                                recovered = true;
                            }
                        }
                    } 
                    
                    
                    if( ! recovered ) {
                        String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );
                        Exception error = Errors.unwrap( ex );
                        String message = "Error evaluating '" + currentPath + "' on '" + expression + "': " + error.getMessage();
                        
                        if( ex instanceof MissingPropertyException ) {
                            throw new MissingPropertyException( message, current, step );
                        } else {
                            throw new ExpressionException( message, error );
                        }
                    }
                }
            }
            
            if( current == null && index < length - 1 && ! allowMissing ) {
                String currentPath = Collections.join( steps.subList( 0, index + 1 ), "." );

                throw new NullPropertyPathException( 
                    "Error evaluating '" + currentPath + "' on '" + expression + "': null property" 
                    ,root
                    ,expression
                    ,currentPath
                );
            }
            
            index++;
        }
        
        Object result = Value.from( current );
        
        if( result == null && modifiers.required ) {
            throw new NullPropertyPathException( 
                "Null value evaluating required '" + expression + "' on " + Objects.className( root ) + " instance"
                ,root
                ,expression
            );
        }

        return (T) result;
        
    }

    protected Method lookupMethod( Object current, String step ) {
        String expr = step.replaceAll( " ", "" ).trim();
        if( expr.endsWith( "()" ) ) {
            return Reflection.findMethod( current, expr.substring( 0, expr.length() - 2 ) );
        } else {
            return null;
        }
    }

    public static class EvaluateFunction<T> implements Mapper<Object,T> {
        
        private String expression;
        
        public EvaluateFunction( String expression ) {
            this.expression = expression;
        }
        
        public T map( Object source ) {
            return (T) ExpressionEvaluator.evaluate( source, expression );
        }
    
    }

    public static <T> EvaluateFunction<T> evaluate( String expression ) {
        return new EvaluateFunction<T>( expression );
    }

    public static void valorize( Object root, Expression expression, Object value ) {
        valorize( root, expression.text(), value, expression.steps(), expression.modifiers() );
    }

    public static void valorize( Object root, String expression, Object value ) {
        Modifiers modifiers = parseModifiers( expression );
        valorize( root, modifiers.expression, value, modifiers );
    }
    
    public static void valorize( Object root, String expression, Object value, Modifiers modifiers ) {
        if( root == null ) {
            throw new ExpressionException( "Null root" );

        } else if( Strings.isEmpty( expression ) ) {
            throw new ExpressionException( "Null or empty expression" );
        
        } else {
            if( ! modifiers.optional || knows( root, modifiers.expression, modifiers ) ) {
                valorize( 
                    root
                    ,expression
                    ,value
                    ,parseExpression( expression )
                    ,modifiers
                );
            }
        }
    }
    
    protected static void valorize( Object root, String expression, Object value, List<String> steps, Modifiers modifiers ) {
        Object target = null;
        String member = null;
        boolean createNulls = modifiers.allowNulls;
        boolean unwrapValue = ! modifiers.rawValue;
        
        Object unwrapped = unwrapValue ? Value.from( value ) : value;

        if( steps.size() == 1 ) {
            target = Value.from( root );
            member = steps.get( 0 );
        } else {
            try {

                if( createNulls ) {
                    target = ensure( Value.from( root ), expression, steps.subList( 0, steps.size() - 1 ) );
                } else {
                    target = evaluate( Value.from( root ), expression, steps.subList( 0, steps.size() - 1 ), modifiers );
                }
            
                member = steps.get( steps.size() - 1 );

            } catch( MissingPropertyException ex ) {
                if( ! modifiers.optional ) {
                    throw ex;
                }
            }
        }
        
        if( target != null ) {

            try {
                
                // Assign target    
                if( target instanceof Assign ) {
                    // ((Assign) target).assignExpression( member, value, (Modifiers) modifiers.clone() );
                    ((Assign) target).assignExpression( new Expression( member, (Modifiers) modifiers.clone() ), value );
            
                // Store target    
                } else if( target instanceof Store ) {
                    ((Store) target).storeItem( member, unwrapped );
                
                // Model target
                } else if( target instanceof Modelized ) {
                    ModelInstance instance = ((Modelized)target).retrieveModelInstance();
                
                    if( instance.has( member ) ) {
                        instance.front().set( member, unwrapped );
                    }
                // List target    
                } else if( target instanceof List ) {
                    int listIndex = Integer.parseInt( member );
                    List list = (List)target;
            
                    if( listIndex >= list.size() ) {
                        // Auto expansion
                        for( int i: Range.of( listIndex - list.size() + 1 ) ) {
                            list.add( (Object)null );
                        }
                    } 
            
                    list.set( listIndex, unwrapped );
                
                // Map target    
                } else if( target instanceof Map ) {
                    ((Map) target).put( member, unwrapped );
            
                // Array target    
                } else if( Collections.isArray( target ) ) {
                    int arrayIndex = Integer.parseInt( member );
        
                    if( arrayIndex < Array.getLength( target ) ) {
                        Array.set( target, arrayIndex, unwrapped );
                    } else {
                        String currentPath = Collections.join( steps.subList( 0, steps.size() - 1 ), "." );
                        throw new ExpressionException( "Error setting '" + member + "' on '" + currentPath + "': Array index out of range" );
                    } 

                } else {
                    try {
                        Properties.set( target, member, unwrapped );
                    } catch( Exception ex ) {
                        String currentPath = Collections.join( steps.subList( 0, steps.size() - 1 ), "." );
                        Exception error = Errors.unwrap( ex );
                        throw new ExpressionException( "Error setting '" + member + "' on '" + currentPath + "': " + error.getMessage(), error );
                    }
            
                }
            } catch( MissingPropertyException ex ) {
                if( ! modifiers.optional ) {
                    throw ex;
                }
            }

        } else {
            if( ! modifiers.optional ) {
                String currentPath = Collections.join( steps.subList( 0, steps.size() - 1 ), "." );
                throw new NullPropertyPathException( 
                    "Error setting '" + member + "' on '" + currentPath + "': null property "
                    ,root
                    ,expression
                    ,currentPath 
                );
            }
        }
            
    }

    public static <T> T ensure( Object root, String expression ) {
        
        if( root == null ) {
            throw new ExpressionException( "Null root" );
 
        } else {
            if( Strings.isEmpty( expression ) ) {
                return (T)root;
            } else {
                return ensure( root, expression, parseExpression( expression ) );
            }

        }
        
    }
    
    protected static <T> T ensure( Object root, String expression, List<String> steps ) {
        Object target = root;
        for( String step: steps ) {
            List singleStepList = language.list( step );
            
            try {
                Object value;
                
                try {
                    value = evaluate( target, step, singleStepList, WITHOUT_NULLS );
                } catch( MissingPropertyException mpe ) {
                    value = null;
                }

                if( value == null ) {
                    
                    if( target instanceof Ensure ) {
                        target = ((Ensure) target).ensureExpression( new Expression( step ) );

                    } else {
                        // Model
                        ModelDescriptor model = ModelDescriptor.of( target );
                        if( model.has( step ) ) {
                            try {
                                TypeDefinition type = model.property( step ).getType();
                                //System.out.println( "Creating " + type.getType() );
                                Object created = type.newInstance();
                                valorize( target, step, created, singleStepList, WITH_RAW_WITHOUT_NULLS );
                                target = created;
                            } catch( Exception ex ) {
                                Exception error = Errors.unwrap( ex );
                                throw new ExpressionException( "Error ensuring '" + step + "' on '" + expression + "':" + error.getMessage(), error );
                        
                            }

                        } else {
                            throw new MissingPropertyException( "Error ensuring '" + step + "' on '" + expression + "': missing property on " + Objects.className( target ) + " instance", target, step );
                        }
                    }

                } else {
                    target = value;
                }

            } catch( MissingListIndexException ex ) {
                
                // List
                if( target instanceof Ensure ) {
                    target = ((Ensure) target).ensureExpression( new Expression( step ) );
                    
                } if( target instanceof ModelCollection ) {
                    TypeDefinition type = ((ModelCollection) target).metadata().getItemType();
                    if( type != null ) {
                        Object created = type.newInstance();
                        valorize( target, step, created, singleStepList, WITH_RAW_WITHOUT_NULLS );
                        target = created;
                    } else {
                        throw new ExpressionException( "Error ensuring '" + step + "' on '" + expression + "': index out of range and collection is untyped" );
                    }
                
                } else {
                    throw new ExpressionException( "Error ensuring '" + step + "' on '" + expression + "': index out of range and collection is not described" );
                }

            } catch( MissingMapKeyException ex ) {
                
                // Map
                if( target instanceof Ensure ) {
                    target = ((Ensure) target).ensureExpression( new Expression( step ) );
                
                } else if( target instanceof ModelCollection ) {
                    TypeDefinition type = ((ModelCollection) target).metadata().getItemType();
                    if( type != null ) {
                        Object created = type.newInstance();
                        valorize( target, step, created, singleStepList, WITH_RAW_WITHOUT_NULLS );
                        target = created;
                    } else {
                        throw new ExpressionException( "Error ensuring '" + step + "' on '" + expression + "': map key is missing and collection is untyped" );
                    }
                
                } else {
                    throw new ExpressionException( "Error ensuring '" + step + "' on '" + expression + "': map key is missing and collection is not described" );
                }
            }

        }
        return (T)target;
    }

    public static Object execute( Object root, Expression expression ) {
        return execute( root, expression.text(), expression.steps() );
    }
    
    public static Object execute( Object root, String expression ) {
        if( root == null ) {
            throw new ExpressionException( "Null root" );
        } else {
            if( Strings.isEmpty( expression ) ) {
                throw new ExpressionException( "Null or empty expression" );
            } else {
                return execute( root, expression, parseExpression( expression ) );
            }
        }
    }
    
    protected static Object execute( Object root, String expression, List<String> steps ) {
        Object target = null;
        String member = null;

        if( steps.size() == 1 ) {
            target = root;
            member = steps.get( 0 );
        } else {
            target = evaluate( root, expression, steps.subList( 0, steps.size() - 1 ), WITHOUT_NULLS );
            member = steps.get( steps.size() - 1 );
        }
        
        if( target != null ) {
            Method method = Reflection.findMethod( target.getClass(), member );
            if( method != null ) {
                return Reflection.invokeMethod( target, method );
            } else {
                String currentPath = Collections.join( steps.subList( 0, steps.size() - 1 ), "." );
                throw new NullPropertyPathException( 
                    "Error calling '" + member + "' on '" + currentPath + "': missing method or signature mismatch"
                    ,root
                    ,expression
                    ,currentPath 
                );
            }

        } else {
            String currentPath = Collections.join( steps.subList( 0, steps.size() - 1 ), "." );
            throw new NullPropertyPathException( 
                "Error calling '" + member + "' on '" + currentPath + "': null property"
                ,root
                ,expression
                ,currentPath 
            );
            
        }
    
    }
    
    public static boolean knows( Object root, Expression expression ) {
        return knows( root, expression.text(), expression.steps(), expression.modifiers() );
    }

    public static boolean knows( Object root, String expression ) {
        Modifiers modifiers = parseModifiers( expression );
        return knows( root, modifiers.expression, modifiers );
    }

    public static boolean knows( Object root, String expression, Modifiers modifiers ) {
        if( root == null ) {
            throw new ExpressionException( "Null root" );
        } else {
            if( Strings.isEmpty( expression ) ) {
                throw new ExpressionException( "Null or empty expression" );
            } else {
                return knows( root, expression, parseExpression( expression ), modifiers );
            }
        }
    }
    
    protected static boolean knows( Object root, String expression, List<String> steps, Modifiers modifiers ) {
        Object target = null;
        String member = null;

        if( steps.size() == 1 ) {
            target = Value.from( root );
            member = steps.get( 0 );

        } else {
            try {
                target = evaluate( Value.from( root ), expression, steps.subList( 0, steps.size() - 1 ), WITHOUT_NULLS );
            } catch( MissingPropertyException ex ) {
                return false;
            }
            member = steps.get( steps.size() - 1 );
        }

        if( target != null ) {
            if( target instanceof Inspect ) {
                return ((Inspect) target).containsItem( member );
            
            } else if( target instanceof Modelized ) {
                return ((Modelized) target).retrieveModelInstance().has( member );
            
            } else if( target instanceof Map ) {
                return ((Map) target).containsKey( member );
            
            } else if( target instanceof List ) {
                try {
                    return Integer.parseInt( member ) < ((List) target).size();
                } catch( NumberFormatException nfe ) {
                    String currentPath = Collections.join( steps, "." );
                    throw new NumberFormatException( "Error inspecting '" + member + "' on '" + currentPath + "': bad number for list index" );
                }
            } else {
                try {
                    return Properties.has( target, member );
                } catch( Exception ex ) {
                    String currentPath = Collections.join( steps, "." );
                    throw new ExpressionException( "Error inspecting '" + member + "' on '" + currentPath + "': " + ex.getMessage(), ex );
                }
                
            }
            
        } else {

            if( modifiers.allowNulls || modifiers.optional ) {
                return false;

            } else {
                String currentPath = Collections.join( steps.subList( 0, steps.size() - 1 ), "." );
                throw new NullPropertyPathException( 
                    "Error inspecting '" + member + "' for '" + currentPath + "': null property "
                    ,root
                    ,expression
                    ,currentPath 
                );
            }
        
        }
        
    }

    public static boolean isExpression( Object expression ) {
       if( expression instanceof String ) {
           char[] chars = Strings.raw( expression );
           int len = chars.length;
           boolean started = false;
           for( int i = 0; i < len; i++ ) {
               char ch = chars[ i ];
               if( ch == '.' 
                   || ch == '['
                   || ch == '(' ) {
                    return true;

               } else if( ! started && (
                   ch == '?'
                   || ch == '!'
                   || ch == '&'
                   || ch == '+'
                   || ch == '/' )) {
                   return true;
               
               } else {
                   started = true;
               }
           }
           
           return false;
       
       } else {
           return expression instanceof Expression;
       }
    }

    public static Expression parse( String expression ) {
        Modifiers modifiers = parseModifiers( expression );
        return new Expression( expression, parseExpression( modifiers.expression ), modifiers );
    }

    protected static Modifiers parseModifiers( String expression ) {
        Modifiers modifiers = new Modifiers();
        int cut = 0;

        if( expression != null ) {
            boolean terminated = false;
            int i = 0;
            int len = expression.length();
            while( ! terminated && i < len ) {
                char ch = expression.charAt( i );

                if( ch == '!' ) {
                    modifiers.allowNulls = true;
                    cut++;
                } else if( ch == '&' ) {
                    modifiers.rawValue = true;
                    cut++;
                } else if( ch == '?' ) {
                    modifiers.optional = true;
                    cut++;
                } else if( ch == '+' ) {
                    modifiers.required = true;
                    cut++;
                } else if( ch == '\\' && i == 0 ) {
                    terminated = true;
                    cut++;
                } else {
                    terminated = true;
                }
                i++;
            }
        }
        
        modifiers.expression = cut > 0 ? expression.substring( cut ) : expression;
        return modifiers;
    }
    
    protected static List<String> parseExpression( String expression ) {
        if( isExpression( expression ) ) {
            List<String> result = new ArrayList<String>( 16 );
            char[] buffer = expression.toCharArray();
            char[] current = new char[ 256 ];
            boolean inBracket = false;
            boolean inWord = true;
            boolean collect = false;
            int index = 0;
            for( int i = 0; i < buffer.length; i++ ){
                switch( buffer[ i ] ){
                    case '[':
                        if( !inBracket ) {
                            if( index > 0 ){
                                collect = true;
                            }
                            inBracket = true;
                            inWord = true;
                        } else {
                            throw new ExpressionException( "Unexpected bracket opening at " + ( i + 1 ) );
                        }
                        break;
                    case ']':
                        if( inBracket ){
                            collect = true;
                            inBracket = false;
                            inWord = false;
                        } else {
                            throw new ExpressionException( "Unexpected bracket closing at " + ( i + 1 ) );
                        }
                        break;
                    case '.':
                        if( !inBracket ) {
                            if( !inWord ) {
                                inWord = true;
                            } else if( index > 0 ) {
                                collect = true;
                            } else {
                                throw new ExpressionException( "Unexpected dot at " + ( i + 1 ) );
                            }
                    
                        } else {
                            current[ index ] = buffer[ i ];
                            index++;
                        }
                        break;
                    default:
                        if( inWord ) {
                            current[ index ] = buffer[ i ];
                            index++;
                        } else {
                            throw new ExpressionException( "Unexpected character at " + ( i + 1 ) );
                        }
                }
                // Last word
                if( i == buffer.length -1 ) {
                    collect = true;
                }
            
                if( collect ) {
                    String word = new String( current, 0, index ).trim();
                    if( word.length() > 0 ) {
                        if( word.startsWith( "'" ) ) {
                            if( word.endsWith( "'" ) ) {
                                word = word.substring( 1, word.length() - 1 );
                            } else {
                                throw new ExpressionException( "Unbalanced single quotes at " + ( i - index + 1 ) );
                            }
                        } else if( word.startsWith( "\"" ) ) {
                            if( word.endsWith( "\"" ) ) {
                                word = word.substring( 1, word.length() - 1 );
                            } else {
                                throw new ExpressionException( "Unbalanced double quotes at " + ( i - index + 1 ) );
                            }
                        }
                        result.add( word );
                        index = 0;
                        collect = false;
                    } else {
                        throw new ExpressionException( "Empty keyword at " + ( i - index + 1 ) );
                    }
                
                }
            }
        
            return result;
            
        } else {
            return language.list( expression );
        }
        
    }
    
    public static boolean isTemplate( Object expression ) {
        return Template.isTemplate( expression );
    }

    public static String apply( Object root, String template ) {
        return new Template( template ).apply( root );
    }
    
    public static String apply( Object root, Template template ) {
        return template.apply( root );
    }
    
}