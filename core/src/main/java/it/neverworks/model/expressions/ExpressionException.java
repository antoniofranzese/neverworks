package it.neverworks.model.expressions;

import it.neverworks.model.ModelException;

public class ExpressionException extends ModelException {
    
    public ExpressionException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public ExpressionException( String message ){
        super( message );
    }
}