package it.neverworks.model.expressions;

import it.neverworks.model.Value;

public class NotIsCriteria extends SingleValueCriteria {
    
    public NotIsCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return ! Value.is( target, value );
    }

}