package it.neverworks.model.expressions;

import it.neverworks.lang.Strings;

public class NullableFormat implements Format<Object> {
    
    public String format( Object value ) {
        if( value != null ) {
            return Strings.valueOf( value );
        } else {
            return "";
        }
    }
    
    public String toString() {
        return "StringFormat()";
    }
}

