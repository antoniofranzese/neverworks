package it.neverworks.model.expressions;

import java.util.regex.Pattern;
import it.neverworks.model.Value;

public class ObjectCriteriaBuilder<T> {
    
    private ObjectQuery<T> query;
    private String expression;
    
    public ObjectCriteriaBuilder( ObjectQuery<T> query, String expression ) {
        this.query = query;
        this.expression = expression;
    }
    
    public ObjectQuery<T> eq( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new EqualsCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> isFalse() {
        return eq( false );
    }

    public ObjectQuery<T> isTrue() {
        return eq( true );
    }

    public ObjectQuery<T> ne( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new NotEqualsCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> gt( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new GreaterThanCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> ge( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new GreaterEqualCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> lt( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new LesserThanCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> le( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new LesserEqualCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> matches( Value value ) {
        if( value.is( Pattern.class ) ) {
            return matches( value.as( Pattern.class ) );
        } else {
            return matches( value.toString() );
        }
    }
    
    public ObjectQuery<T> matches( String value ) {
        return matches( value != null ? Pattern.compile( value ) : null );
    }

    public ObjectQuery<T> matches( Pattern value ) {
        if( value != null ) query.add( new MatchesCriteria( expression, value ) );
        return query;
    }

    public ObjectQuery<T> notMatches( Value value ) {
        if( value.is( Pattern.class ) ) {
            return notMatches( value.as( Pattern.class ) );
        } else {
            return notMatches( value.toString() );
        }
    }

    public ObjectQuery<T> notMatches( String value ) {
        return notMatches( value != null ? Pattern.compile( value ) : null );
    }

    public ObjectQuery<T> notMatches( Pattern value ) {
        if( value != null ) query.add( new NotMatchesCriteria( expression, value ) );
        return query;
    }

    public ObjectQuery<T> is( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new IsCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> notIs( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new NotIsCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> isNot( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new NotIsCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> exact( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new SameReferenceCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> notExact( Object value ) {
        Object v = Value.from( value );
        if( v != null ) query.add( new NotSameReferenceCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> isNull() {
        query.add( new NullCriteria( expression ) );
        return query;
    }

    public ObjectQuery<T> notNull() {
        query.add( new NotNullCriteria( expression ) );
        return query;
    }
    
    public ObjectQuery<T> isEmpty() {
        query.add( new EmptyCriteria( expression ) );
        return query;
    }

    public ObjectQuery<T> notEmpty() {
        query.add( new NotEmptyCriteria( expression ) );
        return query;
    }

    public ObjectQuery<T> contains( Object... values ) {
        Object[] v = Value.from( values );
        if( v != null && v.length > 0 ) query.add( new ContainsCriteria( expression, v ) );
        return query;
    }

    public ObjectQuery<T> notContains( Object... values ) {
        Object[] v = Value.from( values );
        if( v != null && v.length > 0 ) query.add( new NotContainsCriteria( expression, v ) );
        return query;
    }
 
    public ObjectQuery<T> instanceOf( Class<?> value ) {
        if( value != null ) query.add( new InstanceCriteria( expression, value ) );
        return query;
    }

    public ObjectQuery<T> notInstanceOf( Class<?> value ) {
        if( value != null ) query.add( new NotInstanceCriteria( expression, value ) );
        return query;
    }

    public ObjectQuery<T> isMissing() {
        query.add( new MissingCriteria( expression ) );
        return query;
    }

    public ObjectQuery<T> isKnown() {
        query.add( new NotMissingCriteria( expression ) );
        return query;
    }

    public ObjectQuery<T> notMissing() {
        return isKnown();
    }
 
    public ObjectQuery<T> sub( SubEnquirer enquirer ) {
        if( enquirer != null ) query.add( new SubQueryCriteria( expression, enquirer ) );
        return query;
    }
    
}
