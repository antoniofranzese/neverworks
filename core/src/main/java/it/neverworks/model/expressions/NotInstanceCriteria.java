package it.neverworks.model.expressions;

public class NotInstanceCriteria extends SingleValueCriteria {
    
    public NotInstanceCriteria( String expression, Class value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target == null || ! ((Class) value).isInstance( target );
    }

}