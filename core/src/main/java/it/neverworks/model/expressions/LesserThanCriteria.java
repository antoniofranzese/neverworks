package it.neverworks.model.expressions;

public class LesserThanCriteria extends ComparatingCriteria {
    
    public LesserThanCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target != null && value != null && compare( target, value ) < 0;
    }

}