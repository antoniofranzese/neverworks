package it.neverworks.model.expressions;

public interface Criteria {
    boolean matches( Object source );
}