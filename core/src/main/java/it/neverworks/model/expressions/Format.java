package it.neverworks.model.expressions;

public interface Format<T> {
    String format( T value );
}