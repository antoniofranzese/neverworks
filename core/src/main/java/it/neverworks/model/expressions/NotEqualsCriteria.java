package it.neverworks.model.expressions;

public class NotEqualsCriteria extends SingleValueCriteria {
    
    public NotEqualsCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target != null && !target.equals( value );
    }

}