package it.neverworks.model.expressions;

import it.neverworks.model.Value;

public class EmptyCriteria implements Criteria {
    
    protected String expression;
    
    public EmptyCriteria( String expression ){
        this.expression = expression;
    }
    
    public boolean matches( Object source ) {
        return Value.isEmpty( ExpressionEvaluator.evaluate( source, expression ) );
    }

}