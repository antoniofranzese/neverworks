package it.neverworks.model.expressions;

import it.neverworks.model.Value;

public abstract class ComparatingCriteria extends SingleValueCriteria {
    
    public ComparatingCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    protected int compare( Object target, Object value ) {
        return Value.compare( target, value );
    }

}