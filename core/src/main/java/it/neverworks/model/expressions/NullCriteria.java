package it.neverworks.model.expressions;

public class NullCriteria implements Criteria{
    
    protected String expression;
    
    public NullCriteria( String expression ){
        this.expression = expression;
    }
    
    public boolean matches( Object source ) {
        return ExpressionEvaluator.evaluate( source, expression ) == null;
    }

}