package it.neverworks.model.expressions;

import it.neverworks.model.utils.ToStringBuilder;

public class Modifiers {
    
    public final static Modifiers WITH_NULLS = new Modifiers().withNulls().freeze();
    
    /* package */ boolean allowNulls = false;
    /* package */ boolean rawValue = false;
    /* package */ boolean optional = false;
    /* package */ boolean required = false;
    /* package */ String expression;
    
    /* package */ boolean frozen = false;
    
    public Modifiers freeze() {
        this.frozen = true;
        return this;
    }
    
    public boolean nulls() {
        return allowNulls;
    }
    
    public boolean raw() {
        return rawValue;
    }
    
    public boolean optional() {
        return optional;
    }

    public boolean required() {
        return required;
    }
    
    public String expression() {
        return expression;
    }
    
    public Modifiers withNulls() {
        if( ! this.frozen ) { this.allowNulls = true; }
        return this;
    }

    public Modifiers withoutNulls() {
        if( ! this.frozen ) { this.allowNulls = false; }
        return this;
    }

    public Modifiers withRaw() {
        if( ! this.frozen ) { this.rawValue = true; }
        return this;
    }

    public Modifiers withoutRaw() {
        if( ! this.frozen ) { this.rawValue = false; }
        return this;
    }

    public Modifiers withOptional() {
        if( ! this.frozen ) { this.optional = true; }
        return this;
    }

    public Modifiers withoutOptional() {
        if( ! this.frozen ) { this.optional = false; }
        return this;
    }

    public Modifiers withRequired() {
        if( ! this.frozen ) { this.required = true; }
        return this;
    }

    public Modifiers withoutRequired() {
        if( ! this.frozen ) { this.required = false; }
        return this;
    }

    public Object clone() {
        Modifiers cloned = new Modifiers();
        cloned.allowNulls = this.allowNulls;
        cloned.rawValue = this.rawValue;
        cloned.optional = this.optional;
        cloned.required = this.required;
        cloned.expression = this.expression;
        return cloned;
    }
        
    public String toString() {
        return new ToStringBuilder( this )
            .add( "nulls", this.allowNulls )
            .add( "optional", this.optional )
            .add( "raw", this.rawValue )
            .add( "required", this.required )
            .toString();
    }
}
    
