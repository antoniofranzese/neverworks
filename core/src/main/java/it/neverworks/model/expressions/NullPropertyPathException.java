package it.neverworks.model.expressions;

public class NullPropertyPathException extends ExpressionException {
    
    public NullPropertyPathException( String message, Object root, String expression, String path ){
        super( message );
        this.expression = expression;
        this.path = path;
        this.root = root;
    }

    public NullPropertyPathException( String message, Object root, String expression ){
        this( message, root, expression, expression );
    }
    
    private String expression;
    private String path;
    private Object root;
    
    public Object getRoot(){
        return this.root;
    }
    
    public void setRoot( Object root ){
        this.root = root;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
    
    public String getExpression(){
        return this.expression;
    }
    
    public void setExpression( String expression ){
        this.expression = expression;
    }
}