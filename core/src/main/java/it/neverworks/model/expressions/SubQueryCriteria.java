package it.neverworks.model.expressions;

import static it.neverworks.language.*;
import it.neverworks.lang.Collections;

public class SubQueryCriteria implements Criteria {
    
    private String expression;
    private SubEnquirer enquirer;
    private Iterable source;
    
    public SubQueryCriteria( String expression, SubEnquirer enquirer ) {
        this.expression = expression;
        this.enquirer = enquirer;
    }

    public SubQueryCriteria( Iterable source, SubEnquirer enquirer ) {
        this.source = source;
        this.enquirer = enquirer;
    }
    
    public boolean matches( Object source ) {
        return this.enquirer.enquire( new ObjectQuery( this.source != null ? this.source : Collections.iterable( eval( source, expression ) ) ) );
    }

}