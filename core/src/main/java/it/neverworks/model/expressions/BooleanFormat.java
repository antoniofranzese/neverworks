package it.neverworks.model.expressions;

import it.neverworks.lang.Strings;

public class BooleanFormat implements Format<Boolean> {
    
    private String trueFormat = "";
    private String falseFormat = "";
    
    public BooleanFormat( String pattern ) {
        if( Strings.hasText( pattern ) ) {
            for( String element: pattern.split( "\\|" ) ) {
                String[] parts = element.split( "=" );
                if( parts.length == 2 ) {
                    if( "true".equalsIgnoreCase( parts[0].trim() ) ) {
                        this.trueFormat = parts[ 1 ];
                    } else if( "false".equalsIgnoreCase( parts[0].trim() ) ) {
                        this.falseFormat = parts[ 1 ];
                    }
                }
            }
        }
        
    }
    
    public String format( Boolean value ) {
        if( Boolean.TRUE.equals( value ) ) {
            return this.trueFormat;
        } else {
            return this.falseFormat;
        }
    }
    
    public String toString() {
        return "BooleanFormat(true='"+ trueFormat + "', false='" + falseFormat + "')";
    }
}

