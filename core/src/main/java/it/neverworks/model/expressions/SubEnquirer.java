package it.neverworks.model.expressions;

public interface SubEnquirer<T> {
    boolean enquire( ObjectQuery<T> query );
}