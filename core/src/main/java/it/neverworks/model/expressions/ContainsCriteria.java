package it.neverworks.model.expressions;

import java.util.Collection;
import java.util.Arrays;
import java.util.Map;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.Value;

public class ContainsCriteria implements Criteria {
    
    protected String expression;
    protected Object[] values;
    
    public ContainsCriteria( String expression, Object... values ){
        this.expression = expression;
        this.values = values;
    }
    
    public boolean matches( Object source ){
        Object target = ExpressionEvaluator.evaluate( source, expression );

        if( target != null ) {
            for( Object value: values ){
                if( ! Value.contains( target, value ) ) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
        
    }
    
}