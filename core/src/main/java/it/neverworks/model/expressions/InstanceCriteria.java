package it.neverworks.model.expressions;

public class InstanceCriteria extends SingleValueCriteria {
    
    public InstanceCriteria( String expression, Class value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target != null && ((Class) value).isInstance( target );
    }

}