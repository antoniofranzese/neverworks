package it.neverworks.model.expressions;

public class EqualsCriteria extends SingleValueCriteria {
    
    public EqualsCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        if( target != null && value != null ) {
            return target.equals( value );
        } else {
            return target == null && value == null;
        }
    }

}