package it.neverworks.model.expressions;

public class MissingMapKeyException extends ExpressionException {
    
    public MissingMapKeyException( String message, Object root, String expression, String key ){
        super( message );
        this.expression = expression;
        this.key = key;
        this.root = root;
    }
    
    private String expression;
    private Object root;
    private String key;
    
    public String getKey(){
        return this.key;
    }
    
    public void setKey( String key ){
        this.key = key;
    }
        
    public Object getRoot(){
        return this.root;
    }
    
    public void setRoot( Object root ){
        this.root = root;
    }
    
    public String getExpression(){
        return this.expression;
    }
    
    public void setExpression( String expression ){
        this.expression = expression;
    }
}