package it.neverworks.model.expressions;

public class NotMissingCriteria implements Criteria {
    
    protected String expression;
    
    public NotMissingCriteria( String expression ){
        this.expression = expression;
    }
    
    public boolean matches( Object source ) {
        return ExpressionEvaluator.knows( source, expression, new Modifiers().withNulls() );
    }

}