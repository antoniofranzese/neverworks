package it.neverworks.model.expressions;

public class NotNullCriteria implements Criteria {
    
    protected String expression;
    
    public NotNullCriteria( String expression ){
        this.expression = expression;
    }
    
    public boolean matches( Object source ) {
        return ExpressionEvaluator.evaluate( source, expression ) != null;
    }

}