package it.neverworks.model.expressions;

import java.util.Collection;
import java.util.Arrays;
import java.util.Map;
import it.neverworks.model.features.Inspect;

public class NotContainsCriteria implements Criteria {
    
    protected String expression;
    protected Object[] values;
    
    public NotContainsCriteria( String expression, Object... values ){
        this.expression = expression;
        this.values = values;
    }
    
    public boolean matches( Object source ){
        Object target = ExpressionEvaluator.evaluate( source, expression );

        if( target != null && target.getClass().isArray() ) {
            target = Arrays.asList( (Object[]) target );
        }
        
        if( target instanceof Inspect ){
            for( Object value: values ){
                if( ((Inspect)target).containsItem( value  ) ) {
                    return false;
                }
            }
            return true;
            
        } else if( target instanceof Collection ){
            for( Object value: values ){
                if( ((Collection)target).contains( value  ) ) {
                    return false;
                }
            }
            return true;
            
        } else if( target instanceof Map ) {
            for( Object value: values ){
                if( ((Map)target).containsKey( value ) ) {
                    return false;
                }
            }
            return true;
            
        } else {
            return false;
        }
        
    }

}