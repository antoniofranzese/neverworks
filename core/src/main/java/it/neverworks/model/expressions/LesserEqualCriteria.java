package it.neverworks.model.expressions;

public class LesserEqualCriteria extends ComparatingCriteria {
    
    public LesserEqualCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target != null && value != null && compare( target, value ) <= 0;
    }

}