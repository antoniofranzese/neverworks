package it.neverworks.model.expressions;

import it.neverworks.lang.Filter;

public class FilterCriteria implements Criteria {
    
    private Filter filter;
    
    public FilterCriteria( Filter filter ) {
        this.filter = filter;
    }
    
    public boolean matches( Object source ) {
        return this.filter.filter( source );
    }
    
}