package it.neverworks.model.expressions;

public class NotSameReferenceCriteria extends SingleValueCriteria {
    
    public NotSameReferenceCriteria( String expression, Object value ){
        super( expression, value );
    }
    
    public boolean matches( Object target, Object value ){
        return target == null || target != value;
    }

}