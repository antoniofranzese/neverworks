package it.neverworks.model.expressions;

import it.neverworks.model.ModelException;

public class QueryException extends ModelException {
    
    public QueryException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public QueryException( String message ){
        super( message );
    }
}