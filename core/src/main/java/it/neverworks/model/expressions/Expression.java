package it.neverworks.model.expressions;

import java.util.List;
import java.util.ArrayList;
import it.neverworks.lang.Collections;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.Strings;

public class Expression {
    
    private String text;
    private Modifiers modifiers;
    private List<String> steps;
    
    public Expression( String text, List<String> steps, Modifiers modifiers ) {
        this.text = text;
        this.modifiers = modifiers;
        this.steps = steps;
    }

    public Expression( List<String> steps, Modifiers modifiers ) {
        this.text = null;
        this.modifiers = modifiers;
        this.steps = steps;
    }

    public Expression( String step, Modifiers modifiers ) {
        this.text = step;
        this.modifiers = modifiers;
        this.steps = null;
    }

    public Expression( String step ) {
        this( step, null );
    }
    
    public Modifiers modifiers() {
        if( modifiers == null ) {
            modifiers = new Modifiers();
        }
        return this.modifiers;
    }
    
    public String pure() {
        return modifiers != null && Strings.hasText( modifiers.expression ) ? modifiers.expression : text();
    }
    
    public String text() {
        if( this.text == null ) {
            StringBuilder txt = new StringBuilder();
            boolean started = false;
            for( String step: this.steps ){
                if( step.indexOf( " " ) >=0 ) {
                    txt.append( "['" ).append( step ).append( "']" ); 
                } else if( Numbers.isNumber( step ) ) {
                    txt.append( "[" ).append( step ).append( "]" ); 
                } else {
                    if( started ) {
                        txt.append( "." );
                    }
                    txt.append( step );
                }
                started = true;
            }
            text = txt.toString();
        }
        return text;
    }
    
    public List<String> steps() {
        if( steps == null ) {
            steps = new ArrayList<String>();
            steps.add( text );
        }
        return steps;
    }
    
    public String step( int i ) {
        return steps().get( i );
    }
    
    public int size() {
        return steps().size();
    }
    
    public Expression sub( int start ) {
        return sub( start, size() );
    }
    
    public Expression sub( int start, int end ) {
        List<String> steps = this.steps.subList( start, end );
        return new Expression( steps, this.modifiers );
    }
    
    public <T> T evaluate( Object root ) {
        return ExpressionEvaluator.evaluate( root, this );
    }
    
    public void valorize( Object root, Object value ) {
        ExpressionEvaluator.valorize( root, this, value );
    }
    
    public String toString() {
        return text();
    }
}