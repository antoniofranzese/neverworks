package it.neverworks.model.expressions;

import it.neverworks.model.converters.Converter;

public class TemplateConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof Template ) {
            return value;
        } else if( value instanceof String ) {
            return new Template( (String) value );
        } else {
            return value;
        }
    }
}