package it.neverworks.model.expressions;

import it.neverworks.model.Value;

public class NotEmptyCriteria extends EmptyCriteria {
    
    public NotEmptyCriteria( String expression ){
        super( expression );
    }
    
    public boolean matches( Object source ) {
        return ! Value.isEmpty( ExpressionEvaluator.evaluate( source, expression ) );
    }

}