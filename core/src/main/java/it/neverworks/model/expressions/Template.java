package it.neverworks.model.expressions;

import static it.neverworks.language.*;

import java.util.List;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.Locale;
import java.io.Writer;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;
import it.neverworks.io.StringWriter;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.features.Text;
import it.neverworks.model.Value;
import it.neverworks.i18n.Translation;
import it.neverworks.text.TextFormatterRegistry;
import it.neverworks.text.ConfigurableFormatter;
import it.neverworks.text.LocalizableFormatter;
import it.neverworks.text.TextFormatter;
import it.neverworks.text.TextParser;

@Convert( TemplateConverter.class )
public class Template {

    private List<Chunk> chunks;
    
    public Template( Object expression ) {
        this( Value.asText( expression ) );
    }
    
    public Template( String expression ) {
        this.chunks = new TemplateParser().process( expression );
        setModifiers( new Modifiers() );
    }
    
    public Template lenient() {
        setModifiers( new Modifiers().withNulls().withOptional() );
        return this;
    }
    
    protected void setModifiers( Modifiers modifiers ) {
        each( chunks )
            .filter( c -> c instanceof ExpressionChunk )
            .inspect( c -> ((ExpressionChunk) c).modifiers = modifiers );
    }
    
    public void apply( Writer writer, Object root ) {
        apply( writer, Translation.current(), root );
    }

    public void apply( Writer writer, Locale locale, Object root ) {
        if( root != null ) {
            try {
                for( Chunk chunk: this.chunks ) {
                    chunk.apply( writer, locale, root );
                }
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        }
    }
    
    public String apply( Object root ) {
        return apply( Translation.current(), root );
    }

    public String apply( Locale locale, Object root ) {
        if( root != null ) {
            StringWriter writer = new StringWriter();
            apply( writer, locale, root );
            return writer.toString();
        } else {
            return null;
        }
    }

    public static boolean isTemplate( Object expression ) {
        return expression instanceof String && ( ((String) expression).indexOf( "{" ) >= 0 && ((String) expression).indexOf( "}" ) >= 0 );
    }

    private static interface Chunk {
        public void apply( Writer writer, Locale locale, Object root ) throws Exception;
    }
    
    private static class TextChunk implements Chunk {
        private String text;
        
        public void apply( Writer writer, Locale locale, Object root ) throws Exception {
            writer.write( text );
        }
    }
    
    private static class ExpressionChunk implements Chunk {
        private String expression;
        private TextFormatter formatter;
        private Modifiers modifiers;
        
        public void apply( Writer writer, Locale locale, Object root ) throws Exception {
            if( formatter instanceof LocalizableFormatter ) {
                writer.write( ((LocalizableFormatter) formatter).format( locale, ExpressionEvaluator.evaluate( root, expression, modifiers ) ) );
            } else {
                writer.write( formatter.format( ExpressionEvaluator.evaluate( root, expression, modifiers ) ) );
            }
        }
        
    }
    
    private static class TemplateParser extends TextParser {
        
        List<Chunk> chunks;
        
        public List process( String expression ) {
            this.chunks = new ArrayList<Chunk>();
            parse( expression );
            return this.chunks;
        }
        
        public void onText( String text ) {
            TextChunk chunk = new TextChunk();
            chunk.text = text;
            this.chunks.add( chunk );
        }
    
        public void onExpression( String path, String format, String spec ) {
            ExpressionChunk chunk = new ExpressionChunk();
            chunk.expression = Strings.safe( path ).trim();
            chunk.formatter = TextFormatterRegistry.getConfiguredFormatter( format, spec );
            this.chunks.add( chunk );
        }
        
    }

    public String toString() {
        return this.getClass().getSimpleName();
    }
}
