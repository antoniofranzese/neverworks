package it.neverworks.model.expressions;

public class MissingListIndexException extends ExpressionException {
    
    public MissingListIndexException( String message, Object root, String expression, int index ){
        super( message );
        this.expression = expression;
        this.index = index;
        this.root = root;
    }
    
    private String expression;
    private Object root;
    private int index;
    
    public int getIndex(){
        return this.index;
    }
    
    public void setIndex( int index ){
        this.index = index;
    }
        
    public Object getRoot(){
        return this.root;
    }
    
    public void setRoot( Object root ){
        this.root = root;
    }
    
    public String getExpression(){
        return this.expression;
    }
    
    public void setExpression( String expression ){
        this.expression = expression;
    }
}