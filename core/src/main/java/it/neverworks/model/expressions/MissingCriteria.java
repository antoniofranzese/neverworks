package it.neverworks.model.expressions;

public class MissingCriteria implements Criteria {
    
    protected String expression;
    
    public MissingCriteria( String expression ){
        this.expression = expression;
    }
    
    public boolean matches( Object source ) {
        return !( ExpressionEvaluator.knows( source, expression, new Modifiers().withNulls() ) );
    }

}