package it.neverworks.model.expressions;

public abstract class SingleValueCriteria implements Criteria {
    
    protected String expression;
    protected Object value;

    public SingleValueCriteria( String expression, Object value ){
        this.expression = expression;
        this.value = value;
    }
    
    public boolean matches( Object source ) {
        return matches( ExpressionEvaluator.evaluate( source, expression ), value );
    }

    public abstract boolean matches( Object target, Object value );


}