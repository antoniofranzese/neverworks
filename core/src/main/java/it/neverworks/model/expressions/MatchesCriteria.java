package it.neverworks.model.expressions;

import java.util.regex.Pattern;
import it.neverworks.lang.Strings;

public class MatchesCriteria extends SingleValueCriteria {
    
    public MatchesCriteria( String expression, Pattern value ){
        super( expression, value );
    }

    public boolean matches( Object target, Object value ){
        return target != null && ((Pattern) value).matcher( Strings.valueOf( target ) ).matches();
    }

}