package it.neverworks.model;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;

public class BaseModel extends CoreModel {
    
    public BaseModel() {
        super();
    }
    
    public BaseModel( Arguments arguments ) {
        this();
        set( arguments );
    }
    
    public <T extends BaseModel> T set( String name, Object value ) {
        return (T) this.modelInstance.assign( name, value ).actual();
    }

    public <T extends BaseModel> T set( Arguments arguments ) {
        return (T) this.modelInstance.assign( arguments ).actual();
    }

    public <T extends BaseModel> T set( String arguments ) {
        return (T) this.modelInstance.assign( arguments ).actual();
    }

    public <T extends BaseModel> T set( Tuple tuple, String... names ) {
        return (T) this.modelInstance.assign( tuple, names ).actual();
    }
    
}