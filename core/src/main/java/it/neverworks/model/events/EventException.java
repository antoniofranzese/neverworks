package it.neverworks.model.events;

public class EventException extends RuntimeException {
    
    public EventException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public EventException( String message ){
        super( message );
    }
}