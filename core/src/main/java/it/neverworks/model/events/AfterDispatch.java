package it.neverworks.model.events;

public interface AfterDispatch {
    void doAfterDispatch( Signal signal, Event event );
}