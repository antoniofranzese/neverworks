package it.neverworks.model.events;

public interface Subscription {
    
    public void remove();
    public boolean isRemoved();

}