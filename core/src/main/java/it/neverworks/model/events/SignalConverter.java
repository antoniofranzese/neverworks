package it.neverworks.model.events;

import java.util.Collection;

import it.neverworks.lang.Objects;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.InstanceConverter;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.PropertyManager;

public class SignalConverter implements Converter, InstanceConverter, PropertyAware {

    private PropertyManager property;

    public void setPropertyDescriptor( PropertyDescriptor property ){
        this.property = (PropertyManager)property;
    }
    
    public Object convert( Object value ) {
        return value;
    }

    public Object convert( ModelInstance instance, Object value ) {
        Signal signal = (Signal) this.property.getDirectValue( instance );
        return convert( signal, instance, value );
    }
    
    public Object convert( Signal signal, ModelInstance instance, Object value ) {
        
        if( value == signal ) {
            return signal;
            
        } else if( value instanceof Dispatcher ) {
            // System.out.println( "Instance Converting " + value + " on " + property.getFullName() );
            signal.add( (Dispatcher)value );
            return signal;
            
        } else if( value instanceof Collection ) {
            for( Object item: ((Collection) value) ) {
                Object result = convert( signal, instance, item );
                
                if( !( result instanceof Signal ) ) {
                    throw new EventException( "Unknown dispatcher type in collection: " + Objects.repr( item ) );
                }
            }
            return signal;
            
        } else if( value instanceof String ) {
            signal.add( new DispatcherHook( instance.actual(), (String) value ) );
            return signal;
            
        } else {
            return value;
        }
    }
    
}