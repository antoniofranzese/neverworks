package it.neverworks.model.events;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Reflection;
import it.neverworks.lang.Objects;

public class Router implements Dispatcher {
    
    private final static Class TOP_CLASS = Event.class.getSuperclass();
    
    private Map<Class<?>, Signal> routes = new HashMap<Class<?>, Signal>();
    private Map<Class<?>, Signal> handlers = new HashMap<Class<?>, Signal>();
    
    public void dispatch( Event event ) {
        retrieveHandler( event ).fire( event );
    }
    
    public Subscription add( Class<?> eventClass, Dispatcher dispatcher ) {
        return retrieveRoute( eventClass ).add( dispatcher );
    }
    
    public boolean contains( Class<?> eventClass, Dispatcher dispatcher ) {
        return retrieveRoute( eventClass ).contains( dispatcher );
    }
    
    public void remove( Class<?> eventClass, Dispatcher dispatcher ) {
        retrieveRoute( eventClass ).remove( dispatcher );
    }
    
    public Signal retrieveRoute( Class<?> eventClass ) {
        if( ! routes.containsKey( eventClass ) ) {
            synchronized( this ) {
                if( ! routes.containsKey( eventClass ) ) {
                    routes.put( eventClass, new Signal( Event.class.isAssignableFrom( eventClass ) ? eventClass : Event.class ) );
                    handlers.clear();
                }
            }
        }
        return routes.get( eventClass );
    }

    protected Signal retrieveHandler( Event event ) {
        Class eventClass = event.getClass();
        
        if( ! handlers.containsKey( eventClass ) ) {
            synchronized( this ) {
                if( ! handlers.containsKey( eventClass ) ) {
                    
                    List<Signal> matches = new ArrayList<Signal>();
                    
                    for( Class type: routes.keySet() ) {
                        if( type.isAssignableFrom( eventClass ) ) {
                            matches.add( routes.get( type ) );
                        }
                    }
            
                    Signal handler;

                    if( matches.size() == 1 ) {
                        handler = matches.get( 0 );
                    } else {
                        handler = new Signal( eventClass );
                        for( Signal match: matches ) {
                            handler.add( match );
                        }
                    }
                    
                    handlers.put( eventClass, handler );
                }
            }
        }
        
        return handlers.get( eventClass );
    }
}