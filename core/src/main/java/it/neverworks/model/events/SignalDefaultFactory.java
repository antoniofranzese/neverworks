package it.neverworks.model.events;

import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;

import java.util.Arrays;

import it.neverworks.lang.Reflection;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.DefaultFactory;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.ModelException;

public class SignalDefaultFactory implements DefaultFactory, PropertyAware {

    Class<? extends Event> instanceClass = null;
    Class<? extends Signal> mainClass = null;

    public void setPropertyDescriptor( PropertyDescriptor propertyDescriptor ) {
        MetaPropertyManager property = null;
        try {
            property = ((MetaPropertyManager) propertyDescriptor );
        } catch( ClassCastException ex ) {
            throw new ModelException( "Cannot define Signal default, " + property.toString() + " must be annotated with @Property" );
        }
        
        mainClass = propertyDescriptor.getStorageType();
        
        Type propertyType = ((MetaPropertyManager) property).getField().getGenericType();
        if( propertyType instanceof ParameterizedType ) {
            Type[] actualTypes = ((ParameterizedType) propertyType).getActualTypeArguments();
            
            if( actualTypes.length == 1 ) {
                this.instanceClass = (Class)actualTypes[ 0 ];
            } else {
                throw new ModelException( "Unknown Event generic specification: " + Arrays.asList( actualTypes ) );
            }
        
        } else {
            this.instanceClass = defaultEventClass();
        }

        //System.out.println( "Detected " + this.instanceClass + " for " + property.getFullName() );
        
    }
    
    protected Class<? extends Event> defaultEventClass() {
        return Event.class;
    }
    
    public Object getDefaultValue( ModelInstance instance ) {
        // return new Signal( this.instanceClass );
        return Reflection.withConstructor( this.mainClass, Class.class ).newInstance( this.instanceClass );
    }

}

