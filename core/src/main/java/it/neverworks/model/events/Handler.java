package it.neverworks.model.events;

public class Handler<T extends Event> {
    
    private Signal signal;
    private T event;
    /*package*/ boolean performed = false;
    
    public Handler( Signal signal, T event ) {
        this.signal = signal;
        this.event = event;
    }

    public Signal definition() {
        return signal;
    }

    public Signal signal() {
        return signal;
    }
    
    public Signal getSignal() {
        return signal;
    }

    public T event() {
        return event;
    }

    public T getEvent() {
        return event;
    }
    
    public Event handle() {
        performed = true;
        signal.notify( event );
        return event;
    }
    
    public void decline() {
        performed = true;
    }
    
    public void propagate( Signal next ) {
        next.chain( event );
    }
    
}