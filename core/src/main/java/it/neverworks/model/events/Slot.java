package it.neverworks.model.events;

import java.lang.reflect.Method;
import java.lang.ref.WeakReference;
import it.neverworks.aop.Proxy;
import it.neverworks.aop.MethodProxy;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.ExpressionException;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.Strings;

public class Slot implements Dispatcher, AfterDispatch {
    protected WeakReference ownerRef;
    protected String method;
    protected boolean parsed = false;
    protected Proxy actualHandler = null;
    protected boolean reply = false;
    protected boolean runOnce = false;
    protected boolean optional = false;
    protected boolean unsigned = false;
    
    protected Slot( Object owner ) {
        super();
        
        if( owner == null ) {
            throw new IllegalArgumentException( "Null owner for slot" );
        }
        
        this.ownerRef = new WeakReference( owner );
    }

    public Slot( Object owner, String method ) {
        this( owner );
        
        if( ! Strings.hasText( method ) ) {
            throw new IllegalArgumentException( "Null or empty method for slot" );
        }
        
        this.method = method;
    }
    
    public Slot( Object owner, Method method ) {
        this( owner );
        
        if( method == null ) {
            throw new IllegalArgumentException( "Null method for slot" );
        }
        
        this.actualHandler = new MethodProxy( method );
        this.parsed = true;
    }

    public Slot( Object owner, Proxy proxy ) {
        this( owner );
        
        if( proxy == null ) {
            throw new IllegalArgumentException( "Null proxy for slot" );
        }
        
        this.actualHandler = proxy;
        this.parsed = true;
    }
    
    public void dispatch( Event event ) {
        Object owner = owner();

        if( owner != null ) {

            if( ! parsed ) {
                String actualMethodName = null;
                Object actualOwner = null;
                int index = this.method.lastIndexOf( "." );
                if( index > 0 ) {
                    try {
                        actualOwner = ExpressionEvaluator.evaluate( owner, this.method.substring( 0, index ) );
                    } catch( ExpressionException ex ) {
                        throw new EventException( "Error dispatching " + event.getSignal() + ": cannot locate suitable " + this.method + "(" + event.getClass().getName() + ") slot in " + owner.getClass().getName() + ": " + ex.getMessage(), ex );
                    }
                    if( actualOwner == null ) {
                        throw new EventException( "Error dispatching " + event.getSignal() + ": cannot locate suitable " + this.method + "(" + event.getClass().getName() + ") slot in " + owner.getClass().getName() + ": Null owner" );
                    }
                    actualMethodName = this.method.substring( index + 1 );
                } else {
                    actualMethodName = this.method;
                    actualOwner = owner;
                }
            
            
                Method actualMethod = null; 
                if( unsigned ) {
                    actualMethod = Reflection.findMethod( actualOwner.getClass(), actualMethodName );
                } else {
                    actualMethod = Reflection.findMethodWithHierarchy( actualOwner.getClass(), actualMethodName, event.getClass() );
                }

                if( actualMethod != null ) {
                    actualHandler = new MethodProxy( actualMethod );
                    reply = ! Void.TYPE.equals( actualMethod.getReturnType() );
                } else if( ! optional ) {
                    throw new EventException( "Error dispatching " + event.getSignal() + ": No suitable " + actualMethodName + "(" + ( unsigned ? "" : event.getClass().getName() ) + ") slot in " + actualOwner.getClass().getName() );
                }
            
                parsed = true;
            }
        
            if( actualHandler != null ) {
                Object result = null;

                if( unsigned ) {
                    result = actualHandler.invoke( owner );
                } else {
                    result = actualHandler.invoke( owner, event );
                }

                if( reply ) {
                    event.reply( result );
                }
            }
            
        }
        
    }
    
    public boolean equals( Object other ) {
        Object owner = owner();

        if( owner != null ) {
            if( other instanceof Slot ) {
                Slot o = (Slot) other;
                if( owner.equals( o.ownerRef.get() ) ) {
                    if( this.actualHandler != null ) {
                        return this.actualHandler.equals( o.actualHandler );
                    } else if( this.method != null ) {
                        return this.method.equals( o.method );
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public Slot once() {
        this.runOnce = true;
        return this;
    }
    
    public Slot optional() {
        this.optional = true;
        return this;
    }

    public Slot required() {
        this.optional = false;
        return this;
    }

    public Slot unsigned() {
        this.unsigned = true;
        return this;
    }

    public void doAfterDispatch( Signal signal, Event event ) {
        if( runOnce || owner() == null ) {
            signal.remove( this );
        }
    }

    protected Object owner() {
        return this.ownerRef.get();
    }
    
    public String toString() {
        Object owner = owner();
        return getClass().getSimpleName() + "(" + ( owner != null ? owner.getClass().getSimpleName() : "<orphan>" ) + "." + method + ")";
    }
    
}   