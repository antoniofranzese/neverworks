package it.neverworks.model.events;

import static it.neverworks.language.*;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.ModelMetadata;
import it.neverworks.model.description.ModelMetadataCollection;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyInitializer;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.ModelInstanceInitializer;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Modifiers;

class WireProcessor implements MethodProcessor, PropertyProcessor, PropertyInitializer, PropertyMetadata, ModelInstanceInitializer, ModelMetadata {
    
    private String[] paths;
    private MethodDescriptor method;
    private boolean unsigned;
    
    public WireProcessor( AnnotationInfo<Wire> info ) {
        this.paths = info.annotation().value();
    }
    
    /* Method */

    public void processMethod( MethodDescriptor method ) {
        this.method = method;

        Class[] parameters = method.front().getParameterTypes();

        if( parameters.length == 0 ) {
            this.unsigned = true;
        } else if( parameters.length > 1 || ! Event.class.isAssignableFrom( parameters[ 0 ] ) ) {
            throw new EventException( method.front() + " is not a suitable Event handler" );
        }
        
        inheritModel( ModelDescriptor.of( this.method.front().getDeclaringClass() ) );
    }
    
    public void meetModel( ModelDescriptor model ) {
        model.addInitializer( this );
    }

    public void inheritModel( ModelDescriptor model ) {
        model.metadata( Metadata.class ).add( this );
    }
    
    public void initModelInstance( ModelInstance instance ) {
        if( this.method != null ) {
            for( String path: this.paths ) {

                Signal signal = lookupSignal( instance, path );

                if( signal != null ) {
                    Slot slot = new Slot( instance.actual(), this.method );
                    if( this.unsigned ) {
                        slot.unsigned();
                    }
                    signal.add( slot );
                }
            }
        }
    }

    /* Property */
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
 
    public void meetProperty( MetaPropertyManager property ) {
        if( property.is( Signal.class ) ) {
            property.addInitializer( this );
        } else {
            throw new EventException( "Property is not a Signal: " + property.getFullName() );
        }
    }
    
    public void inheritProperty( MetaPropertyManager property ) {
        if( ! property.metadata().contains( WireProcessor.class ) ) {
            property.metadata().add( this );
        }
    }
    
    public void initProperty( MetaPropertyManager property, ModelInstance instance ) {
        for( String path: this.paths ) {

            Signal signal = lookupSignal( instance, path );

            if( signal != null ) {
                signal.add( (Signal) property.getCheckedValue( instance ) );
            }
        }
        
    }
    
    protected Signal lookupSignal( ModelInstance instance, String path ) {
        Signal signal = null;

        if( instance.is( SignalProvider.class ) ) {
            signal = instance.as( SignalProvider.class ).retrieveSignal( path );
        } else {
            signal = ExpressionEvaluator.evaluate( instance.actual(), path, Modifiers.WITH_NULLS );
        }

        return signal;
    }
    
    public static class Metadata extends ModelMetadataCollection {}
}
