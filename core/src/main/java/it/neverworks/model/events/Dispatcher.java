package it.neverworks.model.events;

public interface Dispatcher<T extends Event> {
    void dispatch( T event );
}