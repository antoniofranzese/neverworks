package it.neverworks.model.events;

import static it.neverworks.language.*;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.model.description.MethodProcessor;
import it.neverworks.model.description.MethodDescriptor;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.ModelMetadata;
import it.neverworks.model.description.ModelMetadataCollection;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyInitializer;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.ModelInstanceInitializer;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Modifiers;

class SubscribeProcessor implements MethodProcessor, PropertyProcessor, PropertyInitializer, PropertyMetadata, ModelInstanceInitializer, ModelMetadata {
    
    private Class[] classes;
    private MethodDescriptor method;
    private boolean unsigned;
    
    public SubscribeProcessor( AnnotationInfo<Subscribe> info ) {
        this.classes = info.annotation().value();
    }
    
    /* Method */

    public void processMethod( MethodDescriptor method ) {
        this.method = method;

        Class[] parameters = method.front().getParameterTypes();
        
        if( this.classes.length == 0 && parameters.length == 1 ) {
            this.classes = new Class[]{ parameters[ 0 ] };
        }

        if( parameters.length == 0 ) {
            if( this.classes.length > 0 ) {
                this.unsigned = true;
            } else {
                throw new EventException( "No event type specified while " + method.front() + " is unsigned" );
            }
        } else if( parameters.length > 1 ) {
            throw new EventException( method.front() + " is not a suitable Event handler" );
        }
        
        inheritModel( ModelDescriptor.of( this.method.front().getDeclaringClass() ) );
    }
    
    public void meetModel( ModelDescriptor model ) {
        model.addInitializer( this );
    }

    public void inheritModel( ModelDescriptor model ) {
        model.metadata( Metadata.class ).add( this );
    }
    
    public void initModelInstance( ModelInstance instance ) {
        if( this.method != null && instance.is( EventPublisher.class ) ) {

            Slot slot = new Slot( instance.actual(), this.method );
            if( this.unsigned ) {
                slot.unsigned();
            }
            
            for( Class cls: this.classes ) {
                instance.as( EventPublisher.class ).retrievePublisher( cls ).add( slot );
            }
            
        }
    }

    /* Property */
    
    public void processProperty( MetaPropertyManager property ) {
        property.metadata().add( this );
    }
 
    public void meetProperty( MetaPropertyManager property ) {
        if( property.is( Signal.class ) ) {
            property.addInitializer( this );
        } else {
            throw new EventException( "Property is not a Signal: " + property.getFullName() );
        }
    }
    
    public void inheritProperty( MetaPropertyManager property ) {
        
    }
    
    public void initProperty( MetaPropertyManager property, ModelInstance instance ) {
        if( instance.is( EventPublisher.class ) ) {
            for( Class cls: this.classes ) {
                instance.as( EventPublisher.class ).retrievePublisher( cls ).add( (Signal) property.getCheckedValue( instance ) );
            }
        }
    }
    
    public static class Metadata extends ModelMetadataCollection {}
}
