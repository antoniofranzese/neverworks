package it.neverworks.model.events;

public interface SignalProvider {
    Signal retrieveSignal( String path );
}