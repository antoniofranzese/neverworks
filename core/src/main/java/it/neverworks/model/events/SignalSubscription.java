package it.neverworks.model.events;

import java.lang.ref.WeakReference;

public class SignalSubscription implements Subscription {
    
    private WeakReference<Signal> signalRef;
    private Dispatcher dispatcher;
    private boolean removed = false;

    public SignalSubscription( Signal signal, Dispatcher dispatcher ) {
        this.signalRef = new WeakReference<Signal>( signal );
        this.dispatcher = dispatcher;
    }
    
    public void remove() {
        Signal signal = this.signalRef.get();
        if( ! removed ) {
            this.removed = true;
            if( signal != null ) {
                signal.remove( this.dispatcher );
            }
        }
    }
    
    public Dispatcher getDispatcher(){
        return this.dispatcher;
    }
    
    public Signal getSignal(){
        return this.signalRef.get();
    }
    
    public boolean getRemoved() {
        return this.removed;
    }

    public boolean isRemoved() {
        return this.removed;
    }
}