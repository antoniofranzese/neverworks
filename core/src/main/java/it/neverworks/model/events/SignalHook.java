package it.neverworks.model.events;

import static it.neverworks.language.*;

import it.neverworks.model.expressions.ExpressionEvaluator;
import it.neverworks.model.expressions.Modifiers;

public class SignalHook<T extends Event> extends Signal<T> {
    
    public final static Modifiers OPTIONAL = new Modifiers().withNulls().withOptional().freeze();
     
    protected Subscription hook;
    
    protected Object root;
    protected String path;
    
    public SignalHook( Object root, String path ) {
        super( Event.class );
        this.root = root;
        this.path = path;
    }

    public void refresh(){
        if( this.size() > 0 ) {
            Object value = ExpressionEvaluator.evaluate( this.root, this.path, OPTIONAL );
            if( value != null ) {
                if( value instanceof Signal ) {
                    Signal signal = (Signal) value;
                
                    if( ! signal.contains( this ) ) {
                        this.detach();
                        this.hook = signal.add( this );
                    }
                
                } else {
                    throw new EventException( "Path on " + repr( this.root ) + " is not a Signal: " + this.path );
                }
            
            } else {
                this.detach();
            }
        } else {
            this.detach();
        }

    }
    
    public void detach() {
        if( this.hook != null ) {
            this.hook.remove();
            this.hook = null;
        }
    }

    /* package */ void chain( Event parent ) {
        //System.out.println( "Hook Chaining " + this + " with " + parent );
        fireWithEvent( parent );
    }

    public Event convertEvent( Object value ) {
        if( value instanceof Event ) {
            return (Event) value;
        } else {
            throw new EventException( "Invalid Event through SignalHook: " + repr( value ) );
        }
    }   

    public Subscription add( Dispatcher handler ) {
        Subscription subscription = super.add( handler );
        this.refresh();
        return subscription;
    }
    
    public Signal<T> remove( Dispatcher dispatcher ) {
        super.remove( dispatcher );
        this.refresh();
        return (Signal<T>) this;
    }

    protected void fireWithEvent( Event event ) {
        //System.out.println( "Firing SignalHook " + this.path + " with " + event );
        if( this.hook != null ) {
            super.fireWithEvent( event );
        }
    }
}