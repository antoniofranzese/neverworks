package it.neverworks.model.events;

public interface EventPublisher {
    Signal retrievePublisher( Class<?> eventClass );
}