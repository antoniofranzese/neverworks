package it.neverworks.model.events;

public class AnonymousSlot extends Slot {
 
    private Dispatcher dispatcher;
    
    public AnonymousSlot( Object owner, Dispatcher dispatcher ) {
        super( owner );
        
        if( dispatcher == null ) {
            throw new IllegalArgumentException( "Null or empty dispatcher for slot" );
        }
        
        this.dispatcher = dispatcher;
    }
    
    public void dispatch( Event event ) {
        Object owner = owner();

        if( owner != null ) {
            this.dispatcher.dispatch( event );
        }
    }
    
    public boolean equals( Object other ) {
        Object owner = owner();

        if( owner != null ) {
            if( other instanceof AnonymousSlot ) {
                AnonymousSlot o = (AnonymousSlot) other;
                if( owner.equals( o.ownerRef.get() ) ) {
                    if( this.dispatcher != null ) {
                        return this.dispatcher.equals( o.dispatcher );
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}