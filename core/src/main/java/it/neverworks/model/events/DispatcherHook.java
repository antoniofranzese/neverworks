package it.neverworks.model.events;

import static it.neverworks.language.*;
import it.neverworks.model.expressions.ExpressionEvaluator;

public class DispatcherHook implements Dispatcher {
    
    private Object instance;
    private String path;

    public DispatcherHook( Object instance, String path ) {
        this.instance = instance;
        this.path = path;
    }
    
    public void dispatch( Event event ) {
        //TODO: utilizzare anche SignalProvider per risolvere il path
        Object dispatcher = ExpressionEvaluator.evaluate( this.instance, this.path );
        if( dispatcher instanceof Dispatcher ) {
            ((Dispatcher) dispatcher).dispatch( event );
        } else if( dispatcher != null ){
            throw new EventException( msg( "Invalid dispatcher for ''{0}'' in {1}: {2}", this.path, repr( this.instance ), repr( dispatcher ) ) );
        }
    }
    
    public String getPath(){
        return this.path;
    }
    
    public Object getInstance(){
        return this.instance;
    }
    
}