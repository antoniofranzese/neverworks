package it.neverworks.model.events;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Arguments;
import it.neverworks.model.CoreModel;

/*
 * This class exists only to fool Java generics
 */
abstract class EventModel extends CoreModel {
    
    public <T extends Event> T set( String name, Object value ) {
        return (T) this.modelInstance.assign( name, value ).actual();
    }

    public <T extends Event> T set( Arguments arguments ) {
        return (T) this.modelInstance.assign( arguments ).actual();
    }

    public <T extends Event> T set( String arguments ) {
        return (T) this.modelInstance.assign( arguments ).actual();
    }

    public <T extends Event> T set( Tuple tuple, String... names ) {
        return (T) this.modelInstance.assign( tuple, names ).actual();
    }
    
}