package it.neverworks.model.events;

public interface Locatable {
    Locator getObjectLocator();
}