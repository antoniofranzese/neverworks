package it.neverworks.model.events;

public class Promise<T extends Event> {
    
    public enum Status {
        WAITING, RESOLVED, REJECTED, CANCELLED
    }
    
    private Signal<T> resolveSignal;
    private Signal<T> rejectSignal;
    private Status status;
    private T lastEvent;
    
    public Promise( Signal<T> resolveSignal, Signal<T> rejectSignal ) {
        this.resolveSignal = resolveSignal;
        this.rejectSignal = rejectSignal;
        this.status = Status.WAITING;
    }
    
    public Promise( Class<? extends T> resolveClass, Class<? extends T> rejectClass  ) {
        this( new Signal<T>( resolveClass ), new Signal<T>( rejectClass ) );
    }

    public Promise( Class<? extends T> eventClass ) {
        this( eventClass, eventClass );
    }

    public Promise() {
        this( new Signal<T>(), new Signal<T>() );
    }
    
    public Promise<T> then( Dispatcher<T> resolver ) {
        if( resolver != null ) {
            boolean immediate = false;
            synchronized( this ) {
                if( this.status == Status.WAITING ) {
                    this.resolveSignal.add( resolver );
                } else if( this.status == Status.RESOLVED ){
                    immediate = true;
                }
            }

            if( immediate ) {
                resolver.dispatch( this.lastEvent );
            }
        }
        return this;
    }

    public Promise<T> then( Dispatcher<T> resolver, Dispatcher<T> rejector ) {
        then( resolver );
        if( rejector != null ) {
            boolean immediate = false;
            synchronized( this ) {
                if( this.status == Status.WAITING ) {
                    this.rejectSignal.add( rejector );
                } else if( this.status == Status.REJECTED ) {
                    immediate = true;
                }
            }
            if( immediate ) {
                rejector.dispatch( this.lastEvent );
            }
            
        }
        return this;
    }

    public void resolve() {
        this.resolve( new Event() );
    }
    
    public void resolve( Object event ) {
        if( this.status == Status.WAITING ) {
            synchronized( this ) {
                this.status = Status.RESOLVED;
                this.lastEvent = (T) this.resolveSignal.convertEvent( event );
            }
            this.resolveSignal.fire( this.lastEvent );
            this.resolveSignal.clear();
            this.rejectSignal.clear();
        }
    }

    public void reject() {
        this.reject( new Event() );
    }
    
    public void reject( Object event ) {
        if( this.status == Status.WAITING ) {
            synchronized( this ) {
                this.status = Status.REJECTED;
                this.lastEvent = (T) this.rejectSignal.convertEvent( event );
            }
            this.rejectSignal.fire( this.lastEvent );
            this.resolveSignal.clear();
            this.rejectSignal.clear();
        }
    }
    
    public void cancel() {
        if( this.status == Status.WAITING ) {
            synchronized( this ) {
                this.status = Status.CANCELLED;
            }
            this.status = Status.CANCELLED;
            this.resolveSignal.clear();
            this.rejectSignal.clear();
        }
    }

}