package it.neverworks.model.events;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.description.MethodAnnotation;
import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.METHOD, ElementType.FIELD })
@Retention( RetentionPolicy.RUNTIME )
@MethodAnnotation( processor = WireProcessor.class )
@PropertyAnnotation( processor = WireProcessor.class )
public @interface Wire {
    String[] value();
}
