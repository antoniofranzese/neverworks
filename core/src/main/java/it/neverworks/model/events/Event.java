package it.neverworks.model.events;

import java.util.Stack;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Filter;
import it.neverworks.lang.Arguments;
import it.neverworks.model.description.Virtual;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.utils.ToStringBuilder;
import it.neverworks.model.utils.ExpandoModel;

public class Event<E> extends EventModel {
    
    protected E source;
    protected String reason;    
    protected Event parent;
    protected Signal signal;
    protected boolean stopped = false; 
    protected boolean bubbled = false;
    protected ExpandoModel attributes;
    protected Object response;
    protected Stack responses;
    protected boolean replied = false;
    
    public Event() {

    }

    public Event( E source ) {
        this.source = source;
    }
    
    public Event( E source, String reason ) {
        this.source = source;
        this.reason = reason;
    }
    
    public Event( Arguments arguments ) {
        arguments.populate( this );
    }

    public <T extends Event> T find( Class<T> eventClass  ) {
        Event current = this;
        do {
            if( eventClass.isInstance( current ) ) {
                return (T) current;
            } else {
                current = current.getParent();
            }
        } while( current != null );

        return null;
    }

    public <T extends Event> T find( Filter<Event> eventFilter  ) {
        Event current = this;
        do {
            if( eventFilter.filter( current ) ) {
                return (T) current;
            } else {
                current = current.getParent();
            }
        } while( current != null );

        return null;
    }
    
    public void reply( Object object ) {
        this.replied = true;
        if( this.response != null ) {
            getResponses().push( this.response );
        }
        this.response = object;
    }
    
    public Object getResponse() {
        return this.response;
    }
    
    public Stack getResponses() {
        if( this.responses == null ) {
            this.responses = new Stack();
        }
        return this.responses;
    }
    
    public boolean isReplied() {
        return this.replied;
    }
    
    public Event stop() {
        this.stopped = true;
        return this;
    }

    public Event go() {
        this.stopped = false;
        return this;
    }
    
    public Event propagate() {
        this.go();
        return this;
    }

    public Event bubble() {
        this.bubbled = true;
        return this;
    }

    public Event halt() {
        this.bubbled = false;
        return this;
    }

    public Event reset() {
        go();
        halt();
        return this;
    }
    
    @Virtual
    public ExpandoModel getAttributes(){
        if( this.attributes == null ) {
            this.attributes = new ExpandoModel();
        }
        return this.attributes;
    }
    
    @AutoConvert
    public void setAttributes( ExpandoModel attributes ) {
        this.attributes = attributes;
    }
    
    public E getSource(){
        return this.source;
    }
    
    public void setSource( E source ){
        this.source = source;
    }
    
    public String getReason(){
        return this.reason;
    }
    
    public void setReason( String reason ){
        this.reason = reason;
    }

    public Event getParent(){
        return this.parent;
    }
    
    public void setParent( Event parent ){
        this.parent = parent;
    }

    public boolean isContinued(){
        return ! this.stopped;
    }

    public boolean isStopped(){
        return this.stopped;
    }

    public boolean isHalted(){
        return ! this.bubbled;
    }

    public boolean isBubbled(){
        return this.bubbled;
    }

    public Signal getSignal(){
        return this.signal;
    }
    
    public void setSignal( Signal signal ){
        this.signal = signal;
    }

    public String toString() {
        return new ToStringBuilder( this ).add( "source" ).toString();
    }
    
}