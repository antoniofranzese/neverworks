package it.neverworks.model.events;

import java.util.Map;
import java.util.List;
import java.util.Stack;
import java.util.ArrayList;
import java.lang.reflect.Method;

import it.neverworks.aop.Proxy;
import it.neverworks.aop.MethodProxy;
import it.neverworks.model.converters.Convert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.converters.ConvertProcessor;
import it.neverworks.model.description.Default;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.Child;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.description.ModelDescriptor;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.expressions.ObjectQuery;
import it.neverworks.model.features.Retrieve;
import it.neverworks.model.features.Inspect;
import it.neverworks.model.features.Append;
import it.neverworks.model.features.Delete;
import it.neverworks.model.features.Length;
import it.neverworks.model.features.Clear;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.FluentList;
import it.neverworks.lang.Properties;
import it.neverworks.lang.Callable;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Threads;
import it.neverworks.lang.Numbers;

import it.neverworks.model.io.State;

@Child
@Convert( SignalConverter.class )
@Default( SignalDefaultFactory.class )
public class Signal<T extends Event> implements Dispatcher, ChildModel, Callable, Inspect, Append, Delete, Length, Clear, Retrieve {
 
    protected final static String ANONYMOUS = "[anonymous]";
    protected final static FluentList<String> PROPERTIES = FluentList.of( "name", "fullName" );
    
    protected Class<? extends Event> instanceClass;
    protected Converter instanceConverter;
    protected ModelInstance model;
    protected List<Dispatcher> dispatchers = new ArrayList<Dispatcher>();
    protected String name = ANONYMOUS;
    protected Proxy handler = null;
    
    protected final static ThreadLocal<Stack<Signal>> callStack = new ThreadLocal<Stack<Signal>>() {
        @Override
        protected Stack initialValue() {
            return new Stack<Signal>();
        }
    };
    
    static {
        Threads.registerCleaner( th -> { 
            callStack.remove(); 
        });
    }
    

    public Signal() {
        this( Event.class );
    }
    
    public Signal( Class<? extends Event> instanceClass ) {
        this.instanceClass = instanceClass;
        this.instanceConverter = ConvertProcessor.process( instanceClass );
    }
    
    public Signal( Class<? extends Event> instanceClass, Object owner, String name ) {
        this( instanceClass );
        this.model = ModelInstance.of( owner );
        this.name = name;
    }

    public void adoptModel( ModelInstance model, PropertyDescriptor property ){
        this.model = model;
        this.name = property.getName();
        String handlerName = "handle" + this.name.substring( 0, 1 ).toUpperCase() + this.name.substring( 1 );
        Method handlerMethod = Reflection.findMethod( model.getType(), handlerName, Handler.class );
        if( handlerMethod != null ) {
            this.handler = new MethodProxy( handlerMethod );
        }
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        this.model = null;
    }
    
    public Object getOwner() {
        if( model != null ) {
            return model.actual();
        } else {
            return null;
        }
    }

    public String getName(){
        return this.name;
    }
    
    public int size() {
        return dispatchers.size();
    }
    
    public void clear() {
        this.dispatchers.clear();
    }
    
    public boolean empty() {
        return dispatchers.size() == 0;
    }

    public boolean notEmpty() {
        return dispatchers.size() > 0;
    }
        
    public Subscription add( Dispatcher handler ) {
        if( handler != this ) {
            this.dispatchers.add( handler );
            this.touch();
            return new SignalSubscription( this, handler );
        } else {
            throw new IllegalArgumentException( "Cannot add signal to itself" );
        }
    }
    
    public boolean contains( Dispatcher dispatcher ) {
        return this.dispatchers.contains( dispatcher );
    }
    
    public Signal<T> remove( Dispatcher dispatcher ) {
        this.dispatchers.remove( dispatcher );
        this.touch();
        return this;
    }
    
    public ObjectQuery<Dispatcher> query() {
        return new ObjectQuery<Dispatcher>( dispatchers );
    }

    public <T> ObjectQuery<T> query( Class<T> cls ) {
        return new ObjectQuery<T>( dispatchers ).instanceOf( cls );
    }


    protected void touch() {
        if( model != null && !ANONYMOUS.equals( name ) ) {
            model.touch( name );
        }
    }

    public T fire() {
        return fire( createNewEvent() );
    }
    
    public void dispatch( Event event ) {
        this.chain( event );
    }

    /* package */ void chain( Event parent ) {
        //System.out.println( "Chaining " + this + " with " + parent );
        Event event = createNewEvent();
        if( event.getClass().isAssignableFrom( parent.getClass() ) ) {
            parent.model().copy().excluding( "source" ).to( event );
        }
        event.setParent( parent );
        fireWithEvent( event );
    }
    
    public T fire( Object input ) {
        //System.out.println( "Firing " + this );
        Event event = convertEvent( input );
        fireWithEvent( event );
        return (T) event;
    }        
    
    public Object call( Object... arguments ) {
        Event evt = null;
        if( arguments.length > 0 ) {
            evt = fire( arguments[ 0 ] );
        } else {
            evt = fire();
        }
        return evt.isReplied() ? evt.getResponse() : null;
    }
    
    public Object retrieveItem( Object key ) {
        if( Numbers.isNumber( key ) ) {
            int index = Numbers.Int( key );
            if( dispatchers.size() > index ) {
                return dispatchers.get( index );
            } else {
                throw new IllegalArgumentException( Strings.message( "Dispatcher index out of range({}): {}", dispatchers.size(), index ) );
            }
        } else if( key instanceof String && PROPERTIES.contains( (String) key ) ) {
            return Properties.get( this, (String) key );

        } else {
            throw new IllegalArgumentException( "Cannot retrieve item by key: " + key );
        }
    }

    public void appendItem( Object object ) {
        if( object instanceof Dispatcher ) {
            add( (Dispatcher) object );
        } else {
            throw new IllegalArgumentException( "Cannot add to signal: " + Objects.repr( object ) );
        }
    }
    
    public void deleteItem( Object object ) {
        if( object instanceof Dispatcher ) {
            remove( (Dispatcher) object );
        } else if( Numbers.isNumber( object ) ) {
            int index = Numbers.Int( object );
            if( index < dispatchers.size() ) {
                dispatchers.remove( index );
            } else {
                 throw new IllegalArgumentException( Strings.message( "Dispatcher index out of range({}): {}", dispatchers.size(), index ) );
            }
        } else {
            throw new IllegalArgumentException( "Cannot remove from signal: " + Objects.repr( object ) );
        }
    }

    public boolean containsItem( Object object ) {
        if( object instanceof Dispatcher ) {
            return contains( (Dispatcher) object );
        } else if( Numbers.isNumber( object ) ) {
            return dispatchers.size() > Numbers.Int( object );
        } else if( object instanceof String && PROPERTIES.contains( (String) object ) ) {
            return true;
        } else {
            throw new IllegalArgumentException( "Cannot inspect presence in signal: " + Objects.repr( object ) );
        }
    }
    
    public int itemsCount() {
        return size();
    }
    
    public void clearItems() {
        clear();
    }
    
    protected void fireWithEvent( Event event ) {

        if( this.model != null ) {
            // event event = createNewEvent();
            if( event.getSource() == null ) {
                event.setSource( this.model.actual() );
            }

            if( event.getSignal() == null ) {
                event.setSignal( this );
            }
    
            if( this.handler != null ) {
                Handler invocation = new Handler( this, event );
                this.handler.invoke( this.model.actual(), invocation );
                if( !invocation.performed ) {
                    throw new EventException( Strings.message( "Handler of {0.type.name}.{1} must call handle() or decline() on invocation argument", this.model, this.name ) );
                }
            } else {
                notify( event );
            }
    
        } else {
            notify( event );
        }
    }
    
    protected void notify( Event event ) {
        Stack calls = callStack.get();
        
        for( Dispatcher dispatcher: new ArrayList<Dispatcher>( dispatchers ) ) {
            //System.out.println( "Dispatcher: " + dispatcher );

            // Loop protection
            if( ! calls.contains( dispatcher ) ) {
                calls.push( dispatcher );

                try {
                    //System.out.println( "Dispatching " + this + " with " + event + " to " + dispatcher + " .> " + ( dispatcher instanceof AfterDispatch ));
                    dispatcher.dispatch( event );
                    if( dispatcher instanceof AfterDispatch ) {
                        ((AfterDispatch) dispatcher).doAfterDispatch( this, event );
                    }
                } finally {
                    calls.pop();
                }
            }
        }

    }

    protected Event createNewEvent() {
        return (Event) Reflection.newInstance( this.instanceClass );
    }
    
    public Event createEvent( Object source ) {
        return convertEvent( source );
    }

    public Event createEvent() {
        return convertEvent( null );
    }
    
    public Event convertEvent( Object source ) {
        
        Object value = this.instanceConverter != null ? this.instanceConverter.convert( source ) : source;
        
        if( source != null ) {
            if( instanceClass.isAssignableFrom( value.getClass() ) ) {
                return (Event)value;

            } else if( value instanceof State ) {
                Event event = createNewEvent();
                for( String property: ((State) value).names() ) {
                    // Properties.set( event, property, ((State) value).get( property ) );
                    if( event.model().has( property ) ) {
                        event.model().set( property, ((State) value).get( property ) );
                    }
                }
            
                return event;

            } else if( value instanceof Map ) {
                Event event = createNewEvent();
                Arguments arguments = Arguments.process( value );
                for( String name: arguments.keys() ) {
                    event.model().safeAssign( name, arguments.get( name ) );
                }

                return event;

            } else if( value instanceof Event ) {
                Event event = createNewEvent();
                event.setParent( (Event) value );

                return event;
        
            } else {
                throw new EventException( "Cannot convert " + source.getClass().getName() + " instance to " + instanceClass.getName() );
            }
            
        } else {
            return createNewEvent();
        }

    }
    
    public boolean equals( Object other ) {
        if( other instanceof Signal ) {
            Signal o = (Signal) other;
            if( this.model != null && ! this.model.equals( o.model ) ) {
                return false;
            } else if( this.name != null && ! this.name.equals( o.name ) ) {
                return false;
            } else {
                return true;
            }
            
        } else {
            return false;
        }
    }
    
    public Class<? extends Event> getEventClass(){
        return this.instanceClass;
    }
    
    public String getFullName() {
        return ( model != null ? model.actual().getClass().getSimpleName() : "[orphan]" ) + "." + name + "<" + ( instanceClass.getSimpleName() ) + ">";
    }
    
    public String toString() {
        return this.getClass().getSimpleName() + "(" + getFullName() + ")";
    }
    
}