package it.neverworks.model.collections;

public class MapAdder<K,V> extends BaseInvocation implements Adder<V> {
    
    private Map<K,V> map;
    private K key;
    private V item;
    
    public MapAdder( InstanceMetadata metadata, Map<K,V> map, K key, V item ) {
        super( metadata );
        this.map = map;
        this.item = item;
        this.key = key;
    }
    
    public void add() {
        if( !performed ) {
            add( this.key, this.item );
        } else {
            throw new CollectionException( "Adder already performed" );
        }
    }

    public void add( K key, V item ) {
        performed = true;
        this.map.addRawItem( key, item );
    }
    
    public K key() {
        return key;
    }

    public V item() {
        return item;
    }

    public boolean contains( V item ) {
        return this.map.containsValue( item );
    }

    public boolean containsKey( K key ) {
        return this.map.containsKey( item );
    }

}