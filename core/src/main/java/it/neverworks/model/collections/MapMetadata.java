package it.neverworks.model.collections;

import it.neverworks.lang.Annotations;
import it.neverworks.lang.Reflection;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.types.WrongTypeException;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.types.CustomType;

public class MapMetadata extends InstanceMetadata {

    protected TypeDefinition keyType;
    
    public MapMetadata( Map collection ) {
        super( collection );
        it.neverworks.model.collections.Collection annotation = Annotations.get( collection.getClass(), it.neverworks.model.collections.Collection.class );
        if( annotation != null && annotation.key() != Object.class ) {
            if( Converter.class.isAssignableFrom( annotation.key() ) ) {
                this.keyType = TypeFactory.build( new CustomType(), (Converter)Reflection.newInstance( annotation.key() ) );
            } else {
                this.keyType = TypeFactory.build( annotation.key() );
            }
        }
    }
    
    public TypeDefinition getKeyType(){
        return this.keyType;
    }
    
    public void setKeyType( TypeDefinition keyType ){
        this.keyType = keyType;
    }

    protected void park() {
        park( new java.util.ArrayList( ((Map)collection).entrySet() ) );
    }
    
    public void clearCollection() {
        ((Map) this.collection).clear();
    }

    public void recover( Iterable parked ) {
        ((Map) collection).clear();
        for( Map.Entry entry: (Iterable<Map.Entry>)parked ){
            ((Map) collection).put( entry.getKey(), entry.getValue() );
        }
    }

    public Object add( Object key, Object item ) {
        Object processedKey = processKey( key );
        Object existing = ((Map) collection).containsKey( processedKey ) ? ((Map) collection).get( processedKey ) : null;
        
        if( this.adder != null && this.model != null ) {
            MapAdder adderInvocation = new MapAdder( this, ((Map) collection), processedKey, processItem( item ) );

            this.adder.invoke( this.model.actual(), adderInvocation );

            if( !adderInvocation.performed() ) {
                adderInvocation.add();
            }

        } else {
            ((Map) collection).addRawItem( processedKey, processItem( item ) );   
        }; 
        
        return existing;
    }

    public Object remove( Object key ) {
        Object processedKey = processKey( key );
        
        if( this.remover != null && this.model != null ) {
            MapRemover removerInvocation = new MapRemover( this, ((Map) collection), processedKey, ((Map) collection).get( processedKey ) );
            
            this.remover.invoke( this.model.actual(), removerInvocation );
            
            if( !removerInvocation.performed() ) {
                removerInvocation.remove();
            }
            
            return removerInvocation.item();

        } else {
            Object existing = ((Map) collection).containsKey( processedKey ) ? ((Map) collection).get( processedKey ) : null;
            ((Map) collection).removeRawKey( processedKey );
            return existing;
        }
    }


    public Object processKey( Object key ) {
        if( keyType != null ) {
            try {
                return keyType.process( key );
            } catch( IllegalArgumentException ex ) {
                String objectRepr = String.valueOf( key );
                if( objectRepr.length() > 50 ) {
                    objectRepr = objectRepr.substring( 51 ) + "...";
                }
                
                throw new WrongTypeException( 
                    "Wrong key type for " 
                    + collection.getClass().getSimpleName() + "<" + keyType.getName() + "> item: " 
                    + objectRepr + " <" + ( key != null ? key.getClass().getName() : "null" ) + ">"
                    , ex  
                    , keyType );
            }
        } else {
            return key;
        }
        
    }
    
}