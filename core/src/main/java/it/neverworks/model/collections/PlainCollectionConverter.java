package it.neverworks.model.collections;

import it.neverworks.model.converters.Converter;

// Stub class
public class PlainCollectionConverter implements Converter {
    
    public Object convert( Object value ) {
    
        return value;        
    }
}