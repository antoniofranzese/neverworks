package it.neverworks.model.collections;

import it.neverworks.model.converters.Converter;

public class ModelMapConverter implements Converter {
    
    private CollectionMetadata processor;
    
    public ModelMapConverter( CollectionMetadata processor ) {
        this.processor = processor;
    }
    
    public Object convert( Object value ) {
        if( this.processor.getCollectionType().isInstance( value ) 
            && this.processor.getItemType().equals( ((ModelCollection) value).metadata().getItemType() ) 
            && this.processor.getKeyType().equals( ((MapMetadata)((ModelCollection) value).metadata()).getKeyType() ) 
        ) {
            ((ModelCollection) value).metadata().setAdder( this.processor.getAdder() );
            ((ModelCollection) value).metadata().setRemover( this.processor.getRemover() );
            
            if( !((Map) value).isEmpty() && this.processor.getAdder() != null ) {
                ((ModelCollection) value).metadata().park( ((Map) value).entrySet() );
            } 
            return value;
            
        } else if( value instanceof java.util.Map ) {
            ModelCollection coll = (ModelCollection)this.processor.getDefaultValue( null );
            coll.metadata().park( ((java.util.Map) value).entrySet() );
            return coll;
        
        } else {
            return value;

        }
    }
}