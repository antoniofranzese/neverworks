package it.neverworks.model.collections;

import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.lang.reflect.ParameterizedType;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.AnnotationInfo;
import it.neverworks.aop.Proxy;
import it.neverworks.aop.MethodProxy;
import it.neverworks.lang.Reflection;
import it.neverworks.model.converters.AutoConvert;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyProcessor;
import it.neverworks.model.description.PropertyMetadata;
import it.neverworks.model.description.PropertyManager;
import it.neverworks.model.description.PropertyInitializer;
import it.neverworks.model.description.MetaPropertyManager;
import it.neverworks.model.description.PropertyAware;
import it.neverworks.model.description.DefaultFactory;
import it.neverworks.model.description.ChildMetadata;
import it.neverworks.model.types.TypeAware;
import it.neverworks.model.types.CustomType;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.types.TypeDefinition;

public class CollectionMetadata implements PropertyProcessor, PropertyMetadata, DefaultFactory, PropertyInitializer {

    protected Class<? extends java.util.Collection> collectionClass;
    protected Class<?> itemClass;
    protected Class<?> keyClass;
    protected String adderName;
    protected String removerName;
    
    protected TypeDefinition collectionType = null;
    protected TypeDefinition itemType = null;
    protected TypeDefinition keyType = null;
    protected Proxy adder = null;
    protected Proxy remover = null;
    protected boolean child;
    protected boolean lazy;
    protected boolean wrap;
    protected String split;
    
    protected boolean isList;
    protected boolean isMap;
    
    public CollectionMetadata( AnnotationInfo<Collection> info ) {
        this.collectionClass = info.getAnnotation().type();
        this.adderName = info.getAnnotation().adder();
        this.removerName = info.getAnnotation().remover();
        this.itemClass = info.getAnnotation().item();
        this.keyClass = info.getAnnotation().key();
        this.child = info.getAnnotation().child();
        this.lazy = info.getAnnotation().lazy();
        this.wrap = info.getAnnotation().wrap();
        this.split = Strings.plane( info.getAnnotation().split() );
    }
    
    public void processProperty( MetaPropertyManager property ) {
        // System.out.println( "-------------" + property.getName() );
        Class<?> collectionClass = this.collectionClass;
        
        // Infer collection type using field
        if( collectionClass == java.util.Collection.class ) {
            collectionClass = property.getField().getType();
        } 
        
        // Remap collection type if not compliant
        if( !ModelCollection.class.isAssignableFrom( collectionClass ) ) {
            Class<?> remappedClass = CollectionMapper.remap( collectionClass );
            if( remappedClass != null ) {
                collectionClass = remappedClass;
                // System.out.println(  "Remapped to " + remappedClass );
            } else {
                throw new CollectionException( "Cannot remap collection class " + collectionClass.getName() + " of " + property.getFullName() + " property" );
            }
        }
        
        this.collectionType = TypeFactory.build( collectionClass );
        this.isList = java.util.List.class.isAssignableFrom( collectionClass );
        this.isMap = java.util.Map.class.isAssignableFrom( collectionClass );

        // Parameterized type inference
        Type propertyType = ((MetaPropertyManager) property).getField().getGenericType();
        // System.out.println( "Property type " + propertyType );

        Type actualItemType = null;
        Type actualKeyType = null;
        
        if( propertyType instanceof ParameterizedType ) {
            Type[] actualTypes = ((ParameterizedType) propertyType).getActualTypeArguments();
            
            if( actualTypes.length == 1 ) {
                actualItemType = actualTypes[ 0 ];

            } else if( actualTypes.length == 2 && this.isMap ) {
                actualKeyType = actualTypes[ 0 ];
                actualItemType = actualTypes[ 1 ];

            } else {
                throw new CollectionException( "Collection parameterized type mismatch for " + property.getFullName() + " property" );
            }
        } 

        // Item Type
        if( this.itemClass != void.class ) {
            this.itemType = buildType( this.itemClass, property, actualItemType );
        } else if( actualItemType != null ) {
            this.itemType = TypeFactory.build( actualItemType );
        } else {
            this.itemType = TypeFactory.build( Object.class );
        }
        // System.out.println( "Item type " + this.itemType );

        // Key type
        if( this.isMap ) {
            if( this.keyClass != void.class ) {
                this.keyType = buildType( this.keyClass, property, actualKeyType );
            } else if( actualKeyType != null ) {
                this.keyType = TypeFactory.build( actualKeyType );
            } else {
                this.keyType = TypeFactory.build( Object.class );
            }
        }


        // if( this.itemType == null ) this.itemType = new CustomType( actualItemType );
        // if( this.isMap && this.keyType == null ) this.keyType = new CustomType( actualKeyType );

        // Adder lookup
        if( Strings.isEmpty( this.adderName ) ) {
            this.adderName = "add" + property.getName().substring( 0, 1 ).toUpperCase() + property.getName().substring( 1 ) + "Item";
        }

        Class adderClass = isList ? ListAdder.class : ( isMap ? MapAdder.class : Adder.class ); 
        Method adderMethod = Reflection.findMethodWithHierarchy( property.getModel().getTarget(), this.adderName, adderClass );

        //TODO: findMethodWithHierarchy deve cercare anche le interfacce
        if( adderMethod == null ) {
            adderMethod = Reflection.findMethod( property.getModel().getTarget(), this.adderName, Adder.class );
        }
        if( adderMethod != null ) {
            this.adder = new MethodProxy( adderMethod );
        }
        // System.out.println( "Adder " + adderMethod );

        // Remover lookup
        if( Strings.isEmpty( this.removerName ) ) {
            this.removerName = "remove" + property.getName().substring( 0, 1 ).toUpperCase() + property.getName().substring( 1 ) + "Item";
        }
        
        Class removerClass = isList ? ListRemover.class : ( isMap ? MapRemover.class : Remover.class ); 
        Method removerMethod = Reflection.findMethodWithHierarchy( property.getModel().getTarget(), this.removerName, removerClass );

        //TODO: findMethodWithHierarchy deve cercare anche le interfacce
        if( removerMethod == null ) {
            removerMethod = Reflection.findMethod( property.getModel().getTarget(), this.removerName, Remover.class );
        }
        
        if( removerMethod != null ) {
            this.remover = new MethodProxy( removerMethod );
            
        }
        // System.out.println( "Remover " + removerMethod );
        
        
        // Property setup
        Converter collectionConverter = this.isMap ? new ModelMapConverter( this ) : new ModelCollectionConverter( this );
        property.setType( new CollectionType( property.getField().getType(), collectionConverter, this ) );
        property.setDefault( this );
        property.setChild( true );
        property.setConverter( collectionConverter );
        property.metadata().add( this );
        
        if( ! lazy ) {
            property.addInitializer( this );
        }
    }
    
    protected TypeDefinition buildType( Class source, MetaPropertyManager property, Type actualType ) {
        if( AutoConvert.class.equals( source ) ) {
            if( actualType != null ) {
                return TypeFactory.auto( actualType );
            } else {
                throw new CollectionException( "Cannot auto-convert missing type for " + property.getFullName() + " property" );
            }

        } else if( Converter.class.isAssignableFrom( source ) ) {
            Converter converter = (Converter)Reflection.newInstance( source );
            if( converter instanceof PropertyAware ) {
                ((PropertyAware) converter).setPropertyDescriptor( property );
            }
            if( actualType != null && converter instanceof TypeAware ) {
                TypeAware.set( (TypeAware) converter, actualType );
            }
            return TypeFactory.build( new CustomType(), converter );
        } else if( TypeDefinition.class.isAssignableFrom( source ) ) {
            return (TypeDefinition) Reflection.newInstance( source );
        } else {
            return TypeFactory.build( source );
        }
        
    }
    
    public void initProperty( MetaPropertyManager property, ModelInstance instance ) {
        if( instance.undefined( property.getName() ) ) {
            instance.raw( property.getName(), this.newInstance() ) ;
        }
    }

    public Object getDefaultValue( ModelInstance instance ) {
        return this.newInstance();
    }

    public Object newInstance() {
        ModelCollection collection = (ModelCollection)this.collectionType.newInstance();
        collection.metadata().setAdder( this.adder );
        collection.metadata().setRemover( this.remover );
        collection.metadata().setItemType( this.itemType );
        collection.metadata().setChild( this.child );
        
        if( this.isMap ) {
            ((MapMetadata)((ModelCollection) collection).metadata()).setKeyType( this.keyType );
        }
        
        return collection;
    }
    
    public Object processItem( Object value ) {
        if( this.itemType != null ) {
            return this.itemType.process( value );
        } else {
            return value;
        }
    }

    public Object processKey( Object value ) {
        if( this.keyType != null ) {
            return this.keyType.process( value );
        } else {
            return value;
        }
    }

    public TypeDefinition getCollectionType() {
        return this.collectionType;
    }
    
    public TypeDefinition getItemType(){
        return this.itemType;
    }
    
    public TypeDefinition getKeyType(){
        return this.keyType;
    }

    public Proxy getAdder(){
        return this.adder;
    }
    
    public Proxy getRemover(){
        return this.remover;
    }
    
    public void meetProperty( MetaPropertyManager property ){
        if( property.metadata().contains( ChildMetadata.class ) ) {
            this.child = true;
        }
        if( property.metadata().contains( AutoConvert.class ) ) {
            System.out.println( property.getFullName() + " is autoconvertable" );
        }
    }
    
    public void inheritProperty( MetaPropertyManager property ){
        if( ! property.metadata().contains( CollectionMetadata.class ) ) {
            processProperty( property );
        }
    }
    
    public boolean getWrap(){
        return this.wrap;
    }
    
    public String getSplit(){
        return this.split;
    }
    
}