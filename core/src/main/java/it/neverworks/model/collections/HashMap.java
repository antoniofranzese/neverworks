package it.neverworks.model.collections;

import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;

public class HashMap<K,V> extends java.util.HashMap<K,V> implements ModelCollection, Map<K,V> {

    private MapMetadata metadata;
    
    public HashMap() {
        super();
        this.metadata = new MapMetadata( this );
    }
    
    public InstanceMetadata metadata() {
        return this.metadata;
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        metadata.adoptModel( model, property );
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        metadata.orphanModel( model, property );
    }

    @Override
    public boolean containsKey( Object key ) {
        return super.containsKey( metadata.processKey( key ) );
    }

    @Override
    public boolean containsValue( Object value ) {
        return super.containsValue( metadata.processItem( value ) );
    }
    
    @Override
    public V put( K key, V item ) {
        return (V)metadata.add( key, item );
    }
    
    @Override
    public Map<K,V> set( K key, V item ) {
        metadata.add( key, item );
        return this;
    }
    
    @Override
    public void putAll( java.util.Map<? extends K,? extends V> m ) {
        for( K key: m.keySet() ) {
            put( key, m.get( key ) );
        }
    }
     
    @Override
    public V remove( Object key ) {
        return (V)metadata.remove( key );
    } 
    
    @Override
    public void clear() {
        if( metadata.hasRemover() ) {
            for( Object key: keySet() ) {
                remove( key );
            }
        } else {
            super.clear();
        }
    }

    @Override
    public void addRawItem( K key, V item) {
        super.put( key, (V)metadata.adoptItem( item ) );
    }
    
    @Override
    public void removeRawKey( K key ){
        metadata.orphanItem( super.remove( key ) );
    }
    
    
}