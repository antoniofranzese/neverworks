package it.neverworks.model.collections;

public class ListAdder<T> extends BaseInvocation implements Adder<T> {
    
    private List<T> list;
    private T item;
    private int index;
    
    public ListAdder( InstanceMetadata metadata, List<T> list, T item, int index ) {
        super( metadata );
        this.list = list;
        this.item = item;
        this.index = index;
    }
    
    public void add() {
        add( this.item, this.index );
    }

    public void add( T item ) {
        add( item, this.index );
    }

    public void add( T item, Integer index ) {
        performed = true;
        this.list.addRawItem( item, index );
    }
    
    public T item() {
        return item;
    }
    
    public int index() {
        return index;
    }
    
    public boolean contains( T item ) {
        return this.list.contains( item );
    }

}