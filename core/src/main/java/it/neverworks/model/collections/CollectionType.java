package it.neverworks.model.collections;

import it.neverworks.model.types.BaseType;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.description.DefaultFactory;

public class CollectionType<T> extends BaseType<T> {
    
    protected CollectionMetadata metadata;
    
    public CollectionType( Class type, Converter converter, CollectionMetadata metadata ) {
        super();
        this.type = type;
        this.converter = converter;
        this.metadata = metadata;
    }
    
    public T newInstance() {
        return (T) metadata.newInstance();
    }
    
}