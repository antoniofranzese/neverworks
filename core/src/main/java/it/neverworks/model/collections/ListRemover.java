package it.neverworks.model.collections;

public class ListRemover<T> extends BaseInvocation implements Remover<T> {
    
    private List<T> list;
    private T item;
    private int index;
    public ListRemover( InstanceMetadata metadata, List<T> list, T item, int index ) {
        super( metadata );
        this.list = list;
        this.item = item;
        this.index = index;
    }
    
    public void remove() {
        remove( this.index );
    }

    public void remove( int index ) {
        performed = true;
        this.list.removeRawItem( index );
    }
    
    public T item() {
        return item;
    }
    
    public int index() {
        return index;
    }

    public boolean contains( T item ) {
        return this.list.contains( item );
    }

}