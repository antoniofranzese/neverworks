package it.neverworks.model.collections;

public class MapRemover<K,V> extends BaseInvocation implements Remover<V> {
    
    private Map<K,V> map;
    private K key;
    private V item;
    
    public MapRemover( InstanceMetadata metadata, Map<K,V> map, K key, V item ) {
        super( metadata );
        this.map = map;
        this.item = item;
        this.key = key;
    }
    
    public void remove() {
        if( !performed ) {
            remove( this.key );
        } else {
            throw new CollectionException( "Remover already performed" );
        }
    }

    public void remove( K key ) {
        performed = true;
        this.map.removeRawKey( key );
    }
    
    public K key() {
        return key;
    }

    public V item() {
        return item;
    }
    
    public boolean contains( V item ) {
        return this.map.containsValue( item );
    }

    public boolean containsKey( K key ) {
        return this.map.containsKey( item );
    }

}