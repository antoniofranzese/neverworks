package it.neverworks.model.collections;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import it.neverworks.model.converters.Converter;
import it.neverworks.model.description.PropertyAnnotation;

@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention( RetentionPolicy.RUNTIME )
@PropertyAnnotation( processor = CollectionMetadata.class )
public @interface Collection {
    Class<? extends java.util.Collection> type() default java.util.Collection.class;
    String adder() default "";
    String remover() default "";
    Class<?> item() default void.class;
    Class<?> key() default void.class;
    boolean child() default false;
    boolean lazy() default true;
    boolean wrap() default false;
    String split() default "";
}
