package it.neverworks.model.collections;

import it.neverworks.model.ModelException;

public class CollectionException extends ModelException {
    
    public CollectionException( String message, Throwable throwable ){
        super( message, throwable );
    }
    
    public CollectionException( String message ){
        super( message );
    }
}