package it.neverworks.model.collections;

public class ListMetadata extends InstanceMetadata {
    
    public ListMetadata( List collection ) {
        super( collection );
    }
    
    // public void parkItem( Object item ) {
    //     if( this.parked != null ) {
    //         if( this.parked instanceof List ) {
    //             ((List) this.parked).add( item );
    //         } else {
    //             throw new IllegalStateException( "Cannot park item, park is not List" );
    //         }
    //     } else {
    //         throw new IllegalStateException( "Cannot park item, parking is not active" );
    //     }
    // }

    public void recover( Iterable parked ) {
        ((List) collection).clear();
        for( Object item: parked ){
            ((List) collection).add( item );
        }
    }
    
    public void clearCollection() {
        ((List) this.collection).clear();
    }
    
    public boolean add( Object item, int index ) {
        if( this.adder != null && this.model != null ) {
            ListAdder adderInvocation = new ListAdder( this, ((List) collection), processItem( item ), index );

            this.adder.invoke( this.model.actual(), adderInvocation );

            if( !adderInvocation.performed() ) {
                adderInvocation.add();
            }
        } else {
            ((List) collection).addRawItem( processItem( item ), index );   
        }; 
        return true;
    }

    public Object remove( int index ) {
        if( this.remover != null && this.model != null ) {
            ListRemover removerInvocation = new ListRemover( this, ((List) collection), ((List) collection).get( index ), index );
            
            this.remover.invoke( this.model.actual(), removerInvocation );
            
            if( !removerInvocation.performed() ) {
                removerInvocation.remove();
            }
            
            return removerInvocation.item();
        } else {
            Object item = ((List) collection).get( index );
            ((List) collection).removeRawItem( index );
            return item;
        }
    }
    
    
    
}