package it.neverworks.model.collections;

import java.util.Collection;
import it.neverworks.model.description.PropertyDescriptor;

public interface Adder<T> {
    
    void add();
    void decline();
    T item();
    PropertyDescriptor property();
    String name();
    boolean contains( T item );

}