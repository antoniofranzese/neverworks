package it.neverworks.model.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Arrays;

import it.neverworks.lang.Collections;
import it.neverworks.model.converters.Converter;

public class ModelCollectionConverter implements Converter {
    
    private CollectionMetadata metadata;
    
    public ModelCollectionConverter( CollectionMetadata metadata ) {
        this.metadata = metadata;
    }
    
    public Object convert( Object value ) {
        if( this.metadata.getCollectionType().isInstance( value ) 
         && this.metadata.getItemType().equals( ((ModelCollection) value).metadata().getItemType() ) ) {

            ((ModelCollection) value).metadata().setAdder( this.metadata.getAdder() );
            ((ModelCollection) value).metadata().setRemover( this.metadata.getRemover() );
            
            if( !((Collection) value).isEmpty() && this.metadata.getAdder() != null ) {
                ArrayList v = new ArrayList( ((Collection) value).size() );
                v.addAll( ((Collection) value) );
                ((ModelCollection) value).metadata().park( v );
            } 
            return value;
            
        } else if( value instanceof String && this.metadata.getSplit() != null ) {
            ModelCollection coll = (ModelCollection)this.metadata.getDefaultValue( null );
            coll.metadata().park( Collections.list( ((String) value).split( this.metadata.getSplit() ) ) );
            return coll;
            
        } else if( this.metadata.getWrap() && this.metadata.getItemType().isInstance( value ) ){
            ModelCollection coll = (ModelCollection)this.metadata.getDefaultValue( null );
            ArrayList v = new ArrayList( 1 );
            v.add( value );
            coll.metadata().park( v );
            return coll;

        } else if( value instanceof Iterable ) {
            ModelCollection coll = (ModelCollection)this.metadata.getDefaultValue( null );
            coll.metadata().park( (Iterable)value );
            return coll;
        
        } else if( value != null && value.getClass().isArray() ) {
            ModelCollection coll = (ModelCollection)this.metadata.getDefaultValue( null );
            coll.metadata().park( Arrays.asList( (Object[]) value ) );
            return coll;
        
        } else if( this.metadata.getWrap() ){
            ModelCollection coll = (ModelCollection)this.metadata.getDefaultValue( null );
            ArrayList v = new ArrayList( 1 );
            v.add( value );
            coll.metadata().park( v );
            return coll;
            
        } else {
            return value;
        }
    }
    
    
}