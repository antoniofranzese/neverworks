package it.neverworks.model.collections;

import it.neverworks.model.description.ChildModel;

public interface ModelCollection extends ChildModel {
    InstanceMetadata metadata();
}