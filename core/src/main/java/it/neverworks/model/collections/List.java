package it.neverworks.model.collections;

import it.neverworks.model.expressions.ObjectQuery;

public interface List<T> extends java.util.List<T> {
    
    void addRawItem( T item, Integer index );
    void removeRawItem( Integer index );
    List<T> add( T... items );
    String join( String delimiter );
    ObjectQuery<T> query();
}