package it.neverworks.model.collections;

import it.neverworks.model.description.PropertyDescriptor;

public class BaseInvocation<T>  {
    
    protected boolean performed = false;
    protected InstanceMetadata metadata;
    
    public BaseInvocation( InstanceMetadata metadata ) {
        this.metadata = metadata;
    }
    
    public void decline() {
        performed = true;
    }
    
    public boolean performed() {
        return this.performed;
    }
    
    public PropertyDescriptor property() {
        return this.metadata.property();
    }
    
    public String name() {
        return this.metadata.property().getName();
    }
    
}