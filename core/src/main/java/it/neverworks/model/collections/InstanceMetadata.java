package it.neverworks.model.collections;

import java.util.Collection;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Annotations;
import it.neverworks.lang.Reflection;
import it.neverworks.aop.Proxy;
import it.neverworks.model.types.WrongTypeException;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.model.types.CustomType;
import it.neverworks.model.converters.Converter;
import it.neverworks.model.description.ChildModel;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.lang.Reflection;

public abstract class InstanceMetadata<T> {
    protected Object collection;
    protected ModelInstance model = null;
    protected PropertyDescriptor property = null;
    protected Proxy adder = null;
    protected Proxy remover = null;
    protected TypeDefinition itemType = null;
    protected java.util.Collection parked = null;
    protected boolean child = false;
    protected boolean clearOnOrphan = true; 
    
    public InstanceMetadata( Object collection ) {
        this.collection = collection; 

        it.neverworks.model.collections.Collection annotation = Annotations.get( collection.getClass(), it.neverworks.model.collections.Collection.class );
        if( annotation != null ) {
            this.child = annotation.child();
            
            if( annotation.item() != Object.class ) {
                if( Converter.class.isAssignableFrom( annotation.item() ) ) {
                    this.itemType = TypeFactory.build( new CustomType(), (Converter)Reflection.newInstance( annotation.item() ) );
                } else {
                    this.itemType = TypeFactory.build( annotation.item() );
                }
            }

            if( this.child ) {
                park();
            }
        }
    }
    
    public void setAdder( Proxy adder ) {
        this.adder = adder;
    }
    
    public void setRemover( Proxy remover ) {
        this.remover = remover;
    }
    
    public TypeDefinition getItemType(){
        return this.itemType;
    }
    
    public void setItemType( TypeDefinition itemType ){
        this.itemType = itemType;
    }
    
    public boolean getChild(){
        return this.child;
    }
    
    public void setChild( boolean child ){
        this.child = child;
    }
    
    public boolean getClearOnOrphan(){
        return this.clearOnOrphan;
    }
    
    public void setClearOnOrphan( boolean clearOnOrphan ){
        this.clearOnOrphan = clearOnOrphan;
    }
    
    public void park( java.util.Collection source ) {
        this.parked = source;
    }

    public void park( Iterable source ) {
        java.util.ArrayList list = new java.util.ArrayList();
        for( Object item: source ) {
            list.add( item );
        }
        park( list );
    }
    
    protected void park() {
        park( new java.util.ArrayList( (Collection) this.collection ) );
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        this.model = model;
        this.property = property;
        
        // Parked items recovery
        if( this.parked != null ) {
            recover( this.parked );
        }
        this.parked = null;
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        if( this.remover != null && this.clearOnOrphan ) {
            this.clearCollection();
        }
        this.model = null;
        this.property = null;
    }
    
    public abstract void recover( Iterable parked );
    
    public abstract void clearCollection();
    
    public Object processItem( Object item ) {
        if( itemType != null ) {
            try {
                return itemType.process( item );
            } catch( IllegalArgumentException ex ) {
                String objectRepr = String.valueOf( item );
                if( objectRepr.length() > 50 ) {
                    objectRepr = objectRepr.substring( 51 ) + "...";
                }
                
                throw new WrongTypeException( 
                    "Wrong item type for " 
                    + collection.getClass().getSimpleName() + "<" + itemType.getName() + "> item: " 
                    + Strings.limit( item, 50 ) + " <" + ( Objects.className( item ) ) + ">"
                    , ex
                    , itemType  
                );
            }
        } else {
            return item;
        }
        
    }
    
    // public abstract void parkItem( Object item );
    
    public Object adoptItem( Object item ) {
        if( child && item instanceof ChildModel ) {
            if( this.model != null ) {
                ((ChildModel) item).adoptModel( this.model, this.property );
            } else {
                this.parked.add( item );
            }
        }
        return item;
    }

    public Object orphanItem( Object item ) {
        if( child && item instanceof ChildModel ) {
            ((ChildModel) item).orphanModel( this.model, this.property );
        }
        return item;
    }
    
    public boolean hasAdder() {
        return this.adder != null;
    }
    
    public boolean hasRemover() {
        return this.remover != null;
    }
    
    public PropertyDescriptor property() {
        return this.property;
    }
    
    public ModelInstance model() {
        return this.model;
    }
}