package it.neverworks.model.collections;

public interface Map<K,V> extends java.util.Map<K,V> {
    void addRawItem( K key, V value );
    void removeRawKey( K key );
    Map<K,V> set( K key, V value );
}