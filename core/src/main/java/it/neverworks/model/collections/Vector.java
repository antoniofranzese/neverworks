package it.neverworks.model.collections;

import org.apache.commons.lang.StringUtils;
import it.neverworks.model.description.ModelInstance;
import it.neverworks.model.description.PropertyDescriptor;
import it.neverworks.model.expressions.ObjectQuery;

public class Vector<T> extends java.util.Vector<T> implements ModelCollection, List<T> {
    
    private ListMetadata metadata = new ListMetadata( this );
    
    public Vector() {
        super();
    }
    
    Vector( java.util.Collection<? extends T> c ) {
        super( c );
    } 
    
    public Vector( int initialCapacity ) {
        super( initialCapacity );
    }

    public Vector( int initialCapacity, int capacityIncrement ) {
        super( initialCapacity, capacityIncrement );
    } 

    public InstanceMetadata metadata() {
        return this.metadata;
    }
    
    public void adoptModel( ModelInstance model, PropertyDescriptor property ) {
        metadata.adoptModel( model, property );
    }
    
    public void orphanModel( ModelInstance model, PropertyDescriptor property ) {
        metadata.orphanModel( model, property );
    }

    @Override
    public List<T> add( T... items ) {
        for( T item: items ) {
            metadata.add( item, size() );
        }
        return this;
    }
    
    @Override
    public void add( int index, T item ) {
        metadata.add( item, index );
    }

    @Override
    public boolean add( T item ) {
        return metadata.add( item, size() );
    }
    
    @Override
    public boolean remove( Object item ) {
        remove( this.indexOf( item ) );
        return true;
    } 
    
    @Override
    public T remove( int index ) {
        return (T)metadata.remove( index );
    }
    
    @Override
    public void clear() {
        if( metadata.hasRemover() ) {
            for( int i = this.size() - 1; i >= 0; i-- ) {
                remove( i );
            }
        } else {
            super.clear();
        }
    }
 
    @Override
    public void addRawItem( T item, Integer index ) {
        if( index == size() ){
            super.add( (T)metadata.adoptItem( item ) );
        } else {
            super.add( index.intValue(), (T)metadata.adoptItem( item ) );
        }
    }
    
    @Override
    public void removeRawItem( Integer index ){
        metadata.orphanItem( super.remove( index.intValue() ) );
    }
    
    @Override
    public String join( String delimiter ) {
        return StringUtils.join( this, "," );
    }
    
    @Override
    public ObjectQuery<T> query() {
        return new ObjectQuery<T>( this );
    }
    
}