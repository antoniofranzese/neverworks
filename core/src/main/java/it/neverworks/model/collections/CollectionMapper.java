package it.neverworks.model.collections;

public class CollectionMapper {
    
    private static java.util.Map<Class<?>, Class<? extends ModelCollection>> mappings = new java.util.HashMap<Class<?>, Class<? extends ModelCollection>>();
    
    static {
        mappings.put( it.neverworks.model.collections.List.class, ArrayList.class );
        mappings.put( java.util.List.class,                       ArrayList.class );
        mappings.put( java.util.ArrayList.class,                  ArrayList.class );
        mappings.put( java.util.Vector.class,                     Vector.class );
        mappings.put( it.neverworks.model.collections.Map.class,  HashMap.class );
        mappings.put( java.util.Map.class,                        HashMap.class );
        mappings.put( java.util.HashMap.class,                    HashMap.class );
    }
    
    public static Class<? extends ModelCollection> remap( Class<?> source ) {
        if( mappings.containsKey( source ) ) {
            return mappings.get( source );
        } else {
            return null;
        }
    }
}