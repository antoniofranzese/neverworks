package it.neverworks.model.collections;

import java.util.Collection;
import it.neverworks.model.description.PropertyDescriptor;

public interface Remover<T> {
    
    void remove();
    void decline();
    public T item();
    PropertyDescriptor property();
    String name();
    boolean contains( T item );
}