package it.neverworks.model;

import it.neverworks.lang.Wrapper;
import it.neverworks.model.expressions.ExpressionEvaluator;

public class Hook extends Value {
    protected Object instance;
    protected String expression;

    protected static class WrappedHook extends Hook {
        private Wrapper wrapper;
        
        public WrappedHook( Wrapper wrapper ) {
            super( null, null );
            this.wrapper = wrapper;
        }
        
        public Object asObject() {
            return this.wrapper.asObject();
        }

        protected void setObject( Object value ) {
            throw new IllegalStateException( "WrappedHook is not mutable" );
        }
    }
    
    public static Hook of( Wrapper wrapper ) {
        return new WrappedHook( wrapper );
    }

    public static Hook of( Object instance, String expression ) {
        return new Hook( instance, expression );
    }
    
    protected Hook( Object instance, String expression ) {
        super( null );
        this.instance = instance;
        this.expression = expression;
    }
    
    public Object asObject() {
        return ExpressionEvaluator.evaluate( instance, expression );
    }
    
    protected void setObject( Object value ) {
        ExpressionEvaluator.valorize( instance, expression, value );
    }
}

