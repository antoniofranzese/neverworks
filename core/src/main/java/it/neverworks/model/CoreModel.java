package it.neverworks.model;

import it.neverworks.lang.Tuple;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.lang.Arguments;
import it.neverworks.model.description.ModelCopier;
import it.neverworks.model.features.Get;
import it.neverworks.model.features.Copy;
import it.neverworks.model.features.Probe;
import it.neverworks.model.features.Duplicate;
import it.neverworks.model.features.Language;

public abstract class CoreModel extends AbstractModel implements Language, Get, Probe, Copy, Duplicate {
    
    public CoreModel() {
        super();
    }
    
    public CoreModel( Arguments arguments ) {
        this();
        this.modelInstance.assign( arguments );
    }
    
    public <T> T get( String name ) {
        return (T)this.modelInstance.eval( name );
    }

    public <T> T get( String name, T defaultValue ) {
        return this.modelInstance.safeEval( name, defaultValue );
    }
    
    public boolean has( String name ) {
        return this.containsItem( name );
    }
    
}