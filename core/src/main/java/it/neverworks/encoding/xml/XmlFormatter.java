package it.neverworks.encoding.xml;

import java.util.regex.Pattern;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;

public class XmlFormatter extends AbstractXmlPrinter {

    private Pattern CLOSING1 = Pattern.compile( ".+</\\w[^>]*>$" );
    private Pattern CLOSING2 = Pattern.compile( "^</\\w.*" );
    private Pattern OPENING = Pattern.compile( "^<\\w[^>]*[^/]?>.*$" );
    
	private String text;
	private String indentString = "    ";
    private String newLine = "\n";

	public XmlFormatter(String text) {
		super();
		this.text = text;
	}
	
	public XmlFormatter indent( String indent ) {
		this.indentString = indent;
		return this;
	}

	public XmlFormatter newLine( String newLine ) {
		this.newLine = newLine;
		return this;
	}
	
	public String asString() {
        if( Strings.hasText( text ) ) {
            StringBuilder formatted = new StringBuilder( text.length() * 2 );

            int pad = 0;
        
            for( String node: text.replaceAll( "(>)\\s*(<)(\\/*)", "$1\n$2$3" ).split( "\n" ) ) {
                int indent = 0;
            
                if( CLOSING1.matcher( node ).matches() ) {
                    indent = 0;
                } else if( CLOSING2.matcher( node).matches() ) {
                    if( pad != 0 ) {
                        pad -= 1;
                    }
                } else if( OPENING.matcher( node ).matches() ) {
                    indent = 1;
                } else {
                    indent = 0;
                }
            
                for( int i = 0; i < pad; i++ ) {
                    formatted.append( indentString );
                }

                formatted
                    .append( node )
                    .append( newLine );
                
                pad += indent;
            
            
            }
            return formatted.toString();
        } else {
            return text;
        }
	}
    
    
}
