package it.neverworks.encoding.xml;

import it.neverworks.lang.Errors;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class TransformerSupport  {

    /** Logger available to subclasses. */
    protected final Log logger = LogFactory.getLog(getClass());

    private static TransformerFactory transformerFactory = TransformerFactory.newInstance();

    /** Returns the <code>TransformerFactory</code>. */
    protected TransformerFactory getTransformerFactory() {
        return transformerFactory;
    }

    /**
     * Creates a new <code>Transformer</code>. Must be called per request, as transformers are not thread-safe.
     *
     * @return the created transformer
     * @throws TransformerConfigurationException
     *          if thrown by JAXP methods
     */
    protected final Transformer createTransformer() throws TransformerConfigurationException {
        return transformerFactory.newTransformer();
    }

    /**
     * Transforms the given {@link Source} to the given {@link Result}. Creates a new {@link Transformer} for every
     * call, as transformers are not thread-safe.
     *
     * @param source the source to transform from
     * @param result the result to transform to
     * @throws TransformerException if thrown by JAXP methods
     */
    protected final void transform(Source source, Result result) {
        try {
			Transformer transformer = createTransformer();
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			Errors.lower( e );
		} catch (TransformerException e) {
			Errors.lower( e );
		}
    }

	
}
