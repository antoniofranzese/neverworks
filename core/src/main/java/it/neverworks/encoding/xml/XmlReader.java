package it.neverworks.encoding.xml;

import static it.neverworks.language.*;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;

import org.jdom.Element;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.JDomReader;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.UnimplementedException;

public abstract class XmlReader {
		
	public Element asElement() {
		return doGetElement();
	}
	
	protected abstract Element doGetElement();
	
	public String asString() {
		return doGetString();
	}
	
	protected abstract String doGetString();

	protected abstract void doParseSource( DefaultHandler handler );
	
	
	@SuppressWarnings("unchecked")
	public <T> T as( Class<T> type ) {
        return as( type, new Arguments() );
    }

	public <T> T as( Class<T> type, Arguments arguments ) {

        if( Map.class.equals( type ) ) {
            if( arguments.get( "flat", false ) ) {
                return (T) asFlatMap( arguments );
            } else {
                throw new UnimplementedException( "Tree map reader is not implemented yet" );
            }
        } else {
    		Element element = doGetElement();
    		XStream xstream = new XStream( new PureJavaReflectionProvider() );
    		xstream.autodetectAnnotations( true );
    		xstream.alias( element.getName(), type );
		
    		return (T)xstream.unmarshal( new JDomReader( element ) );
        }
	}

	protected Map<String, String> asFlatMap( Arguments arguments ) {
        boolean includeRoot = type( Boolean.class ).process( arguments.get( "root", false ) );
        boolean includeAttributes = type( Boolean.class ).process( arguments.get( "attributes", false ) );
        
		Map<String, String> params = new TreeMap<String, String>();
		doParseSource( new MapFlatteningHandler( params, includeRoot, includeAttributes ) );
		return params;
	}
	
	protected class MapFlatteningHandler extends DefaultHandler {

		private Stack<String> elements = new Stack<String>();
		private boolean skipRoot;
		private boolean includeAttributes;
		private Map<String, String> params;
		private StringBuilder characters = new StringBuilder( 512 );
		
		public MapFlatteningHandler(Map<String, String> params, boolean includeRoot, boolean includeAttributes) {
			super();
			this.params = params;
			this.skipRoot = !includeRoot;
			this.includeAttributes = includeAttributes;
		}

		private String getCurrentPath() {
			StringBuilder path = new StringBuilder( 64 );
			Iterator<String> steps = elements.iterator();
			while( steps.hasNext() ) {
				path.append( steps.next() );
				if( steps.hasNext() ) {
					path.append( "." );
				}
			}
			return path.toString();
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			characters.append( Arrays.copyOfRange( ch, start, start + length ) );
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if( !skipRoot ) {
				elements.push( qName );
			} else {
				skipRoot = false;
			}
			
			if( includeAttributes && attributes.getLength() > 0 ) {
				for( int i = 0; i < attributes.getLength(); i++ ) {
					elements.push( attributes.getQName( i ) );
					params.put( getCurrentPath(), attributes.getValue( i ) );
					elements.pop();
				}
			}
			characters.delete( 0, characters.length() );
		}
		
		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if( elements.size() > 0 ) {
				if( characters.length() > 0 ) {
					params.put( getCurrentPath(), characters.toString() );
					characters.delete( 0, characters.length() );
				}
				elements.pop();
			}
		}

	}

}
