package it.neverworks.encoding.xml;

import it.neverworks.lang.Errors;

import java.io.Reader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.StringReader;
import java.io.ByteArrayInputStream;

public abstract class AbstractXmlPrinter {

	public abstract String asString();
    
    public Reader asReader() {
        return new StringReader( this.asString() );
    }
    
    public InputStream asStream() {
        return new ByteArrayInputStream( this.asString().getBytes() );
    }
    
    
}
