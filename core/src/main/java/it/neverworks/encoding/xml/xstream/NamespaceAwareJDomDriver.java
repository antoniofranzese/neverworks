package it.neverworks.encoding.xml.xstream;

import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.jdom.Namespace;
import org.springframework.util.Assert;

import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.JDomDriver;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;

public class NamespaceAwareJDomDriver extends JDomDriver {
	
	protected Namespace namespace;
	
	public NamespaceAwareJDomDriver() {
		super();
	}
	
	public NamespaceAwareJDomDriver( Namespace namespace ) {
		super();
		Assert.notNull( namespace );
		this.namespace = namespace;
	}

	public NamespaceAwareJDomDriver( Namespace namespace, XmlFriendlyReplacer replacer) {
		super(replacer);
		Assert.notNull( namespace );
		this.namespace = namespace;
	}

	@Override
    public HierarchicalStreamWriter createWriter(Writer out) {
        return new NamespaceAwarePrettyPrintWriter( namespace.getPrefix(), namespace.getURI(), out, xmlFriendlyReplacer());
    }

	@Override
    public HierarchicalStreamWriter createWriter(OutputStream out) {
        return new NamespaceAwarePrettyPrintWriter( namespace.getPrefix(), namespace.getURI(), new OutputStreamWriter(out));
    }

	public Namespace getNamespace() {
		return namespace;
	}

	public void setNamespace(Namespace namespace) {
		this.namespace = namespace;
	}



}
