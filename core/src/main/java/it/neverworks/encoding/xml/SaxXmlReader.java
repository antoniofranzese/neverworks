package it.neverworks.encoding.xml;

import static it.neverworks.language.*;

import it.neverworks.lang.Errors;
import it.neverworks.io.Streams;

import java.io.IOException;
import java.nio.CharBuffer;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.JDomReader;

public class SaxXmlReader extends XmlReader {

	private InputSource source;

	public SaxXmlReader(InputSource source) {
		super();
		this.source = source;
	}

	protected void doParseSource( DefaultHandler handler ){
		try {
			SAXParserFactory sfactory = SAXParserFactory.newInstance();
			SAXParser parser = sfactory.newSAXParser();
			parser.parse( source, handler );
		} catch (Exception e) {
			throw Errors.wrap( e );
		}
	}

	@Override
	protected String doGetString() {
		if( source.getByteStream() != null ) {
			return Streams.asString( source.getByteStream(), arg( "encoding", source.getEncoding() ) );
		} else {
			return Streams.asString( source.getCharacterStream() );
		}
	}

	@Override
	protected Element doGetElement() {
		try {
			return new SAXBuilder().build( source ).getRootElement();
		} catch (JDOMException e) {
			throw Errors.wrap( e );
		} catch (IOException e) {
			throw Errors.wrap( e );
		}
	}

}
