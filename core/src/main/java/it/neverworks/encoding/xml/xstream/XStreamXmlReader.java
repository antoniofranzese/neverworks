package it.neverworks.encoding.xml.xstream;

import org.jdom.Element;
import org.xml.sax.helpers.DefaultHandler;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.JDomWriter;
import com.thoughtworks.xstream.io.xml.SaxWriter;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;

import it.neverworks.encoding.xml.XmlReader;

public class XStreamXmlReader extends XmlReader {

	private Object object;
	XStream xstream;
	
	public XStreamXmlReader(Object object) {
		super();
		this.object = object;
		xstream = new XStream( new PureJavaReflectionProvider() );
		xstream.autodetectAnnotations( true );
	}
	
	@Override
	protected Element doGetElement() {
		Element element = new Element( "node" );
		JDomWriter writer = new JDomWriter( element );
		xstream.marshal( this.object, writer );
		return (Element)element.getChildren().get(0);
	}

	@Override
	protected String doGetString() {
		return xstream.toXML( this.object );
	}

	@Override
	protected void doParseSource(DefaultHandler handler) {
		SaxWriter writer = new SaxWriter();
		writer.setContentHandler( handler );
		xstream.marshal( this.object, writer );
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T as(Class<T> type) {
		if( type.isAssignableFrom( this.object.getClass() ) ) {
			return (T)this.object;
		} else {
			throw new IllegalStateException( "Incompatible object" );
		}
	}

}
