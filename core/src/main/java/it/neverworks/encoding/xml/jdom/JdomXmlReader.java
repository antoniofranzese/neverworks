package it.neverworks.encoding.xml.jdom;

import it.neverworks.lang.Errors;

import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.output.SAXOutputter;
import org.jdom.output.XMLOutputter;
import org.xml.sax.helpers.DefaultHandler;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.JDomReader;

import it.neverworks.encoding.xml.XmlReader;

public class JdomXmlReader extends XmlReader{

	private Element element;
	
	public JdomXmlReader(Element element) {
		super();
		this.element = element;
	}

	@Override
	protected void doParseSource(DefaultHandler handler) {
		try {
			SAXOutputter outputter = new SAXOutputter( handler );
			outputter.output( element );
		} catch (JDOMException e) {
			throw Errors.wrap( e );
		}
	}

	@Override
	protected String doGetString() {
		XMLOutputter outputter = new XMLOutputter();
		return outputter.outputString( element );
	}

	@Override
	protected Element doGetElement() {
		return element;
	}

}
