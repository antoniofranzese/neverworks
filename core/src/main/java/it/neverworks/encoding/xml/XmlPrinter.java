package it.neverworks.encoding.xml;

import it.neverworks.lang.Errors;

import java.io.Reader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.StringReader;
import java.io.ByteArrayInputStream;

import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class XmlPrinter extends AbstractXmlPrinter {

	private XmlReader reader;
	private String indentString = "    ";
	private Format format = Format.getPrettyFormat();

	public XmlPrinter(XmlReader reader) {
		super();
		this.reader = reader;
		
	}
	
	public XmlPrinter indent( String indent ) {
		this.indentString = indent;
		return this;
	}
	
	public XmlPrinter pretty() {
		format = Format.getPrettyFormat();
		return this;
	}
	
	public XmlPrinter compact() {
		format = Format.getCompactFormat();
		return this;
	}
	
	public XmlPrinter raw() {
		format = Format.getRawFormat();
		return this;
	}

	public String asString() {
		try {
			format.setIndent( indentString );
			XMLOutputter outputter = new XMLOutputter(format);
			StringWriter writer = new StringWriter();
			outputter.output( reader.asElement(), writer );
			return writer.toString();
		} catch (IOException e) {
			throw Errors.wrap( e );
		}
	}
    
}
