package it.neverworks.encoding.xml.xstream;

import it.neverworks.lang.Strings;

import java.io.Writer;

import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.AbstractXmlWriter;
import com.thoughtworks.xstream.io.xml.PrettyPrintWriter;
import com.thoughtworks.xstream.io.xml.XmlFriendlyReplacer;

public class NamespaceAwarePrettyPrintWriter extends AbstractXmlWriter {

	protected String prefix;
	protected String uri;
	protected boolean firstNode = true;
	private PrettyPrintWriter writer;

	public NamespaceAwarePrettyPrintWriter(String prefix, String uri, Writer writer, XmlFriendlyReplacer replacer) {
		super(replacer);
		this.prefix = prefix;
		this.uri = uri;
		this.writer = new PrettyPrintWriter( writer, replacer );
	}

	public NamespaceAwarePrettyPrintWriter(String prefix, String uri, Writer writer) {
		super();
		this.prefix = prefix;
		this.uri = uri;
		this.writer = new PrettyPrintWriter( writer );
	}


	@Override
	public void startNode(String name, Class clazz) {
		startNode(name);
		if( firstNode ) {
			if ( Strings.hasText( prefix ) ) {
				writer.addAttribute( "xmlns:" + prefix, uri );
			} else {
				writer.addAttribute( "xmlns", uri );
			}
			firstNode = false;
		}
	}

	@Override
	public HierarchicalStreamWriter underlyingWriter() {
		return writer.underlyingWriter();
	}

	public void addAttribute( String key, String value ) {
		writer.addAttribute( key, value );
	}

	public void close() {
		writer.close();
	}

	public void endNode() {
		writer.endNode();
	}

	public void flush() {
		writer.flush();
	}

	public void setValue(String text) {
		writer.setValue( text );
	}

	public void startNode(String name) {
		if ( Strings.hasText( prefix ) ) {
			writer.startNode( prefix + ":" + name );
		} else {
			writer.startNode( name );
		}
		
	}

}
