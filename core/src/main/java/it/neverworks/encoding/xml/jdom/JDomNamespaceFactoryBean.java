package it.neverworks.encoding.xml.jdom;

import org.jdom.Namespace;
import org.springframework.beans.factory.FactoryBean;

public class JDomNamespaceFactoryBean implements FactoryBean {

	private String prefix = "";
	private String uri;
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Object getObject() throws Exception {
		return Namespace.getNamespace( prefix, uri );
	}

	public Class getObjectType() {
		return Namespace.class;
	}

	public boolean isSingleton() {
		return false;
	}

}
