package it.neverworks.encoding;

import it.neverworks.lang.Arguments;
import it.neverworks.lang.Strings;

public class HTTP {
    
    public static Arguments queryArguments( String queryString ) {
        Arguments result = new Arguments();
        String[] params = Strings.safe( queryString ).split( "&" );  

        for( String param : params ) {
            String p = param.trim();
            
            if( p.indexOf( "=" ) > 0 ) {
                String[] parts = p.split( "=" );
                if( parts.length > 1 ) {
                    result.arg( URL.decode( parts[ 0 ] ), URL.decode( parts[ 1 ] ) );
                } else if( parts.length == 1 ) {
                    result.arg( URL.decode( parts[ 0 ] ), "" );
                }
            } else {
                result.arg( URL.decode( p ), "" );
            }

        }  

        return result; 
    }

}