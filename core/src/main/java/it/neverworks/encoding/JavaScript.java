package it.neverworks.encoding;

import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang.StringEscapeUtils;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Collections;

public class JavaScript {
    
    public static String encode( Object text ) {
        return text != null ? encode( text instanceof String ? (String)text : text.toString() ) : "";
    }
    
    public static String encode( String text ) {
        //return text != null ? StringEscapeUtils.escapeJavaScript( text ).replace( "\\\"", "\\x22" ).replace( "\\\\\\\"", "\\x22" ).replace( "\\'", "\\x27" ).replace( "\\\\n", "\\n" ) : null;
        return text != null ? StringEscapeUtils.escapeJavaScript( text )
            .replace( "\\\\\\\"", "\\x22" )
            .replace( "\\'", "\\x27" )
            .replace( "<", "\\x3c" )
            .replace( ">", "\\x3e" )
            .replace( "\\\\n", "\\n" ) 
            : null;
    }
    
    public static String decode( String text ) {
        return StringEscapeUtils.unescapeJavaScript( text );
    }
    
    public static String literal( Object value ) {
        if( value == null ) {
            return "null";
        } else if( value instanceof Number ) {
            return Strings.valueOf( value );
        } else if( value instanceof Boolean ) {
            return Boolean.TRUE.equals( value ) ? "true" : "false";
        } else if( value instanceof Iterable ) {
            List<String> result = new ArrayList<String>();
            for( Object item: (Iterable) value ) {
                result.add( literal( item ) );
            }
            return "[" + Collections.join( result, "," ) + "]";
        } else {
            return "\"" + encode( Strings.valueOf( value ) ) + "\"";
        }
    }

}