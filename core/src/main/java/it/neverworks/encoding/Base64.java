package it.neverworks.encoding;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.commons.codec.binary.Base64OutputStream;

import it.neverworks.io.Streams;
import it.neverworks.io.InputStreamProvider;
import it.neverworks.io.StringWriter;
import it.neverworks.io.WriterOutputStream;

public class Base64 /* implements InputStreamProvider */ {
    
    public static String asString( InputStream stream ) {
        StringWriter writer = new StringWriter();
        Streams.copy( 
            stream
            ,new Base64OutputStream( new WriterOutputStream( writer ), /* doEncode */ true, /* lineLength */ -1, /* lineSeparator */ null ) 
        );
        return writer.toString();
    }
    
    
    
    // private String content;
    // private InputStream stream;
    // private boolean hasContent;
    //
    // public Base64( String content ) {
    //     this.content = content;
    //     this.hasContent = true;
    // }
    //
    // public Base64( InputStream stream ) {
    //     this.stream = stream;
    //     this.hasContent = false;
    // }
    //
    // public InputStream getStream() {
    //     return new Base64InputStream( this.getRawStream() );
    // }
    //
    // protected InputStream getRawStream() {
    //     if( this.hasContent ) {
    //         return Streams.asStream( this.content );
    //     } else {
    //         return this.stream;
    //     }
    // }
    //
    // protected String getRawContent() {
    //     if( this.hasContent ) {
    //         return this.content;
    //     } else {
    //         return Streams.asString( this.stream );
    //     }
    // }
    //
    // public InputStream toStream() {
    //     return getStream();
    // }

}