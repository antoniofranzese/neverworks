package it.neverworks.encoding.json;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.encoding.JSON;

public interface JSONEncodable {
    
    default JsonNode toJSON() {
        return JSON.mapper().valueToTree( this );
    }

}