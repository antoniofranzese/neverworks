package it.neverworks.encoding.json;

public interface JSONSerializable {
    
    Object asJSON();
}