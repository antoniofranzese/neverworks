package it.neverworks.encoding.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

public class RelaxedJSONCodec extends UnquotedJSONCodec {
    
    @Override
    public ObjectMapper mapper() {
        ObjectMapper mapper = super.mapper();
        mapper.configure( JsonParser.Feature.ALLOW_SINGLE_QUOTES, true );
        mapper.configure( JsonParser.Feature.ALLOW_COMMENTS, true );
        mapper.configure( JsonGenerator.Feature.QUOTE_FIELD_NAMES, false );
        return mapper;
    }
    
}