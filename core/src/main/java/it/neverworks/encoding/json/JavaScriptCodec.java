package it.neverworks.encoding.json;

import java.util.Date;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.lang.Strings;

public class JavaScriptCodec extends JSONCodec {

    public JsonNode encode( Object value ) {
        if( value instanceof String ) {
            return new JavaScriptTextNode( (String) value );
        } else {
            return super.encode( value );
        }
    }
    
}