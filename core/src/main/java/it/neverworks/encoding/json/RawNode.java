package it.neverworks.encoding.json;

import com.fasterxml.jackson.databind.node.TextNode;

public class RawNode extends TextNode {
    public RawNode( String text ) {
        super( text );
    }

    public String toString(){
        return super.textValue();
    }
}
