package it.neverworks.encoding.json;

import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Date;
import java.io.StringReader;
import java.io.InputStream;
import java.math.BigDecimal;

import com.fasterxml.jackson.databind.node.BooleanNode;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.FloatNode;
import com.fasterxml.jackson.databind.node.DoubleNode;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.JsonNode;
import org.joda.time.DateTime;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Errors;
import it.neverworks.lang.Dates;
import it.neverworks.lang.Listable;
import it.neverworks.lang.Arguments;
import it.neverworks.lang.Collections;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.FluentArrayList;
import it.neverworks.io.StringWriter;
import it.neverworks.io.Streams;

public class JSONCodec {
    
    public JsonNode encode( Object value ) {
        if( value == null ) {
            return NullNode.instance;

        } else if( value instanceof JSONEncodable ) {
            return ((JSONEncodable) value).toJSON();

        } else if( value instanceof JSONSerializable ) {
            return encode( ((JSONSerializable) value).asJSON() );
        
        } else if( value instanceof JsonNode ) {
            return (JsonNode)value;
        
        } else if( value instanceof String ) {
            return new TextNode( (String)value );

        } else if( value instanceof Long ) {
            return new LongNode( (Long)value );

        } else if( value instanceof Float ) {
            return new FloatNode( (Float)value );

        } else if( value instanceof Double ) {
            return new DoubleNode( (Double)value );

        } else if( value instanceof Boolean ) {
            return ((Boolean) value).booleanValue() ? BooleanNode.TRUE : BooleanNode.FALSE;

        } else if( value instanceof BigDecimal ) {
            BigDecimal dec = (BigDecimal) value;
            if( dec.scale() > 0 ) {
                return new DoubleNode( dec.doubleValue() );
            } else {
                return new LongNode( dec.longValue() );
            }

        } else if( value instanceof Number ) {
            return new IntNode( ((Number) value).intValue() );

        } else if( value instanceof Date || value instanceof DateTime ) {
            return new TextNode( Dates.toISOWithMillis( value ) );

        } else if( value instanceof FluentMap ) {
            ObjectNode obj = new ObjectNode( JsonNodeFactory.instance );
            for( Object name: ((Arguments) value).keys() ) {
                obj.set( name.toString(), encode( ((Arguments) value ).get( name ) ) );
            }
            return obj;

        } else if( value instanceof Map ) {
            ObjectNode obj = new ObjectNode( JsonNodeFactory.instance );
            for( Object name: ((Map) value).keySet() ) {
                obj.set( name.toString(), encode( ((Map) value ).get( name ) ) );
            }
            return obj;

        } else if( Collections.isIterable( value ) ) {
            ArrayNode array = new ArrayNode( JsonNodeFactory.instance );
            for( Object item: Collections.iterable( value ) ) {
                array.add( encode( item ) );
            }
            return array;


        } else {
            throw new RuntimeException( "Cannot encode to JSON: " + value.getClass().getName() );
        }
    }

    public String print( Object value ) {
        if( value != null ) {
            try {
                return mapper().writeValueAsString( encode( value ) );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            return null;
        }
    }
    
    public InputStream stream( Object value ) {
        return Streams.asStream( print( value ) );
    }
    
    public JsonNode parse( String text ) {
        if( Strings.hasText( text ) ) {
            try {
                return mapper().readTree( new StringReader( text ) );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        } else {
            return NullNode.instance;
        }

    }

    public Object decode( String text ) {
        return decode( parse( text ) );
    }
    
    public ObjectMapper mapper() {
        return new ObjectMapper();
    }
    
    public Object decode( JsonNode node ) {
        if( node != null ) {
            switch( node.getNodeType() ) {
                case ARRAY:
                    return decodeArray( (ArrayNode) node );
                
                case BINARY:
                    throw new RuntimeException( "BINARY node is not supported" );

                case BOOLEAN:
                    return node.asBoolean();

                case NULL:
                    return null;

                case NUMBER:
                    return node.numberValue();

                case OBJECT:
                    return decodeObject( (ObjectNode) node );
 
                case POJO:
                    throw new RuntimeException( "POJO node is not supported" );
 
                case STRING:
                    return node.asText();
 
                default:
                    throw new RuntimeException( "Node type is not supported: " + node.getNodeType() );
            }
        } else {
            return null;
        }
    }
    
    protected Object decodeArray( ArrayNode array ) {
        FluentArrayList result = new FluentArrayList( array.size() );
        for( int i = 0; i < array.size(); i++ ) {
            result.add( decode( array.get( i ) ) );
        }
        return result;
    }
    
    protected Object decodeObject( ObjectNode obj ) {
        Arguments result = new Arguments();
        Iterator<String> names = obj.fieldNames();
        while( names.hasNext() ) {
            String name = names.next();
            result.arg( name, decode( obj.get( name ) ) );
        }
        return result;
    }
    
    public ObjectNode object() {
        return mapper().createObjectNode();
    }

    public ArrayNode array() {
        return mapper().createArrayNode();
    }
    
    public JsonNode serialize( Object object ) {
        return mapper().valueToTree( object );
    }

    public <T> T deserialize( Object object, Class<T> target ) {
        try {
            if( object instanceof String ) {
                return mapper().readValue( (String) object, target );
            } else if( object instanceof InputStream ) {
                return mapper().readValue( (InputStream) object, target );
            } else {
                return mapper().treeToValue( encode( object ), target );
            }
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

}