package it.neverworks.encoding.json;

import com.fasterxml.jackson.databind.node.TextNode;
import it.neverworks.encoding.JavaScript;

public class JavaScriptTextNode extends TextNode {
    public JavaScriptTextNode( String text ) {
        super( text );
    }

    public String toString(){
        String text = super.toString();
        //TODO: testare con textValue() al posto del substring
        return "\"" + JavaScript.encode( text.substring( 1, text.length() - 1 ) ) + "\"";
    }
}

    
