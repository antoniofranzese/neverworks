package it.neverworks.encoding.json;

import java.util.Date;
import org.joda.time.DateTime;

import com.fasterxml.jackson.databind.JsonNode;

import it.neverworks.lang.Strings;

public class JavaScriptObjectCodec extends JavaScriptCodec {

    public JsonNode encode( Object value ) {
        if( value instanceof Date ) {
            DateTime date = new DateTime( ((Date) value).getTime() );
            return new RawNode( Strings.message( "new Date({0,number,0000},{1},{2},{3},{4},{5},{6})"
                ,date.year().get()
                ,date.monthOfYear().get() - 1
                ,date.dayOfMonth().get()
                ,date.hourOfDay().get()
                ,date.minuteOfHour().get()
                ,date.secondOfMinute().get()
                ,date.millisOfSecond().get()
            ));
        } else {
            return super.encode( value );
        }
    }
    
}