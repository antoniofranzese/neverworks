package it.neverworks.encoding.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;

public class UnquotedJSONCodec extends JSONCodec {
    
    @Override
    public ObjectMapper mapper() {
        ObjectMapper mapper = super.mapper();
        mapper.configure( JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true );
        mapper.configure( JsonGenerator.Feature.QUOTE_FIELD_NAMES, false );
        return mapper;
    }
    
}