package it.neverworks.encoding;

import java.util.regex.Pattern;
import org.apache.commons.lang.StringEscapeUtils;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.html.HTMLProcessor;

public class HTML {
    
    private String content;
    
    public HTML( String content ) {
        this.content = content;
    }
    
    public String toString() {
        return this.content;
    }
    
    public static String encode( Object text ) {
        return encode( Strings.valueOf( text ) );
    }
    
    public static String encode( String text ) {
        return Strings.hasText( text ) ? StringEscapeUtils.escapeHtml( text ) : "";
    }

    private final static Pattern ORIGINAL_LT      = Pattern.compile( "<" ); 
    private final static Pattern ORIGINAL_GT      = Pattern.compile( ">" ); 
    private final static Pattern ORIGINAL_QUOT    = Pattern.compile( "\"" ); 
    private final static Pattern ORIGINAL_ENTITY  = Pattern.compile( "\\&([a-zA-Z0-9#]+)[\\s;]" ); 
    private final static Pattern SCRAMBLED_LT     = Pattern.compile( "\\$LT\\$" ); 
    private final static Pattern SCRAMBLED_GT     = Pattern.compile( "\\$GT\\$" ); 
    private final static Pattern SCRAMBLED_QUOT   = Pattern.compile( "\\$QU\\$" ); 
    private final static Pattern SCRAMBLED_ENTITY = Pattern.compile( "\\$\\\\([a-zA-Z0-9#]+)\\$" ); 

    public static String escape( Object text ) {
        return escape( Strings.valueOf( text ) );
    }
    
    public static String escape( String text ) {
        if( Strings.hasText( text ) ) {
            text = ORIGINAL_LT.matcher( text ).replaceAll( "\\$LT\\$" );
            text = ORIGINAL_GT.matcher( text ).replaceAll( "\\$GT\\$" );
            text = ORIGINAL_QUOT.matcher( text ).replaceAll( "\\$QU\\$" );
            text = ORIGINAL_ENTITY.matcher( text ).replaceAll( "\\$\\\\$1\\$" );
            text = StringEscapeUtils.escapeHtml( text );
            text = SCRAMBLED_LT.matcher( text ).replaceAll( "<" );
            text = SCRAMBLED_GT.matcher( text ).replaceAll( ">" );
            text = SCRAMBLED_QUOT.matcher( text ).replaceAll( "\"" );
            text = SCRAMBLED_ENTITY.matcher( text ).replaceAll( "&$1;" );
            return text;
        } else {
            return "";
        }
    }
    
    public static String decode( Object text ) {
        return decode( Strings.valueOf( text ) );
    }

    public static String decode( String text ) {
        return Strings.hasText( text ) ? StringEscapeUtils.unescapeHtml( text ) : "";
    }

    public static String sanitize( Object html ) {
        return sanitize( Strings.valueOf( html ) );
    }
    
    private final static Pattern FORBIDDEN_TAGS       = Pattern.compile( "(?i)<\\s*(\\/?)\\s*(script|object|iframe|applet|embed|meta)" );
    private final static Pattern FORBIDDEN_PROTOCOLS  = Pattern.compile( "(?i)(javascript|behavior):" );
    private final static Pattern FORBIDDEN_ATTRIBUTES = Pattern.compile( "(?i)on([a-zA_Z]+)\\s*=" );
    private final static Pattern FORBIDDEN_EXEC       = Pattern.compile( "&\\s*\\{" );

    public static String sanitize( String html ) {
        if( Strings.hasText( html ) ) {
            html = FORBIDDEN_TAGS.matcher( html ).replaceAll( "<$1no-$2" );
            html = FORBIDDEN_PROTOCOLS.matcher( html ).replaceAll( "***:" );
            html = FORBIDDEN_ATTRIBUTES.matcher( html ).replaceAll( "no-$1=" );
            html = FORBIDDEN_EXEC.matcher( html ).replaceAll( "***{" );
            return html;
        } else {
            return "";
        }
    }
    
    // private final static HTMLProcessor sanitizeProcessor = new HTMLProcessor().sanitize();
    //
    // public static String sanitize2( String html ) {
    //     return sanitizeProcessor.process( html );
    // }
    //
    // private final static HTMLProcessor sanitizeAndEscapeProcessor = new HTMLProcessor().sanitize().escape();
    //
    // public static String sanitizeAndEscape2( String html ) {
    //     return sanitizeAndEscapeProcessor.process( html );
    // }
    //
    
    public static String strip( Object html ) {
        return strip( Strings.valueOf( html ) );
    }

    public static String strip( String html ) {
        return Strings.safe( html ).replaceAll( "\\<.*?\\>", "" );
    }
    
}