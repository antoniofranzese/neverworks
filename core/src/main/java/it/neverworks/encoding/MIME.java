package it.neverworks.encoding;

import static it.neverworks.language.*;

import java.util.List;
import java.io.File;
import java.io.InputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;
import net.sf.jmimemagic.MagicMatchNotFoundException;
import net.sf.jmimemagic.MagicParseException;
import net.sf.jmimemagic.MagicException;

import it.neverworks.lang.Pair;
import it.neverworks.lang.Application;
import it.neverworks.lang.Strings;
import it.neverworks.lang.FluentList;

public class MIME {
    
    public final static String DEFAULT = "application/octet-stream";
    
    private final static FluentList<Pair> mappings = list( "META-INF/application/mime.properties", "META-INF/neverworks/mime.properties" )
        .map( name -> Application.resource( name ) )
        .filter( stream -> stream != null )
        .map( stream -> {
            return each( new BufferedReader( new InputStreamReader( stream ) ).lines() )
                .map( line -> {
                    String[] parts = line.split( "=" );
                    return Pair.of( parts[ 1 ].trim(), parts[ 0 ].trim() );
                });
        })
        .list()
        .flatten( Pair.class );
    
    public static String detect( String name ) {
        return detect( name, DEFAULT );
    }
    
    public static String detect( String name, String defaultType ) {
        if( Strings.hasText( name ) ) {
            String n = name.trim();
            String type = mappings
                .filter( p -> name.endsWith( str( p.get( 0 ) ) ) )
                .map( p -> p.get( 1 ) )
                .list( String.class )
                .head();
            return type != null ? type : defaultType;
        } else {
            return defaultType;
        }
    }

    public static String detect( File file ) {
        return detect( file, DEFAULT );
    }
    
    public static String detect( File file, String defaultType ) {
        if( file != null ) {
            String type = null;

            try {
                MagicMatch match = Magic.getMagicMatch( file, /* extensionHints */ true, /* onlyMimeMatch */ false );

                if( match != null ) {
                    type = match.getMimeType();
                }
        
            } catch( MagicMatchNotFoundException ex ){}
              catch( MagicParseException ex ){}
              catch( MagicException ex ){}
            
            if( type == null ) {
                type = detect( file.getName(), defaultType );
            }
            
            return type;
            
        } else {
            return defaultType;
        }
    }
    
    public static String detect( InputStream stream ) {
        return detect( stream, DEFAULT );
    }

    public static String detect( InputStream stream, String defaultType ) {
        if( stream != null && stream.markSupported() ) {
    		try { 
                stream.mark( 1024 );
    			byte buffer[] = new byte[ 1000 ];
    			int readCount = stream.read( buffer );
                stream.reset();

                String type = null;

                try {
                    MagicMatch match = Magic.getMagicMatch( buffer, /* onlyMimeMatch */ true );
                    if( match != null ) {
                        type = match.getMimeType();
                    }
        
                } catch( MagicMatchNotFoundException ex ){}
                  catch( MagicParseException ex ){}
                  catch( MagicException ex ){}
  
                  return type != null ? type : defaultType;
                  
    		} catch( IOException e ) {
    			return defaultType;
    		}
            
        } else {
            return defaultType;
        }
    }
    
    public static String extension( String type ) {
        return extension( type, "" );
    }

    public static String extension( String type, String defaultExtension ) {
        if( Strings.hasText( type ) ) {

            String extension = mappings
                .filter( p -> type.equals( p.get( 1 ) ) )
                .map( p -> p.get( 0 ) )
                .list( String.class )
                .head();
            return extension != null ? extension : defaultExtension;
        } else {
            return defaultExtension;
        }
    }
    
}
