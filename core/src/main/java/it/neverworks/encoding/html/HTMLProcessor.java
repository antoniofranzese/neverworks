package it.neverworks.encoding.html;

import java.util.List;

import net.htmlparser.jericho.StreamedSource;
import net.htmlparser.jericho.Segment;
import net.htmlparser.jericho.StartTag;
import net.htmlparser.jericho.EndTag;
import net.htmlparser.jericho.Attribute;
import org.apache.commons.lang.StringEscapeUtils;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Numbers;
import it.neverworks.lang.FluentArrayList;

public class HTMLProcessor {
    
    private final static List<String> FORBIDDEN_TAGS = new FluentArrayList<String>().append( "script", "iframe", "object", "applet", "embed", "meta" );
    private final static List<String> FORBIDDEN_PROTOCOLS = new FluentArrayList<String>().append( "javascript", "behavior" );

    private boolean sanitize = false;
    private boolean escape = false;
    
    public HTMLProcessor() {
        super();
    }
    
    public HTMLProcessor sanitize() {
        this.sanitize = true;
        return this;
    }

    public HTMLProcessor escape() {
        this.escape = true;
        return this;
    }
    
    public String process( String html ) {

        StringBuilder builder = new StringBuilder( Numbers.Int( html.length() * 1.5 ) );
		StreamedSource streamedSource = new StreamedSource( html );

		for( Segment segment : HTMLParser.stream( html ) ) {
            
            if( segment instanceof StartTag ) {
                StartTag tag = (StartTag) segment;

                builder.append( "<" );

                if( this.sanitize && FORBIDDEN_TAGS.contains( tag.getName() ) ) {
                    builder.append( "no-" );
                }

                builder.append( tag.getName() );

                for( Attribute attr: tag.getAttributes() ) {
                    builder.append( " " );
                    
                    boolean sanitized = false;
                    if( this.sanitize && attr.getName().startsWith( "on" ) ) {
                        builder.append( "no-" ).append( attr.getName().substring( 2 ) );
                        sanitized = true;
                    } else {
                        builder.append( attr.getName() );
                    }

                    builder.append( "=\"" );
                    
                    if( this.sanitize && ! sanitized ) {
                        String value = attr.getValue();
                        int colon = value.indexOf( ":" );
                        
                        if( colon > 0 ) {
                            String protocol = value.substring( 0, colon ).trim().toLowerCase();
                            if( FORBIDDEN_PROTOCOLS.contains( protocol ) ) {
                                builder.append( "***" ).append( value.substring( colon ) );
                            } else {
                                builder.append( value );
                            }
                        
                        } else {
                            builder.append( value );    
                        
                        }
                    
                    } else {
                        builder.append( attr.getValue() );   
                    }
                    builder.append( "\"" );
                }

                builder.append( ">" );

            } else if( segment instanceof EndTag ) {
                EndTag tag = (EndTag) segment;
                builder.append( "</" );

                if( this.sanitize && FORBIDDEN_TAGS.contains( tag.getName() ) ) {
                    builder.append( "no-" );
                }

                builder.append( tag.getName() ).append( ">" );

            } else {
                if( this.escape ) {
                    builder.append( StringEscapeUtils.escapeHtml( Strings.valueOf( segment ) ) ); 
                } else {
                    builder.append( Strings.valueOf( segment ) );
                }

            }
		}
        
        return builder.toString();
    }
    
}