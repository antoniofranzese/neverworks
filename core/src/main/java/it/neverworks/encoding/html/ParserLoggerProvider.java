package it.neverworks.encoding.html;

import net.htmlparser.jericho.LoggerProvider;
import net.htmlparser.jericho.Logger;

public class ParserLoggerProvider implements LoggerProvider {
    
    public Logger getLogger( String name ) {
        return new ParserLogger( name );
    }

    public net.htmlparser.jericho.Logger getSourceLogger() {
        return getLogger( "net.htmlparser.jericho" );
    }
    
}
