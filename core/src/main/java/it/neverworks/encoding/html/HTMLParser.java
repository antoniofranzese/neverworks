package it.neverworks.encoding.html;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Reader;
import java.io.IOException;

import net.htmlparser.jericho.StreamedSource;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Objects;
import it.neverworks.io.FileInfo;
import it.neverworks.io.InputStreamProvider;

public class HTMLParser {
    
    static {
        net.htmlparser.jericho.Config.LoggerProvider = new ParserLoggerProvider();
    }
 
    public static StreamedSource stream( Object source ) {
        try {
            
            if( source instanceof InputStream ) {
                return new StreamedSource( (InputStream) source );
                
            } else if( source instanceof File ) {
                return new StreamedSource( new FileInputStream( (File) source ) );
            
            } else if( source instanceof Reader ) {
                return new StreamedSource( (Reader) source );
            
            } else if( source instanceof CharSequence ) {
                return new StreamedSource( (CharSequence) source );
            
            } else if( source instanceof FileInfo ) {
                return new StreamedSource( ((FileInfo) source ).getStream() ) ;
                
            } else if( source instanceof InputStreamProvider ) {
                return new StreamedSource( ((InputStreamProvider) source ).toStream() ) ;

            } else {
                throw new IllegalArgumentException( "Cannot parse HTML with " + Objects.repr( source ) );
            }

        } catch( IOException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
}