package it.neverworks.encoding.html;

import it.neverworks.log.Logger;

public class ParserLogger implements net.htmlparser.jericho.Logger {

    private Logger logger;
    
    public ParserLogger( String name ) {
        this.logger = Logger.of( name );
    }

    public void error(String paramString) {
        logger.error( paramString );
    }

    public void warn(String paramString) {
        logger.warn( paramString );
    }

    public void info(String paramString) {
        logger.info( paramString );
    }

    public void debug(String paramString) {
        logger.debug( paramString );
    }

    public boolean isErrorEnabled() {
        return logger.isErrorEnabled();
    }

    public boolean isWarnEnabled() {
        return logger.isWarnEnabled();
    }

    public boolean isInfoEnabled() {
        return logger.isInfoEnabled();
    }

    public boolean isDebugEnabled() {
        return logger.isDebugEnabled();
    }
}
