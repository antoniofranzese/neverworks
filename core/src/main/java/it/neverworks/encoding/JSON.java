package it.neverworks.encoding;

import java.io.InputStream;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import it.neverworks.encoding.json.JSONEncodable;
import it.neverworks.encoding.json.JSONCodec;
import it.neverworks.encoding.json.UnquotedJSONCodec;
import it.neverworks.encoding.json.RelaxedJSONCodec;
import it.neverworks.encoding.json.JavaScriptCodec;
import it.neverworks.encoding.json.JavaScriptObjectCodec;

public class JSON {
    
    public final static JSONCodec standard = new JSONCodec();
    public final static JSONCodec js = new JavaScriptCodec();
    public final static JSONCodec lang = new JavaScriptCodec();
    public final static JSONCodec object = new JavaScriptObjectCodec();
    public final static JSONCodec unquoted = new UnquotedJSONCodec();
    public final static JSONCodec relaxed = new RelaxedJSONCodec();
    
    private String content;
    
    public JSON( String content ) {
        this.content = content;
    }
    
    public String toString() {
        return this.content;
    }
    
    public static JsonNode encode( Object value ) {
        return standard.encode( value );
    }
    
    public static Object decode( String text ) {
        return standard.decode( text );
    }

    public static Object decode( JsonNode node ) {
        return standard.decode( node );
    }
    
    public static String print( Object value ) {
        return standard.print( value );
    }

    public static InputStream stream( Object value ) {
        return standard.stream( value );
    }
    
    public static JsonNode serialize( Object value ) {
        return standard.serialize( value );
    }

    public static <T> T deserialize( Object object, Class<T> target ) {
        return standard.deserialize( object, target );
    }

    public static ObjectNode object() {
        return standard.object();
    }

    public static ArrayNode array() {
        return standard.array();
    }
    
    public static ObjectMapper mapper() {
        return standard.mapper();
    }
    
}