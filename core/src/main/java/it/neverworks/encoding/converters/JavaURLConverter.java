package it.neverworks.encoding.converters;

import java.net.URL;
import java.net.MalformedURLException;
import it.neverworks.model.converters.Converter;

public class JavaURLConverter implements Converter {
    
    public Object convert( Object value ) {
        if( value instanceof URL ) {
            return value;

        } else if( value instanceof String ) {
            try {
                return new URL( (String) value );
            } catch( MalformedURLException ex ) {
                return value;
            }
            
        } else if( value instanceof it.neverworks.encoding.URL ) {
            return convert( ((it.neverworks.encoding.URL) value ).toString() );
            
        } else {
            return value;
        }

    }
    
}