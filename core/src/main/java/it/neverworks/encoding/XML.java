package it.neverworks.encoding;

import java.io.InputStream;
import java.io.StringReader;
import java.io.Reader;

import org.jdom.Element;
import org.xml.sax.InputSource;

import org.apache.commons.lang.StringEscapeUtils;
import it.neverworks.lang.Strings;
import it.neverworks.encoding.xml.XmlFormatter;
import it.neverworks.encoding.xml.XmlPrinter;
import it.neverworks.encoding.xml.XmlReader;
import it.neverworks.encoding.xml.SaxXmlReader;
import it.neverworks.encoding.xml.jdom.JdomXmlReader;
import it.neverworks.encoding.xml.xstream.XStreamXmlReader;

public class XML {
    
    public static String encode( Object text ) {
        return encode( Strings.valueOf( text ) );
    }
    
    public static String encode( String text ) {
        return Strings.hasText( text ) ? StringEscapeUtils.escapeXml( text ) : "";
    }
    
    public static String decode( Object text ) {
        return decode( Strings.valueOf( text ) );
    }

    public static String decode( String text ) {
        return Strings.hasText( text ) ? StringEscapeUtils.unescapeXml( text ) : "";
    }
    
	public static XmlReader read( String xml ) {
		return new SaxXmlReader( new InputSource( new StringReader( xml ) ) );
	}

	public static XmlReader read( InputStream stream ) {
		return new SaxXmlReader( new InputSource( stream ) );
	}

	public static XmlReader read( Reader reader ) {
		return new SaxXmlReader( new InputSource( reader ) );
	}

	public static XmlReader read( Element element ) {
		return new JdomXmlReader( element );
	}
	
	public static XmlReader read( Object object ) {
		if( object instanceof String ) {
			return read( (String)object );
		} else if( object instanceof Element ) {
			return read( (Element)object );
		} else if( object instanceof InputStream ) {
			return read( (InputStream)object );
		} else if( object instanceof Reader ) {
			return read( (Reader)object );
		} else {
			return new XStreamXmlReader( object );
		}
	}
    
	public static XmlPrinter print( Object object ) {
		return new XmlPrinter( read( object ) );
	}

	public static XmlFormatter format( Object object ) {
        String text = object instanceof String ? (String) object : read( object ).asString();
		return new XmlFormatter( text );
	}
    
}