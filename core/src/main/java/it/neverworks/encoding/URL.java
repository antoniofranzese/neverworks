package it.neverworks.encoding;

import java.net.URLEncoder;
import java.net.URLDecoder;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import it.neverworks.lang.Errors;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Arguments;

public class URL {

    public static String encode( Object value ) {
        return encode( value, "utf-8" );
    }

    public static String encode( Object value, String encoding ) {
        try {
            return value != null ? URLEncoder.encode( Strings.valueOf( value ), encoding ) : null; 
        } catch( Exception ex ) {
            throw Errors.wrap( ex  );
        }
    }

    public static String decode( Object value ) {
        return decode( value, "utf-8" );
    }    
    
    public static String decode( Object value, String encoding ) {
        try {
            return value != null ? URLDecoder.decode( Strings.valueOf( value ), encoding ) : null; 
        } catch( Exception ex ) {
            throw Errors.wrap( ex  );
        }
    }
    
    private final static Pattern RE = Pattern.compile( "^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?" );
    
    private String protocol;
    private String port;
    private String host;
    private String path;
    private String query;
    private Arguments args;
    
    public URL( String spec ) {
        Matcher matcher = RE.matcher( spec );
        if( matcher.matches() ) {
            if( matcher.groupCount() > 2 && Strings.hasText( matcher.group( 2 ) ) ) {
                this.protocol = matcher.group( 2 );
            }

            if( matcher.groupCount() > 4 && Strings.hasText( matcher.group( 4 ) ) ) {
                String host = matcher.group( 4 );
                int colon = host.indexOf( ":" );
                if( colon > 0 ) {
                    this.host = host.substring( 0, colon );
                    this.port = host.substring( colon + 1 );
                } else {
                    this.host = host;
                }
            }

            if( matcher.groupCount() > 5 && Strings.hasText( matcher.group( 5 ) ) ) {
                this.path = matcher.group( 5 );
            }

            if( matcher.groupCount() > 7 && Strings.hasText( matcher.group( 7 ) ) ) {
                this.query = matcher.group( 7 );
            }
            
        } else {
            throw new IllegalArgumentException( "The string does not appear to be an URL: " + spec );
        }
    }
    
    public String toString() {
        StringBuilder s = new StringBuilder();
        
        if( Strings.hasText( this.protocol ) && Strings.hasText( this.host ) ) {
            s.append( this.protocol ).append( "://" ).append( this.host );
            if( Strings.hasText( this.port ) ) {
                s.append( ":" ).append( this.port );
            }
        }
        
        if( Strings.hasText( this.path ) ) {
            s.append( this.path );
            String query = getQuery();
            
            if( Strings.hasText( query ) ) {
                s.append( "?" ).append( query );
            }
        }
    
        return s.toString();
    }
    
    public String getQuery(){
        if( this.args != null ) {
            Arguments encoded = new Arguments();
            for( String key: this.args.keys() ) {
                encoded.set( encode( key ), encode( this.args.get( key ) ) );
            }
            return encoded.format( '=', '&' );
        } else {
            return this.query;
        }
    }
    
    public void setQuery( String query ){
        this.query = query;
        this.args = null;
    }
    
    public Arguments getArguments() {
        if( this.args == null ) {
            Arguments encoded = Arguments.parse( this.query, '=', '&' );
            this.args = new Arguments();
            for( String key: encoded.keys() ) {
                this.args.set( decode( key ), decode( encoded.get( key ) ) );
            }
            for( Object obj: encoded.positionals() ) {
                this.args.set( decode( Strings.valueOf( obj ) ), null );
            }
            this.query = null;
        }
        return this.args;
    }

    public boolean has( String name ) {
        return getArguments().has( name );
    }
    
    public String get( String name ) {
        return getArguments().get( name, null );
    }
    
    public URL set( String name, String value ) {
        getArguments().set( name, value );
        return this;
    }
    
    public String getPath(){
        return this.path;
    }
    
    public void setPath( String path ){
        this.path = path;
    }
    
    public String getHost(){
        return this.host;
    }
    
    public void setHost( String host ){
        this.host = host;
    }
    
    public String getPort(){
        return this.port;
    }
    
    public void setPort( String port ){
        this.port = port;
    }
    
    public String getProtocol(){
        return this.protocol;
    }
    
    public void setProtocol( String protocol ){
        this.protocol = protocol;
    }

}

