package it.neverworks.lang;

import java.util.List;
import java.util.Collection;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.stream.Stream;

public interface ListLike<E> extends List<E> {

    default boolean add(E e) {
        throw new UnsupportedMethodException( this, "add" );
    }
    
    default void add(int index, E element) {
        throw new UnsupportedMethodException( this, "add" );
    }
    
    default boolean addAll(Collection<? extends E> c) {
        throw new UnsupportedMethodException( this, "addAll" );
    }
    
    default boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedMethodException( this, "addAll" );
    }
    
    default void clear() {
        throw new UnsupportedMethodException( this, "clear" );
    }
    
    default boolean contains(Object o) {
        throw new UnsupportedMethodException( this, "contains" );
    }
    
    default boolean containsAll(Collection<?> c) {
        throw new UnsupportedMethodException( this, "containsAll" );
    }
    
    default E get(int index) {
        throw new UnsupportedMethodException( this, "get" );
    }
    
    default int indexOf(Object o) {
        throw new UnsupportedMethodException( this, "indexOf" );
    }
    
    default boolean isEmpty() {
        throw new UnsupportedMethodException( this, "isEmpty" );
    }
    
    default Iterator<E> iterator() {
        throw new UnsupportedMethodException( this, "iterator" );
    }
    
    default int lastIndexOf(Object o) {
        throw new UnsupportedMethodException( this, "lastIndexOf" );
    }
    
    default ListIterator<E> listIterator() {
        throw new UnsupportedMethodException( this, "listIterator" );
    }
    
    default ListIterator<E> listIterator(int index) {
        throw new UnsupportedMethodException( this, "listIterator" );
    }
    
    default E remove(int index) {
        throw new UnsupportedMethodException( this, "remove" );
    }
    
    default boolean remove(Object o) {
        throw new UnsupportedMethodException( this, "remove" );
    }
    
    default boolean removeAll(Collection<?> c) {
        throw new UnsupportedMethodException( this, "removeAll" );
    }
    
    default boolean retainAll(Collection<?> c) {
        throw new UnsupportedMethodException( this, "retainAll" );
    }
    
    default E set(int index, E element) {
        throw new UnsupportedMethodException( this, "set" );
    }
    
    default int size() {
        throw new UnsupportedMethodException( this, "size" );
    }
    
    default List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedMethodException( this, "subList" );
    }
    
    default Object[] toArray() {
        throw new UnsupportedMethodException( this, "toArray" );
    }
    
    default <T> T[] toArray(T[] a) {
        throw new UnsupportedMethodException( this, "toArray" );
    }
    
    default Stream<E> stream() {
        throw new UnsupportedMethodException( this, "stream" );
    }
    
}
