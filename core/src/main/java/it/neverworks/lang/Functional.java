package it.neverworks.lang;

import java.util.List;
import java.util.Iterator;
import java.util.stream.Stream;

public interface Functional<T> extends Iterable<T>, Listable<T> {
    
    default <O> Functional<O> map( Mapper<T,O> mapper ) {
        if( mapper != null ) {
            FluentArrayList<O> result = new FluentArrayList<O>();
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                result.add( mapper.map( it.next() ) );
            }
            return result;
        } else {
            return (Functional<O>)this;
        }
    }
    
    default Functional<T> filter( Filter<T> filter ) {
        if( filter != null ) {
            FluentArrayList<T> result = new FluentArrayList<T>();
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                T item = it.next();
                if( filter.filter( item ) ){
                    result.add( item );
                }
            }
            return result;
        } else {
            return this;
        }
    }

    default Functional<T> inspect( Inspector<T> inspector ){
        if( inspector != null ) {
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                inspector.inspect( it.next() );
            }
        }
        return this;
    }
    
    default <I extends Inspector<T>> I reduce( I inspector ){
        if( inspector != null ) {
            inspect( inspector );
        }
        return inspector;
    }

    default <A> A reduce( A accumulator, Reducer<A,T> reducer ){
        if( reducer != null ) {
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                accumulator = reducer.reduce( accumulator, it.next() );
            }
        }
        return accumulator;
    }

    default boolean any( Filter filter ) {
        if( filter != null ) {
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                if( filter.filter( it.next() ) ) {
                    return true;
                }
            }
            return false;
        } else {
            return any();
        }
    }

    default boolean any() {
        return iterator().hasNext();
    }

    default boolean none() {
        return ! iterator().hasNext();
    }

    default boolean all( Filter filter ) {
        if( filter != null ) {
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                if( ! filter.filter( it.next() ) ) {
                    return false;
                }
            }
            return true;
        } else {
            return any();
        }
    }

    default Stream<T> stream() {
        return FluentList.stream( (Iterable<T>) this );
    }
    
    default FluentList<T> list() {
        return FluentList.list( (Iterable<T>) this );
    }

    default <K,V> FluentMap<K,V> dict() {
        return new FluentHashMap<K,V>().cat( (Iterable<T>) this );
    }

    default <K,V> FluentMap<K,V> dict( Mapper<T,Object> mapper ) {
        if( mapper != null ) {
            FluentMap<K,V> result = new FluentHashMap<K,V>();
            Iterator<T> it = iterator();
            while( it.hasNext() ) {
                result.add( mapper.map( it.next() ) );
            }
            return result;
        } else {
            return dict();
        }
    }

    default <O> O[] array( Class<O> target ) {
        return FluentList.array( (Iterable<T>) this, target );
    }

    default String join( String delimiter ) {
        return Collections.join( this.iterator(), delimiter );
    }

    default Functional<T> flatten( Filter<T> filter ) {
        return Collections.<T>flatten( filter, this );
    }

    default <O> Functional<O> flatten( Class<O> cls ) {
        return Collections.<O>flatten( cls , this );
    }


}