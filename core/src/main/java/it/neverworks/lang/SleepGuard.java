package it.neverworks.lang;

public class SleepGuard implements Guard<SleepGuard> {
    
    private Long timeout;

    public SleepGuard( Number timeout ) {
        this.timeout = Numbers.Long( timeout );
    }

    public SleepGuard enter() {
        try {
            Thread.sleep( this.timeout );
            return this;
        } catch( InterruptedException ex ) {
            throw new Break();
        }
    }

}