package it.neverworks.lang;

import java.util.HashMap;

public class FluentHashMap<K,V> extends HashMap<K,V> implements FluentMap<K,V>, Hatching {
    
    public Object hatch() {
        return new FluentHashMap();
    }
        
}