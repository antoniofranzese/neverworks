package it.neverworks.lang;

import java.util.Iterator;
import java.util.Enumeration;
import org.apache.commons.collections.iterators.EnumerationIterator;

public class EnumerationIterable<T> implements Iterable<T> {
    
    private Enumeration<T> enumeration;
    
    public EnumerationIterable( Enumeration<T> enumeration ) {
        this.enumeration = enumeration;
    }
    
    public Iterator<T> iterator() {
        return new EnumerationIterator( this.enumeration );
    }
}