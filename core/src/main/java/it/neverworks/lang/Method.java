package it.neverworks.lang;

import java.util.Arrays;

public class Method {
    
    private java.lang.reflect.Method actual;
    
    public Method( Class owner, String name, Class... types ) {
        java.lang.reflect.Method method = Reflection.findMethod( owner, name, types );
        if( method != null ) {
            this.actual = method;
        } else {
            throw new RuntimeException( "No " + name + " method found in " + owner.getName() + " with signature:" + Arrays.asList( types ) );
        }
    }
    
    public java.lang.reflect.Method getActual() {
        return this.actual;
    }
    
}