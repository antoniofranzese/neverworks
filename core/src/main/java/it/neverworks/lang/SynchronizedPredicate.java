package it.neverworks.lang;

import java.util.Map;
import java.util.function.Predicate;

public class SynchronizedPredicate<T> implements Guard<T> {
    
    private T target;
    private Predicate<T> predicate;
    
    public SynchronizedPredicate( T target, Predicate<T> predicate ) {
        this.target = target;
        this.predicate = predicate;
    }
    
    public void surround( Work<T> work ){
        if( this.predicate.test( this.target ) ) {
            synchronized( this.target ) {
                if( this.predicate.test( this.target ) ) {
                    try {
                        work.doWithinWork( this.target );
                    } catch( Break brk ) {
                        
                    } catch( Throwable err ) {
                        this.error( err );
                    }
                }
            }
        }
    }
    
}
