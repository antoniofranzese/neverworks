package it.neverworks.lang;

public interface Callable<T> {
    T call( Object... arguments );
}