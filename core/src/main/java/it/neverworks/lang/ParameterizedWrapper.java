package it.neverworks.lang;

public interface ParameterizedWrapper<T> {
    
    public Object asObjectFor( T parameter );
}