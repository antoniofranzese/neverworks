package it.neverworks.lang;

import java.util.Map;
import java.util.List;
import java.util.Iterator;
import java.util.AbstractMap;
import java.util.stream.Stream;

public interface FluentMap<K,V> extends Map<K,V> {
    
    default FluentList<K> keys() {
        return new FluentArrayList<K>( this.keySet() );
    }
    
    default FluentList<Map.Entry<K,V>> entries() {
        return keys().map( k -> (Map.Entry<K,V>) new AbstractMap.SimpleImmutableEntry<K,V>( k, get( k ) ) ).list();
    }

    default FluentList<V> assets() {
        return keys().map( k -> get( k ) ).list();
    }
    
    default FluentMap<K,V> add( K key, V value ) {
        if( key != null ) {
            this.put( key, value );
        }
        return this;
    }

    default FluentMap<K,V> add( Object item ) {
        
        if( item instanceof Map.Entry ) {
            Object key = ((Map.Entry) item).getKey();
            if( key != null ) {
                this.put( (K) key, (V)((Map.Entry) item).getValue() );
            }

        } else if( item instanceof Tuple ) {
            Object key = ((Tuple) item).get( 0 );
            if( key != null ) {
                this.put( (K) key, (V) ((Tuple) item).get( 1 ) );
            }

        } else if( item instanceof Iterable ) {
            Iterator iterator = ((Iterable) item).iterator();
            if( iterator.hasNext() ) {
                Object key = iterator.next();
                if( key != null ) {
                    this.put( (K) key, (V) ( iterator.hasNext() ? iterator.next() : null ) );
                }
            }

        } else if( item instanceof Map ) {
            Object key = ((Map) item).containsKey( "key" ) ? ((Map) item).get( "key" ) : null;
            if( key != null ) {
                this.put( (K) key, (V) ( ((Map) item).containsKey( "value" ) ? ((Map) item).get( "value" ) : null ) );
            }

        } else if( Collections.isListable( item ) ) {
            List list = Collections.list( item );
            Object key = list.size() > 0 ? list.get( 0 ) : null;
            if( key != null ) {
                this.put( (K) key, (V) ( list.size() > 1 ? list.get( 1 ) : null ) );
            }
        
        } else {
            throw new IllegalArgumentException( "Invalid item: " + Objects.repr( item ) );
        }
        
        return this;
    }

    default <N> FluentMap<K,N> map( Mapper<K,N> mapper ) {
        FluentMap<K,N> result = Objects.hatch( this );
        for( K key: this.keys() ) {
            result.put( key, mapper.map( key ) );
        }
        return result;
    }

    default FluentMap<K,V> filter( Filter<K> filter ) {
        if( filter != null ) {
            FluentMap<K,V> result = Objects.hatch( this );
            for( K key: this.keys() ) {
                if( filter.filter( key ) ) {
                    result.put( key, get( key ) );
                }
            }
            return result;
        } else {
            return this;
        }
    }

    default FluentMap<K,V> inspect( Inspector<K> inspector ) {
        if( inspector != null ) {
            for( K key: this.keys() ) {
                inspector.inspect( key );
            }
        } 
        return this;
    }

    default <I extends Inspector<K>> I reduce( I inspector ) {
        if( inspector != null ) {
            this.inspect( inspector );
        } 
        return inspector;
    }

    default FluentMap<K,V> set( K key, V value ) {
        this.put( key, value );
        return this;
    }
    
    default <X,Y> FluentMap<X,Y> as( Class<X> keyClass, Class<Y> valueClass ) {
        return (FluentMap<X,Y>)this;
    }
    
    default Iterator<K> iterator() {
        return this.keys().iterator();
    }
    
    default Stream<K> stream() {
        return this.keys().stream();
    }
    
    default Guard<FluentMap<K,V>> missing( K key ) {
        return new SynchronizedPredicate<FluentMap<K,V>>( this, t -> ! t.containsKey( key ) );
    }
    
    default V probe( K key, Wrapper initializer ) {
        if( initializer != null && ! this.containsKey( key ) ) {
            synchronized( this ) {
                if( ! this.containsKey( key ) ) {
                    V value = (V) initializer.asObject();
                    this.put( key, value );
                    return value;
                } else {
                    return this.get( key );
                }
            }
        } else {
            return this.get( key );
        }
    }
    
    default V probe( K key, ParameterizedWrapper<K> initializer ) {
        if( initializer != null && ! this.containsKey( key ) ) {
            synchronized( this ) {
                if( ! this.containsKey( key ) ) {
                    V value = (V) initializer.asObjectFor( key );
                    this.put( key, value );
                    return value;
                } else {
                    return this.get( key );
                }
            }
        } else {
            return this.get( key );
        }
    }
    
    default FluentMap<K,V> cat( Tuple... items ) {
        for( Tuple item: items ) {
            Object key = item.get( 0 );
            if( key != null ) {
                this.put( (K) key, (V) item.get( 1 ) );
            }
        }
        return this;
    }

    default FluentMap<K,V> cat( Iterable iterable ) {
        for( Object item: iterable ) {
            this.add( item );
        }
        return this;
    }
    
    default FluentMap<K,V> discard( K... keys ) {
        return discard( Collections.<K>iterable( keys ) );
    }
    
    default FluentMap<K,V> discard( Iterable<K> keys ) {
        for( K key: keys ) {
            if( this.containsKey( key ) ) {
                this.remove( key );
            }
        }
        return this;
    }

    default FluentMap<K,V> discard( Filter<K> filter ) {
        for( K key: this.keys() ) {
            if( filter.filter( key ) ) {
                this.remove( key );
            }
        }
        return this;
    }

    default FluentMap<K,V> retain( K... keys ) {
        return retain( Collections.<K>iterable( keys ) );
    }
    
    default FluentMap<K,V> retain( Iterable<K> keys ) {
        for( K key: this.keys().discard( keys ) ) {
            this.remove( key );
        }
        return this;
    }

    default FluentMap<K,V> retain( Filter<K> filter ) {
        for( K key: this.keys() ) {
            if( ! filter.filter( key ) ) {
                this.remove( key );
            }
        }
        return this;
    }

    
}