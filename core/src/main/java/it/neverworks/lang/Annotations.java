package it.neverworks.lang;

import java.lang.reflect.Type;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.annotation.Annotation;

import it.neverworks.lang.annotation.AnnotatedClass;
import it.neverworks.lang.annotation.AnnotatedMethod;
import it.neverworks.lang.annotation.AnnotationManager;

public class Annotations {

    // Type

    public static FluentList<Annotation> all( Type target ) {
        AnnotatedClass annotated = AnnotationManager.getAnnotatedClass( Reflection.getRawClass( target ) );
        return new FluentArrayList<Annotation>( annotated.getAllAnnotations() );
    }

    public static <T extends Annotation> T get( Type target, Class<T> annotationType ) {
        AnnotatedClass annotated = AnnotationManager.getAnnotatedClass( Reflection.getRawClass( target ) );
        return (T)annotated.getAnnotation( annotationType );
    }

    public static boolean has( Type target, Class<? extends Annotation> annotationType ) {
        return get( target, annotationType ) != null;
    }
    
    // Method

    public static FluentList<Annotation> all( Method method ) {
        return new FluentArrayList<Annotation>( method.getDeclaredAnnotations() );
    }

    public static <T extends Annotation> T get( Method method, Class<T> annotationType ) {
        return (T)method.getAnnotation( annotationType );
    }

    public static boolean has( Method method, Class<? extends Annotation> annotationType ) {
        return get( method, annotationType ) != null;
    }
    
    // Field

    public static FluentList<Annotation> all( Field field ) {
        return new FluentArrayList<Annotation>( field.getDeclaredAnnotations() );
    }

    public static <T extends Annotation> T get( Field field, Class<T> annotationType ) {
        return (T)field.getAnnotation( annotationType );
    }

    public static boolean has( Field field, Class<? extends Annotation> annotationType ) {
        return get( field, annotationType ) != null;
    }

    // Package

    public static FluentList<Annotation> all( Package pack ) {
        return new FluentArrayList<Annotation>( pack.getAnnotations() );
    }

    public static <T extends Annotation> T get( Package pack, Class<T> annotationType ) {
        return (T)pack.getAnnotation( annotationType );
    }

    public static boolean has( Package pack, Class<? extends Annotation> annotationType ) {
        return get( pack, annotationType ) != null;
    }

}