package it.neverworks.lang;

public interface Filter<I> {
    boolean filter( I value );
}