package it.neverworks.lang;

import java.util.Map;
import java.util.Set;
import java.util.Collection;

public class FluentMapAdapter<K,V> implements FluentMap<K,V>, Hatching {

    private Map source;

    public FluentMapAdapter( Map map ) {
        this.source = map;
    }

    public V get( Object key ) {
        return (V) this.source.get( key );
    }
    
    public void clear() {
        this.source.clear();
    }

    public Set<Map.Entry<K,V>> entrySet() {
        return (Set<Map.Entry<K,V>>) this.source.entrySet();
    }
    
    public boolean isEmpty() {
        return this.source.isEmpty();
    }

    public Set<K> keySet() {
        return (Set<K>) this.source.keySet();
    }

    public void putAll( Map<? extends K,? extends V> m )  {
        if( m != null ) {
            for( K key: m.keySet() ) {
                this.put( key, m.get( key ) );
            }
        }
    }

    public V remove( Object key ) {
        return (V) this.source.remove( key );
    }

    public int size() {
        return this.source.size();
    }

    public Collection<V> values() {
        return (Collection<V>) this.source.values();
    }

    public boolean containsKey( Object key ) {
        return this.source.containsKey( key );
    }

    public boolean containsValue( Object value ) {
        return this.source.containsValue( value );
    }

    public V put( K key, V value ) {
        return (V) this.source.put( key, value );
    }
    
    public Object hatch() {
        return new FluentMapAdapter( Reflection.newInstance( this.source.getClass() ) );
    }

}