package it.neverworks.lang;

public class Undefined {
    
    public final static Undefined INSTANCE = new Undefined();
    
    public String toString() {
        return "UNDEFINED";
    }
    
    public boolean equals( Object other ) {
        return other != null && other instanceof Undefined;
    }

    public static boolean matches( Object obj ) {
        return INSTANCE.equals( obj );
    }
    
    public static Undefined value() {
        return INSTANCE;
    }

}