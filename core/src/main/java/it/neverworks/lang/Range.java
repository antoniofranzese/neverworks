package it.neverworks.lang;

import java.util.Iterator;
import java.util.List;
import java.lang.reflect.Array;

public class Range implements Functional<Integer> {

    public static Range of( Object value ) {
        if( value instanceof Wrapper ) {
            return of( ((Wrapper) value).asObject() );
            
        } else if( value instanceof Number ) {
            return new Range( ((Number) value).intValue() );

        } else if( Numbers.isNumber( value ) ) {
            return new Range( Numbers.Int( value ) );

        } else if( value instanceof List ) {
            return new Range( ((List) value).size() );

        } else if( Collections.isArray( value ) ) {
            return new Range( Array.getLength( value ) );

        } else if( value instanceof String ) {
            return new Range( ((String) value).length() );

        } else if( Collections.isListable( value ) ) {
            return new Range( Collections.list( value ).size() );

        } else if( value == null ) {
            return new Range( 0 );

        } else {
            throw new IllegalArgumentException( "Cannot detect range on: " + Objects.repr( value ) );
        }
        
    }

    public static Range of( Object start, Object stop ) {
        return new Range( Numbers.safeNumber( start ).intValue(), Numbers.safeNumber( stop ).intValue() );
    }
    
    public class RangeIterator implements Iterator<Integer> {
        private int value;
        private int max;
        private int step;
        
        public RangeIterator( int value, int max, int step ) {
            this.value = value;
            this.max = max;
            this.step = step;
        }
        
        public boolean hasNext() {
            return step > 0 ? value < max : value > max;
        }
        
        public Integer next() {
            int result = value;
            value += step;
            return result;
        }
        
        public void remove() {
            throw new RuntimeException( "Range is not mutable" );
        }
    }
    
    private int start;
    private int stop;
    private int step;
    private Iterator<Integer> ownIterator;
    
    protected Range() {
        this( 0, Integer.MAX_VALUE, 1 );
    }

    protected Range( int stop ) {
        this( 0, stop, 1 );
    }

    protected Range( int start, int stop ){
        this( start, stop, 1 );
    }
    
    protected Range( int start, int stop, int step ){
        this.start = start;
        this.stop = stop;
        this.step = step;
    }
    
    public Range step( int step ) {
        this.step = step;
        return this;
    }

    public Range reverse() {
        return new Range( this.stop, this.start, this.step * -1 ).dec( this.step );
    }
    
    public Range inc() {
        return inc( 1 );
    }

    public Range dec() {
        return inc( -1 );
    }
    
    public Range inc( int amount ) {
        this.start += amount;
        this.stop += amount;
        this.ownIterator = null;
        return this;
    }

    public Range dec( int amount ) {
        return inc( amount * -1 );
    }
    
    public Range from( int start ) {
        this.start = start;
        this.ownIterator = null;
        return this;
    }

    public Range to( int stop ) {
        this.stop = stop;
        this.ownIterator = null;
        return this;
    }
    
    public Iterator<Integer> iterator() {
        return new RangeIterator( this.start, this.stop, this.step );
    }
    
    protected Iterator<Integer> ownIterator() {
        if( this.ownIterator == null ) {
            this.ownIterator = this.iterator();
        }
        return this.ownIterator;
    }
    
    public int next() {
        return this.ownIterator().next();
    }
    
    public boolean hasNext() {
        return this.ownIterator().hasNext();
    }

}


