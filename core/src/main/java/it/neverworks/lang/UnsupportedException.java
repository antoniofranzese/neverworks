package it.neverworks.lang;

public class UnsupportedException extends UnsupportedOperationException {
    
    public UnsupportedException( String message ) {
        super( message );
    }

    public UnsupportedException() {
        super( "Feature is not supported" );
    }

}