package it.neverworks.lang;

import java.io.DataInputStream;

public class Java {
    
    public final static int V8 = 52;
    public final static int V7 = 51;
    public final static int V6 = 50;
    public final static int V5 = 49;
    public final static int V4 = 48;
    public final static int V3 = 47;
    public final static int V2 = 46;
    public final static int V1 = 45;

    private final static int CLASS_MAGIC = 0xCAFEBABE;
    
    private static int classMajor = -1;
    private static int classMinor = -1;
    
    static {
        try {
            DataInputStream dis = new DataInputStream(  Thread.currentThread().getContextClassLoader().getResourceAsStream( "/java/lang/Runtime.class" ) );
            int magic = dis.readInt();
            if( magic == CLASS_MAGIC ) {
                classMinor= dis.readUnsignedShort();
                classMajor = dis.readUnsignedShort();
            }
        } catch( Exception ex ) {
            //TODO: log
        }
    }

    public static int getClassMinor(){
        return classMinor;
    }
    
    public static int getClassMajor(){
        return classMajor;
    }
    
    public static Class<?> getPrimitiveType( Class<?> type ) {
        if( type != null ) {
            if( type.equals( Boolean.class ) ) {
                return boolean.class;
            } else if( type.equals( Character.class ) ) {
                return char.class;
            } else if( type.equals( Byte.class ) ) {
                return byte.class;
            } else if( type.equals( Short.class ) ) {
                return short.class;
            } else if( type.equals( Integer.class ) ) {
                return int.class;
            } else if( type.equals( Long.class ) ) {
                return long.class;
            } else if( type.equals( Float.class ) ) {
                return float.class;
            } else if( type.equals( Double.class ) ) {
                return double.class;
            } else if( type.equals( Void.class ) ) {
                return void.class;
            } else {
                return type;
            }
        } else {
            return type;
        }
    }

    public static Class<?> getReferenceType( Class<?> type ) {
        if( type != null && type.isPrimitive() ) {
            if( type.equals( boolean.class ) ) {
                return Boolean.class;
            } else if( type.equals( char.class ) ) {
                return Character.class;
            } else if( type.equals( byte.class ) ) {
                return Byte.class;
            } else if( type.equals( short.class ) ) {
                return Short.class;
            } else if( type.equals( int.class ) ) {
                return Integer.class;
            } else if( type.equals( long.class ) ) {
                return Long.class;
            } else if( type.equals( float.class ) ) {
                return Float.class;
            } else if( type.equals( double.class ) ) {
                return Double.class;
            } else if( type.equals( void.class ) ) {
                return Void.class;
            } else {
                return type;
            }
        } else {
            return type;
        }
    }
}
