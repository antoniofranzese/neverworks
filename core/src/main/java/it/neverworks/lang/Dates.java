package it.neverworks.lang;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.DateTimeZone;
import org.joda.time.DateTime;
import org.joda.time.Seconds;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

public class Dates {
    private final static Pattern italianDatePattern = Pattern.compile( "\\d{1,2}/\\d{1,2}/\\d{4}" );
    private final static Pattern italianDateTimePattern = Pattern.compile( "\\d{1,2}/\\d{1,2}/\\d{1,4}\\s\\d{1,2}:\\d{1,2}:\\d{1,2}" );
    private final static Pattern englishDatePattern = Pattern.compile( "\\d{4}-\\d{1,2}-\\d{1,2}" );
    private final static Pattern isoPattern = Pattern.compile( "\\d{4}-\\d{2}-\\d{2}T.*" );
    private final static Pattern isoDateTimePattern = Pattern.compile( "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}" );
    private final static Pattern isoDateTimeWithZonePattern = Pattern.compile( "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.+" );
    private final static Pattern isoDateTimeWithMillisPattern = Pattern.compile( "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}.+" );
    private final static Pattern isoTimePattern = Pattern.compile( "T?\\d{2}:\\d{2}:\\d{2}" );
    private final static Pattern shorterTimePattern = Pattern.compile( "\\d{2}:\\d{2}" );

    private final static DateTimeFormatter italianDate = new DateTimeFormatterBuilder()
        .appendDayOfMonth( 1 )
        .appendLiteral( '/' )
        .appendMonthOfYear( 1 )
        .appendLiteral( '/' )
        .appendYear( 4, 4 )
        .toFormatter();

    private final static DateTimeFormatter italianDateTime = new DateTimeFormatterBuilder()
        .appendDayOfMonth( 1 )
        .appendLiteral( '/' )
        .appendMonthOfYear( 1 )
        .appendLiteral( '/' )
        .appendYear( 4, 4 )
        .appendLiteral( ' ' )
        .appendClockhourOfDay( 1 )
        .appendLiteral( ':' )
        .appendMinuteOfHour( 1 )
        .appendLiteral( ':' )
        .appendSecondOfMinute( 1 )
        .toFormatter();

    private final static DateTimeFormatter englishDate = new DateTimeFormatterBuilder()
        .appendYear( 4, 4 )
        .appendLiteral( '-' )
        .appendMonthOfYear( 1 )
        .appendLiteral( '-' )
        .appendDayOfMonth( 1 )
        .toFormatter();

    private final static DateTimeFormatter shorterTime = new DateTimeFormatterBuilder()
        .appendClockhourOfDay( 1 )
        .appendLiteral( ':' )
        .appendMinuteOfHour( 1 )
        .toFormatter();

    private final static DateTimeFormatter iso = ISODateTimeFormat.dateTimeNoMillis();
    private final static DateTimeFormatter isoDateTimeWithZone = ISODateTimeFormat.dateTimeNoMillis();
    private final static DateTimeFormatter isoDateTimeWithMillis = ISODateTimeFormat.dateTime();
    private final static DateTimeFormatter isoDateTime = ISODateTimeFormat.dateHourMinuteSecond();
    private final static DateTimeFormatter isoDate = ISODateTimeFormat.date();

    private static SimpleDateFormat httpDate;

    static {
        httpDate = new SimpleDateFormat( "EEE, dd MMM yyyy HH:mm:ss z", Locale.US );
        httpDate.setTimeZone( TimeZone.getTimeZone( "GMT" ) );
    }

    public static boolean isDate( Object date ) {
        if( date != null ) {
            try {
                date( date );
                return true;
            } catch( IllegalArgumentException ex ) {
                return false;
            }
        } else {
            return false;
        }
    }

    public static String toISO( Object date ) {
        return toISO( date( date ) );
    }
    
    public static String toISO( Date date ) {
        return date != null ? iso.withZone( DateTimeZone.UTC ).print( date.getTime() ) : null;
    }

    public static String toISOWithMillis( Object date ) {
        return toISOWithMillis( date( date ) );
    }
    
    public static String toISOWithMillis( Date date ) {
        return date != null ? isoDateTimeWithMillis.withZone( DateTimeZone.UTC ).print( date.getTime() ) : null;
    }

    public static String toLocalISO( Object date ) {
        return toLocalISO( date( date ) );
    }
    
    public static String toLocalISO( Date date ) {
        return date != null ? iso.print( date.getTime() ) : null;
    }

    public static String toLocalISOWithMillis( Object date ) {
        return toLocalISOWithMillis( date( date ) );
    }

    public static String toLocalISOWithMillis( Date date ) {
        return date != null ? isoDateTimeWithMillis.print( date.getTime() ) : null;
    }
    
    public static Date fromISO( String date ) {
        if( Strings.hasText( date ) ) {
            if( isoDateTimeWithMillisPattern.matcher( date ).matches() ) {
                return isoDateTimeWithMillis.parseDateTime( date ).toDate();    

            } else if( isoDateTimeWithZonePattern.matcher( date ).matches() ) {
                return isoDateTimeWithZone.parseDateTime( date ).toDate();    

            } else if( isoDateTimePattern.matcher( date ).matches() ) {
                return isoDateTime.parseDateTime( date ).toDate();    

            } else {
                return isoDate.parseDateTime( date ).toDate();    
            }
        } else {
            return null;
        } 
    }

    public static String toHTTP( Object date ) {
        return date != null ? httpDate.format( toDate( date ) ) : null;
    }

    public static int secondsBetween( Object startDate, Object endDate ) {
        DateTime startDateTime = DateTime( startDate );
        DateTime endDateTime = DateTime( endDate );
        return Seconds.secondsBetween( endDateTime, startDateTime ).getSeconds();
    }
    
    public static Date mergeDateAndTime( Object date, Object time ) {
        if( date != null ) {
            DateTime dateValue = DateTime( date );
            DateTime timeValue = ( time != null ? DateTime( time ) : DateTime( beginning( dateValue ) ) ) ;
        
            DateTime result = dateValue
                .withHourOfDay( timeValue.getHourOfDay() )
                .withMinuteOfHour( timeValue.getMinuteOfHour() )
                .withSecondOfMinute( timeValue.getSecondOfMinute() );

            return result.toDate();
        } else {
            return null;
        }
    }
    
    public static Date beginning( Object date ) {
        if( date != null ) {
            return DateTime( date )
                .withHourOfDay( 0 )
                .withMinuteOfHour( 0 )
                .withSecondOfMinute( 0 )
                .withMillisOfSecond( 0 )
                .toDate();
        } else {
            return null;
        }
    }

    public static Date ending( Object date ) {
        if( date != null ) {
            return DateTime( date )
                .withHourOfDay( 23 )
                .withMinuteOfHour( 59 )
                .withSecondOfMinute( 59 )
                .withMillisOfSecond( 999 )
                .toDate();
        } else {
            return null;
        }
    }
    
    public static Date toDate( Object date ) {
        return date( date );
    }
    
    
    private static DateTime parseDateTime( String date ) {
        DateTimeFormatter formatter = null;

        if( isoDateTimeWithMillisPattern.matcher( date ).matches() ) {
            formatter = isoDateTimeWithMillis;
    
        } else if( isoDateTimePattern.matcher( date ).matches() ) {
            formatter = isoDateTime;

        } else if( isoPattern.matcher( date ).matches() ) {
            formatter = iso;

        } else if( italianDatePattern.matcher( date ).matches() ) {
            formatter = italianDate;

        } else if( englishDatePattern.matcher( date ).matches() ) {
            formatter = englishDate; 

        } else if( isoTimePattern.matcher( date ).matches() ) {
            date = isoDate.print( new DateTime() ) + ( date.startsWith( "T" ) ? date : ( "T" + date ) ) ;
            formatter = isoDateTime; 
        
        } else if( shorterTimePattern.matcher( date ).matches() ) {
            formatter = shorterTime;

        } else if( italianDateTimePattern.matcher( date ).matches() ) {
            formatter = italianDateTime;
        } 

        if( formatter != null ) {
            return formatter.parseDateTime( date ); 
        } else if( Numbers.isNumber( date ) ){
            return new DateTime( Numbers.number( date ).longValue() );
        } else {
            throw new IllegalArgumentException( "Invalid date format: " + date );
        }
        
    }
    
    public static Date now() {
        return new Date();
    }

    public static Date Date() {
        return new Date();
    }
    
    public static Date date( Object date ) {
        return Date( date );
    }
    
    public static Date Date( Object date ) {
        if( date != null ) {
            if( date instanceof Date ) {
                return (Date) date;

            } else if( date instanceof String ) {
                if( Strings.hasText( date ) ) {
                    return parseDateTime( (String) date ).toDate();
                } else {
                    return null;
                }
                
            } else if( date instanceof DateTime ) {
                return ((DateTime) date).toDate();

            } else if( date instanceof Number ) {
                return new Date( ((Number) date).longValue() );

            } else if( date instanceof Wrapper ) {
                return date( ((Wrapper) date).asObject() );

            } else {
                throw new IllegalArgumentException( "Invalid date: " + Objects.repr( date ) );
            }
        } else {
            return null;
        }
    }
    
    public static DateTime toDateTime( Object date ) {
        return DateTime( date );
    }

    public static DateTime DateTime() {
        return new DateTime();
    }
    
    public static DateTime datetime() {
        return new DateTime();
    }
    
    public static DateTime datetime( Object date ) {
        return DateTime( date );
    }

    public static DateTime DateTime( Object date ) {
        if( date != null ) {
            if( date instanceof DateTime ) {
                return (DateTime) date;

            } else if( date instanceof Date ) {
                return new DateTime( ((Date) date).getTime() );

            } else if( date instanceof String ) {
                if( Strings.hasText( date ) ) {
                    return parseDateTime( (String) date );
                } else {
                    return null;
                }
            } else if( date instanceof Number ) {
                return new DateTime( ((Number) date).longValue() );

            } else if( date instanceof Wrapper ) {
                return DateTime( ((Wrapper) date).asObject() );
            } else {
                throw new IllegalArgumentException( "Invalid DateTime: " + Objects.repr( date ) );
            }
        } else {
            return null;
        }
    }
    
    public static int offsetMillis() {
        return offsetMillis( new Date() );
    }

    public static int offsetMillis( Object date ) {
        return TimeZone.getDefault().getOffset( date( date ).getTime() );
    }

    public static int offsetHours() {
        return offsetMillis() / 3600000;
    }

    public static int offsetHours( Object date ) {
        return offsetMillis( date ) / 3600000;
    }

}