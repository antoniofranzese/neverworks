package it.neverworks.lang;

public interface FluentMapLike<K,V> extends MapLike<K,V>, FluentMap<K,V> {
    
}