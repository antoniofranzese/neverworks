package it.neverworks.lang;

public interface Representable {
    String toRepresentation();
}