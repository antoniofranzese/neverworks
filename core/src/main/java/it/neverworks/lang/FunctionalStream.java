package it.neverworks.lang;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;
import java.lang.reflect.Array;

public class FunctionalStream<T> implements Functional<T> {
    
    protected Stream<T> stream;
    
    public FunctionalStream( Stream<T> stream ) {
        this.stream = stream;
    }
    
    public Iterator<T> iterator() {
        return this.stream.iterator();
    }
    
    public FluentList<T> list() {
        return FluentList.list( this.iterator() );
    }
    
    public <O> O[] array( Class<O> target ) {
        return this.stream.toArray( i -> (O[]) Array.newInstance( target, i ) );
    }
    
    public Stream<T> stream() {
        return this.stream;
    }

}