package it.neverworks.lang;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.util.Enumeration;

import java.net.URL;
import java.net.URLClassLoader;

import java.io.File;
import java.io.Closeable;
import java.io.IOException;
import java.util.jar.JarFile;
import java.util.jar.JarEntry;
import java.util.regex.Pattern;

import org.springframework.util.AntPathMatcher;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import io.github.classgraph.ClassInfoList;
import io.github.classgraph.ClassInfo;

public class ClassScanner implements Guard<ClassScanner>, Closeable {
    
    private ClassGraph graph;
    private ScanResult scan;

    public ClassScanner( String... packages ) {
        this.graph = new ClassGraph()
            .enableAllInfo()
            .whitelistPackages( packages );
    }

    public ClassScanner( List<String> packages ) {
        this( Collections.array( String.class, packages ) );
    }

    protected ScanResult scan() {
        if( this.scan == null ) {
            this.scan = this.graph.scan();
        }
        return this.scan;
    }

    protected FluentList<Class> load( ClassInfoList infoList ) {
        FluentList<Class> result = new FluentArrayList<>( infoList.size() );
        for( ClassInfo ci: infoList ) {
            result.add( ci.loadClass() );
        }
        return result;
    }

    public FluentList<Class> allClasses() {
        return load( scan().getAllClasses() );
    }

    public FluentList<Class> annotatedWith( Class annotationClass ) {
        return load( scan().getClassesWithAnnotation( annotationClass.getName() ) );
    }

    public FluentList<Class> implementing( Class superClass ) {
        return load( scan().getClassesImplementing( superClass.getName() ) );
    }

    public List<Class> extending( Class superClass ) {
        return load( scan().getSubclasses( superClass.getName() ) );
    }

    public Throwable exit( Throwable error ) {
        close();
        return error;
    }

    public void close() {
        if( this.scan != null ) {
            this.scan.close();
            this.scan = null;
        }
    }

}