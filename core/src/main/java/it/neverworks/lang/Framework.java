package it.neverworks.lang;

import java.util.List;
import java.io.InputStream;

public class Framework {
    
    public final static Framework INSTANCE = new Framework();
    private final static Settings settings;
    private final static List<String> packages;
    private static boolean booted = false;

    static {
        InputStream properties = Thread.currentThread().getContextClassLoader().getResourceAsStream( "META-INF/neverworks/framework.properties" );
        if( properties == null ) {
            throw new RuntimeException( "Missing framework configuration" );
        }
        settings = new Settings( "neverworks." ).load( properties );

        packages = Collections.each( Collections.list(( 
            getProperty( "packages", "" ) 
            + "," 
            + getProperty( "package", "" ) 
        ).split( "," )))
        .filter( s -> Strings.hasText( s ) )
        .list();

    }
    
    public static void boot() {
        if( ! booted ) {
            synchronized( Framework.class ) {
                if( ! booted ) {
                    booted = true;
                    bootFramework();
                }
            }
        }
    }

    private static void bootFramework() {
        System.out.println( "NeverWorks3 Bootstrap: Framework/" + getRevision() );
    }

    //TODO: eliminare
    public static String getRevision() {
        return property( "revision", "unknown" );
    }
    
    public static String getProperty( String name ) {
        return settings.readProperty( name, null );
    }

    public static String getProperty( String name, String defaultValue ) {
        return settings.readProperty( name, defaultValue );
    }
    
    public static void setProperty( String name, String value ) {
        settings.writeProperty( name, value );
    }

    public static String property( String name ) {
        return settings.readProperty( name, null );
    }

    public static String property( String name, String defaultValue ) {
        return settings.readProperty( name, defaultValue );
    }

    public static boolean feature( String feature ) {
        return settings.checkFeature( feature, false );
    }

    public static boolean feature( String feature, boolean defaultValue ) {
        return settings.checkFeature( feature, defaultValue );
    }

    public static ClassScanner scan() {
        return new ClassScanner( packages );
    }

}