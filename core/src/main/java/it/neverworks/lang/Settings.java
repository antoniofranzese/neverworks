package it.neverworks.lang;

import java.io.InputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;

public class Settings {
    
    protected Properties properties = new Properties();
    protected String systemPrefix = "";

    public Settings() {
        super();
    }
    
    public Settings( String prefix ) {
        this();
        this.systemPrefix = prefix;
    }

    public void setSystemPrefix( String systemPrefix ){
        this.systemPrefix = systemPrefix;
    }
    
    public Settings load( String path ) {
        return load( Thread.currentThread().getContextClassLoader().getResourceAsStream( path ) );
    }
    
    public Settings load( InputStream stream ) {
        if( stream != null ) {
            try {
                this.properties.load( stream );
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }
        }
        return this;
    }
    
    public String readProperty( String name, String defaultValue ) {
        if( properties.containsKey( name ) ) {
            return properties.getProperty( name );
        } else if( System.getProperty( this.systemPrefix + name ) != null ) {
            return System.getProperty( this.systemPrefix + name );
        } else {
            return defaultValue;
        }
    }
    
    public void writeProperty( String name, String value ) {
        properties.setProperty( name, value );
    }
    
    protected List<String> allFeatures( String feature ) {
        List<String> result = new ArrayList<String>();
        result.add( feature );
        
        String current = feature;
        while( current.indexOf( "." ) > 0 ) {
            current = current.substring( 0, current.lastIndexOf( "." ) );
            result.add( current + ".*" );
        } 
        result.add( "*" );
        return result;
    }
    
    public Boolean checkFeature( String feature, Boolean defaultValue ) {
        for( String property: allFeatures( feature ) ) {
            String value = readProperty( property, null );
            if( value != null ) {
                try {
                    return Booleans.isTrue( value );
                } catch( Exception ex ) {
                    return false;
                }
            }
        }
        return defaultValue;
    }
    
    public Collection<String> names() {
        return Collections.<String>list( properties.propertyNames() );
    }
}