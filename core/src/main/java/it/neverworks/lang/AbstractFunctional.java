package it.neverworks.lang;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.lang.reflect.Array;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class AbstractFunctional<T> implements Functional<T> {
    
    protected abstract Iterable<T> iterable();

    public Iterator<T> iterator() {
        return iterable().iterator();
    }
    
    public FluentList<T> list() {
        return FluentList.list( iterable() );
    }
    
    public <O> O[] array( Class<O> target ) {
        return FluentList.array( iterable(), target );
    }
    
    public Stream<T> stream() {
        return FluentList.stream( iterable() );
    }
    
}