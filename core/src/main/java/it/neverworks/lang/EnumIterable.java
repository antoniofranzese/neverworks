package it.neverworks.lang;

import java.util.Iterator;

public class EnumIterable<T> implements Iterable<T> {
    
    private Class<T> enumClass;
    
    public EnumIterable( Class<T> enumClass ) {
        if( enumClass != null && enumClass.isEnum() ) {
            this.enumClass = enumClass;
        } else {
            throw new IllegalArgumentException( "Invalid enum: " + enumClass );
        }
    }
    
    public Iterator<T> iterator() {
        return new EnumIterator<T>( this.enumClass );
    }
    
    private class EnumIterator<T> implements Iterator<T> {
        
        private T[] items;
        private int index = 0;
        
        public EnumIterator( Class<T> enumClass ) {
            this.items = enumClass.getEnumConstants();
        }
        
        public boolean hasNext() {
            return this.index < this.items.length;
        }
        
        public T next() {
            T value = this.items[ this.index ];
            this.index++;
            return value;
        }
        
        public void remove() {
            
        }
    }
}