package it.neverworks.lang;

import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.stream.Stream;
import java.lang.reflect.Array;

public class FluentArrayList<T> extends ArrayList<T> implements FluentList<T>, Hatching {
    
    public FluentArrayList() {
        super();
    }
    
    public FluentArrayList( int size ) {
        super( size );
    }

    public FluentArrayList( Collection<? extends T> collection ) {
        super( collection );
    }

    public FluentArrayList( Iterable<? extends T> iterable ) {
        super();
        cat( iterable );
    } 

    public FluentArrayList( T[] array ) {
        super( array.length );
        cat( array );
    } 

    public FluentArrayList( Enumeration<T> enumeration ) {
        super();
        cat( enumeration );
    } 

    public Stream<T> stream() {
        return super.stream();
    }
    
    public Object hatch() {
        return new FluentArrayList();
    }
    
}