package it.neverworks.lang;

public class UnsupportedFeatureException extends UnsupportedException {
    
    public UnsupportedFeatureException( String message ) {
        super( message );
    }

    public UnsupportedFeatureException() {
        super( "Feature is not supported" );
    }

}