package it.neverworks.lang;

public class Booleans {

    public static boolean Bool( Object value ) {
        return bool( value );
    }
    
    public static Boolean NBool( Object value ) {
        return value != null ? bool( value ) : null;
    }

    public static boolean Boolean( Object value ) {
        return bool( value );
    }
    
    public static Boolean NBoolean( Object value ) {
        return value != null ? bool( value ) : null;
    }
    
    public static boolean bool( Object value ) {
        if( value instanceof Boolean ) {
            return (Boolean)value;

        } else if( value instanceof String ) {
            String s = ((String) value).toLowerCase().trim();
            if( s.equals( "true" ) 
                || s.equals( "yes" ) 
                || s.equals( "on" ) 
                || s.equals( "y" ) 
                || s.equals( "1" ) ) {
                
                return Boolean.TRUE;
            } else if( s.equals( "false" ) 
                || s.equals( "no" ) 
                || s.equals( "off" )
                || s.equals( "n" ) 
                || s.equals( "0" ) ) {
                    
                return Boolean.FALSE;
            } else {
                throw invalid( value );
            }

        } else if( value instanceof Number ) {
            return ((Number) value).intValue() != 0;

        } else {
            throw invalid( value );
        }
        
    }
    
    
    public static boolean isTrue( Object value ) {
        return bool( value );
    }
    
    public static boolean isFalse( Object value ) {
        return ! bool( value );
    }
    
    public static boolean isBoolean( Object value ) {
        if( value != null ) {
            try {
                bool( value );
                return true;
            } catch( IllegalArgumentException ex ) {
                return false;
            }
        } else {
            return false;
        }
    }
    
    protected static RuntimeException invalid( Object value ) {
        throw new IllegalArgumentException( "Invalid boolean: " + Objects.repr( value ) );
    }
}