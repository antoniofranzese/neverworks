package it.neverworks.lang;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.BufferedReader;
import java.util.Properties;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import it.neverworks.encoding.JSON;

public class MultiFormatProperties extends Properties {
    
    private static enum Format { PROPS, JSON };
    
    private Format format;
    
    public void load( InputStream stream ) {
        load( new InputStreamReader( stream ) );
    }
    
    public void load( Reader reader ) {
        if( !( reader instanceof BufferedReader ) ) {
            reader = new BufferedReader( reader );
        }

        String line = null;
        List<String> lines = new ArrayList<String>();
        
        do {

            try {
                line = ((BufferedReader) reader).readLine();
            } catch( IOException ex ) {
                throw Errors.wrap( ex );
            }

            if( line != null ) {
                line = line.trim();
                if( Strings.hasText( line ) ) {
                    if( line.startsWith( "#" ) ) {
                        if( line.startsWith( "#!json" ) ) {
                            this.format = Format.JSON;
                        } 
                    } else {
                        lines.add( line );
                    } 
                } 
            }

        } while( line != null );
        
        if( lines.size() > 0 ) {
            if( format == Format.JSON || lines.get( 0 ).startsWith( "{" ) ) {
                loadJson( lines );
            } else {
                loadProperties( lines );
            }
        }
        
    }
    
    protected void loadJson( List<String> lines ) {
        Object json = JSON.relaxed.decode( Collections.join( lines, " " ) );
        if( json instanceof Map ) {
            for( Object key: ((Map) json).keySet() ) {
                this.put( Strings.valueOf( key ), ((Map) json).get( key ) );
            }
        } else {
            throw new UnsupportedFeatureException( "Unsupported decoded JSON type: " + Objects.repr( json ) );
        }
    }
    
    protected void loadProperties( List<String> lines ) {
        for( String line: lines ) {
            int index = line.indexOf( "=" );
            if( index > 0 ) {
                this.put( line.substring( 0, index ).trim(), line.substring( index + 1 ).trim() );
            }
        }
    }
    
}