package it.neverworks.lang;

import java.util.List;
import java.util.Iterator;
import org.apache.commons.collections.iterators.ObjectArrayIterator;

public class Tuple implements Functional {
    
    protected Object[] values;
    
    public static Tuple of( Object... values ) {
        return new Tuple( values );
    }

    public static Tuple of( Iterable values ) {
        return new Tuple( Collections.array( values ) );
    }

    protected Tuple( Object[] values ) {
        this.values = values != null ? values : new Object[]{};
    }
    
    public <T> T get( int index ) {
        return get( index, null );
    }
    
    public <T> T get( int index, T defaultValue ) {
        if( index < this.values.length ) {
            return (T) ( this.values[ index ] != null ? this.values[ index ] : defaultValue );
        } else {
            return defaultValue;
        }
    }
    
    public boolean has( int index ) {
        return index < this.values.length - 1;
    }

    public boolean contains( int index ) {
        return has( index );
    }
    
    public int size() {
        return this.values.length;
    }

    public Tuple append( Object value ) {
        Object[] newValues = new Object[ this.values.length + 1 ];
        for( int i = 0; i < this.values.length; i++ ) {
            newValues[ i ] = this.values[ i ];
        }
        newValues[ this.values.length ] = value;
        return new Tuple( newValues );
    }

    public Tuple prepend( Object value ) {
        Object[] newValues = new Object[ this.values.length + 1 ];
        for( int i = 0; i < this.values.length; i++ ) {
            newValues[ i + 1 ] = this.values[ i ];
        }
        newValues[ 0 ] = value;
        return new Tuple( newValues );
    }
    
    public Iterator iterator() {
        return new ObjectArrayIterator( this.values );
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder( this.getClass().getSimpleName() ).append( "(" );
        for( int i = 0; i < this.values.length; i++ ) {
            builder.append( Strings.valueOf( this.values[ i ] ) );
            if( i < this.values.length - 1 ) {
                builder.append( ", " );
            }
        }
        return builder.append( ")" ).toString();
        
    }
}