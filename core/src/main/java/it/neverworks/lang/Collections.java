package it.neverworks.lang;

import java.util.Collection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Enumeration;
import java.util.Comparator;
import java.util.stream.Stream;
import java.lang.reflect.Array;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.collections.EnumerationUtils;
import org.apache.commons.collections.CollectionUtils;

public class Collections {
    
    public static class FunctionalBuilder<T> implements Filter<T> {
        protected Class<T> target;
    
        public FunctionalBuilder( Class<T> target ) {
            if( target != null ) {
                this.target = target;
            } else {
                throw new IllegalArgumentException( "Null type" );
            }
        }
    
        public Functional<T> in( Iterable source ) {
            return Collections.<T>functional( source ).filter( this );
        }

        public Functional<T> in( Stream source ) {
            return Collections.<T>functional( source ).filter( this );
        }

        public Functional<T> in( Enumeration source ) {
            return Collections.<T>functional( source ).filter( this );
        }

        public Functional<T> in( Map source ) {
            return Collections.<T>functional( source ).filter( this );
        }
    
        public Functional<T> in( Object source ) {
            return Collections.<T>functional( source ).filter( this );
        }
    
        protected Functional<T> build( Object source ) {
            return Collections.<T>functional( source ).filter( this );
        }
    
        public boolean filter( T item ) {
            return this.target != null ? this.target.isInstance( item ) : true;
        }
    }

    public static boolean isArray( Object value ) {
        return value != null && value.getClass().isArray();
    }

    public static boolean isArrayOf( Class type, Object value ) {
        return isArray( value ) && type.equals( value.getClass().getComponentType() );
    }

    public static boolean isIterable( Object value ) {
        return isListable( value );
    }
    
    public static boolean isListable( Object value ) {
        return value != null && ( 
            value instanceof Iterable 
            || value instanceof Map 
            || value instanceof Listable 
            || value instanceof Enumeration 
            || value instanceof Stream 
            || isArray( value ) );
    }
    
    public static Object[] toArray( Object value ) {
        return toArray( Object.class, value );
    }

    public static <T> T[] toArray( Class<T> itemType, Object value ) {
        if( value != null ) {
            if( isArray( value ) ) {
                if( value.getClass().getComponentType().equals( itemType ) ) {
                    return (T[]) value;
                } else {
                    int len = Array.getLength( value );
                    T[] result = (T[]) Array.newInstance( itemType, len );
                    for( int i = 0; i < len; i++ ) {
                        result[ i ] = (T) Array.get( value, i );
                    }
                    return result;
                }
                
            } else {
                List l = toList( value );
                T[] result = (T[]) Array.newInstance( itemType, l.size() );
                return (T[]) l.toArray( result );
            }
        } else {
            return (T[]) Array.newInstance( itemType, 0 );
        }
    }
    
    public static Object[] array( Object value ) {
        return toArray( Object.class, value );
    }

    public static <T> T[] array( Class<T> itemType, Object value ) {
        return Collections.<T>toArray( itemType, value );
    }

    public static <T> T[] array( Class<T> itemType, Iterable<T> value ) {
        if( value != null ) {
            List l = toList( value );
            T[] result = (T[]) Array.newInstance( itemType, l.size() );
            return (T[]) l.toArray( result );
        } else {
            return (T[]) Array.newInstance( itemType, 0 );
        }
    }

    public static <T> List<T> toList( Object value ) {
        if( value != null ) {
            
            if( value instanceof List ) {
                return (List<T>)value;
            
            } else if( isArray( value ) ) {
                int len = Array.getLength( value );
                ArrayList<T> list = new ArrayList<T>( len );
                for( int i = 0; i < len; i++ ) {
                    list.add( (T) Array.get( value, i ) );
                }
                return list;

            } else if( value instanceof Iterable ){
                ArrayList<T> list = new ArrayList<T>();
                for( Object it: (Iterable)value ) {
                    list.add( (T) it );
                }
                return list;
            
            } else if( value instanceof Listable ) {
                return (List<T>) ((Listable) value).list();
            
            } else if( value instanceof Map ) {
                return (List<T>) ( new ArrayList( ((Map) value).values() ) );

            } else if( value instanceof Enumeration ) {
                return (List<T>) EnumerationUtils.toList( (Enumeration) value );
                
            } else if( value instanceof Class && ((Class) value).isEnum() ) {
                return (List<T>) toList( new EnumIterable( (Class) value ) );

            } else if( value instanceof Wrapper ) {
                return Collections.<T>toList( ((Wrapper) value).asObject() );
                
            } else {
                throw new IllegalArgumentException( "Value cannot be converted to List: " + value.getClass().getName() );
            }
        } else {
            return new ArrayList();
        }
    }

    public static <T> List<T> list( Object value ) {
        return toList( value );
    }
    
    public static <T> List<T> list( Iterable<T> value ) {
        return (List<T>) toList( value );
    }

    public static <T> List<T> list( T[] value ) {
        return (List<T>) toList( value );
    }
    
    public static <T> List<T> asList( T... values ) {
        return (List<T>) toList( values );
    }

    public static class ListFunction<T> implements Inspector<T> {
        private List<T> list = new ArrayList<T>();

        public void inspect( T attr ) {
            list.add( attr );
        }
        
        public List<T> getList(){
            return this.list;
        }

        public List<T> list(){
            return this.list;
        }
        
        public <R> List<R> list( Class<R> resultClass ) {
            return (List<R>) this.list;
        }
        
    }

    public static <T> ListFunction<T> list() {
        return new ListFunction<T>();
    }

    public static class SetFunction<T> implements Inspector<T> {
        private Set<T> set = new HashSet<T>();

        public void inspect( T attr ) {
            set.add( attr );
        }
        
        public Set<T> getSet(){
            return this.set;
        }

        public Set<T> set(){
            return this.set;
        }
        
        public <R> Set<R> set( Class<R> resultClass ) {
            return (Set<R>) this.set;
        }
        
    }

    public static <T> SetFunction<T> set() {
        return new SetFunction<T>();
    }
    
    public static <T> Set<T> set( Object value ) {
        if( value != null ) {
            
            if( value instanceof Set ) {
                return (Set<T>)value;
            
            } else if( isListable( value ) ) {
                return new HashSet<T>( (List<T>) toList( value ) );

            } else {
                throw new IllegalArgumentException( "Value cannot be converted to Set: " + value.getClass().getName() );
            }
        } else {
            return new HashSet<T>();
        }
    }
    
    public static <T> Iterable<T> iterable( Object source ) {
        if( source != null ) {
            if( source instanceof Iterable ) {
                return (Iterable) source;

            } else if( Collections.isArray( source ) ) {
                return Collections.list( source );

            } else if( source instanceof Listable ) {
                return ((Listable) source).list();    

            } else if( source instanceof Map ) {
                return ((Map) source).values();

            } else if( source instanceof Stream ) {
                return new FunctionalStream<T>( (Stream) source );

            } else if( source instanceof Enumeration ) {
                return new EnumerationIterable<T>( (Enumeration<T>) source );

            } else if( source instanceof Class && ((Class) source).isEnum() ) {
                return (EnumIterable<T>) new EnumIterable( (Class) source );

            } else if( source instanceof Wrapper ) {
                return Collections.<T>toIterable( ((Wrapper) source).asObject() );

            } else {
                throw new IllegalArgumentException( "Source is not iterable: " + Objects.repr( source ) );
            }
        } else {
            return new ArrayList<T>();
        }
    }
    
    public static <T> Functional<T> functional( Object source ) {
        if( source != null ) {
            if( source instanceof Functional ) {
                return (Functional<T>) source;

            } else if( source instanceof Iterable ) {
                return new FunctionalIterable<T>( (Iterable) source );

            } else if( Collections.isArray( source ) ) {
                return new FunctionalIterable<T>( Collections.<T>toList( source ) );

            } else if( source instanceof Listable ) {
                return new FunctionalIterable<T>( ((Listable) source).list() );    

            } else if( source instanceof Map ) {
                return new FunctionalIterable<T>( ((Map) source).keySet() );

            } else if( source instanceof Stream ) {
                return new FunctionalStream<T>( (Stream) source );

            } else if( source instanceof Enumeration ) {
                return new FunctionalIterable<T>( new EnumerationIterable<T>( (Enumeration<T>) source ) );

            } else if( source instanceof Wrapper ) {
                return Collections.<T>functional( ((Wrapper) source).asObject() );

            } else {
                throw new IllegalArgumentException( "Cannot convert to Functional, source is not iterable: " + Objects.repr( source ) );
            }
        } else {
            return new FunctionalIterable<T>( new ArrayList<T>() );
        }
    }

    public static <T> Functional<T> each( Iterable<T> source ) {
        return (Functional<T>) functional( source );
    }

    public static <T> Functional<T> each( Enumeration<T> source ) {
        return (Functional<T>) functional( source );
    }

    public static <T> Functional<T> each( T[] source ) {
        return (Functional<T>) functional( source );
    }

    public static <T> Functional<T> each( Stream<T> source ) {
        return (Functional<T>) functional( source );
    }

    public static <T> Functional<T> toFunctional( Object source ) {
        return (Functional<T>) functional( source );
    }
    
    public static String join( Object collection, String delimiter ) {
        if( collection instanceof Collection ) {
            return StringUtils.join( (Collection) collection, delimiter );
        } else if( collection instanceof Iterator ) {
            return StringUtils.join( (Iterator) collection, delimiter );
        } else {
            return StringUtils.join( toList( collection ), delimiter );
        }
    }

    public static <T> List<T> reverse( List<T> list ) {
        ArrayList v = new ArrayList( list );
        java.util.Collections.reverse( v );
        return v;
    }

    public static <T> List<T> reverse( Object list ) {
        List v = new ArrayList( toList( list ) );
        java.util.Collections.reverse( v );
        return v;
    }
    
    public static <T> List<T> cat( Iterable... items ) {
        List<T> result = new ArrayList<T>();
        for( Iterable item: items ) {
            Iterator it = item.iterator();
            while( it.hasNext() ) {
                result.add( (T)it.next() );
            }
        }
        return result;
    }
    
    public static <T extends Comparable<? super T>> List<T> sort( Object source ) {
        List<T> sortable = source instanceof List ? new ArrayList<T>( (List<T>) source ) : Collections.<T>toList( source );
        java.util.Collections.sort( sortable );
        return sortable;
    }

    public static <T extends Comparable<? super T>> List<T> sort( Object source, Comparator<? super T> comparator ) {
        List<T> sortable = source instanceof List ? new ArrayList<T>( (List<T>) source ) : Collections.<T>toList( source );
        java.util.Collections.sort( sortable, comparator );
        return sortable;
    }
    
    public static boolean equals( Iterable iterable1, Iterable iterable2 ) {

        if( iterable1 == null ) { 
            return iterable2 == null;
        } else if( iterable2 == null ) {
            return false;
        }
        
        Iterator it1 = iterable1.iterator();
        Iterator it2 = iterable2.iterator();
        
        if( ! it1.hasNext() ) {
            return ! it2.hasNext();
        }
        
        while( it1.hasNext() ) {
            if( it2.hasNext() ) {
                Object n1 = it1.next();
                Object n2 = it2.next();
                
                if( n1 != null ? ! n1.equals( n2 ) : n2 != null ) {
                    return false;
                }
                
            } else {
                return false;
            }
        } 
        
        return ! it2.hasNext();
    
    }
    
    public static <K,V> Guard<Map<K,V>> missing( Map<K,V> map, K key ) {
        return map != null ? new SynchronizedPredicate<Map<K,V>>( map, t -> ! t.containsKey( key ) ) : new NullGuard<Map<K,V>>();
    }

    public static <K,V> Guard<FluentMap<K,V>> missing( FluentMap<K,V> map, K key ) {
        return map != null ? map.missing( key ) : new NullGuard<FluentMap<K,V>>();
    }

    public static <T> FluentList<T> flatten( Object source ) {
        return flatten( (Filter) null, source );      
    }                                        
                                             
    public static <T> FluentList<T> flatten( Class<T> cls, Object source ) {
        return flatten( Filters.instance( cls ), source );
    }
    
    public static <T> FluentList<T> flatten( Filter filter, Object source ) {
        if( isIterable( source ) ) {
            FluentList<T> result = new FluentArrayList<T>();
            flatten( filter, toIterable( source ), result );
            return result;
        } else {
            throw new IllegalArgumentException( "Source is not iterable: " + Objects.repr( source ) );
        }
    }
    
    protected static void flatten( Filter filter, Iterable source, FluentList result ) {
        Iterator iterator = source.iterator();
        while( iterator.hasNext() ) {
            Object value = iterator.next();
            if( filter != null && filter.filter( value ) ) {
                result.add( value );
            } else if( isIterable( value ) ) {
                flatten( filter, toIterable( value ), result );
            } else if( filter == null ) {
                result.add( value );
            }
        }
    }

    public static <T> Collection<T> unmodifiable( Collection<T> collection ) {
        return (Collection<T>) CollectionUtils.unmodifiableCollection( collection );
    }

    // Deprecated

    @Deprecated
    public static <T> Iterable<T> toIterable( Object source ) {
        return (Iterable<T>) iterable( source );
    }
    


}