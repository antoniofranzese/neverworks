package it.neverworks.lang;

public interface Hatching {
    public Object hatch();
}