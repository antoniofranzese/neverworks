package it.neverworks.lang;

import java.util.List;
import java.util.stream.Stream;

public interface Listable<T> {

    FluentList<T> list();

    default <O> FluentList<O> list( Class<O> target ) {
        return (FluentList<O>) list();
    }
    
    <O> O[] array( Class<O> target );

    public Stream<T> stream();
    
}