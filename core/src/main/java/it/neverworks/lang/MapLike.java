package it.neverworks.lang;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

public interface MapLike<K, V> extends Map<K, V> {

    default V get( Object key ) {
        throw new UnsupportedMethodException( this, "get" );
    }
    
	default void clear() {
		throw new UnsupportedMethodException( this, "clear" );
	}

	default Set<Map.Entry<K,V>> entrySet() {
		throw new UnsupportedMethodException( this, "entrySet" );
	}
	
	default boolean isEmpty() {
		throw new UnsupportedMethodException( this, "isEmpty" );
	}

	default Set<K> keySet() {
		throw new UnsupportedMethodException( this, "keySet" );
	}

	default void putAll( Map<? extends K,? extends V> m )  {
        if( m != null ) {
            for( K key: m.keySet() ) {
                this.put( key, m.get( key ) );
            }
        }
	}

	default V remove( Object key ) {
		throw new UnsupportedMethodException( this, "remove" );
	}

	default int size() {
		throw new UnsupportedMethodException( this, "size" );
	}

	default Collection<V> values () {
		throw new UnsupportedMethodException( this, "values" );
	}

	default boolean containsKey( Object key ) {
		throw new UnsupportedMethodException( this, "containsValue" );
	}

	default boolean containsValue( Object value ) {
		throw new UnsupportedMethodException( this, "containsValue" );
	}

	default V put( K key, V value ) {
	    throw new UnsupportedMethodException( this, "put" );
	}

}
