package it.neverworks.lang;

public interface ReturningWork<I,O> {
    O returnFromWork( I context ) throws Exception;
}