package it.neverworks.lang;

public class MissingPropertyException extends RuntimeException {
    
    private String property;
    private Class source;
    
    public MissingPropertyException( String message, Class source, String property ) {
        super( message );
        this.source = source;
        this.property = property;
    }

    public MissingPropertyException( String message, Object source, String property ) {
        this( message, source != null ? source.getClass() : (Class) null, property );
    }

    public MissingPropertyException( Class source, String property ) {
        this( Strings.message( "Missing ''{0}'' property in {1}", property, source.getName() ), source, property );
    }
    
    public MissingPropertyException( Object source, String property ) {
        this( source != null ? source.getClass() : (Class) null, property );
    }

    public Class getSource(){
        return this.source;
    }
    
    public String getProperty(){
        return this.property;
    }
    
}