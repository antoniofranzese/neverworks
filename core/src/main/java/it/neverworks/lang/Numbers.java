package it.neverworks.lang;

import java.util.Date;
import java.util.Random;
import java.util.Locale;
import java.util.Map;
import java.util.HashMap;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.NumberFormat;

public class Numbers {
    
	private static Random random;
    private static Map<Class<?>, Class<? extends Number>> primitives;

	static {
		random = new Random();
		random.setSeed( System.currentTimeMillis() );

        primitives = new HashMap<>();

        primitives.put( byte.class, Byte.class );
        primitives.put( short.class, Short.class );
        primitives.put( int.class, Integer.class );
        primitives.put( long.class, Long.class );
        primitives.put( float.class, Float.class );
        primitives.put( double.class, Double.class );

	}

    public final static Number ZERO = BigDecimal.ZERO;

    public static int random( int min, int max ) {
        return random.nextInt( ( max - min ) + 1 ) + min;
    }

    public static boolean isNumber( Object value ) {
        if( value != null ) {
            try {
                number( value );
                return true;
            } catch( IllegalArgumentException ex ) {
                return false;
            }
        } else {
            return false;
        }
    }
    
    public static Number number( Object value ) {
        if( value instanceof Number ) {
            return (Number) value;
        } else {
            return decimal( value );
        }
    }
    
    public static Number safeNumber( Object value ) {
        return value == null ? 0 : number( value );
    }
    
    public static BigDecimal decimal( Object value ) {
        return decimal( value, MathContext.UNLIMITED );
    }

    public static BigDecimal decimal( Object value, int scale ) {
        return decimal( value, new MathContext( scale ) );
    }

    public static BigDecimal decimal( Object value, int scale, RoundingMode rounding ) {
        return decimal( value, new MathContext( scale, rounding ) );
    }
    
    protected static BigDecimal decimal( Object value, MathContext mathContext ) {
        if( value instanceof BigDecimal ) {
            return (BigDecimal)value;
        } else if( value instanceof Byte || value instanceof Short || value instanceof Integer || value instanceof Long ) {
            return new BigDecimal( ((Number) value).longValue(), mathContext );
        } else if( value instanceof Double || value instanceof Float ) {
            return new BigDecimal( ((Number) value).doubleValue(), mathContext );
        } else if( value instanceof String ) {
            try {
                return new BigDecimal( (String) value, mathContext );
            } catch( NumberFormatException ex ) {
                throw new IllegalArgumentException( "Wrong number format: " + Objects.repr( value ) );
            }
        } else if( value instanceof Wrapper ) {
            return decimal( ((Wrapper) value).asObject() );

        } else {
            throw new IllegalArgumentException( "Wrong number: " + Objects.repr( value ) );
        }
    }
    
    public static BigDecimal safeDecimal( Object value ) {
        return value != null ? decimal( value ) : new BigDecimal( 0 );
    }

    public static BigDecimal safeDecimal( Object value, int scale ) {
        return value != null ? decimal( value, scale ) : new BigDecimal( 0, new MathContext( scale ) );
    }

    public static BigDecimal safeDecimal( Object value, int scale, RoundingMode rounding ) {
        return value != null ? decimal( value ) : new BigDecimal( 0, new MathContext( scale, rounding ) );
    }
    
    public static <T extends Number> T convert( Object value, Class<T> type ) {
        if( type.isInstance( value ) ) {
            return (T)value;

        } else if( value instanceof Number ) {
            if( type == BigDecimal.class ) {
                return (T) decimal( value );
            } else if( type == Long.class ) {
                return (T) new Long( ((Number) value).longValue() );
            } else if( type == Integer.class ) {
                return (T) new Integer( ((Number) value).intValue() );
            } else if( type == Short.class ) {
                return (T) new Short( ((Number) value).shortValue() );
            } else if( type == Byte.class ) {
                return (T) new Byte( ((Number) value).byteValue() );
            } else if( type == Float.class ) {
                return (T) new Float( ((Number) value).floatValue() );
            } else if( type == Double.class ) {
                return (T) new Double( ((Number) value).doubleValue() );
            } else {
                throw new IllegalArgumentException( "Cannot convert to " + type + ": " + Objects.repr( value ) );
            }
    
        } else if( value instanceof Wrapper ) {
            return convert( ((Wrapper) value).asObject(), type );
        
        } else {
            try {
                BigDecimal decimal = value != null ? new BigDecimal( Strings.valueOf( value ) ) : new BigDecimal( 0 );
            
                if( type == BigDecimal.class ) {
                    return (T) decimal;
                } else if( type == Long.class ) {
                    return (T) new Long( decimal.longValue() );
                } else if( type == Integer.class ) {
                    return (T) new Integer( decimal.intValue() );
                } else if( type == Short.class ) {
                    return (T) new Short( decimal.shortValue() );
                } else if( type == Byte.class ) {
                    return (T) new Byte( decimal.byteValue() );
                } else if( type == Float.class ) {
                    return (T) new Float( decimal.floatValue() );
                } else if( type == Double.class ) {
                    return (T) new Double( decimal.doubleValue() );
                } else {
                    throw new IllegalArgumentException( "Cannot convert to unsupported " + type + ": " + Objects.repr( value ) );
                }
            
            } catch( NumberFormatException ex ) {
                throw new IllegalArgumentException( "Cannot convert wrong number to " + type + ": " + Objects.repr( value ) );
            }
            
        }
    }
    
    public static Comparator test( Object number ) {
        return new Comparator( number );
    }
    
    public static class Comparator {

        private Object number;
        
        public Comparator( Object number ) {
            this.number = number;
        }
        
        public boolean gt( Object other ) {
            return Numbers.compare( number, other ) > 0;
        }
        
        public boolean ge( Object other ) {
            return Numbers.compare( number, other ) >= 0;
        }

        public boolean eq( Object other ) {
            return Numbers.compare( number, other ) == 0;
        }

        public boolean ne( Object other ) {
            return Numbers.compare( number, other ) != 0;
        }
        
        public boolean lt( Object other ) {
            return Numbers.compare( number, other ) < 0;
        }

        public boolean le( Object other ) {
            return Numbers.compare( number, other ) <= 0;
        }
        
    }
    
    public static int compare( Object number1, Object number2 ) {
        if( number1 != null && number2 != null ) {
            Number n1 = number( number1 );
            Number n2 = number( number2 );

            if( n1.getClass().equals( n2.getClass() ) ) {
                if( n1 instanceof Long ) {
                    return ( (Long) n1    ).compareTo( (Long) n2 );
                } else if( n1 instanceof Integer ) {
                    return ( (Integer) n1 ).compareTo( (Integer) n2 );
                } else if( n1 instanceof Float ) {
                    return ( (Float) n1   ).compareTo( (Float) n2 );
                } else if( n1 instanceof Double ) {
                    return ( (Double) n1  ).compareTo( (Double) n2 );
                } else if( n1 instanceof Short ) {
                    return ( (Short) n1   ).compareTo( (Short) n2 );
                } else if( n1 instanceof Byte ) {
                    return ( (Byte) n1    ).compareTo( (Byte) n2 );
                } else {
                    return decimal( n1 ).compareTo( decimal( n2 ) );
                }
            } else {
                return decimal( n1 ).compareTo( decimal( n2 ) );
            }

        } else if( number1 != null ) {
            return 1;
        } else if( number2 != null ) {
            return -1;
        } else {
            return 0;
        }
    }

    public static boolean isNaN( Object number ) {
        if( number instanceof Number ) {
            if( number instanceof Double && ((Double) number).isNaN() ) {
                return true;
            } else if( number instanceof Float && ((Float) number).isNaN() ) {
                return true;
            } else {
                return false;
            }
            
        } else {
            return false;
        }
    }
    
    public static NumberFormat formatter( Arguments arguments ) {
        Number maxFraction = arguments.get( Number.class, "maxDecimals", arguments.get( Number.class, "decimals", null ) );
        Number minFraction = arguments.get( Number.class, "minDecimals", ( maxFraction != null && maxFraction.intValue() > 0 ) ? 1 : null );
        Boolean grouping = arguments.get( Boolean.class, "group", null );
        
        Locale locale = null;

        if( arguments.has( "locale" ) ) {
            Object l = arguments.get( "locale" );
            if( l instanceof Locale ) {
                locale = (Locale) l;
            } else if( l instanceof String ) {
                locale = new Locale( ((String) l).toLowerCase() );
            } else if( locale != null ){
                throw new IllegalArgumentException( "Invalid locale: " + Objects.repr( l ) );
            }
        }
        
        NumberFormat format = locale != null ? NumberFormat.getInstance( locale ) : NumberFormat.getInstance();
        if( minFraction != null ) {
            format.setMinimumFractionDigits( minFraction.intValue() );
        }
        if( maxFraction != null ) {
            format.setMaximumFractionDigits( maxFraction.intValue() );
        }
        if( grouping != null ) {
            format.setGroupingUsed( grouping );
        }
        
        return format;
    }
    
    public static int loop( int number, int min, int max ) {
        int distance = max - min + 1;
        
        if( number > max ) {
            return min + ( number - min ) % distance;
        } else if( number < min ) {
            int page = ( ( number - min ) - ( ( number - min + 1) % distance ) ) / distance * distance;
            return number - page + distance;
        } else {
            return number;
        }
        
    }    
    
    public static int limit( int number, int min, int max ) {
        if( number > max ) {
            return max;
        } else if( number < min ) {
            return min;
        } else {
            return number;
        }
    }

    public static float limit( float number, float min, float max ) {
        if( number > max ) {
            return max;
        } else if( number < min ) {
            return min;
        } else {
            return number;
        }
    }

    public static double limit( double number, double min, double max ) {
        if( number > max ) {
            return max;
        } else if( number < min ) {
            return min;
        } else {
            return number;
        }
    }

    public static Byte NByte( Object number ) {
        return number == null ? null : Byte( number );
    }
    
    public static byte Byte( Object number ) {
        return number instanceof Byte ? (Byte) number : ( number( number != null ? number : 0 ).byteValue() );
    }

    public static Short NShort( Object number ) {
        return number == null ? null : Short( number );
    }
    
    public static short Short( Object number ) {
        return number instanceof Short ? (Short) number : ( number( number != null ? number : 0 ).shortValue() );
    }

    public static Integer NInt( Object number ) {
        return number == null ?null : Int( number );
    }
    
    public static int Int( Object number ) {
        return number instanceof Integer ? (Integer) number : ( number( number != null ? number : 0 ).intValue() );
    }

    public static Long NLong( Object number ) {
        return number == null ? null : Long( number );
    }
    
    public static long Long( Object number ) {
        return number instanceof Long ? (Long) number : ( number( number != null ? number : 0 ).longValue() );
    }

    public static Float NFloat( Object number ) {
        return number == null ? null : Float( number );
    }
    
    public static float Float( Object number ) {
        return number instanceof Float ? (Float) number : ( number( number != null ? number : 0 ).floatValue() );
    }

    public static Double NDouble( Object number ) {
        return number == null ? null : Double( number );
    }
    
    public static double Double( Object number ) {
        return number instanceof Double ? (Double) number : ( number( number != null ? number : 0 ).doubleValue() );
    }
    
    public static BigDecimal NDecimal( Object number ) {
        return number == null ? null : decimal( number );
    }
    
    public static BigDecimal NDecimal( Object number, int scale ) {
        return number == null ? null : decimal( number, scale );
    }

    public static BigDecimal NDecimal( Object number, int scale, RoundingMode rounding ) {
        return number == null ? null : decimal( number, scale, rounding );
    }

    public static BigDecimal Decimal( Object number ) {
        return safeDecimal( number );
    }
    
    public static BigDecimal Decimal( Object number, int scale ) {
        return safeDecimal( number, scale );
    }

    public static BigDecimal Decimal( Object number, int scale, RoundingMode rounding ) {
        return safeDecimal( number, scale, rounding );
    }
    
    public static Number round( Object number ) {
        return round( number, 0 );
    }

    public static Number round( Object number, int decimals ) {
        if( number != null ) {
            return decimal( number ).setScale( decimals, BigDecimal.ROUND_HALF_UP );
        } else {
            return null;
        }
    }

    public static Class<? extends Number> reference( Class<?> cls ) {
        if( primitives.containsKey( cls ) ) {
            return primitives.get( cls );
        } else {
            throw new IllegalArgumentException( "Invalid primitive number type: " + cls );
        }
    }
 
}