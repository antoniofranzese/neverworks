package it.neverworks.lang;

import org.apache.commons.beanutils.PropertyUtils;
import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Map;
import java.util.HashMap;

public class Properties {
    
    private final static Pattern GETTER_REGEX = Pattern.compile( "(is|get)([A-Z].*)" );
    private final static Pattern SETTER_REGEX = Pattern.compile( "set([A-Z].*)" );
    private final static Object ME = new Object();

    private final static Map CLASS_DESCRIPTORS = Collections.each( PropertyUtils.getPropertyDescriptors( Class.class ) )
        .dict( f -> Pair.of( f.getName(), f ) ); 

    private static class Description {
        public Class type;
        public Map descriptors;
        public Map fields;
        public Method universalGetter;
        public Method universalSetter;
        public Map staticDescriptors;
        public Map staticFields;
    }

    private final static FluentMap<Class,Description> descriptions = new FluentHashMap<>();

    @SuppressWarnings("unchecked")
	public static <T> T get( Object obj, String name ) {
        if( obj == null ) {
            throw new IllegalArgumentException( "Null object getting '" + name + "' property" );
        }

        Description description = getDescription( obj );

        if( obj instanceof Class ) {
            if( description.staticDescriptors.containsKey( name ) ) {
                Method getter = ((PropertyDescriptor) description.staticDescriptors.get( name )).getReadMethod();
                if( getter != null ) {
                    return (T) Reflection.invokeMethod( null, getter );
                } else {
                    throw new MissingPropertyException( "Unreadable " + ((Class) obj).getName() + "." + name + " static property", (Class) obj, name );
                }

            } else if( description.staticFields.containsKey( name ) ) {
                try {
                    return (T) Reflection.getField( (Class) obj, name ).get( null );
                } catch( Exception ex ) {
                    throw Errors.wrap( ex );
                }

            } else if( CLASS_DESCRIPTORS.containsKey( name ) ) {
                Method getter = ((PropertyDescriptor) CLASS_DESCRIPTORS.get( name )).getReadMethod();
                if( getter != null ) {
                    return (T) Reflection.invokeMethod( obj, getter );
                } else {
                    throw new MissingPropertyException( "Unreadable " + ((Class) obj).getName() + "." + name + " class property", (Class) obj, name );
                }

            } else {
                throw new MissingPropertyException( "Missing " + ((Class) obj).getName() + "." + name + " static property, field or class property", (Class) obj, name );
            }
            
        } else {
            
            if( description.descriptors.containsKey( name ) ) {
                Method getter = ((PropertyDescriptor) description.descriptors.get( name )).getReadMethod();
                if( getter != null ) {
                    return (T) Reflection.invokeMethod( obj, getter );
                } else {
                    throw new MissingPropertyException( "Unreadable " + Objects.className( obj ) + "." + name + " property", obj.getClass(), name );
                }

            } else if( description.universalGetter != null ) {
                return (T)Reflection.invokeMethod( obj, description.universalGetter, name );

            } else if( description.fields.containsKey( name ) ) {
                try {
                    return (T) Reflection.getField( obj.getClass(), name ).get( obj );
                } catch( Exception ex ) {
                    throw Errors.wrap( ex );
                }

            } else {
                throw new MissingPropertyException( "Missing " + Objects.className( obj ) + "." + name + " property or field", obj.getClass(), name );
            }

        }

    }
    
    public static void set( Object obj, String name, Object value ) {
        if( obj == null ) {
            throw new IllegalArgumentException( "Null object setting '" + name + "' property" );
        }

        Description description = getDescription( obj );

        if( obj instanceof Class ) {

            if( description.staticDescriptors.containsKey( name ) ) {
                Method setter = ((PropertyDescriptor) description.staticDescriptors.get( name )).getWriteMethod();
                if( setter != null ) {
                    Reflection.invokeMethod( null, setter, value );
                } else {
                    throw new MissingPropertyException( "Unwritable " + ((Class) obj).getName() + "." + name + " static property", (Class) obj, name );
                }

            } else if( description.staticFields.containsKey( name ) ) {
                try {
                    Reflection.getField( (Class) obj, name ).set( null, value );
                } catch( Exception ex ) {
                    throw Errors.wrap( ex );
                }

            } else {
                throw new MissingPropertyException( "Missing " + ((Class) obj).getName() + "." + name + " static property or field", (Class) obj, name );
            }

        } else {

            if( description.descriptors.containsKey( name ) ) {
                Method setter = ((PropertyDescriptor) description.descriptors.get( name )).getWriteMethod();
                if( setter != null ) {
                    Reflection.invokeMethod( obj, setter, value );
                } else {
                    throw new MissingPropertyException( "Unwritable " + Objects.className( obj ) + "." + name + " property", obj.getClass(), name );
                }

            } else if( description.universalSetter != null ) {
                Reflection.invokeMethod( obj, description.universalSetter, name, value );

            } else if( description.fields.containsKey( name ) ) {
                try {
                    Reflection.getField( obj.getClass(), name ).set( obj, value );
                } catch( Exception ex ) {
                    throw Errors.wrap( ex );
                }

            } else {
                throw new MissingPropertyException( "Missing " + Objects.className( obj ) + "." + name + " property or field", obj.getClass(), name );
            }

        }            

    }
    
    public static boolean has( Object obj, String name ) {
        if( obj != null ) {
            Description description = getDescription( obj );

            if( obj instanceof Class ) {

                if( description.staticDescriptors.containsKey( name )
                    || description.staticFields.containsKey( name ) ) {
                    return true;
                } else {
                    return false;
                }

            } else {

                if( description.descriptors.containsKey( name ) 
                    || description.fields.containsKey( name ) ) {
                    return true;
                } else {
                    return false;
                }
            }
        
        } else {
            return false;
        }

    }
    
    public static Class typeOf( Object obj, String name ) {
        Description description = getDescription( obj );
        if( description.descriptors.containsKey( name ) ) {
            Method getter = ((PropertyDescriptor) description.descriptors.get( name )).getReadMethod();
            return getter != null ? getter.getReturnType() : null;

        } else if( description.staticDescriptors.containsKey( name ) ) {
            Method getter = ((PropertyDescriptor) description.staticDescriptors.get( name )).getReadMethod();
            return getter != null ? getter.getReturnType() : null;

        } else if( description.fields.containsKey( name ) ) {
            return ((Field) description.fields.get( name )).getType();
        
        } else if( description.staticFields.containsKey( name ) ) {
            return ((Field) description.staticFields.get( name )).getType();
        
        } else {
            return null;
        }
    }
    
    public static class GetFunction<I,O> implements Mapper<I,O> {
        
        private String property;
        
        public GetFunction( String property ) {
            this.property = property;
        }
        
        public O map( I source ) {
            return (O) Properties.get( source, property );
        }
    
    }

    public static <T> GetFunction<Object,T> get( String property ) {
        return new GetFunction<Object,T>( property );
    }
    
    public static PropertyDescriptor getDescriptor( Object target, String property ) {
        Description description = getDescription( target );
        if( description.descriptors.containsKey( property ) ) {
            return (PropertyDescriptor) description.descriptors.get( property );
        } else {
            return null;
        }
    }

    public static PropertyDescriptor getStaticDescriptor( Class target, String property ) {
        Description description = getDescription( target );
        if( description.staticDescriptors.containsKey( property ) ) {
            return (PropertyDescriptor) description.staticDescriptors.get( property );
        } else {
            return null;
        }
    }

    public static FluentList<PropertyDescriptor> getDescriptors( Object target ) {
        return new FluentArrayList<PropertyDescriptor>( getDescription( target ).descriptors.values() );
    }

    public static FluentList<PropertyDescriptor> getStaticDescriptors( Class target ) {
        return new FluentArrayList<PropertyDescriptor>( getDescription( target ).staticDescriptors.values() );
    }

    //TODO: static description
    private static Description getDescription( Object target ) {
        Class type = target instanceof Class ? (Class) target : target.getClass(); 
        try {
            if( ! descriptions.containsKey( type ) ) {
                synchronized( ME ) {
                    if( ! descriptions.containsKey( type ) ) {
                        Description description = new Description();
                        description.type = type;
                        description.universalGetter = Reflection.findMethod( type, "get", String.class );
                        description.universalSetter = Reflection.findMethod( type, "set", String.class, Object.class );
                        
                        description.fields = new HashMap( Collections.each( type.getFields() )
                            .filter( f -> ! Reflection.isStatic( f ) )
                            .filter( f -> Reflection.isPublic( f ) )
                            .dict( f -> Pair.of( f.getName(), f ) ) 
                        );

                        description.staticFields = new HashMap( Collections.each( type.getFields() )
                            .filter( f -> Reflection.isStatic( f ) )
                            .filter( f -> Reflection.isPublic( f ) )
                            .dict( f -> Pair.of( f.getName(), f ) ) 
                        );
                        
                        description.descriptors = new HashMap( Collections.each( PropertyUtils.getPropertyDescriptors( type ) ).map( d -> {
                            String name = d.getName();
                            Method getter = d.getReadMethod();
                            if( getter == null ) {
                                String getterName = "get" + Strings.capitalizeFirst( name );
                                FluentList<Method> getters = Collections.each( type.getDeclaredMethods() )
                                    .filter( m -> m.getName().equals( getterName ) )
                                    .filter( m -> m.getParameterCount() == 0 )
                                    .filter( m -> m.getReturnType() != Void.TYPE )
                                    .filter( m -> Reflection.isPublic( m ) )
                                    .filter( m -> ! Reflection.isStatic( m ) )
                                    .list();
                                
                                if( getters.size() == 1 ) {
                                    getter = getters.get( 0 );
                                }

                            }

                            Class propertyType = getter != null ? getter.getReturnType() : Object.class;
                            Method setter = d.getWriteMethod();
                            if( setter == null ) {
                                String setterName = "set" + Strings.capitalizeFirst( name );
                                FluentList<Method> setters = Collections.each( type.getDeclaredMethods() )
                                    .filter( m -> m.getName().equals( setterName ) )
                                    .filter( m -> m.getParameterCount() == 1 )
                                    .filter( m -> m.getParameterTypes()[ 0 ].isAssignableFrom( propertyType ) )
                                    .filter( m -> m.getReturnType() == Void.TYPE )
                                    .filter( m -> Reflection.isPublic( m ) )
                                    .filter( m -> ! Reflection.isStatic( m ) )
                                    .list();

                                if( setters.size() == 1 ) {
                                    setter = setters.get( 0 );
                                }
                            }

                            try {
                                return new PropertyDescriptor( name, getter, setter );
                            } catch( IntrospectionException ex ) {
                                throw Errors.wrap( ex );
                            }
                        }).dict( d -> Pair.of( d.getName(), d ) ) );

                        //Interface properties  
                        FluentMap <String, PropertyInfo> propInfos = new FluentHashMap<>();
                        FluentMap <String, PropertyInfo> staticPropInfos = new FluentHashMap<>();
                        for( Method method: type.getMethods() ) {
                            if( Reflection.isDefault( method ) ) {
                                evaluateAccessor( method, propInfos );
                            } else if( Reflection.isStatic( method ) && Reflection.isPublic( method ) ) {
                                evaluateAccessor( method, staticPropInfos );
                            }
                        }

                        if( propInfos.size() > 0 ) {
                            for( PropertyInfo propInfo: propInfos.values() ) {
                                PropertyDescriptor descriptor = propInfo.getDescriptor();
                                if( !description.descriptors.containsKey( descriptor.getName() )  ) {
                                    description.descriptors.put( descriptor.getName(), descriptor );
                                }
                            }
                        }

                        description.staticDescriptors = new HashMap();
                        if( staticPropInfos.size() > 0 ) {
                            for( PropertyInfo staticPropInfo: staticPropInfos.values() ) {
                                PropertyDescriptor descriptor = staticPropInfo.getDescriptor();
                                description.staticDescriptors.put( descriptor.getName(), descriptor );
                            }
                        }

                        descriptions.put( type, description );

                    }
                }

            }

            return descriptions.get( type );

        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    private static void evaluateAccessor( Method method, FluentMap <String, PropertyInfo> infos ) {
        Matcher getterMatcher = GETTER_REGEX.matcher( method.getName() );
        if( getterMatcher.matches() 
            && method.getParameterCount() == 0
            && method.getReturnType() != Void.TYPE ) {

            String name = getterMatcher.group( 2 );
            if( ! infos.containsKey( name ) ) {
                infos.put( name, new PropertyInfo( name ) );
            }
            infos.get( name ).setGetter( method );

        } else {
            Matcher setterMatcher = SETTER_REGEX.matcher( method.getName() );
            if( setterMatcher.matches() 
                && method.getParameterCount() == 1
                && method.getReturnType() == Void.TYPE ) {

                String name = setterMatcher.group( 1 );
                if( ! infos.containsKey( name ) ) {
                    infos.put( name, new PropertyInfo( name ) );
                }
                infos.get( name ).setSetter( method );
            }
        }  
    }

    private static class PropertyInfo {
        protected String name;
        protected Method getter;
        protected Method setter;
        
        public PropertyInfo( String name ) {
            this.name = name.substring( 0, 1 ).toLowerCase() + name.substring( 1 );
        }

        public java.beans.PropertyDescriptor getDescriptor() {
            try {
                return new java.beans.PropertyDescriptor( this.name, this.getter, this.setter );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        }

        public Method getSetter(){
            return this.setter;
        }
        
        public void setSetter( Method setter ){
            this.setter = setter;
        }
        
        public Method getGetter(){
            return this.getter;
        }
        
        public void setGetter( Method getter ){
            this.getter = getter;
        }

        public String getName(){
            return this.name;
        }
        
    }

}