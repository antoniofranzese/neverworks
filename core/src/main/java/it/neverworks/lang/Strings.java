package it.neverworks.lang;

import java.util.List;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Date;
import java.util.Random;
import java.util.Collection;
import java.util.Formatter;
import java.util.regex.Pattern;
import java.lang.reflect.Field;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.lang.StringUtils;
import it.neverworks.text.TextFormat;

public class Strings {
	private static Random random;
    private static Field stringValueField;
    private static char[] alphaChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ".toCharArray();

	static {
		random = new Random();
		random.setSeed( System.currentTimeMillis() );

        try {
            stringValueField = Reflection.getField( String.class, "value" );
            Reflection.makeAccessible( stringValueField );
        } catch( Exception ex ) {
            stringValueField = null;
        }

	}

    public static String safe( Object object ) {
        return object == null ? "" : valueOf( object );
    }
    
    public static String generateHexID() {
        return generateHexID( 16 );
    }
    
	public static String generateHexID( int length ) {

		int len = Math.abs( length );
		if ( len > 1024 ) {
			len = 1024;
		}

		StringBuilder result = new StringBuilder( len + 32 );
		while ( result.length() < len ) {
			result.append( Long.toHexString( random.nextLong() ) );
		}

		result.setLength( len );
		return result.toString();
	}
    
    public static String generateAlphaID() {
        return generateAlphaID( 16 );
    }
    
    public static String generateAlphaID( int length ) {
		int len = Math.abs( length );
		if ( len > 1024 ) {
			len = 1024;
		}
        
        StringBuilder result = new StringBuilder( len );
        for( int i = 0; i < len; i++ ) {
            result.append( alphaChars[ random.nextInt( alphaChars.length ) ] );
        }
        
        return result.toString();
    }
    
    public static String valueOf( Object object ) {
        return object != null ? ( object instanceof String ? (String) object : String.valueOf( object ) ): null;
    }

	public static boolean isEmpty( Object object ) {
		return !hasText( object );
	}
	
	public static boolean hasText( String string ) {
		return ( string != null && string.length() > 0 && string.trim().length() > 0 );
	}
    
    public static boolean hasText( Object object ) {
        return hasText( valueOf( object ) );
    }
    
    public static String join( Collection collection, String delimiter ) {
        return StringUtils.join( collection, delimiter );
    }
    
    public static String message( String message, Object... params ) {
        return TextFormat.format( message, params );
    }

    public static String message( Locale locale, String message, Object... params ) {
        return TextFormat.format( locale, message, params );
    }

    public static String format( String message, Object... params ) {
        return new Formatter().format( message, params ).toString();
    }
    
    public static String snake2camel( String str ) {
        return separator2camel( str, '_' );
    }
    
    public static String camel2snake( String str ) {
        return camel2separator( str, '_' );
    }
    
    public static String hyphen2camel( String str ) {
        return separator2camel( str, '-' );
    }
    
    public static String camel2hyphen( String str ) {
        return camel2separator( str, '-' );
    }
    
    public static String separator2camel( String str, char separator ) {
        if( hasText( str ) ) {
            char[] source = raw( str );
            StringBuffer result = new StringBuffer( source.length );
            boolean upper = false;
            for( int i = 0; i < source.length; i++ ) {
                if( source[ i ] == separator ) {
                    upper = true;
                } else {
                    if( upper ) {
                        result.append( Character.toUpperCase( source[ i ] ) );
                        upper = false;
                    } else {
                        result.append( source[ i ] );
                    }
                }
            }
            return result.toString();
        } else {
            return str;
        }
    }

    public static String camel2separator( String str, char separator ) {
        if( hasText( str ) ) {
            char[] source = raw( str );
            StringBuffer result = new StringBuffer( (int)( source.length * 1.2 ) );
            boolean lower = false;
            for( int i = 0; i < source.length; i++ ) {
                if( Character.isUpperCase( source[ i ] ) && lower ) {
                    lower = false;
                    result.append( separator );
                } else {
                    lower = true;
                }
                result.append( Character.toLowerCase( source[ i ] ) );
            }
            return result.toString();
        } else {
            return str;
        }
    }
    
    public static String capitalizeFirst( Object str ) {
        return StringUtils.capitalize( safe( str ) );
    }
    
    public static String capitalize( Object str ) {
        char[] source = raw( str );
        StringBuilder target = new StringBuilder( source.length );
        boolean canCapitalize = true;
        boolean capitalize = false;
        for( char c = 0; c < source.length; c++ ) {
            if( Character.isLetter( source[ c ] ) ) {
                if( canCapitalize ) {
                    capitalize = true;
                }
            } else {
                canCapitalize = true;
            }
            
            if( capitalize ) {
                capitalize = false;
                canCapitalize = false;
                target.append( Character.toUpperCase( source[ c ] ) );
            } else {
                target.append( Character.toLowerCase( source[ c ] ) );
            }

        }
        return target.toString();
    }
    
    public static String limit( Object str, int max ) {
        return limit( str, max, "" );
    }
    
    public static String limit( Object str, int max, String truncation ) {
        String value = safe( valueOf( str ) );
        if( value.length() > max ) {
            return value.substring( 0, max ) + safe( truncation );
        } else {
            return value;
        }
    }
    
    public static List<String> split( Object str, String delimiter ) {
        String value = valueOf( str );
        if( hasText( value ) ) {
            return Collections.<String>list( value.split( delimiter ) );
        } else {
            return new ArrayList<String>();
        }
    }
    
    public static String join( Object collection, String delimiter ) {
        return Collections.join( collection, delimiter );
    }

    public static String keep( Object string, String allowedCharacters ) {
        return keep( string, Pattern.compile( allowedCharacters ) );
    }
    
    public static String keep( Object string, Pattern allowedCharacters ) {
        if( string != null ) {
            char[] characters = raw( string );
            StringBuilder result = new StringBuilder( characters.length );
            for( int i = 0; i < characters.length; i++ ) {
                if( allowedCharacters.matcher( new String( characters, i, 1 ) ).matches() ) {
                    result.append( characters[ i ] );
                }
            }
            return result.toString();
        } else {
            return null;
        }
    }
    
    public static String waste( Object string, String allowedCharacters ) {
        return waste( string, Pattern.compile( allowedCharacters ) );
    }
    
    public static String waste( Object string, Pattern allowedCharacters ) {
        if( string != null ) {
            char[] characters = raw( string );
            StringBuilder result = new StringBuilder( characters.length );
            for( int i = 0; i < characters.length; i++ ) {
                if( ! allowedCharacters.matcher( new String( characters, i, 1 ) ).matches() ) {
                    result.append( characters[ i ] );
                }
            }
            return result.toString();
        } else {
            return null;
        }
    }
    
    public static boolean hasRaw() {
        return stringValueField != null;
    }
    
    public static char[] raw( Object string ) {
        if( string != null ) {
            if( stringValueField != null ) {
                return (char[]) Reflection.getField( string instanceof String ? string : valueOf( string ), stringValueField );
            } else {
                return valueOf( string ).toCharArray();
            }
        } else {
            return new char[0];
        }
    }

    public static String repeat( Object string, int number ) {
        String s = valueOf( string );
        if( s != null ) {
            StringBuilder b = new StringBuilder( s.length() * number );
            for( int i = 0; i < number; i++ ) {
                b.append( s );
            }
            return b.toString();
        } else {
            return null;
        }
    }

    public static String plane( Object string ) {
        if( hasText( string ) ) {
            return valueOf( string );
        } else {
            return null;
        }
    }

    public static byte[] gzip( String string ) {
        if( hasText( string ) ) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Streams.gzip( string, baos );
            return baos.toByteArray();
        } else {
            return null;
        }
    }
    
    public static FluentList<String> list( String string ) {
        if( hasText( string ) ) {
            FluentList<String> result = new FluentArrayList<String>( string.length() );
            for( char c: raw( string ) ) {
                result.add( new String( new char[]{ c } ) );
            }
            return result;
        } else {
            return new FluentArrayList<String>();
        }
    }

    public static String trim( Object value ) {
        if( value != null ) {
            if( value instanceof StringBuilder ) {
                //TODO: optimize
                return ((StringBuilder) value).toString().trim();
            } else {
                return valueOf( value ).trim();
            }
        } else {
            return null;
        }
    }
}