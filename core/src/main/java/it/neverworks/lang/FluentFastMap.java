package it.neverworks.lang;

import java.util.Map;
import java.util.Set;
import java.util.Collection;
import org.apache.commons.collections.FastHashMap;

public class FluentFastMap<K,V> implements FluentMap<K,V>, Hatching {
    private FastHashMap fast;

    public FluentFastMap() {
        this.fast = new FastHashMap();
    }

    public FluentFastMap( int capacity ) {
        this.fast = new FastHashMap( capacity );
    }
    public FluentFastMap( int capacity, float factor ) {
        this.fast = new FastHashMap( capacity, factor );
    }
    public FluentFastMap( Map map ) {
        this.fast = new FastHashMap( map );
    }

    public V get( Object key ) {
        return (V) this.fast.get( key );
    }
    
    public void clear() {
        this.fast.clear();
    }

    public Set<Map.Entry<K,V>> entrySet() {
        return (Set<Map.Entry<K,V>>) this.fast.entrySet();
    }
    
    public boolean isEmpty() {
        return this.fast.isEmpty();
    }

    public Set<K> keySet() {
        return (Set<K>) this.fast.keySet();
    }

    public void putAll( Map<? extends K,? extends V> m )  {
        if( m != null ) {
            for( K key: m.keySet() ) {
                this.put( key, m.get( key ) );
            }
        }
    }

    public V remove( Object key ) {
        return (V) this.fast.remove( key );
    }

    public int size() {
        return this.fast.size();
    }

    public Collection<V> values() {
        return (Collection<V>) this.fast.values();
    }

    public boolean containsKey( Object key ) {
        return this.fast.containsKey( key );
    }

    public boolean containsValue( Object value ) {
        return this.fast.containsValue( value );
    }

    public V put( K key, V value ) {
        return (V) this.fast.put( key, value );
    }
    
    public Object hatch() {
        return new FluentFastMap();
    }

    public FluentFastMap<K,V> fast() {
        this.fast.setFast( true );
        return this;
    }

    public FluentFastMap<K,V> slow() {
        this.fast.setFast( false );
        return this;
    }

}