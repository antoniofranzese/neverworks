package it.neverworks.lang;

import java.lang.annotation.Annotation;

public class Filters {
    
    protected static class Annotated<T> implements Filter<T> {
        private Class<? extends Annotation> annotation;
        
        public Annotated( Class<? extends Annotation> annotation ) {
            this.annotation = annotation;
        }
        
        public boolean filter( T value ) {
            if( value != null ) {
                Class cls = value instanceof Class ? (Class) value : value.getClass();
                return Annotations.has( cls, annotation );
            } else {
                return false;
            }
        }
    }

    protected static class SubClass<T> implements Filter<T> {
        private Class<?> type;
        
        public SubClass( Class<?> type ) {
            this.type = type;
        }
        
        public boolean filter( T value ) {
            if( value != null ) {
                Class cls = value instanceof Class ? (Class) value : value.getClass();
                return type.isAssignableFrom( cls );
            } else {
                return false;
            }
        }
    }

    protected static class SuperClass<T> implements Filter<T> {
        private Class<?> type;
        
        public SuperClass( Class<?> type ) {
            this.type = type;
        }
        
        public boolean filter( T value ) {
            if( value != null ) {
                Class cls = value instanceof Class ? (Class) value : value.getClass();
                return cls.isAssignableFrom( type );
            } else {
                return false;
            }
        }
    }

    protected static class Instance<T> implements Filter<T> {
        private Class<?> type;
        
        public Instance( Class<?> type ) {
            this.type = type;
        }
        
        public boolean filter( T value ) {
            if( value != null ) {
                return type.isInstance( value );
            } else {
                return false;
            }
        }
    }

    protected static class NotInstance<T> implements Filter<T> {
        private Class<?> type;
        
        public NotInstance( Class<?> type ) {
            this.type = type;
        }
        
        public boolean filter( T value ) {
            if( value != null ) {
                return !( type.isInstance( value ) );
            } else {
                return true;
            }
        }
    }
    
    public static <T> Filter<T> annotated( Class<? extends Annotation> annotation ) {
        return new Annotated( annotation );
    }

    public static <T> Filter<T> subclass( Class<?> type ) {
        return new SubClass( type );
    }

    public static <T> Filter<T> superclass( Class<?> type ) {
        return new SuperClass( type );
    }

    public static <T> Filter<T> instance( Class<?> type ) {
        return new Instance( type );
    }

    public static <T> Filter<T> notInstance( Class<?> type ) {
        return new NotInstance( type );
    }
    
}