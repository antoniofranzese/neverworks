package it.neverworks.lang;

import java.util.List;
import java.util.Properties;
import java.io.InputStream;

public class Application {
    
    public final static Application INSTANCE = new Application();
    private final static Settings settings;
    private final static List<String> packages;

    static {
        //TODO: alternative setup source
        settings = new Settings().load( "META-INF/application/application.properties" );
        settings.setSystemPrefix( getProperty( "system.prefix", "application" ).trim() + "." );

        packages = Collections.each( Collections.list(( 
            getProperty( "packages", "" ) 
            + "," 
            + getProperty( "package", "" ) 
            + "," 
            + Framework.property( "packages", "" ) 
            + "," 
            + Framework.property( "package", "" ) 
        ).split( "," )))
        .filter( s -> Strings.hasText( s ) )
        .list();
    }

    public static void register( Package applicationPackage ) {
        packages.add( 0, applicationPackage.getName() );
    }

    public static String getProperty( String name ) {
        return settings.readProperty( name, null );
    }

    public static String getProperty( String name, String defaultValue ) {
        return settings.readProperty( name, defaultValue );
    }
    
    public static void setProperty( String name, String value ) {
        settings.writeProperty( name, value );
    }
    
    public static String property( String name ) {
        return settings.readProperty( name, null );
    }

    public static String property( String name, String defaultValue ) {
        return settings.readProperty( name, defaultValue );
    }

    public static boolean feature( String feature ) {
        return settings.checkFeature( feature, false );
    }

    public static boolean feature( String feature, boolean defaultValue ) {
        return settings.checkFeature( feature, defaultValue );
    }

    public static FluentList<Class> scan() {
        return null; //new ClassScanner( Application.class.getClassLoader() ).scanPackages( packages );
    }
    
    public static ClassScanner scanner() {
        return new ClassScanner( packages );
    }

    public static InputStream resource( String path ) {
        return Thread.currentThread().getContextClassLoader().getResourceAsStream( path );
    }
    
    public static InputStream resource( Object root, String name ) {
        String path;
        if( root instanceof String ) {
            path = ((String) root).trim();
        
        } else {
            Package pkg = null;
            if( root instanceof Class ) {
                pkg = ((Class) root).getPackage();
            } else if( root instanceof Package ) {
                pkg = (Package) root; 
            } else if( root != null ){
                pkg = root.getClass().getPackage();
            }
        
            path = pkg != null ? pkg.getName().replaceAll( "\\.", "/" ) : "";
        }
        
        name = Strings.safe( name ).trim();
        while( name.startsWith( "/" ) ) {
            name = name.substring( 1 );
        }
        
        return resource( path + "/" + name );
    }

}