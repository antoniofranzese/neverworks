package it.neverworks.lang;

public class UnimplementedMethodException extends UnimplementedException {
    
    public UnimplementedMethodException( Object owner, String method ) {
        this( owner.getClass(), method );
    }

    public UnimplementedMethodException( Class owner, String method ) {
        super( owner.getName() + "." + method + " is not supported" );
    }

}