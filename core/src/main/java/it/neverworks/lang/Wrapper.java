package it.neverworks.lang;

import java.util.function.Supplier;

public interface Wrapper<T> extends Supplier<T> {
    Object asObject();
    default T get() {
        return (T) asObject();
    }
}