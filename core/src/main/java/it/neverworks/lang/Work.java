package it.neverworks.lang;

public interface Work<T> {
    void doWithinWork( T context ) throws Exception;
}