package it.neverworks.lang;

import java.util.stream.Stream;

public interface FluentListLike<E> extends ListLike<E>,FluentList<E> {

    default Stream<E> stream() {
        throw new UnsupportedMethodException( this, "stream" );
    }
    
}