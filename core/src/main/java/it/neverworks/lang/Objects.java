package it.neverworks.lang;

public class Objects {

    public static String repr( Object object ) {
        return repr( object, 50 );
    }

    public static String repr( Object object, int limit ) {
        
        String repr = null;
        if( object != null && object instanceof Representable ) {
            repr = ((Representable) object).toRepresentation();
        } else {
            if( object != null ) {
                repr = Strings.limit( Strings.valueOf( object ), limit );
            } else {
                repr = "null";
            }
        }
        return repr + " <" + className( object ) + ">";
    }
    
    public static String className( Object object ) {
        return object != null ? ( object instanceof Class ? ((Class) object).getName() : object.getClass().getName() ) : "null";
    }
    
    public static Object unwrap( Object object ) {
        return object instanceof Wrapper ? ((Wrapper) object).asObject() : object;
    }
    
    public static boolean equals( Object object1, Object object2 ) {
        return object1 != null ? object1.equals( object2 ) : object2 == null;
    }
    
    public static <T> T hatch( Object obj ) {
        if( obj != null ) {
            if( obj instanceof Hatching ) {
                return (T) ((Hatching) obj).hatch();
            } else {
                return (T) Reflection.newInstance( obj.getClass() );
            }
        } else {
            return null;
        }
    }
}