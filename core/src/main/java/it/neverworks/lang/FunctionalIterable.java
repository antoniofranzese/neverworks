package it.neverworks.lang;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Stream;

public class FunctionalIterable<T> implements Functional<T> {
    
    protected Iterable<T> iterable;
    
    public FunctionalIterable( Iterable<T> iterable ) {
        this.iterable = iterable != null ? iterable : new ArrayList();
    }
    
    public Iterator<T> iterator() {
        return this.iterable.iterator();
    }
    
    public FluentList<T> list() {
        return FluentList.list( this.iterable );
    }
    
    public <O> O[] array( Class<O> target ) {
        return FluentList.array( this.iterable, target );
    }
    
    public Stream<T> stream() {
        return FluentList.stream( this.iterable );
    }

}