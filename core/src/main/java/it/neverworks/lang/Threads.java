package it.neverworks.lang;

import java.util.List;
import java.util.ArrayList;

public class Threads {
    
    public interface Cleaner {
        void cleanup( Thread thread );
    }
    
    private static List<Cleaner> cleaners = new ArrayList<>();
    
    public static void registerCleaner( Cleaner cleaner ) {
        if( !( cleaners.contains( cleaner ) ) ) {
            cleaners.add( cleaner );
        }
    } 
    
    public static void cleanup() {
        for( Cleaner cleaner: cleaners ) {
            cleaner.cleanup( Thread.currentThread() );
        }
    }
    
    public static void sleep( Number timeout ) {
        try {
            Thread.sleep( Numbers.Long( timeout ) );
        } catch( InterruptedException ex ) {

        }
    }    
    
}