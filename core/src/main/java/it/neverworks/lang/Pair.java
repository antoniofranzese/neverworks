package it.neverworks.lang;

import java.util.Map;

public class Pair extends Tuple {
    
    public static Pair of( Object... values ) {
        if( values == null || values.length != 2 ) {
            throw new IllegalArgumentException( "Invalid pair: " + Collections.list( values ) );
        }
        return new Pair( values );
    }

    public static Pair of( Iterable values ) {
        return new Pair( Collections.array( values ) );
    }

    public static Pair of( Object value1, Object value2 ) {
        return new Pair( new Object[]{ value1, value2 } );
    }

    public static Pair of( Map.Entry mapEntry ) {
        return new Pair( new Object[]{ mapEntry.getKey(), mapEntry.getValue() }); 
    }
    
    public Tuple append( Object value ) {
        return new Pair( new Object[]{ this.values[ 1 ], value } );
    }

    public Tuple prepend( Object value ) {
        return new Pair( new Object[]{ value, this.values[ 0 ] } );
    }
    
    protected Pair( Object[] values ) {
        super( values );
    }
    
}