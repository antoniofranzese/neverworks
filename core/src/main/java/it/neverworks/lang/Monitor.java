package it.neverworks.lang;

public class Monitor {
    
    private Object actual = new Object();
    private boolean released = false;
    private long defaultTimeout;
    
    public Monitor() {
        this( 300000 );
    }
    
    public Monitor( long defaultTimeout ) {
        super();
        this.defaultTimeout = defaultTimeout;
    }
    
    public void hold() {
        hold( defaultTimeout );
    }
    
    public void hold( long timeout ) {
        boolean wait = true;
        synchronized( this ) {
            if( released ) {
                wait = false;
            }
        }

        if( wait ) {
            synchronized( actual ) {
                if( !released ) {
                    try {
                        //System.out.println( "Monitor on " + Thread.currentThread().getName() + " Waiting" );
                        actual.wait( timeout );
                        //System.out.println( "Monitor on " + Thread.currentThread().getName() + " Unblocked" );
                    } catch( InterruptedException ex ) {
                
                    }
                } else {
                    //System.out.println( "Monitor on " + Thread.currentThread().getName() + " Already released in between" );
                }
            }
        } else {
            //System.out.println( "Monitor on " + Thread.currentThread().getName() + " Already released" );
        }
    } 
    
    public void release() {
        boolean release;
        synchronized( this ) {
            if( released ) {
                release = false;
            } else {
                release = true;
                released = true;
            }
        }
        if( release ) {
            synchronized( actual ) {
                actual.notifyAll();
            }
        }
    }
    
    public boolean released() {
        boolean result;
        synchronized( this ) {
            result = this.released;
        }
        return result;
    }
}

