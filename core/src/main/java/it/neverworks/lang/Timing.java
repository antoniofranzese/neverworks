package it.neverworks.lang;

import java.util.Date;

public class Timing implements Guard<Timing> {
    
    private String description;
    private long start;
    
    public Timing( String description ) {
        this.description = description;
    }
    
    public Timing enter() {
        this.start = new Date().getTime();
        return this;
    }
    
    public Timing restart() {
        return enter();
    }

    public long sample() {
        return new Date().getTime() - this.start;
    }
    
    public Throwable exit( Throwable error ){
        System.out.println( this.description + " time: " + ( new Date().getTime() - this.start ) + " ms" );
        return error;
    }

}