package it.neverworks.lang;

public class UnimplementedException extends RuntimeException {

    public UnimplementedException( Object target, String method ) {
        this( target.getClass(), method );
    }

    public UnimplementedException( Class cls, String method ) {
        super( cls.getName() + "." + method + " is not implemented" );
    }
    
    public UnimplementedException( String message ) {
        super( message );
    }
    
    public UnimplementedException() {
        super( "Feature is not implemented" );
    }
}