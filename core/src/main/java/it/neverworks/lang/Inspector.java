package it.neverworks.lang;

public interface Inspector<T> {
    void inspect( T item );
}