package it.neverworks.lang;

public class DeprecatedException extends RuntimeException {
    
    public DeprecatedException( String message ) {
        super( message );
    }

    public DeprecatedException() {
        super( "Feature is deprecated" );
    }

}