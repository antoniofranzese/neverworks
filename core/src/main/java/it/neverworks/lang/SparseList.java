package it.neverworks.lang;

import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

//TODO: experimental!!!
public class SparseList<E> implements FluentListLike<E> {
    
    private Map<Integer,E> items = new HashMap<Integer,E>();
    
    public int size() {
        return max() + 1;
    }
    
    public int max() {
        return items.size() > 0 ? Collections.max( items.keySet(), null ) : -1;
    }
    
    public boolean add( E e ) {
        add( size(), e );
        return true;
    }
    
    public void clear() {
        items.clear();
    }
    
    public boolean contains( Object o ) {
        return items.containsValue( o );
    }
    
    public E get( int index ) {
        if( items.containsKey( index ) ) {
            return items.get( index );
        } else if( index >= 0 && index < size() ){
            return null;
        } else {
            throw new IndexOutOfBoundsException( "Index out of bounds: " + index );
        }
    }
    
    public int indexOf( Object o ) {
        if( o != null ) {
            for( int index: items.keySet() ) {
                if( o.equals( items.get( index ) ) ) {
                    return index;
                }
            }
        }
        return -1;
    }
    
    public boolean isEmpty() {
        return items.size() > 0;
    }    
    
    private class SparseIterator implements Iterator {
        private Iterator<Integer> iterator;
        
        public SparseIterator( Iterator<Integer> iterator ) {
            this.iterator = iterator;
        }
        
        public boolean hasNext() {
            return this.iterator.hasNext();
        }
        
        public Object next() {
            return SparseList.this.get( this.iterator.next() );
        }
        
        public void remove() {
            this.iterator.remove();
        }
    }
    
    public List<Integer> indexes() {
        List<Integer> indexes = new ArrayList( items.keySet() );
        Collections.sort( indexes );
        return indexes;
    }
    
    public Iterator<E> iterator() {
        return new SparseIterator( indexes().iterator() );
    }
    
    public E remove( int index ) {
        if( items.containsKey( index ) ) {
            return items.remove( index );
        } else {
            return null;
        }
    }
    
    public boolean remove( Object o ) {
        int index = indexOf( o );
        if( index >= 0 ) {
            remove( index );
            return true;
        } else {
            return false;
        }
    }
    
    public List<E> subList( int fromIndex, int toIndex ) {
        SparseList<E> result = new SparseList<E>();
        for( int index : items.keySet() ) {
            if( index >= fromIndex && index <= toIndex ) {
                result.set( index, items.get( index ) );
            }
        }
        return result;
    }
    
    public E set( int index, E element ) {
        if( index >= 0 ) {
            return items.put( index, element );
        } else {
            throw new IndexOutOfBoundsException( "Index out of bounds: " + index );
        }
    }
    
    public boolean removeAll( Collection<?> c ) {
        boolean changed = false;
        for( int index: this.indexes() ) {
            if( c.contains( this.items.get( index ) ) ) {
                this.remove( index );
                changed = true;
            }
        }
        return changed;
    }
    
    public boolean retainAll( Collection<?> c ) {
        boolean changed = false;
        for( int index: this.indexes() ) {
            if( ! c.contains( this.items.get( index ) ) ) {
                this.remove( index );
                changed = true;
            }
        }
        return changed;
    }
    
}