package it.neverworks.lang;

import java.lang.annotation.Annotation;

public class AnnotationInfo<T extends Annotation> {
    
    private T annotation;
    private Object target;
    
    public Object getTarget(){
        return this.target;
    }

    public T getAnnotation(){
        return this.annotation;
    }

    public T annotation(){
        return this.annotation;
    }
    
    public AnnotationInfo( T annotation, Object target ) {
        this.annotation = annotation;
        this.target = target;
    }
    
}