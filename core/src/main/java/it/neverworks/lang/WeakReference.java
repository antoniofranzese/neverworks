package it.neverworks.lang;

public class WeakReference<T> extends java.lang.ref.WeakReference<T> implements Guard<T> {

    public WeakReference( T referent ) {
        super( referent );
    }

    public WeakReference( T referent, java.lang.ref.ReferenceQueue<? super T> q ) {
        super( referent, q );
    }

    public T enter(){
        T instance = this.get();
        if( instance != null ) {
            return instance;
        } else {
            throw new Guard.Exit();
        }
    }
    
}