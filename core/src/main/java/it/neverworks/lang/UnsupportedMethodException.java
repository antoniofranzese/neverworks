package it.neverworks.lang;

public class UnsupportedMethodException extends UnsupportedException {
    
    public UnsupportedMethodException( Object owner, String method ) {
        this( owner.getClass(), method );
    }

    public UnsupportedMethodException( Class owner, String method ) {
        super( owner.getName() + "." + method + " is not supported" );
    }

}