package it.neverworks.lang;

public interface Reducer<A,T> {
    A reduce( A accumulator, T item );
}