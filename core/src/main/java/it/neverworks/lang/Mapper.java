package it.neverworks.lang;

public interface Mapper<I,O> {
    O map( I value );
}