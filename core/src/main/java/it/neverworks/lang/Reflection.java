package it.neverworks.lang;

import java.lang.reflect.Type;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Constructor;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.annotation.Annotation;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import org.springframework.util.ReflectionUtils;
import org.apache.commons.beanutils.MethodUtils;

import it.neverworks.lang.annotation.AnnotatedClass;
import it.neverworks.lang.annotation.AnnotatedMethod;
import it.neverworks.lang.annotation.AnnotationManager;

@SuppressWarnings( "unchecked" )
public class Reflection {
    
    public static class AnyType {}
        
    public static Mapper<Object, Class> CLASSES_MAPPER = new Mapper<Object, Class>(){
        public Class map( Object object ){
            return object != null ? object.getClass() : null;
        }
    };

    public static boolean isClass( String name ) {
        return findClass( name ) != null;
    }
    
    public static boolean isArray( Object value ) {
        return value != null && value.getClass().isArray();
    }

    public static boolean isArrayOf( Class type, Object value ) {
        return isArray( value ) && type.equals( value.getClass().getComponentType() );
    }

    public static Class getRawClass( Type source ) {
        return (Class) ( source instanceof ParameterizedType ? ((ParameterizedType) source).getRawType() : source );
    }

    public static Class[] getClassHierarchy( Class source ) {
        return getClassHierarchy( source, Object.class );
    }
    
    public static Class[] getClassHierarchy( Class source, Class topClass ) {
        List<Class> all = new ArrayList<Class>();
        Class cls = source;
        do {
             cls = cls.getSuperclass();
             if( ! Object.class.equals( cls ) ) {
                 all.add( cls );
             }
            
        } while( ! topClass.equals( cls ) );
        
        return all.toArray( new Class[ all.size() ] );
    }


    public static Class[] getInterfaceHierarchy( Class source ) {
        List<Class> all = new ArrayList<Class>();
        collectInterfaces( source, all );
        return all.toArray( new Class[ all.size() ] );
    }
    
    private static void collectInterfaces( Class source, List<Class> all ) {
        List<Class> queue = null;
        for( Class iface: source.getInterfaces() ) {
            if( ! all.contains( iface ) ) {
                all.add( iface );
                if( queue == null ) {
                    queue = new ArrayList<Class>();
                }
                queue.add( iface );
            }
        }
        
        if( queue != null ) {
            for( Class cls: queue ) {
                collectInterfaces( cls, all );
            }
        }
    }

	public static Class findClass( String name ) {
		try {
			return Class.forName( name );
			//return Class.forName( name, true, Thread.currentThread().getContextClassLoader() );
		} catch( ClassNotFoundException ex ) {
            return null;
		} catch( NoClassDefFoundError ex ) {
            return null;
		}
	}

    public static Field getField( Class source, String name ) {
        try {
            return source.getDeclaredField( name );
        } catch( NoSuchFieldException ex ) {
            return null;
        }
    }

    public static Object getField( Object target, String field ) {
        try {
            Field actualField = getField( target.getClass(), field );
            makeAccessible( actualField );
            return actualField.get( target );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static Object getField( Object target, Field field ) {
        try {
            return field.get( target );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public static void setField( Object target, Field field, Object value ) {
        try {
            field.set( target, value );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static void setField( Object target, String field, Object value ) {
        try {
            Field actualField = getField( target.getClass(), field );
            makeAccessible( actualField );
            actualField.set( target, value );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
    }

    public static Constructor getConstructor( Class source, Class... types ) {
        try {
            Constructor ctor = source.getDeclaredConstructor( types );
            makeAccessible( ctor );
            return ctor;
        } catch( NoSuchMethodException ex ) {
            return null;
        }
        
    }
    
    public static Method findMethodWithHierarchy( Object source, String name, Class reference ) {
        return findMethodWithHierarchy( source.getClass(), name, reference );
    }
    
    public static Method findMethodWithHierarchy( Class source, String name, Class reference ) {
        Class current = reference;
        Method found = null;
        
        do {
            found = findMethod( source, name, current );
            if( found == null ) {
                current = current.getSuperclass();
            }
        } while( found == null && current != Object.class );
        
        return found;
    }
    
    public static Method findMethod( Object source, String name, Class... types ) {
        return findMethod( source.getClass(), name, types );
    }
    
    public static Method findMethod( Class source, String name, Class... types ) {
        
        for( Method method: source.getDeclaredMethods() ) {
            if( method.getName().equals( name ) ) {
                boolean matches = true;
                Class[] parameters = method.getParameterTypes();
                if( parameters.length != types.length ) {
                    matches = false;
                } else {
                    for( int i = 0; i < parameters.length; i++ ) {
                        if( !parameters[ i ].equals( types[ i ] ) ) {
                            matches = false;
                        }
                    }
                }
                
                if( matches ) {
                    return method;
                }    
            }
        }
        if( source.getSuperclass() != Object.class ) {
            return findMethod( source.getSuperclass(), name, types );
        } else {
            return null;
        }
    }

    public static FluentList<Method> findMethods( Class source, String name ) {
        return findMethods( source, name, false );
    }

    public static FluentList<Method> findAccessibleMethods( Class source, String name ) {
        return findMethods( source, name, true );
    }

    private static FluentList<Method> findMethods( Class source, String name, boolean accessible ) {
        FluentList<Method> result = new FluentArrayList<Method>();
        Class current = source;
        while( current != Object.class ) {
            for( Method method: current.getDeclaredMethods() ) {
                if( method.getName().equals( name ) && ( !accessible || method.isAccessible() ) ) {
                    result.add( method );
                }
            }
            current = current.getSuperclass();
        }
        return result;
    }
    
    public static Method findMatchingMethod( Class source, String name, Class... types ) {
        return MethodUtils.getMatchingAccessibleMethod( source, name, types );
    }
    
    public static FluentList<Method> findMethods( Class source ) {
        FluentList<Method> result = new FluentArrayList<Method>();
        Class current = source;
        while( current != Object.class ) {
            for( Method method: current.getDeclaredMethods() ) {
                boolean found = false;
                for( Method existing: result ) {
                    if( methodMatch( method, existing ) ) {
                        found = true;
                        break;
                    }
                }
                
                if( ! found ) {
                    result.add( method );
                }
            }
            current = current.getSuperclass();
        }
        
        return result;
        
    }
    
    public static FluentList<Method> findMethods( Class source, final Class... types ) {
        if( types != null && types.length > 0 ) {
            return findMethods( source )
                .filter( method -> {
                    Class[] parameters = method.getParameterTypes();
                    if( parameters.length != types.length ) {
                        return false;
                    }
                    for( int i = 0; i < parameters.length; i++ ) {
                        if( types[ i ] != AnyType.class && ! parameters[ i ].isAssignableFrom( types[ i ] ) ) {
                            return false;
                        }
                    }
                    return true;
                })
                .list();
            
        } else {
            return findMethods( source );
        }
    }
    
    public static Method getMethod( Class source, String name, Class... types ) {
        try {
            return source.getMethod( name, types );
        } catch( NoSuchMethodException ex ) {
            return null;
        }
    }
    
    public static Object invokeMethod( Object target, Method method, Object... params ){
        try {
            if( ! method.isAccessible() ) {
                ReflectionUtils.makeAccessible( method );
            }
            return method.invoke( target, params );
        } catch( InvocationTargetException ex ) {
            throw Errors.wrap( ex );
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        }
        
    }
    
    public static boolean methodMatch( Method method1, Method method2 ) {
        if( ! method1.getName().equals( method2.getName() ) ) {
            return false;
        }
        
        if( ! method1.getReturnType().equals( method2.getReturnType() ) ) {
            return false;
        }
        
        Class[] param1 = method1.getParameterTypes();
        Class[] param2 = method2.getParameterTypes();

        if( param1.length != param2.length ) {
            return false;
        }
        
        for( int i = 0; i < param1.length; i++ ) {
            if( ! param1[ i ].equals( param2[ i ] ) ) {
                return false;
            }
        }
        
        return true;
    }
    
    public static Object newInstance( Constructor ctor, Object... params ) {
        try {
            Object instance = ctor.newInstance( params );
            
            return instance;
            
        } catch( IllegalAccessException ex ) {
            throw Errors.wrap( ex );
        } catch( IllegalArgumentException ex ) {
            throw Errors.wrap( ex );
        } catch( InstantiationException ex ) {
            throw Errors.wrap( ex );
        } catch( InvocationTargetException ex ) {
            throw Errors.wrap( ex );
        } catch( ExceptionInInitializerError ex ) {
            throw Errors.wrap( ex );
        }
    }
 
    public static <T> T newInstance( Class<T> source ) {
        Constructor ctor = getConstructor( source );
        if( ctor != null ) {
            return (T) newInstance( ctor );
        } else {
            throw new RuntimeException( "Missing default constructor for " + source );
        }
    }

    public static class Instantiator<T> {
        public T newInstance( Object... params ) {
            return null;
        }
    }
    
    public static class ConcreteInstantiator<T> extends Instantiator {
        Constructor ctor;
        
        public ConcreteInstantiator( Constructor ctor ) {
            this.ctor = ctor;
        }
        
        public T newInstance( Object... params ) {
            return (T) Reflection.newInstance( this.ctor, params );
        }
    }

    public static Package findPackage( String name ) {
        return Package.getPackage( name );
    }
    
    public static <T> Instantiator<T> withConstructor( Class<T> source, Class... types ) {
        Constructor ctor = getConstructor( source, types );
        if( ctor == null ) {
            return new Instantiator<T>();
        } else {
            return new ConcreteInstantiator<T>( ctor );
        }
    }
    
    public static boolean hasConstructor( Class source, Class... types ) {
        return getConstructor( source, types ) != null;
    }
    
    public static void makeAccessible( Constructor ctor ) {
		if( !ctor.isAccessible() ) {
			ReflectionUtils.makeAccessible( ctor );
		}
    }

    public static void makeAccessible( Field field ) {
		if( !field.isAccessible() ) {
			ReflectionUtils.makeAccessible( field );
		}
    }

    public static void makeAccessible( Method method ) {
		if( !method.isAccessible() ) {
			ReflectionUtils.makeAccessible( method );
		}
    }
    
    public static Annotation[] getAnnotations( Class<?> cls ) {
        return AnnotationManager.getAnnotatedClass( cls ).getAllAnnotations();
    }

    public static Annotation[] getAnnotations( Method method ) {
        return method.getAnnotations();
    }

    public static Annotation[] getAnnotations( Field field ) {
        return field.getDeclaredAnnotations();
    }

    public static Annotation[] getAnnotations( Package pack ) {
        return pack.getAnnotations();
    }

    public static boolean isInner( Class<?> cls ) {
        return cls != null && cls.getDeclaringClass() != null;
    }

    public static boolean isInterface( Class<?> cls ) {
        return cls != null && ( cls.getModifiers() & Modifier.INTERFACE ) != 0;
    }

    public static boolean isAbstract( Class<?> cls ) {
        return cls != null && ( cls.getModifiers() & Modifier.ABSTRACT ) != 0;
    }

    public static boolean isPublic( Class<?> cls ) {
        return cls != null && ( cls.getModifiers() & Modifier.PUBLIC ) != 0;
    }

    public static boolean isProtected( Class<?> cls ) {
        return cls != null && ( cls.getModifiers() & Modifier.PROTECTED ) != 0;
    }

    public static boolean isPrivate( Class<?> cls ) {
        return cls != null && ( cls.getModifiers() & Modifier.PRIVATE ) != 0;
    }

    public static boolean isStatic( Class<?> cls ) {
        return cls != null && ( cls.getModifiers() & Modifier.STATIC ) != 0;
    }


    public static boolean isAbstract( Method method ) {
        return method != null && ( method.getModifiers() & Modifier.ABSTRACT ) != 0;
    }

    public static boolean isPublic( Method method ) {
        return method != null && ( method.getModifiers() & Modifier.PUBLIC ) != 0;
    }

    public static boolean isProtected( Method method ) {
        return method != null && ( method.getModifiers() & Modifier.PROTECTED ) != 0;
    }

    public static boolean isPrivate( Method method ) {
        return method != null && ( method.getModifiers() & Modifier.PRIVATE ) != 0;
    }

    public static boolean isStatic( Method method ) {
        return method != null && ( method.getModifiers() & Modifier.STATIC ) != 0;
    }

    public static boolean isDefault( Method method ) {
        return method != null && method.isDefault();
    }

    public static boolean isPublic( Field field ) {
        return field != null && ( field.getModifiers() & Modifier.PUBLIC ) != 0;
    }

    public static boolean isProtected( Field field ) {
        return field != null && ( field.getModifiers() & Modifier.PROTECTED ) != 0;
    }

    public static boolean isPrivate( Field field ) {
        return field != null && ( field.getModifiers() & Modifier.PRIVATE ) != 0;
    }

    public static boolean isStatic( Field field ) {
        return field != null && ( field.getModifiers() & Modifier.STATIC ) != 0;
    }

    public static String fullName( Class cls ) {
        return cls != null ? cls.getName() : null;
    }
    
    public static String fullName( Method method ) {
        return method != null ? ( fullName( method.getDeclaringClass() ) + "." + method.getName() ) : null;
    }
    
    public static String fullName( Field field ) {
        return field != null ? ( fullName( field.getDeclaringClass() ) + "." + field.getName() ) : null;
    }
    
    public static Class thisClass() {
        return findClass( Thread.currentThread().getStackTrace()[ 2 ].getClassName() );
    }

    public static Class callerClass() {
        return findClass( Thread.currentThread().getStackTrace()[ 3 ].getClassName() );
    }

    public static Class callerClass( int level ) {
        return findClass( Thread.currentThread().getStackTrace()[ 3 + level ].getClassName() );
    }

    public static Package thisPackage() {
        return findClass( Thread.currentThread().getStackTrace()[ 2 ].getClassName() ).getPackage();
    }

    public static Package callerPackage() {
        return findClass( Thread.currentThread().getStackTrace()[ 3 ].getClassName() ).getPackage();
    }

    public static Package callerPackage( int level ) {
        return findClass( Thread.currentThread().getStackTrace()[ 3 + level ].getClassName() ).getPackage();
    }
    
    public static Class getGenericType( Class cls, int index ) {
        Type superClass = cls.getGenericSuperclass();
        if( superClass instanceof ParameterizedType ) {
            Type[] types = ((ParameterizedType) superClass).getActualTypeArguments();
            if( types.length > index ) {
                return (Class) types[ index ];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static FluentList<Class> getGenericTypes( Class cls ) {
        Type superClass = cls.getGenericSuperclass();
        if( superClass instanceof ParameterizedType ) {
            Type[] types = ((ParameterizedType) superClass).getActualTypeArguments();
            FluentList<Class> classes = new FluentArrayList<Class>( types.length );
            for( int i = 0; i < types.length; i++ ) {
                classes.add( (Class) types[ i ] );
            }
            return classes;
        } else {
            return new FluentArrayList<Class>();
        }
    }

    public static Class<?> detectType( Class container, Type type ) {
        if( type instanceof TypeVariable ) {
            return getTypeArgument( container, ((TypeVariable) type).getName() );
        } else if( type instanceof ParameterizedType ) {
            return (Class)((ParameterizedType) type).getRawType();
        } else if( type instanceof Class ) {
            return (Class) type;
        } else {
            throw new IllegalArgumentException( "Unknown Type: " + type.getClass() );
        }
    }

    public static Class<?> detectParameterType( Class container, Type type, int index ) {
        if( type instanceof ParameterizedType ) {
            return detectType( container, ((ParameterizedType) type).getActualTypeArguments()[ index ] );
        } else {
            return Object.class;
        }
    }

    public static Class<?> getTypeArgument( Class container, String name ) {
        TypeVariable[] tvs =  container.getSuperclass().getTypeParameters(); 
        for( int i = 0; i < tvs.length; i++ ) {
            if( tvs[ i ].getName().equals( name ) ) {
                return (Class) ((ParameterizedType) container.getGenericSuperclass()).getActualTypeArguments()[ i ];
            }
        }
        return Object.class;
    }

}