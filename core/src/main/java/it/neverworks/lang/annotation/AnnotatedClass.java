package it.neverworks.lang.annotation;

import java.lang.annotation.*;
import java.lang.reflect.*;

public interface AnnotatedClass
{
    Class<?> getTheClass();
    
    Annotation[] getAllAnnotations();
    
    Annotation getAnnotation(final Class<?> p0);
    
    AnnotatedMethod[] getAnnotatedMethods();
    
    AnnotatedMethod getAnnotatedMethod(final String p0, final Class<?>[] p1);
    
    AnnotatedMethod getAnnotatedMethod(final Method p0);
}
