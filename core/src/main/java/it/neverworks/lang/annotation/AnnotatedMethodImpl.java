package it.neverworks.lang.annotation;

import java.lang.reflect.*;
import java.lang.annotation.*;
import java.util.*;

class AnnotatedMethodImpl implements AnnotatedMethod
{
    private final AnnotatedClass annotatedClass;
    private final Method method;
    private Map<Class<?>, Annotation> classToAnnotationMap;
    private Annotation[] annotations;
    
    AnnotatedMethodImpl(final AnnotatedClass annotatedClass, final Method method) {
        this.classToAnnotationMap = null;
        this.annotations = null;
        this.annotatedClass = annotatedClass;
        this.method = method;
    }
    
    private Map<Class<?>, Annotation> getAllAnnotationMap() {
        if (this.classToAnnotationMap == null) {
            this.classToAnnotationMap = this.getAllAnnotationMapCalculated();
        }
        return this.classToAnnotationMap;
    }
    
    private Map<Class<?>, Annotation> getAllAnnotationMapCalculated() {
        final HashMap<Class<?>, Annotation> result = new HashMap<Class<?>, Annotation>();
        final Class<?> superClass = (Class<?>)this.getAnnotatedClass().getTheClass().getSuperclass();
        if (superClass != null) {
            this.fillAnnotationsForOneMethod(result, AnnotationManager.getAnnotatedClass((Class)superClass).getAnnotatedMethod(this.getMethod().getName(), (Class[])this.getMethod().getParameterTypes()));
        }
        final Class[] interfaces = this.getAnnotatedClass().getTheClass().getInterfaces();
        for (int i = 0; i < interfaces.length; ++i) {
            final Class<?> c = (Class<?>)interfaces[i];
            this.fillAnnotationsForOneMethod(result, AnnotationManager.getAnnotatedClass((Class)c).getAnnotatedMethod(this.getMethod().getName(), (Class[])this.getMethod().getParameterTypes()));
        }
        final Annotation[] declaredAnnotations = this.getMethod().getDeclaredAnnotations();
        for (int j = 0; j < declaredAnnotations.length; ++j) {
            final Annotation annotation = declaredAnnotations[j];
            result.put(annotation.getClass().getInterfaces()[0], annotation);
        }
        return result;
    }
    
    private void fillAnnotationsForOneMethod(final HashMap<Class<?>, Annotation> result, final AnnotatedMethod annotatedMethod) {
        if (annotatedMethod == null) {
            return;
        }
        this.addAnnotations(result, annotatedMethod.getAllAnnotations());
    }
    
    private void addAnnotations(final HashMap<Class<?>, Annotation> result, final Annotation[] annotations) {
        for (int i = 0; i < annotations.length; ++i) {
            final Annotation annotation = annotations[i];
            if (annotation != null) {
                result.put(annotation.getClass().getInterfaces()[0], annotation);
            }
        }
    }
    
    public Annotation[] getAllAnnotations() {
        if (this.annotations == null) {
            this.annotations = this.getAllAnnotationsCalculated();
        }
        return this.annotations;
    }
    
    private Annotation[] getAllAnnotationsCalculated() {
        final Collection<Annotation> values = this.getAllAnnotationMap().values();
        return values.toArray(new Annotation[0]);
    }
    
    public AnnotatedClass getAnnotatedClass() {
        return this.annotatedClass;
    }
    
    public Annotation getAnnotation(final Class<?> annotationClass) {
        return this.getAllAnnotationMap().get(annotationClass);
    }
    
    public Method getMethod() {
        return this.method;
    }
}
