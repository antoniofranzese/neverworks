package it.neverworks.lang.annotation;

import java.util.*;

public class AnnotationManager
{
    private static Map<Class<?>, AnnotatedClass> classToAnnotatedMap;
    
    static {
        AnnotationManager.classToAnnotatedMap = new HashMap<Class<?>, AnnotatedClass>();
    }
    
    public static AnnotatedClass getAnnotatedClass(final Class<?> theClass) {
        AnnotatedClass annotatedClass = AnnotationManager.classToAnnotatedMap.get(theClass);
        if (annotatedClass == null) {
            annotatedClass = (AnnotatedClass)new AnnotatedClassImpl((Class)theClass);
            AnnotationManager.classToAnnotatedMap.put(theClass, annotatedClass);
        }
        return annotatedClass;
    }
}
