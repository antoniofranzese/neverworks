package it.neverworks.lang.annotation;

import java.lang.annotation.*;
import java.lang.reflect.*;
import java.util.*;

class AnnotatedClassImpl implements AnnotatedClass
{
    private final Class<?> theClass;
    private Map<Class<?>, Annotation> classToAnnotationMap;
    private Map<Method, AnnotatedMethod> methodToAnnotatedMap;
    private Annotation[] annotations;
    private AnnotatedMethod[] annotatedMethods;
    
    AnnotatedClassImpl(final Class<?> theClass) {
        this.classToAnnotationMap = null;
        this.methodToAnnotatedMap = null;
        this.annotations = null;
        this.annotatedMethods = null;
        this.theClass = theClass;
    }
    
    private Map<Class<?>, Annotation> getAllAnnotationMap() {
        if (this.classToAnnotationMap == null) {
            this.classToAnnotationMap = this.getAllAnnotationMapCalculated();
        }
        return this.classToAnnotationMap;
    }
    
    private Map<Class<?>, Annotation> getAllAnnotationMapCalculated() {
        final HashMap<Class<?>, Annotation> result = new HashMap<Class<?>, Annotation>();
        final Class<?> superClass = this.getTheClass().getSuperclass();
        if (superClass != null) {
            this.fillAnnotationsForOneClass(result, superClass);
        }
        final Class<?>[] interfaces = this.getTheClass().getInterfaces();
        for (int i = 0; i < interfaces.length; ++i) {
            final Class<?> c = interfaces[i];
            this.fillAnnotationsForOneClass(result, c);
        }
        final Annotation[] declaredAnnotations = this.getTheClass().getDeclaredAnnotations();
        for (int j = 0; j < declaredAnnotations.length; ++j) {
            final Annotation annotation = declaredAnnotations[j];
            result.put(annotation.getClass().getInterfaces()[0], annotation);
        }
        return result;
    }
    
    private void fillAnnotationsForOneClass(final HashMap<Class<?>, Annotation> result, final Class<?> baseClass) {
        this.addAnnotations(result, AnnotationManager.getAnnotatedClass((Class)baseClass).getAllAnnotations());
    }
    
    private void addAnnotations(final HashMap<Class<?>, Annotation> result, final Annotation[] annotations) {
        for (int i = 0; i < annotations.length; ++i) {
            final Annotation annotation = annotations[i];
            if (annotation != null) {
                if (result.containsKey(annotation.getClass().getInterfaces()[0])) {
                    result.put(annotation.getClass().getInterfaces()[0], null);
                }
                else {
                    result.put(annotation.getClass().getInterfaces()[0], annotation);
                }
            }
        }
    }
    
    public Class<?> getTheClass() {
        return this.theClass;
    }
    
    public Annotation[] getAllAnnotations() {
        if (this.annotations == null) {
            this.annotations = this.getAllAnnotationsCalculated();
        }
        return this.annotations;
    }
    
    private Annotation[] getAllAnnotationsCalculated() {
        return this.getAllAnnotationMap().values().toArray(new Annotation[0]);
    }
    
    public Annotation getAnnotation(final Class<?> annotationClass) {
        return this.getAllAnnotationMap().get(annotationClass);
    }
    
    private Map<Method, AnnotatedMethod> getMethodMap() {
        if (this.methodToAnnotatedMap == null) {
            this.methodToAnnotatedMap = this.getMethodMapCalculated();
        }
        return this.methodToAnnotatedMap;
    }
    
    private Map<Method, AnnotatedMethod> getMethodMapCalculated() {
        final HashMap<Method, AnnotatedMethod> result = new HashMap<Method, AnnotatedMethod>();
        final Method[] methods = this.getTheClass().getMethods();
        for (int i = 0; i < methods.length; ++i) {
            final Method method = methods[i];
            result.put(method, (AnnotatedMethod)new AnnotatedMethodImpl((AnnotatedClass)this, method));
        }
        return result;
    }
    
    public AnnotatedMethod getAnnotatedMethod(final Method method) {
        return this.getMethodMap().get(method);
    }
    
    public AnnotatedMethod[] getAnnotatedMethods() {
        if (this.annotatedMethods == null) {
            this.annotatedMethods = this.getAnnotatedMethodsCalculated();
        }
        return this.annotatedMethods;
    }
    
    private AnnotatedMethod[] getAnnotatedMethodsCalculated() {
        final Collection<AnnotatedMethod> values = this.getMethodMap().values();
        return values.toArray(new AnnotatedMethod[0]);
    }
    
    public AnnotatedMethod getAnnotatedMethod(final String name, final Class<?>[] parameterType) {
        try {
            return this.getAnnotatedMethod(this.getTheClass().getMethod(name, parameterType));
        }
        catch (SecurityException e) {
            throw new RuntimeException(e);
        }
        catch (NoSuchMethodException e2) {
            return null;
        }
    }
}
