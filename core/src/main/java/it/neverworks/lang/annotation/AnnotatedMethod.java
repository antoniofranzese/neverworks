package it.neverworks.lang.annotation;

import java.lang.reflect.*;
import java.lang.annotation.*;

public interface AnnotatedMethod
{
    AnnotatedClass getAnnotatedClass();
    
    Method getMethod();
    
    Annotation[] getAllAnnotations();
    
    Annotation getAnnotation(final Class<?> p0);
}
