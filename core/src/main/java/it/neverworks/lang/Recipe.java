package it.neverworks.lang;

import java.util.List;
import java.util.ArrayList;

public class Recipe {
    
    private List steps = new ArrayList();
    
    public Recipe map( Mapper mapper ) {
        this.steps.add( mapper );
        return this;
    }

    public Recipe filter( Filter filter ) {
        this.steps.add( filter );
        return this;
    }

    public Recipe inspect( Inspector inspector ) {
        this.steps.add( inspector );
        return this;
    }
    
    public <T> Functional<T> apply( Functional functional ) {
        Functional current = functional;
        for( Object step: steps ) {
            if( step instanceof Mapper ) {
                current = current.map( (Mapper) step );
            } else if( step instanceof Filter ) {
                current = current.filter( (Filter) step );
            } else {
                current.inspect( (Inspector) step );
            }
        }
        return current;
    }
    
    public <T> Functional<T> apply( Iterable iterable ) {
        return (Functional<T>) apply( new FunctionalIterable( iterable ) );
    }
    
    public <T> T apply( Object value ) {
        Object current = value;
        for( Object step: steps ) {
            if( current == null ) {
                return null;
            } else if( step instanceof Mapper ) {
                current = ((Mapper) step).map( current );
            } else if( step instanceof Filter ) {
                current = ((Filter) step).filter( current ) ? current : null;
            } else {
                ((Inspector) step).inspect( current );
            }
        }
        return (T) current;
    }
}