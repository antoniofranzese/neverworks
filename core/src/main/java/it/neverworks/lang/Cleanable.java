package it.neverworks.lang;

public interface Cleanable {
    void clean();
}