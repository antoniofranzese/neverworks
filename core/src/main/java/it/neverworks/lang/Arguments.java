package it.neverworks.lang;

import java.util.Map;
import java.util.HashMap;
import java.util.AbstractMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;
import java.util.HashSet;
import java.util.Collection;
import java.util.stream.Stream;

import java.io.Reader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

import it.neverworks.encoding.JSON;

public class Arguments extends HashMap<String, Object> implements FluentMap<String, Object>, Hatching {

    private FluentList<String> keys = new FluentArrayList<String>();
    private FluentList<Object> positionals;
    private boolean lenient = false;

    public static Arguments of( Object value ) {
        try {
            return process( value );
        } catch( IllegalArgumentException ex ) {
            return process( Strings.valueOf( value ) );
        }
    }
    
    public static Arguments process( Object value ) {
        if( value instanceof Arguments ) {
            return (Arguments) value;
        } else if( value instanceof Map ) {
            return new Arguments( (Map) value );
        } else if( value instanceof Iterable ) {
            return new Arguments( (Iterable) value );
        } else if( Collections.isListable( value ) ) {
            return new Arguments( Collections.list( value ) );
        } else if( value instanceof String ){
            return Arguments.parse( (String) value );
        } else if( value == null ) {
            return new Arguments();
        } else {
            throw new IllegalArgumentException( "Invalid arguments source: " + Objects.repr( value ) );
        }
    }

    public Arguments() {
        super();
    }

    public Arguments( Map value ) {
        this();
        load( value );
    }

    public Arguments( Iterable value ) {
        this();
        load( value );
    }

    public Arguments( Object... values ) {
        this();
        args( values );
    }

    public Arguments( Object value ) {
        this();
        if( Collections.isListable( value ) ) {
            args( Collections.list( value ) );
        } else if( value != null ){
            args( value );
        }
    }
    
    public Object hatch() {
        return new Arguments();
    }
    
    public Arguments lenient() {
        this.lenient = true;
        return this;
        
    }
    public Arguments arg( Object value ) {
        return args( (Object) value );
    }
    
    public Arguments arg( String key, Object value ) {
        if( containsKey( key ) && lenient ) {
            remove( key );
        }
        
        if( !containsKey( key ) ) {
            put( key, value );
            return this;
        } else {
            throw new IllegalArgumentException( "Duplicate argument: " + key );
        }
    }

    public Object put( String key, Object value ) {
        boolean addKey = !containsKey( key );
        Object o = super.put( key, value );
        if( addKey ) {
            keys.add( key );
        }
        return o;
    } 

    @Override
    public Object remove( Object key ) {
        if( containsKey( key ) ) {
            keys.remove( Strings.valueOf( key ) );
        }       
        return super.remove( key );
    }

    public boolean remove( Object key, Object value ) {
        if( containsKey( key ) ) {
            keys.remove( Strings.valueOf( key ) );
        }       
        return super.remove( key, value );
    }   

    public FluentList<String> keys() {
        return new FluentArrayList<String>( this.keys );
    }
    
    public FluentList<String> names() {
        return new FluentArrayList<String>( this.keys );
    }

    public FluentList<Integer> indexes() {
        if( positionals != null ) {
            return new FluentArrayList<Integer>( Range.of( positionals ) );
        } else {
            return new FluentArrayList<Integer>();
        }
    }
    
    public FluentList<Object> positionals() {
        if( positionals == null ) {
            positionals = new FluentArrayList<Object>();
        }
        return positionals;
    }
    
    public FluentList<Map.Entry<String, Object>> keywords() {
        FluentList<Map.Entry<String, Object>> result = new FluentArrayList<Map.Entry<String, Object>>();
        for( String key: this.keys ) {
            result.add( new AbstractMap.SimpleEntry<String,Object>( key, this.get( key ) ) );
        }
        return result;
    }

    public boolean has( String key ) {
        return containsKey( key );
    }

    public boolean has( int index ) {
        return positionals != null && positionals.size() > index;
    }

    public boolean has( Class type, String key ) {
        return has( key ) && ( type != null ? type.isInstance( get( key ) ) : true );
    }

    public boolean has( Class type, int index ) {
        return has( index ) && ( type != null ? type.isInstance( get( index ) ) : true );
    }

    public <T> T get( String key ) {
        return (T)super.get( key );
    }

    public <T> T get( int index ) {
        return has( index ) ? (T) positionals.get( index ) : null;
    }
    
    public <T> T get( Number index ) {
        return index != null ? ( has( index.intValue() ) ? (T) positionals.get( index.intValue() ) : null ) : null;
    }
    
    @Override
    public Object get( Object key ) {
        if( key != null ) {
            if( key instanceof String ) {
                return get( (String) key );
            } else if( key instanceof Number ) {
                return get( ((Number) key).intValue() );
            } else {
                return get( Strings.valueOf( key ) );
            }
        } else {
            return null;
        }
    }
    
    public <T> T get( String key, T defaultValue ) {
        if( containsKey( key ) ) {
            T value = (T)get( key );
            return value != null ? value : defaultValue;
        } else {
            return defaultValue;
        }
    }

    public <T> T get( int index, T defaultValue ) {
        if( has( index ) ) {
            return (T)get( index );
        } else {
            return defaultValue;
        }
    }
    
    public <T> T get( Class<T> type, String key ) {
        Object value = get( key );
        if( value == null || type.isInstance( value ) ) {
            return (T) value;
        } else {
            throw new IllegalArgumentException( Strings.message( "Wrong type for argument '{0}'<{1}>: {2}", key, type.getName(), Objects.repr( value ) ) );
        }
    
    }

    public <T> T get( Class<T> type, int index ) {
        Object value = get( index );
        if( value == null || type.isInstance( value ) ) {
            return (T) value;
        } else {
            throw new IllegalArgumentException( Strings.message( "Wrong type for argument [{0}]<{1}>: {2}", index, type.getName(), Objects.repr( value ) ) );
        }
    
    }
    
    public <T> T get( Class<T> type, String key, T defaultValue ) {
        Object value = get( key, defaultValue );
        if( value == null || type.isInstance( value ) ) {
            return (T) value;
        } else {
            throw new IllegalArgumentException( Strings.message( "Wrong type for argument '{0}'<{1}>: {2}", key, type.getName(), Objects.repr( value ) ) );
        }
    
    }

    public <T> T get( Class<T> type, int index, T defaultValue ) {
        Object value = get( index, defaultValue );
        if( value == null || type.isInstance( value ) ) {
            return (T) value;
        } else {
            throw new IllegalArgumentException( Strings.message( "Wrong type for argument [{0}]<{1}>: {2}", index, type.getName(), Objects.repr( value ) ) );
        }

    }
    
    public <T> T get( Mapper<Object,T> mapper, String key ) {
        return (T) process( mapper, key, null );
    }

    public <T> T get( Mapper<Object,T> mapper, int index ) {
        return (T) process( mapper, index, null );
    }
    
    public <T> T get( Mapper<Object,T> mapper, String key, T defaultValue ) {
        return (T) process( mapper, key, defaultValue );
    }

    public <T> T get( Mapper<Object,T> mapper, int index, T defaultValue ) {
        return (T) process( mapper, index, defaultValue );
    }
 
    protected <T> T process( Mapper mapper, String key, T defaultValue ) {
        if( has( key ) ) {
            return (T) mapper.map( get( key ) );
        } else {
            return defaultValue;
        }
    }

    protected <T> T process( Mapper mapper, int index, T defaultValue ) {
        if( hasPositionals() && positionals.size() > index ) {
            return (T) mapper.map( get( index ) );
        } else {
            return defaultValue;
        }
    }

    public Arguments set( String key, Object value ) {
        put( key, value );
        return this;
    }
    
    public Arguments set( int index, Object value ) {
        if( positionals == null ) {
            positionals = new FluentArrayList<Object>();
        }
        
        if( positionals.size() <= index ) {
            for( int i = positionals.size(); i <= index; i++ ) {
                positionals.add( i, null );
            }
        }
        
        positionals.set( index, value );
        return this;
    }
    
    public Arguments set( Arguments other ) {
        if( other != null ) {
            for( String key: other.keys() ) {
                this.set( key, other.get( key ) );
            }
        }
        return this;
    }
    
    public Arguments split( String... keys ) {
        return split( Collections.<String>iterable( keys ) );
    }
    
    public Arguments split( Iterable<String> keys ) {
        Arguments result = Objects.hatch( this );
        for( String key: keys ) {
            if( this.containsKey( key ) ) {
                result.put( key, this.get( key ) );
                this.remove( key );
            }
        }
        return result;
    }

    public Arguments split( Filter<String> filter ) {
        Arguments result = Objects.hatch( this );
        for( String key: this.keys() ) {
            if( filter.filter( key ) ) {
                result.put( key, this.get( key ) );
                this.remove( key );
            }
        }
        return result;
    }
    
    public Arguments copy( String... keys ) {
        return copy( Collections.<String>iterable( keys ) );
    }
    
    public Arguments copy( Iterable<String> keys ) {
        Arguments result = Objects.hatch( this );
        for( String key: keys ) {
            if( this.containsKey( key ) ) {
                result.put( key, get( key ) );
            }
        }
        return result;
    }

    public Arguments copy( Filter<String> filter ) {
        Arguments result = Objects.hatch( this );
        for( String key: this.keys() ) {
            if( filter.filter( key ) ) {
                result.put( key, get( key ) );
            }
        }
        return result;
    }

    public Arguments retain( String... keys ) {
        return retain( Collections.<String>iterable( keys ) );
    }
    
    public Arguments retain( Iterable<String> keys ) {
        for( String key: this.keys().discard( keys ) ) {
            this.remove( key );
        }
        return this;
    }

    public Arguments retain( Filter<String> filter ) {
        for( String key: this.keys() ) {
            if( ! filter.filter( key ) ) {
                this.remove( key );
            }
        }
        return this;
    }
    
    public Arguments discard( String... keys ) {
        return discard( Collections.<String>iterable( keys ) );
    }
    
    public Arguments discard( Iterable<String> keys ) {
        for( String key: keys ) {
            this.remove( key );
        }
        return this;
    }

    public Arguments discard( Filter<String> filter ) {
        for( String key: this.keys() ) {
            if( filter.filter( key ) ) {
                this.remove( key );
            }
        }
        return this;
    }

    public boolean hasPositionals() {
        return positionals != null && positionals.size() > 0 ;
    }

    public boolean hasKeywords() {
        return this.size() > 0 ;
    }
    
    public Arguments restrict( String... allows ) {
        Set keys = new HashSet( keySet() );
        keys.removeAll( Arrays.asList( allows ) );
        
        if( keys.size() > 0 ) {
            throw new IllegalArgumentException( "Arguments not allowed: " + keys.toString() );
        }
        
        return this;
    }
    
    public <T> T require( String key ) {
        if( ! has( key ) ) {
            throw new IllegalArgumentException( "Argument is required: " + key );
        } else {
            return (T) get( key );
        }
    }
    
    public <T> T require( Class<T> type, String key ) {
        if( ! has( key ) ) {
            throw new IllegalArgumentException( "Argument is required: " + key );
        } else {
            return (T) get( type, key );
        }
    }

    public <T> T require( Mapper<Object,T> mapper, String key ) {
        if( ! has( key ) ) {
            throw new IllegalArgumentException( "Argument is required: " + key );
        } else {
            return (T) get( mapper, key );
        }
    }

    public <T> T require( Class<T> type, int index ) {
        if( ! has( index ) ) {
            throw new IllegalArgumentException( "Argument " + index + " is required" );
        } else {
            return (T) get( type, index );
        }
    }

    public <T> T require( Mapper<Object,T> mapper, int index ) {
        if( ! has( index ) ) {
            throw new IllegalArgumentException( "Argument " + index + " is required" );
        } else {
            return (T) get( mapper, index );
        }
    }
    
    public <T> T populate( Class<T> targetClass ) {
        return (T) populate( Reflection.newInstance( targetClass ) );
    }
    
    public <T> T populate( T target ) {
        for( String key: keys ) {
            Properties.set( target, key, get( key ) );
        }
        return target;
    }
    
    public Arguments args( Object... values ) {
        if( positionals == null ) {
            positionals = new FluentArrayList<Object>();
        }
        for( Object value: values ) {
            positionals.add( value );
        }
        return this;
    }

    public Arguments args( Arguments values ) {
        if( values != null ) {
            for( String key: values.keys() ) {
                arg( key, values.get( key ) );
            }
        }
        return this;
    }

    
    public Arguments copy() {
        Arguments result = Objects.hatch( this );
        if( hasKeywords() ) {
            for( String key: keys() ) {
                result.arg( key, get( key ) );
            }
        }
        if( hasPositionals() ) {
            for( Object obj: positionals ) {
                result.arg( obj );
            }
        }
        return result;
    }
    
    public String toString() {
        FluentList<String> snippets = new FluentArrayList<String>();
        boolean qualify = hasKeywords() && hasPositionals();
        if( hasKeywords() ) {
            snippets.add( ( qualify ? "keywords: " : "" ) + this.names().map( n -> Strings.message( "{}={}", n, this.get( n ) ) ).join( ", " ) );
        }
        if( hasPositionals() ) {
            snippets.add( ( qualify ? "positionals: " : "" ) + positionals.toString() );
        }
        
        return this.getClass().getSimpleName() + "(" + snippets.join( ", " ) + ")";
    }
    
    public String format() {
        return format( '=', ',' );
    }
    
    public String json() {
        return JSON.encode( this ).toString();
    }
    
    public String format( char assignment, char separator ) {
        StringBuilder result = new StringBuilder();
        String stringSeparator = new String( new char[]{ separator } );
        String quotedSeparator = new String( new char[]{ separator, separator } );
        if( hasKeywords() ) {
            boolean started = false;
            for( String key: keys() ) {
                if( started ) {
                    result.append( separator );
                } else {
                    started = true;
                }
                result.append( key ).append( assignment ).append( Strings.safe( Strings.valueOf( get( key ) ) ).replace( stringSeparator, quotedSeparator ) );
            }
        }
        return result.toString();
    }

    public static boolean isParsable( Object value ) {
        return isParsable( value, '=' );
    }
    
    public static boolean isParsable( Object value, char assignment ) {
        if( value instanceof String ) {
            int len = ((String) value).length();

            if( len < 3 ) {
                return false;
            }

            char[] chars = Strings.raw( (String) value );

            if( chars[ 0 ] == '{' ) {
                return true;
            }
            
            for( int i = 1; i < len; i++ ) {
                if( chars[ i ] == assignment ) {
                    return true;
                }
            }
            
            return false;
        
        } else {
            return false;
        }
    }

    public static Arguments parse( Object specification ) {
        return parse( new Arguments().lenient(), specification );
    }

    protected static Arguments parse( Arguments result, Object specification ) {
        if( specification instanceof String ) {
            return parse( result, (String) specification );
        } else {
            throw new IllegalArgumentException( "Unparsable value: " + Objects.repr( specification ) );
        }
    }

    public static Arguments parse( String specification ) {
        return parse( new Arguments().lenient(), specification );
    }

    protected static Arguments parse( Arguments result, String specification ) {
        if( specification.startsWith( "{" ) ) {
            ArgumentsProperties.loadJson( result, JSON.relaxed.decode( specification ) );
            return result;
        } else {
            return parse( specification, '=', ',' );
        }
    }

    public static Arguments parse( String specification, char assignment, char separator ) {
        return parse( new Arguments().lenient(), specification, assignment, separator );
    }
    
    protected static Arguments parse( Arguments result, String specification, char assignment, char separator ) {
        String stringSeparator = new String( new char[]{ separator } );
        String quotedSeparator = new String( new char[]{ separator, separator } );

        if( specification != null ) {
            
            char[] chars = Strings.raw( specification );
            int start = 0;
            int length = 0;
            boolean inValue = false;
            boolean collect = false;
            boolean quote = false;
            boolean quoted = true;
            boolean terminated = false;
            String name = null;
        
        
            for( int index = 0; index < chars.length; index++ ) {
                if( chars[ index ] == assignment ) {
                    if( name == null && length > 0 ) {
                        collect = true;
                    } else {
                        if( inValue ){
                            length += 1;
                        } else {
                            throw new IllegalArgumentException( "Invalid '" + assignment + "' at " + ( index + 1 ) + ": " + specification );
                        }
                    }
                } else if( chars[ index ] == separator ) {
                    if( quote ) {
                        quote = false;
                        quoted = true;
                        length += 1;
                    } else {
                        if( index < chars.length - 1 && chars[ index + 1 ] == separator ) {
                            quote = true;
                            length += 1;
                        } else {
                            collect = true;
                            terminated = true;
                        }
                    }
                } else {
                    length += 1;
                }
            
                if( collect || index == chars.length - 1 ) {
                    String text = quoted ? new String( chars, start, length ).replace( quotedSeparator, stringSeparator ) : new String( chars, start, length );
                
                    if( inValue ) {
                        result.arg( name, text );
                        name = null;
                        inValue = false;
                    } else {
                        if( terminated || index == chars.length - 1 ) {
                            result.arg( text.trim() );
                        } else {
                            name = text.trim();
                            inValue = true;
                        }
                    }
                    length = 0;
                    collect = false;
                    quoted = false;
                    terminated = false;
                    start = index + 1;
                }
            }
        }
        
        return result;
    }

    protected Iterable keys( Map map ) {
        if( map instanceof FluentMap ) {
            return ((FluentMap) map).keys();
        } else if( map != null ) {
            return map.keySet();
        } else {
            return new ArrayList();
        }
    }

    public Arguments merge( Map other ) {
        if( other != null ) {
            for( Object key: keys( other ) ) {
                String k = String.valueOf( key );
                if( ! containsKey( k ) ) {
                    this.arg( k, other.get( key ) );
                }
            }
        }
        return this;
    }

    public Arguments extend( Map other ) {
        Arguments result = this.copy();
        if( other != null ) {
            for( Object key: keys( other ) ) {
                result.arg( Strings.valueOf( key ), other.get( key ) );
            }
        }
        return result;
    }

    public Arguments load( Map map ) {
        for( Object key: keys( map ) ) {
            put( Strings.valueOf( key ), map.get( key ) );
        }
        return this;
    }

    public Arguments load( Iterable value ) {
        for( Object item: value ) {
            args( item );
        }
        return this;
    }
    
    public static Arguments load( InputStream stream ) {
        return load( new Arguments(), stream );
    }

    public static Arguments load( Reader reader ) {
        return load( new Arguments(), reader );
    }
    
    private static class ArgumentsProperties extends MultiFormatProperties {
        
        private Arguments arguments;
        
        public ArgumentsProperties( Arguments arguments ) {
            super();
            this.arguments = arguments;
        }
        
        @Override
        protected void loadJson( List<String> lines ) {
            loadJson( this.arguments, JSON.relaxed.decode( Collections.join( lines, " " ) ) );
        }
        
        public static void loadJson( Arguments result, Object json ) {
            if( json instanceof FluentMap ) {
                for( Object key: ((FluentMap) json).keys() ) {
                    result.arg( Strings.valueOf( key ), ((FluentMap) json).get( key ) );
                }
            } else if( json instanceof List ) {
                for( Object item: ((List) json )) {
                    result.arg( item );
                }
            } else if( json instanceof Map ) {
                for( Object key: ((Map) json).keySet() ) {
                    result.arg( Strings.valueOf( key ), ((Map) json).get( key ) );
                }
            } else {
                throw new UnsupportedFeatureException( "Unsupported decoded JSON type: " + Objects.repr( json ) );
            }
        }
    
        @Override
        protected void loadProperties( List<String> lines ) {
            for( String line: lines ) {
                int index = line.indexOf( "=" );
                if( index > 0 ) {
                    this.arguments.arg( line.substring( 0, index ).trim(), line.substring( index + 1 ).trim() );
                } else if( index < 0 ) {
                    this.arguments.arg( line );
                }
            }
        }
        
    }
    
    private static Arguments load( Arguments result, InputStream stream ) {
        if( stream != null ) {
            new ArgumentsProperties( result ).load( stream );
        }
        return result;
    }

    private static Arguments load( Arguments result, Reader reader ) {
        if( reader != null ) {
            new ArgumentsProperties( result ).load( reader );
        }
        return result;
    }
    
    
}