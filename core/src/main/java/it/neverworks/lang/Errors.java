package it.neverworks.lang;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class Errors {

	public static void lower( Throwable throwable ) {
        if( throwable != null ) {
    		throw wrap( throwable );
        }
	}

	public static RuntimeException wrap( Throwable throwable ) {
        return runtime( unwrapThrowable( throwable ) );
	}

	public static RuntimeException runtime( Throwable throwable ) {
		if( throwable instanceof RuntimeException ) {
			return (RuntimeException)throwable;
		} else {
			WrappedException ex = new WrappedException( throwable );
			ex.fillInStackTrace();
			return ex;
		}
	}
    
    public static class WrapGuard implements Guard {

        public RuntimeException wrap( Throwable throwable ) {
            return Errors.wrap( throwable );
        }

    	public void lower( Throwable throwable ) {
            Errors.lower( throwable );
    	}

    }
    
    public static WrapGuard wrap() {
        return new WrapGuard();
    }
    
    public static class SpyGuard extends WrapGuard {
        
        private Handler handler;
        
        public SpyGuard( Handler handler ) {
            this.handler = handler;
        }
        
        public void error( Throwable error ) {
            Exception unwrapped = unwrap( error );
            try {
                this.handler.handle( unwrapped );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
            throw Errors.wrap( unwrapped );
        }
        
    }
    
    public static SpyGuard spy( Handler handler ) {
        return new SpyGuard( handler );
    }

    public static class TrapGuard extends WrapGuard {
        
        private Handler handler;
        
        public TrapGuard( Handler handler ) {
            this.handler = handler;
        }
        
        public void error( Throwable error ) {
            Exception unwrapped = unwrap( error );
            try {
                this.handler.handle( unwrapped );
            } catch( Exception ex ) {
                throw Errors.wrap( ex );
            }
        }
        
    }

    public static TrapGuard trap( Handler handler ) {
        return new TrapGuard( handler );
    }

    public static interface Handler {
        public void handle( Exception exception ) throws Exception;
    }
    
	public static Throwable unwrapThrowable( Throwable throwable ) {

		Throwable result = throwable;

		while ( result instanceof InvocationTargetException || 
				result instanceof UndeclaredThrowableException || 
				result instanceof WrappedException ) {

			if ( result instanceof UndeclaredThrowableException ) {
				result = ( (UndeclaredThrowableException) result ).getUndeclaredThrowable();

			} else {
				result = result.getCause();
			}

		}
        
		return result;
		
	}	
    

    public static Exception unwrap ( Throwable throwable ) {
        return convert( unwrapThrowable( throwable ) );
    }
	
	public static Throwable[] unfoldThrowable( Throwable throwable ) {

		List<Throwable> folds = new ArrayList<Throwable>();
		Throwable result = throwable;
		folds.add( 0, result );
		
		while ( result instanceof InvocationTargetException || 
				result instanceof UndeclaredThrowableException || 
				result.getCause() != null ) {

			if ( result instanceof InvocationTargetException ) {
				result = ( (InvocationTargetException) result ).getCause();
				
			} else if ( result instanceof UndeclaredThrowableException ) {
				result = ( (UndeclaredThrowableException) result ).getUndeclaredThrowable();

			} else {
				result = result.getCause();
			}
			folds.add( 0, result );
		}
		return folds.toArray( new Throwable[ folds.size() ] );
		
	}	
    
	public static Exception[] unfold( Throwable throwable) {
		Throwable[] throwables = unfoldThrowable( throwable );
		Exception[] exceptions = new Exception[ throwables.length ];
		for( int i = 0; i < throwables.length; i++ ) {
			exceptions[ i ] = convert( throwables[ i ] );
		}
		return exceptions;
	}
	
	public static Exception convert( Throwable throwable ) {
		if ( throwable instanceof Exception ) {
			return (Exception) throwable;
		} else {
			return new Exception( throwable );
		}
	}
	
	public static void printf( String message, Object... args ) {
		throw format( message, args );
	}

	public static RuntimeException format( String message, Object... args ) {
		StringBuilder builder = new StringBuilder();
		Formatter formatter = new Formatter( builder );
		formatter.format( message, args );
		return new RuntimeException( builder.toString() );
	}
	
}
