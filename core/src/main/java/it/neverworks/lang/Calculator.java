package it.neverworks.lang;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;
import java.util.ArrayList;
import java.text.NumberFormat;
import java.text.DecimalFormat;

public class Calculator {
    
    private List<BigDecimal> stack = new ArrayList<BigDecimal>();
    private int scale = 0;
    private RoundingMode rounding = RoundingMode.HALF_UP;
    
    public Calculator(){

    }
    
    public Calculator( int scale ) {
        this.scale = scale;
    }
    
    public Calculator( int scale, RoundingMode rounding ) {
        this.scale = scale;
        this.rounding = rounding;
    }
    
    public Calculator push( Object value ) {
        if( value != null ) {
            stack.add( Numbers.decimal( value ) );
        } else {
            stack.add( BigDecimal.ZERO );
        }
        return this;
    }
    
    public BigDecimal peek() {
        if( stack.size() > 0 ) {
            return stack.get( stack.size() - 1 );
        } else {
            throw new IllegalStateException( "Empty number stack" );
        }
    }

    public BigDecimal pop() {
        BigDecimal value = peek();
        stack.remove( stack.size() - 1 );
        return value;
    }
    
    public Calculator add() {
        if( stack.size() >= 2 ) {
            BigDecimal second = pop();
            BigDecimal first = pop();
            push( first.add( second ) );
        } else {
            throw new IllegalStateException( "Cannot add, insufficient stack size" );
        }
        return this;
    }
    
    public Calculator add( Object value ) {
        push( value );
        return add();
    }

    public Calculator sub() {
        if( stack.size() >= 2 ) {
            BigDecimal second = pop();
            BigDecimal first = pop();
            push( first.subtract( second ) );
        } else {
            throw new IllegalStateException( "Cannot subtract, insufficient stack size" );
        }
        return this;
    }
    
    public Calculator sub( Object value ) {
        push( value );
        return sub();
    }
    
    public Calculator mul() {
        if( stack.size() >= 2 ) {
            BigDecimal second = pop();
            BigDecimal first = pop();
            push( first.multiply( second ) );
        } else {
            throw new IllegalStateException( "Cannot multiply, insufficient stack size" );
        }
        return this;
    }
    
    public Calculator mul( Object value ) {
        push( value );
        return mul();
    }

    public Calculator div() {
        if( stack.size() >= 2 ) {
            BigDecimal second = pop();
            BigDecimal first = pop();
            try {
                push( first.divide( second, MathContext.UNLIMITED ) );
            } catch( ArithmeticException ex ) {
                push( first.divide( second, MathContext.DECIMAL128 ) );
            }
        } else {
            throw new IllegalStateException( "Cannot divide, insufficient stack size" );
        }
        return this;
    }
    
    public Calculator div( Object value ) {
        push( value );
        return div();
    }
    
    public Calculator rem() {
        if( stack.size() >= 2 ) {
            BigDecimal second = pop();
            BigDecimal first = pop();
            push( first.divideAndRemainder( second )[ 1 ] );
        } else {
            throw new IllegalStateException( "Cannot get remainder, insufficient stack size" );
        }
        return this;
    }
    
    public Calculator rem( Object value ) {
        push( value );
        return rem();
    }

    public Calculator mod() {
        return rem();
    }
    
    public Calculator mod( Object value ) {
        return rem( value );
    }

    public BigDecimal r() {
        return result();
    }
    
    public BigDecimal result() {
        return result( this.scale, this.rounding );
    }

    public BigDecimal result( int scale ) {
        return result( scale, this.rounding );
    }

    public BigDecimal result( int scale, RoundingMode rounding ) {
        if( scale > 0 ) {
            return peek().setScale( scale, rounding );
        } else {
            return peek();
        }
    }
    
    public <T extends Number> T to( Class<T> type ) {
        return to( type, this.scale, this.rounding );
    }

    public <T extends Number> T to( Class<T> type, int scale ) {
        return to( type, scale, this.rounding );
    }
    
    public <T extends Number> T to( Class<T> type, int scale, RoundingMode rounding ) {
        if( type != BigDecimal.class ) {
            return (T) Numbers.convert( result( scale, rounding ), type );
        } else {
            return (T) result( scale, rounding );
        }
    }
    
    public int toInt() {
        return to( Integer.class );
    }

    public long toLong() {
        return to( Long.class );
    }

    public short toShort() {
        return to( Short.class );
    }
    
    public byte toByte() {
        return to( Byte.class );
    }
    
    public float toFloat() {
        return to( Float.class );
    }

    public double toDouble() {
        return to( Double.class );
    }
    
    public Calculator clear() {
        return c();
    }
    
    public Calculator c() {
        stack.clear();
        return this;
    }
    
    public Calculator ce() {
        pop();
        return this;
    }
    
    public String format( String pattern ) {
        return format( new DecimalFormat( pattern ) );
    }

    public String format( NumberFormat formatter ) {
        return formatter.format( result() );
    }

}