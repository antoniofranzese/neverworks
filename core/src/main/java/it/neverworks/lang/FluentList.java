package it.neverworks.lang;

import java.util.List;
import java.util.Iterator;
import java.util.Collection;
import java.util.Enumeration;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import java.lang.reflect.Array;

public interface FluentList<T> extends List<T>, Functional<T> {
    
    static <I> FluentList<I> of( I... values ) {
        return new FluentArrayList<I>().cat( values );
    }

    default String join( String delimiter ) {
        return Collections.join( this, delimiter );
    }

    default <O> O[] array( Class<O> target ) {
        O[] a = (O[]) Array.newInstance( target, this.size() );
        for( int i = 0; i < this.size(); i++ ) {
            a[ i ] = (O) this.get( i );
        }
        return a;
    }

    
    default <O> FluentList<O> as( Class<O> other ) {
        return (FluentList<O>) this;
    }
    
    default FluentList<T> cat( Iterable<T> iterable ) {
        if( iterable != null ) {
            for( T o: iterable ) {
                this.add( o );
            }
        }
        return this;
    }

    default FluentList<T> cat( Iterator<T> iterator ) {
        if( iterator != null ) {
            while( iterator.hasNext() ) {
                this.add( iterator.next() );
            }
        }
        return this;
    }

    default FluentList<T> cat( T[] array ) {
        if( array != null ) {
            for( T o: array ) {
                this.add( o );
            }
        }
        return this;
    }

    default FluentList<T> cat( Enumeration<T> enumeration) {
        if( enumeration != null ) {
            while( enumeration.hasMoreElements() ) {
                this.add( enumeration.nextElement() );
            }
        }
        return this;
    }

    default FluentList<T> cat( Object obj ) {
        if( obj != null ) {
            if( Collections.isListable( obj ) ) {
                for( T o: Collections.<T>toList( obj ) ) {
                    this.add( o );
                }
            } else {
                this.add( (T) obj );
            }
        }
        return this;
    }

    default FluentList<T> append( T... items ) {
        for( T o: items ) {
            if( o != null ) {
                this.add( o );
            }
        }
        return this;
    }
    
    default FluentList<T> discard( Filter<T> filter ) {
        for( int i = this.size() - 1; i >= 0; i-- ) {
            if( filter.filter( this.get( i ) ) ) {
                this.remove( i );
            }
        }
        return this;
    }
    
    default FluentList<T> discard( Iterable<T> iterable ) {
        for( T item: iterable ) {
            remove( item );
        }
        return this;
    }
    
    default FluentList<T> discard( T[] array ) {
        for( T item: array ) {
            remove( item );
        }
        return this;
    }
    
    default FluentList<T> discard( Object obj ) {
        remove( obj );
        return this;
    }
    
    default FluentList<T> put( int index, T value ) {
        if( index > this.size() -1 ) {
            synchronized( this ) {
                while( index > this.size() - 1 ) {
                    this.add( null );
                }
            }
        }
        this.set( index, value );
        return this;
    }
    
    default T head() {
        if( this.size() > 0 ) {
            return get( 0 );
        } else {
            return null;
        }
    }

    default FluentList<T> head( int limit ) {
        int max = Math.min( limit, this.size() );
        FluentList<T> result = new FluentArrayList<T>();
        if( max > 0 ) {
            for( int i = 0; i < max; i++ ) {
                result.add( this.get( i ) );
            }
        }
        return result;
    }

    default T tail() {
        if( this.size() > 0 ) {
            return get( this.size() - 1 );
        } else {
            return null;
        }
    }
    
    default FluentList<T> tail( int limit ) {
        int max = Math.min( limit, this.size() );
        FluentList<T> result = new FluentArrayList<T>();
        if( max > 0 ) {
            for( int i = max; i > 0; i-- ) {
                result.add( this.get( this.size() - i ) );
            }
        }
        return result;
    }

    default FluentList<T> reverse() {
        FluentList<T> result = new FluentArrayList<T>();
        for( int i = this.size(); i > 0; i-- ) {
            result.add( this.get( i - 1 ) );
        }
        return result;
    }

    default FluentList<T> flatten() {
        return this.flatten( (Filter) null );
    }

    default FluentList<T> flatten( Filter<T> filter ) {
        return Collections.<T>flatten( filter, this );
    }

    default <O> FluentList<O> flatten( Class<O> cls ) {
        return Collections.<O>flatten( Filters.instance( cls ), this );
    }
    
    public Stream<T> stream();
    
    public static <T> Stream<T> stream( Iterable<T> iterable ) {
        return StreamSupport.stream( iterable.spliterator(), /* parallel */ false );
    }

    public static <T> FluentList<T> list( Iterator<T> iterator ) {
        return new FluentArrayList<T>().cat( iterator );
    }
    
    public static <T> FluentList<T> list( Iterable<T> iterable ) {
        if( iterable instanceof FluentList ) {
            return (FluentList<T>)iterable;
        } else if( iterable instanceof Collection ) {
            return new FluentArrayList<T>( (Collection<T>) iterable );
        } else {
            return new FluentArrayList<T>().cat( iterable );
        }
    }
    
    public static <O> O[] array( Iterable iterable, Class<O> target ) {
        if( iterable instanceof FluentList ) {
            return (O[]) ((FluentList)iterable).array( target );
        } else if( iterable instanceof Collection ) {
            O[] array = (O[]) Array.newInstance( target, ((Collection) iterable).size() );
            return (O[]) ((Collection<O>) iterable).toArray( array );
        } else {
            List<O> l = Collections.<O>list( iterable );
            return (O[]) l.toArray( (O[]) Array.newInstance( target, l.size() ) );
        }
    }
    
    
}