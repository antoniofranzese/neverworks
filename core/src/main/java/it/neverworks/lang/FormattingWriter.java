package it.neverworks.lang;

import java.io.PrintWriter;
import java.io.File;
import java.io.Writer;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

import it.neverworks.lang.Range;

public class FormattingWriter {

    private boolean autoFlush = true;
    private PrintWriter out;
    
    public FormattingWriter( File file ) {
        try {
            this.out = new PrintWriter( file );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }
    
    public FormattingWriter( File file, String csn ) throws FileNotFoundException, UnsupportedEncodingException {
        try {
            this.out = new PrintWriter( file, csn );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public FormattingWriter( OutputStream out ) {
        this.out = new PrintWriter( out );
    }
    
    public FormattingWriter( OutputStream out, boolean autoFlush ) {
        this.out = new PrintWriter( out );
        this.autoFlush = autoFlush;
    }

    public FormattingWriter( String fileName ){
        try {
            this.out = new PrintWriter( fileName );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public FormattingWriter( String fileName, String csn ) {
        try {
            this.out = new PrintWriter( fileName, csn );
        } catch( Exception ex ) {
            throw Errors.wrap( ex );
        }
    }

    public FormattingWriter( Writer out )  {
        this.out = new PrintWriter( out );
    }
    
    public FormattingWriter( Writer out, boolean autoFlush )  {
         this.out = new PrintWriter( out, autoFlush );
         this.autoFlush = autoFlush;
    }

    private void printObjects( Object... objects ) {
        for( int i: Range.of( objects ) ) {
            if( i != 0 ) {
                out.print( " " );
            }
            out.print( objects[ i ] );
        }
    }
    
    public void print( Object... objects ) {
        this.printObjects( objects );
        if( autoFlush ) {
            this.flush();
        }
    }

    public void println( Object... objects ) {
        this.printObjects( objects );
        out.println( "" );
        if( autoFlush ) {
            this.flush();
        }
    }

    public void printm( String message, Object... objects ) {
        out.print( Strings.hasText( message ) ? Strings.message( message, objects ) : message );
        if( autoFlush ) {
            this.flush();
        }
    }
    
    public void printf( String format, Object... objects ) {
        out.printf( format, objects );
    }
    
    public void flush() {
        out.flush();
    }

}