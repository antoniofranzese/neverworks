package it.neverworks.lang;

public class WrappedException extends RuntimeException {
    public WrappedException( Throwable cause ) {
        super( cause );
    }
}
