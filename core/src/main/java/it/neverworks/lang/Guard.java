package it.neverworks.lang;

public interface Guard<T> {
    
    default T enter(){
        return (T) this;
    }
    
    default Throwable exit( Throwable error ){
        return error;
    }
    
    default void error( Throwable error ) {
        Errors.lower( error );
    }

    default void surround( Work<T> work ){
        Throwable error = null;
        try {
            try {
                work.doWithinWork( this.enter() );
            } catch( Exit ex ) {
                // Nothing to do
            }
            error = this.exit( null );
        } catch( Break br ) {
            // Nothing to do
        } catch( Throwable th ) {
            error = this.exit( th );
        }
        if( error != null ) {
            this.error( error );
        }
    }
    
    default <O> O extract( ReturningWork<T,O> work ){
        Throwable error = null;
        O value = null;
        try {
            try {
                value = work.returnFromWork( this.enter() );
            } catch( Exit ex ) {
                // Nothing to do
            }
            error = this.exit( null );
        } catch( Break br ) {
            // Nothing to do
        } catch( Throwable th ) {
            error = this.exit( th );
        }
        if( error != null ) {
            this.error( error );
        } 
        return value;
    }

    public class Exit extends Break {}
    public class End extends Break {}
        
}