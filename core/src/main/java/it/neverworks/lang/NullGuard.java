package it.neverworks.lang;

public class NullGuard<T> implements Guard<T> {
    
    public void surround( Work<T> work ) {}
}