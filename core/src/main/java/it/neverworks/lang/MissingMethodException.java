package it.neverworks.lang;

public class MissingMethodException extends RuntimeException {
    
    private String method;
    private Class source;
    
    public MissingMethodException( String message, Class source, String method ) {
        super( message );
        this.source = source;
        this.method = method;
    }

    public MissingMethodException( String message, Object source, String property ) {
        this( message, source != null ? source.getClass() : (Class) null, property );
    }

    public MissingMethodException( Class source, String method ) {
        this( Strings.message( "Missing ''{0}'' method in {1}", method, source.getSimpleName() ), source, method );
    }
    
    public MissingMethodException( Object source, String method ) {
        this( source.getClass(), method );
    }

    public Class getSource(){
        return this.source;
    }
    
    public String getMethod(){
        return this.method;
    }
    
}