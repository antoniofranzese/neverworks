package it.neverworks.cache;

import it.neverworks.lang.Guard;
import it.neverworks.lang.Wrapper;
import it.neverworks.model.utils.ToStringBuilder;

public class CacheProxy<T> implements Cache<T> {
    
    private Cache<T> actual = null;
    private String name = null;
    
    public CacheProxy( String name ) {
        this.name = name;
    }
    
    public CacheProxy( String name, Cache<T> actual ) {
        this( name );
        this.actual = actual;
    }
    
    public boolean contains( String key ) {
        return actual().contains( key );
    }
    
    public T get( String key ){
        return (T) actual().get( key );
    }
    
    public void put( String key, T value ) {
        actual().put( key, value );
    }

    public <T extends Cache> T set( String name, Object value ) {
        actual().set( name, value );
        return (T)this;
    }
    
    public void clear() {
        actual.clear();
    }
    
    public T remove( String key ) {
        return (T) actual.get( key );
    }
    
    public Guard<Cache<T>> missing( String key ) {
        return actual().missing( key );
    }
    
    public Cache<T> create() {
        return (Cache<T>) actual().create();
    }

    public T probe( String key, Wrapper initializer ) {
        return (T) actual().probe( key, initializer );
    }

    public T probe( String key, CacheKeyInitializer initializer ) {
        return (T) actual().probe( key, initializer );
    }
    
    protected Cache actual() {
        if( this.actual == null ) {
            synchronized( this ) {
                if( this.actual == null && this.name != null && CacheManager.has( this.name ) ) {
                    this.actual = CacheManager.retrieveCache( name );
                }
            }
        }
        return this.actual != null ? this.actual : NullCache.INSTANCE;
    }
    
    void setActual( Cache<T> actual ){
        this.actual = actual;
    }
    
    public int size() {
        if( actual == null ) {
            return 0;
        } else {
            return actual.size();
        }
    }

    public String toString() {
        return new ToStringBuilder( this )
            .add( "to", this.actual )
            .toString();
    }
}