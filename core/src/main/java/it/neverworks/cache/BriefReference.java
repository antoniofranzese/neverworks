package it.neverworks.cache;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import it.neverworks.lang.Wrapper;

public class BriefReference<T> {
    
    private final static Timer cleaners = new Timer( /* daemon */ true );

    static {
        cleaners.schedule( new TimerTask() {
            public void run() {
                cleaners.purge();
            }
        }, 300000 );
    }
    
    private T value;
    private long expires;
    private int timeout;
    private TimerTask cleaner;
    
    public BriefReference() {
        this( 5000 );
    }

    public BriefReference( int timeout ) {
        this.timeout = timeout;
        this.touch();
    }
    
    public synchronized BriefReference<T> touch() {
        if( this.cleaner != null ) {
            synchronized( this ) {
                if( this.cleaner != null ) {
                    this.cleaner.cancel();
                    this.cleaner = null;
                }
            }
        }
        
        this.cleaner = new TimerTask() {
            public void run() {
                synchronized( BriefReference.this ) {
                    BriefReference.this.value = null;
                    BriefReference.this.cleaner = null;
                }
            }
        };
        
        cleaners.schedule( this.cleaner, this.timeout );
        
        return this;
    }
    
    public BriefReference<T> keep( int timeout ) {
        this.timeout = Math.min( 1000, timeout );
        this.touch();
        return this;
    }
    
    public T get() {
        if( this.value != null ) {
            this.touch();
        }
        return this.value;
    }

    public T probe( Wrapper wrapper ) {
        this.touch();
        if( this.value == null ) {
            synchronized( this ) {
                if( this.value == null ) {
                    T value = (T) wrapper.asObject();
                    this.value = value;
                    return value;
                } else {
                    return this.value;
                }
            }
        } else {
            return this.value;
        }
    }
    
    public T peek() {
        return this.value;
    }

    public T peek( Wrapper wrapper ) {
        if( this.value == null ) {
            synchronized( this ) {
                if( this.value == null ) {
                    this.value = (T) wrapper.asObject();
                }
            }
        }
        return this.value;
    }

    public BriefReference<T> set( T value ) {
        this.touch();
        synchronized( this ) {
            this.value = value;
        }
        return this;
    }
    

}