package it.neverworks.cache;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.lang.Guard;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.FluentMap;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.FluentHashMap;
import it.neverworks.model.AbstractModel;
import it.neverworks.model.Property;
import it.neverworks.model.utils.ToStringBuilder;

public class SimpleLRUCache<T> extends AbstractModel implements Cache<T> {
    
    protected FluentMap<String, Element> elements = new FluentHashMap<String, Element>();
    protected List<String> access = new ArrayList<String>();
    protected int max; 
    protected int ttl = -1;
    
    protected class Element<T> {
        private long last;
        private T value;
        
        public Element( T value ) {
            this.value = value;
            this.last = new Date().getTime();
        }
    }
    
    public SimpleLRUCache() {
        this( 1000 );
    }
    
    public SimpleLRUCache( int max ) {
        super();
        this.max = max;
    }

    protected long now() {
        return new Date().getTime();
    }
    
    public boolean contains( String key ) {
        return elements.containsKey( key ) && ! isExpired( elements.get( key ) );
    }
    
    protected boolean isExpired( Element element ) {
        return this.ttl > 0 && ( now() - element.last ) > this.ttl;
    }
    
    public T get( String key ) {
        if( elements.containsKey( key ) ) {
            Element element = elements.get( key );
            if( isExpired( element ) ) {
                remove( key );
                return null;
            } else {
                touch( key );
                return (T)element.value;
            }
        } else {
            return null;
        }
    }
    
    public void put( String key, T value ) {
        if( elements.size() >= max ) {
            removeAll( elements.keys().filter( k -> isExpired( elements.get( k ) ) ).list() );
        }
        
        if( elements.size() >= max ) {
            removeAll( access.subList( max - 1, access.size() ) );
        }

        touch( key );
        elements.put( key, new Element( value ) );
    }
    
    protected void removeAll( List<String> keys ) {
        for( String deletable: keys ) {
            elements.remove( deletable );
            access.remove( deletable );
        }
    }
    
    protected void touch( String key ) {
        int idx = access.indexOf( key );
        if( idx >= 0 ) {
            access.remove( idx );
        }
        
        access.add( 0, key );
        
        if( elements.containsKey( key ) ) {
            elements.get( key ).last = now();
        }
    }
    
    public int size() {
        return elements.size();
    }
    
    public int getMax(){
        return this.max;
    }
    
    public void setMax( int max ){
        this.max = max;
    } 
    
    public int getTtl(){
        return this.ttl / 1000;
    }
    
    public void setTtl( int ttl ){
        this.ttl = ttl * 1000;
    }
    
    public void clear() {
        this.elements.clear();
        this.access.clear();
    }
    
    public T remove( String key ) {
        this.access.remove( key );
        return (T) elements.remove( key );
    }

    public Cache<T> create() {
        SimpleLRUCache created = (SimpleLRUCache) Reflection.newInstance( this.getClass() );
        created.ttl = this.ttl;
        created.max = this.max;
        return (Cache<T>) created;
    }
    
    public <T extends Cache> T set( String name, Object value ) {
        this.modelInstance.assign( name, value );
        return (T)this;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .add( "current", this.elements.size() )
            .add( "max", this.max )
            .add( "ttl", this.getTtl() )
            .toString();
    }
    
}