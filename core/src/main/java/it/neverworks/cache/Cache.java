package it.neverworks.cache;

import it.neverworks.lang.Guard;
import it.neverworks.lang.Strings;
import it.neverworks.lang.Wrapper;
import it.neverworks.lang.Reflection;
import it.neverworks.lang.SynchronizedPredicate;
import it.neverworks.lang.UnimplementedMethodException;

public interface Cache<T> {
    boolean contains( String key );
    T get( String key );
    void put( String key, T value );
    <T extends Cache> T set( String name, Object value );
    
    default int size() {
        throw new UnimplementedMethodException( this, "size" );
    }

    default void clear() {
        throw new UnimplementedMethodException( this, "clear" );
    }

    default T remove( String key ) {
        throw new UnimplementedMethodException( this, "remove" );
    }
    
    default T probe( String key, Wrapper initializer ) {
        if( initializer != null && ! this.contains( key ) ) {
            synchronized( this ) {
                if( ! this.contains( key ) ) {
                    T value = (T) initializer.asObject();
                    this.put( key, value );
                    return value;
                } else {
                    return this.get( key );
                }
            }
        } else {
            return this.get( key );
        }
    }

    default T probe( String key, CacheKeyInitializer initializer ) {
        if( initializer != null && ! this.contains( key ) ) {
            synchronized( this ) {
                if( ! this.contains( key ) ) {
                    T value = (T) initializer.asObjectFor( key );
                    this.put( key, value );
                    return value;
                } else {
                    return this.get( key );
                }
            }
        } else {
            return this.get( key );
        }
    }
    
    default Guard<Cache<T>> missing( String key ) {
        return new SynchronizedPredicate<Cache<T>>( this, t -> ! t.contains( key ) );
    }
    
    default Cache<T> create() {
        return (Cache<T>) Reflection.newInstance( this.getClass() );
    }
    
}