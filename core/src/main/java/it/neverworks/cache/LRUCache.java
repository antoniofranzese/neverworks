package it.neverworks.cache;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.AbstractModel;
import it.neverworks.model.Property;

public class LRUCache<T> extends SimpleLRUCache<T> {
    
    public LRUCache() {
        super();
    }
    
    public LRUCache( int max ) {
        super( max );
    }

    public boolean contains( String key ) {
        if( elements.containsKey( key ) ) {
            synchronized( elements ) {
                if( elements.containsKey( key ) ) {
                    return ! isExpired( elements.get( key ) );
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    
    public T get( String key ) {
        T result = null;
        if( this.contains( key ) ) {
            synchronized( elements ) {
                if( this.contains( key ) ) {
                    result = super.get( key );
                }
            }
        }
        return result;
    }
    
    public void put( String key, T value ) {
        synchronized( elements ) {
            super.put( key, value );
        }
    }
    
    public void clear() {
        synchronized( elements ) {
            super.clear();
        }
    }
    
    public T remove( String key ) {
        T removed = null;
        if( this.contains( key ) ) {
            synchronized( elements ) {
                if( this.contains( key ) ) {
                    removed = super.remove( key );
                }
            }
        }
        return removed;
    }
    
    
}