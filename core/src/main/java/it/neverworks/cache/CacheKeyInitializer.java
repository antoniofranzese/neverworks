package it.neverworks.cache;

import it.neverworks.lang.ParameterizedWrapper;

public interface CacheKeyInitializer extends ParameterizedWrapper<String> {
    
}