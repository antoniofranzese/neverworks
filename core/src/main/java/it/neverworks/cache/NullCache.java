package it.neverworks.cache;

import it.neverworks.lang.Guard;
import it.neverworks.lang.NullGuard;
import it.neverworks.lang.Wrapper;
import it.neverworks.model.utils.ToStringBuilder;

public class NullCache<T> implements Cache<T> {

    public final static Cache INSTANCE = new NullCache();
    
    public boolean contains( String key ) {
        return false;
    }
    
    public T get( String key ) {
        return null;
    }
    
    public void put( String key, T value ) {
        
    }

    public <T extends Cache> T set( String name, Object value ) {
        return (T)this;
    }
    
    public int size() {
        return 0;
    }
 
    public void clear() {
        
    }
    
    public T remove( String key ) {
        return null;
    }
    
    public Cache<T> create() {
        return (Cache<T>) INSTANCE;
    }
    
    public Guard<Cache<T>> missing( String key ) {
        return new NullGuard<Cache<T>>();
    }
    
    public T probe( String key, Wrapper initializer ) {
        return initializer != null ? (T) initializer.asObject() : null;
    }

    public T probe( String key, CacheKeyInitializer initializer ) {
        return initializer != null ? (T) initializer.asObjectFor( key ) : null;
    }
    
    public String toString() {
        return new ToStringBuilder( this )
            .toString();
    }
    
}