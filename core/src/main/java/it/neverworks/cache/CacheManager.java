package it.neverworks.cache;

import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import it.neverworks.model.features.Evaluate;
import it.neverworks.model.expressions.Expression;

public class CacheManager implements Evaluate {
    
    private static Map<String, Cache> caches = new HashMap<String, Cache>();
    private static Map<String, CacheProxy> proxies = new HashMap<String, CacheProxy>();
    
    public Object evaluateExpression( Expression expression ) {
        return get( expression.text() );
    }

    public static <T> Cache<T> get( String name ) {
        if( ! proxies.containsKey( name ) ) {
            synchronized( proxies ) {
                if( ! proxies.containsKey( name ) ) {
                    CacheProxy proxy = caches.containsKey( name ) ? new CacheProxy<T>( name, (Cache<T>)caches.get( name ) ) : new CacheProxy<T>( name );
                    proxies.put( name, proxy );
                }
            }
        }
        return proxies.get( name );
    }
    
    /* package */ static <T> Cache<T> retrieveCache( String name ) {
        return (Cache<T>)caches.get( name );
    }

    public static boolean has( String name ) {
        return caches.containsKey( name );
    }
    
    public static void put( String name, Cache cache ) {
        caches.put( name, cache );
        if( proxies.containsKey( name ) ) {
            proxies.get( name ).setActual( cache );
        }
    }

    public static <T> Cache<T> create( String name ) {
        if( has( name ) ) {
            return caches.get( name ).create();
        } else {
            return NullCache.INSTANCE;
        }
    }
    
    public void setCaches( Map<String, Cache> caches ){
        Set<String> currentNames = this.caches.keySet();
        this.caches = caches;
        for( String name: this.caches.keySet() ) {
            if( proxies.containsKey( name ) ) {
                proxies.get( name ).setActual( this.caches.get( name ) );
            }
        }
        
        currentNames.removeAll( this.caches.keySet() );
        if( currentNames.size() > 0 ) {
            for( String name: currentNames ) {
                if( proxies.containsKey( name ) ) {
                    proxies.get( name ).setActual( null );
                }
            }
        }
    }
    
    public void setItems( Map<String, Cache> caches ) {
        setCaches( caches );
    }
}