package it.neverworks.log;

public class Log4jLogger extends Logger {
    
    private static String WRAPPER = Logger.class.getName(); 
    
    private org.apache.log4j.Logger logger;
    
    public Log4jLogger( org.apache.log4j.Logger logger ) {
        this.logger = logger;
    }
    
    public Log4jLogger( Class target ) {
        this( org.apache.log4j.Logger.getLogger( target ) );
    }

    public Log4jLogger( String name ) {
        this( org.apache.log4j.Logger.getLogger( name ) );
    }
    
    public void setLogger( org.apache.log4j.Logger logger ) {
        this.logger = logger;
    }

    public void setName( String name ) {
        setLogger( org.apache.log4j.Logger.getLogger( name ) );
    }
    
    public void setTarget( Class target ) {
        setLogger( org.apache.log4j.Logger.getLogger( target ) );
    }
    
    public boolean fatal() {
        return org.apache.log4j.Level.FATAL.isGreaterOrEqual( this.logger.getEffectiveLevel() );
    }
    
    public void logFatal( String message ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.FATAL, message, null );
    }
    
    public void logFatal( Throwable throwable ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.FATAL, "", throwable );
    }
    
    public boolean error() {
        return org.apache.log4j.Level.ERROR.isGreaterOrEqual( this.logger.getEffectiveLevel() );
    }
    
    public void logError( String message ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.ERROR, message, null );
    }
    
    public void logError( Throwable throwable ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.ERROR, "", throwable );
    }
    
    public boolean warn() {
        return org.apache.log4j.Level.WARN.isGreaterOrEqual( this.logger.getEffectiveLevel() );
    }

    public void logWarn( String message ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.WARN, message, null );
    }
    
    public void logWarn( Throwable throwable ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.WARN, "", throwable );
    }
    
    public boolean info() {
        return org.apache.log4j.Level.INFO.isGreaterOrEqual( this.logger.getEffectiveLevel() );
    }
    
    public void logInfo( String message ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.INFO, message, null );
    }

    public void logInfo( Throwable throwable ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.INFO, "", throwable );
    }

    public boolean debug() {
        return org.apache.log4j.Level.DEBUG.isGreaterOrEqual( this.logger.getEffectiveLevel() );
    }
    
    public void logDebug( String message ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.DEBUG, message, null );
    }
    
    public void logDebug( Throwable throwable ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.DEBUG, "", throwable );
    }
    
    public boolean trace() {
        return org.apache.log4j.Level.TRACE.isGreaterOrEqual( this.logger.getEffectiveLevel() );
    }
    
    public void logTrace( String message ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.TRACE, message, null );
    }
    
    public void logTrace( Throwable throwable ) {
        this.logger.log( WRAPPER, org.apache.log4j.Level.TRACE, "", throwable );
    }
    
}