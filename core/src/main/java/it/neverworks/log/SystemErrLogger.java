package it.neverworks.log;

public class SystemErrLogger extends PrintStreamLogger {
    
    public SystemErrLogger() {
        super( System.err );
    }

    
}