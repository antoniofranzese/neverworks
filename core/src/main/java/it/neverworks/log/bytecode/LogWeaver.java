package it.neverworks.log.bytecode;

import java.util.List;
import java.util.ArrayList;

import javassist.ClassPool;
import javassist.Modifier;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.CtField;
import javassist.CtConstructor;
import javassist.NotFoundException;
import javassist.bytecode.ConstPool;
import javassist.bytecode.ClassFile;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.StringMemberValue;

import static it.neverworks.language.*;
import it.neverworks.lang.Strings;
import it.neverworks.model.description.MethodAnnotation;
import it.neverworks.bytecode.ByteCodeUtils;
import it.neverworks.bytecode.Weaver;
import it.neverworks.bytecode.WeaverContext;
import it.neverworks.bytecode.AutoWeaver;
import it.neverworks.bytecode.WeaverException;
import it.neverworks.log.Logger;

@AutoWeaver
public class LogWeaver implements Weaver {
    private CtClass LoggerClass = ByteCodeUtils.getType( "it.neverworks.log.Logger" );
    
    public boolean instrument( String className ) throws Exception {

        CtClass cls = ByteCodeUtils.getType( className );
        boolean instrumented = false;

        if( !cls.isInterface() && ! ByteCodeUtils.isMarked( cls, "logger" ) ) {
            try {
                instrumentClass( cls );
            } catch( Exception ex ) {
                throw new WeaverException( cls, ex );
            }
        }
        return instrumented;
        
    }
    
    private void instrumentClass( CtClass cls ) throws Exception {
        boolean performed = false;
        
        if( cls.isFrozen() ) {
            cls.defrost();
        }

        ClassFile clsFile = cls.getClassFile();
        ConstPool constPool = clsFile.getConstPool();

        List<CtField> instrumentables = new ArrayList<CtField>();
        CtField[] fields = cls.getFields();
        for( int i: range( fields ) ) {
            if( ( fields[ i ].getModifiers() & Modifier.STATIC ) != 0 
                && fields[ i ].getType().equals( LoggerClass ) 
                && fields[ i ].hasAnnotation( Logger.ForMe.class ) ) {

                instrumentables.add( fields[ i ] );
            }
        }

        fields = cls.getDeclaredFields();
        for( int i: range( fields ) ) {
            if( ( fields[ i ].getModifiers() & Modifier.STATIC ) != 0 
                && ( fields[ i ].getModifiers() & Modifier.PRIVATE ) != 0
                && fields[ i ].getType().equals( LoggerClass ) 
                && fields[ i ].hasAnnotation( Logger.ForMe.class ) ) {

                instrumentables.add( fields[ i ] );
            }
        }
        
        
        if( instrumentables.size() > 0 ) {
            for( CtField instrumentable: instrumentables ) {
                if( instrumentable.getDeclaringClass().equals( cls ) ) {
                    WeaverContext.debug( "Replacing " + cls.getName() + "." + instrumentable.getName() + " Logger field" );
                    cls.removeField( instrumentable );
                } else {
                    WeaverContext.debug( "Creating " + cls.getName() + "." + instrumentable.getName() + " Logger field" );
                }
    
                CtField field = new CtField( LoggerClass, instrumentable.getName(), cls );
                field.setModifiers( instrumentable.getModifiers() | Modifier.FINAL );
                
                if( instrumentable.getDeclaringClass().equals( cls ) ) {
                    ByteCodeUtils.createAnnotation( cls, Logger.ForMe.class ).annotate( field );
                    ByteCodeUtils.mark( field, "logger.instrumented" );
                } else {
                    ByteCodeUtils.mark( field, "logger.inherited" );
                }
    
                String initializer = "it.neverworks.log.Logger.of( " + cls.getName() +".class );";
                cls.addField( field, CtField.Initializer.byExpr( initializer ) );
            }

            ByteCodeUtils.mark( cls, "logger" );
        
            ByteCodeUtils.write( cls );
            
        }
        
    }
    
    private void instrumentField( CtClass cls, CtField field ) {
        
    }
}