package it.neverworks.log;

import java.io.PrintStream;

public class PrintStreamLogger extends Logger {
    
    protected PrintStream stream;
    protected Level level = Level.ERROR;
    
    public PrintStreamLogger( PrintStream stream ) {
        this.stream = stream;
    }

    public PrintStreamLogger withLevel( Level level ) {
        this.level = level;
        return this;
    }

    public boolean fatal() {
        return this.level.is( Level.FATAL );
    }
    
    public void logFatal( String message ) {
        this.stream.println( message );
    }

    public void logFatal( Throwable throwable) {
        print( throwable );
    }
    
    public boolean error() {
        return this.level.is( Level.ERROR );
    }
    
    public void logError( String message ) {
        this.stream.println( message );
    }

    public void logError( Throwable throwable ) {
        print( throwable );
    }
    
    public boolean warn() {
        return this.level.is( Level.WARN );
    }

    public void logWarn( String message ) {
        this.stream.println( message );
    }
    
    public void logWarn( Throwable throwable ) {
        print( throwable );
    }
    
    public boolean info() {
        return this.level.is( Level.INFO );
    }
    
    public void logInfo( String message ) {
        this.stream.println( message );
    }

    public void logInfo( Throwable throwable ) {
        print( throwable );
    }

    public boolean debug() {
        return this.level.is( Level.DEBUG );
    }
    
    public void logDebug( String message ) {
        this.stream.println( message );
    }
    
    public void logDebug( Throwable throwable ) {
        print( throwable );
    }
    
    public boolean trace() {
        return this.level.is( Level.TRACE );
    }
    
    public void logTrace( String message ) {
        this.stream.println( message );
    }
    
    public void logTrace( Throwable throwable ) {
        print( throwable );
    }
    
    protected void print( Throwable throwable ) {
        this.stream.println( throwable.getClass().getName() + ( throwable.getMessage() != null ? ( ": " + throwable.getMessage() ) : "" ) );
        throwable.printStackTrace( this.stream );
    }
}