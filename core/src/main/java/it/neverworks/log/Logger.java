package it.neverworks.log;

import java.util.Map;
import java.util.HashMap;
import java.util.Locale;
import java.util.function.Supplier;

import it.neverworks.lang.Strings;
import it.neverworks.lang.Properties;
import it.neverworks.model.types.TypeDefinition;
import it.neverworks.model.types.TypeFactory;
import it.neverworks.text.TextFormat;

public abstract class Logger {
    
    protected final static Locale LC = Locale.ITALIAN;
    
    public @interface ForMe {
        
    }
    
    public static Logger of( String target ) {
        return LoggerFactory.getLogger( target );
    }
    
    public static Logger of( Class target ) {
        return LoggerFactory.getLogger( target );
    }

    // Log4j drop-in replacement
    public static Logger getLogger( String target ) {
        return Logger.of( target );
    }
    
    // Log4j drop-in replacement
    public static Logger getLogger( Class target ) {
        return Logger.of( target );
    }

    public static Logger detect() {
        String cls = Thread.currentThread().getStackTrace()[ 2 ].getClassName();
        return LoggerFactory.getLogger( cls );
    }
    
    public enum Level {
        OFF( 0 ),
        FATAL( 1 ),
        ERROR( 2 ),
        WARN( 3 ),
        INFO( 4 ),
        DEBUG( 5 ),
        TRACE( 6 );

        private int value;

        private Level( int value ) {
            this.value = value;
        }

        public int value() {
            return this.value;
        }

        public boolean is( Level other ) {
            return this.value >= other.value;
        }
    }
    
    public class LevelLogger {
        
        private Level level;
        
        public LevelLogger( Level level ) {
            this.level = level;
        }

        public boolean enabled() {
            switch( level ) {
                case FATAL:
                    return Logger.this.fatal();
                case ERROR:
                    return Logger.this.error();
                case WARN:
                    return Logger.this.warn();
                case INFO:
                    return Logger.this.info();
                case DEBUG:
                    return Logger.this.debug();
                case TRACE:
                    return Logger.this.trace();
                default:
                    return false;
            }
        }
        
        public void log( String message ) {
            if( this.enabled() ) {
                switch( level ) {
                    case FATAL:
                        Logger.this.logFatal( message );
                        break;
                    case ERROR:
                        Logger.this.logError( message );
                        break;
                    case WARN:
                        Logger.this.logWarn( message );
                        break;
                    case INFO:
                        Logger.this.logInfo( message );
                        break;
                    case DEBUG:
                        Logger.this.logDebug( message );
                        break;
                    case TRACE:
                        Logger.this.logTrace( message );
                        break;
                    default:
                        break;
                }
            }
        }
        
        public void log( Throwable throwable ) {
            if( this.enabled() ) {
                switch( level ) {
                    case FATAL:
                        Logger.this.logFatal( throwable );
                        break;
                    case ERROR:
                        Logger.this.logError( throwable );
                        break;
                    case WARN:
                        Logger.this.logWarn( throwable );
                        break;
                    case INFO:
                        Logger.this.logInfo( throwable );
                        break;
                    case DEBUG:
                        Logger.this.logDebug( throwable );
                        break;
                    case TRACE:
                        Logger.this.logTrace( throwable );
                        break;
                    default:
                        break;
                }
            }
        }
        
        public void log( String message, Object param1 ) {
            if( this.enabled() ) { this.log( TextFormat.format( LC, message, param1 ) ); }
        }
        
        public void log( String message, Object param1, Object param2 ) {
            if( this.enabled() ) { this.log( TextFormat.format( LC, message, param1, param2 ) ); }
        }
    
        public void log( String message, Object param1, Object param2, Object param3 ) {
            if( this.enabled() ) { this.log( TextFormat.format( LC, message, param1, param2, param3 ) ); }
        }
    
        public void log( String message, Object param1, Object param2, Object param3, Object param4 ) {
            if( this.enabled() ) { this.log( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
        }
    
        public void log( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
            if( this.enabled() ) { this.log( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
        }
    
        public void log( String message, Object... params ) {
            if( this.enabled() ) { this.log( TextFormat.format( LC, message, params ) ); }
        }

        public void log( Object object ) {
            if( this.enabled() ) { this.log( Strings.valueOf( object ), null ); }
        }

        public void log( Supplier<String> supplier ) {
            if( this.enabled() && supplier != null ) { this.log( supplier.get(), null ); }
        }

    }
    
    private final static TypeDefinition<Level> LevelType = TypeFactory.auto( Level.class );
    private Map<Level, LevelLogger> levelLoggers;
    
    public LevelLogger level( String level ) {
        return level( LevelType.process( level ) );
    }

    public LevelLogger level( Level level ) {
        if( this.levelLoggers == null ) {
            this.levelLoggers = new HashMap<Level, LevelLogger>();
        }
        if( !( this.levelLoggers.containsKey( level ) ) ) {
            this.levelLoggers.put( level, new LevelLogger( level ) );
        }
        return this.levelLoggers.get( level );
    }
    
    public abstract boolean fatal();
    public abstract void logFatal( String message );
    public abstract void logFatal( Throwable throwable );
    
    public boolean isFatalEnabled() {
        return this.fatal();
    }

    public void fatal( Throwable throwable ) {
        if( this.fatal() ) { this.logFatal( throwable ); }
    }
    
    public void fatal( String message ) {
        if( this.fatal() ) { this.logFatal( message ); }
    }

    public void fatal( String message, Object param1 ) {
        if( this.fatal() ) { this.logFatal( TextFormat.format( LC, message, param1 ) ); }
    }
        
    public void fatal( String message, Object param1, Object param2 ) {
        if( this.fatal() ) { this.logFatal( TextFormat.format( LC, message, param1, param2 ) ); }
    }
    
    public void fatal( String message, Object param1, Object param2, Object param3 ) {
        if( this.fatal() ) { this.logFatal( TextFormat.format( LC, message, param1, param2, param3 ) ); }
    }
    
    public void fatal( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( this.fatal() ) { this.logFatal( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
    }
    
    public void fatal( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( this.fatal() ) { this.logFatal( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public void fatal( String message, Object... params ) {
        if( this.fatal() ) { this.logFatal( TextFormat.format( LC, message, params ) ); }
    }

    public void fatal( Supplier<String> supplier ) {
        if( this.fatal() && supplier != null ) { this.logFatal( supplier.get() ); }
    }

    public abstract boolean error();
    public abstract void logError( String message );
    public abstract void logError( Throwable throwable );
    
    public boolean isErrorEnabled() {
        return this.error();
    }
    
    public void error( Throwable throwable ) {
        if( this.error() ) { this.logError( throwable ); }
    }
    
    public void error( String message ) {
        if( this.error() ) { this.logError( message ); }
    }
    
    public void error( String message, Object param1 ) {
        if( this.error() ) { this.logError( TextFormat.format( LC, message, param1 ) ); }
    }
        
    public void error( String message, Object param1, Object param2 ) {
        if( this.error() ) { this.logError( TextFormat.format( LC, message, param1, param2 ) ); }
    }
    
    public void error( String message, Object param1, Object param2, Object param3 ) {
        if( this.error() ) { this.logError( TextFormat.format( LC, message, param1, param2, param3 ) ); }
    }
    
    public void error( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( this.error() ) { this.logError( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
    }
    
    public void error( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( this.error() ) { this.logError( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public void error( String message, Object... params ) {
        if( this.error() ) { this.logError( TextFormat.format( LC, message, params ) ); }
    }

    public void error( Supplier<String> supplier ) {
        if( this.error() && supplier != null ) { this.logError( supplier.get() ); }
    }
        
    public abstract boolean warn();
    public abstract void logWarn( String message );
    public abstract void logWarn( Throwable throwable );
    
    public boolean isWarnEnabled() {
        return this.warn();
    }
    
    public void warn( Throwable throwable ) {
        if( this.warn() ) { this.logWarn( throwable ); }
    }
    
    public void warn( String message ) {
        if( this.warn() ) { this.logWarn( message ); }
    }
    
    public void warn( String message, Object param1 ) {
        if( this.warn() ) { this.logWarn( TextFormat.format( LC, message, param1 ) ); }
    }
        
    public void warn( String message, Object param1, Object param2 ) {
        if( this.warn() ) { this.logWarn( TextFormat.format( LC, message, param1, param2 ) ); }
    }
    
    public void warn( String message, Object param1, Object param2, Object param3 ) {
        if( this.warn() ) { this.logWarn( TextFormat.format( LC, message, param1, param2, param3 ) ); }
    }
    
    public void warn( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( this.warn() ) { this.logWarn( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
    }
    
    public void warn( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( this.warn() ) { this.logWarn( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public void warn( String message, Object... params ) {
        if( this.warn() ) { this.logWarn( TextFormat.format( LC, message, params ) ); }
    }

    public void warn( Supplier<String> supplier ) {
        if( this.warn() && supplier != null ) { this.logWarn( supplier.get() ); }
    }
    
    public abstract boolean info();
    public abstract void logInfo( String message );
    public abstract void logInfo( Throwable throwable );
    
    public boolean isInfoEnabled() {
        return this.info();
    }
    
    public void info( Throwable throwable ) {
        if( this.info() ) { this.logInfo( throwable ); }
    }
    
    public void info( String message ) {
        if( this.info() ) { this.logInfo( message ); }
    }
    
    public void info( String message, Object param1 ) {
        if( this.info() ) { this.logInfo( TextFormat.format( LC, message, param1 ) ); }
    }
        
    public void info( String message, Object param1, Object param2 ) {
        if( this.info() ) { this.logInfo( TextFormat.format( LC, message, param1, param2 ) ); }
    }
    
    public void info( String message, Object param1, Object param2, Object param3 ) {
        if( this.info() ) { this.logInfo( TextFormat.format( LC, message, param1, param2, param3 ) ); }
    }
    
    public void info( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( this.info() ) { this.logInfo( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
    }
    
    public void info( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( this.info() ) { this.logInfo( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public void info( String message, Object... params ) {
        if( this.info() ) { this.logInfo( TextFormat.format( LC, message, params ) ); }
    }

    public void info( Supplier<String> supplier ) {
        if( this.info() && supplier != null ) { this.logInfo( supplier.get() ); }
    }
    
    public abstract boolean debug();
    public abstract void logDebug( String message );
    public abstract void logDebug( Throwable throwable );
    
    public boolean isDebugEnabled() {
        return this.debug();
    }
    
    public void debug( Throwable throwable ) {
        if( this.debug() ) { this.logDebug( throwable ); }
    }
    
    public void debug( String message ) {
        if( this.debug() ) { this.logDebug( message ); }
    }
    
    public void debug( String message, Object param1 ) {
        if( this.debug() ) { this.logDebug( TextFormat.format( LC, message, param1 ) ); }
    }
        
    public void debug( String message, Object param1, Object param2 ) {
        if( this.debug() ) { this.logDebug( TextFormat.format( LC, message, param1, param2 ) ); }
    }
    
    public void debug( String message, Object param1, Object param2, Object param3 ) {
        if( this.debug() ) { this.logDebug( TextFormat.format( LC, message, param1, param2, param3 ) ); }
    }
    
    public void debug( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( this.debug() ) { this.logDebug( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
    }
    
    public void debug( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( this.debug() ) { this.logDebug( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public void debug( String message, Object... params ) {
        if( this.debug() ) { this.logDebug( TextFormat.format( LC, message, params ) ); }
    }
        
    public void debug( Supplier<String> supplier ) {
        if( this.debug() && supplier != null ) { this.logDebug( supplier.get() ); }
    }

    public abstract boolean trace();
    public abstract void logTrace( String message );
    public abstract void logTrace( Throwable throwable );

    public boolean isTraceEnabled() {
        return this.trace();
    }
    
    public void trace( Throwable throwable ) {
        if( this.trace() ) { this.logTrace( throwable ); }
    }
    
    public void trace( String message ) {
        if( this.trace() ) { this.logTrace( message ); }
    }

    public void trace( String message, Object param1 ) {
        if( this.trace() ) { this.logTrace( TextFormat.format( LC, message, param1 ) ); }
    }
        
    public void trace( String message, Object param1, Object param2 ) {
        if( this.trace() ) { this.logTrace( TextFormat.format( LC, message, param1, param2 ) ); }
    }
    
    public void trace( String message, Object param1, Object param2, Object param3 ) {
        if( this.trace() ) { this.logTrace( TextFormat.format( LC, message, param1, param2, param3 ) ); }
    }
    
    public void trace( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( this.trace() ) { this.logTrace( TextFormat.format( LC, message, param1, param2, param3, param4 ) ); }
    }
    
    public void trace( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( this.trace() ) { this.logTrace( TextFormat.format( LC, message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public void trace( String message, Object... params ) {
        if( this.trace() ) { this.logTrace( TextFormat.format( LC, message, params ) ); }
    }

    public void trace( Supplier<String> supplier ) {
        if( this.trace() && supplier != null ) { this.logTrace( supplier.get() ); }
    }
    
    public Logger set( String name, Object value ) {
        Properties.set( this, name, value );
        return this;
    }
}