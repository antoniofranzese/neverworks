package it.neverworks.log;

public class LoggerFactory {
    
    public static Logger getLogger( String target ) {
        return new Log4jLogger( target );
    }
    
    public static Logger getLogger( Class target ) {
        return new Log4jLogger( target );
    }
    
}