package it.neverworks.log;

public class SystemOutLogger extends PrintStreamLogger {
    
    public SystemOutLogger() {
        super( System.out );
    }

    
}