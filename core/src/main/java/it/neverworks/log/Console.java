package it.neverworks.log;

import it.neverworks.lang.Strings;
import it.neverworks.text.TextFormat;

public class Console {
    
    private static Logger consoleLogger = new SystemOutLogger();
    
    public static void log( String message, Object param1 ) {
        if( consoleLogger.info() ) { consoleLogger.info( TextFormat.format( message, param1 ) ); }
    }
        
    public static void log( String message, Object param1, Object param2 ) {
        if( consoleLogger.info() ) { consoleLogger.info( TextFormat.format( message, param1, param2 ) ); }
    }
    
    public static void log( String message, Object param1, Object param2, Object param3 ) {
        if( consoleLogger.info() ) { consoleLogger.info( TextFormat.format( message, param1, param2, param3 ) ); }
    }
    
    public static void log( String message, Object param1, Object param2, Object param3, Object param4 ) {
        if( consoleLogger.info() ) { consoleLogger.info( TextFormat.format( message, param1, param2, param3, param4 ) ); }
    }
    
    public static void log( String message, Object param1, Object param2, Object param3, Object param4, Object param5 ) {
        if( consoleLogger.info() ) { consoleLogger.info( TextFormat.format( message, param1, param2, param3, param4, param5 ) ); }
    }
    
    public static void log( String message, Object... params ) {
        if( consoleLogger.info() ) { consoleLogger.info( TextFormat.format( message, params ) ); }
    }
    
    public static void log( Object object ) {
        if( consoleLogger.info() ) { 
            if( object instanceof Throwable ) {
                consoleLogger.logInfo( (Throwable) object );
            } else {
                consoleLogger.logInfo( Strings.valueOf( object ) );
            }
        }
    }
    
    public void setLogger( Logger logger ) {
        if( logger != null ) {
            consoleLogger = logger;
        }
    }
    
}