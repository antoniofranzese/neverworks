package it.neverworks.log;

public class SilentLogger extends Logger {
    
    public boolean fatal() {
        return false;
    }
    
    public void logFatal( String message ) {
        // Nothing to do
    }

    public void logFatal( Throwable throwable ) {
        // Nothing to do
    }
    
    public boolean error() {
        return false;
    }
    
    public void logError( String message ) {
        // Nothing to do
    }
    
    public void logError( Throwable throwable ) {
        // Nothing to do
    }
    
    public boolean warn() {
        return false;
    }

    public void logWarn( String message ) {
        // Nothing to do
    }
    
    public void logWarn( Throwable throwable ) {
        // Nothing to do
    }
    
    public boolean info() {
        return false;
    }
    
    public void logInfo( String message ) {
        // Nothing to do
    }

    public void logInfo( Throwable throwable ) {
        // Nothing to do
    }

    public boolean debug() {
        return false;
    }
    
    public void logDebug( String message ) {
        // Nothing to do
    }
    
    public void logDebug( Throwable throwable ) {
        // Nothing to do
    }
    
    public boolean trace() {
        return false;
    }
    
    public void logTrace( String message ) {
        // Nothing to do
    }
    
    public void logTrace( Throwable throwable ) {
        // Nothing to do
    }
    
}